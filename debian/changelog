conquests (1.2) precise; urgency=low

  * Fixed makefile for Ubuntu 12.04.
  * Slow time rate on some Linux installations.
  * Mousewheel zoom wasn't working when mouse was over minimap.
  * FPS now accurate on Linux.
  * Application icon.
  * Display history for military strength too, under F5 scores screens.
  * Improved method of not displaying the movement buttons (wasn't working
    right for high resolutions).

 -- Mark Harman <mark.harman@ntlworld.com>  Sat, 22 Sep 2012 20:42:00 +0100

conquests (1.1) maverick; urgency=low

  * Fixed source code to compile on GCC 4.6 (Ubuntu 11.10 Oneiric).

 -- Mark Harman <mark.harman@ntlworld.com>  Fri, 4 May 2012 01:18:00 +0000

conquests (1.0) maverick; urgency=low

  * Crash when declaring war on a civilization not yet made contact with,
    by sea travel.
  * Fixed some minor memory leaks.
  * Sound wasn't being properly freed on exit; now fades out.
  * Issues with drawing rectangles on OpenGL.
  * Water animation was too fast on Linux.
  * AI now prefers to stack units, and prefers to attack larger cities
    (particularly when invading by sea).
  * Rebellions can now occur - units that attack your own civilization!
  * Win conditions - conquering the entire world, or having the highest
    power after a set number of turns.
  * All cities now produce power depending on size; increased power output
    of city improvements.
  * New civilization: Mayans.
  * New Great Projects - improvements that are specific to a civilization.
  * Modern Tanks cost changed from 3000 to 2500.
  * Sam Launcher cost changed from 2000 to 1600.
  * City window now has option to give advice on what to build next.
  * New option for Units window "Activate All Automated Units Of This
    Type".
  * Support for touchscreens / one mouse button - right mouse button no
    longer required.
  * Option to save world map of current game.
  * Scores window now has buttons to display historical graphs showing
    Power, Population, Production, Science, Territory.
  * Progress bar on game startup.
  * Leader names for civilization races.
  * Information about each civilization displayed when choosing a
    civilization to play.
  * Linux version now installs to /opt.
  * Linux version now stores data in ~/.config/gigalomania instead of
    ~/.gigalomania.
  * Explains that entering enemy territory is an act of war, when player
    tries to do so (rather than simply asking whether to declare war).
  * When a player city builds an improvement, a more intelligent choice is
    chosen for the default new buildable, instead of simply a defensive
    unit.
  * UI now supports click/drag to move map view around (useful for
    touchscreen!)
  * Save game files now sorted by creation date (most recent first); option
    to sort by filename.
  * Horizontal scrolling for string gadgets (no longer has overdraw).
  * Increased save filename limit to 255 characters.
  * Horizontal scrollbars for listboxes (no longer has overdraw).
  * Improved frame rate when viewing city windows.

 -- Mark Harman <mark.harman@ntlworld.com>  Sun, 19 Feb 2012 16:09:00 +0000

conquests (0.15) maverick; urgency=low

  * First packaged release.

 -- Mark Harman <mark.harman@ntlworld.com>  Sat, 10 Sep 2011 00:35:49 +0100
