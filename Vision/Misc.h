#pragma once

#include "VisionUtils.h"

//#define _DEBUG

/*const float RES_LARGE = 1000;
const float RES_LINEAR = 1.0e-3;
const float RES_ANGULAR = RES_LINEAR / RES_LARGE;*/
/*extern const float RES_LARGE;
extern const float RES_LINEAR;
extern const float RES_ANGULAR;*/

class Color {
protected:
	unsigned char col[4];
public:
	Color() {
		seti(255,255,255,255);
	}
	Color(int R,int G,int B) {
		seti(R,G,B);
	}
	Color(int R,int G,int B,int A) {
		seti(R,G,B,A);
	}
	void seti(int R,int G,int B) {
		seti(R,G,B,255);
	}
	void seti(int R,int G,int B,int A) {
		col[0] = (unsigned char)R;
		col[1] = (unsigned char)G;
		col[2] = (unsigned char)B;
		col[3] = (unsigned char)A;
	}
	void setf(float R,float G,float B) {
		setf(R,G,B,1.0);
	}
	void setf(float R,float G,float B,float A) {
		col[0] = (unsigned char)(R*255.0);
		col[1] = (unsigned char)(G*255.0);
		col[2] = (unsigned char)(B*255.0);
		col[3] = (unsigned char)(A*255.0);
	}
	void setAi(int A) {
		col[3] = (unsigned char)A;
	}
	void setAf(float A) {
		col[3] = (unsigned char)(A*255.0);
	}
	void set(const Color *color) {
		col[0] = color->col[0];
		col[1] = color->col[1];
		col[2] = color->col[2];
		col[3] = color->col[3];
	}
	void setRGB(unsigned char rgb[3]) {
		col[0] = rgb[0];
		col[1] = rgb[1];
		col[2] = rgb[2];
		col[3] = 255;
	}
	int getR() const {
		return col[0];
	}
	int getG() const {
		return col[1];
	}
	int getB() const {
		return col[2];
	}
	int getA() const {
		return col[3];
	}
	void get(int *R,int *G,int *B) const {
		*R = col[0];
		*G = col[1];
		*B = col[2];
	}
	/*unsigned char *getArray() {
	return this->col;
	}*/
	float getRf() const {
		return ((float)col[0])/255.0f;
	}
	float getGf() const {
		return ((float)col[1])/255.0f;
	}
	float getBf() const {
		return ((float)col[2])/255.0f;
	}
	float getAf() const {
		return ((float)col[3])/255.0f;
	}
	void floatArray(float *array) const {
		for(int i=0;i<4;i++) {
			array[i] = ((float)col[i])/255.0f;
		}
	}
	void array(unsigned char *array) const {
		for(int i=0;i<4;i++) {
			array[i] = col[i];
		}
	}

	/*const*/ Color operator+ () const {
		return Color(col[0],col[1],col[2],col[3]);
		//return (*this);
	}
	const Color& operator= (const Color& v) {
		for(int i=0;i<4;i++)
			col[i] = v.col[i];
		return *this;
	}
	/*const*/ Color& operator+= (const Color& v) {
		for(int i=0;i<4;i++)
			col[i] += v.col[i];
		return *this;
	}
	/*const*/ Color& operator-= (const Color& v) {
		for(int i=0;i<4;i++)
			col[i] -= v.col[i];
		return *this;
	}
	/*const*/ Color& operator*= (const float& s) {
		scale(s);
		return *this;
	}
	/*const*/ Color& operator/= (const float& s) {
		const float r = 1 / s;
		scale(r);
		return *this;
	}
	/*const*/ Color operator+ (const Color& v) {
		return Color(col[0] + v.col[0], col[1] + v.col[1], col[2] + v.col[2], col[3] + v.col[3]);
	}
	/*const*/ Color operator- (const Color& v) {
		return Color(col[0] - v.col[0], col[1] - v.col[1], col[2] - v.col[2], col[3] - v.col[3]);
	}
	/*const*/ Color operator* (const float& s) {
		return Color((int)(s*col[0]),(int)(s*col[1]),(int)(s*col[2]),(int)(s*col[3]));
	}
	/*friend inline const Vector3D operator* (const float& s,const Vector3D& v) {
	return v * s;
	}*/
	/*const*/ Color operator/ (float s) const {
		s = 1/s;
		return Color((int)(s*col[0]),(int)(s*col[1]),(int)(s*col[2]),(int)(s*col[3]));
	}

	bool equals(Color *that) const {
		bool equal = true;
		for(int i=0;i<4 && equal;i++) {
			equal = ( this->col[i] == that->col[i] );
		}
		return equal;
	}
	void scale(float s) {
		for(int i=0;i<4;i++) {
			col[i] = (unsigned char)(col[i]*s);
		}
	}
	void add(Color *c) {
		int newcol[4]; // must use (signed) int to avoid overflow
		for(int i=0;i<4;i++) {
			newcol[i] = col[i] + c->col[i];
			if( newcol[i] > 255 )
				col[i] = 255;
			else if( newcol[i] < 0 )
				col[i] = 0;
			else
				col[i] = (unsigned char)newcol[i];

		}
	}
	void addf(float R,float G,float B) {
		int newcol[3]; // must use (signed) int to avoid overflow
		newcol[0] = (int)(R * 255.0) + (int)col[0];
		newcol[1] = (int)(G * 255.0) + (int)col[1];
		newcol[2] = (int)(B * 255.0) + (int)col[2];
		for(int i=0;i<3;i++) {
			if( newcol[i] > 255 )
				col[i] = 255;
			else if( newcol[i] < 0 )
				col[i] = 0;
			else
				col[i] = (unsigned char)newcol[i];
		}
	}
	/*void add(float R,float G,float B,bool capped) {
	col[0] += R;
	col[1] += G;
	col[2] += B;
	if(capped)
	cap();
	}
	void add(float a) {
	for(int i=0;i<4;i++)
	col[i] += a;
	cap();
	}
	void cap() {
	for(int i=0;i<4;i++) {
	if( col[i] < 0 )
	col[i] = 0.0;
	else if( col[i] > 255 )
	col[i] = 255;
	}
	}*/
	unsigned char getMaxRGB() const {
		unsigned char max = (col[0] > col[1]) ? col[0] : col[1];
		max = (max > col[2]) ? max : col[2];
		return max;
	}
	float getMaxRGBf() const {
		unsigned char max = getMaxRGB();
		return ((float)max)/255.0f;
	}
};

class Point3DChangedPositionListener {
public:
	virtual void changedPosition()=0;
};

class Point3D {
	//SceneGraphNode *node;
	Point3DChangedPositionListener *changedPositionListener;
	Vector3D pos;
	Rotation3D rot;
	bool infinite;
	void checkNode();
public:
	Point3D();
	/*void setNode(SceneGraphNode *node) {
		this->node = node;
	}*/
	void registerChangedPositionListener(Point3DChangedPositionListener *changedPositionListener) {
		this->changedPositionListener = changedPositionListener;
	}
	void setInfinite(bool infinite) {
		this->infinite = infinite;
	}
	bool isInfinite() const {
		return this->infinite;
	}
	void moveTo(float x,float y,float z) {
		pos.x = x;
		pos.y = y;
		pos.z = z;
		checkNode();
	}
	void moveTo(const Vector3D &v) {
		moveTo(v.x, v.y, v.z);
	}
	void move(float x,float y,float z) {
		pos.x+=x;
		pos.y+=y;
		pos.z+=z;
		checkNode();
	}
	void move(const Vector3D *v) {
		move(v->x, v->y, v->z);
	}
	void move(const Vector3D *v,float d) {
		move( v->x*d, v->y*d, v->z*d );
	}
	void moveDirection(Vector3D v) {
		Matrix mat;
		mat.setRotation(rot);
		mat.transformVector(&v);
		move(&v);
	}
	void moveDirection(Vector3D v,float d) {
		Matrix mat;
		mat.setRotation(rot);
		mat.transformVector(&v);
		move(&v,d);
	}
	void moveDirectionX(Vector3D v,float d) {
		Matrix mat;
		float x, y, z;
		this->rot.getEuler(&x, &y, &z);
		Rotation3D r;
		r.rotateToEuler(x,0,0);
		mat.setRotation(r);
		mat.transformVector(&v);
		move(&v,d);
	}
	void moveDirectionY(Vector3D v,float d) {
		Matrix mat;
		float x, y, z;
		this->rot.getEuler(&x, &y, &z);
		Rotation3D r;
		r.rotateToEuler(0,y,0);
		mat.setRotation(r);
		mat.transformVector(&v);
		move(&v,d);
	}
	void multGLMatrix() const;
	void multGLMatrixInverse() const;
	Vector3D getPosition(bool *is_infinite) const {
		*is_infinite = this->infinite;
		return this->pos;
	}
	Rotation3D getRotation() const {
		return this->rot;
	}
	void rotateToIdentity() {
		this->rot.rotateToIdentity();
	}
	void rotateTo(const Rotation3D &rot) {
		this->rot.rotateTo(rot);
	}
	void rotate(float x,float y,float z) {
		this->rot.rotate(x, y, z);
	}
	void rotateLocal(float x,float y,float z) {
		this->rot.rotateLocal(x, y, z);
	}
	void rotateToEuler(float x,float y,float z) {
		this->rot.rotateToEuler(x, y, z);
	}
	void rotateToDirection(const Vector3D &axis, const Vector3D &dir) {
		this->rot.rotateToDirection(axis, dir);
	}
	void rotateToDirection(const Vector3D &dir) {
		this->rot.rotateToDirection(dir);
	}
};

/*class DirectionablePoint3D : public Point3D {
public:
Vector3D dir;
DLL DirectionablePoint3D();
DLL DirectionablePoint3D(float x,float y,float z,float rx,float ry,float rz);
DLL inline void resetDir() {
dir.set(0,0,-1);
Matrix mat;
mat.setRotationMatrix(&rot);
mat.transformVector(&dir);
}
DLL inline void moveForwards(float d) {
move(&dir,d);
}
};*/

/*#ifdef _DEBUG

class AllocInfo {
public:
unsigned int address;
unsigned int size;
char file[256];
int line;
static Vector allocs;
DLL AllocInfo(unsigned int address,unsigned int size, const char *file, int line);
};

//void AddTrack(DWORD ptr, unsigned int size, const char *file, int line);
static void AddTrack(DWORD ptr, unsigned int size, const char *file, int line) {
AllocInfo *allocInfo = new AllocInfo(ptr,size,file,line);
AllocInfo::allocs.add(allocInfo);
}

bool RemoveTrack(DWORD ptr);
void dumpAllocs();

inline void * __cdecl operator new(unsigned int size, const char *file, int line) {
void *ptr = (void *)malloc(size);
AddTrack((DWORD)ptr, size, file, line);
return(ptr);
};
inline void * __cdecl operator new[](unsigned int size, const char *file, int line) {
void *ptr = (void *)malloc(size);
AddTrack((DWORD)ptr, size, file, line);
return(ptr);
};
inline void __cdecl operator delete(void *ptr) {
RemoveTrack((DWORD)ptr);
free(ptr);
};
inline void __cdecl operator delete[](void *ptr) {
RemoveTrack((DWORD)ptr);
free(ptr);
};

//inline void * malloc_track(unsigned int size) {
inline void * malloc_track(unsigned int size, const char *file, int line) {
void *ptr = (void *)malloc(size);
AddTrack((DWORD)ptr, size, file, line);
//AddTrack((DWORD)ptr, size, "unknown", -1);
return(ptr);
};
inline void free_track(void *ptr) {
RemoveTrack((DWORD)ptr);
free(ptr);
};

#endif

#ifdef _DEBUG
#define DEBUG_NEW new(__FILE__, __LINE__)
#define malloc( x ) malloc_track( x, __FILE__, __LINE__ )
#define free free_track
#else
#define DEBUG_NEW new
#endif
#define new DEBUG_NEW*/
