#define _CRT_SECURE_NO_WARNINGS

#include "Loader.h"
#include "VisionUtils.h"
#include "Entity.h"
#include "GraphicsEnvironment.h"
#include "Renderer.h"
#include "Modeller.h"
#include "RenderData.h"

#include <cstring>

#include <string>
using std::string;

// needed for Windows at least, prefer to use std
#undef min
#undef max

//---------------------------------------------------------------------------

VI_SceneGraphNode *Loader::importEntityMD2(VI_Mesh **v_obj,const char *filename,const char *texture) {
	*v_obj = NULL;
	VI_Mesh *obj = Loader::importMD2(filename, texture);
	//Entity *entity = new Entity(obj);
	VI_SceneGraphNode *node = VI_createSceneGraphNode(obj);
	*v_obj = obj;
	return node;
}

//

class NamedResource {
protected:
	//char *name;
	string name;
	V_CLASS_t type;
public:
	void *ptr;

	NamedResource(const char *name, V_CLASS_t type, void *ptr) {
		this->name = name;
		this->type = type;
		this->ptr = ptr;
	}
	/*~NamedResource() {
		delete [] this->name;
	}*/
	bool equals(const char *cmpname, V_CLASS_t cmptype) const {
		//return ( type == cmptype && _stricmp( name.c_str(), cmpname ) == 0 );
		return ( type == cmptype && stricmp( name.c_str(), cmpname ) == 0 );
	}
};

class TDS_Material {
public:
	string name;
	Color col;
	TDS_Material * next;
};

void Loader::fskip(FILE *fp, int num_bytes) {
	for (int i=0; i<num_bytes; i++)
		fgetc(fp);
}

bool Loader::fgetLine(FILE *fp, char *ptr) {
	// returns whether eof
	for(;;) {
		if(feof(fp)) {
			*ptr=0;
			return true;
		}
		*ptr = (char)fgetc(fp);
		//if(*ptr==10 || *ptr==13 || *ptr==0) {
		if(*ptr==10 || *ptr==0) {
			*ptr=0;
			return false;
		}
		if(*ptr==13)
			*ptr=0;
		ptr++;
	}
}

char * Loader::stripWhiteSpace(char *ptr) {
	char * str = ptr;
	while(*ptr!=0) {
		if(*ptr==' ' || *ptr=='\t')
			str++;
		else
			break;
		ptr++;
	}
	return str;
}

string getFolder(const string &filename) {
	const char *slash = filename.c_str();
	for(;;) {
		const char *next_slash = strchr(slash,'/');
		const char *next_backslash = strchr(slash,'\\');
		if( next_slash == NULL && next_backslash == NULL )
			break;
		else if( next_slash == NULL )
			slash = next_backslash+1;
		else if( next_backslash == NULL )
			slash = next_slash+1;
		else {
			slash = ( ( next_backslash < next_slash ) ? next_backslash : next_slash ) + 1;
		}
	}
	string folder;
	if(slash != filename.c_str()) {
		size_t n = slash - filename.c_str();
		folder = filename.substr(0, n);
	}
	return folder;
}

#define MAX_SKINNAME 64

struct md2_stIndex_t {
	short   s;
	short   t;
};

struct md2_framePoint_t {
	unsigned char v[3]; // scaled byte to fit in frame mins/maxs
	unsigned char lightnormalindex;
};

struct md2_mesh_t {
	unsigned short index_xyz[3];
	unsigned short index_st[3];
};

struct md2_texCoord_t {
	float s;
	float t;
};

struct md2_frame_t {
	float scale[3];
	float translate[3];
	char name[16];
	md2_framePoint_t fp[1];
};

struct md2_dmdl_t {
	int                     ident;
	int                     version;
	int                     skinwidth;
	int                     skinheight;
	int                     framesize;              // byte size of each frame
	int                     num_skins;
	int                     num_xyz;
	int                     num_st;                 // greater than num_xyz for seams
	int                     num_tris;
	int                     num_glcmds;             // dwords in strip/fan command list
	int                     num_frames;
	int                     ofs_skins;              // each skin is a MAX_SKINNAME string
	int                     ofs_st;                 // byte offset from start for stverts
	int                     ofs_tris;               // offset for dtriangles
	int                     ofs_frames;             // offset for first frame
	int                     ofs_glcmds;
	int                     ofs_end;                // end of file
};

const int MAX_LEVELS=64;
const int MAX_PROTO=64;

vector<string> Loader::getWords(const char *str) {
	vector<string> words;
	const char *wordstart = NULL;
	for(;;) {
		if( *str != ' ' && *str != '\t' && *str != '\0' && wordstart==NULL ) {
			wordstart = str;
		}
		else if( ( *str == ' ' || *str == '\t' || *str == '\0' ) && wordstart != NULL ) {
			string word;
			for(const char *ptr = wordstart;ptr<str;ptr++) {
				word.push_back(*ptr);
			}
			words.push_back(word);
			wordstart = NULL;
		}
		if( *str == '\0' )
			break;
		str++;
	}
	return words;
}

class RWX_VertexMap {
public:
	int vx_indx; // corresponding index in the RWX file
	bool color_set; // has a colour been set for this vertex yet?
	int col_indx;
	RWX_VertexMap(int vx_indx) {
		this->vx_indx = vx_indx;
		this->color_set = false;
		this->col_indx = -1;
	}
};

int find_real_vertex_index(VI_VObject *obj,vector<RWX_VertexMap> *vertex_maps,vector<TextureUV> *uv_entries,int vx_indx,int col_indx,Color color) {
	int real_vx_indx = -1;
	for(size_t i=0;i<vertex_maps->size() && real_vx_indx == -1;i++) {
		//RWX_VertexMap *vx_map = static_cast<RWX_VertexMap *>(vertex_maps->get(i));
		RWX_VertexMap *vx_map = &vertex_maps->at(i);
		if( vx_map->vx_indx == vx_indx && ( !vx_map->color_set || vx_map->col_indx == col_indx ) ) {
			//if( vx_map->vx_indx == vx_indx ) {
			real_vx_indx = i;
			vx_map->color_set = true;
			vx_map->col_indx = col_indx;
			//Vertex3D *verts = obj->getVertices(0);
			//Vertex3D *vx = &verts[real_vx_indx];
			//vx->color.set(&color);
			/*Color *colors = obj->getColors();
			colors[real_vx_indx].set(&color);*/
			obj->setVertexColori(real_vx_indx, color.getR(), color.getG(), color.getB());
		}
	}
	//real_vx_indx = -1;
	if( real_vx_indx == -1 ) {
		/*RWX_VertexMap *vx_map = new RWX_VertexMap(vx_indx);
		vx_map->color_set = true;
		vx_map->col_indx = col_indx;*/
		RWX_VertexMap vx_map(vx_indx);
		vx_map.color_set = true;
		vx_map.col_indx = col_indx;
		vertex_maps->push_back(vx_map);
		real_vx_indx = vertex_maps->size()-1;
		/*const Vertex3D *verts = obj->getVertices(0);
		const Vertex3D *vx = &verts[vx_indx]; // assumes that the first set of vertices are mapped 1-1*/
		//Vertex3D *n_vx = vx->copy();
		//Vertex3D n_vx = obj->getVertex(0, vx_indx); // assumes that the first set of vertices are mapped 1-1*/
		Vector3D n_vx = obj->getVertexPos(vx_indx); // assumes that the first set of vertices are mapped 1-1*/
		//n_vx->color.set(&color);
		//TextureUV *uv = uv_entries->at(vx_indx);
		TextureUV *uv = &uv_entries->at(vx_indx);
		//TextureUV *n_uv = new TextureUV(uv->u,uv->v);
		TextureUV n_uv(uv->u,uv->v);
		uv_entries->push_back(n_uv);
		size_t n_vx_indx = obj->addVertex(0, n_vx.x, n_vx.y, n_vx.z);
		//obj->setVertexColor(obj->get_n_vertices()-1, color.getR(), color.getG(), color.getB());
		obj->setVertexColori(n_vx_indx, color.getR(), color.getG(), color.getB());
		/*Color *cols = obj->getColors();
		cols[obj->get_n_vertices()-1].set(&color);*/
		//obj->setColor(obj->get_n_vertices()-1, &color);
	}
	/*if( real_vx_indx == -1 ) {
	Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_RWX_MDL_ERROR,"can't find real vertex index"));
	}*/
	return real_vx_indx;
}

VI_VObject *Loader::importRWX(const char * filename) {
	LOG("Importing RenderWare File: %s\n",filename);
	const bool allowcolortex = true;

	FILE *file = fopen(filename,"rb");
	if( file == NULL ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open RWX file"));
		return NULL;
	}
	/*FILE *file = NULL;
	if( fopen_s(&file, filename, "rb") != 0 ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open RWX file"));
		return NULL;
	}*/

	VI_VObject *obj[MAX_LEVELS];
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		string folder = getFolder(filename);

		VI_VObject *proto[MAX_PROTO];
		int cproto = 0;
		Matrix4d * mat4d[MAX_LEVELS];
		Matrix4d * jmat4d[MAX_LEVELS];
		//vector<RWX_VertexMap *> *vertex_maps = new vector<RWX_VertexMap *>();
		//vector<RWX_VertexMap *> vertex_maps;
		vector<RWX_VertexMap> vertex_maps;
		int vx_index = 0;
		int col_index = 0;
		//vector<TextureUV *> uv_entries;
		vector<TextureUV> uv_entries;
		for(int i=0;i<MAX_LEVELS;i++) {
			obj[i]=NULL;
			mat4d[i]=NULL;
			jmat4d[i]=NULL;
		}
		//vector<NamedResource *> *resources = new vector<NamedResource *>();
		//vector<NamedResource *> resources;
		vector<NamedResource> resources;
		int cobj=-1;
		int cmat4d=-1;
		int cjmat4d=-1;

		mat4d[++cmat4d] = Matrix4d::createIdentity();
		jmat4d[++cjmat4d] = Matrix4d::createIdentity();

		//rgb_struct c_rgb = {255,255,255};
		//Color c_col(1.0f,1.0f,1.0f);
		Color c_col;
		c_col.seti(255,255,255);
		VI_Texture *c_texture = NULL;
		char buffer[1024];
		char *str;
		//char *words[MAX_WORDS];
		//vector<string> words;
		for(;;) {
			if(fgetLine(file,buffer))
				break;
			/*if( strstr(buffer, "11597") != NULL )
			printf("!\n");*/
			//printf("->:%s\n",buffer);
			str = stripWhiteSpace(buffer);
			//printf("->:%s\n",str);
			/*int n_words = getWords(str,words);
			if(n_words == 0)
				continue;*/
			vector<string> words = getWords(str);
			if( words.size() == 0 )
				continue;
			//if(*words[0]=='#')
			if( words[0].at(0) =='#' )
			{
			}
			//else if(_stricmp(words[0],"ModelBegin")==0)
			else if(stricmp(words[0].c_str(),"ModelBegin")==0)
			{
				if(cobj!=-1)
					LOG("Error - ModelBegin must be first statement!\n");
				else {
					obj[++cobj] = VI_VObject::create("ModelBegin RWX object");
					vx_index = 0;
					col_index = 0;
					/*for(size_t i=0;i<uv_entries.size();i++) {
						delete uv_entries.at(i);
					}*/
					uv_entries.clear();
					/*for(size_t i=0;i<vertex_maps.size();i++) {
						delete vertex_maps.at(i);
					}*/
					vertex_maps.clear();
				}
			}
			//else if(_stricmp(words[0],"ModelEnd")==0)
			else if(stricmp(words[0].c_str(),"ModelEnd")==0)
			{
				if(cobj!=0)
					LOG("Error - ModelEnd must be last statement!\n");
				else {
					cobj--;
				}
			}
			//else if(_stricmp(words[0],"ClumpBegin")==0)
			else if(stricmp(words[0].c_str(),"ClumpBegin")==0)
			{
				obj[++cobj] = VI_VObject::create("ClumpBegin RWX Object");
				vx_index = 0;
				col_index = 0;
				/*for(size_t i=0;i<uv_entries.size();i++) {
					delete uv_entries.at(i);
				}*/
				uv_entries.clear();
				/*for(size_t i=0;i<vertex_maps.size();i++) {
					delete vertex_maps.at(i);
				}*/
				vertex_maps.clear();
				mat4d[++cmat4d] = Matrix4d::createIdentity();
				jmat4d[++cjmat4d] = Matrix4d::createIdentity();
				//printf("Now on: %d\n",cobj);
			}
			//else if(_stricmp(words[0],"ClumpEnd")==0)
			else if(stricmp(words[0].c_str(),"ClumpEnd")==0)
			{
				if(cobj<1)
					LOG("No ModelBegin and/or ClumpBegin statement!\n");
				else {
					// joint
					delete jmat4d[cjmat4d];
					jmat4d[cjmat4d--]=NULL;
					obj[cobj]->transform4d(*jmat4d[cjmat4d]);
					// ctm
					delete mat4d[cmat4d];
					mat4d[cmat4d--]=NULL;
					obj[cobj]->transform4d(*mat4d[cmat4d]);
					obj[cobj-1]->combineObject(obj[cobj]);
					delete obj[cobj];
					obj[cobj--]=NULL;
					//printf("Now on: %d\n",cobj);
				}
			}
			//else if(_stricmp(words[0],"ProtoBegin")==0)
			else if(stricmp(words[0].c_str(),"ProtoBegin")==0)
			{
				obj[++cobj] = VI_VObject::create(words[1].c_str());
				vx_index = 0;
				col_index = 0;
				/*for(size_t i=0;i<uv_entries.size();i++) {
					delete uv_entries.at(i);
				}*/
				uv_entries.clear();
				/*for(size_t i=0;i<vertex_maps.size();i++) {
					delete vertex_maps.at(i);
				}*/
				vertex_maps.clear();
				//strcpy(obj[cobj]->name,words[1]);
				//obj[cobj]->setName(words[1]);
				mat4d[++cmat4d] = Matrix4d::createIdentity(); // ?
				jmat4d[++cjmat4d] = Matrix4d::createIdentity(); // ?
				//printf("Proto now on: %d\n",cobj);
			}
			//else if(_stricmp(words[0],"ProtoEnd")==0)
			else if(stricmp(words[0].c_str(),"ProtoEnd")==0)
			{
				if(cobj<0)
					LOG("No ProtoBegin statement!\n");
				else {
					proto[cproto++] = obj[cobj];
					obj[cobj--]=NULL;
					delete jmat4d[cjmat4d]; // ?
					jmat4d[cjmat4d--]=NULL; // ?
					delete mat4d[cmat4d]; // ?
					mat4d[cmat4d--]=NULL; // ?
					//printf("Proto Now on: %d\n",cproto);
				}
			}
			//else if(_stricmp(words[0],"TransformBegin")==0)
			else if(stricmp(words[0].c_str(),"TransformBegin")==0)
			{
				mat4d[++cmat4d] = Matrix4d::createIdentity();
				//printf("*** %d : %d\n",cmat4d,mat4d[cmat4d]);
			}
			//else if(_stricmp(words[0],"TransformEnd")==0)
			else if(stricmp(words[0].c_str(),"TransformEnd")==0)
			{
				if(cmat4d<1)
					LOG("No TransformBegin statement!\n");
				else {
					delete mat4d[cmat4d];
					mat4d[cmat4d--]=NULL;
				}
			}
			//else if(_stricmp(words[0],"Transform")==0)
			else if(stricmp(words[0].c_str(),"Transform")==0)
			{
				int w = 1;
				for(int i=0;i<4;i++) { // row
					for(int j=0;j<4;j++) { // column
						float n;
						sscanf(words[w++].c_str(),"%f",&n);
						//nm->m[i][j] = n;
						//mat4d[cmat4d]->m[i][j] = n;
						mat4d[cmat4d]->setElement(j,i,n);
					}
				}
				//printf("*** %d : %d\n",cmat4d,mat4d[cmat4d]);
			}
			//else if(_stricmp(words[0],"Identity")==0)
			else if(stricmp(words[0].c_str(),"Identity")==0)
			{
				mat4d[cmat4d]->setIdentity();
			}
			//else if(_stricmp(words[0],"Scale")==0)
			else if(stricmp(words[0].c_str(),"Scale")==0)
			{
				float x,y,z;
				sscanf(words[1].c_str(),"%f",&x);
				sscanf(words[2].c_str(),"%f",&y);
				sscanf(words[3].c_str(),"%f",&z);
				Matrix4d * trans = Matrix4d::createScale(x,y,z);
				Matrix4d *out = mat4d[cmat4d]->multiply(*trans);
				//Matrix4d *out = trans->multiply(mat4d[cmat4d]);
				delete mat4d[cmat4d];
				delete trans;
				mat4d[cmat4d] = out;
			}
			//else if(_stricmp(words[0],"Rotate")==0)
			else if(stricmp(words[0].c_str(),"Rotate")==0)
			{
				float x,y,z,a;
				sscanf(words[1].c_str(),"%f",&x);
				sscanf(words[2].c_str(),"%f",&y);
				sscanf(words[3].c_str(),"%f",&z);
				sscanf(words[4].c_str(),"%f",&a);
				Matrix4d * trans = Matrix4d::createAngleAxisRotation(x,y,z,a);
				Matrix4d *out = mat4d[cmat4d]->multiply(*trans);
				//Matrix4d *out = trans->multiply(mat4d[cmat4d]);
				delete mat4d[cmat4d];
				delete trans;
				mat4d[cmat4d] = out;
			}
			//else if(_stricmp(words[0],"Rotate")==0)
			else if(stricmp(words[0].c_str(),"Rotate")==0)
			{
				float x,y,z;
				sscanf(words[1].c_str(),"%f",&x);
				sscanf(words[2].c_str(),"%f",&y);
				sscanf(words[3].c_str(),"%f",&z);
				Matrix4d *trans = Matrix4d::createTranslation(x,y,z);
				Matrix4d *out = mat4d[cmat4d]->multiply(*trans);
				//Matrix4d *out = trans->multiply(mat4d[cmat4d]);
				delete mat4d[cmat4d];
				delete trans;
				mat4d[cmat4d] = out;
			}
			//else if(_stricmp(words[0],"JointTransformBegin")==0)
			else if(stricmp(words[0].c_str(),"JointTransformBegin")==0)
			{
				//jmat4d[++cjmat4d] = Matrix4d::createIdentity();
				jmat4d[++cjmat4d] = new Matrix4d();
				*jmat4d[cjmat4d] = *jmat4d[cjmat4d-1];
			}
			//else if(_stricmp(words[0],"JointTransformEnd")==0)
			else if(stricmp(words[0].c_str(),"JointTransformEnd")==0)
			{
				if(cjmat4d<1)
					LOG("No JointTransformBegin statement!\n");
				else {
					delete jmat4d[cjmat4d];
					jmat4d[cjmat4d--]=NULL;
				}
			}
			//else if(_stricmp(words[0],"TransformJoint")==0)
			else if(stricmp(words[0].c_str(),"TransformJoint")==0)
			{
				int w = 1;
				for(int i=0;i<4;i++) { // row
					for(int j=0;j<4;j++) { // column
						float n;
						sscanf(words[w++].c_str(),"%f",&n);
						//jmat4d[cjmat4d]->m[i][j] = n;
						jmat4d[cmat4d]->setElement(j,i,n);
					}
				}
			}
			//else if(_stricmp(words[0],"IdentityJoint")==0)
			else if(stricmp(words[0].c_str(),"IdentityJoint")==0)
			{
				jmat4d[cjmat4d]->setIdentity();
			}
			//else if(_stricmp(words[0],"RotateJoint")==0)
			else if(stricmp(words[0].c_str(),"RotateJoint")==0)
			{
				float x,y,z,a;
				sscanf(words[1].c_str(),"%f",&x);
				sscanf(words[2].c_str(),"%f",&y);
				sscanf(words[3].c_str(),"%f",&z);
				sscanf(words[4].c_str(),"%f",&a);
				Matrix4d * trans = Matrix4d::createAngleAxisRotation(x,y,z,a);
				Matrix4d *out = jmat4d[cjmat4d]->multiply(*trans);
				delete jmat4d[cjmat4d];
				delete trans;
				jmat4d[cjmat4d] = out;
			}
			//else if(_stricmp(words[0],"Color")==0)
			else if(stricmp(words[0].c_str(),"Color")==0)
			{
				float r,g,b;
				sscanf(words[1].c_str(),"%f",&r);
				sscanf(words[2].c_str(),"%f",&g);
				sscanf(words[3].c_str(),"%f",&b);
				/*c_col.R = r;
				c_col.G = g;
				c_col.B = b;*/
				c_col.setf(r,g,b);
				//printf("Found Color!: %d,%d,%d\n",c_rgb.r,c_rgb.g,c_rgb.b);
				col_index++;
			}
			//else if(_stricmp(words[0],"Texture")==0)
			else if(stricmp(words[0].c_str(),"Texture")==0)
			{
				//if(_stricmp(words[1],"NULL")==0) {
				if(stricmp(words[1].c_str(),"NULL")==0) {
					c_texture = NULL;
				}
				else {
					char buffer[1024] = "";
					c_texture = NULL;
					for(size_t i=0;i<resources.size() && c_texture == NULL;i++) {
						const NamedResource *resource = &resources.at(i);
						if( resource->equals( words[1].c_str(), V_CLASS_TEXTURE ) ) {
							c_texture = (VI_Texture *)resource->ptr;
						}
					}

					if( c_texture == NULL ) {
						sprintf(buffer,"%s%s",folder.c_str(),words[1].c_str());
						//c_texture = Texture::loadTexture(buffer);
						try {
							c_texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
						}
						catch(VisionException *ve) {
							if( ve->type == VisionException::V_FILE_ERROR ) {
								if(c_texture == NULL) {
									LOG("WARNING - cannot load %s\n",buffer);
								}
								delete ve;
							}
							else {
								throw ve;
							}
						}

						if( c_texture == NULL ) {
							sprintf(buffer,"%s%s.jpg",folder.c_str(),words[1].c_str());
							c_texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
							/*try {
								c_texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
							}
							catch(VisionException *ve) {
								if( ve->type == VisionException::V_FILE_ERROR ) {
									if(c_texture == NULL) {
										LOG("WARNING - cannot load %s\n",buffer);
									}
									delete ve;
								}
								else {
									throw ve;
								}
							}*/
						}
						//NamedResource *resource = new NamedResource(words[1], V_CLASS_TEXTURE, c_texture);
						NamedResource resource(words[1].c_str(), V_CLASS_TEXTURE, c_texture);
						resources.push_back(resource);
					}
				}
			}
			//else if(_stricmp(words[0],"Vertex")==0 || _stricmp(words[0],"VertexExt")==0)
			else if(stricmp(words[0].c_str(),"Vertex")==0 || stricmp(words[0].c_str(),"VertexExt")==0)
			{
				float x,y,z;
				sscanf(words[1].c_str(),"%f",&x);
				sscanf(words[2].c_str(),"%f",&y);
				sscanf(words[3].c_str(),"%f",&z);
				//Vertex3D v(x, y, z);
				Vector3D v(x, y, z);
				//printf("Found Vertex!: %f,%f,%f\n",x,y,z);
				//printf("  : %d,%d,%d\n",v->x,v->y,v->z);
				//printf("*** %d : %d\n",cmat4d,mat4d[cmat4d]);
				mat4d[cmat4d]->transformVector(&v);
				//printf("  : %d,%d,%d\n",v->x,v->y,v->z);
				//TextureUV *uv = NULL;
				TextureUV uv(-1.0f, -1.0f);
				//if(_stricmp(words[0],"VertexExt")==0) {
				if(stricmp(words[0].c_str(),"VertexExt")==0) {
					int w = -1;
					//if(_stricmp(words[4],"UV")==0)
					if(stricmp(words[4].c_str(),"UV")==0)
						w=4;
					//else if(_stricmp(words[8],"UV")==0)
					else if(stricmp(words[8].c_str(),"UV")==0)
						w=8;
					if(w != -1) {
						float tx_u = -1,tx_v = -1;
						//sscanf(words[w+1],"%f",&v->u);
						//sscanf(words[w+2],"%f",&v->v);
						sscanf(words[w+1].c_str(),"%f",&tx_u);
						sscanf(words[w+2].c_str(),"%f",&tx_v);
						//uv = new TextureUV(tx_u,tx_v);
						uv.set(tx_u, tx_v);
					}
				}
				/*if( uv == NULL )
					uv = new TextureUV(-1,-1);*/
				uv_entries.push_back(uv);
				//RWX_VertexMap *vx_map = new RWX_VertexMap(vx_index++);
				RWX_VertexMap vx_map(vx_index++);
				vertex_maps.push_back(vx_map);
				obj[cobj]->addVertex(0, v.x, v.y, v.z);
			}
			//else if(_stricmp(words[0],"Quad")==0 || _stricmp(words[0],"QuadExt")==0)
			else if(stricmp(words[0].c_str(),"Quad")==0 || stricmp(words[0].c_str(),"QuadExt")==0)
			{
				int v1,v2,v3,v4;
				sscanf(words[1].c_str(),"%d",&v1);
				sscanf(words[2].c_str(),"%d",&v2);
				sscanf(words[3].c_str(),"%d",&v3);
				sscanf(words[4].c_str(),"%d",&v4);
				// must -1 from all vertices!
				v1--; v2--; v3--; v4--;
				int r_v1 = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v1, col_index, c_col);
				int r_v2 = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v2, col_index, c_col);
				int r_v3 = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v3, col_index, c_col);
				int r_v4 = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v4, col_index, c_col);
				VI_Face *face = obj[cobj]->addPolygon(r_v1,r_v2,r_v3,r_v4, "");
				if( c_texture != NULL ) {
					//obj[cobj]->setPolygonTextureWrap(face, c_texture, &*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//obj[cobj]->setPolygonTextureWrap(face, c_texture, &uv_entries[0]);
					obj[cobj]->setPolygonTextureWrap(face, c_texture, &uv_entries_ptrs[0]);
				}
			}
			//else if(_stricmp(words[0],"Triangle")==0 || _stricmp(words[0],"TriangleExt")==0)
			else if(stricmp(words[0].c_str(),"Triangle")==0 || stricmp(words[0].c_str(),"TriangleExt")==0)
			{
				int v1,v2,v3;
				sscanf(words[1].c_str(),"%d",&v1);
				sscanf(words[2].c_str(),"%d",&v2);
				sscanf(words[3].c_str(),"%d",&v3);
				// must -1 from all vertices!
				v1--; v2--; v3--;
				int r_v1 = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v1, col_index, c_col);
				int r_v2 = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v2, col_index, c_col);
				int r_v3 = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v3, col_index, c_col);
				VI_Face *face = obj[cobj]->addPolygon(r_v1,r_v2,r_v3, "");
				if( c_texture != NULL ) {
					//obj[cobj]->setPolygonTextureWrap(face, c_texture, &*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//obj[cobj]->setPolygonTextureWrap(face, c_texture, &uv_entries[0]);
					obj[cobj]->setPolygonTextureWrap(face, c_texture, &uv_entries_ptrs[0]);
				}
			}
			//else if(_stricmp(words[0],"Polygon")==0 || _stricmp(words[0],"PolygonExt")==0)
			else if(stricmp(words[0].c_str(),"Polygon")==0 || stricmp(words[0].c_str(),"PolygonExt")==0)
			{
				int nv=0,v=0;
				sscanf(words[1].c_str(),"%d",&nv);
				//size_t *vlist = new size_t[nv];
				vector<size_t> vlist;
				for(int i=0;i<nv;i++) {
					sscanf(words[i+2].c_str(),"%d",&v);
					v--;
					int r_v = find_real_vertex_index(obj[cobj], &vertex_maps, &uv_entries, v, col_index, c_col);
					//vlist[i] = r_v;
					vlist.push_back(r_v);
				}
				VI_Face *face = obj[cobj]->addPolygon(&vlist[0], nv, "");
				//delete [] vlist;
				if( c_texture != NULL ) {
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//obj[cobj]->setPolygonTextureWrap(face, c_texture, &*uv_entries.begin());
					//obj[cobj]->setPolygonTextureWrap(face, c_texture, &uv_entries[0]);
					obj[cobj]->setPolygonTextureWrap(face, c_texture, &uv_entries_ptrs[0]);
				}
			}
			//else if(_stricmp(words[0],"Cone")==0)
			else if(stricmp(words[0].c_str(),"Cone")==0)
			{
				float h,r;
				int n;
				sscanf(words[1].c_str(),"%f",&h);
				sscanf(words[2].c_str(),"%f",&r);
				sscanf(words[3].c_str(),"%d",&n);
				VI_VObject * nobj = VI_Modeller::createCone(h,r,n);
				nobj->transform4d(*mat4d[cmat4d]);
				//Object3D *d_nobj = dynamic_cast<Object3D *>(nobj); // cast to the internal representation
				if(c_texture!=NULL) {
					//nobj->setTextureWrap(c_texture,&*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//nobj->setTextureWrap(c_texture,&uv_entries[0]);
					nobj->setTextureWrap(c_texture,&uv_entries_ptrs[0]);
				}
				if(c_texture == NULL || allowcolortex)
					nobj->setBaseColori(c_col.getR(), c_col.getG(), c_col.getB());
				obj[cobj]->combineObject(nobj);
				delete nobj;
			}
			//else if(_stricmp(words[0],"Disc")==0)
			else if(stricmp(words[0].c_str(),"Disc")==0)
			{
				float h,r;
				int n;
				sscanf(words[1].c_str(),"%f",&h);
				sscanf(words[2].c_str(),"%f",&r);
				sscanf(words[3].c_str(),"%d",&n);
				VI_VObject * nobj = VI_Modeller::createDisc(h,r,n,true);
				nobj->transform4d(*mat4d[cmat4d]);
				//Object3D *d_nobj = dynamic_cast<Object3D *>(nobj); // cast to the internal representation
				if(c_texture!=NULL) {
					//nobj->setTextureWrap(c_texture,&*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//nobj->setTextureWrap(c_texture,&uv_entries[0]);
					nobj->setTextureWrap(c_texture,&uv_entries_ptrs[0]);
				}
				if(c_texture == NULL || allowcolortex)
					nobj->setBaseColori(c_col.getR(), c_col.getG(), c_col.getB());
				obj[cobj]->combineObject(nobj);
				delete nobj;
			}
			//else if(_stricmp(words[0],"Sphere")==0)
			else if(stricmp(words[0].c_str(),"Sphere")==0)
			{
				float r;
				int d;
				sscanf(words[1].c_str(),"%f",&r);
				sscanf(words[2].c_str(),"%d",&d);
				VI_VObject * nobj = VI_Modeller::createSphere(r,d);
				nobj->transform4d(*mat4d[cmat4d]);
				//Object3D *d_nobj = dynamic_cast<Object3D *>(nobj); // cast to the internal representation
				if(c_texture!=NULL) {
					//nobj->setTextureWrap(c_texture,&*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//nobj->setTextureWrap(c_texture,&uv_entries[0]);
					nobj->setTextureWrap(c_texture,&uv_entries_ptrs[0]);
				}
				if(c_texture == NULL || allowcolortex)
					nobj->setBaseColori(c_col.getR(), c_col.getGf(), c_col.getB());
				obj[cobj]->combineObject(nobj);
				delete nobj;
			}
			//else if(_stricmp(words[0],"Hemisphere")==0)
			else if(stricmp(words[0].c_str(),"Hemisphere")==0)
			{
				float r;
				int d;
				sscanf(words[1].c_str(),"%f",&r);
				sscanf(words[2].c_str(),"%d",&d);
				VI_VObject * nobj = VI_Modeller::createHemisphere(r,d);
				nobj->transform4d(*mat4d[cmat4d]);
				//Object3D *d_nobj = dynamic_cast<Object3D *>(nobj); // cast to the internal representation
				if(c_texture!=NULL) {
					//nobj->setTextureWrap(c_texture,&*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//nobj->setTextureWrap(c_texture,&uv_entries[0]);
					nobj->setTextureWrap(c_texture,&uv_entries_ptrs[0]);
				}
				if(c_texture == NULL || allowcolortex)
					nobj->setBaseColori(c_col.getR(), c_col.getG(), c_col.getB());
				obj[cobj]->combineObject(nobj);
				delete nobj;
			}
			//else if(_stricmp(words[0],"Cylinder")==0)
			else if(stricmp(words[0].c_str(),"Cylinder")==0)
			{
				float h,r1,r2;
				int n;
				sscanf(words[1].c_str(),"%f",&h);
				sscanf(words[2].c_str(),"%f",&r1);
				sscanf(words[3].c_str(),"%f",&r2);
				sscanf(words[4].c_str(),"%d",&n);
				VI_VObject * nobj = VI_Modeller::createCylinder(h,r1,r2,n);
				nobj->transform4d(*mat4d[cmat4d]);
				//Object3D *d_nobj = dynamic_cast<Object3D *>(nobj); // cast to the internal representation
				if(c_texture!=NULL) {
					//nobj->setTextureWrap(c_texture,&*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//nobj->setTextureWrap(c_texture,&uv_entries[0]);
					nobj->setTextureWrap(c_texture,&uv_entries_ptrs[0]);
				}
				if(c_texture == NULL || allowcolortex)
					nobj->setBaseColori(c_col.getR(), c_col.getG(), c_col.getB());
				obj[cobj]->combineObject(nobj);
				delete nobj;
			}
			//else if(_stricmp(words[0],"Block")==0)
			else if(stricmp(words[0].c_str(),"Block")==0)
			{
				float w,h,d;
				sscanf(words[1].c_str(),"%f",&w);
				sscanf(words[2].c_str(),"%f",&h);
				sscanf(words[3].c_str(),"%f",&d);
				VI_VObject * nobj = VI_Modeller::createCuboid(w,h,d);
				nobj->transform4d(*mat4d[cmat4d]);
				//Object3D *d_nobj = dynamic_cast<Object3D *>(nobj); // cast to the internal representation
				if(c_texture!=NULL) {
					//nobj->setTextureWrap(c_texture,&*uv_entries.begin());
					vector<const TextureUV *> uv_entries_ptrs;
					for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
						uv_entries_ptrs.push_back(&*iter);
					}
					//nobj->setTextureWrap(c_texture,&uv_entries[0]);
					nobj->setTextureWrap(c_texture,&uv_entries_ptrs[0]);
				}
				if(c_texture == NULL || allowcolortex)
					nobj->setBaseColori(c_col.getR(), c_col.getG(), c_col.getB());
				obj[cobj]->combineObject(nobj);
				delete nobj;
			}
			//else if(_stricmp(words[0],"ProtoInstance")==0)
			else if(stricmp(words[0].c_str(),"ProtoInstance")==0)
			{
				for(int i=0;i<cproto;i++) {
					VisionObject *d_proto = dynamic_cast<VisionObject *>(proto[i]); // cast to the internal representation
					//if(_stricmp(d_proto->getName(),words[1])==0) {
					if(stricmp(d_proto->getName(),words[1].c_str())==0) {
						VI_VObject *nobj = proto[i]->clone();
						nobj->transform4d(*mat4d[cmat4d]);
						//nobj->setBaseColour(c_rgb);
						//delete proto[i];
						//printf("");
						obj[cobj]->combineObject(nobj);
						break;
					}
				}
			}
			//else if(_stricmp(words[0],"ProtoInstanceGeometry")==0)
			else if(stricmp(words[0].c_str(),"ProtoInstanceGeometry")==0)
			{
				for(int i=0;i<cproto;i++) {
					VisionObject *d_proto = dynamic_cast<VisionObject *>(proto[i]); // cast to the internal representation
					//if(_stricmp(d_proto->getName(),words[1])==0) {
					if(stricmp(d_proto->getName(),words[1].c_str())==0) {
						VI_VObject *nobj = proto[i]->clone();
						nobj->transform4d(*mat4d[cmat4d]);
						//Object3D *d_nobj = dynamic_cast<Object3D *>(nobj); // cast to the internal representation
						//nobj->setTexture(c_texture);
						if(c_texture!=NULL) {
							//nobj->setTextureWrap(c_texture,&*uv_entries.begin());
							vector<const TextureUV *> uv_entries_ptrs;
							for(vector<TextureUV>::const_iterator iter = uv_entries.begin(); iter != uv_entries.end(); ++iter) {
								uv_entries_ptrs.push_back(&*iter);
							}
							//nobj->setTextureWrap(c_texture,&uv_entries[0]);
							nobj->setTextureWrap(c_texture,&uv_entries_ptrs[0]);
						}
						if(c_texture == NULL || allowcolortex)
							nobj->setBaseColori(c_col.getR(), c_col.getG(), c_col.getB());
						else {
							nobj->setBaseColori(255, 255, 255);
						}
						obj[cobj]->combineObject(nobj);
						break;
					}
				}
			}
			else {
				LOG("Unknown Command : %s\n",words[0].c_str());
			}
			/*for(int i=0;i<n_words;i++) {
				delete words[i];
				words[i]=NULL;
			}*/
		}
		fclose(file);
		for(int i=0;i<cproto;i++) {
			delete proto[i];
		}
		delete mat4d[cmat4d];
		delete jmat4d[cjmat4d];
		/*for(size_t i=0;i<uv_entries.size();i++) {
			delete uv_entries.at(i);
		}*/
		//delete uv_entries;
		/*for(size_t i=0;i<vertex_maps.size();i++) {
			delete vertex_maps.at(i);
		}*/
		//delete vertex_maps;
		/*for(size_t i=0;i<resources.size();i++) {
			delete resources.at(i);
		}*/
		//delete resources;
		LOG("    %d vertices\n",obj[0]->get_n_vertices());
		LOG("    %d polygons\n",obj[0]->get_n_polys());
		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}
	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(obj[0]);
		vo->prepare();
	}

	if( !obj[0]->check() ) {
		delete obj[0];
		Vision::setError(new VisionException(obj[0], VisionException::V_IMPORT_RWX_MDL_ERROR, "Imported RWX model is corrupt"));
	}
	return obj[0];
}

class NWNPoly {
public:
	int v[3];
	int t[3];

	NWNPoly() {
		for(int i=0;i<3;i++) {
			v[i] = 0;
			t[i] = 0;
		}
	}
};

/* Import 3D object in NeverWinter Nights MDL format.
*/
VI_SceneGraphNode *Loader::importNodeNWN(int *n_objs,VI_Object ***objs,const char *filename,bool want_textures) {
	LOG("Importing NeverWinter Nights MDL File: %s\n",filename);
	VI_SceneGraphNode *root = NULL;
	if( n_objs != NULL )
		*n_objs = 0;
	if( objs != NULL )
		*objs = NULL;

	FILE *file = fopen(filename,"rb");
	if( file == NULL ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open NWN MDL file"));
		return NULL;
	}
	/*FILE *file = NULL;
	if( fopen_s(&file, filename, "rb") != 0 ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open NWN MDL file"));
		return NULL;
	}*/

	//vector<VI_Object *> *objs_v = new vector<VI_Object *>();
	vector<VI_Object *> objs_v;

	VI_SceneGraphNode *c_node = NULL;
	//Object3D *c_obj = NULL;
	VI_Object *c_obj = NULL;
	vector<NWNPoly> auto_polys;
	NWNPoly *polys = NULL;
	int n_polys = 0;
	vector<Vector3D> auto_tverts;
	Vector3D *tverts = NULL;
	VI_Texture *c_texture = NULL;
	//char c_animname[NAMELEN+1] = "";
	string c_animname;
	AnimationInfo *c_animinfo = NULL;
	float c_length = 0.0;

	//bool ok = true;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		string folder = getFolder(filename);

		char buffer[1024];
		char *str = NULL;
		//char *words[MAX_WORDS];
		//int geom_count = 0;
		enum Mode {
			MODE_NONE = 0,
			MODE_GEOM = 1,
			MODE_ANIM = 2,
			MODE_POSITIONKEYLIST = 3,
			MODE_ROTATIONKEYLIST = 4
		};
		Mode mode = MODE_NONE;
		bool done_geom = false;
		float animationscale = 1.0;

		//while(ok) {
		for(;;) {
			if(fgetLine(file,buffer))
				break;
			str = stripWhiteSpace(buffer);
			/*int n_words = getWords(str,words);
			if(n_words == 0)
				continue;*/
			vector<string> words = getWords(str);
			int n_words = words.size();
			if( n_words == 0 )
				continue;
			//if(*words[0]=='#') {
			if( words[0].at(0) =='#' ) {
				// comment
			}
			/*else if(_stricmp(words[0],"setanimationscale")==0) {
			if( mode != MODE_NONE || n_words < 2 ) {
			LOG("ERROR - setanimationscale\n");
			ok = false;
			}
			animationscale = atof(words[1]);
			}*/
			//else if(_stricmp(words[0],"beginmodelgeom")==0) {
			else if(stricmp(words[0].c_str(),"beginmodelgeom")==0) {
				/*if( geom_count != 0 ) {
				LOG("ERROR - beginmodelgeom\n");
				ok = false;
				}
				geom_count = 1;*/
				if( mode != MODE_NONE || done_geom ) {
					LOG("ERROR - beginmodelgeom\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				mode = MODE_GEOM;
			}
			//else if(_stricmp(words[0],"endmodelgeom")==0) {
			else if(stricmp(words[0].c_str(),"endmodelgeom")==0) {
				/*if( geom_count != 1 ) {
				LOG("ERROR - endmodelgeom\n");
				ok = false;
				}
				geom_count = 2;*/
				if( mode != MODE_GEOM ) {
					LOG("ERROR - endmodelgeom\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				mode = MODE_NONE;
				done_geom = true;
			}
			//else if(_stricmp(words[0],"newanim")==0) {
			else if(stricmp(words[0].c_str(),"newanim")==0) {
				if( mode != MODE_NONE || n_words < 3 ) {
					LOG("ERROR - newanim\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				mode = MODE_ANIM;
				/*strncpy(c_animname, words[1], NAMELEN);
				c_animname[NAMELEN] = '\0';*/
				c_animname = words[1];
			}
			//else if(_stricmp(words[0],"doneanim")==0) {
			else if(stricmp(words[0].c_str(),"doneanim")==0) {
				if( mode != MODE_ANIM ) {
					LOG("ERROR - doneanim\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				mode = MODE_NONE;
				//*c_animname = '\0';
				c_animname.clear();
			}
			/*else if( geom_count != 1 ) {
			}*/
			else if( mode == MODE_NONE ) {
			}
			//else if(_stricmp(words[0],"endlist")==0) {
			else if(stricmp(words[0].c_str(),"endlist")==0) {
				if( mode == MODE_POSITIONKEYLIST || mode == MODE_ROTATIONKEYLIST ) {
					mode = MODE_ANIM;
				}
				else {
					/*LOG("ERROR - endlist\n");
					ok = false;*/
				}
			}
			//else if(_stricmp(words[0],"positionkey")==0) {
			else if(stricmp(words[0].c_str(),"positionkey")==0) {
				if( c_animinfo == NULL ) {
				}
				/*else if( mode != MODE_ANIM ) {
				LOG("ERROR - positionkey\n");
				ok = false;
				}*/
				else {
					mode = MODE_POSITIONKEYLIST;
				}
			}
			//else if(_stricmp(words[0],"orientationkey")==0) {
			else if(stricmp(words[0].c_str(),"orientationkey")==0) {
				if( c_animinfo == NULL ) {
				}
				/*else if( mode != MODE_ANIM ) {
				LOG("ERROR - orientationkey\n");
				ok = false;
				}*/
				else {
					mode = MODE_ROTATIONKEYLIST;
				}
			}
			//else if(_stricmp(words[0],"node")==0) {
			else if(stricmp(words[0].c_str(),"node")==0) {
				if( c_node != NULL || n_words < 3 ) {
					LOG("ERROR - node\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( mode == MODE_GEOM ){
					/*if( _stricmp(words[1],"dummy") == 0
						|| _stricmp(words[1],"patch") == 0
						) {*/
					if( stricmp(words[1].c_str(),"dummy") == 0
						|| stricmp(words[1].c_str(),"patch") == 0
						) {
							// dummy node
							c_node = new SceneGraphNode();
							//c_node = new Entity();
							c_obj = NULL;
					}
					/*else if( _stricmp(words[1],"trimesh") == 0
						|| _stricmp(words[1],"danglymesh") == 0
						) {*/
					else if( stricmp(words[1].c_str(),"trimesh") == 0
						|| stricmp(words[1].c_str(),"danglymesh") == 0
						) {
							c_obj = VI_VObject::create(words[1].c_str());
							//c_node = VI_createEntity(c_obj);
							c_node = VI_createSceneGraphNode(c_obj);
							objs_v.push_back(c_obj);
							//vision_garbage_collector.add(c_obj);
					}
					//else if( _stricmp(words[1],"light") == 0 ) {
					else if( stricmp(words[1].c_str(),"light") == 0 ) {
						c_node = new SceneGraphNode();
						c_obj = NULL;
						// not implemented!
					}
					//else if( _stricmp(words[1],"emitter") == 0 ) {
					else if( stricmp(words[1].c_str(),"emitter") == 0 ) {
						c_obj = VI_GeneralParticlesystem::create(1000);
						//c_node = new Entity(c_obj);
						c_node = VI_createSceneGraphNode(c_obj);
						objs_v.push_back(c_obj);
						//vision_garbage_collector.add(c_obj);
					}
					else {
						LOG("ERROR - unknown node type\n");
						Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
					}
					// init node
					//if( ok )
					{
						c_node->setName(words[2].c_str());
						c_node->setSourcetype(V_SOURCETYPE_NWN);
					}
				}
				else if( mode == MODE_ANIM ) {
					c_node = root->findChildNode(words[2].c_str());
					if( c_node == NULL ) {
						LOG("WARNING - can't find node\n");
						//ok = false;
					}
					/*else if( c_node->animationinfos == NULL ) {
						c_node->animationinfos = new vector<AnimationInfo *>();
					}*/
					/*else {
						SceneGraphNode *d_c_node = dynamic_cast<SceneGraphNode *>(c_node); // cast to the internal representation
						d_c_node->initAnimationInfos();
					}*/
					c_obj = NULL;
					if( c_node != NULL ) {
						c_animinfo = new AnimationInfo();
						c_animinfo->setName(c_animname.c_str());
						c_animinfo->end = c_animinfo->start + c_length;
					}
				}
			}
			//else if(_stricmp(words[0],"endnode")==0) {
			else if(stricmp(words[0].c_str(),"endnode")==0) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_obj != NULL && d_c_obj->getClass() == V_CLASS_VOBJECT && (dynamic_cast<VI_VObject *>(d_c_obj))->get_n_vertices() > 0 ) {
					// need to set texture coordinates
					if( mode != MODE_GEOM || polys == NULL || ( c_texture != NULL && tverts == NULL ) ) {
						LOG("ERROR - polys or tverts not set\n");
						//LOG(">>> %s\n",c_node->getName());
						Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
					}
					else {
						for(int i=0;i<n_polys;i++) {
							VI_Face *face = (dynamic_cast<VI_VObject *>(d_c_obj))->addPolygon(polys[i].v[0], polys[i].v[1], polys[i].v[2], "");
							if( c_texture != NULL ) {
								float u[3] = {0.0f, 0.0f, 0.0f};
								float v[3] = {0.0f, 0.0f, 0.0f};
								for(int j=0;j<3;j++) {
									int indx = polys[i].t[j];
									u[j] = tverts[indx].x;
									v[j] = tverts[indx].y;
								}
								(dynamic_cast<VI_VObject *>(d_c_obj))->setPolygonTextureCoords(face, c_texture, u, v);
							}
						}
						//(static_cast<Object3D *>(c_obj))->setShading(Polygon3D::CURVED, false);
					}
					if( polys != NULL ) {
						//delete [] polys;
						auto_polys.clear();
						polys = NULL;
					}
					if( tverts != NULL ) {
						//delete [] tverts;
						auto_tverts.clear();
						tverts = NULL;
					}
					n_polys = 0;
				}
				//else if( mode == MODE_ANIM ) {
				else {
					if( mode != MODE_GEOM )
						mode = MODE_ANIM;
					if( c_animinfo == NULL ) {
						/*LOG("ERROR - animinfo not set\n");
						LOG(">>> %s\n",c_node->getName());
						ok = false;*/
					}
					/*else if( c_node->equals("rootdummy") ) { // ??? NWN hack
					}*/
					else {
						//LOG("%s : %d , %d\n", c_animinfo->getName(), c_animinfo->position_keys->size(), c_animinfo->rotation_keys->size());
						//c_node->animationinfos->add(c_animinfo);
						//c_node->animationinfos->push_back(c_animinfo);
						/*SceneGraphNode *d_c_node = dynamic_cast<SceneGraphNode *>(c_node); // cast to the internal representation
						d_c_node->addAnimationInfo(c_animinfo);*/
						c_node->addHAnimationInfo(c_animinfo);
					}
				}
				c_node = NULL;
				c_obj = NULL;
				c_texture = NULL;
				c_animinfo = NULL;
			}
			else if( mode == MODE_POSITIONKEYLIST ) {
				if( c_animinfo == NULL || n_words < 4 ) {
					LOG("ERROR - positionkeylist\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				/*else if( c_node->equals("rootdummy") ) { // ??? NWN hack
				}*/
				else {
					// should this be scaled by animationscale?
					float time = (float)atof(words[0].c_str());
					Vector3D pos;
					pos.x = (float)atof(words[1].c_str());
					pos.z = - (float)atof(words[2].c_str());
					pos.y = (float)atof(words[3].c_str());
					pos *= animationscale;
					c_animinfo->addPositionKey(time, pos);
				}
			}
			else if( mode == MODE_ROTATIONKEYLIST ) {
				if( c_animinfo == NULL || n_words < 5 ) {
					LOG("ERROR - rotationkeylist\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float time = (float)atof(words[0].c_str());
					float x = (float)atof(words[1].c_str());
					float z = - (float)atof(words[2].c_str());
					float y = (float)atof(words[3].c_str());
					float a = (float)atof(words[4].c_str());
					Rotation3D rot;
					rot.getQuaternion()->setAngleAxis(x, y, z, a);
					c_animinfo->addRotationKey(time, rot);
				}
			}
			//else if(_stricmp(words[0],"parent")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"parent")==0 && mode == MODE_GEOM) {
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - parent\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					//if( _stricmp(words[1],"NULL") == 0 ) {
					if( stricmp(words[1].c_str(),"NULL") == 0 ) {
						if( root != NULL ) {
							LOG("ERROR - root already defined\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						else {
							root = c_node;
							//vision_garbage_collector.add(root);
						}
					}
					else {
						VI_SceneGraphNode *parent = root->findChildNode(words[1].c_str());
						if( parent == NULL ) {
							LOG("ERROR - can't find parent\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						else {
							parent->addChildNode(c_node);
						}
					}
				}
			}
			//else if(_stricmp(words[0],"position")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"position")==0 && mode == MODE_GEOM) {
				if( c_node == NULL || n_words < 4 ) {
					LOG("ERROR - position\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				/*else if( c_node->equals("rootdummy") ) { // bizarre NWN hack
				printf("!\n");
				float x = atof(words[1]);
				float z = - atof(words[2]);
				float y = -1000;
				//c_node->point.moveTo(x, y, z);
				Vector3D pos(x, y, z);
				c_node->setPosition(&pos);
				}*/
				else {
					float x = (float)atof(words[1].c_str());
					float z = - (float)atof(words[2].c_str());
					float y = (float)atof(words[3].c_str());
					//c_node->point.moveTo(x, y, z);
					Vector3D pos(x, y, z);
					pos *= animationscale;
					/*if( c_node->equals("rootdummy") ) { // ??? NWN hack
					pos *= animationscale;
					}*/
					c_node->setPosition(pos);
				}
			}
			//else if(_stricmp(words[0],"orientation")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"orientation")==0 && mode == MODE_GEOM) {
				if( c_node == NULL || n_words < 5 ) {
					LOG("ERROR - orientation\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float x = (float)atof(words[1].c_str());
					float z = - (float)atof(words[2].c_str());
					float y = (float)atof(words[3].c_str());
					float a = (float)atof(words[4].c_str());
					//c_node->point.rot.getQuaternion()->setAngleAxis(x, y, z, a);
					//c_node->rotation()->getQuaternion()->setAngleAxis(x, y, z, a);
					Rotation3D rot;
					rot.getQuaternion()->setAngleAxis(x, y, z, a);
					SceneGraphNode *d_c_node = dynamic_cast<SceneGraphNode *>(c_node); // cast to the internal representation
					d_c_node->setRotation(rot);
				}
			}
			//else if(_stricmp(words[0],"verts")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"verts")==0 && mode == MODE_GEOM) {
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - verts\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					// ??? not a trimesh etc
				}
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( d_c_obj->getClass() != V_CLASS_VOBJECT ) {
					LOG("ERROR - not a vobject\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					int n_verts = atoi(words[1].c_str());
					for(int i=0;i<n_verts;i++) {
						if(fgetLine(file,buffer)) {
							LOG("ERROR - not enough vertices\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						str = stripWhiteSpace(buffer);
						/*char *sub_words[MAX_WORDS];
						int n_sub_words = getWords(str,sub_words);*/
						vector<string> sub_words = getWords(str);
						int n_sub_words = sub_words.size();
						if( n_sub_words != 3 ) {
							LOG("ERROR - vertex\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						else {
							Vector3D vert;
							vert.x = (float)atof(sub_words[0].c_str());
							vert.z = - (float)atof(sub_words[1].c_str());
							vert.y = (float)atof(sub_words[2].c_str());
							vert *= animationscale;
							//(static_cast<Object3D *>(d_c_obj))->addVertex(0, vert);
							VI_VObject *vobj = dynamic_cast<VI_VObject *>(c_obj);
							vobj->addVertex(vert.x, vert.y, vert.z);
						}
						/*for(int i=0;i<n_sub_words;i++) {
							delete sub_words[i];
						}*/
					}
				}
			}
			//else if(_stricmp(words[0],"faces")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"faces")==0 && mode == MODE_GEOM) {
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - faces\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					// ??? not a trimesh etc
				}
				else {
					n_polys = atoi(words[1].c_str());
					//polys = new NWNPoly[n_polys];
					auto_polys.resize(n_polys);
					polys = &auto_polys[0];
					for(int i=0;i<n_polys;i++) {
						if(fgetLine(file,buffer)) {
							LOG("ERROR - not enough faces\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						str = stripWhiteSpace(buffer);
						/*char *sub_words[MAX_WORDS];
						int n_sub_words = getWords(str,sub_words);*/
						vector<string> sub_words = getWords(str);
						int n_sub_words = sub_words.size();
						if( n_sub_words != 8 ) {
							LOG("ERROR - face\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						else {
							/*int v1 = atof(sub_words[0]);
							int v2 = atof(sub_words[1]);
							int v3 = atof(sub_words[2]);
							Polygon3D poly(v1, v2, v3);
							c_obj->addPolygon(poly);*/
							for(int j=0;j<3;j++) {
								/*polys[i].v[j] = (float)atof(sub_words[j]);
								polys[i].t[j] = (float)atof(sub_words[4+j]);*/
								polys[i].v[j] = atoi(sub_words[j].c_str());
								polys[i].t[j] = atoi(sub_words[4+j].c_str());
							}
						}
						/*for(int i=0;i<n_sub_words;i++) {
							delete sub_words[i];
						}*/
					}
				}
			}
			//else if(_stricmp(words[0],"tverts")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"tverts")==0 && mode == MODE_GEOM) {
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - tverts\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					// ??? not a trimesh etc
				}
				else {
					int n_tverts = atoi(words[1].c_str());
					//tverts = new Vector3D[n_tverts];
					auto_tverts.resize(n_tverts);
					tverts = &auto_tverts[0];
					for(int i=0;i<n_tverts;i++) {
						if(fgetLine(file,buffer)) {
							LOG("ERROR - not enough tverts\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						str = stripWhiteSpace(buffer);
						/*char *sub_words[MAX_WORDS];
						int n_sub_words = getWords(str,sub_words);*/
						vector<string> sub_words = getWords(str);
						int n_sub_words = sub_words.size();
						if( n_sub_words != 3 ) {
							LOG("ERROR - tvert\n");
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
						}
						else {
							tverts[i].x =(float) atof(sub_words[0].c_str());
							tverts[i].y = (float)atof(sub_words[1].c_str());
							//tverts[i].z = (float)atof(sub_words[2]); // should be zero
						}
						/*for(int i=0;i<n_sub_words;i++) {
							delete sub_words[i];
						}*/
					}
				}
			}
			//else if(_stricmp(words[0],"bitmap")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"bitmap")==0 && mode == MODE_GEOM) {
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - bitmap\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					// ??? not a trimesh etc
				}
				//else if( _stricmp(words[1],"NULL") == 0 ) {
				else if( stricmp(words[1].c_str(),"NULL") == 0 || !want_textures ) {
					c_texture = NULL;
				}
				else {
					char filename_tx[1024] = "";
					sprintf(filename_tx,"%s%s.tga",folder.c_str(),words[1].c_str());
					c_texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(filename_tx);
					/*try {
						c_texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(filename_tx);
					}
					catch(VisionException *ve) {
						if( ve->type == VisionException::V_FILE_ERROR ) {
							if(c_texture == NULL) {
								LOG("WARNING - cannot load %s\n",filename_tx);
							}
							delete ve;
						}
						else {
							throw ve;
						}
					}*/
				}
			}
			//else if(_stricmp(words[0],"texture")==0 && mode == MODE_GEOM) {
			else if( stricmp(words[0].c_str(),"texture")==0 && mode == MODE_GEOM ) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - texture\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for texture\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				//else if( _stricmp(words[1],"NULL") == 0 ) {
				else if( stricmp(words[1].c_str(),"NULL") == 0 || !want_textures ) {
					//(static_cast<NWNParticleSystem *>(d_c_obj))->texture = NULL;
					//(dynamic_cast<NWNParticleSystem *>(c_obj))->texture = NULL;
					(dynamic_cast<VI_Particlesystem *>(c_obj))->setTexture(NULL);
				}
				else {
					char filename_tx[1024] = "";
					sprintf(filename_tx,"%s%s.tga",folder.c_str(),words[1].c_str());
					//(static_cast<NWNParticleSystem *>(c_obj))->texture = Texture::loadTexture(filename_tx);
					//(static_cast<NWNParticleSystem *>(d_c_obj))->texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(filename_tx);
					//if((static_cast<NWNParticleSystem *>(d_c_obj))->texture == NULL) {
					/*VI_Texture *texture = NULL;
					try {
						texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(filename_tx);
					}
					catch(VisionException *ve) {
						if( ve->type == VisionException::V_FILE_ERROR ) {
							if(c_texture == NULL) {
								LOG("WARNING - cannot load %s\n",filename_tx);
							}
							delete ve;
						}
						else {
							throw ve;
						}
					}
					if( texture != NULL ) {
						(dynamic_cast<VI_Particlesystem *>(c_obj))->setTexture(texture);
					}*/
					VI_Texture *texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(filename_tx);
					(dynamic_cast<VI_Particlesystem *>(c_obj))->setTexture(texture);
					//vision_garbage_collector.add(texture);
				}
			}
			//else if(_stricmp(words[0],"colorstart")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"colorstart")==0 && mode == MODE_GEOM) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 4 ) {
					LOG("ERROR - colorstart\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for colorstart\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float r = (float)atof(words[1].c_str());
					float g = (float)atof(words[2].c_str());
					float b = (float)atof(words[3].c_str());
					//(static_cast<NWNParticleSystem *>(d_c_obj))->color_st.setf(r,g,b);
					(dynamic_cast<VI_GeneralParticlesystem *>(c_obj))->setColoriStart((unsigned char)(255*r),(unsigned char)(255*g),(unsigned char)(255*b));
				}
			}
			//else if(_stricmp(words[0],"colorend")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"colorend")==0 && mode == MODE_GEOM) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 4 ) {
					LOG("ERROR - colorend\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for colorend\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float r = (float)atof(words[1].c_str());
					float g = (float)atof(words[2].c_str());
					float b = (float)atof(words[3].c_str());
					//(static_cast<NWNParticleSystem *>(d_c_obj))->color_nd.setf(r,g,b);
					(dynamic_cast<VI_GeneralParticlesystem *>(c_obj))->setColoriEnd((unsigned char)(255*r),(unsigned char)(255*g),(unsigned char)(255*b));
				}
			}
			//else if(_stricmp(words[0],"sizestart")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"sizestart")==0 && mode == MODE_GEOM) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - sizestart\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for sizestart\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float size = (float)atof(words[1].c_str());
					//(static_cast<NWNParticleSystem *>(d_c_obj))->size_st = size;
					(dynamic_cast<VI_GeneralParticlesystem *>(c_obj))->setSizeStart(size);
				}
			}
			//else if(_stricmp(words[0],"sizeend")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"sizeend")==0 && mode == MODE_GEOM) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - sizeend\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for sizeend\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float size = (float)atof(words[1].c_str());
					//(static_cast<NWNParticleSystem *>(d_c_obj))->size_nd = size;
					(dynamic_cast<VI_GeneralParticlesystem *>(c_obj))->setSizeEnd(size);
				}
			}
			//else if(_stricmp(words[0],"mass")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"mass")==0 && mode == MODE_GEOM) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - mass\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for mass\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float mass = (float)atof(words[1].c_str());
					//(static_cast<NWNParticleSystem *>(d_c_obj))->mass = mass;
					(dynamic_cast<VI_GeneralParticlesystem *>(c_obj))->setMass(mass);
				}
			}
			//else if(_stricmp(words[0],"birthrate")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"birthrate")==0 && mode == MODE_GEOM) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - birthrate\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for birthrate\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float birthrate = (float)atoi(words[1].c_str());
					//(static_cast<NWNParticleSystem *>(d_c_obj))->birthRate = birthrate;
					(dynamic_cast<VI_GeneralParticlesystem *>(c_obj))->setBirthRate(birthrate);
				}
			}
			//else if(_stricmp(words[0],"velocity")==0 && mode == MODE_GEOM) {
			else if(stricmp(words[0].c_str(),"velocity")==0 && mode == MODE_GEOM) {
				VisionObject *d_c_obj = dynamic_cast<VisionObject *>(c_obj); // cast to the internal representation
				if( c_node == NULL || n_words < 2 ) {
					LOG("ERROR - velocity\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( c_obj == NULL ) {
					LOG("ERROR - no object defined for velocity\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else if( d_c_obj->getClass() != V_CLASS_NWNPARTICLESYSTEM ) {
					LOG("ERROR - not a particlesystem\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					float velocity = (float)atof(words[1].c_str());
					//(static_cast<NWNParticleSystem *>(d_c_obj))->initVel = velocity;
					(dynamic_cast<VI_GeneralParticlesystem *>(c_obj))->setInitialVelocity(velocity);
				}
			}
			//else if(_stricmp(words[0],"length")==0) {
			else if(stricmp(words[0].c_str(),"length")==0) {
				if( mode != MODE_ANIM || n_words < 2 ) {
					LOG("ERROR - length\n");
					Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
				}
				else {
					c_length = (float)atof(words[1].c_str());
				}
			}

			/*for(int i=0;i<n_words;i++) {
				delete words[i];
				words[i]=NULL;
			}*/
		}

		for(size_t i=0;i<objs_v.size() ;i++) {
			if( !objs_v.at(i)->check() ) {
				LOG("Imported NWN model is corrupt");
				Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Corrupt or unsupported NWN MDL file"));
			}
		}
		//Vision::unlock(false);
		//if( ok )
		{
			vision_garbage_collector.setSucceeded();
		}
	}
	//if( ok )
	{
		if( !Vision::isLocked() ) {
			for(size_t i=0;i<objs_v.size();i++) {
				VisionObject *vo = dynamic_cast<VisionObject *>(objs_v.at(i));
				vo->prepare();
			}
		}
		if( n_objs != NULL || objs != NULL ) {
			if( n_objs != NULL )
				*n_objs = objs_v.size();
			if( objs != NULL ) {
				*objs = new VI_Object *[objs_v.size()];
				for(size_t i=0;i<objs_v.size();i++) {
					(*objs)[i] = objs_v.at(i);
				}
			}
		}
	}
	/*else {
		// no need to free memory - should be already done by the VisionObjectGarbageCollector
		Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_NWN_MDL_ERROR,"Failed to import NWN MDL file"));
	}*/
	//delete objs_v;
	return root;
}

VI_Mesh *Loader::importMD2(const char *filename, const char *skinfile) {
	LOG("Importing MD2 (Quake2) File: %s Skin: %s\n",filename,skinfile);

	FILE *file = fopen(filename,"rb");
	if( file == NULL ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open file"));
	}
	/*FILE *file = NULL;
	if( fopen_s(&file, filename, "rb") != 0 ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open file"));
		return NULL;
	}*/

	fseek(file,0,SEEK_END);
	int filelen = ftell(file);
	fseek(file,0,SEEK_SET);
	//char *buffer = new char[filelen + 1];
	// use a vector, so we don't have to manually delete, so this is safe if exceptions thrown!
	vector<char> auto_buffer(filelen+1);
	char *buffer = &auto_buffer[0]; 
	fread(buffer,sizeof(char),filelen,file);
	fclose(file);
	file = NULL;

	VI_Mesh *mesh = NULL;
	{
		//Vision::lock();
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		Texture *texture = NULL;
		if(skinfile != NULL) {
			texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(skinfile);
			// n.b., will throw exception if file not found
		}

		md2_dmdl_t *header = (md2_dmdl_t *)buffer;
		ENDIANSWAPLONG(&header->ident);
		ENDIANSWAPLONG(&header->version);
		ENDIANSWAPLONG(&header->skinwidth);
		ENDIANSWAPLONG(&header->skinheight);
		ENDIANSWAPLONG(&header->framesize);
		ENDIANSWAPLONG(&header->num_skins);
		ENDIANSWAPLONG(&header->num_xyz);
		ENDIANSWAPLONG(&header->num_st);
		ENDIANSWAPLONG(&header->num_tris);
		ENDIANSWAPLONG(&header->num_glcmds);
		ENDIANSWAPLONG(&header->num_frames);
		ENDIANSWAPLONG(&header->ofs_skins);
		ENDIANSWAPLONG(&header->ofs_st);
		ENDIANSWAPLONG(&header->ofs_tris);
		ENDIANSWAPLONG(&header->ofs_frames);
		ENDIANSWAPLONG(&header->ofs_glcmds);
		ENDIANSWAPLONG(&header->ofs_end);
		LOG("MD2Object ID %d version %d\n",header->ident,header->version);
		if(header->ident != 844121161) {
			delete texture;
			Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_MD2_ERROR,"Not a valid MD2 file"));
		}
		if(header->version != 8) {
			delete texture;
			Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_MD2_ERROR,"Only version 8 MD2 files are supported"));
		}

		LOG("    %d frames\n",header->num_frames);
		LOG("    %d vertices\n",header->num_xyz);
		LOG("    %d polygons\n",header->num_tris);

		mesh = VI_Mesh::create("Imported from Loader::importMD2", header->num_frames);

		mesh->setAnimFrames(V_ANIMSTATE_MD2_IDLE, 0, 39);
		mesh->setAnimFrames(V_ANIMSTATE_MD2_CROUCH, 136, 154);
		mesh->setAnimFrames(V_ANIMSTATE_MD2_RUN, 40, 45);
		mesh->setAnimFrames(V_ANIMSTATE_MD2_JUMP, 67, 73);
		mesh->setAnimFrames(V_ANIMSTATE_MD2_FIRE, 47, 53);
		mesh->setAnimFrames(V_ANIMSTATE_MD2_DIE, 170, 177);
		mesh->setAnimFrames(V_ANIMSTATE_MD2_ALL, 0, -1);

		//mesh->addTexture(texture); // texture now owned by mesh
		mesh->addChild(texture); // texture now owned by mesh

		//Vertex3D *pointList = new Vertex3D[header->num_xyz * mesh->getNFrames()];
		vector<Vertex3D> auto_pointList(header->num_xyz * mesh->getNFrames());
		Vertex3D *pointList = &auto_pointList[0]; 

		for(int j=0;j<mesh->getNFrames();j++) {
			md2_frame_t *frame = (md2_frame_t *)&buffer[header->ofs_frames + header->framesize * j ];
			ENDIANSWAPLONG(&frame->scale[0]);
			ENDIANSWAPLONG(&frame->scale[1]);
			ENDIANSWAPLONG(&frame->scale[2]);
			ENDIANSWAPLONG(&frame->translate[0]);
			ENDIANSWAPLONG(&frame->translate[1]);
			ENDIANSWAPLONG(&frame->translate[2]);
			//LOG("frame %d : scale %f , %f , %f : translate %f , %f , %f\n", j, frame->scale[0], frame->scale[1], frame->scale[2], frame->translate[0], frame->translate[1], frame->translate[2]);
			Vertex3D *pL = &pointList[header->num_xyz * j];
			for(int i=0;i<header->num_xyz;i++) {
				pL[i].z = - ( frame->scale[0] * frame->fp[i].v[0] + frame->translate[0] );
				pL[i].x =   ( frame->scale[1] * frame->fp[i].v[1] + frame->translate[1] );
				pL[i].y =   ( frame->scale[2] * frame->fp[i].v[2] + frame->translate[2] );
				/*if( j == 0 ) {
				LOG("%d : %f , %f , %f\n", i, pL[i].x, pL[i].y, pL[i].z);
				}*/
			}
		}

		int n_st = 0;
		//md2_texCoord_t *st = NULL;
		vector<md2_texCoord_t> st;
		if( texture != NULL ) {
			n_st = header->num_st;
			//st = new md2_texCoord_t[n_st];

			md2_stIndex_t *stPtr = (md2_stIndex_t *)&buffer[header->ofs_st];
			for(int i=0;i<n_st;i++) {
				ENDIANSWAPSHORT(&stPtr[i].s);
				ENDIANSWAPSHORT(&stPtr[i].t);
				// textures may be rescaled (to power of 2), so we need to use the original width/heights!
				/*st[i].s = (float)stPtr[i].s / (float)texture->getWidth();
				st[i].t = 1.0f - (float)stPtr[i].t / (float)texture->getHeight();*/
				/*st[i].s = (float)stPtr[i].s / (float)header->skinwidth;
				st[i].t = 1.0f - (float)stPtr[i].t / (float)header->skinheight;*/
				md2_texCoord_t this_st;
				this_st.s = (float)stPtr[i].s / (float)header->skinwidth;
				this_st.t = 1.0f - (float)stPtr[i].t / (float)header->skinheight;
				st.push_back(this_st);
			}
		}

		//md2_mesh_t *triIndex = new md2_mesh_t[header->num_tris];
		vector<md2_mesh_t> auto_triIndex(header->num_tris);
		md2_mesh_t *triIndex = &auto_triIndex[0]; 

		md2_mesh_t *bufIndexPtr = (md2_mesh_t *)&buffer[header->ofs_tris];
		for(int j=0;j<mesh->getNFrames();j++) {
			for(int i=0;i<header->num_tris;i++) {
				ENDIANSWAPSHORT(&bufIndexPtr[i].index_xyz[0]);
				ENDIANSWAPSHORT(&bufIndexPtr[i].index_xyz[1]);
				ENDIANSWAPSHORT(&bufIndexPtr[i].index_xyz[2]);
				ENDIANSWAPSHORT(&bufIndexPtr[i].index_st[0]);
				ENDIANSWAPSHORT(&bufIndexPtr[i].index_st[1]);
				ENDIANSWAPSHORT(&bufIndexPtr[i].index_st[2]);
				triIndex[i].index_xyz[0] = bufIndexPtr[i].index_xyz[0];
				triIndex[i].index_xyz[1] = bufIndexPtr[i].index_xyz[1];
				triIndex[i].index_xyz[2] = bufIndexPtr[i].index_xyz[2];
				triIndex[i].index_st[0] = bufIndexPtr[i].index_st[0];
				triIndex[i].index_st[1] = bufIndexPtr[i].index_st[1];
				triIndex[i].index_st[2] = bufIndexPtr[i].index_st[2];
			}
		}

		// calculate vertex normals
		//Vector *vx_polys = new Vector[header->num_xyz];
		//vector<int> *vx_polys = new vector<int>[header->num_xyz];
		// we want an array (done as vector) of vector<int>s!
		vector< vector<int> > auto_vx_polys(header->num_xyz);
		vector<int> *vx_polys = &auto_vx_polys[0]; 

		for(int j=0;j<header->num_tris;j++) {
			for(int k=0;k<3;k++) {
				int vx_indx = triIndex[j].index_xyz[k];
				//vx_polys[vx_indx].add((void *)j);
				vx_polys[vx_indx].push_back(j);
			}
		}

		//Vector3D *poly_normals = new Vector3D[header->num_tris];
		vector<Vector3D> auto_poly_normals(header->num_tris);
		Vector3D *poly_normals = &auto_poly_normals[0]; 

		for(int frame = 0;frame<mesh->getNFrames();frame++) {
			Vertex3D *pL = &pointList[header->num_xyz * frame];
			for(int i=0;i<header->num_tris;i++) {
				Vector3D *p0 = &pL[ triIndex[i].index_xyz[0] ];
				Vector3D *p1 = &pL[ triIndex[i].index_xyz[1] ];
				Vector3D *p2 = &pL[ triIndex[i].index_xyz[2] ];

				poly_normals[i] = *p2 - *p0;
				Vector3D v = *p1 - *p0;
				poly_normals[i].cross(v);
				if(poly_normals[i].square() > V_TOL_MACHINE)
					poly_normals[i].normalise();
				else
					poly_normals[i].set(0.0,0.0,0.0);
			}
			for(int i=0;i<header->num_xyz;i++) {
				pL[i].normal.set(0.0,0.0,0.0);
				//for(int j=0;j<header->num_tris;j++) {
				//	if( i == triIndex[j].index_xyz[0] ||
				//                i == triIndex[j].index_xyz[1] ||
				//        	i == triIndex[j].index_xyz[2] ) {
				//                pL[i].normal += poly_normals[j];
				//        }
				//}
				for(size_t j=0;j<vx_polys[i].size();j++) {
					//int poly_indx = (int)vx_polys[i].get(j);
					int poly_indx = vx_polys[i].at(j);
					pL[i].normal += poly_normals[poly_indx];
				}
				if(pL[i].normal.square() > V_TOL_MACHINE)
					pL[i].normal.normalise();
			}
		}
		/*delete [] vx_polys;
		vx_polys = NULL;
		delete [] poly_normals;
		poly_normals = NULL;*/

		int max_points = 3 * header->num_tris;
		/*mesh->n_renderDatas = 1;
		mesh->renderDatas = new RenderData[ mesh->n_renderDatas ];*/
		RenderData *rd = new RenderData();
		/*mesh->renderDatas.resize( 1 );
		mesh->renderDatas[0] = rd;*/
		Mesh *d_mesh = dynamic_cast<Mesh *>(mesh); // cast to the internal representation
		d_mesh->addRenderData(rd);
		//rd->initVertexArrays(mesh->getNFrames(), max_points, false, /*false,*/ true, true);
		rd->initVertexArrays(mesh->getNFrames(), max_points, true);
		rd->setNTextures(1);
		rd->setTexture(0, texture);
		//rd->texture = texture;
		rd->setSolid(true);
		/*rd->cols[0] = 255;
		rd->cols[1] = 255;
		rd->cols[2] = 255;
		rd->cols[3] = 255;*/

		//unsigned short *vx_index = new unsigned short[max_points]; // this maps vertices in the RenderData to vertices in the MD2 object, since some vertices may have different texture coords!
		//unsigned short *st_index = new unsigned short[max_points];
		vector<unsigned short> auto_vx_index(max_points);
		unsigned short *vx_index = &auto_vx_index[0]; // this maps vertices in the RenderData to vertices in the MD2 object, since some vertices may have different texture coords!
		vector<unsigned short> auto_st_index(max_points);
		unsigned short *st_index = &auto_st_index[0];
		
		//rd->va_indices = new unsigned short[max_points];
		int indx = 0;
		//for(int i=0,count=0;i<header->num_tris;i++) {
		for(int i=0;i<header->num_tris;i++) {
			for(int j=0;j<3;j++) {
				int this_vx_index = triIndex[i].index_xyz[j];
				int this_st_index = triIndex[i].index_st[j];
				int found_indx = -1;
				for(int k=0;k<indx && found_indx == -1;k++) {
					if( vx_index[k] == this_vx_index && st_index[k] == this_st_index ) {
						//if( vx_index[k] == this_vx_index ) {
						found_indx = k;
					}
				}
				if( found_indx != -1 ) {
					//rd->va_indices[count++] = found_indx;
					//rd->va_indices.push_back(found_indx);
					rd->addVaIndex(found_indx);
				}
				else {
					vx_index[indx] = this_vx_index;
					st_index[indx] = this_st_index;
					//rd->va_indices[count++] = indx++;
					//rd->va_indices.push_back(indx++);
					rd->addVaIndex(indx++);
				}
			}
		}
		/*int indx = header->num_xyz;
		for(int i=0;i<header->num_xyz;i++) {
		vx_index[i] = i;
		}
		for(int i=0,count=0;i<header->num_tris;i++) {
		for(int j=0;j<3;j++) {
		rd.va_indices[count++] = triIndex[i].index_xyz[j];
		}
		}*/
		rd->setNVertices(indx);
		//rd->n_renderlength = header->num_tris*3;
		rd->setRenderlength(false, header->num_tris*3);

		for(int frame=0/*,count = 0*/;frame<mesh->getNFrames();frame++) {
			Vertex3D *pL = &pointList[header->num_xyz * frame];
			for(size_t i=0;i<rd->getNVertices();i++) {
				Vertex3D *vx = &pL[ vx_index[i] ];
				/*rd->va_vertices[count] = vx->x;
				rd->va_normals[count++] = vx->normal.x;
				rd->va_vertices[count] = vx->y;
				rd->va_normals[count++] = vx->normal.y;
				rd->va_vertices[count] = vx->z;
				rd->va_normals[count++] = vx->normal.z;*/
				rd->setVertex(frame, i, *vx);
				rd->setNormal(frame, i, vx->normal);
			}
		}

		for(size_t i=0;i<rd->getNVertices();i++) {
			md2_texCoord_t *this_st = &st[ st_index[i] ];
			//md2_texCoord_t *this_st = &st[ 0 ];
			/*rd->va_texcoords[2*i] = this_st->s;
			rd->va_texcoords[2*i+1] = this_st->t;*/
			//LOG("texcood %d : %f, %f\n", i, this_st->s, this_st->t);
			rd->setTexCoord(i, this_st->s, this_st->t);
		}

		mesh->setHardwareAnimation(GraphicsEnvironment::getSingleton(), true);
		/*delete [] vx_index;
		vx_index = NULL;
		delete [] st_index;
		st_index = NULL;*/

		/*for(int i=0;i<mesh->n_renderDatas;i++)
		mesh->renderDatas[i].prepare();*/

		//delete [] pointList;
		/*if( st != NULL )
			delete [] st;*/
		//delete [] triIndex;

		//Vision::unlock(false);
		vision_garbage_collector.setSucceeded();
	}

	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(mesh);
		vo->prepare();
	}

	if( !mesh->check() ) {
		delete mesh;
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_MD2_ERROR, "Imported MD2 model is corrupt"));
	}
	LOG("    done\n");
	return mesh;
}

struct MeshInfo {
	int material_index;
	int n_texture_channels;
	int n_vertices;
	int n_faces;
	int n_bones;

	MeshInfo() : material_index(0), n_texture_channels(0), n_vertices(0), n_faces(0), n_bones(0) {
	}
};

struct MaterialInfo {
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float emissive[4];
	float shininess;

	MaterialInfo() : shininess(0.0f) {
		for(int i=0;i<4;i++) {
			this->ambient[i] = 0.0f;
			this->diffuse[i] = 0.0f;
			this->specular[i] = 0.0f;
			this->emissive[i] = 0.0f;
		}
	}
};

struct NodeInfo {
	float position[3];
	float rotation[4]; // quaternion: w, x, y, z
	float scale[3];

	NodeInfo() {
		for(int i=0;i<3;i++) {
			this->position[i] = 0.0f;
		}
		for(int i=0;i<4;i++) {
			this->rotation[i] = 0.0f;
		}
		for(int i=0;i<3;i++) {
			this->scale[i] = 0.0f;
		}
	}
};

bool compare_int4(const int lhs[4], const int rhs[4]) {
	for(int i=0;i<4;i++) {
		if( lhs[i] != rhs[i] )
			return false;
	}
	return true;
}

void float4_to_int4(int ival[4], const float f[4]) {
	for(int i=0;i<4;i++) {
		ival[i] = static_cast<int>( 255.0f * f[i] + 0.5f );
		if( ival[i] < 0 )
			ival[i] = 0;
		else if( ival[i] > 255 )
			ival[i] = 255;
	}
}

string read_string(FILE *file) {
	string str;
	int length = 0;
	fread(&length, sizeof(size_t), 1, file);
	//LOG("    string length %d\n", length);
	if( length != 0 ) {
		char *buffer = new char[length+1];
		fread(buffer, sizeof(char), length, file);
		buffer[length] = '\0';
		//LOG("    string: %s\n", buffer);
		str = buffer;
		delete [] buffer;
	}
	return str;
}

const int ID_LENGTH = 4;
char id[ID_LENGTH] = "";

class BoneInfo {
public:
	string name;
	Matrix4d matrix;
	vector<unsigned int> indices;
	vector<float> weights;
};

// n.b., done as global functions rather than members of Loader, so we don't have to export MaterialInfo

//VI_Mesh *readVisionMeshCore(map<VI_Mesh *, vector<BoneInfo> > *bone_map, FILE *file, const vector<MaterialInfo> &materials, const vector<Texture *> &textures) {
VI_Mesh *readVisionMeshCore(map<RenderData *, vector<BoneInfo> > *bone_map, FILE *file, const vector<MaterialInfo> &materials, const vector<Texture *> &textures) {
	int n_rd = 0;
	// read: number of meshes
	fread(&n_rd, sizeof(int), 1, file);
	LOG("read %d submeshes\n", n_rd);

	if( n_rd == 0 ) {
		return NULL;
	}

	VI_Mesh *mesh = VI_Mesh::create("", 1);
	Mesh *d_mesh = dynamic_cast<Mesh *>(mesh); // cast to the internal representation

	for(int i=0;i<n_rd;i++) {
		LOG("read submesh: %d\n", i);
		// read: mesh ID
		fread(id, sizeof(char), ID_LENGTH, file);
		if( strncmp(id, "MESH", ID_LENGTH) != 0 ) {
			Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id MESH"));
		}

		MeshInfo mesh_info;
		// read: header
		fread(&mesh_info, sizeof(MeshInfo), 1, file);
		LOG("read header: material index %d, %d texture channels, %d vertices, %d faces\n", mesh_info.material_index, mesh_info.n_texture_channels, mesh_info.n_vertices, mesh_info.n_faces);
		if( mesh_info.material_index < 0 || (static_cast<size_t>(mesh_info.material_index)) >= textures.size() ) {
			Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "material index is out of range"));
		}

		// read: vertex positions
		Vector3D *vertices = new Vector3D[mesh_info.n_vertices];
		for(int j=0;j<mesh_info.n_vertices;j++) {
			Vector3D vec;
			fread(&vec.x, sizeof(float), 1, file);
			fread(&vec.y, sizeof(float), 1, file);
			fread(&vec.z, sizeof(float), 1, file);
			vertices[j] = vec;
		}

		// read: vertex normals
		Vector3D *normals = new Vector3D[mesh_info.n_vertices];
		for(int j=0;j<mesh_info.n_vertices;j++) {
			Vector3D vec;
			fread(&vec.x, sizeof(float), 1, file);
			fread(&vec.y, sizeof(float), 1, file);
			fread(&vec.z, sizeof(float), 1, file);
			normals[j] = vec;
		}

		// read: texture coordinates
		Vector3D *tex_coords = NULL;
		if( mesh_info.n_texture_channels > 0 ) {
			tex_coords = new Vector3D[mesh_info.n_vertices];
			for(int j=0;j<mesh_info.n_vertices;j++) {
				Vector3D vec;
				fread(&vec.x, sizeof(float), 1, file);
				fread(&vec.y, sizeof(float), 1, file);
				//LOG("texcood %d : %f, %f\n", j, vec.x, vec.y);
				tex_coords[j] = vec;
			}
		}

		// read: polygon indices
		unsigned short *indices = new unsigned short[3*mesh_info.n_faces];
		for(int j=0,c=0;j<mesh_info.n_faces;j++) {
			for(int k=0;k<3;k++,c++) {
				unsigned int index = 0;
				fread(&index, sizeof(unsigned int), 1, file);
				if( index >= 65536 ) {
					Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "face index is out of range, >= 65536"));
				}
				indices[c] = static_cast<unsigned short>(index);
			}
		}

		// read: bones
		LOG("    %d bones\n", mesh_info.n_bones);
		vector<BoneInfo> bone_infos;
		for(int j=0;j<mesh_info.n_bones;j++) {
			// read: name
			string bone_name = read_string(file);
			// read: offset matrix
			Matrix4d bone_matrix;
			float matrix_values[16];
			fread(matrix_values, sizeof(float), 16, file);
			// stored as row-major
			for(int row=0,indx=0;row<4;row++) {
				for(int col=0;col<4;col++,indx++) {
					bone_matrix.setElement(row, col, matrix_values[indx]);
				}
			}
			unsigned int n_bone_weights = 0;
			fread(&n_bone_weights, sizeof(unsigned int), 1, file);
			LOG("    bone %d %s has %d weights\n", j, bone_name.c_str(), n_bone_weights);
			unsigned int *bone_indices = new unsigned int[n_bone_weights];
			float *bone_weights = new float[n_bone_weights];
			fread(bone_indices, sizeof(unsigned int), n_bone_weights, file);
			fread(bone_weights, sizeof(float), n_bone_weights, file);
			for(unsigned int k=0;k<n_bone_weights;k++) {
				LOG("        %d : %d : %f\n", k, bone_indices[k], bone_weights[k]);
			}
			/*for(int row=0;row<4;row++) {
				for(int col=0;col<4;col++) {
					LOG("  %f", bone_matrix.getElement(row, col));
				}
				LOG("\n");
			}*/
			BoneInfo bone_info;
			bone_info.name = bone_name;
			bone_info.matrix = bone_matrix;
			bone_info.indices.resize(n_bone_weights);
			bone_info.weights.resize(n_bone_weights);
			for(unsigned int k=0;k<n_bone_weights;k++) {
				bone_info.indices[k] = bone_indices[k];
				bone_info.weights[k] = bone_weights[k];
			}
			delete [] bone_indices;
			delete [] bone_weights;
			bone_infos.push_back(bone_info);
		}
		//(*bone_map)[mesh] = bone_infos;

		// now set up the submesh

		RenderData *rd = new RenderData();
		d_mesh->addRenderData(rd);

		(*bone_map)[rd] = bone_infos;

		//rd->initVertexArrays(mesh->getNFrames(), mesh_info.n_vertices, true, /*false,*/ true, mesh_info.n_texture_channels > 0);
		rd->initVertexArrays(mesh->getNFrames(), mesh_info.n_vertices, true);
		rd->setSolid(true);
		if( mesh_info.n_texture_channels > 0 ) {
			rd->setNTextures(1);
			rd->setTexture(0, textures.at(mesh_info.material_index));
		}

		int black[4] = {255, 255, 255, 255};
		int white[4] = {255, 255, 255, 255};
		int ambient[4], diffuse[4], specular[4], emissive[4];
		float4_to_int4(ambient, materials.at(mesh_info.material_index).ambient);
		float4_to_int4(diffuse, materials.at(mesh_info.material_index).diffuse);
		float4_to_int4(specular, materials.at(mesh_info.material_index).specular);
		float4_to_int4(emissive, materials.at(mesh_info.material_index).emissive);

		if( !compare_int4(ambient, diffuse) ) {
			LOG("    warning, materials with ambient different to diffuse not supported - assuming ambient is same as diffuse\n");
		}
		if( !compare_int4(specular, black) ) {
			LOG("    warning, materials with specular not supported - specular will be ignored\n");
		}
		if( !compare_int4(emissive, black) ) {
			LOG("    warning, materials with emissive not supported - emissive will be ignored\n");
		}
		if( !compare_int4(diffuse, white) ) {
			LOG("    set vertex colors from material diffuse color\n");
			for(int i=0;i<mesh_info.n_vertices;i++) {
				for(int j=0;j<3;j++) {
					rd->setColor(i, j, diffuse[j]);
				}
			}
		}

		for(int j=0;j<mesh_info.n_vertices;j++) {
			rd->setVertex(0, j, vertices[j]);
			rd->setNormal(0, j, normals[j]);
		}

		if( mesh_info.n_texture_channels > 0 ) {
			for(int j=0;j<mesh_info.n_vertices;j++) {
				rd->setTexCoord(j, tex_coords[j].x, tex_coords[j].y);
			}
		}

		for(int j=0;j<3*mesh_info.n_faces;j++) {
			rd->addVaIndex(indices[j]);
		}
		rd->setRenderlength(false, 3*mesh_info.n_faces);

		delete [] vertices;
		delete [] normals;
		delete [] tex_coords;
		delete [] indices;
		LOG("    done reading submesh: %d\n", i);

	}

	return mesh;
}

VI_Mesh *readVisionMesh(FILE *file, const vector<MaterialInfo> &materials, const vector<Texture *> &textures) {
	// read: node ID
	fread(id, sizeof(char), ID_LENGTH, file);
	if( strncmp(id, "NODE", ID_LENGTH) != 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id NODE"));
	}

	NodeInfo node_info;
	// read: header
	fread(&node_info, sizeof(NodeInfo), 1, file);

	// read: name
	string mesh_name = read_string(file);
	LOG("mesh name: %s\n", mesh_name.c_str());

	//map<VI_Mesh *, vector<BoneInfo> > bone_map; 
	map<RenderData *, vector<BoneInfo> > bone_map; 
	VI_Mesh *mesh = readVisionMeshCore(&bone_map, file, materials, textures);
	if( mesh == NULL ) {
		// create a dummy one
		mesh = VI_Mesh::create("", 1);
	}
	mesh->setName(mesh_name.c_str());

	// read: child ID
	fread(id, sizeof(char), ID_LENGTH, file);
	if( strncmp(id, "CHLD", ID_LENGTH) != 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id CHLD"));
	}

	int n_children = 0;
	// read: number of children
	fread(&n_children, sizeof(int), 1, file);
	LOG("read %d children\n", n_children);
	// recursively read children
	for(int i=0;i<n_children;i++) {
		LOG("read child %d:\n", i);
		VI_Mesh *child = readVisionMesh(file, materials, textures);
		Mesh *d_child = dynamic_cast<Mesh *>(child); // cast to the internal representation
		if( d_child->getNRenderData() == 0 && d_child->getNChildObjects() == 0 ) {
			LOG("    ignore empty child\n");
		}
		else {
			mesh->combineObject(child);
		}
		delete child;
		LOG("done child %d\n", i);
	}

	Quaternion quat(node_info.rotation[0], node_info.rotation[1], node_info.rotation[2], node_info.rotation[3]);
	Rotation3D rot(quat);
	mesh->rotate(rot);

	return mesh;
}

//VI_SceneGraphNode *readVisionNode(map<VI_Mesh *, vector<BoneInfo> > *bone_map, FILE *file, const vector<MaterialInfo> &materials, const vector<Texture *> &textures) {
VI_SceneGraphNode *readVisionNode(map<RenderData *, vector<BoneInfo> > *bone_map, FILE *file, const vector<MaterialInfo> &materials, const vector<Texture *> &textures) {
	// read: node ID
	fread(id, sizeof(char), ID_LENGTH, file);
	if( strncmp(id, "NODE", ID_LENGTH) != 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id NODE"));
	}

	NodeInfo node_info;
	// read: header
	fread(&node_info, sizeof(NodeInfo), 1, file);

	// read: name
	string mesh_name = read_string(file);
	LOG("mesh name: %s\n", mesh_name.c_str());

	VI_SceneGraphNode *node = NULL;
	VI_Mesh *mesh = readVisionMeshCore(bone_map, file, materials, textures);
	if( mesh != NULL ) {
		mesh->setName(mesh_name.c_str());
		//VI_Entity *entity = VI_createEntity(mesh);
		//node = entity;
		node = VI_createSceneGraphNode(mesh);
	}
	else {
		node = VI_createSceneGraphNode();
	}
	node->setName(mesh_name.c_str());

	// read: child ID
	fread(id, sizeof(char), ID_LENGTH, file);
	if( strncmp(id, "CHLD", ID_LENGTH) != 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id CHLD"));
	}

	int n_children = 0;
	// read: number of children
	fread(&n_children, sizeof(int), 1, file);
	LOG("read %d children\n", n_children);
	// recursively read children
	for(int i=0;i<n_children;i++) {
		LOG("read child %d:\n", i);
		VI_SceneGraphNode *child = readVisionNode(bone_map, file, materials, textures);
		node->addChildNode(child);
		LOG("done child %d\n", i);
	}

	node->setPosition(node_info.position[0], node_info.position[1], node_info.position[2]);
	Quaternion quat(node_info.rotation[0], node_info.rotation[1], node_info.rotation[2], node_info.rotation[3]);
	Rotation3D rot(quat);
	node->setRotation(rot);

	return node;
}

struct GlobalAnimationInfo {
	string name;
	float duration;
	GlobalAnimationInfo(const string &name, float duration) : name(name), duration(duration) {
	}
};

map<string, AnimationInfo *> readVisionAnimations(vector<GlobalAnimationInfo> *global_animation_infos, FILE *file) {
	// returns a map from each bone/node to the corresponding animation info
	// also returns a global list
	// read: animations ID
	fread(id, sizeof(char), ID_LENGTH, file);
	if( strncmp(id, "ANIM", ID_LENGTH) != 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id ANIM"));
	}
	// read: number of animations
	unsigned int n_animations = 0;
	fread(&n_animations, sizeof(unsigned int), 1, file);
	LOG("read %d n_animations\n", n_animations);
	map<string, AnimationInfo *> animation_infos;
	for(unsigned int i=0;i<n_animations;i++) {
		LOG("    animation %d:\n", i);
		string animation_name = read_string(file);
		LOG("    name: %s\n", animation_name.c_str());
		double duration = 0.0, ticks_per_second = 0.0f;
		fread(&duration, sizeof(double), 1, file);
		fread(&ticks_per_second, sizeof(double), 1, file);
		LOG("    duration = %f\n", duration);
		LOG("    ticks_per_second = %f\n", ticks_per_second);
		GlobalAnimationInfo global_animation_info(animation_name, duration);
		global_animation_infos->push_back(global_animation_info);
		unsigned int n_bone_channels = 0;
		fread(&n_bone_channels, sizeof(unsigned int), 1, file);
		LOG("    read %d n_bone_channels\n", n_bone_channels);
		for(unsigned int j=0;j<n_bone_channels;j++) {
			AnimationInfo *animation_info = new AnimationInfo();
			animation_info->end = animation_info->start + static_cast<float>(duration);
			animation_info->setName(animation_name.c_str());
			LOG("        bone channel %d:\n", j);
			string channel_name = read_string(file);
			LOG("        name: %s\n", channel_name.c_str());
			unsigned int n_position_keys = 0, n_rotation_keys = 0, n_scaling_keys = 0;

			// read: position keys
			fread(&n_position_keys, sizeof(unsigned int), 1, file);
			Vector3D *position_keys = new Vector3D[n_position_keys];
			double *position_key_times = new double[n_position_keys];
			fread(position_keys, sizeof(Vector3D), n_position_keys, file);
			fread(position_key_times, sizeof(double), n_position_keys, file);
			// read: rotation keys
			fread(&n_rotation_keys, sizeof(unsigned int), 1, file);
			Quaternion *rotation_keys = new Quaternion[n_rotation_keys];
			double *rotation_key_times = new double[n_rotation_keys];
			fread(rotation_keys, sizeof(Quaternion), n_rotation_keys, file);
			fread(rotation_key_times, sizeof(double), n_rotation_keys, file);
			// read: scaling keys
			fread(&n_scaling_keys, sizeof(unsigned int), 1, file);
			Vector3D *scaling_keys = new Vector3D[n_scaling_keys];
			double *scaling_key_times = new double[n_scaling_keys];
			fread(scaling_keys, sizeof(Vector3D), n_scaling_keys, file);
			fread(scaling_key_times, sizeof(double), n_scaling_keys, file);
			LOG("        %d position keys, %d rotation keys, %d scaling keys\n", n_position_keys, n_rotation_keys, n_scaling_keys);

			for(unsigned int k=0;k<n_position_keys;k++) {
				LOG("            %d: position: %f: %f, %f, %f\n", k, position_key_times[k], position_keys[k].x, position_keys[k].y, position_keys[k].z);
				animation_info->addPositionKey(position_key_times[k], position_keys[k]);
			}
			for(unsigned int k=0;k<n_rotation_keys;k++) {
				LOG("            %d: rotation: %f: %f, %f, %f, %f\n", k, rotation_key_times[k], rotation_keys[k].w, rotation_keys[k].x, rotation_keys[k].y, rotation_keys[k].z);
				animation_info->addRotationKey(rotation_key_times[k], rotation_keys[k]);
			}
			if( n_scaling_keys > 0 ) {
				LOG("### scaling keys not supported!\n");
			}
			animation_infos[channel_name] = animation_info;

			delete [] position_keys;
			delete [] position_key_times;
			delete [] rotation_keys;
			delete [] rotation_key_times;
			delete [] scaling_keys;
			delete [] scaling_key_times;
		}
	}

	return animation_infos;
}

void readVisionMaterials(vector<MaterialInfo> *materials, vector<Texture *> *textures, FILE *file, string folder, const char *override_texture) {
	// read: materials ID
	fread(id, sizeof(char), ID_LENGTH, file);
	if( strncmp(id, "MATS", ID_LENGTH) != 0 ) {
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id MATS"));
	}
	// read: number of materials
	unsigned int n_materials = 0;
	fread(&n_materials, sizeof(unsigned int), 1, file);
	LOG("read %d materials\n", n_materials);
	for(unsigned int i=0;i<n_materials;i++) {
		// read: material ID
		fread(id, sizeof(char), ID_LENGTH, file);
		if( strncmp(id, "MAT ", ID_LENGTH) != 0 ) {
			Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "expected id MAT "));
		}
		// read: material info
		MaterialInfo material_info;
		fread(&material_info, sizeof(MaterialInfo), 1, file);
		materials->push_back(material_info);
		LOG("material %d:\n", i);
		LOG("    ambient: %f, %f, %f, %f\n", material_info.ambient[0], material_info.ambient[1], material_info.ambient[2], material_info.ambient[3]);
		LOG("    diffuse: %f, %f, %f, %f\n", material_info.diffuse[0], material_info.diffuse[1], material_info.diffuse[2], material_info.diffuse[3]);
		LOG("    specular: %f, %f, %f, %f\n", material_info.specular[0], material_info.specular[1], material_info.specular[2], material_info.specular[3]);
		LOG("    emissive: %f, %f, %f, %f\n", material_info.emissive[0], material_info.emissive[1], material_info.emissive[2], material_info.emissive[3]);

		// read: material path
		size_t material_path_length = 0;
		fread(&material_path_length, sizeof(size_t), 1, file);
		LOG("    path length %d\n", material_path_length);
		string texture_filename;
		if( material_path_length != 0 ) {
			char *buffer = new char[material_path_length+1];
			fread(buffer, sizeof(char), material_path_length, file);
			buffer[material_path_length] = '\0';
			texture_filename = folder + buffer;
			LOG("    texture: %s\n", texture_filename.c_str());
			delete [] buffer;
		}
		if( override_texture != NULL ) {
			LOG("    override with: %s\n", override_texture);
			texture_filename = override_texture;
		}
		if( texture_filename.length() == 0 ) {
			textures->push_back(NULL);
		}
		else {
			Texture *texture = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(texture_filename.c_str());
			// n.b., will throw exception if file not found
			textures->push_back(texture);
		}
	}
}

struct BonesToVertexFrameInfo {
	string animation_name;
	float time;
	BonesToVertexFrameInfo(const string &animation_name, float time) : animation_name(animation_name), time(time) {
	}
};

//void convertNodeMeshFromBonesToVertex(VI_SceneGraphNode *node, VI_SceneGraphNode *root, const map<VI_Mesh *, vector<BoneInfo> > &bone_map, const map<string, AnimationInfo *> &animation_infos, const vector<BonesToVertexFrameInfo> &frame_infos) {
void convertNodeMeshFromBonesToVertex(VI_SceneGraphNode *node, VI_SceneGraphNode *root, const map<RenderData *, vector<BoneInfo> > &bone_map, const map<string, AnimationInfo *> &animation_infos, const vector<BonesToVertexFrameInfo> &frame_infos) {
	if( node->getObject() != NULL ) {
		LOG("convert mesh from node: %s\n", node->getName());
		VI_Mesh *mesh = dynamic_cast<VI_Mesh *>(node->getObject());
		Mesh *d_mesh = dynamic_cast<Mesh *>(mesh); // cast to the internal representation

		/*const map<VI_Mesh *, vector<BoneInfo> >::const_iterator bone_infos_iter = bone_map.find(mesh);
		if( bone_infos_iter == bone_map.end() ) {
			Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_VISION_MESH_ERROR,"Can't find bone infos for this mesh"));
		}
		vector<BoneInfo> bone_infos = bone_infos_iter->second;*/

		VI_Mesh *new_mesh = VI_Mesh::create("", frame_infos.size());
		Mesh *d_new_mesh = dynamic_cast<Mesh *>(new_mesh); // cast to the internal representation
		new_mesh->setName(mesh->getName());
		new_mesh->setAnimFrames(V_ANIMSTATE_MD2_ALL, 0, -1);

		for(size_t i=0;i<d_mesh->getNRenderData();i++) {
			LOG("convert renderdata %d\n", i);
			RenderData *rd = d_mesh->getRenderData(i);
			RenderData *new_rd = new RenderData();
			d_new_mesh->addRenderData(new_rd);

			const map<RenderData *, vector<BoneInfo> >::const_iterator bone_infos_iter = bone_map.find(rd);
			if( bone_infos_iter == bone_map.end() ) {
				Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_VISION_MESH_ERROR,"Can't find bone infos for this renderdata"));
			}
			vector<BoneInfo> bone_infos = bone_infos_iter->second;

			int max_weights_per_vertex = 0;
			{
				vector<int> vertex_n_weights; // number of weights for each vertex
				vertex_n_weights.resize(rd->getNVertices());
				for(size_t i=0;i<rd->getNVertices();i++) {
					vertex_n_weights[i] = 0;
				}
				for(vector<BoneInfo>::const_iterator iter = bone_infos.begin(); iter != bone_infos.end(); ++iter) {
					const BoneInfo bone_info = *iter;
					for(vector<unsigned int>::const_iterator iter2 = bone_info.indices.begin(); iter2 != bone_info.indices.end(); ++iter2) {
						unsigned int indx = *iter2;
						vertex_n_weights[indx]++;
					}
				}
				for(size_t i=0;i<rd->getNVertices();i++) {
					max_weights_per_vertex = std::max( vertex_n_weights[i], max_weights_per_vertex );
				}
				LOG("max_weights_per_vertex = %d\n", max_weights_per_vertex);
			}
			vector<size_t> vertex_n_bones;
			vector<size_t> vertex_bone_indices; // bone index for each vertex
			vector<float> vertex_bone_weights;
			vertex_n_bones.resize(rd->getNVertices());
			vertex_bone_indices.resize(max_weights_per_vertex*rd->getNVertices());
			vertex_bone_weights.resize(max_weights_per_vertex*rd->getNVertices());
			for(size_t j=0;j<rd->getNVertices();j++) {
				vertex_n_bones[j] = 0;
			}
			for(size_t j=0;j<bone_infos.size();j++) {
				const BoneInfo bone_info = bone_infos[j];
				for(size_t k=0;k<bone_info.indices.size();k++) {
					size_t indx = bone_info.indices[k];
					float weight = bone_info.weights[k];
					int c_n_bones = vertex_n_bones[indx];
					vertex_bone_indices[max_weights_per_vertex*indx+c_n_bones] = j;
					vertex_bone_weights[max_weights_per_vertex*indx+c_n_bones] = weight;
					vertex_n_bones[indx]++;
				}
			}

			//new_rd->initVertexArrays(new_mesh->getNFrames(), rd->getNVertices(), true, /*false,*/ true, rd->getNTextures() > 0);
			new_rd->initVertexArrays(new_mesh->getNFrames(), rd->getNVertices(), true);
			new_rd->setSolid(true);

			new_rd->setNTextures(rd->getNTextures());
			for(int j=0;j<new_rd->getNTextures();j++) {
				new_rd->setTexture(j, rd->getTexture(j));
			}

			for(size_t j=0;j<rd->getNVertices();j++) {
				int dim = rd->getDim();
				for(int k=0;k<dim;k++) {
					unsigned char col = rd->getColor(j, k);
					new_rd->setColor(j, k, col);
				}
			}

			if( new_rd->getNTextures() > 0 ) {
				for(size_t j=0;j<rd->getNVertices();j++) {
					Vector3D uv = rd->getTexCoord(j);
					new_rd->setTexCoord(j, uv.x,  uv.y);
				}
			}

			for(unsigned short j=0;j<rd->getRenderlength();j++) {
				unsigned short indx = rd->getVaIndex(j);
				new_rd->addVaIndex(indx);
			}
			new_rd->setRenderlength(false, rd->getRenderlength());

			// now do vertices/normal for each frame
			for(size_t frame=0;frame<frame_infos.size();frame++) {
				const BonesToVertexFrameInfo frame_info = frame_infos.at(frame);
				// adopt animation
				root->setHAnimation(frame_info.animation_name.c_str(), true);
				root->setHAnimationTime(frame_info.time, true);
				root->adoptHAnimation(true);

				vector<Matrix4d> bone_matrices;
				for(size_t j=0;j<bone_infos.size();j++) {
					const BoneInfo bone_info = bone_infos[j];
					VI_SceneGraphNode *bone_node = root->findChildNode(bone_info.name.c_str());
					if( bone_node == NULL ) {
						LOG("can't find node for bone: %s\n", bone_info.name.c_str());
						Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_VISION_MESH_ERROR,"Can't find node for this bone"));
					}
					Matrix4d matrix;
					bone_node->localToWorldMatrix(&matrix);
					Matrix4d res_matrix = bone_info.matrix;
					res_matrix.premultiply(matrix);
					bone_matrices.push_back(res_matrix);
					/*LOG("%d bone: %s\n", j, bone_info.name.c_str());
					for(int row=0;row<4;row++) {
						for(int col=0;col<4;col++) {
							//LOG("  %f", res_matrix.getElement(row, col));
							//LOG("  %f", bone_info.matrix.getElement(row, col));
							LOG("  %f", matrix.getElement(row, col));
						}
						LOG("\n");
					}*/
				}

				for(size_t j=0;j<rd->getNVertices();j++) {
					size_t n_bones = vertex_n_bones[j];
					Matrix4d matrix;
					for(size_t k=0;k<n_bones;k++) {
						size_t bone_indx = vertex_bone_indices[max_weights_per_vertex*j+k];
						float bone_weight = vertex_bone_weights[max_weights_per_vertex*j+k];
						if( bone_indx >= bone_matrices.size() ) {
							LOG("vertex %d, %d th bone_indx %d is larger than n bone matrices %d\n", j, k, bone_indx, bone_matrices.size());
							Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_VISION_MESH_ERROR,"bone indx out of range"));
						}
						Matrix4d this_matrix = bone_matrices[bone_indx];
						this_matrix *= bone_weight;
						matrix += this_matrix;
					}

					Vector3D vec = rd->getVertex(0, j);
					matrix.transformVector(&vec);
					new_rd->setVertex(frame, j, vec);
					Vector3D norm = rd->getNormal(0, j);
					matrix.transformDirection(&norm);
					new_rd->setNormal(frame, j, norm);
				}

			}

		}

		delete mesh;
		node->setObject(new_mesh);
	}

	for(size_t i=0;i<node->getNChildNodes();i++) {
		VI_SceneGraphNode *child = node->getChildNode(i);
		convertNodeMeshFromBonesToVertex(child, root, bone_map, animation_infos, frame_infos);
	}
}

VI_Mesh *Loader::importVisionMesh(const char *filename)  {
	return importVisionMesh(filename, NULL);
}

// imports as a single mesh (children are merged into a single mesh object, rather than preserving the scenegraph hierarchy); bones are for now converted to per-vertex animation frames
VI_Mesh *Loader::importVisionMesh(const char *filename, const char *override_texture)  {
	LOG("Importing Vision Mesh File: %s\n", filename);
	string folder = getFolder(filename);

	FILE *file = fopen(filename,"rb");
	if( file == NULL ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open file"));
	}

	VI_Mesh *mesh = NULL;
	{
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		vector<GlobalAnimationInfo> global_animation_infos;
		map<string, AnimationInfo *> animation_infos = readVisionAnimations(&global_animation_infos, file);

		vector<MaterialInfo> materials;
		vector<Texture *> textures;
		readVisionMaterials(&materials, &textures, file, folder, override_texture);

		mesh = readVisionMesh(file, materials, textures);

		// cleanup
		for(map<string, AnimationInfo *>::iterator iter = animation_infos.begin(); iter != animation_infos.end(); ++iter) {
			AnimationInfo *animation_info = iter->second;
			delete animation_info;
		}

		vision_garbage_collector.setSucceeded();
	}

	if( !Vision::isLocked() ) {
		VisionObject *vo = dynamic_cast<VisionObject *>(mesh);
		vo->prepare();
	}

	if( !mesh->check() ) {
		delete mesh;
		Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "Imported Vision Mesh model is corrupt"));
	}
	LOG("    done\n");
	return mesh;
}

VI_SceneGraphNode *Loader::importVisionNode(const char *filename)  {
	return importVisionNode(filename, NULL);
}

// imports as a scenegraphnode
VI_SceneGraphNode *Loader::importVisionNode(const char *filename, const char *override_texture)  {
	LOG("Importing Vision Node File: %s\n", filename);
	string folder = getFolder(filename);

	FILE *file = fopen(filename,"rb");
	if( file == NULL ) {
		Vision::setError(new VisionException(NULL,VisionException::V_FILE_ERROR,"Cannot open file"));
	}

	VI_SceneGraphNode *node = NULL;
	{
		// destructors called in reverse order, so we'll garbage collect before unlocking
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		VisionObjectGarbageCollector vision_garbage_collector; // will free objects when it goes out of scope, unless we set the success flag

		vector<GlobalAnimationInfo> global_animation_infos;
		map<string, AnimationInfo *> animation_infos = readVisionAnimations(&global_animation_infos, file);

		LOG("convert %d animations\n", global_animation_infos.size());
		if( global_animation_infos.size() == 0 ) {
			Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_VISION_MESH_ERROR,"Model file has no animations"));
		}
		const float fps = 5;
		vector<BonesToVertexFrameInfo> frame_infos;
		for(vector<GlobalAnimationInfo>::const_iterator iter = global_animation_infos.begin(); iter != global_animation_infos.end(); ++iter) {
			GlobalAnimationInfo global_animation_info = *iter;
			string name = global_animation_info.name;
			float duration = global_animation_info.duration;
			int n_frames = static_cast<int>(ceil(fps * duration)) + 1;
			if( n_frames < 2 )
				n_frames = 2;
			LOG("    animation: %s : length %f : make %d frames\n", name.c_str(), duration, n_frames);
			for(int i=0;i<n_frames;i++) {
				float alpha = ((float) i) / (float)(n_frames-1);
				float time = alpha * duration;
				LOG("        %d : %f : %f\n", i, alpha, time);
				BonesToVertexFrameInfo frame_info(name, time);
				frame_infos.push_back(frame_info);
			}
		}

		vector<MaterialInfo> materials;
		vector<Texture *> textures;
		readVisionMaterials(&materials, &textures, file, folder, override_texture);

		//map<VI_Mesh *, vector<BoneInfo> > bone_map;
		map<RenderData *, vector<BoneInfo> > bone_map;
		node = readVisionNode(&bone_map, file, materials, textures);

		// apply animation
		for(map<string, AnimationInfo *>::iterator iter = animation_infos.begin(); iter != animation_infos.end(); ++iter) {
			string name = iter->first;
			AnimationInfo *animation_info = iter->second;
			VI_SceneGraphNode *child = node->findChildNode(name.c_str());
			if( child == NULL ) {
				LOG("can't find animation name: %s\n", name.c_str());
				Vision::setError(new VisionException(NULL,VisionException::V_IMPORT_VISION_MESH_ERROR,"Animation name doesn't refer to a node name"));
			}
			child->addHAnimationInfo(animation_info);
		}

		convertNodeMeshFromBonesToVertex(node, node, bone_map, animation_infos, frame_infos);

		vision_garbage_collector.setSucceeded();
	}

	vector<VI_Mesh *> meshes;
	if( !Vision::isLocked() ) {
		for(vector<VI_Mesh *>::iterator iter = meshes.begin(); iter != meshes.end(); ++iter) {
			VI_Mesh *mesh = *iter;
			VisionObject *vo = dynamic_cast<VisionObject *>(mesh);
			vo->prepare();
			if( !mesh->check() ) {
				Vision::setError(new VisionException(NULL, VisionException::V_IMPORT_VISION_MESH_ERROR, "Imported Vision Mesh model is corrupt"));
			}
		}
	}

	LOG("    done\n");
	return node;
}
