#pragma once

#include "GraphicsEnvironment.h"

class SDL_GraphicsEnvironment : public GraphicsEnvironment {
protected:
	bool active;
#ifdef _WIN32
	HWND hWnd;
#endif
#ifdef __linux
	Display *x_display;
	Window x_window;
#endif

	void initSDL();

public:
	SDL_GraphicsEnvironment(VisionPrefs visionPrefs);
	virtual ~SDL_GraphicsEnvironment();

	virtual bool hasContext() const {
		return false;
	}

	// interface
	virtual void setVisible(bool visible);
	virtual void setWindowTitle(const char *title);
	virtual void grabInput(bool grab);
	virtual void showCursor(bool show);
	virtual void setCursor(unsigned char *data, unsigned char *mask, int w, int h, int hot_x, int hot_y);

	virtual bool isActive() const {
		//VI_log("### ACTIVE %d\n", this->active);
		return active;
	}
	//virtual void handleInput();
	virtual void processEvents();
	virtual void waitForEvents();
	virtual void sleep(int time);
	virtual int getRealTimeMS();

	// OS specific functions
#ifdef _WIN32
	virtual HWND getHWND() const {
		return this->hWnd;
	}
	virtual bool isTrueFullscreen() const {
		return isFullScreen();
	}
#endif
#ifdef __linux
	virtual Display *getXDisplay() const {
		return this->x_display;
	}
	virtual Window getXWindow() const {
		return this->x_window;
	}
#endif
};
