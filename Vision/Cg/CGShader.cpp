//---------------------------------------------------------------------------
#include "CGShader.h"

//---------------------------------------------------------------------------

CGShader::CGShader(bool vertex_shader) : Shader(vertex_shader) {
	this->cgProfile = CG_PROFILE_UNKNOWN;
	//this->cgProfile = vertex_shader ? g3->getGraphicsEnvironment()->cgVertexProfile : g3->getGraphicsEnvironment()->cgPixelProfile;
	this->cgProgram = NULL;
}

CGShader::~CGShader() {
	if( this->cgProgram != NULL ) {
		cgDestroyProgram(cgProgram);
		this->cgProgram = NULL;
	}
}

/*void CGShader::init() {
	// Matrices
	cgparam_ModelViewProj = cgGetNamedParameter(cgProgram, "mx.ModelViewProj");
	cgparam_ModelViewIT = cgGetNamedParameter(cgProgram, "mx.ModelViewIT");
	cgparam_ModelView = cgGetNamedParameter(cgProgram, "mx.ModelView");
}*/

void CGShader::SetUniformParameter1f(const char *pszParameter, float p1) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgSetParameter1f(p, p1);
	}
}

void CGShader::SetUniformParameter2f(const char *pszParameter, float p1, float p2) const {
	/*float v[2] = {p1, p2};
	this->SetUniformParameter2fv(pszParameter, v);*/
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgSetParameter2f(p, p1, p2);
	}
}


void CGShader::SetUniformParameter3f(const char *pszParameter, float p1, float p2, float p3) const {
	/*float v[3] = {p1, p2, p3};
	this->SetUniformParameter3fv(pszParameter, v);*/
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgSetParameter3f(p, p1, p2, p3);
	}
}

void CGShader::SetUniformParameter4f(const char *pszParameter, float p1, float p2, float p3, float p4) const {
	/*float v[4] = {p1, p2, p3, p4};
	this->SetUniformParameter4fv(pszParameter, v);*/
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgSetParameter4f(p, p1, p2, p3, p4);
	}
}

/*void CGShader::SetUniformParameter2fv(const char *pszParameter, const float v[2]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgSetParameter2fv(p, v);
	}
}

void CGShader::SetUniformParameter3fv(const char *pszParameter, const float v[3]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgSetParameter3fv(p, v);
	}
}*/

void CGShader::SetUniformParameter4fv(const char *pszParameter, const float v[4]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p ) {
		cgSetParameter4fv(p, v);
	}
}

/*void CGShader::setTexture(const Texture *texture) const {
	this->SetUniformParameter1f("textureID", texture != NULL ? 1.0f : 0.0f);
}*/

/*void CGShader::setMatSpecular(const float specular[4]) const {
	this->SetUniformParameter4fv("specular", specular);
}

void CGShader::setLightPos(float p1, float p2, float p3, float d1, float d2, float d3) const {
	this->SetUniformParameter4f("lt.LightPos", p1, p2, p3, 0.0f);
	this->SetUniformParameter4f("lightpos", p1, p2, p3, 0.0f);
	this->SetUniformParameter4f("lt.LightVec", d1, d2, d3, 0.0f);
}

void CGShader::setLightAmbient(float rf, float gf, float bf) const {
	this->SetUniformParameter4f("lt.AmbLight", rf, gf, bf, 1.0f);
}

void CGShader::setLightDiffuse(float rf, float gf, float bf) const {
	this->SetUniformParameter4f("lt.DiffLight", rf, gf, bf, 1.0f);
}

void CGShader::setLightSpecular(float rf, float gf, float bf) const {
	this->SetUniformParameter4f("lt.SpecLight", rf, gf, bf, 1.0f);
}

void CGShader::setLightAtten(float a, float b, float c) const {
	this->SetUniformParameter3f("lt.AttenLight", a, b, c);
}*/

/*void CGShader::setBumpTexture(const Texture *texture) const {
	this->SetUniformParameter1f("bump_textureID", texture != NULL ? 1.0f : 0.0f);
}*/

/*void CGShader::setHardwareAnimationAlpha(float alpha) const {
	SetUniformParameter1f("frame_alpha", alpha);
}*/

CGFXShaderEffect::CGFXShaderEffect(CGcontext cg_context, const char *file, const char **args) {
	this->effect = cgCreateEffectFromFile(cg_context, file, args);
	this->technique = cgGetFirstTechnique(effect);
	while( technique != NULL && cgValidateTechnique(technique) == CG_FALSE) {
		LOG("CGFX warning: %s: Technique %s did not validate, skipping.\n", file, cgGetTechniqueName(technique));
		technique = cgGetNextTechnique(technique);
	}
	if( technique == NULL ) {
		LOG("CGFX: %s can't find valid technique\n", file);
		cgDestroyEffect(effect);
		effect = NULL;
		Vision::setError(new VisionException(this,VisionException::V_CG_ERROR, "Failed to load CGFX program"));
	}

	cgparam_ModelViewProj = cgGetEffectParameterBySemantic(effect, "ModelViewProjection");
	if( cgparam_ModelViewProj == NULL ) {
		LOG("CGFX: %s can't find ModelViewProjection semantic\n", file);
		cgDestroyEffect(effect);
		effect = NULL;
		Vision::setError(new VisionException(this,VisionException::V_CG_ERROR, "Can't find ModelViewProjection semantic"));
	}
}

CGFXShaderEffect::~CGFXShaderEffect() {
	cgDestroyEffect(effect);
}

void CGFXShaderEffect::enable(Renderer *renderer) const {
	CGpass pass = cgGetFirstPass(technique);
	if( pass == NULL ) {
		Vision::setError(new VisionException(this,VisionException::V_CG_ERROR, "Failed to set CGFX pass"));
	}
    cgSetPassState(pass);
	renderer->setActiveShader(this);
}

void CGFXShaderEffect::SetUniformParameter1f(const char *parameter, float p1) const {
	CGparameter p = cgGetNamedEffectParameter(effect, parameter);
	if( p ) {
		cgSetParameter1f(p, p1);
	}
}

void CGFXShaderEffect::SetUniformParameter2f(const char *parameter, float p1, float p2) const {
	CGparameter p = cgGetNamedEffectParameter(effect, parameter);
	if( p ) {
		cgSetParameter2f(p, p1, p2);
	}
}

void CGFXShaderEffect::SetUniformParameter3f(const char *parameter, float p1, float p2, float p3) const {
	CGparameter p = cgGetNamedEffectParameter(effect, parameter);
	if( p ) {
		cgSetParameter3f(p, p1, p2, p3);
	}
}

void CGFXShaderEffect::SetUniformParameter4f(const char *parameter, float p1, float p2, float p3, float p4) const {
	CGparameter p = cgGetNamedEffectParameter(effect, parameter);
	if( p ) {
		cgSetParameter4f(p, p1, p2, p3, p4);
	}
}

void CGFXShaderEffect::SetUniformParameter4fv(const char *parameter, const float v[4]) const {
	CGparameter p = cgGetNamedEffectParameter(effect, parameter);
	if( p ) {
		cgSetParameter4fv(p, v);
	}
}
