//---------------------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>
#include <cassert>
//#include <ctime>

#include <algorithm>
using std::min;
using std::max;

#include <set>
using std::set;

#include "Terrain.h"
#include "Renderer.h"
#include "GraphicsEnvironment.h"
#include "GraphicsEngine.h"
#include "Entity.h"
#include "Texture.h"
#include "RenderData.h"
#include "World.h"

const int MAX_SPLATS = RenderData::MAX_TEXTURES-1;

//---------------------------------------------------------------------------

// times: AMD prepare; Atom prepare / AMD fps; Atom fps
// run outside of exe, with garbage, with D3D9, shaders, splatting, 800x600 on AMD, 640x480 on Atom
// on The Russians Year 286 perf terrain prepare.sav
// average of moving Militia (near Sverdlovsk): west, west, nw, nw, west
//const int SECTION_SIZE = 1024;
//const int SECTION_SIZE = 128; // 43; 131
//const int SECTION_SIZE = 64; // 38; 113 // 225 fps; 33fps
const int SECTION_SIZE = 32; // 62; 187 // 230 fps; 32.5fps

// Atmosphere constants

const float Kr = 0.0025f;		// Rayleigh scattering constant
const float Kr4PI = (float)(Kr*4.0f*M_PI);
//const float Km = 0.0001f;		// Mie scattering constant (ok on NVIDIA)
const float Km = 0.0004f;		// Mie scattering constant
//const float Km = 0.001f;		// Mie scattering constant (original)
const float Km4PI = (float)(Km*4.0f*M_PI);
const float ESun = 20.0f;		// Sun brightness constant
const float Kg = -0.95f;		// The Mie phase asymmetry factor

const float fInnerRadius = 10.0f;
//const float fOuterRadius = 10.0 + 0.16;
const float fOuterRadius = 10.0f + 0.3f;
const float fScale = 1.0f / (fOuterRadius - fInnerRadius);

const float fRayleighScaleDepth = 0.25f; // needs to be set to same value as in shader!
const float height = fInnerRadius; // camera height

Atmosphere::Atmosphere(SceneGraphNode *light_ent) {
	ASSERT( GraphicsEnvironment::getSingleton()->getGraphics3D()->hasAtmosphereShaders() );

	this->light_ent = light_ent;
	fWavelength[0] = 0.650f;		// 650 nm for red
	fWavelength[1] = 0.570f;		// 570 nm for green
	fWavelength[2] = 0.475f;		// 475 nm for blue
	fWavelength4[0] = pow(fWavelength[0], 4.0f);
	fWavelength4[1] = pow(fWavelength[1], 4.0f);
	fWavelength4[2] = pow(fWavelength[2], 4.0f);

	//pSphere = GraphicsEnvironment::getSingleton()->getRenderer()->createQuadricObject();
	//pSphere = GraphicsEnvironment::getSingleton()->getRenderer()->createSphereObject(fOuterRadius, 200, 100);
	pSphere = GraphicsEnvironment::getSingleton()->getRenderer()->createSphereObject(fOuterRadius, 100, 50); // original
	//pSphere = GraphicsEnvironment::getSingleton()->getRenderer()->createSphereObject(fOuterRadius, 20, 10);
}

Atmosphere::~Atmosphere() {
	delete pSphere;
}

void Atmosphere::render(Graphics3D *g,SceneGraphNode *portal) {
	/*if( !g->hasAtmosphereShaders() ) {
		return;
	}*/
	/** Adapted from GPU Gems 2, Chapter 16 - Accurate Atmosphere Scattering.
	*/
	Vector3D camera_pos(0.0, height, 0.0);

	/*Vector3D light_pos(0.0, 0.0, -1.0);
	//Vector3D light_pos(0.0, 0.1, -1.0);
	//Vector3D light_pos(0.0, 1.0, 0.0);
	//Vector3D light_pos(0.0, 0.2, -1.0);*/
	Vector3D light_pos = light_ent->getCurrentWorldPos();
	light_pos.normalise();

	const ShaderEffect *effect = g->getAtmosShader();
	effect->enable(g->getRenderer());
	//LOG("%f, %f, %f : %f, %f, %f\n", camera_pos.x, camera_pos.y, camera_pos.z, light_pos.x, light_pos.y, light_pos.z);
	/*effect->getVertexShader()->SetUniformParameter3f("v3CameraPos", camera_pos.x, camera_pos.y, camera_pos.z);
	effect->getVertexShader()->SetUniformParameter3f("v3LightPos", light_pos.x, light_pos.y, light_pos.z);
	effect->getVertexShader()->SetUniformParameter3f("v3InvWavelength", 1/fWavelength4[0], 1/fWavelength4[1], 1/fWavelength4[2]);
	effect->getVertexShader()->SetUniformParameter1f("fCameraHeight", height);
	effect->getVertexShader()->SetUniformParameter1f("fInnerRadius", fInnerRadius);
	effect->getVertexShader()->SetUniformParameter1f("fKrESun", Kr*ESun);
	effect->getVertexShader()->SetUniformParameter1f("fKmESun", Km*ESun);
	effect->getVertexShader()->SetUniformParameter1f("fKr4PI", Kr4PI);
	effect->getVertexShader()->SetUniformParameter1f("fKm4PI", Km4PI);
	effect->getVertexShader()->SetUniformParameter1f("fScale", fScale);
	effect->getVertexShader()->SetUniformParameter1f("fScaleOverScaleDepth", fScale / fRayleighScaleDepth);

	effect->getPixelShader()->SetUniformParameter3f("v3LightPos", light_pos.x, light_pos.y, light_pos.z);
	effect->getPixelShader()->SetUniformParameter1f("g", Kg);
	effect->getPixelShader()->SetUniformParameter1f("g2", Kg*Kg);*/

	effect->SetUniformParameter3f("v3CameraPos", camera_pos.x, camera_pos.y, camera_pos.z);
	effect->SetUniformParameter3f("v3LightPos", light_pos.x, light_pos.y, light_pos.z);
	effect->SetUniformParameter3f("v3InvWavelength", 1/fWavelength4[0], 1/fWavelength4[1], 1/fWavelength4[2]);
	effect->SetUniformParameter1f("fCameraHeight", height);
	effect->SetUniformParameter1f("fInnerRadius", fInnerRadius);
	effect->SetUniformParameter1f("fKrESun", Kr*ESun);
	effect->SetUniformParameter1f("fKmESun", Km*ESun);
	effect->SetUniformParameter1f("fKr4PI", Kr4PI);
	effect->SetUniformParameter1f("fKm4PI", Km4PI);
	effect->SetUniformParameter1f("fScale", fScale);
	effect->SetUniformParameter1f("fScaleOverScaleDepth", fScale / fRayleighScaleDepth);

	effect->SetUniformParameter3f("v3LightPos", light_pos.x, light_pos.y, light_pos.z);
	effect->SetUniformParameter1f("g", Kg);
	effect->SetUniformParameter1f("g2", Kg*Kg);

	g->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_NONE);
	g->getRenderer()->enableTexturing(false);
	//g->getRenderer()->enableFixedFunctionLighting(false);
	//g->getRenderer()->setColor4(0,0,0,255);
	g->getRenderer()->setFrontFace(portal != NULL);

	g->getRenderer()->pushMatrix();
	g->getRenderer()->translate(0.0, -camera_pos.y, 0.0);
	//CGparameter pMatrix = cgGetNamedParameter(g->vertex_shader_skyfromatmos->cgProgram, "ModelViewProj");
	//cgGLSetStateMatrixParameter(pMatrix, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
	//cgGLSetStateMatrixParameter(g->vertex_shader_skyfromatmos->cgparam_ModelViewProj, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
	//g->getRenderer()->getActiveVertexShader()->SetModelViewProjMatrix("ModelViewProj");
	//g->getRenderer()->getActiveShader()->getVertexShader()->SetModelViewProjMatrix("ModelViewProj");
	g->getRenderer()->getActiveShader()->SetModelViewProjMatrix("ModelViewProj");

	//pSphere->drawSphere(fOuterRadius, 100, 50);
	pSphere->draw();
	g->getRenderer()->popMatrix();

	g->getRenderer()->setFrontFace(portal == NULL);
	//g->getRenderer()->enableFixedFunctionLighting(true);
	g->getRenderer()->enableTexturing(true);
	g->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);

	/*const Shader *c_vertex_shader = g->getRenderer()->getActiveVertexShader();
	const Shader *c_pixel_shader = g->getRenderer()->getActivePixelShader();
	if( c_vertex_shader != NULL )
		c_vertex_shader->disable();
	if( c_pixel_shader != NULL )
		c_pixel_shader->disable();*/
	g->getRenderer()->disableShaders();
}


SkyBox::SkyBox() : VisionObject() {
	//this->half = false;
	this->mode = V_SKYMODE_BOTTOMLESS;
	this->distinct_textures = true;
	//this->mode = V_SKYMODE_FULL;
	//this->mode = V_SKYMODE_HALF;
	for(int i=0;i<6;i++)
		textures[i] = NULL;
	for(int i=0;i<3;i++)
		col[i] = 255;
}

SkyBox::~SkyBox() {
	free();
}

void SkyBox::free() {
	this->deleteAllOwnedObjects();
	for(int i=0;i<6;i++) {
		textures[i] = NULL;
	}
	/*if( distinct_textures ) {
		for(int i=0;i<6;i++) {
			if( textures[i] != NULL ) {
				delete textures[i];
				textures[i] = NULL;
			}
		}
	}
	else {
		 // textures are identical
		for(int i=0;i<6;i++) {
			if( textures[i] != NULL ) {
				delete textures[i];
				break;
			}
		}
		for(int i=0;i<6;i++) {
			textures[i] = NULL;
		}
	}*/
}

void SkyBox::init(const char *texture,const char *ext) {
	char buffer[1024] = "";
	free();

	distinct_textures = true;

	/*try {
		sprintf(buffer,"%s_u%s",texture,ext);
		textures[0] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
		textures[0]->owned = true;

		sprintf(buffer,"%s_b%s",texture,ext);
		textures[1] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
		textures[1]->owned = true;

		sprintf(buffer,"%s_r%s",texture,ext);
		textures[2] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
		textures[2]->owned = true;

		sprintf(buffer,"%s_f%s",texture,ext);
		textures[3] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
		textures[3]->owned = true;

		sprintf(buffer,"%s_l%s",texture,ext);
		textures[4] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
		textures[4]->owned = true;

		sprintf(buffer,"%s_d%s",texture,ext);
		textures[5] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
		textures[5]->owned = true;
	}
	catch(...) {
		// make sure we free up any textures created so far
		free();
		throw;
	}*/

	sprintf(buffer,"%s_u%s",texture,ext);
	textures[0] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
	this->addChild(textures[0]);
	//textures[0]->owned = true;

	sprintf(buffer,"%s_b%s",texture,ext);
	textures[1] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
	this->addChild(textures[1]);
	//textures[1]->owned = true;

	sprintf(buffer,"%s_r%s",texture,ext);
	textures[2] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
	this->addChild(textures[2]);
	//textures[2]->owned = true;

	sprintf(buffer,"%s_f%s",texture,ext);
	textures[3] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
	this->addChild(textures[3]);
	//textures[3]->owned = true;

	sprintf(buffer,"%s_l%s",texture,ext);
	textures[4] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
	this->addChild(textures[4]);
	//textures[4]->owned = true;

	sprintf(buffer,"%s_d%s",texture,ext);
	textures[5] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(buffer);
	this->addChild(textures[5]);
	//textures[5]->owned = true;
}

void SkyBox::init1(const char *texture) {
	free();

	distinct_textures = false;

	textures[0] = GraphicsEnvironment::getSingleton()->getRenderer()->loadTexture(texture);
	this->addChild(textures[0]);

	//textures[0]->owned = true;
	for(int i=1;i<6;i++) {
		textures[i] = textures[0];
	}
}

void SkyBox::init1(Texture *texture) {
	free();

	distinct_textures = false;

	//texture->owned = true;

	for(int i=0;i<6;i++) {
		textures[i] = texture;
	}
}

void SkyBox::setMode(V_SKYMODE_t mode) {
	CHECK_SKYMODE(mode);
	this->mode = mode;
}

void SkyBox::render(Renderer *renderer) {
	renderer->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_NONE);

	renderer->enableFixedFunctionLighting(false);

	unsigned char color_data[] = {255, 255, 255, 100};

	float w1 = 0.0f, w2 = 0.0f, h1 = 0.0f, h2 = 0.0f;
	bool half = mode == V_SKYMODE_HALF;
	float vh = half ? 0.0f : -1.0f;
	// front = 3
	// back = 1
	// left = 2
	// right = 4
	// up = 0
	// down = 5

	// front
	textures[3]->enable();
	w1 = 1.0f/textures[3]->getWidth();
	h1 = 1.0f/textures[3]->getHeight();
	w2 = 1.0f - w1;
	h2 = 1.0f - h1;
	if( half )
		h1 = 0.5;
	float texcoord_data3[] = {
		w1, h2,
		w1, h1,
		w2, h1,
		w2, h2
	};
	float vertex_data3[] = {
		-1.0, 1.0, -1.0,
		-1.0, vh, -1.0,
		1.0, vh, -1.0,
		1.0, 1.0, -1.0
	};
	renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data3, texcoord_data3, 4, color_data, true, 4);

	/*if( skip ) {
		g->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);
		g->getRenderer()->enableLighting(true);
		return;
	}*/
	// right
	textures[2]->enable();
	w1 = 1.0f/textures[2]->getWidth();
	h1 = 1.0f/textures[2]->getHeight();
	w2 = 1.0f - w1;
	h2 = 1.0f - h1;
	if( half )
		h1 = 0.5;
	float texcoord_data2[] = {
		w2, h2,
		w1, h2,
		w1, h1,
		w2, h1
	};
	float vertex_data2[] = {
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		-1.0, vh, 1.0,
		-1.0, vh, -1.0
	};
	renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data2, texcoord_data2, 4, color_data, true, 4);

	// back
	textures[1]->enable();
	w1 = 1.0f/textures[1]->getWidth();
	h1 = 1.0f/textures[1]->getHeight();
	w2 = 1.0f - w1;
	h2 = 1.0f - h1;
	if( half )
		h1 = 0.5;
	float texcoord_data1[] = {
		w1, h2,
		w1, h1,
		w2, h1,
		w2, h2
	};
	float vertex_data1[] = {
		1.0, 1.0, 1.0,
		1.0, vh, 1.0,
		-1.0, vh, 1.0,
		-1.0, 1.0, 1.0
	};
	renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data1, texcoord_data1, 4, color_data, true, 4);

	// left
	textures[4]->enable();
	w1 = 1.0f/textures[3]->getWidth();
	h1 = 1.0f/textures[3]->getHeight();
	w2 = 1.0f - w1;
	h2 = 1.0f - h1;
	if( half )
		h1 = 0.5;
	float texcoord_data4[] = {
		w1, h2,
		w1, h1,
		w2, h1,
		w2, h2
	};
	float vertex_data4[] = {
		1.0, 1.0, -1.0,
		1.0, vh, -1.0,
		1.0, vh, 1.0,
		1.0, 1.0, 1.0
	};
	renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data4, texcoord_data4, 4, color_data, true, 4);

	// top
	textures[0]->enable();
	w1 = 1.0f/textures[0]->getWidth();
	h1 = 1.0f/textures[0]->getHeight();
	w2 = 1.0f - w1;
	h2 = 1.0f - h1;
	float texcoord_data0[] = {
		w1, h1,
		w2, h1,
		w2, h2,
		w1, h2
	};
	float vertex_data0[] = {
		-1.0, 1.0, 1.0,
		-1.0, 1.0, -1.0,
		1.0, 1.0, -1.0,
		1.0, 1.0, 1.0
	};
	renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data0, texcoord_data0, 4, color_data, true, 4);

	// bottom
	//if(!half) {
	if( mode == V_SKYMODE_FULL ) {
		textures[5]->enable();
		w1 = 1.0f/textures[5]->getWidth();
		h1 = 1.0f/textures[5]->getHeight();
		w2 = 1.0f - w1;
		h2 = 1.0f - h1;
		float texcoord_data5[] = {
			w1, h1,
			w2, h1,
			w2, h2,
			w1, h2
		};
		float vertex_data5[] = {
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0
		};
		renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data5, texcoord_data5, 4, color_data, true, 4);
	}

	renderer->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);
	renderer->enableFixedFunctionLighting(true);
}

void SkyBox::render(Graphics3D *g) {
	Renderer *renderer = g->getRenderer();
	this->render(renderer);

	int n_calls = mode == V_SKYMODE_FULL ? 6 : 5;
	g->getGraphicsEnvironment()->updateStats(n_calls, n_calls*4, n_calls*4, n_calls);
}


struct createNoise_data {
	Image2D *noise;
	int index;
};

void createNoise_thread(void *ptr) {
	createNoise_data *data = static_cast<createNoise_data *>(ptr);

	unsigned char filter_max[3] = {255, 255, 255};
	unsigned char filter_min[3] = {0, 0, 0};
	const int cloud_size_c = 1024;
	//const int cloud_size_c = 256;
	// cloud_scale_c: larger number means smaller scale!
	const float cloud_scale_c = 8.0f;
	const int n_iterations = 8;
	//const int n_iterations = 4;
	//const int n_iterations = 1;
	//const V_NOISEMODE_t noisemode = V_NOISEMODE_CLOUDS;
	const V_NOISEMODE_t noisemode = V_NOISEMODE_PERLIN;

	float scale = cloud_scale_c;
	if( data->index == 1 )
		scale *= 0.55f;
	data->noise = Image2D::createNoise(cloud_size_c, cloud_size_c, scale, scale, filter_max, filter_min, noisemode, n_iterations, true);
}

Clouds::Clouds() :
dir0(1.0f, 0.0f, 0.0f), dir1(0.8f, 0.0f, 0.6f),
	min_brightness(0.5f), density(0.2f)
{
	dir0 *= 0.003f;
	dir1 *= 0.002f;
	for(int i=0;i<3;i++) {
		this->col[i] = 255;
	}

	// create cloud textures, with SMP

	const int n_threads_c = 2;
	createNoise_data thread_data[n_threads_c];
	for(int i=0;i<n_threads_c;i++) {
		thread_data[i].index = i;
		thread_data[i].noise = NULL;
	}

	VI_ThreadFunction *funcs[n_threads_c];
	void *data[n_threads_c];
	for(int i=0;i<n_threads_c;i++) {
		funcs[i] = createNoise_thread;
		data[i] = &thread_data[i];
	}
	LOG("create cloud textures...\n");
	createThreads(n_threads_c, funcs, data);
	LOG("done!\n");

	//thread_data[0].noise->save("cloud0.png", "png");
	//thread_data[1].noise->save("cloud1.png", "png");

	this->cloud_texture0 = GraphicsEnvironment::getSingleton()->getRenderer()->createTexture(thread_data[0].noise);
	//this->cloud_texture0->owned = true;
	this->addChild(this->cloud_texture0);
	this->cloud_texture1 = GraphicsEnvironment::getSingleton()->getRenderer()->createTexture(thread_data[1].noise);
	//this->cloud_texture1->owned = true;
	this->addChild(this->cloud_texture1);

	this->cloud_texture0->setWrapMode(VI_Texture::WRAPMODE_MIRROR);
	this->cloud_texture1->setWrapMode(VI_Texture::WRAPMODE_MIRROR);
}

Clouds::~Clouds() {
	/*if( this->cloud_texture0 != NULL ) {
		delete this->cloud_texture0;
	}
	if( this->cloud_texture1 != NULL ) {
		delete this->cloud_texture1;
	}*/
}

void Clouds::render(Graphics3D *g) {
	//g->getRenderer()->enableFixedFunctionLighting(false);
	//g->getRenderer()->enableTexturing(false);
	g->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_NONE);
	const float h = 1.0f;
	//const float d = 1000.0;
	float d = g->getRenderer()->getZFar();
	//const float d = 1.0;
	float vertex_data[] = {
		-d, h, d,
		-d, h, -d,
		d, h, -d,
		d, h, d
	};

	const ShaderEffect *effect = g->getCloudsShader();
	effect->enable(g->getRenderer());

	//effect->setMatrices();
	effect->SetModelViewProjMatrix("mx.ModelViewProj");

	effect->SetUniformParameter1f("time", ((float)Vision::getGameTimeMS())/1000.0f);
	effect->SetUniformParameter1f("min_brightness", min_brightness);
	effect->SetUniformParameter1f("density", density);

	effect->SetUniformParameter2f("dir0", dir0.x, dir0.z);
	effect->SetUniformParameter2f("dir1", dir1.x, dir1.z);

	// need to pass colors as floats in range [0, 1]
	effect->SetUniformParameter3f("cloud_color", ((float)col[0])/255.0f, ((float)col[1])/255.0f, ((float)col[2])/255.0f);

	cloud_texture0->enable();
	g->getRenderer()->activeTextureUnit(1);
	cloud_texture1->enable();
	g->getRenderer()->activeTextureUnit(0);
	
	//cloud_texture0->enable();
	/*float w1 = 1.0f/texture->getWidth();
	float h1 = 1.0f/texture->getHeight();
	float w2 = 1.0f - w1;
	float h2 = 1.0f - h1;
	float texcoord_data[] = {
		w1, h1,
		w2, h1,
		w2, h2,
		w1, h2
	};*/
	//const float scale_c = 0.25;
	//const float scale_c = 1.0/5.0;
	//const float scale_c = 1.0;
	//float td = 127.0;
	//float td = 1.0;
	//float td = d / 2.0f;
	float td = d / 8.0f;
	//float td = d / 16.0f;
	float texcoord_data[] = {
		0, 0,
		0, td,
		td, td,
		td, 0
	};

	//unsigned char color_data[3] = { 0, 0, 255 };
	unsigned char color_data[3] = { 255, 255, 255 };

	//g->getRenderer()->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, NULL, 4, color_data, false, 4);
	g->getRenderer()->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4, color_data, false, 4);
	
	g->getRenderer()->setDepthBufferMode(Renderer::RENDERSTATE_DEPTH_READWRITE);
	//g->getRenderer()->enableFixedFunctionLighting(true);
	//g->getRenderer()->enableTexturing(true);
	/*const Shader *c_vertex_shader = g->getRenderer()->getActiveVertexShader();
	const Shader *c_pixel_shader = g->getRenderer()->getActivePixelShader();
	if( c_vertex_shader != NULL )
		c_vertex_shader->disable();
	if( c_pixel_shader != NULL )
		c_pixel_shader->disable();*/
	g->getRenderer()->disableShaders();
}


TerrainEngine::TerrainEngine(Graphics3D *g3,float xscale,float zscale,float hscale) : VisionObject(), pretend_reflection_texture(NULL), waves_bump_texture(NULL) {
	this->g3 = g3;
	this->recalc = true;
	lod = true;
	//lod = false;
	width = 0;
	depth = 0;
	data = NULL;
	texture_map = NULL;
	//lighting = LIGHTING_NONE;
	//lighting_enabled = false;
	lighting_enabled = true;
	//color_enabled = false;
	colormode = V_COLORMODE_NONE;
	//lightmap = NULL;
	for(int i=0;i<N_TERRAIN_TEXTURES;i++)
		lightmap[i] = NULL;
	visible = NULL;
	for(int i=0;i<N_TERRAIN_TEXTURES;i++)
		textures[i] = NULL;
	sea_texture = NULL;
	//entities = NULL;
	//meshes = NULL;
	//entity_sea = NULL;
	entity_sea_dummy = NULL;
	//mesh_sea = NULL;
	quadtree = NULL;
	this->splat_sort_textures = true;
	//pixel_shader_ambient = NULL;
	shader_ambient = NULL;
	this->nodes_rendered = 0;

	this->sea = false;
	this->sea_effects = false;
	this->unscaled_sea_height = 150.0;
	this->sea_extent = 200.0;

	this->xscale = xscale;
	this->zscale = zscale;
	this->hscale = hscale;

	/*this->pixel_shader = NULL;
	if( g3->getGraphicsEnvironment()->cgContext != NULL )
	this->pixel_shader = Shader::createShader(g3, "shaders/simple.cg", "main_water_ps", false);*/

	/*sea = false;
	sea_level = 0.0;
	sea_texture = NULL;
	wave_level = 0.0;
	wave_rising = true;
	tex_coord = 0.0;
	wave_amp = 0.2;
	wave_sp = 0.00006;
	tex_sp = 0.00002;
	tex_scale = 1.0;

	min_height = 0;
	mid_height = 0;
	max_height = 0;

	bright_off = 0.0;
	bright_sp = 0.0;

	bright_min = 0.0;
	bright_max = 1.0;*/

	pretend_reflection_min_rgb[0] = 38;
	pretend_reflection_min_rgb[1] = 59;
	pretend_reflection_min_rgb[2] = 96;
	pretend_reflection_max_rgb[0] = 65;
	pretend_reflection_max_rgb[1] = 96;
	pretend_reflection_max_rgb[2] = 140;
}

TerrainEngine::~TerrainEngine() {
	free();
	for(int i=0;i<N_TERRAIN_TEXTURES;i++) {
		if( this->lightmap[i] != NULL )
			delete [] this->lightmap[i];
	}
	//delete this->pretend_reflection_texture;
	/*if(texture != NULL)
	delete texture;
	if(sea_texture != NULL)
	delete sea_texture;*/
	/*if(mesh != NULL)
	delete mesh;*/
	/*if(meshes != NULL) {
	for(int i=0;i<n_chunks_x*n_chunks_z;i++)
	if(meshes[i] != NULL)
	delete meshes[i];
	delete [] meshes;
	}*/
	/*if(entities != NULL) {
	for(int i=0;i<n_chunks_x*n_chunks_z;i++)
	if(entities[i] != NULL) {
	delete entities[i]->getObject();
	delete entities[i];
	}
	delete [] entities;
	}*/
	/*if(mesh_sea != NULL) {
	delete mesh_sea;
	}*/
	/*if(entity_sea != NULL) {
		delete entity_sea->getObject();
		delete entity_sea;
	}*/
	/*if(entity_sea_dummy != NULL) {
		delete entity_sea_dummy->getObject();
		delete entity_sea_dummy;
	}*/
	if(quadtree != NULL)
		delete quadtree;
}

void TerrainEngine::free() {
	delete [] data;
	data = NULL;
	delete [] texture_map;
	texture_map = NULL;
	/*delete [] lightmap;
	lightmap = NULL;*/
	delete [] visible;
	visible = NULL;
	this->width = this->depth = 0;
	/*for(size_t i=0;i<this->garbage_objects.size();i++) {
		delete this->garbage_objects.at(i);
	}
	this->garbage_objects.clear();*/
	this->deleteAllOwnedObjects();
	this->entity_sea_dummy = NULL;
	this->pretend_reflection_texture = NULL;
	this->waves_bump_texture = NULL;
}

bool TerrainEngine::loadMap(const char *filename) {
	this->recalc = true;
	Image2D *image = Image2D::loadImage(filename);
	if(image == NULL)
		return false;

	free();
	this->width = image->getWidth();
	this->depth = image->getHeight();
	int size = this->width * this->depth;
	this->data = new unsigned char[size];
	this->texture_map = new unsigned char[(width-1)*(depth-1)];
	//this->lightmap = new Color[size];
	this->visible = new bool[(width-1)*(depth-1)];
	rgb_struct *rgb = (rgb_struct *)image->getData();
	for(int i=0;i<size;i++) {
		this->data[i] = rgb[i].r;
	}
	for(int i=0;i<(width-1)*(depth-1);i++) {
		this->texture_map[i] = 0;
		this->visible[i] = true;
	}
	//initLightMap(1.0, 1.0, 1.0);
	//Init();
	if( !Vision::isLocked() ) {
		prepare();
	}

	delete image;
	return true;
}

void TerrainEngine::create(int width,int depth) {
	{
		//Vision::lock();
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		this->recalc = true;
		free();
		this->width = width;
		this->depth = depth;
		int size = this->width * this->depth;
		this->data = new unsigned char[size];
		this->texture_map = new unsigned char[(width-1)*(depth-1)];
		//this->lightmap = new Color[size];
		this->visible = new bool[(width-1)*(depth-1)];
		for(int i=0;i<size;i++) {
			this->data[i] = 0;
		}
		for(int i=0;i<(width-1)*(depth-1);i++) {
			this->texture_map[i] = 0;
			this->visible[i] = true;
		}
		//initLightMap(1.0, 1.0, 1.0);
		//Init();
		//Vision::unlock(false);
	}
	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::createRandomSubdivide(int off_x,int off_z,int size_x,int size_z,int min_height,int max_height,float roughness) {
	ASSERT( size_x == size_z );
	ASSERT( size_x >= 2 );
	ASSERT( VI_isPower(size_x-1, 2) );
	if( size_x == 2 )
		return;
	int half_size = (size_x-1)/2 + 1;
	int half_x = off_x + half_size - 1;
	int half_z = off_z + half_size - 1;
	int end_x = off_x + size_x - 1;
	int end_z = off_z + size_z - 1;
	int heights[4]; // cast to int to avoid overflow
	heights[0] = this->get(off_x, off_z);
	heights[1] = this->get(end_x, off_z);
	heights[2] = this->get(off_x, end_z);
	heights[3] = this->get(end_x, end_z);
	//LOG("### %d , %d -> %d , %d\n", off_x, off_z, end_x, end_z);
	//LOG("heights %d %d %d %d\n", heights[0], heights[1], heights[2], heights[3]);

	this->set(half_x, off_z, (int)(0.5*(heights[0] + heights[1])));
	this->set(off_x, half_z, (int)(0.5*(heights[0] + heights[2])));
	this->set(end_x, half_z, (int)(0.5*(heights[1] + heights[3])));
	this->set(half_x, end_z, (int)(0.5*(heights[2] + heights[3])));
	/*LOG("set %d, %d = %d\n", half_x, off_z, this->get(half_x, off_z));
	LOG("set %d, %d = %d\n", off_x, half_z, this->get(off_x, half_z));
	LOG("set %d, %d = %d\n", end_x, half_z, this->get(end_x, half_z));
	LOG("set %d, %d = %d\n", half_x, end_z, this->get(half_x, end_z));*/

	int middle = (int)(0.25*(heights[0] + heights[1] + heights[2] + heights[3]));
	int error_mag = (int)(roughness * (size_x-2));
	int error = (rand() % (2*error_mag-1)) - error_mag + 1;
	middle += error;
	if( middle < min_height )
		middle = min_height;
	else if( middle > max_height )
		middle = max_height;

	this->set(half_x, half_z, middle);
	//LOG("set middle %d, %d = %d\n", half_x, half_z, middle);

	createRandomSubdivide(off_x, off_z, half_size, half_size, min_height, max_height, roughness);
	createRandomSubdivide(half_x, off_z, half_size, half_size, min_height, max_height, roughness);
	createRandomSubdivide(off_x, half_z, half_size, half_size, min_height, max_height, roughness);
	createRandomSubdivide(half_x, half_z, half_size, half_size, min_height, max_height, roughness);
}

void TerrainEngine::createRandom(int width,int depth,const int base_heights[4],int min_height,int max_height,float roughness,int seed) {
	{
		//Vision::lock();
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope
		ASSERT( VI_isPower(width-1, 2) );
		ASSERT( VI_isPower(depth-1, 2) );

		this->create(width, depth);
		this->set(0, 0, base_heights[0]);
		this->set(width-1, 0, base_heights[1]);
		this->set(0, depth-1, base_heights[2]);
		this->set(width-1, depth-1, base_heights[3]);
		this->createRandomSubdivide(0, 0, width, depth, min_height, max_height, roughness);

		//Vision::unlock(false);
	}
	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::prepare() {
	if( !this->recalc )
		return;

	//LOG("prepare %d\n", this->tag);
	//VI_check();
	//VI_debug_mem_usage_all();
	GraphicsEnvironment::getSingleton()->checkError("TerrainEngine::prepare enter");
	/*if(entity_sea != NULL) {
		delete entity_sea->getObject();
		delete entity_sea;
		entity_sea = NULL;
	}*/
	/*if(entity_sea_dummy != NULL) {
		delete entity_sea_dummy->getObject();
		delete entity_sea_dummy;
		entity_sea_dummy = NULL;
	}*/
	if(quadtree != NULL)
		delete quadtree;
	//int time_s = clock();
	/*for(size_t i=0;i<this->garbage_objects.size();i++) {
		delete this->garbage_objects.at(i);
	}
	this->garbage_objects.clear();*/
	this->deleteAllOwnedObjects();
	this->entity_sea_dummy = NULL;
	this->pretend_reflection_texture = NULL;
	this->waves_bump_texture = NULL;
	//LOG("### %d\n", clock() - time_s);
	/*if( this->pretend_reflection_texture != NULL ) {
		delete this->pretend_reflection_texture;
		this->pretend_reflection_texture = NULL;
	}*/

	if( this->sea && !this->sea_effects ) {
		LOG("create reflection texture\n");
		Image2D *image = Image2D::createNoise(64, 64, 1.0f, 1.0f, pretend_reflection_max_rgb, pretend_reflection_min_rgb, V_NOISEMODE_PERLIN, 1, false);
		this->pretend_reflection_texture = GraphicsEnvironment::getSingleton()->getRenderer()->createTexture(image);
		//this->pretend_reflection_texture->owned = true;
		this->pretend_reflection_texture->setWrapMode(VI_Texture::WRAPMODE_MIRROR);
		this->addChild(this->pretend_reflection_texture);
	}
	if( this->sea ) {
		LOG("create waves texture\n");
		unsigned char filter_max[3] = {255, 255, 255};
		unsigned char filter_min[3] = {0, 0, 0};
		const int waves_size_c = 512;
		//const int waves_size_c = 256;
		// wave_scale_c: larger number means smaller scale!
		const float wave_scale_c = 8.0f;
		const int n_iterations = 8;
		//const V_NOISEMODE_t noisemode = V_NOISEMODE_CLOUDS;
		const V_NOISEMODE_t noisemode = V_NOISEMODE_PERLIN;
		Image2D *waves_image = Image2D::createNoise(waves_size_c, waves_size_c, wave_scale_c, wave_scale_c, filter_max, filter_min, noisemode, n_iterations, true);
		Image2D *waves_bump_image = static_cast<Image2D *>(waves_image->createDerivative(4.0f));
		this->waves_bump_texture = g3->getRenderer()->createTexture(waves_bump_image);
		this->waves_bump_texture->setWrapMode(VI_Texture::WRAPMODE_MIRROR);
		delete waves_image;
		this->addChild(waves_bump_texture);
	}

	int levels = 1;
	int quadtree_size = 0;
	{
		quadtree_size = SECTION_SIZE;
		int max = width > depth ? width : depth;
		while( quadtree_size < max-1 ) {
			quadtree_size *= 2;
			levels++;
		}
	}
	quadtree = new Quadtree(0.0, 0.0, 0.0, quadtree_size*xscale, 255*hscale, quadtree_size*zscale, levels);

	//int n_x = (width+SECTION_SIZE-2)/SECTION_SIZE; // divide and round up
	//int n_z = (height+SECTION_SIZE-2)/SECTION_SIZE; // divide and round up
	n_chunks_x = quadtree_size/SECTION_SIZE;
	n_chunks_z = n_chunks_x;

	if( this->sea && this->sea_effects && g3->getGraphicsEnvironment()->hasShaders() && g3->hasWaterShaders() ) {
		Plane plane;
		plane.normal.set(0.0f, 1.0f, 0.0f);
		plane.D = unscaled_sea_height*hscale + offset.y;
		//ShadowPlane *sp = new ShadowPlane(&plane, false, false, true);
		ShadowPlane *sp = new ShadowPlane(&plane, false, true, true);

		Mesh *mesh = new Mesh();
		mesh->setName("Terrain Sea");
		//mesh->owned = true;
		this->addChild(mesh);
		this->addChild(sp);
		int texture_size = g3->getRenderToTextureSize();
		/*ObjectDataShaders shaders(
			g3->getWaterAmbientVertexShader(), g3->getWaterAmbientPixelShader(),
			g3->getWaterDirectionalVertexShader(), g3->getWaterDirectionalPixelShader(),
			g3->getWaterPointVertexShader(), g3->getWaterPointPixelShader()
			);*/
		ObjectDataShaders shaders(
			g3->getWaterAmbientShader(),
			g3->getWaterDirectionalShader(),
			g3->getWaterPointShader()
			);
		mesh->setShadowPlane(g3->getRenderer(), sp, &shaders, texture_size);
		//this->entity_sea_dummy = new Entity(mesh);
		this->entity_sea_dummy = new SceneGraphNode();
		this->entity_sea_dummy->setObject(mesh);
		//this->entity_sea_dummy->owned = true;
		this->addChild(this->entity_sea_dummy);
	}

	//GraphicsEnvironment::checkGLError("TerrainEngine::prepare buildQuadtreenodeMesh");
	buildQuadtreenodeMesh(this->quadtree->quadtreenodes);

	this->recalc = false;
	GraphicsEnvironment::getSingleton()->checkError("TerrainEngine::prepare exit");
	//LOG("done preparing %d\n", this->tag);
	//VI_check();
	//VI_debug_mem_usage_all();
}

void TerrainEngine::buildQuadtreenodeMesh(Quadtreenode *quadtreenode) {
	//int time_s = 0;
	//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh enter");
	if( lod || quadtreenode->level == this->quadtree->levels-1 )
	{
		//LOG(">>>\n");
		//VI_check();
		//int rd_index = quadtreenode->index_z * n_chunks_x + quadtreenode->index_x;
		int res_index = this->quadtree->levels - quadtreenode->level - 1;
		int res = 1;
		for(int i=0;i<res_index;i++)
			res *= 2;
		//this->meshes[rd_index] = NULL;
		//this->entities[rd_index] = NULL;
		int m_x = quadtreenode->index_x;
		int m_z = quadtreenode->index_z;

		if( m_x*SECTION_SIZE*res >= width-1 )
			return;
		if( m_z*SECTION_SIZE*res >= depth-1 )
			return;
		int this_width = SECTION_SIZE+1;
		int this_height = SECTION_SIZE+1;
		if( (m_x+1)*SECTION_SIZE*res >= width-1 )
			this_width = width/res - m_x * SECTION_SIZE;
		if( (m_z+1)*SECTION_SIZE*res >= depth-1 )
			this_height = depth/res - m_z * SECTION_SIZE;
		float all_min_b_h = 255; // only looks at boundary height
		float all_min_h = 255;
		float all_max_h = this->sea ? unscaled_sea_height : 0;
		bool same_height = false;

		bool any_visible = false;
		for(int tz=0;tz<this_height-1 && !any_visible;tz++) {
			for(int tx=0;tx<this_width-1 && !any_visible;tx++) {
				int x = m_x*SECTION_SIZE + tx;
				int z = m_z*SECTION_SIZE + tz;
				x *= res; z *= res;
				any_visible = isVisible(x,z);
			}
		}
		// land
		if( any_visible )
		{
			//bool splatting = g3->getSplatAmbientPixelShader() != NULL && g3->getSplatDirectionalPixelShader() != NULL && g3->getSplatPointPixelShader() != NULL
			bool splatting = g3->hasSplatShaders();
				//&& unique_textures->size() > 1 // doesn't seem to help performance?
				;
			//splatting = true;
			//splatting = false;
			//LOG("texture splatting? %d\n", splatting);
			//float splat_detail_scale = res*SECTION_SIZE/8.0f;
			float splat_detail_scale = (float)(res*SECTION_SIZE);

			// For splatting, need to include adjacent squares to this section, when seeing what textures are there.
			// When not splatting, we shouldn't - otherwise risk having renderdatas with 0 polygons.
			int extra = splatting ? 1 : 0;

			Mesh *mesh = new Mesh();
			mesh->setName("Terrain Chunk");
			//mesh->owned = true;
			mesh->n_frames = 1;

			// how many textures?
			//vector<unsigned char> *unique_textures = new vector<unsigned char>();
			std::set<unsigned char> *unique_textures = new std::set<unsigned char>();
			int texture_counts[N_TERRAIN_TEXTURES];
			for(int i=0;i<N_TERRAIN_TEXTURES;i++)
				texture_counts[i] = 0;
			/*for(int tz=0;tz<this_height-1;tz++) {
				for(int tx=0;tx<this_width-1;tx++) {*/
			for(int tz=-extra;tz<this_height-1+extra;tz++) {
				for(int tx=-extra;tx<this_width-1+extra;tx++) {
			/*for(int tz=-1;tz<this_height;tz++) {
				for(int tx=-1;tx<this_width;tx++) {*/
					int x = m_x*SECTION_SIZE + tx;
					int z = m_z*SECTION_SIZE + tz;
					x *= res; z *= res;
					if( x < 0 || x >= width-1 || z < 0 || z >= depth-1 )
						continue;
					if( !splatting && !isVisible(x,z) ) {
						// If we're not splatting, we need only include a texture if we're drawing a square with it on (if we are splatting, it may still blended into adjacent squares.
						// This is important if not splatting, otherwise we can end up with mesh renderdatas that have no polygons! Not a problem if we are splatting, as we draw the entire section as one renderdata, which must have at least one visible square (otherwise we wouldn't be here).
						continue;
					}
					unsigned char texture = getTextureMap(x,z);
					unique_textures->insert(texture);
					texture_counts[texture]++;
				}
			}
			if( splatting && unique_textures->size() > MAX_SPLATS ) {
				Vision::setError(new VisionException(this, VisionException::V_TERRAIN_TOO_MANY_TEXTURE_SPLATS, "Too many textures in terrain section for splatting"));
			}
			unsigned char most_common_texture = -1;
			if( splat_sort_textures ) {
				int most_common_texture_count = 0;
				for(int i=0;i<N_TERRAIN_TEXTURES;i++) {
					if( i == 0 || texture_counts[i] > most_common_texture_count ) {
						most_common_texture = i;
						most_common_texture_count = texture_counts[i];
					}
				}
			}
			else {
				most_common_texture = 0;
			}

			//mesh->n_renderDatas = splatting ? 1 : unique_textures->size();
			//mesh->renderDatas = new RenderData[ mesh->n_renderDatas ];
			mesh->renderDatas.resize( splatting ? 1 : unique_textures->size() );

			same_height = res > 1 && mesh->getNRenderData() == 1;
			//same_height = true;
			//same_height = false;
			unsigned char same_height_value = 0;
			for(int stz=0;stz<this_height*res && same_height;stz++) {
				for(int stx=0;stx<this_width*res && same_height;stx++) {
					int x = m_x*SECTION_SIZE*res + stx;
					int z = m_z*SECTION_SIZE*res + stz;
					unsigned char y = get(x,z);
					if( stz == 0 && stx == 0 ) {
						same_height_value = y;
					}
					else if( same_height_value != y ) {
						same_height = false;
					}
				}
			}

			int mesh_width = this_width;
			int mesh_height = this_height;
			int mesh_scale_x = 1;
			int mesh_scale_z = 1;
			//LOG("same height? %d\n", same_height);
			if( same_height ) {
				// performance speed up
				mesh_scale_x = this_width-1;
				mesh_scale_z = this_height-1;
				mesh_height = 2;
				mesh_width = 2;
			}
			int mesh_max_size = max(mesh_width, mesh_height);

			//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh a");
			std::set<unsigned char>::iterator iter = unique_textures->begin();
			for(size_t index=0;index<mesh->getNRenderData();index++,++iter) {
				//unsigned char texture_index = splatting ? most_common_texture : unique_textures->at(index);
				unsigned char texture_index = splatting ? most_common_texture : *iter;
				//unsigned char colormap_index = colormode == V_COLORMODE_PER_TEXTURE ? texture_index : 0;
				unsigned char colormap_index = 0;

				//RenderData *rd = &mesh->renderDatas[index];
				mesh->renderDatas[index] = new RenderData();
				RenderData *rd = mesh->renderDatas[index];
				int n_points = mesh_width*mesh_height;
				//const bool alpha = true;
				//const bool alpha = false;
				bool want_colors = this->colormode != V_COLORMODE_NONE;
				/*if( splatting )
				want_colors = false; // TODO:*/
				//want_colors = false; // so is consistent with splatting, which doesn't support colors
				bool want_normals = this->lighting_enabled;
				//want_normals = false;
				//bool want_alphamap = index != most_common_texture;
				//bool want_alphamap = index > 0;
				Texture *texture = textures[texture_index]; // if splatting, this is the base texture
				//texture = NULL;
				bool want_texcoords = texture != NULL;
				//bool want_alphamap = false;
				int n_vertices = n_points;
				//LOG("%d : %d : %d\n", index, texture_index, rd->n_vertices);
				if( lod )
					n_vertices += 2*mesh_width + 2*(mesh_height-1);
				//rd->initVertexArrays(1, rd->n_vertices, want_colors, alpha, want_normals, texture!=NULL, want_alphamap);
				//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh initVertexArrays");
				//rd->initVertexArrays(1, n_vertices, want_colors, /*alpha,*/ want_normals, want_texcoords);
				rd->initVertexArrays(1, n_vertices, want_normals);
				//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh initVertexArrays done");
				//rd->textureID = texture == NULL ? 0 : texture->textureID;
				//rd->texture = texture;
				rd->setSolid(true);
				/*rd->cols[0] = 255;
				rd->cols[1] = 255;
				rd->cols[2] = 255;
				rd->cols[3] = 255;*/
				/*rd->material_specular[0] = 0.0;
				rd->material_specular[1] = 0.0;
				rd->material_specular[2] = 0.0;
				rd->material_specular[3] = 0.0;*/
				float material_specular[4] = {0.0f, 0.0f, 0.0f, 0.0f};
				rd->setMaterialSpecular(material_specular);

				/*if( want_alphamap ) {
					Image2D *alphamap = new Image2D(this_width, this_height);
					//rgb_struct *rgb = (rgb_struct *)alphamap->rgbdata;
					for(int z=0;z<this_height;z++) {
						for(int x=0;x<this_width;x++) {
							alphamap->putRGB(x, z, 127, 0, 0);
						}
					}
					//rd->secondary_texture = new Texture(alphamap);
					rd->secondary_texture = g3->getRenderer()->createTexture(alphamap);
				}*/
				if( splatting ) {
					//LOG("### do splatting\n");
					//rd->n_splats = unique_textures->size();
					/*rd->n_splats = MAX_SPLATS;
					for(int c=1;c<MAX_SPLATS;c++) {
						rd->splat_textures[c] = texture; // all textures should be set to something, even if not used, to avoid OpenGL error when rendering
					}*/
					rd->setNTextures(MAX_SPLATS+1);
					rd->setTexture(0, texture);
					for(int c=1;c<MAX_SPLATS;c++) {
						rd->setTexture(c+1, texture); // all textures should be set to something, even if not used, to avoid OpenGL error when rendering
					}
					// texture 1 is the alphamap, set later
					
					int c=1;
					for(std::set<unsigned char>::iterator iter = unique_textures->begin();iter != unique_textures->end();++iter) {
						//unsigned char splat_texture_index = unique_textures->at(i);
						unsigned char splat_texture_index = *iter;
						if( splat_texture_index == texture_index )
							continue;
						//rd->splat_textures[c] = textures[splat_texture_index];
						rd->setTexture(c+1, textures[splat_texture_index]);
						c++;
					}

					//Image2D *alphamap = new Image2D(this_width-1, this_height-1);
					// fix for non-power-of-two textures
					// TODO: does this work okay where we go beyond the values of this_width-1 or this_height-1?
					/*int texture_w = getNextPower(this_width-1, 2);
					int texture_h = getNextPower(this_height-1, 2);
					Image2D *alphamap = new Image2D(texture_w, texture_h);*/
					//Image2D *alphamap = new Image2D(SECTION_SIZE, SECTION_SIZE);
					//time_s = clock();
					Image2D *alphamap = new Image2D(SECTION_SIZE+2*extra, SECTION_SIZE+2*extra);
					//rgb_struct *rgb = (rgb_struct *)alphamap->rgbdata;
					/*for(int tz=0;tz<this_height-1;tz++) {
						for(int tx=0;tx<this_width-1;tx++) {*/
					for(int tz=-extra;tz<this_height-1+extra;tz++) {
						for(int tx=-extra;tx<this_width-1+extra;tx++) {
							int x = m_x*SECTION_SIZE + tx;
							int z = m_z*SECTION_SIZE + tz;
							x *= res; z *= res;
							if( x < 0 || x >= width-1 || z < 0 || z >= depth-1 )
								continue;
							//rgb_struct rgb = {0, 0, 0};
							unsigned char rgb[3] = {0, 0, 0};
							unsigned char texture_id = getTextureMap(x,z);
							Texture *this_texture = textures[texture_id];
							bool done = texture == this_texture; // already done if base texture
							for(size_t i=1;i<unique_textures->size() && !done;i++) {
								//if( rd->splat_textures[i] == this_texture ) {
								if( rd->getTexture(i+1) == this_texture ) {
									if( i == 1 )
										rgb[0] = 255;
									else if( i == 2 )
										rgb[1] = 255;
									else if( i == 3 )
										rgb[2] = 255;
									done = true;
								}
							}
							// n.b., done may be false if splat_sort_textures is false, as it means we can have sections where the base texture 0 is not present
							//alphamap->put(tx, tz, &rgb);
							//alphamap->putRGB(tx, tz, rgb[0], rgb[1], rgb[2]);
							alphamap->putRGB(tx+extra, tz+extra, rgb[0], rgb[1], rgb[2]);
						}
					}
					//LOG("### alphamap %d\n", clock() - time_s);
					//time_s = clock();
					alphamap->makePowerOf2();
					//LOG("### make power of 2 %d\n", clock() - time_s);
					/*Image2D *alphamap = new Image2D(32, 32);
					for(int tz=0;tz<32;tz++) {
					for(int tx=0;tx<32;tx++) {
					rgb_struct rgb = {0,0,0};
					alphamap->put(tx, tz, rgb);
					}
					}*/
					//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh new alphamap texture");
					//rd->splat_textures[0] = new Texture(alphamap);
					//time_s = clock();
					/*rd->splat_textures[0] = g3->getRenderer()->createTexture(alphamap);
					rd->splat_textures[0]->setWrapMode(VI_Texture::WRAPMODE_CLAMP); // alpha map shouldn't wrap!*/
					Texture *alphamap_texture = g3->getRenderer()->createTexture(alphamap);
					alphamap_texture->setWrapMode(VI_Texture::WRAPMODE_CLAMP); // alpha map shouldn't wrap!*/
					//rd->splat_textures[0] = alphamap_texture;
					rd->setTexture(1, alphamap_texture);
					//LOG("### splat texture %d\n", clock() - time_s);
					//rd->splat_textures[0]->owned = true;
					//this->garbage_objects.push_back(rd->splat_textures[0]);
					//this->addChild(rd->splat_textures[0]);
					this->addChild(alphamap_texture);

					/*const float splat_detail_scale = 100.0f;
					if( g3->getSplatAmbientPixelShader() != NULL ) {
						g3->getSplatAmbientPixelShader()->SetUniformParameter1f("splat_detail_scale", splat_detail_scale);
					}
					if( g3->getSplatPointPixelShader() != NULL ) {
						g3->getSplatPointPixelShader()->SetUniformParameter1f("splat_detail_scale", splat_detail_scale);
					}
					if( g3->getSplatDirectionalPixelShader() != NULL ) {
						g3->getSplatDirectionalPixelShader()->SetUniformParameter1f("splat_detail_scale", splat_detail_scale);
					}*/

					/*mesh->shaders.pixel_shader_ambient = g3->getSplatAmbientPixelShader();
					mesh->shaders.pixel_shader_point = g3->getSplatPointPixelShader();
					mesh->shaders.pixel_shader_directional = g3->getSplatDirectionalPixelShader();*/
					mesh->shaders.shader_ambient = g3->getSplatAmbientShader();
					mesh->shaders.shader_directional = g3->getSplatDirectionalShader();
					mesh->shaders.shader_point = g3->getSplatPointShader();

					/*if( this->pixel_shader_ambient != NULL ) {
						//LOG("override with user supplied shader\n");
						//mesh->shaders.pixel_shader_ambient = this->pixel_shader_ambient;
						//mesh->shaders.shader_ambient->pixel_shader = this->pixel_shader_ambient;
						// TODO: fix
						//mesh->shaders.shader_ambient = new ShaderEffect(mesh->shaders.shader_ambient->getVertexShader(), this->pixel_shader_ambient);
					}*/
					if( this->shader_ambient != NULL ) {
						mesh->shaders.shader_ambient = this->shader_ambient;
					}
				}
				else {
					rd->setNTextures(1);
					rd->setTexture(0, texture);
				}

				//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh do vertices");
				//int min[4] = {255,255,255,255};
				int min_b_h = 255;
				for(int tz=0;tz<mesh_height;tz++) {
					for(int tx=0;tx<mesh_width;tx++) {
						int x = m_x*SECTION_SIZE + tx*mesh_scale_x;
						int z = m_z*SECTION_SIZE + tz*mesh_scale_z;
						x *= res; z *= res;
						unsigned char y = get(x,z);
						if( tz == 0 || tz == mesh_height-1 || tx == 0 || tx == mesh_width-1 ) {
							if( y < min_b_h )
								min_b_h = y;
						}
						if( y < all_min_h )
							all_min_h = y;
						if( y > all_max_h )
							all_max_h = y;
					}
				}
				if( min_b_h < all_min_b_h )
					all_min_b_h = (float)min_b_h;
				//time_s = clock();
				for(int tz=0;tz<mesh_height;tz++) {
					for(int tx=0;tx<mesh_width;tx++) {
						int x = m_x*SECTION_SIZE + tx*mesh_scale_x;
						int z = m_z*SECTION_SIZE + tz*mesh_scale_z;
						x *= res; z *= res;
						unsigned char y = get(x,z);
						float wp_x = x*xscale;
						//float wp_y = y*hscale;
						float wp_z = z*zscale;
						float p_x = tx*mesh_scale_x*xscale*res;
						float p_y = y*hscale;
						float p_z = tz*mesh_scale_z*zscale*res;
						//p_x += m_x*SECTION_SIZE*xscale*res;
						//p_z += m_z*SECTION_SIZE*zscale*res;
						//rd->va_vertices[0] = 0.0;
						/*rd->va_vertices[3*mesh_width*tz + 3*tx + 0] = p_x;
						rd->va_vertices[3*mesh_width*tz + 3*tx + 1] = p_y;
						rd->va_vertices[3*mesh_width*tz + 3*tx + 2] = p_z;*/
						rd->setVertex(0, mesh_width*tz + tx, Vector3D(p_x, p_y, p_z));

						if( want_normals ) {
							Vector3D norm = getNormalAt(wp_x, wp_z);
							//norm.set(0,1,0);
							/*rd->va_normals[3*mesh_width*tz + 3*tx + 0] = norm.x;
							rd->va_normals[3*mesh_width*tz + 3*tx + 1] = norm.y;
							rd->va_normals[3*mesh_width*tz + 3*tx + 2] = norm.z;*/
							rd->setNormal(0, mesh_width*tz + tx, norm);
						}
						if( want_colors ) {
							Color col = getLightmap(colormap_index,x,z);
							/*int step = alpha ? 4 : 3;
							rd->va_colors[step*mesh_width*tz + step*tx + 0] = col.getR();
							rd->va_colors[step*mesh_width*tz + step*tx + 1] = col.getG();
							rd->va_colors[step*mesh_width*tz + step*tx + 2] = col.getB();
							if( alpha )
								rd->va_colors[step*mesh_width*tz + step*tx + 3] = 255;*/
							rd->setColor(mesh_width*tz + tx, 0, col.getR());
							rd->setColor(mesh_width*tz + tx, 1, col.getG());
							rd->setColor(mesh_width*tz + tx, 2, col.getB());
							if( rd->hasAlphaChannel() )
								rd->setColor(mesh_width*tz + tx, 3, 255);
						}
						// skirts
						if( lod && ( tz == 0 || tz == mesh_height-1 || tx == 0 || tx == mesh_width-1 ) ) {
							int idx = n_points;
							if( tz == 0 )
								idx += tx;
							else if( tz == mesh_height-1 )
								idx += mesh_width + tx;
							else if( tx == 0 )
								idx += 2*mesh_width + tz-1;
							else
								idx += 2*mesh_width + mesh_height-1 + tz-1;
							/*rd->va_vertices[3*idx + 0] = p_x;
							//rd->va_vertices[3*idx + 1] = (min_h-1)*hscale;
							rd->va_vertices[3*idx + 1] = all_min_b_h*hscale;
							//rd->va_vertices[3*idx + 1] = 0.0;
							rd->va_vertices[3*idx + 2] = p_z;*/
							rd->setVertex(0, idx, Vector3D(p_x, all_min_b_h*hscale, p_z));
							if( want_normals )  {
								Vector3D norm = getNormalAt(wp_x, wp_z);
								/*rd->va_normals[3*idx + 0] = norm.x;
								rd->va_normals[3*idx + 1] = norm.y;
								rd->va_normals[3*idx + 2] = norm.z;*/
								rd->setNormal(0, idx, norm);
							}
							if( want_colors ) {
								Color col = getLightmap(colormap_index,x,z);
								/*int step = alpha ? 4 : 3;
								rd->va_colors[step*idx + 0] = col.getR();
								rd->va_colors[step*idx + 1] = col.getG();
								rd->va_colors[step*idx + 2] = col.getB();
								if( alpha )
									rd->va_colors[step*idx + 3] = 255;*/
								rd->setColor(idx, 0, col.getR());
								rd->setColor(idx, 1, col.getG());
								rd->setColor(idx, 2, col.getB());
								if( rd->hasAlphaChannel() )
									rd->setColor(idx, 3, 255);
							}
						}
					}
				}
				//LOG("### verts etc %d\n", clock() - time_s);
				//time_s = clock();
				if( want_texcoords ) {
					for(int z=0,count=0;z<mesh_height;z++) {
						for(int x=0;x<mesh_width;x++) {
							float tu = (float)x;
							float tv = (float)z;
							/*tu /= ((float)mesh_width - 1.0f);
							tv /= ((float)mesh_height - 1.0f);*/
							tu /= (float)SECTION_SIZE;
							tv /= (float)SECTION_SIZE;
							if( extra > 0 ) {
								//const float step = (1.0f/(float)extra);
								const float step = ((float)extra) / (float)(SECTION_SIZE+2*extra);
								tu = step * (1.0f - tu) + (1.0f - step) * tu;
								tv = step * (1.0f - tv) + (1.0f - step) * tv;
							}
							/*tu /= 32.0f;
							tv /= 32.0f;*/
							if( !splatting ) {
								// with splatting, the scaling for detail textures is done in the shader
								//float splat_detail_scale_c = 8.0f; // for SECTION_SIZE 64
								//float splat_detail_scale_c = 4.0f; // for SECTION_SIZE 32
								//float splat_detail_scale_c = 2.0f; // for SECTION_SIZE 16
								tu *= splat_detail_scale;
								tv *= splat_detail_scale;
							}
							/*else {
								rd->splat_detail_scale = splat_detail_scale;
							}*/
							/*rd->va_texcoords[count] = tu;
							rd->va_texcoords[count+1] = tv;
							//if( want_alphamap ) {
							//        rd->va_texcoords_secondary[count] = (float)x/((float)this_width-1.0f);
							//        rd->va_texcoords_secondary[count+1] = (float)z/((float)this_height-1.0f);
							//}
							count += 2;*/
							rd->setTexCoord(count++, tu, tv);
						}
					}
				}
				//LOG("### texcoords %d\n", clock() - time_s);
				//time_s = clock();

				//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh do indices");
				//rd->n_renderlength = 2*3*(this_width-1)*(this_height-1);
				/*rd->n_renderlength = 2*3*(mesh_width-1)*(mesh_height-1) + 2*3*2*(mesh_width-1) + 2*3*2*(mesh_height-1);
				//rd->va_indices = new unsigned short[rd->n_renderlength];
				rd->va_indices.resize(rd->n_renderlength);
				rd->n_renderlength = 0;*/
				rd->setRenderlength(true, 2*3*(mesh_width-1)*(mesh_height-1) + 2*3*2*(mesh_width-1) + 2*3*2*(mesh_height-1));
				int n_renderlength = 0;
				for(int tz=0;tz<mesh_height-1;tz++) {
					for(int tx=0;tx<mesh_width-1;tx++) {
						int x = m_x*SECTION_SIZE + tx*mesh_scale_x;
						int z = m_z*SECTION_SIZE + tz*mesh_scale_z;
						x *= res; z *= res;
						unsigned char texture_id = getTextureMap(x,z);
						if( !splatting && texture_id != texture_index )
							continue;
						if( !isVisible(x,z) )
							continue;

						int i0 = mesh_width*tz+tx;
						int i1 = mesh_width*tz+(tx+1);
						int i2 = mesh_width*(tz+1)+tx;
						int i3 = mesh_width*(tz+1)+(tx+1);
						/*rd->va_indices[n_renderlength++] = i2;
						rd->va_indices[n_renderlength++] = i1;
						rd->va_indices[n_renderlength++] = i0;
						rd->va_indices[n_renderlength++] = i3;
						rd->va_indices[n_renderlength++] = i1;
						rd->va_indices[n_renderlength++] = i2;*/
						rd->setVaIndex(n_renderlength++, i2);
						rd->setVaIndex(n_renderlength++, i1);
						rd->setVaIndex(n_renderlength++, i0);
						rd->setVaIndex(n_renderlength++, i3);
						rd->setVaIndex(n_renderlength++, i1);
						rd->setVaIndex(n_renderlength++, i2);
					}
				}
				// skirts
				if( lod ) {
					for(int s=0;s<2;s++) {
						for(int tx=0;tx<mesh_width-1;tx++) {
							int x = m_x*SECTION_SIZE + tx*mesh_scale_x;
							int z = m_z*SECTION_SIZE + ( s==0 ? 0 : this_height-2 );
							x *= res; z *= res;
							unsigned char texture_id = getTextureMap(x,z);
							if( !splatting && texture_id != texture_index )
								continue;

							int i0, i1, i2, i3;
							if( s == 0 ) {
								i0 = n_points+tx;
								i1 = n_points+tx+1;
								i2 = tx;
								i3 = tx+1;
							}
							else {
								i0 = mesh_width*(mesh_height-1) + tx;
								i1 = mesh_width*(mesh_height-1) + tx+1;
								i2 = n_points+mesh_width+tx;
								i3 = n_points+mesh_width+tx+1;
							}
							/*rd->va_indices[n_renderlength++] = i2;
							rd->va_indices[n_renderlength++] = i1;
							rd->va_indices[n_renderlength++] = i0;
							rd->va_indices[n_renderlength++] = i3;
							rd->va_indices[n_renderlength++] = i1;
							rd->va_indices[n_renderlength++] = i2;*/
							rd->setVaIndex(n_renderlength++, i2);
							rd->setVaIndex(n_renderlength++, i1);
							rd->setVaIndex(n_renderlength++, i0);
							rd->setVaIndex(n_renderlength++, i3);
							rd->setVaIndex(n_renderlength++, i1);
							rd->setVaIndex(n_renderlength++, i2);
						}
					}
					for(int s=0;s<2;s++) {
						for(int tz=0;tz<mesh_height-1;tz++) {
							int x = m_x*SECTION_SIZE + ( s==0 ? 0 : this_width-2 );
							int z = m_z*SECTION_SIZE + tz*mesh_scale_z;
							x *= res; z *= res;
							unsigned char texture_id = getTextureMap(x,z);
							if( !splatting && texture_id != texture_index )
								continue;

							int i0, i1, i2, i3;
							if( s == 0 ) {
								i0 = tz==0 ? n_points : n_points + 2*mesh_width + tz-1;
								i1 = mesh_width*tz;
								i2 = tz==mesh_height-2 ? n_points + mesh_width : n_points + 2*mesh_width + tz;
								i3 = mesh_width*(tz+1);
							}
							else {
								i0 = mesh_width*tz + mesh_width-1;
								i1 = tz==0 ? n_points + mesh_width-1 : n_points + 2*mesh_width + tz-1 + mesh_height-1;
								i2 = mesh_width*(tz+1) + mesh_width-1;
								i3 = tz==mesh_height-2 ? n_points + mesh_width + mesh_width-1 : n_points + 2*mesh_width + tz + mesh_height-1;
							}
							/*rd->va_indices[n_renderlength++] = i2;
							rd->va_indices[n_renderlength++] = i1;
							rd->va_indices[n_renderlength++] = i0;
							rd->va_indices[n_renderlength++] = i3;
							rd->va_indices[n_renderlength++] = i1;
							rd->va_indices[n_renderlength++] = i2;*/
							rd->setVaIndex(n_renderlength++, i2);
							rd->setVaIndex(n_renderlength++, i1);
							rd->setVaIndex(n_renderlength++, i0);
							rd->setVaIndex(n_renderlength++, i3);
							rd->setVaIndex(n_renderlength++, i1);
							rd->setVaIndex(n_renderlength++, i2);
						}
					}
				}
				//ASSERT( rd->n_renderlength > 0 );
				//LOG("terrain node level %d index %d has %d polys\n", quadtreenode->level, index, rd->n_renderlength);
				//LOG("### vx indices %d\n", clock() - time_s);

			}

			delete unique_textures;

			//mesh->shadow_caster = true;
			//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh mesh prepare");
			/*for(int i=0;i<mesh->getNRenderData();i++) {
				LOG("%d, %d : %d\n", i, mesh->getRenderData(i), mesh->getRenderData(i)->n_renderlength);
			}*/
			//time_s = clock();
			Vector3D chunk_offset = mesh->findCentre();
			mesh->translate(-chunk_offset.x, -chunk_offset.y, -chunk_offset.z);
			mesh->prepare();
			//LOG("### mesh prepare %d\n", clock() - time_s);

			//Entity *entity = new Entity(mesh);
			SceneGraphNode *entity = new SceneGraphNode();
			entity->setObject(mesh);
			entity->setName("Terrain Chunk Node");
			entity->setPropertyFloat("splat_detail_scale", splat_detail_scale);
			entity->setShaderCallback(shaderCallback);
			Vector3D shift(m_x*SECTION_SIZE*xscale*res, 0.0, m_z*SECTION_SIZE*zscale*res);
			entity->setPosition(shift + chunk_offset);

			//quadtreenode->entity = new Entity(mesh);
			//quadtreenode->entity->owned = true;
			this->addChild(mesh);
			this->addChild(entity);

			quadtreenode->nodes->push_back(entity); // need to add it directly
		}

		//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh set dimensions");
		quadtreenode->set(quadtreenode->x,all_min_h*hscale,quadtreenode->z,quadtreenode->width,(all_max_h-all_min_h)*hscale,quadtreenode->depth);
		if( same_height ) {
			// doesn't seem to help performance
			//quadtreenode->dont_recurse_lower = true;
		}

		//if( this->sea ) {
		//if( false ) {
		//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh sea");
		if( this->sea && all_min_h < unscaled_sea_height ) {
			Mesh *mesh = new Mesh();
			//mesh->owned = true;
			mesh->n_frames = 1;
			/*mesh->n_renderDatas = 1;
			mesh->renderDatas = new RenderData[ mesh->n_renderDatas ];*/
			mesh->renderDatas.resize( 1 );
			//RenderData *rd = &mesh->renderDatas[0];
			mesh->renderDatas[0] = new RenderData();
			RenderData *rd = mesh->renderDatas[0];

			const int n_x = 32, n_z = n_x;
			//rd->initVertexArrays(1, n_x*n_z, false, /*true,*/ true, sea_texture!=NULL);
			rd->initVertexArrays(1, n_x*n_z, true);
			//rd->texture = sea_texture;
			rd->setNTextures(1);
			rd->setTexture(0, sea_texture);
			rd->setSolid(true);
			rd->setBlending(true);
			rd->setBlendType(V_BLENDTYPE_FADE);
			for(int i=0;i<n_x*n_z;i++) {
				/*rd->va_colors[4*i+0] = 255;
				rd->va_colors[4*i+1] = 255;
				rd->va_colors[4*i+2] = 255;
				rd->va_colors[4*i+3] = 160; // only affects when not using shaders; when using shaders, we set alpha in the pixel shader, depending on fresnel effect calculation*/
				rd->setColor(i, 0, 255);
				rd->setColor(i, 1, 255);
				rd->setColor(i, 2, 255);
				rd->setColor(i, 3, 160); // only affects when not using shaders; when using shaders, we set alpha in the pixel shader, depending on fresnel effect calculation
			}

			/*rd->material_specular[0] = 1.0;
			rd->material_specular[1] = 1.0;
			rd->material_specular[2] = 1.0;
			rd->material_specular[3] = 1.0;*/
			float material_specular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
			rd->setMaterialSpecular(material_specular);

			int x0 = m_x*SECTION_SIZE * res;
			int z0 = m_z*SECTION_SIZE * res;

			for(int z=0,c=0;z<n_z;z++) {
				for(int x=0;x<n_x;x++) {
					/*rd->va_vertices[c++] = x0 + (this_width-1)*res*((float)x)/((float)n_x-1.0f);
					//rd->va_vertices[c++] = (this_width-1)*res*((float)x)/((float)n_x-1.0f);
					//rd->va_vertices[c++] = unscaled_sea_height*hscale;
					rd->va_vertices[c++] = 0.0f; // height is set by moving the Entity (allows sea plane to intersect original for better bounding)
					rd->va_vertices[c++] = z0 + (this_height-1)*res*((float)z)/((float)n_z-1.0f);*/
					rd->setVertex(0, c++, Vector3D(
							x0 + (this_width-1)*res*((float)x)/((float)n_x-1.0f),
							0.0f, // height is set by moving the Entity (allows sea plane to intersect original for better bounding)
							z0 + (this_height-1)*res*((float)z)/((float)n_z-1.0f)
							));
					//VI_log("%f, %f, %f\n", rd->va_vertices[c-3], rd->va_vertices[c-2], rd->va_vertices[c-1]);
					//rd->va_vertices[c++] = (this_height-1)*res*((float)z)/((float)n_z-1.0f);
				}
			}

			/*int x1 = x0 + (this_width-1)*res;
			int z1 = z0 + (this_height-1)*res;
			rd->va_vertices[3*0+0] = x0*xscale;
			rd->va_vertices[3*0+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*0+2] = z0*zscale;

			rd->va_vertices[3*1+0] = x1*xscale;
			rd->va_vertices[3*1+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*1+2] = z0*zscale;

			rd->va_vertices[3*2+0] = x0*xscale;
			rd->va_vertices[3*2+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*2+2] = z1*zscale;

			rd->va_vertices[3*3+0] = x1*xscale;
			rd->va_vertices[3*3+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*3+2] = z1*zscale;*/

			/*rd->va_vertices[3*0+0] = -sea_extent*xscale;
			rd->va_vertices[3*0+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*0+2] = -sea_extent*zscale;

			rd->va_vertices[3*1+0] = (width+sea_extent)*xscale;
			rd->va_vertices[3*1+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*1+2] = -sea_extent*zscale;

			rd->va_vertices[3*2+0] = -sea_extent*xscale;
			rd->va_vertices[3*2+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*2+2] = (depth+sea_extent)*zscale;

			rd->va_vertices[3*3+0] = (width+sea_extent)*xscale;
			rd->va_vertices[3*3+1] = unscaled_sea_height*hscale;
			rd->va_vertices[3*3+2] = (depth+sea_extent)*zscale;*/

			// needed for shadowplane
			for(int i=0;i<n_x*n_z;i++) {
				/*rd->va_vertices[3*i+0] += offset.x;
				rd->va_vertices[3*i+1] += offset.y;
				rd->va_vertices[3*i+2] += offset.z;*/
				/*rd->va_normals[3*i+0] = 0.0;
				rd->va_normals[3*i+1] = 1.0;
				rd->va_normals[3*i+2] = 0.0;*/
				rd->setNormal(0, i, Vector3D(0.0f, 1.0f, 0.0f));
			}
			/*float u_scale = 0.1f * width;
			float v_scale = 0.1f * depth;*/
			/*float u_scale = 0.1f * this_width;
			float v_scale = 0.1f * this_height;*/
			float u_scale = 0.1f;
			float v_scale = 0.1f;
			if( sea_texture != NULL ) {
				for(int z=0,c=0;z<n_z;z++) {
					for(int x=0;x<n_x;x++) {
						/*rd->va_texcoords[c++] = u_scale*((float)x)/((float)n_x-1.0f);
						rd->va_texcoords[c++] = v_scale*((float)z)/((float)n_z-1.0f);*/
						//rd->setTexCoord(c++, u_scale*((float)x)/((float)n_x-1.0f), v_scale*((float)z)/((float)n_z-1.0f));
						rd->setTexCoord(c++,
							u_scale * (x0 + (this_width-1)*res*((float)x)/((float)n_x-1.0f)),
							v_scale * (z0 + (this_height-1)*res*((float)z)/((float)n_z-1.0f))
						);
					}
				}

				/*rd->va_texcoords[2*0+0] = 0.0*u_scale;
				rd->va_texcoords[2*0+1] = 0.0*v_scale;

				rd->va_texcoords[2*1+0] = 0.0*u_scale;
				rd->va_texcoords[2*1+1] = 1.0*v_scale;

				rd->va_texcoords[2*2+0] = 1.0*u_scale;
				rd->va_texcoords[2*2+1] = 0.0*v_scale;

				rd->va_texcoords[2*3+0] = 1.0*u_scale;
				rd->va_texcoords[2*3+1] = 1.0*v_scale;*/
			}

			/*rd->n_renderlength = 6*(n_x-1)*(n_z-1);
			//rd->va_indices = new unsigned short[rd->n_renderlength];
			rd->va_indices.resize(rd->n_renderlength);*/
			rd->setRenderlength(true, 6*(n_x-1)*(n_z-1));
			for(int z=0,c=0;z<n_z-1;z++) {
				for(int x=0;x<n_x-1;x++) {
					int v0 = n_x * z + x;
					int v1 = n_x * z + x+1;
					int v2 = n_x * (z+1) + x;
					int v3 = n_x * (z+1) + x+1;
					/*rd->va_indices[c++] = v2;
					rd->va_indices[c++] = v1;
					rd->va_indices[c++] = v0;
					rd->va_indices[c++] = v3;
					rd->va_indices[c++] = v1;
					rd->va_indices[c++] = v2;*/
					rd->setVaIndex(c++, v2);
					rd->setVaIndex(c++, v1);
					rd->setVaIndex(c++, v0);
					rd->setVaIndex(c++, v3);
					rd->setVaIndex(c++, v1);
					rd->setVaIndex(c++, v2);
				}
			}
			/*rd->va_indices[0] = 2;
			rd->va_indices[1] = 1;
			rd->va_indices[2] = 0;
			rd->va_indices[3] = 3;
			rd->va_indices[4] = 1;
			rd->va_indices[5] = 2;*/
			Vector3D chunk_offset = mesh->findCentre();
			mesh->translate(-chunk_offset.x, -chunk_offset.y, -chunk_offset.z);
			mesh->prepare();
			//mesh->shaders.pixel_shader_ambient = g3->getWaterSimplePixelShader(); // shader used for no reflections - overridden if there are reflections, when we set the shadowplane

			// won't work until we can feed the shadowplane entity into GraphicsEngine
			/*ShadowPlane *sp = new ShadowPlane();
			sp->plane.normal.set(0.0, 1.0, 0.0);
			sp->plane.D = unscaled_sea_height*hscale + offset.y;
			sp->shadow = false;
			sp->reflection = true;
			sp->render_to_texture = true;
			mesh->setShadowPlane(g3, sp);*/
			if( g3->getGraphicsEnvironment()->hasShaders() && g3->hasWaterShaders() ) {
				/*ObjectDataShaders shaders(
					g3->getWaterAmbientVertexShader(), g3->getWaterAmbientPixelShader(),
					g3->getWaterDirectionalVertexShader(), g3->getWaterDirectionalPixelShader(),
					g3->getWaterPointVertexShader(), g3->getWaterPointPixelShader()
					);*/
				ObjectDataShaders shaders(
					g3->getWaterAmbientShader(),
					g3->getWaterDirectionalShader(),
					g3->getWaterPointShader()
					);
				if( this->sea_effects ) {
					ObjectData *d_obj = dynamic_cast<ObjectData *>(this->entity_sea_dummy->getObject()); // cast to the internal representation
					ShadowPlane *sp = d_obj->getShadowPlane();
					int texture_size = g3->getRenderToTextureSize();
					mesh->setShadowPlane(g3->getRenderer(), sp, &shaders, texture_size);
				}
				else {
					// do effects, but without a real reflection
					mesh->setShaders(&shaders);
					mesh->setSecondaryTexture( this->pretend_reflection_texture );
				}
				// must do after setting the secondary texture
				rd->setNTextures(3);
				rd->setTexture(2, waves_bump_texture);
			}

			//Entity *entity = new Entity(mesh);
			SceneGraphNode *entity = new SceneGraphNode();
			entity->setObject(mesh);
			entity->setName("Terrain Chunk Sea Node");
			//entity->setPosition(&chunk_offset);
			Vector3D sea_offset(0.0f, unscaled_sea_height*hscale, 0.0f);
			//entity->setPosition(&sea_offset);
			entity->setPosition(chunk_offset+sea_offset);
			this->addChild(mesh);
			this->addChild(entity);
			//quadtreenode->nodes->add(entity); // need to add it directly
			quadtreenode->nodes->push_back(entity); // need to add it directly
			//this->entity_sea->setTextureSpeed(0.0, 0.00002);
		}
		//LOG("<<<\n");
		//VI_check();
	}
	//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh do children");
	for(int i=0;i<4;i++) {
		if( quadtreenode->children[i] != NULL )
			buildQuadtreenodeMesh(quadtreenode->children[i]);
	}
	//GraphicsEnvironment::checkGLError("TerrainEngine::buildQuadtreenodeMesh exit");
}

void TerrainEngine::setLOD(bool lod) {
	if( this->lod != lod ) {
		this->lod = lod;
		this->recalc = true;
		if( !Vision::isLocked() ) {
			prepare();
		}
	}
}

void TerrainEngine::setTexture(int index,VI_Texture *texture) {
	ASSERT( index >= 0 && index < N_TERRAIN_TEXTURES );
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	this->recalc = true;
	this->textures[index] = d_texture;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::setSeaTexture(VI_Texture *sea_texture) {
	Texture *d_sea_texture = static_cast<Texture *>(sea_texture); // cast to the internal representation
	this->recalc = true;
	this->sea_texture = d_sea_texture;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

/*void TerrainEngine::setLightingNone() {
this->recalc = true;
if( this->lightmap != NULL )
delete [] this->lightmap;
this->lightmap = NULL;
this->lighting = LIGHTING_NONE;
if( !Vision::isLocked() ) {
prepare();
}
}*/

void TerrainEngine::setLighting(bool lighting_enabled) {
	this->recalc = true;
	this->lighting_enabled = lighting_enabled;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::toggleSeaReflections() {
	this->recalc = true;
	this->sea_effects = !this->sea_effects;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::setColorMode(V_COLORMODE_t colormode) {
	if( this->colormode != colormode ) {
		this->recalc = true;
		this->colormode = colormode;
		for(int i=0;i<N_TERRAIN_TEXTURES;i++) {
			if( this->lightmap[i] != NULL ) {
				delete [] this->lightmap[i];
				this->lightmap[i] = NULL;
			}
		}
		if( !Vision::isLocked() ) {
			prepare();
		}
	}
}

void TerrainEngine::initColor(int index, unsigned char r, unsigned char g, unsigned char b) {
	ASSERT( index >= 0 && index < N_TERRAIN_TEXTURES );
	this->recalc = true;
	if( this->lightmap[index] == NULL )
		this->lightmap[index] = new Color[width*depth];
	Color col(r, g, b);
	for(int z=0;z<depth;z++) {
		for(int x=0;x<width;x++) {
			this->setLightmap(index, x, z, col);
		}
	}

	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::setColorLightmap(int index,Color color_amb,Color color_diff,Vector3D dir) {
	ASSERT( index >= 0 && index < N_TERRAIN_TEXTURES );
	this->recalc = true;
	if( this->lightmap[index] == NULL )
		this->lightmap[index] = new Color[width*depth];
	//this->lighting = LIGHTING_PRECALCULATED;

	dir.normalise();
	for(int z=0;z<depth;z++) {
		for(int x=0;x<width;x++) {
			/*unsigned char h = this->get(x,z);
			const unsigned char low = 64;
			const unsigned char high = 255;
			float alpha = ((float)h) / 255.0;
			unsigned char c = (1.0 - alpha)*low + alpha*high;
			Color this_col;
			this_col.seti(c, c, c);*/
			Color this_col;
			Vector3D norm = this->getNormalAt(x*xscale,z*zscale);
			float dot = norm % dir;
			if( dot > 0.0 ) {
				this_col = color_diff;
				this_col.scale(dot);
				this_col.add(&color_amb);
			}
			else {
				this_col = color_amb;
			}
			this->setLightmap(index, x, z, this_col);
		}
	}

	if( !Vision::isLocked() ) {
		prepare();
	}
}

//void TerrainEngine::setColorNoise(int index,float min_r,float min_g,float min_b,float max_r,float max_g,float max_b,float noise_scale) {
void TerrainEngine::setColorNoisei(int index,unsigned char min_r,unsigned char min_g,unsigned char min_b,unsigned char max_r,unsigned char max_g,unsigned char max_b,float noise_scale) {
	ASSERT( index >= 0 && index < N_TERRAIN_TEXTURES );
	this->recalc = true;
	if( this->lightmap[index] == NULL ) {
		this->lightmap[index] = new Color[width*depth];
		Color col(255, 255, 255);
		for(int z=0;z<depth;z++) {
			for(int x=0;x<width;x++) {
				this->setLightmap(index, x, z, col);
			}
		}
	}

	Color color_min, color_max;
	//color_min.setf(min_r,min_g,min_b);
	//color_max.setf(max_r,max_g,max_b);
	color_min.seti(min_r,min_g,min_b);
	color_max.seti(max_r,max_g,max_b);

	const float off = 0.0145f;
	for(int z=0;z<depth;z++) {
		float nz = ((float)z + off) * noise_scale;
		for(int x=0;x<width;x++) {
			float nx = ((float)x + off) * noise_scale;
			float vec[2] = {nx, nz};
			float noise = 0.5f * ( 1.0f + VI_perlin_noise2(vec) );
			Color this_col = color_min * (1.0f-noise) + color_max * noise;
			this->multLightmap(index, x, z, this_col);
		}
	}

	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::setColorAmbientOcclusion() {
	// should manually switch to V_COLORMODE_GLOBAL first!
	this->recalc = true;
	if( this->lightmap[0] == NULL ) {
		this->lightmap[0] = new Color[width*depth];
		Color col(255, 255, 255);
		for(int z=0;z<depth;z++) {
			for(int x=0;x<width;x++) {
				this->setLightmap(0, x, z, col);
			}
		}
	}

	int weights[] = {1, 2, 1, 2, 4, 2, 1, 2, 1};
	for(int z=0;z<depth;z++) {
		for(int x=0;x<width;x++) {
			if( x > 0 && x < width-1 && z > 0 && z < depth-1 ) {
				int average = 0;
				int sum = 0;
				for(int tz=z-1,c=0;tz<=z+1;tz++) {
					for(int tx=x-1;tx<=x+1;tx++,c++) {
						int th = this->get(tx, tz);
						int weight = weights[c];
						average += weight * th;
						sum += weight;
					}
				}
				average /= sum;
				int h = this->get(x, z);
				int diff = h - average;
				diff *= 20;
				/*if( abs(diff) > 5 ) {
					LOG("");
				}*/
				const int offset_c = 255;
				int val = offset_c + diff;
				//val = 127;
				val = min(val, 255);
				val = max(val, 0);
				Color col(val, val, val);
				this->multLightmap(0, x, z, col);
			}
		}
	}

	if( !Vision::isLocked() ) {
		prepare();
	}
}

void TerrainEngine::setColorAt(int index, int x, int z, unsigned char r, unsigned char g, unsigned char b) {
	Color col(r, g, b);
	this->setLightmap(index, x, z, col);
	if( !Vision::isLocked() ) {
		prepare();
	}
}

/*void TerrainEngine::setLightingPerVertex() {
this->recalc = true;
if( this->lightmap != NULL )
delete [] this->lightmap;
this->lightmap = NULL;
this->lighting = LIGHTING_PERVERTEX;
if( !Vision::isLocked() ) {
prepare();
}
}*/

void TerrainEngine::renderQueueQuadtreenode(GraphicsEnvironment *genv, Quadtreenode *quadtreenode) {
	if( /*quadtreenode->level < this->quadtree->levels-1 ||*/ quadtreenode->isVisible(genv) )
	{
		int render_level = 1;
		if( lod ) {
			if( quadtreenode->dont_recurse_lower )
				render_level = 999;
			else {
				// TODO: improve?
				World *world = genv->getGraphics3D()->getWorld();
				Vector3D wpos = world->getViewpoint()->getCurrentWorldPos();
				wpos -= offset;
				float dx = 0, dz = 0;
				quadtreenode->distFrom(&dx, &dz, wpos.x, wpos.z);
				//LOG("%d : %d , %d : %f %f %f %f\n", quadtreenode->level, quadtreenode->index_x, quadtreenode->index_z, quadtreenode->x, quadtreenode->z, quadtreenode->x+quadtreenode->width, quadtreenode->z+quadtreenode->depth);
				float d2 = dx*dx + dz*dz;
				if( d2 > 10000 )
					render_level = 3;
				else if( d2 > 6000 )
					render_level = 2;
				//LOG("    dist %f , %f from %f , %f\n", dx, dz, wpos.x, wpos.z);
			}
		}
		if( quadtreenode->level >= this->quadtree->levels-render_level ) {
			/*
			// first check children
			// if only one child has visible nodes, then we recurse to that child
			// DISABLED as untested / unclear if any performance benefit
			Quadtreenode *child = NULL;
			int n_child = 0;
			for(int i=0;i<4;i++) {
			if( quadtreenode->children[i] != NULL ) {
			if( quadtreenode->children[i]->isVisible(g) ) {
			child = quadtreenode->children[i];
			n_child++;
			if( n_child > 1 ) {
			// too many
			child = NULL;
			break;
			}
			}
			}
			}

			if( child != NULL ) {
			renderQueueQuadtreenode(g, child);
			}
			else*/
			{
				this->nodes_rendered++;

				//LOG("    >>> RENDER <<<\n");
				//Entity *entity = quadtreenode->entity;
				for(size_t i=0;i<quadtreenode->nodes->size();i++) {
					//Entity *entity = static_cast<Entity *>(quadtreenode->nodes->get(i));
					SceneGraphNode *entity = quadtreenode->nodes->at(i);
					//VI_log("terrain entity: %s\n", entity->getName());

					/*glGetFloatv(GL_MODELVIEW_MATRIX, entity->mvmatrix);
					g->c_queue->renderQueue->add( entity );
					g->c_queue->renderQueue_nonopaque->add( entity );
					if( entity->getObject()->shadow_caster )
					g->c_queue->renderQueue_shadowcasters->add( entity );*/
					genv->getGraphics3D()->buildRenderQueue( entity );
				}
				if( this->entity_sea_dummy != NULL ) {
					//g->c_queue->renderQueue_shadowplanes->addIfAbsent( this->entity_sea_dummy );
				}
			}
		}
		else {
			for(int i=0;i<4;i++) {
				if( quadtreenode->children[i] != NULL )
					renderQueueQuadtreenode(genv, quadtreenode->children[i]);
			}
		}
	}
}

void TerrainEngine::renderQueue(RenderQueue *queue, GraphicsEnvironment *genv) {
	this->nodes_rendered = 0;
	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate(offset.x, offset.y, offset.z);
	renderQueueQuadtreenode(genv, this->quadtree->quadtreenodes);
	/*if( this->entity_sea != NULL ) {
		this->entity_sea->getMatrix()->save();
		// add to both for now, as we could set it as being either
		//g->c_queue->renderQueue->add( this->entity_sea );
		//g->c_queue->renderQueue_nonopaque->add( this->entity_sea );
		//g->c_queue->renderQueue->push_back( this->entity_sea );
		queue->addOpaque( this->entity_sea );
		//g->c_queue->renderQueue_nonopaque->push_back( this->entity_sea );
		queue->addNonOpaque( this->entity_sea );
	}*/
	genv->getRenderer()->popMatrix();
}

/*void TerrainEngine::renderQuadtreenode(Graphics3D *g, Quadtreenode *quadtreenode) {
if( this->lighting != LIGHTING_PERVERTEX ) {
glDisable(GL_LIGHTING);
}
if( quadtreenode->isVisible(g) ) {
//if( true ) {
//for(int i=0;i<quadtreenode->nodes->size();i++) {
//        RenderData *rd = static_cast<RenderData *>(quadtreenode->nodes->get(i));
//        rd->render(g,0,0,0.0,false,NULL,255);
//}
if( quadtreenode->level == this->quadtree->levels-1 ) {
//int index = quadtreenode->index_z * n_chunks_x + quadtreenode->index_x;
//if( this->meshes[index] != NULL )
//        this->meshes[index]->renderDatas[0].render(g,0,0,0.0,false,NULL,255);
//Entity *entity = quadtreenode->entity;
//if( entity != NULL ) {
for(int i=0;i<quadtreenode->nodes->size();i++) {
Entity *entity = static_cast<Entity *>(quadtreenode->nodes->get(i));
entity->getObject()->renderDatas[0].render(g,0,0,0.0,false,NULL,255,false);
}
}
for(int i=0;i<4;i++) {
if( quadtreenode->children[i] != NULL )
renderQuadtreenode(g, quadtreenode->children[i]);
}
}
glEnable(GL_LIGHTING);
}*/

/*void TerrainEngine::render(Graphics3D *g) {
renderQuadtreenode(g, this->quadtree->quadtreenodes);
//for(int i=0;i<n_chunks_x*n_chunks_z;i++) {
//        this->meshes[i]->renderDatas[0].render(g,0,0,0.0,false,NULL,255);
//}
}*/

unsigned char TerrainEngine::get(int x,int z) const {
	ASSERT( x >= 0 && x < width );
	ASSERT( z >= 0 && z < depth );
	return data[z * width + x];
}

void TerrainEngine::set(int x,int z,unsigned char v) {
	ASSERT( x >= 0 && x < width );
	ASSERT( z >= 0 && z < depth );
	this->recalc = true;
	data[z * width + x] = v;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

unsigned char TerrainEngine::getTextureMap(int x,int z) const {
	ASSERT( x >= 0 && x < width-1 );
	ASSERT( z >= 0 && z < depth-1 );
	return texture_map[z * (width-1) + x];
}

void TerrainEngine::setTextureMap(int x,int z,unsigned char v) {
	ASSERT( x >= 0 && x < width-1 );
	ASSERT( z >= 0 && z < depth-1 );
	this->recalc = true;
	texture_map[z * (width-1) + x] = v;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

Color TerrainEngine::getLightmap(int index,int x,int z) const {
	ASSERT( index >= 0 && index < N_TERRAIN_TEXTURES );
	ASSERT( x >= 0 && x < width );
	ASSERT( z >= 0 && z < depth );
	if( this->lightmap[index] == NULL ) {
		// no colormap set for this index
		Color col(255,255,255);
		return col;
	}
	return lightmap[index][z * width + x];
}

void TerrainEngine::setLightmap(int index,int x,int z,Color color) {
	// n.b., caller should call prepare() after!
	ASSERT( index >= 0 && index < N_TERRAIN_TEXTURES );
	ASSERT( this->lightmap[index] != NULL );
	ASSERT( x >= 0 && x < width );
	ASSERT( z >= 0 && z < depth );
	this->recalc = true;
	lightmap[index][z * width + x] = color;
}

void TerrainEngine::multLightmap(int index,int x,int z,Color color) {
	// n.b., caller should call prepare() after!
	ASSERT( index >= 0 && index < N_TERRAIN_TEXTURES );
	ASSERT( this->lightmap[index] != NULL );
	ASSERT( x >= 0 && x < width );
	ASSERT( z >= 0 && z < depth );
	this->recalc = true;
	Color c_color = lightmap[index][z * width + x];
	float r = color.getRf() * c_color.getRf();
	float g = color.getGf() * c_color.getGf();
	float b = color.getBf() * c_color.getBf();
	float a = color.getAf() * c_color.getAf();
	Color n_color;
	n_color.setf(r, g, b, a);
	lightmap[index][z * width + x] = n_color;
}

void TerrainEngine::setVisible(int x,int z,bool v) {
	ASSERT( x >= 0 && x < width-1 );
	ASSERT( z >= 0 && z < depth-1 );
	this->recalc = true;
	visible[z * (width-1) + x] = v;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

bool TerrainEngine::isVisible(int x,int z) const {
	ASSERT( x >= 0 && x < width-1 );
	ASSERT( z >= 0 && z < depth-1 );
	return visible[z * (width-1) + x];
}

void TerrainEngine::setSea(bool sea,float unscaled_sea_height,float sea_extent,bool sea_effects) {
	this->recalc = true;
	if( !g3->getGraphicsEnvironment()->hasShaders() || !g3->hasWaterShaders() ) {
		// need shaders for sea_effects
		LOG("don't have correct water shaders, disabling sea effects\n");
		sea_effects = false;
	}
	this->sea = sea;
	this->sea_effects = sea_effects;
	this->unscaled_sea_height = unscaled_sea_height;
	this->sea_extent = sea_extent;
	if( !Vision::isLocked() ) {
		prepare();
	}
}

float TerrainEngine::getHeightAt(float x,float z) const {
	int wx = (int)(x / xscale);
	int wz = (int)(z / zscale);
	int wx1 = (wx + 1 ) % getWidth();
	int wz1 = (wz + 1 ) % getDepth();
	float fx = x/xscale - (float)wx;
	float fz = z/zscale - (float)wz;
	if(x < 0 || z < 0 || wx > getWidth()-1 || wz > getDepth()-1)
		return 0;
	/*if(wx == getWidth()-1) {
	if(fx == 0) {
	fx = 0.99999;
	//fx = 1.0;
	wx--;
	}
	else
	return 0;
	}*/
	/*if(wy == getHeight()-1) {
	if(fy == 0) {
	fy = 1.0;
	wy--;
	}
	else
	return 0;
	}*/
	/*
	float h00 = get(wx,wy) * hscale;
	float h10 = get(wx+1,wy) * hscale;
	float h01 = get(wx,wy+1) * hscale;
	float h11 = get(wx+1,wy+1) * hscale;
	float h0 = h00 * (1.0 - fx) + h10 * fx;
	float h1 = h01 * (1.0 - fx) + h11 * fx;
	float h = h0 * (1.0 - fy) + h1 * fy;
	return h;
	*/
	// compute height on diagonal
	float h10 = get(wx1,wz) * hscale;
	float h01 = get(wx,wz1) * hscale;
	float diag_h = h01 * fz + h10 * (1.0f - fz);
	float h = 0.0;
	if(fx + fz < 1.0) {
		float h00 = get(wx,wz) * hscale;
		float left_h = h01 * fz + h00 * (1.0f - fz);
		h = left_h + ( fx * ( diag_h - left_h ) ) / (1.0f - fz);
	}
	else {
		float h11 = get(wx1,wz1) * hscale;
		float right_h = h11 * fz + h10 * (1.0f - fz);
		h = right_h + ( (1.0f - fx) * ( diag_h - right_h ) ) / fz;
	}
	return h;
}

Vector3D TerrainEngine::getNormalAt(float x,float z) const {
	int wx = (int)(x / xscale);
	int wz = (int)(z / zscale);
	int wx1 = (wx + 1 ) % getWidth();
	int wz1 = (wz + 1 ) % getDepth();
	float fx = x/xscale - (float)wx;
	float fz = z/zscale - (float)wz;
	if(x < 0 || z < 0 || wx > getWidth()-1 || wz > getDepth()-1)
		return Vector3D(0.0f, 1.0f, 0.0f);
	float h10 = get(wx1,wz) * hscale;
	float h01 = get(wx,wz1) * hscale;
	Vector3D p10(xscale, h10, 0.0);
	Vector3D p01(0.0, h01, zscale);
	Vector3D normal(0.0, 0.0, 0.0);
	if(fx + fz < 1.0) {
		float h00 = get(wx,wz) * hscale;
		Vector3D p00(0.0, h00, 0.0);
		Vector3D A = p10 - p00;
		normal = p01 - p00;
		normal.cross(A);
	}
	else {
		float h11 = get(wx1,wz1) * hscale;
		Vector3D p11(xscale, h11, zscale);
		Vector3D A = p01 - p11;
		normal = p10 - p11;
		normal.cross(A);
	}
	normal = - normal;
	normal.normalise();
	//normal.set(0.0, 1.0, 0.0);
	return normal;
}

void TerrainEngine::update(int time_ms) {
}

void TerrainEngine::shaderCallback(const GraphicsEnvironment *genv, const ShaderEffect *shader, const SceneGraphNode *node) {
	float splat_detail_scale = node->getPropertyFloat("splat_detail_scale");
	//LOG("callback: %f\n", splat_detail_scale);
	shader->SetUniformParameter1f("splat_detail_scale", splat_detail_scale);
}

bool TerrainEngine::rayfire(Vector3D *hit, Vector3D start, Vector3D dir, float range) const {
	// simple version
	/*if( dir.x < dir.z ) {
	}*/
	Vector3D p1 = start;
	float h1 = this->getHeightAt(p1.x, p1.z);
	float above = p1.y - h1;
	if( above <= 0.0 ) {
		return false;
	}
	float dist = 0.0f;
	Vector3D p2 = start;
	for(;;) {
		p2 += dir;
		dist += 1.0f;
		float h2 = this->getHeightAt(p2.x, p2.z);
		above = p2.y - h2;
		if( above <= 0.0 ) {
			hit->set(p2.x, h2, p2.z);
			return true;
		}
		if( dist >= range ) {
			break;
		}
	}
	return false;
}
