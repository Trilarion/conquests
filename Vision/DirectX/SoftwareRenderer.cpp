//---------------------------------------------------------------------------

#include <cassert>
#include <tchar.h>

#include "SoftwareRenderer.h"
#include "../GraphicsEnvironment.h"
#include "../GraphicsEngine.h"
#include "../Texture.h"

//---------------------------------------------------------------------------

//bool D3D9RendererInfo::twoSidedStencil = false;

SoftwareVertexArray::SoftwareVertexArray(SoftwareRenderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
	this->renderer = renderer;
	this->data_type = data_type;
	this->tex_unit = tex_unit;
	this->stream = stream;
	this->size = size;
	this->data = data;
	if( data_type == DATATYPE_COLORS && ( dim != 3 && dim != 4 ) ) {
		Vision::setError(new VisionException(NULL, VisionException::V_RENDER_ERROR, "DATATYPECOLORS must have dim 3 or 4"));
	}
}

SoftwareVertexArray::~SoftwareVertexArray() {
}

SoftwareVertexArray *SoftwareVertexArray::create(SoftwareRenderer *renderer, VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
	if( data != NULL ) {
		ASSERT( size > 0 );
	}
	SoftwareVertexArray *va = new SoftwareVertexArray(renderer, data_type, tex_unit, stream, data, size, dim);
	bool ok = true;

	/*if( data == NULL ) {
		// no data
	}
	else if( data_type == DATATYPE_INDICES ) {
		if( FAILED( renderer->m_pDevice->CreateIndexBuffer( size, usage, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &va->ib, NULL ) ) ) {
			ok = false;
		}
	}
	else {
		DWORD fvf = 0;
		if( data_type == DATATYPE_VERTICES )
			fvf = D3DFVF_XYZ;
		else if( data_type == DATATYPE_NORMALS )
			fvf = D3DFVF_NORMAL;
		else if( data_type == DATATYPE_COLORS )
			fvf = D3DFVF_DIFFUSE;
		else if( data_type == DATATYPE_TEXCOORDS )
			fvf = D3DFVF_TEX1; // TODO: does texture unit matter?
		else {
			Vision::setError(new VisionException(NULL, VisionException::V_RENDERER_INVALID_STATE, "unknown data type\n"));
		}
		if( FAILED( renderer->m_pDevice->CreateVertexBuffer( size, usage, fvf, D3DPOOL_DEFAULT, &va->vb, NULL ) ) ) {
			ok = false;
		}
	}

	if( ok && data != NULL ) {
		VOID *ptr = NULL;
		if( data_type == DATATYPE_INDICES ) {
			if( FAILED( va->ib->Lock( 0, size, (void**)&ptr, 0 ) ) ) {
				ok = false;
			}
		}
		else {
			if( FAILED( va->vb->Lock( 0, size, (void**)&ptr, 0 ) ) ) {
				ok = false;
			}
		}
		if( ok ) {
			if( data_type == DATATYPE_COLORS ) {
				// TODO: assumes alpha!
				ASSERT( size % 4 == 0 );
				DWORD *d3ddata = new DWORD[size/4];
				unsigned char *srcdata = (unsigned char *)data;
				for(int i=0;i<size/4;i++) {
					d3ddata[i] = D3DCOLOR_RGBA(srcdata[0],srcdata[1],srcdata[2],srcdata[3]);
					srcdata += 4;
				}
				memcpy( ptr, d3ddata, size );
				delete [] d3ddata;
			}
			else {
				memcpy( ptr, data, size );
			}
			if( data_type == DATATYPE_INDICES ) {
				va->ib->Unlock();
			}
			else {
				va->vb->Unlock();
			}
		}
	}*/

	if( !ok ) {
		delete va;
		va = NULL;
		Vision::setError(new VisionException(NULL, VisionException::V_DIRECT3D_ERROR, "SoftwareVertexArray::create() failed\n"));
	}
	return va;
}

void SoftwareVertexArray::renderVertices(bool new_data, int offset) const {
	if( new_data ) {
		ASSERT( stream );
		// no need to do anything
	}
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	this->renderer->render_array_vertices = &((float *)this->data)[offset];
	this->renderer->render_array_vertices_size = size;
}

void SoftwareVertexArray::renderNormals(bool new_data, int offset) const {
	if( new_data ) {
		ASSERT( stream );
		// no need to do anything
	}
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	this->renderer->render_array_normals = &((float *)this->data)[offset];
}

void SoftwareVertexArray::renderTexCoords(bool new_data, int offset) const {
	if( new_data ) {
		ASSERT( stream );
		// no need to do anything
	}
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	this->renderer->render_array_texcoords = &((float *)this->data)[offset];
}

void SoftwareVertexArray::renderTexCoords(bool new_data, int offset, int unit) const {
	if( new_data ) {
		ASSERT( stream );
		// no need to do anything
	}
	Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
}

void SoftwareVertexArray::renderColors(bool new_data, int offset) const {
	if( new_data ) {
		ASSERT( stream );
		// no need to do anything
	}
	/*if( !alpha ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareVertexArray::renderColors must have alpha channel\n"));
	}*/
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
	this->renderer->render_array_colors = &((unsigned char *)this->data)[offset];
	this->renderer->render_array_colors_alpha = dim==4;
}

void SoftwareVertexArray::update() const {
	ASSERT( data != NULL );
	// no need to do anything
}

void SoftwareVertexArray::updateIndices() const {
	//ASSERT( stream );
	ASSERT( data != NULL );
	// no need to do anything
}

void SoftwareVertexArray::renderElements(DrawingMode mode, int n_this_renderlength, int n_this_vertices) const {
	this->renderer->renderElements(mode, (unsigned short *)data, n_this_renderlength, n_this_vertices);
}

void SoftwareTransformationMatrix::load() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTransformationMatrix:: not supported\n"));
	renderer->setTransform(&mat);
}

void SoftwareTransformationMatrix::save() {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTransformationMatrix:: not supported\n"));
	renderer->getTransform(&mat);
}

void SoftwareTransformationMatrix::mult() const {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTransformationMatrix:: not supported\n"));
	renderer->multiplyTransform(&mat);
}

SoftwareTexture::SoftwareTexture(SoftwareRenderer *renderer, Image2D *image) : renderer(renderer), image(image) {
	//image->owned = true;
	this->addChild(image);
}

SoftwareTexture::~SoftwareTexture() {
	//delete image;
}

Texture *SoftwareTexture::create(SoftwareRenderer *renderer, int width, int height, bool alpha) {
	Image2D *image = new Image2D(width, height, alpha);
	//image->setAlpha(alpha);
	SoftwareTexture *texture = new SoftwareTexture(renderer, image);
	return texture;
}

Texture *SoftwareTexture::createFromImage(SoftwareRenderer *renderer, VI_Image *image, bool mask, unsigned char maskcol[3]) {
	bool image_alpha = image->getAlpha();
	bool tex_alpha = image_alpha || mask;
	// need to copy the image, and do it manually (due to mask/maskcol options)
	Image2D *tex_image = new Image2D(image->getWidth(), image->getHeight(), tex_alpha);
	//tex_image->setAlpha(tex_alpha);

	unsigned char *s_ptr = (unsigned char *)image->getData();
	unsigned char *dest = (unsigned char *)tex_image->getData();
	for(int y=0;y<image->getHeight();y++) {
		for(int x=0;x<image->getWidth();x++) {
			unsigned char r = s_ptr[0];
			unsigned char g = s_ptr[1];
			unsigned char b = s_ptr[2];
			*dest++ = r;
			*dest++ = g;
			*dest++ = b;
			if( mask ) {
				int alpha = 255;
				//if( rgbdata[i].r == maskcol->r && rgbdata[i].g == maskcol->g && rgbdata[i].b == maskcol->b ) {
				//if( r == maskcol->r && g == maskcol->g && b == maskcol->b ) {
				if( r == maskcol[0] && g == maskcol[1] && b == maskcol[2] ) {
					alpha = 0;
				}
				*dest++ = alpha;
			}
			else if( image_alpha ) {
				*dest++ = s_ptr[3];
			}
			else {
				//*line++ = 255;
				// texture is only in RGB format, not RGBA
			}
			s_ptr += image_alpha ? 4 : 3;
		}
	}
	SoftwareTexture *texture = new SoftwareTexture(renderer, tex_image);
	delete image;
	return texture;
}

Texture *SoftwareTexture::loadTexture(SoftwareRenderer *renderer, const char *filename, bool mask, unsigned char maskcol[3]) {
	Image2D *image = Image2D::loadImage(filename);
	/*if( image == NULL ) {
		return NULL;
	}*/
	return createFromImage(renderer, image, mask, maskcol);
}

Texture *SoftwareTexture::copy() {
	// TODO: use handle/body idiom?
	Image2D *image_copy = static_cast<Image2D *>(image->copy());
	SoftwareTexture *texture = new SoftwareTexture(renderer, image_copy);
	return texture;
}

void SoftwareTexture::enable() {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTexture:: not supported\n"));
	this->renderer->c_texture = this;
}

/*void SoftwareTexture::clampToEdge() {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTexture:: not supported\n"));
	// TODO:
}

void SoftwareTexture::clampMirror() {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTexture:: not supported\n"));
	// TODO:
}*/

void SoftwareTexture::copyTo(int width, int height) {
	Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTexture:: not supported\n"));
}

void SoftwareTexture::draw(int x, int y) {
	int w = image->getWidth();
	int h = image->getHeight();
	this->draw(x, y, w, h);
}

void SoftwareTexture::draw(int x, int y, int w, int h) {
	//Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareTexture:: not supported\n"));
	// TODO: improve performance by drawing directly
	this->renderer->enableTexturing(true);
	this->enable();
	if( image->getAlpha() ) {
		this->renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
	}
	float vertex_data[] = {
		(float)x, (float)(y-h), 0.0f,
		(float)(x+w), (float)(y-h), 0.0f,
		(float)(x+w), (float)y, 0.0f,
		(float)x, (float)y, 0.0f
	};
	float texcoord_data[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};
	this->renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4, NULL, false, 0);
	this->renderer->enableTexturing(false);
	if( image->getAlpha() ) {
		this->renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
	}
}

int SoftwareTexture::getWidth() const {
	return this->image->getWidth();
}

int SoftwareTexture::getHeight() const {
	return this->image->getHeight();
}

const int START = 32;
const int N_CHARS = 256;

SoftwareFontBuffer::SoftwareFontBuffer(SoftwareRenderer *renderer,const char *family,int pt,int style) : renderer(renderer) {
	advance_x = NULL;
	offset_x = NULL;
	offset_y = NULL;
	width = NULL;
	height = NULL;
	//base = 0;

	Image2D **images = NULL;
	Image2D::FontInfo *fontInfo = NULL;
	Image2D::GlobalFontInfo globalFontInfo;
	int n_images = Image2D::loadFont(&images, &globalFontInfo, &fontInfo, family, pt, START, N_CHARS);
	descent = globalFontInfo.descent;
	font_height = globalFontInfo.height;
	advance_x = new int[n_images];
	offset_x = new int[n_images];
	offset_y = new int[n_images];
	width = new int[n_images];
	height = new int[n_images];
	textures = new Texture *[n_images];
	for(int i=0;i<n_images;i++) {
		advance_x[i] = fontInfo[i].advance_x;
		offset_x[i] = fontInfo[i].offset_x;
		offset_y[i] = fontInfo[i].offset_y;
		//height[i] = globalFontInfo.height;
		if( images[i] == NULL ) {
			width[i] = 0;
			height[i] = 0;
			textures[i] = NULL;
		}
		else {
			images[i]->flip(false, true);
			width[i] = images[i]->getWidth();
			height[i] = images[i]->getHeight();
			//images[i]->makePowerOf2();
			/*for(int y=0;y<images[i]->getHeight();y++) {
				for(int x=0;x<images[i]->getWidth();x++) {
					unsigned char r, g, b, a;
					images[i]->getRGBA(&r, &g, &b, &a, x, y);
					if( r != 0 && r != 255 ) {
						LOG("%d, %d: %d %d %d %d\n", x, y, r, g, b, a);
					}
				}
			}*/
			/*if( START + i == 'i' ) {
				images[i]->save("c:\\temp\\test.png", "png");
			}*/
			textures[i] = new SoftwareTexture(renderer, images[i]); // image now owned by texture class
		}
	}
	delete [] images;
	delete [] fontInfo;
}

SoftwareFontBuffer::~SoftwareFontBuffer() {
	delete [] advance_x;
	delete [] offset_x;
	delete [] offset_y;
	delete [] width;
	delete [] height;
	if( textures != NULL ) {
		for(int i=0;i<N_CHARS-START;i++) {
			delete textures[i];
		}
		delete [] textures;
	}
}

int SoftwareFontBuffer::getIndex(char letter) const {
	return (int)letter - START;
}

int SoftwareFontBuffer::writeText(int x,int y,const char *text) const {
	return this->writeText(x, y, text, strlen(text));
}

int SoftwareFontBuffer::writeText(int x,int y,const char *text,size_t length) const {
	int sx = x;
	for(size_t i=0;i<length;i++) {
		unsigned char ch = text[i];
		//ASSERT( ch >= START && ch < N_CHARS );
		if( ch < START || ch >= N_CHARS )
			continue;
		Texture *texture = textures[ch-START];
		if( texture != NULL ) {
			int ox = offset_x[ch-START];
			int oy = texture->getHeight() + descent - offset_y[ch-START];
			texture->draw(x+ox, y+oy);
		}
		x += advance_x[ch-START];
	}
	return x - sx;

	/*
	//glPushAttrib(GL_ENABLE_BIT|GL_COLOR_BUFFER_BIT|GL_CURRENT_BIT|GL_TEXTURE_BIT|GL_TRANSFORM_BIT);
	glPushAttrib(GL_ENABLE_BIT|GL_COLOR_BUFFER_BIT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	//glLoadIdentity();
	//int this_height = this->getHeight();
	//glTranslated(x,y + this_height/2,0);
	glTranslated(x,y,0);
	for(int i=0;i<length;i++) {
		unsigned char ch = text[i];
		//ASSERT( ch >= START && ch < N_CHARS );
		if( ch < START || ch >= N_CHARS )
		continue;
		Texture *texture = textures[ch-START];
		int wid = width[ch-START];
		if( texture != NULL ) {
			int hgt = height[ch-START];
			texture->enable();
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			//float tx_w = ((float)wid) / (float)texture->getWidth();
			//float tx_h = ((float)hgt) / (float)texture->getHeight();
			float tx_w = 1.0f;
			float tx_h = 1.0f;
			int x = offset_x[ch-START];
			int y = descent - offset_y[ch-START];
			//int y = font_height - descent - offset_y[ch-START];
			//int y = 0;
			//int y = - hgt;
			//int y = descent;
			//int y = - offset_y[ch-START];
			glBegin(GL_QUADS);
			glTexCoord2f(0.0,0.0);
			glVertex2d(x,y);
			glTexCoord2f(0.0,tx_h);
			glVertex2d(x,y+hgt);
			glTexCoord2f(tx_w,tx_h);
			glVertex2d(x+wid,y+hgt);
			glTexCoord2f(tx_w,0.0);
			glVertex2d(x+wid,y);
			glEnd();
		}
		glTranslated(advance_x[ch-START], 0, 0);
	}
	glPopMatrix();
	//glDisable(GL_TEXTURE_2D);
	//glDisable(GL_BLEND);
	glPopAttrib();*/
}

int SoftwareFontBuffer::getWidth(char letter) const {
	int index = getIndex(letter);
	int w = advance_x[index];
	return w;
}

SoftwareGraphics2D::SoftwareGraphics2D(GraphicsEnvironment *genv, SoftwareRenderer *renderer) : Graphics2D(genv), renderer(renderer) {
}

void SoftwareGraphics2D::setColor3i(unsigned char r, unsigned char g, unsigned char b) {
	this->renderer->setColor4(r, g, b, 255);
}
void SoftwareGraphics2D::setColor4i(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	this->renderer->setColor4(r, g, b, a);
}

void SoftwareGraphics2D::fillRect(short x,short y,short width,short height,const float tu[4], const float tv[4]) const {
	// TODO: improve performance by drawing directly
	if( this->texture != NULL ) {
		this->renderer->enableTexturing(true);
		this->texture->enable();
	}
	float vertex_data[] = {
		(float)x, (float)y, 0.0f,
		(float)x, (float)(y+height), 0.0f,
		(float)(x+width), (float)(y+height), 0.0f,
		(float)(x+width), (float)y, 0.0f
	};
	/*float texcoord_data[] = {
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f
	};*/
	float texcoord_data[] = {
		tu[1], tv[1],
		tu[0], tv[0],
		tu[2], tv[2],
		tu[3], tv[3]
	};
	unsigned char color_data[] = {renderer->color_current.x, renderer->color_current.y, renderer->color_current.z, renderer->color_current.w};
	//this->renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, this->texture != NULL ? texcoord_data : NULL, 4, NULL, false, 0);
	this->renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, this->texture != NULL ? texcoord_data : NULL, 4, color_data, true, 4);
	if( this->texture != NULL ) {
		this->renderer->enableTexturing(false);
	}
}

SoftwareRenderer::SoftwareRenderer(GraphicsEnvironment *genv, LPDIRECT3D9 pD3D, LPDIRECT3DDEVICE9 pDevice) : Renderer(genv), m_pD3D(pD3D), m_pDevice(pDevice) {
	LOG("Initialising Software Renderer...\n");

	RendererInfo::framebufferObject = false;
	RendererInfo::maxLights = MAX_SOFTWARE_LIGHTS;

	this->pitch = genv->getWidth();
	int size = pitch * genv->getHeight();
	color_buffer = new DWORD[size];
	depth_stencil_buffer = new DWORD[size];

	c_transform = TRANSFORMTYPE_UNKNOWN;

	const float black[] = {0, 0, 0, 0};
	this->setFixedFunctionAmbientLighting(black);
	this->setFixedFunctionMatSpecular(black);
	this->setColor4(255, 255, 255, 255);
	this->setClearColor(0, 0, 0, 0);
	//this->clear(true, true, true); // no need to initialise, as should be done by Graphics3D::init()

	this->cull_face = true;
	this->front_face = true;
	this->lighting = false;
	this->texturing = false;
	this->c_texture = NULL;
	this->disableFixedFunctionLights();

	this->state_depth_buffer_mode = RENDERSTATE_DEPTH_READWRITE;
	this->state_depth_buffer_func = RENDERSTATE_DEPTHFUNC_LESS;
	this->state_blend_mode = RENDERSTATE_BLEND_NONE;

	this->render_normals = false;
	this->render_texcoords = false;
	this->render_colors = false;
	this->render_array_vertices = NULL;
	this->render_array_vertices_size = 0;
	this->render_array_normals = NULL;
	this->render_array_texcoords = NULL;
	this->render_array_colors = NULL;
	this->render_array_colors_alpha = false;

	D3DXMatrixIdentity(&transform_model);
	D3DXMatrixIdentity(&transform_projection);
	D3DXMatrixIdentity(&transform_texture);

	if( FAILED( D3DXCreateMatrixStack(0, &this->stack_model) ) ) {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"D3DXCreateMatrixStack failed"));
	}
	if( FAILED( D3DXCreateMatrixStack(0, &this->stack_projection) ) ) {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"D3DXCreateMatrixStack failed"));
	}
	if( FAILED( D3DXCreateMatrixStack(0, &this->stack_texture) ) ) {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"D3DXCreateMatrixStack failed"));
	}
	c_stack = NULL;
}

SoftwareRenderer::~SoftwareRenderer() {
/*#ifdef _DEBUG
	ASSERT( _CrtCheckMemory() );
#endif*/
	delete [] color_buffer;
	delete [] depth_stencil_buffer;
}

void SoftwareRenderer::setTransform(const D3DXMATRIX *transform) {
	if( c_transform == TRANSFORMTYPE_MODEL ) {
		this->transform_model = *transform;
	}
	else if( c_transform == TRANSFORMTYPE_PROJECTION ) {
		this->transform_projection = *transform;
	}
	else if( c_transform == TRANSFORMTYPE_TEXTURE ) {
		this->transform_texture = *transform;
	}
	else {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"unknown transform type in setTransform()"));
	}
}

void SoftwareRenderer::multiplyTransform(const D3DXMATRIX *transform) {
	if( c_transform == TRANSFORMTYPE_MODEL ) {
		//this->transform_model *= *transform;
		this->transform_model = *transform * transform_model;
	}
	else if( c_transform == TRANSFORMTYPE_PROJECTION ) {
		//this->transform_projection *= *transform;
		this->transform_projection = *transform * transform_projection;
	}
	else if( c_transform == TRANSFORMTYPE_TEXTURE ) {
		//this->transform_texture *= *transform;
		this->transform_texture = *transform * transform_texture;
	}
	else {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"unknown transform type in multiplyTransform()"));
	}
}

void SoftwareRenderer::getTransform(D3DXMATRIX *transform) const {
	if( c_transform == TRANSFORMTYPE_MODEL ) {
		*transform = this->transform_model;
	}
	else if( c_transform == TRANSFORMTYPE_PROJECTION ) {
		*transform = this->transform_projection;
	}
	else if( c_transform == TRANSFORMTYPE_TEXTURE ) {
		*transform = this->transform_texture;
	}
	else {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"unknown transform type in getTransform()"));
	}
}

void SoftwareRenderer::beginScene() const {
}

void SoftwareRenderer::endScene() const {
	//return;
	V__ENTER;
	LPDIRECT3DSURFACE9 surface = NULL;
	this->m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &surface);
	/*D3DSURFACE_DESC desc;
	surface->GetDesc(&desc);*/

	//D3DCOLOR color = D3DCOLOR_RGBA(255, 0, 255, 0);
	D3DLOCKED_RECT d3drect;
	if( FAILED( surface->LockRect(&d3drect, NULL, 0) ) ) {
		Vision::setError(new VisionException(this,VisionException::V_DIRECT3D_ERROR,"failed to lock back buffer"));
	}
	else {
		unsigned char *dest = (unsigned char *)d3drect.pBits;
		const DWORD *ptr = this->color_buffer;
		const int height = this->genv->getHeight();
		const int width = this->genv->getWidth();
		for(int y=0;y<height;y++) {
			DWORD *line = (DWORD *)&dest[y*d3drect.Pitch];
			/*for(int x=0;x<width;x++) {
				//*line++ = this->read(x, y);
				*line++ = *ptr++;
				//*line++ = 0;
			}*/
			memcpy(line, ptr, width * sizeof(DWORD));
			ptr += width;
		}
		surface->UnlockRect();
	}

	surface->Release();
	V__EXIT;
}

void SoftwareRenderer::resizeScene(float frustum[6][4], int width, int height, bool persp, float z_near, float z_far, float fovy) {
	this->z_near = z_near;
	this->z_far = z_far;
	this->persp = persp;
	D3DXMATRIX matrix;
	D3DXMatrixIdentity(&matrix);
	this->transform_model = matrix;

	D3DXMatrixPerspectiveFovRH(&matrix, fovy * (float)M_PI / 180.0f, (float)width/(float)height, z_near, z_far);
	// we do this, as the code expects there to be a projection matrix (for the 3D viewing mode) on the stack
	this->switchToProjection();
	this->popMatrix();
	this->transform_projection = matrix;

	double clip[16];
	float t = 0.0f;
	D3DXMATRIX proj = this->transform_projection;
	D3DXMATRIX modl = this->transform_model;

	/* Combine the two matrices (multiply projection by modelview) */
	clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
	clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
	clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
	clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

	clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
	clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
	clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
	clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

	clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
	clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
	clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
	clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

	clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
	clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
	clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
	clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

	/* Extract the numbers for the RIGHT plane */
	frustum[0][0] = (float)(clip[ 3] - clip[ 0]);
	frustum[0][1] = (float)(clip[ 7] - clip[ 4]);
	frustum[0][2] = (float)(clip[11] - clip[ 8]);
	frustum[0][3] = (float)(clip[15] - clip[12]);
	/* Normalize the result */
	t = sqrt( frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2] );
	frustum[0][0] /= t;
	frustum[0][1] /= t;
	frustum[0][2] /= t;
	frustum[0][3] /= t;
	LOG("FRUSTUM RIGHT %f %f %f %f\n", frustum[0][0],  frustum[0][1], frustum[0][2], frustum[0][3]);

	/* Extract the numbers for the LEFT plane */
	frustum[1][0] = (float)(clip[ 3] + clip[ 0]);
	frustum[1][1] = (float)(clip[ 7] + clip[ 4]);
	frustum[1][2] = (float)(clip[11] + clip[ 8]);
	frustum[1][3] = (float)(clip[15] + clip[12]);
	/* Normalize the result */
	t = sqrt( frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2] );
	frustum[1][0] /= t;
	frustum[1][1] /= t;
	frustum[1][2] /= t;
	frustum[1][3] /= t;
	LOG("FRUSTUM LEFT %f %f %f %f\n", frustum[1][0],  frustum[1][1], frustum[1][2], frustum[1][3]);

	/* Extract the BOTTOM plane */
	frustum[2][0] = (float)(clip[ 3] + clip[ 1]);
	frustum[2][1] = (float)(clip[ 7] + clip[ 5]);
	frustum[2][2] = (float)(clip[11] + clip[ 9]);
	frustum[2][3] = (float)(clip[15] + clip[13]);
	/* Normalize the result */
	t = sqrt( frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2] );
	frustum[2][0] /= t;
	frustum[2][1] /= t;
	frustum[2][2] /= t;
	frustum[2][3] /= t;
	LOG("FRUSTUM BOTTOM %f %f %f %f\n", frustum[2][0],  frustum[2][1], frustum[2][2], frustum[2][3]);

	/* Extract the TOP plane */
	frustum[3][0] = (float)(clip[ 3] - clip[ 1]);
	frustum[3][1] = (float)(clip[ 7] - clip[ 5]);
	frustum[3][2] = (float)(clip[11] - clip[ 9]);
	frustum[3][3] = (float)(clip[15] - clip[13]);
	/* Normalize the result */
	t = sqrt( frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2] );
	frustum[3][0] /= t;
	frustum[3][1] /= t;
	frustum[3][2] /= t;
	frustum[3][3] /= t;
	LOG("FRUSTUM TOP %f %f %f %f\n", frustum[3][0],  frustum[3][1], frustum[3][2], frustum[3][3]);

	/* Extract the FAR plane */
	frustum[4][0] = (float)(clip[ 3] - clip[ 2]);
	frustum[4][1] = (float)(clip[ 7] - clip[ 6]);
	frustum[4][2] = (float)(clip[11] - clip[10]);
	frustum[4][3] = (float)(clip[15] - clip[14]);
	/* Normalize the result */
	t = sqrt( frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2] );
	frustum[4][0] /= t;
	frustum[4][1] /= t;
	frustum[4][2] /= t;
	frustum[4][3] /= t;
	LOG("FRUSTUM FAR %f %f %f %f\n", frustum[4][0],  frustum[4][1], frustum[4][2], frustum[4][3]);

	/* Extract the NEAR plane */
	frustum[5][0] = (float)(clip[ 3] + clip[ 2]);
	frustum[5][1] = (float)(clip[ 7] + clip[ 6]);
	frustum[5][2] = (float)(clip[11] + clip[10]);
	frustum[5][3] = (float)(clip[15] + clip[14]);
	/* Normalize the result */
	t = sqrt( frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2] );
	frustum[5][0] /= t;
	frustum[5][1] /= t;
	frustum[5][2] /= t;
	frustum[5][3] /= t;
	LOG("FRUSTUM NEAR %f %f %f %f\n", frustum[5][0],  frustum[5][1], frustum[5][2], frustum[5][3]);

	this->pushMatrix();
	this->ortho2D(width, height);
}

void SoftwareRenderer::clear(bool color, bool depth, bool stencil) const {
	//return;
	V__ENTER;
	// stencil ignored for now
	int size = pitch * genv->getHeight();
	color = true; // for now, as skybox drawing disabled
	if( color ) {
		//memset(color_buffer, 0, size * sizeof(color_buffer));
		for(int i=0;i<size;i++) {
			color_buffer[i] = clear_color;
		}
	}
	if( depth ) {
		memset(depth_stencil_buffer, 0, size * sizeof(depth_stencil_buffer));
	}
	V__EXIT;
}

void SoftwareRenderer::ortho2D(int width, int height) {
	D3DXMATRIX matrix;
	D3DXMatrixOrthoOffCenterRH(&matrix, 0.0f, (float)width, (float)height, 0.0f, 0.0f, 1.0f);
	this->transform_projection = matrix;
}

void SoftwareRenderer::getViewOrientation(Vector3D *right,Vector3D *up) {
	// TODO: optimise so we don't have to allocate a matrix? Just call the matrix->save D3D code directly
	TransformationMatrix *mat = this->createTransformationMatrix();
	mat->save();
	right->set((*mat)[0],(*mat)[4],(*mat)[8]);
	up->set((*mat)[1],(*mat)[5],(*mat)[9]);
	delete mat;
}

void SoftwareRenderer::getViewDirection(Vector3D *dir) {
	// TODO: optimise so we don't have to allocate a matrix? Just call the matrix->save D3D code directly
	TransformationMatrix *mat = this->createTransformationMatrix();
	mat->save();
	dir->set((*mat)[2],(*mat)[6],(*mat)[10]);
	delete mat;
}

// rendering operations

void SoftwareRenderer::transformVertex(SoftwareVertexOut *vertOut, const D3DXVECTOR3 *vertIn, const D3DXVECTOR3 *normIn, const D3DXVECTOR4 *colorIn, const D3DXVECTOR2 *texcoordIn, const D3DXMATRIX *transform_model, const D3DXMATRIX *transform_projection) const {
	// TODO: optimise to a single matrix
	D3DXVECTOR4 vert, view;
	D3DXVec3Transform(&view, vertIn, transform_model);
	D3DXVec4Transform(&vert, &view, transform_projection);

	vertOut->x = vert.x / vert.w;
	vertOut->y = vert.y / vert.w;
	vertOut->z = vert.z / vert.w;
	/*vertOut->x = vert.x / abs(vert.z);
	vertOut->y = vert.y / abs(vert.z);
	vertOut->z = vert.z;*/

	if( this->lighting ) {
		//float f[4];
		D3DXVECTOR4 lights_color = this->color_ambient;
		for(int i=0;i<MAX_SOFTWARE_LIGHTS;i++) {
			//if( this->c_light[i] != NULL ) {
			if( this->c_lighton[i] ) {
				/*this->c_light[i]->getLightAmbient().floatArray(f);
				D3DXVECTOR4 light_col(f);
				this->c_light[i]->getLightDiffuse().floatArray(f);
				D3DXVECTOR4 light_diffuse_col(f);*/
				/*D3DXVECTOR4 light_col( (float)this->c_light[i]->getLightAmbient().getR(), (float)this->c_light[i]->getLightAmbient().getG(), (float)this->c_light[i]->getLightAmbient().getB(), (float)this->c_light[i]->getLightAmbient().getA());
				D3DXVECTOR4 light_diffuse_col( (float)this->c_light[i]->getLightDiffuse().getR(), (float)this->c_light[i]->getLightDiffuse().getG(), (float)this->c_light[i]->getLightDiffuse().getB(), (float)this->c_light[i]->getLightDiffuse().getA());*/
				D3DXVECTOR4 light_col( (float)(255.0f*c_lightambient[i][0]), (float)(255.0f*c_lightambient[i][1]), (float)(255.0f*c_lightambient[i][2]), (float)(255.0f*c_lightambient[i][3]));
				D3DXVECTOR4 light_diffuse_col( (float)(255.0f*c_lightdiffuse[i][0]), (float)(255.0f*c_lightdiffuse[i][1]), (float)(255.0f*c_lightdiffuse[i][2]), (float)(255.0f*c_lightdiffuse[i][3]));
				//if( this->c_lightpos[i][3] == 0.0 ) {
				if( this->c_lightdirectional[i] ) {
					// directional
				}
				else {
					// point
					D3DXVECTOR3 light_pos(this->c_lightpos[i][0], this->c_lightpos[i][1], this->c_lightpos[i][2]);
					D3DXVECTOR3 view3(view.x, view.y, view.z);
					D3DXVECTOR3 light_vec = light_pos - view3;
					D3DXVECTOR3 light_vecN;
					D3DXVec3Normalize(&light_vecN, &light_vec);
					D3DXVECTOR3 norm_view;
					D3DXVec3TransformNormal(&norm_view, normIn, transform_model); // should really use Inverse Transpose of transform_model!
					float dot = D3DXVec3Dot(&light_vecN, &norm_view);
					if( dot > 0.0 ) {
						light_col += light_diffuse_col * dot;
					}
				}
				lights_color += light_col;
			}
		}
		vertOut->color.x = (colorIn->x * lights_color.x)/255.0f;
		vertOut->color.y = (colorIn->y * lights_color.y)/255.0f;
		vertOut->color.z = (colorIn->z * lights_color.z)/255.0f;
		vertOut->color.w = (colorIn->w * lights_color.w)/255.0f;
		//vertOut->color = lights_color;
	}
	else {
		vertOut->color = *colorIn;
	}

	vertOut->texcoord = *texcoordIn;
}

void SoftwareRenderer::flatScan32(DWORD *line, DWORD *zbuffer, int x1, int x2, float z1, float z2, D3DXVECTOR4 color1, const D3DXVECTOR4 *color2, D3DXVECTOR2 texcoord1, const D3DXVECTOR2 *texcoord2) {
	//const float z_near = this->genv->getGraphics3D()->getZNear();
	//const float z_far = this->genv->getGraphics3D()->getZFar();
	const int width = this->genv->getWidth();

	// quick rejection
	if( x2 < 0 || x1 >= width ) {
		// TODO: do at a higher level
		return;
	}
	//if( z1 < z_near && z2 < z_near ) {
	if( z1 < 0.0f && z2 < 0.0f ) {
		return;
	}

	int dx = x2 - x1;

	float d_z = 0;
	/*float d_a = 0;
	float d_r = 0;
	float d_g = 0;
	float d_b = 0;*/
	D3DXVECTOR4 d_color(0, 0, 0, 0);
	D3DXVECTOR2 d_texcoord(0, 0);
	if( dx > 0 ) {
		d_z = (z2 - z1) / dx;
		/*d_a = (a2 - a1) / dx;
		d_r = (r2 - r1) / dx;
		d_g = (g2 - g1) / dx;
		d_b = (b2 - b1) / dx;*/
		d_color = (*color2 - color1) / (float)dx;
		d_texcoord = (*texcoord2 - texcoord1) / (float)dx;
	}

	if( x1 < 0 ) {
		z1 -= d_z * x1;
		/*a1 -= d_a * x1;
		r1 -= d_r * x1;
		g1 -= d_g * x1;
		b1 -= d_b * x1;*/
		color1 -= d_color * (float)x1;
		texcoord1 -= d_texcoord * (float)x1;

		x1 = 0;
	}
	else {
		line += x1;
		zbuffer += x1;
	}

	const int max_z_c = 16777215;
	float z_scale = max_z_c / z_far;

	x2 = min(x2, width-1);
	for(int x=x1;x<=x2;x++) {
		//if( x >= 0 )
		{
			//if( z1 < z_near || z1 >= z_far ) {
			if( z1 < 0.0f || z1 >= z_far ) {
				// z rejection
			}
			else {
				bool passed = true;
				if( this->state_depth_buffer_mode == RENDERSTATE_DEPTH_READONLY || this->state_depth_buffer_mode == RENDERSTATE_DEPTH_READWRITE ) {
					// z test
					int new_z = (int)(z1 * z_scale);
					new_z = min(new_z, max_z_c);
					int current_z = *zbuffer & 0x00ffffff;
					if( current_z == 0 ) {
						// okay
					}
					else if( this->state_depth_buffer_func == RENDERSTATE_DEPTHFUNC_LESS && new_z >= current_z ) {
					//else if( this->state_depth_buffer_func == RENDERSTATE_DEPTHFUNC_LESS && new_z <= current_z ) {
					//else if( this->state_depth_buffer_func == RENDERSTATE_DEPTHFUNC_LESS && new_z > current_z ) {
						passed = false;
					}
					else if( this->state_depth_buffer_func == RENDERSTATE_DEPTHFUNC_EQUAL && new_z != current_z ) {
						passed = false;
					}
					//passed = true;
					if( passed && this->state_depth_buffer_mode == RENDERSTATE_DEPTH_READWRITE ) {
						// write the new z value
						// TODO: take into account stencil buffer
						*zbuffer = new_z;
					}
				}
				if( passed ) {
					//DWORD color = D3DCOLOR_RGBA(r1, g1, b1, a1);
					D3DXVECTOR4 color;
					if( this->texturing && this->c_texture != NULL ) {
						// modulate with texture color
						/*int u = (int)(this->c_texture->getWidth() * texcoord1.x);
						int v = (int)(this->c_texture->getHeight() * texcoord1.y);*/
						int u = (int)((this->c_texture->getWidth()-1) * texcoord1.x);
						int v = (int)((this->c_texture->getHeight()-1) * texcoord1.y);
						// TODO: different texture modes?
						u %= this->c_texture->getWidth();
						v %= this->c_texture->getHeight();
						v = this->c_texture->getHeight() - 1 - v; // invert v coord
						//ASSERT( !this->c_texture->getImage()->getAlpha() );
						if( this->c_texture->getImage()->getAlpha() ) {
							const rgba_struct *rgba = (const rgba_struct *)this->c_texture->getImage()->get(u, v);
							D3DXVECTOR4 tex_color((float)rgba->r, (float)rgba->g, (float)rgba->b, (float)rgba->a);
							color = tex_color;
						}
						else {
							const rgb_struct *rgb = (const rgb_struct *)this->c_texture->getImage()->get(u, v);
							D3DXVECTOR4 tex_color((float)rgb->r, (float)rgb->g, (float)rgb->b, 255.0f);
							color = tex_color;
						}
						// modulate with base color
						color.x *= (color1.x/255.0f);
						color.y *= (color1.y/255.0f);
						color.z *= (color1.z/255.0f);
						color.w *= (color1.w/255.0f);
					}

					else {
						color = color1;
					}

					if( this->state_blend_mode != RENDERSTATE_BLEND_NONE ) {
						// blending
						D3DXVECTOR4 dest_color;
						dest_color.w = (float)((*line & 0xff000000) >> 24);
						dest_color.x = (float)((*line & 0xff0000) >> 16);
						dest_color.y = (float)((*line & 0xff00) >> 8);
						dest_color.z = (float)(*line & 0xff);
						if( this->state_blend_mode == RENDERSTATE_BLEND_BOTH ) {
							color += dest_color;
						}
						else if( this->state_blend_mode == RENDERSTATE_BLEND_ADDITIVE ) {
							color *= color.w/255.0f;
							color += dest_color;
						}
						else if( this->state_blend_mode == RENDERSTATE_BLEND_TRANSPARENCY ) {
							float src_alpha = color.w/255.0f;
							color *= src_alpha;
							dest_color *= 1.0f - src_alpha;
							color += dest_color;
						}
						else {
							Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer:: not supported\n"));
						}
						color.x = min(color.x, 255.0f);
						color.y = min(color.y, 255.0f);
						color.z = min(color.z, 255.0f);
						color.w = min(color.w, 255.0f);
					}
					DWORD screen_color = D3DCOLOR_RGBA((unsigned char)color.x, (unsigned char)color.y, (unsigned char)color.z, (unsigned char)color.w);
					*line = screen_color;
				}
			}
		}

		line++;
		zbuffer++;

		z1 += d_z;
		/*a1 += d_a;
		r1 += d_r;
		g1 += d_g;
		b1 += d_b;*/
		color1 += d_color;
		texcoord1 += d_texcoord;
	}
}

void SoftwareRenderer::drawPoly(const SoftwareVertexOut *p1, const SoftwareVertexOut *p2, const SoftwareVertexOut *p3) {
	//DWORD color = D3DCOLOR_RGBA(255, 255, 255, 255);

	// order so that p1->sy <= p2->sy <= p3->sy
	if( p1->sy > p2->sy ) {
		std::swap(p1, p2);
	}
	if( p2->sy > p3->sy ) {
		std::swap(p2, p3);
	}
	if( p1->sy > p2->sy ) {
		std::swap(p1, p2);
	}

	if( p1->sy == p3->sy ) {
		return;
	}

	D3DXVECTOR4 color1 = p1->color;
	D3DXVECTOR4 color2 = p2->color;
	D3DXVECTOR4 color3 = p3->color;

	D3DXVECTOR2 texcoord1 = p1->texcoord;
	D3DXVECTOR2 texcoord2 = p2->texcoord;
	D3DXVECTOR2 texcoord3 = p3->texcoord;

	const int height = this->genv->getHeight();

	int y = p1->sy;

	float dy = (float)(p3->sy - p1->sy);

	float x = (float)p1->sx;
	float d = ((float)(p3->sx - p1->sx)) / dy;

	float z = p1->z;
	float d_z = ((float)(p3->z - p1->z)) / dy;

	D3DXVECTOR4 color = color1;
	D3DXVECTOR4 d_color = (color3 - color1) / dy;

	D3DXVECTOR2 texcoord = texcoord1;
	D3DXVECTOR2 d_texcoord = (texcoord3 - texcoord1) / dy;

	DWORD *color_ptr = this->color_buffer + y * pitch;
	DWORD *z_ptr = this->depth_stencil_buffer + y * pitch;

	if( p1->sy < p2->sy ) {
		// draw top half
		float dy0 = (float)(p2->sy - p1->sy);

		float x0 = (float)p1->sx;
		float d0 = ((float)(p2->sx - p1->sx)) / dy0;

		float z0 = p1->z;
		float d0_z = ((float)(p2->z - p1->z)) / dy0;

		D3DXVECTOR4 color0 = color1;
		D3DXVECTOR4 d0_color = (color2 - color1) / dy0;

		D3DXVECTOR2 texcoord0 = texcoord1;
		D3DXVECTOR2 d0_texcoord = (texcoord2 - texcoord1) / dy0;

		int y2 = min(p2->sy, height);
		if( d < d0 ) {
			while( y < y2 ) {
				if( y >= 0 ) {
					this->flatScan32(color_ptr, z_ptr, (int)x, (int)x0, z, z0, color, &color0, texcoord, &texcoord0);
				}
				y++;
				color_ptr += pitch;
				z_ptr += pitch;

				x += d;
				x0 += d0;

				z += d_z;
				z0 += d0_z;

				color += d_color;
				color0 += d0_color;

				texcoord += d_texcoord;
				texcoord0 += d0_texcoord;
			}
		}
		else {
			while( y < y2 ) {
				if( y >= 0 ) {
					this->flatScan32(color_ptr, z_ptr, (int)x0, (int)x, z0, z, color0, &color, texcoord0, &texcoord);
				}
				y++;
				color_ptr += pitch;
				z_ptr += pitch;
				x += d;
				x0 += d0;

				z += d_z;
				z0 += d0_z;

				color += d_color;
				color0 += d0_color;

				texcoord += d_texcoord;
				texcoord0 += d0_texcoord;
			}
		}
	}

	/*int y2 = min(p2->sy, height);
	while( y < y2 ) {
		y++;
		color_ptr += pitch;
		z_ptr += pitch;

		x += d;

		z += d_z;

		color += d_color;

		texcoord += d_texcoord;
	}*/

	if( p3->sy == p2->sy ) {
		return;
	}
	//return;

	float dy0 = (float)(p3->sy - p2->sy);

	float x0 = (float)p2->sx;
	float d0 = ((float)(p3->sx - p2->sx)) / dy0;

	float z0 = p2->z;
	float d0_z = ((float)(p3->z - p2->z)) / dy0;

	D3DXVECTOR4 color0 = color2;
	D3DXVECTOR4 d0_color = (color3 - color2) / dy0;

	D3DXVECTOR2 texcoord0 = texcoord2;
	D3DXVECTOR2 d0_texcoord = (texcoord3 - texcoord2) / dy0;

	int y3 = min(p3->sy, height);
	if( x < x0 ) {
		while( y < y3 ) {
			if( y >= 0 ) {
				this->flatScan32(color_ptr, z_ptr, (int)x, (int)x0, z, z0, color, &color0, texcoord, &texcoord0);
			}
			y++;
			color_ptr += pitch;
			z_ptr += pitch;

			x += d;
			x0 += d0;

			z += d_z;
			z0 += d0_z;

			color += d_color;
			color0 += d0_color;

			texcoord += d_texcoord;
			texcoord0 += d0_texcoord;
		}
	}
	else {
		while( y < y3 ) {
			if( y >= 0 ) {
				this->flatScan32(color_ptr, z_ptr, (int)x0, (int)x, z0, z, color0, &color, texcoord0, &texcoord);
			}
			y++;
			color_ptr += pitch;
			z_ptr += pitch;
			x += d;
			x0 += d0;

			z += d_z;
			z0 += d0_z;

			color += d_color;
			color0 += d0_color;

			texcoord += d_texcoord;
			texcoord0 += d0_texcoord;
		}
	}
}

void SoftwareRenderer::draw(const Vector3D *p0, const Vector3D *p1, const unsigned char rgba[4]) {
	// TODO:
}

void SoftwareRenderer::renderElements(VertexArray::DrawingMode mode, unsigned short *indices, int n_this_renderlength, int n_this_vertices) {
	//LOG(">>>\n");
	//D3DXMATRIX transform_model_projection = transform_model * transform_projection; // TODO: precalculate
	/*const D3DXVECTOR3 *vertices = (const D3DXVECTOR3 *)this->render_array_vertices;
	const D3DXVECTOR3 *normals = (const D3DXVECTOR3 *)this->render_array_normals;
	const D3DXVECTOR2 *texcoords = (const D3DXVECTOR2 *)this->render_array_texcoords;*/
	//this->render(mode, this->render_array_vertices, this->render_texcoords ? this->render_array_texcoords : NULL, this->render_normals ? this->render_array_normals : NULL, indices, n_this_renderlength, NULL, false, 0); // TODO: colour?
	if( this->render_colors ) {
		this->render(mode, this->render_array_vertices, this->render_texcoords ? this->render_array_texcoords : NULL, this->render_normals ? this->render_array_normals : NULL, indices, n_this_renderlength, this->render_array_colors, this->render_array_colors_alpha, 1);
	}
	else {
		unsigned char color_data[] = {this->color_current.x, this->color_current.y, this->color_current.z, this->color_current.w};
		this->render(mode, this->render_array_vertices, this->render_texcoords ? this->render_array_texcoords : NULL, this->render_normals ? this->render_array_normals : NULL, indices, n_this_renderlength, color_data, true, n_this_renderlength);
	}
}

void SoftwareRenderer::render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count, const unsigned char *color_data, bool alpha, int color_repeat) {
	this->render(mode, vertex_data, texcoord_data, NULL, NULL, count, color_data, alpha, color_repeat);
}

void SoftwareRenderer::render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, const float *normal_data, const unsigned short *indices, int count, const unsigned char *color_data, bool alpha, int color_repeat) {
	V__ENTER;
	const D3DXVECTOR3 *vertices = (const D3DXVECTOR3 *)vertex_data;
	const D3DXVECTOR3 *normals = (const D3DXVECTOR3 *)normal_data;
	const D3DXVECTOR2 *texcoords = (const D3DXVECTOR2 *)texcoord_data;
	const D3DXVECTOR2 texcoordDummy(0, 0);
	int halfw = this->genv->getWidth() / 2;
	int halfh = this->genv->getHeight() / 2;
	int vpp = 0;
	if( mode == VertexArray::DRAWINGMODE_TRIANGLES )
		vpp = 3;
	else if( mode == VertexArray::DRAWINGMODE_QUADS )
		vpp = 4;
	D3DXVECTOR4 color(255.0f, 255.0f, 255.0f, 255.0f);
	if( mode == VertexArray::DRAWINGMODE_TRIANGLES || mode == VertexArray::DRAWINGMODE_QUADS ) {
		int n_polys = count / vpp;
		for(int i=0,c=0;i<n_polys;i++) {
			SoftwareVertexOut buffer[4];
			// vertex shader stage
			for(int j=0;j<vpp;j++,c++) {
				int index  = indices == NULL ? c : indices[c];

				D3DXVECTOR3 normal;
				//if( this->render_normals ) {
				if( normals != NULL ) {
					normal = normals[index];
				}
				else {
					normal.x = 0;
					normal.y = 1;
					normal.z = 0;
				}
				//unsigned char sav_rgba[4] = {0, 0, 0, 0,};
				if( color_data != NULL && index % color_repeat == 0 ) {
					int ci = (alpha ? 4 : 3) * (index / color_repeat);
					unsigned char r = color_data[ci++];
					unsigned char g = color_data[ci++];
					unsigned char b = color_data[ci++];
					unsigned char a = 255;
					if( alpha ) {
						a = color_data[ci++];
					}
					color.x = (float)r;
					color.y = (float)g;
					color.z = (float)b;
					color.w = (float)a;
					/*sav_rgba[0] = r;
					sav_rgba[1] = g;
					sav_rgba[2] = b;
					sav_rgba[3] = a;*/
				}
				//D3DXVECTOR4 color;
				/*if( this->render_colors ) {
					const unsigned char *col_ptr = &this->render_array_colors[4*index];
					color.x = (float)col_ptr[0];
					color.y = (float)col_ptr[1];
					color.z = (float)col_ptr[2];
					color.w = (float)col_ptr[3];
					LOG("### %d %d %d %d\n", col_ptr[0], col_ptr[1], col_ptr[2], col_ptr[3]);
					LOG("    %d %d %d %d\n", sav_rgba[0], sav_rgba[1], sav_rgba[2], sav_rgba[3]);
				}
				else {
					//color.x = 255;
					//color.y = 255;
					//color.z = 255;
					//color.w = 255;
					color = this->color_current;
				}*/
				const D3DXVECTOR2 *texcoord_ptr = NULL;
				//if( this->render_texcoords ) {
				if( texcoords != NULL ) {
					texcoord_ptr = &texcoords[index];
				}
				else {
					texcoord_ptr = &texcoordDummy;
				}
				transformVertex(&buffer[j], &vertices[index], &normal, &color, texcoord_ptr, &transform_model, &transform_projection);
				buffer[j].sx = (int)(halfw * (1.0 + buffer[j].x));
				buffer[j].sy = (int)(halfh * (1.0 - buffer[j].y));
				/*if( buffer[j].x >= 0 && buffer[j].x < 2*halfw && buffer[j].y >= 0 && buffer[j].y < 2*halfh ) {
					this->write(buffer[j].sx, buffer[j].sy, color);
				}*/
			}
			this->drawPoly(&buffer[0], &buffer[1], &buffer[2]);
			if( mode == VertexArray::DRAWINGMODE_QUADS )
				this->drawPoly(&buffer[2], &buffer[3], &buffer[0]);
		}
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "SoftwareRenderer::renderElements render mode not supported\n"));
	}
	//LOG("<<<\n");
	V__EXIT;
}
