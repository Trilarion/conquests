#pragma once

#include <vector>
using std::vector;
#include <string>
using std::string;

class FontBuffer;
class InputSystem;

#include "Resource.h"
#include "VisionIface.h"
#include "Misc.h"

//typedef void (PaintPanel) (GraphicsEnvironment *genv,void *data);

/*class KeyRepeatHandler {
	int time_repeat;
	int lasttime_input;
public:
	KeyRepeatHandler() : time_repeat(-1), lasttime_input(-1) {
	}

	bool isNewRepeat(bool keydown, int thistime);
};*/

class Panel : public VisionObject, public virtual VI_Panel {
protected:
	Color bgcolor;
	Color fgcolor;
	Color highlightcolor;
	VI_Texture *texture;
	//bool opaque;
	Panel *parent;
	vector<Panel *> children;
	int x, y;
	VI_Panel_MouseClickEvent *mouseClickEventFunc;
	void *mouseClickEventData;
	VI_Panel_MouseReleaseEvent *mouseReleaseEventFunc;
	void *mouseReleaseEventData;
	VI_Panel_KeyDownEvent *keyDownEventFunc;
	void *keyDownEventData;
	VI_Panel_Paint *paintPanel;
	void *paintData;
	VI_Panel_Action *action;
	VI_Panel_Action *mouse_enter_action;
	VI_Panel_Action *mouse_exit_action;
	void *userData;
	bool has_shortcut;
	int shortcut;
	string popup_text;
	const FontBuffer *popup_font;
	bool show_popup;
	int popup_x, popup_y;
	int popup_width, popup_height;

	bool mouse_over;
	int last_event_time_ms;
	bool enabled;
	bool visible;
	bool has_input_focus;
	int fixed_x,fixed_y,width,height;
	// for scrollbars:
	bool scrollbar_v_pressed;
	bool scrollbar_v_pressed_up;
	bool scrollbar_v_pressed_down;
	bool scrollbar_h_pressed;
	bool scrollbar_h_pressed_up;
	bool scrollbar_h_pressed_down;

	void inputChildren(InputSystem *iS);
	void processMouseClickEventChildren(VI_MouseClickEvent *mouse_click_event);
	void processMouseReleaseEventChildren(VI_MouseReleaseEvent *mouse_release_event);
	void processKeydownEventChildren(VI_KeyEvent *key_event);
	void paintChildren(Graphics2D *g);
	void inputScrollbar(InputSystem *iS, bool *moved, size_t *new_position, size_t n_viewentries, size_t n_entries, bool vertical);
	void paintScrollbar(Graphics2D *g, size_t position, size_t n_viewentries, size_t n_entries, bool vertical);
public:
	/*V_Action *action_tag;
	V_Action *mouse_enter_action_tag;
	V_Action *mouse_exit_action_tag;*/

	bool from_left;
	bool from_top;

	Panel();
	virtual ~Panel();
	virtual V_CLASS_t getClass() const { return V_CLASS_PANEL; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_PANEL );
	}
	virtual size_t memUsage();

	virtual void input(InputSystem *iS);
	virtual void processMouseClickEvent(VI_MouseClickEvent *mouse_click_event);
	virtual void processMouseReleaseEvent(VI_MouseReleaseEvent *mouse_release_event);
	virtual void processKeydownEvent(VI_KeyEvent *key_event);
	virtual void paint(Graphics2D *g);
	virtual void validate();
	virtual void setBackground(Color bg);
	virtual void setForeground(Color fg);
	//virtual void setOpaque(bool opaque);
	void drawPopup(Graphics2D *g);

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}
	virtual const char *getName() const {
		return VisionObject::getName();
	}
	virtual void setName(const char *name) {
		VisionObject::setName(name);
	}*/

	/*virtual void setX(int x) {
		this->x = x;
	}
	virtual void setY(int y) {
		this->y = y;
	}*/
	virtual void setPos(int fixed_x, int fixed_y) {
		this->fixed_x = fixed_x;
		this->fixed_y = fixed_y;
		if( this->parent != NULL ) {
			this->parent->validate();
		}
	}
	virtual void setPosCentred(int fixed_x, int fixed_y) {
		this->setPos(fixed_x - width/2, fixed_y - height/2);
	}
	virtual int getX() const {
		return this->x;
	}
	virtual int getY() const {
		return this->y;
	}

	virtual void setEnabled(bool enabled,bool do_children);
	virtual bool isEnabled() const {
		return this->enabled;
	}
	virtual void setVisible(bool visible) {
		this->visible = visible;
	}
	virtual bool isVisible() const {
		return this->visible;
	}
	virtual void setHasInputFocus(bool has_input_focus) {
		this->has_input_focus = has_input_focus;
	}

	virtual void setPopupText(const char *popup_text, const VI_Font *font);

	virtual void addChildPanel(VI_Panel *panel,int x,int y);
	virtual int getNChildPanels() const {
		return children.size();
	}
	virtual VI_Panel *getChildPanel(size_t i) {
		return children.at(i);
	}
	virtual const VI_Panel *getChildPanel(size_t i) const {
		return children.at(i);
	}
	virtual bool removeChildPanel(VI_Panel *panel);
	virtual void removeAllChildPanels();
	virtual void detachFromParentPanel();

	virtual void setMeasureFrom(bool from_left, bool from_top) {
		this->from_left = from_left;
		this->from_top = from_top;
		this->validate();
	}
	virtual void setAction(VI_Panel_Action *action);
	virtual void setMouseEnterAction(VI_Panel_Action *mouse_enter_action) {
		this->mouse_enter_action = mouse_enter_action;
	}
	virtual void setMouseExitAction(VI_Panel_Action *mouse_exit_action) {
		this->mouse_exit_action = mouse_exit_action;
	}
	virtual void setMouseClickEventFunc(VI_Panel_MouseClickEvent *mouseClickEventFunc,void *mouseClickEventData);
	virtual void setMouseReleaseEventFunc(VI_Panel_MouseReleaseEvent *mouseReleaseEventFunc,void *mouseReleaseEventData);
	virtual void setKeyDownEventFunc(VI_Panel_KeyDownEvent *keyDownEventFunc,void *keyDownEventData);
	virtual void setPaintFunc(VI_Panel_Paint *paintPanel,void *paintData);
	virtual void setKeyShortcut(int shortcut) {
		this->has_shortcut = true;
		this->shortcut = shortcut;
	}
	virtual void disableKeyShortcut() {
		this->has_shortcut = false;
		this->shortcut = -1;
	}
	virtual void setForeground(float r,float g,float b);
	virtual void setForeground(float r,float g,float b,float a);
	virtual void setBackground(float r,float g,float b);
	virtual void setBackground(float r,float g,float b,float a);
	virtual void setBackgroundAlphai(int alpha);
	virtual void setHighlightColor(unsigned char r, unsigned char g, unsigned char b);
	virtual void setTexture(VI_Texture *texture) {
		this->texture = texture;
	}
	virtual void setSize(int width, int height) {
		this->width = width;
		this->height = height;
		this->validate();
	}
	virtual void getSize(int *width, int *height) const {
		*width = this->width;
		*height = this->height;
	}
	virtual int getWidth() const {
		return width;
	}
	virtual int getHeight() const {
		return height;
	}
	virtual void setUserData(void *userData) {
		this->userData = userData;
	}
	virtual void *getUserData() const {
		return userData;
	}
	virtual VI_Panel *find(int fx, int fy);
};

class FlowPanel : public Panel {
	bool across;
	int size;
	int marginX,marginY;
public:
	FlowPanel(bool across,int size);
	virtual V_CLASS_t getClass() const { return V_CLASS_FLOWPANEL; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_FLOWPANEL || Panel::isSubClass(v_class) );
	}

	virtual void addChildPanel(VI_Panel *panel);
	virtual void validate();
};

class Button : public Panel, public virtual VI_Button {
	string text;
	const FontBuffer *font;
	bool border;

	virtual void init(const char *text,const FontBuffer *font);
public:
	//Button();
	Button(const char *text,const FontBuffer *font);
	virtual ~Button();
	virtual V_CLASS_t getClass() const { return V_CLASS_BUTTON; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_BUTTON || Panel::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = Panel::memUsage();
		size += text.capacity();
		return size;
	}

	//virtual void input(InputSystem *iS);
	virtual void paint(Graphics2D *g);

	// interface
	virtual void setText(const char *text);
	virtual void setBorder(bool border) {
		this->border = border;
	}
};

class ImageButton : public Panel, public virtual VI_ImageButton {
	VI_Texture *texture;
	bool has_tex_coords;
	float tu[4], tv[4];
public:
	ImageButton(VI_Texture *texture);
	ImageButton(VI_Texture *texture, int width, int height);
	virtual ~ImageButton();
	virtual V_CLASS_t getClass() const { return V_CLASS_IMAGEBUTTON; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_IMAGEBUTTON || Panel::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = Panel::memUsage();
		// todo:
		return size;
	}

	virtual void setTexCoords(const float tu[4], const float tv[4]) {
		this->has_tex_coords = true;
		for(int i=0;i<4;i++) {
			this->tu[i] = tu[i];
			this->tv[i] = tv[i];
		}
	}

	//virtual void input(InputSystem *iS);
	virtual void paint(Graphics2D *g);
};

class Cycle : public Panel, public virtual VI_Cycle {
	/*char **entries;
	int n_entries;*/
	vector<string> entries;

	const FontBuffer *font;

	virtual void init(char **entries,const FontBuffer *font);
	void updateWidth(const char *entry);
public:
	size_t active;

	Cycle();
	Cycle(char **entries,const FontBuffer *font);
	virtual ~Cycle();
	virtual V_CLASS_t getClass() const { return V_CLASS_CYCLE; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_CYCLE || Panel::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = Panel::memUsage();
		//size += n_entries * sizeof(char *);
		// TODO: entries
		return size;
	}

	//virtual void input(InputSystem *iS);
	virtual void processMouseClickEvent(VI_MouseClickEvent *mouse_click_event);
	virtual void paint(Graphics2D *g);

	// interface
	virtual size_t getActive() const {
		return this->active;
	}
	virtual size_t getNEntries() const {
		return this->entries.size();
	}
	virtual void addEntry(const char *entry);

	virtual void setActive(size_t active) {
		this->active = active;
	}
};

class Listbox : public Panel, public virtual VI_Listbox {
	vector<string> entries;
	int position;
	int scroll_x;
	int max_width_pixels;

	//KeyRepeatHandler keyRepeatHandler;
	const FontBuffer *font;
	int active;
	bool allow_select;

	void prepare();
	bool hasHScrollbar() const;
	int getNViewEntries() const;
public:

	Listbox(int width,int height,char **entries,const FontBuffer *font);
	Listbox(int width,int height,const string *entries,int n_entries,const FontBuffer *font);
	virtual ~Listbox() {
	}
	virtual V_CLASS_t getClass() const { return V_CLASS_LISTBOX; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_LISTBOX || Panel::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = Panel::memUsage();
		//size += n_entries * sizeof(char *);
		// TODO: entries
		return size;
	}

	virtual void input(InputSystem *iS);
	virtual void processMouseClickEvent(VI_MouseClickEvent *mouse_click_event);
	virtual void processKeydownEvent(VI_KeyEvent *key_event);
	virtual void paint(Graphics2D *g);

	// interface
	virtual int getNEntries() const {
		return (int)this->entries.size();
	}
	virtual int getActive() const {
		return this->active;
	}
	virtual void setActive(int active) {
		this->active = active;
	}
	virtual void setAllowSelect(bool allow_select) {
		this->allow_select = allow_select;
	}
	virtual bool isAllowSelect() const {
		return this->allow_select;
	}
	virtual void reset(const string *entries, int n_entries);
	virtual void reset(char **entries);
};

class StringGadget : public Panel, public virtual VI_Stringgadget {
private:
	size_t view_pos; // used only for StringGadget - to do horizontal scrolling

	void updateView();
protected:
	//StringBuffer *text;
	string text;        
	const FontBuffer *font;
	bool read_only;
	size_t cursor_pos; // where the cursor is, also allows vertical scrolling on TextField
	string blocked_characters;
	/*int time_repeat;
	int lasttime_input;*/
	//KeyRepeatHandler keyRepeatHandler;
	size_t max_len;
	VI_Panel_Action *enter_action;
	int cursor_blink_time_start_ms;

	virtual void init(const FontBuffer *font,int width);
	//static bool acceptKey(int key);
public:
	//V_Action *enter_action_tag;

	StringGadget(const FontBuffer *font,int width);
	StringGadget();
	virtual ~StringGadget();
	virtual V_CLASS_t getClass() const { return V_CLASS_STRINGGADGET; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_STRINGGADGET || Panel::isSubClass(v_class) );
	}
	virtual size_t memUsage() {
		size_t size = Panel::memUsage();
		// TODO: text
		return size;
	}

	//virtual void input(InputSystem *iS);
	virtual void processKeydownEvent(VI_KeyEvent *key_event);
	virtual void paint(Graphics2D *g);

	// interface
	virtual void setReadOnly(bool read_only) {
		this->read_only = read_only;
	}
	virtual void setMaxLen(size_t max_len) {
		this->max_len = max_len;
	}
	virtual void addText(const char *str);
	virtual void clear();
	virtual void getText(char **text,size_t *length) const;
	virtual const char *getText() const {
		return this->text.c_str();
	}
	virtual void setBlockedCharacters(const char *blocked_characters) {
		this->blocked_characters = blocked_characters;
	}
	virtual void setEnterAction(VI_Panel_Action *action);
};

class TextField : public StringGadget, public virtual VI_Textfield {
	//int cline_off;
	size_t cpos;
	//Vector *line_indxs;
	vector<size_t> *line_indxs;
	bool at_end;
	void scrollTo(size_t new_line);
	void scroll(bool up, size_t step);
public:
	TextField(const FontBuffer *font,int width,int height);
	TextField();
	virtual ~TextField();
	virtual V_CLASS_t getClass() const { return V_CLASS_TEXTFIELD; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_TEXTFIELD || StringGadget::isSubClass(v_class) );
	}

	virtual void paint(Graphics2D *g);
	virtual void input(InputSystem *iS);
	virtual void processMouseClickEvent(VI_MouseClickEvent *mouse_click_event);
	virtual void processKeydownEvent(VI_KeyEvent *key_event);

	// interface
	virtual void addText(const char *str);
	virtual void setCPos(size_t cpos) {
		this->cpos = cpos;
	}
};
