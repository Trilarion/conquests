#pragma once

#include "VisionUtils.h"
#include "Resource.h"
#include "VisionIface.h"

class Texture;
class GraphicsEnvironment;

const int MAX_TEXUNITS = 8;

class RendererInfo {
public:
	static bool multiSample;
	static unsigned int maxLights;
	//static int maxTextureUnits;
	static bool hardwareOcclusion;
	static bool framebufferObject;

	static void init() {
		multiSample = false;
		maxLights = 0;
		//maxTextureUnits = 0;
		hardwareOcclusion = false;
		framebufferObject = false;
	}
};

class VertexArray {
//protected:
public:
	void *data;
	bool stream;
	int dim;
	int size;

public:
	enum DrawingMode {
		DRAWINGMODE_POINTS = 0,
		DRAWINGMODE_LINES = 1,
		DRAWINGMODE_LINE_LOOP = 2,
		DRAWINGMODE_LINE_STRIP = 3,
		DRAWINGMODE_TRIANGLES = 4,
		DRAWINGMODE_TRIANGLE_STRIP = 5,
		DRAWINGMODE_TRIANGLE_FAN = 6,
		DRAWINGMODE_QUADS = 7,
		DRAWINGMODE_QUAD_STRIP = 8,
		DRAWINGMODE_POLYGON = 9
	};
	enum DataType {
		DATATYPE_INDICES = 0,
		DATATYPE_VERTICES = 1,
		DATATYPE_NORMALS = 2,
		DATATYPE_COLORS = 3,
		DATATYPE_TEXCOORDS = 4
	};
	DataType data_type;
	int tex_unit; // if DATATYPE_TEXCOORDS

	virtual ~VertexArray() {
	}

	//virtual void free()=0;
	virtual bool hasVBO() const=0;
	/*void updateStream() {
		assert( stream );
		if( vbo != 0 ) {
			glBufferSubDataARB( GL_ARRAY_BUFFER_ARB, 0, size, data );
		}
	}*/
	virtual void renderVertices(bool new_data, int offset) const=0;
	virtual void renderNormals(bool new_data, int offset) const=0;
	virtual void renderTexCoords(bool new_data, int offset) const=0;
	virtual void renderTexCoords(bool new_data, int offset, int unit) const=0;
	virtual void renderColors(bool new_data, int offset) const=0;
	virtual void update() const=0;
	virtual void updateIndices() const=0;
	virtual void renderElements(DrawingMode mode, int n_this_renderlength, int n_this_vertices) const=0;
};

class Shader {
	friend class ShaderEffectSimple;
	friend class Renderer;
protected:
	bool vertex_shader;

	virtual void enable() const=0;
	//virtual void disable() const=0;

	virtual void setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const=0;
	//virtual void setHardwareAnimationAlpha(float alpha) const=0;
	virtual void SetUniformParameter1f(const char *pszParameter, float p1) const=0;
	virtual void SetUniformParameter2f(const char *pszParameter, float p1, float p2) const=0;
	virtual void SetUniformParameter3f(const char *pszParameter, float p1, float p2, float p3) const=0;
	virtual void SetUniformParameter4f(const char *pszParameter, float p1, float p2, float p3, float p4) const=0;
	/*virtual void SetUniformParameter2fv(const char *pszParameter, const float v[2]) const=0;
	virtual void SetUniformParameter3fv(const char *pszParameter, const float v[3]) const=0;*/
	virtual void SetUniformParameter4fv(const char *pszParameter, const float v[4]) const=0;
	virtual void SetModelViewProjMatrix(const char *parameter) const=0;

	//static Shader *createShader(VI_GraphicsEnvironment *genv,const char *file,const char *func,bool vertex_shader);

	Shader(bool vertex_shader) : vertex_shader(vertex_shader) {
	}
public:
	virtual ~Shader();

	//void setDefaults() const;
	/*virtual void setMatSpecular(const float specular[4]) const=0;
	virtual void setLightPos(float p1, float p2, float p3, float d1, float d2, float d3) const=0;
	virtual void setLightAmbient(float rf, float gf, float bf) const=0;
	virtual void setLightDiffuse(float rf, float gf, float bf) const=0;
	virtual void setLightSpecular(float rf, float gf, float bf) const=0;
	virtual void setLightAtten(float a, float b, float c) const=0;*/
	//virtual void setTexture(const Texture *texture) const=0;
	//virtual void setSecondaryTexture(const Texture *texture) const=0;
	//virtual void setBumpTexture(const Texture *texture) const=0;
};

/*class ShaderEffect;
class GraphicsEnvironment;
typedef void (VI_ShaderEffect_Callback) (const ShaderEffect *shader, const GraphicsEnvironment *genv);*/

class ShaderEffect {
	//VI_ShaderEffect_Callback *callback;
protected:
	ShaderEffect() /*: callback(NULL)*/ {
	}
public:
	virtual ~ShaderEffect() {
	}

	virtual void enable(Renderer *renderer) const=0;
	virtual void setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const=0;
	virtual void SetModelViewProjMatrix(const char *parameter) const=0;
	virtual void SetUniformParameter1f(const char *parameter, float p1) const=0;
	virtual void SetUniformParameter2f(const char *parameter, float p1, float p2) const=0;
	virtual void SetUniformParameter3f(const char *parameter, float p1, float p2, float p3) const=0;
	virtual void SetUniformParameter4f(const char *parameter, float p1, float p2, float p3, float p4) const=0;
	virtual void SetUniformParameter4fv(const char *parameter, const float v[4]) const=0;
	/*void setCallback(VI_ShaderEffect_Callback *callback) {
		this->callback = callback;
	}
	VI_ShaderEffect_Callback *getCallback() const {
		return callback;
	}*/
};

// This class simulates an "Effect" by simply encapsulating a separate vertex and pixel shader.
class ShaderEffectSimple : public ShaderEffect {
	const Shader *vertex_shader;
	const Shader *pixel_shader;
public:
	ShaderEffectSimple(const Shader *vertex_shader, const Shader *pixel_shader);
	virtual ~ShaderEffectSimple();

	virtual void enable(Renderer *renderer) const;
	virtual void setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const;
	virtual void SetModelViewProjMatrix(const char *parameter) const;
	virtual void SetUniformParameter1f(const char *parameter, float p1) const;
	virtual void SetUniformParameter2f(const char *parameter, float p1, float p2) const;
	virtual void SetUniformParameter3f(const char *parameter, float p1, float p2, float p3) const;
	virtual void SetUniformParameter4f(const char *parameter, float p1, float p2, float p3, float p4) const;
	virtual void SetUniformParameter4fv(const char *parameter, const float v[4]) const;
};

class TransformationMatrix {
public:
	virtual void load() const=0;
	virtual void save()=0;
	virtual void mult() const=0;

	virtual void set(int indx, float value)=0;
	virtual float &operator[](const int i)=0;
	//virtual const float &operator[](const int i) const=0;
	virtual float get(int indx) const=0;
	virtual float get(int row, int col) const=0;
	virtual Vector3D getPos() const=0;
	virtual Vector3D transformVec(const Vector3D &in) const=0;
};

class Texture : public VisionObject, public VI_Texture {
protected:
	WrapMode wrap_mode;
	DrawMode draw_mode;
	//char *filename; // if loaded from file directly
	//string filename;
	//bool mipmap;
	//bool load(char *filename,bool mask, struct rgb_struct *maskcol);

	Texture();
public:
	//int w,h;

	//Texture(bool mipmap,int w,int h);
	//Texture(Image2D *image);
	virtual ~Texture();

	virtual V_CLASS_t getClass() const { return V_CLASS_TEXTURE; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		//return ( v_class == V_CLASS_TEXTURE || Image2D::isSubClass(v_class) );
		return ( v_class == V_CLASS_TEXTURE );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}

	//void init(Image2D *image,bool mask, struct rgb_struct *maskcol);
	virtual Texture *copy()=0;
	virtual void copyTo(int width, int height)=0;

	//static Texture *loadTexture(char *filename);
	//static Texture *loadTextureWithMask(char *filename, struct rgb_struct maskcol);

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/
	virtual void setWrapMode(WrapMode wrap_mode) {
		this->wrap_mode = wrap_mode;
	}
	virtual void setDrawMode(DrawMode draw_mode) {
		this->draw_mode = draw_mode;
	}
};

class FramebufferObject {
public:

	FramebufferObject() {}
	virtual ~FramebufferObject() {}

	virtual bool generate(int width,int height,bool want_depth_stencil)=0;
	virtual void free()=0;

	virtual void bind() const=0;
	virtual void enableTexture() const=0;
};

/*class QuadricObjectData {
};

class SphereQuadricObjectData : public QuadricObjectData {
	double radius;
	int slices;
	int stacks;

	SphereQuadricObjectData(double radius, int slices, int stacks) : radius(radius), slices(slices), stacks(stacks) {
	}
};*/

class QuadricObject {
	/*enum Type {
		TYPE_SPHERE = 0
	};
	Type type;
	QuadricObjectData *data;*/

	//QuadricObject(Type type) : type(type), data(NULL) {
	//}
public:

	/*virtual ~QuadricObject() {
		if( data != NULL )
			delete data;
	}*/
	virtual ~QuadricObject() {
	}

	virtual void draw() const=0;

	//static virtual QuadricObject *createSphere(float radius, int slices, int stacks)=0;
};

class FontBuffer : public VisionObject, public VI_Font {
public:
	FontBuffer();
	virtual ~FontBuffer() {}

	virtual V_CLASS_t getClass() const { return V_CLASS_FONT; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_FONT );
	}

	// interface
	virtual V_TAG_t getTag() const {
		return this->tag;
	}

	virtual int writeTextExt(int x,int y,const char *text,...) const;
};

class Graphics2D : public VisionObject, public VI_Graphics2D {
protected:
//class Graphics2D {
	GraphicsEnvironment *genv;
	short curr_x, curr_y;
	Texture *texture;

	Graphics2D(GraphicsEnvironment *genv);

public:

	virtual V_CLASS_t getClass() const { return V_CLASS_GRAPHICS2D; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_GRAPHICS2D );
	}
	virtual size_t memUsage() {
		size_t size = sizeof(this);
		return size;
	}
	Renderer *getRenderer() const;

	// interface now in VI_Graphics2D
	virtual void setTexture(VI_Texture *texture) {
		this->texture = static_cast<Texture *>(texture); // cast to the internal representation
	}
	virtual void move(short x,short y);
};

class Renderer {
protected:
	GraphicsEnvironment *genv;
	TransformationMatrix *static_transformationMatrix;

	// current shader pointers
	/*const Shader *c_vertex_shader;
	const Shader *c_pixel_shader;*/
	const ShaderEffect *c_shader;
	//vector<Shader *> shaders;
	vector<ShaderEffect *> shader_effects;

	float z_near, z_far;
	bool persp;

	void addShaderEffect(ShaderEffect *shader_effect) {
		this->shader_effects.push_back(shader_effect);
	}
public:
	//Renderer(Graphics3D *g3);
	Renderer(GraphicsEnvironment *genv);
	//Renderer();
	virtual ~Renderer();

	float getZNear() const {
		return z_near;
	}
	float getZFar() const {
		return z_far;
	}
	bool getPersp() const {
		return persp;
	}

	virtual void beginScene() const {}
	virtual void endScene() const {}
	virtual void drawFrameTest() const {}

	//virtual void freeHDRBuffers()=0;

	// factories
	virtual TransformationMatrix *createTransformationMatrix()=0;
	virtual TransformationMatrix *getStaticTransformationMatrix() {
		if( static_transformationMatrix == NULL ) {
			static_transformationMatrix = createTransformationMatrix();
		}
		return static_transformationMatrix;
	}
	//virtual Shader *createShaderCG(const char *file, const char *func, bool vertex_shader)=0;
	virtual Texture *createTexture(int w,int h,bool alpha,bool floating_point)=0;
	virtual Texture *createTexture(VI_Image *image)=0;
	virtual Texture *createTextureWithMask(VI_Image *image, unsigned char maskcol[3])=0;
	virtual Texture *loadTexture(const char *filename)=0;
	virtual Texture *loadTextureWithMask(const char *filename, unsigned char maskcol[3])=0;
	virtual FramebufferObject *createFramebufferObject()=0;
	//virtual QuadricObject *createQuadricObject() const=0;
	virtual QuadricObject *createSphereObject(float radius, int slices, int stacks)=0;
	virtual VertexArray *createVertexArray(VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim)=0;
	virtual FontBuffer *createFont(const char *family,int pt)=0;
	virtual FontBuffer *createFont(const char *family,int pt,int style)=0;

	// general commands
	virtual void setViewport(int width, int height)=0;
	virtual void resizeScene(float frustum[6][4], int width, int height, bool persp, float z_near, float z_far, float fovy)=0;
	virtual void setClearColor(float r, float g, float b, float a)=0;
	virtual void clear(bool color, bool depth, bool stencil) const=0;

	// render states
	virtual void setToDefaults()=0;

	enum RenderState {
		RENDERSTATE_DEPTH_READWRITE,
		RENDERSTATE_DEPTH_READONLY,
		RENDERSTATE_DEPTH_NONE,

		RENDERSTATE_DEPTHFUNC_EQUAL,
		RENDERSTATE_DEPTHFUNC_LESS,

		RENDERSTATE_BLEND_BOTH,
		RENDERSTATE_BLEND_ADDITIVE,
		RENDERSTATE_BLEND_TRANSPARENCY,
		RENDERSTATE_BLEND_REVERSE_TRANSPARENCY,
		RENDERSTATE_BLEND_ONE_BY_SRCALPHA,
		RENDERSTATE_BLEND_ONE_BY_ONEMINUSSRCALPHA,
		RENDERSTATE_BLEND_DST_BY_ZERO,
		RENDERSTATE_BLEND_NONE,

		RENDERSTATE_ALPHA_TEST_ON,
		RENDERSTATE_ALPHA_TEST_OFF,

		RENDERSTATE_STENCIL_REPLACE,
		RENDERSTATE_STENCIL_DRAWATSTENCIL_OVERDRAW,
		RENDERSTATE_STENCIL_DRAWATNOSTENCIL,
		RENDERSTATE_STENCIL_SHADOWPASS_ONE,
		RENDERSTATE_STENCIL_SHADOWPASS_TWO,
		RENDERSTATE_STENCIL_SHADOWPASS_OFF,
		RENDERSTATE_STENCIL_NONE,

		RENDERSTATE_FOG_LINEAR,
		RENDERSTATE_FOG_EXP,
		RENDERSTATE_FOG_EXP2,
		RENDERSTATE_FOG_NONE
	};
	virtual void setDepthBufferMode(RenderState state)=0;
	virtual void setDepthBufferFunc(RenderState state)=0;
	virtual void setBlendMode(RenderState state)=0;
	virtual void setAlphaTestMode(RenderState state)=0;
	virtual void setStencilMode(RenderState state)=0;
	virtual void setFogMode(RenderState state, float start, float end, float density)=0;
	virtual void setFogColor(const float color[4])=0;

	virtual int nShadowPasses() const=0;

	virtual void enableCullFace(bool enable)=0;
	virtual void setFrontFace(bool ccw)=0;
	virtual void enableTexturing(bool enable)=0;
	virtual void enableMultisample(bool enable)=0;
	virtual void enableColorMask(bool enable)=0;
	virtual void enableClipPlane(const double pl[4])=0;
	virtual void reenableClipPlane()=0;
	virtual void disableClipPlane()=0;
	virtual void enableScissor(int x,int y,int w,int h)=0;
	virtual void reenableScissor()=0;
	virtual void disableScissor()=0;
	//virtual void disableFog() const=0;
	virtual void pushAttrib(bool viewport)=0;
	virtual void popAttrib() const=0;

	// fixed function only functions
	virtual void setFixedFunctionMatSpecular(const float specular[4])=0;
	void setFixedFunctionMatSpecularOff() {
		const float zero[4] = {0.0, 0.0, 0.0, 0.0};
		setFixedFunctionMatSpecular(zero);
	}
	//virtual void setFixedFunctionMatColor(const float color[4])=0;
	virtual void enableFixedFunctionLighting(bool enable)=0;
	virtual void setFixedFunctionAmbientLighting(const float col[4])=0;
	virtual void enableFixedFunctionLight(unsigned int index, const float ambient[4], const float diffuse[4], const float specular[4], const float attenuation[3], const float position[3], bool directional)=0;
	virtual void disableFixedFunctionLights()=0;
	virtual void disableFixedFunctionLight(unsigned int index)=0;

	// drawing
	virtual void draw(const Vector3D *p0, const Vector3D *p1, const unsigned char rgba[4])=0;
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count)=0;
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count, const unsigned char *color_data, bool alpha, int color_repeat)=0;
	virtual bool supportsQuads() const=0;

	enum VertexFormat {
		//VERTEXFORMAT_PNTC = 0,
		VERTEXFORMAT_UNDEFINED = -1,
		VERTEXFORMAT_P = 0,
		//VERTEXFORMAT_P_MC = 1,
		//VERTEXFORMAT_PC = 2,
		//VERTEXFORMAT_PN_MC = 3,
		//VERTEXFORMAT_PNC = 4,
		//VERTEXFORMAT_PT0_MC = 5,
		VERTEXFORMAT_PT0C = 6,
		//VERTEXFORMAT_PNT0_MC = 7,
		VERTEXFORMAT_PNT0C = 8,
		VERTEXFORMAT_PT0T1C = 9,
		//VERTEXFORMAT_PNT0T1_MC = 10,
		VERTEXFORMAT_PNT0T1C = 11,
		//VERTEXFORMAT_PNT0T2_MC = 12,
		//VERTEXFORMAT_PNT0T2C = 13,
		VERTEXFORMAT_PNT0T1T2C = 14
	};
	//virtual void startArrays(VertexFormat vertexFormat, bool material_color, bool have_texcoords0, bool have_texcoords1)=0;
	virtual void startArrays(VertexFormat vertexFormat)=0;
	virtual void endArrays()=0;

	// texture unit handling
	virtual void activeTextureUnit(int unit)=0;
	//virtual void activeClientTextureUnit(int uint) const=0;

	// transformation matrices
	virtual void switchToModelview()=0;
	virtual void switchToProjection()=0;
	virtual void switchToTexture()=0;
	virtual void ortho2D(int width, int height)=0;
	virtual void pushMatrix()=0;
	virtual void popMatrix()=0;
	virtual void loadIdentity()=0;
	virtual void translate(float x,float y,float z)=0;
	virtual void rotate(float angle,float x,float y,float z)=0;
	virtual void scale(float x,float y,float z)=0;
	virtual void transform(const Vector3D *pos, const Quaternion *rot, bool infinite)=0;
	virtual void transform(const Quaternion *rot)=0;
	virtual void transformInverse(const Quaternion *rot)=0;
	virtual void getViewOrientation(Vector3D *right,Vector3D *up)=0;
	virtual void getViewDirection(Vector3D *dir)=0;

	// frame buffers
	/*virtual bool generateHdrFbo(int width, int height)=0;
	virtual void bindHdrFbo() const=0;
	virtual void enableHdrFbo() const=0;*/
	virtual void unbindFramebuffer() const=0;

	// current shaders
	/*const Shader *getActiveVertexShader() const {
		return this->c_vertex_shader;
	}
	const Shader *getActivePixelShader() const {
		return this->c_pixel_shader;
	}*/
	const ShaderEffect *getActiveShader() const {
		return this->c_shader;
	}
	// should not call directly! use Shader::enable() and Shader::disable() instead!
	/*void setActiveVertexShader(const Shader *shader) {
		this->c_vertex_shader = shader;
	}
	void setActivePixelShader(const Shader *shader) {
		this->c_pixel_shader = shader;
	}*/
	void setActiveShader(const ShaderEffect *shader) {
		this->c_shader = shader;
	}
	/*void addShader(Shader *shader) {
		this->shaders.push_back(shader);
	}*/

	virtual bool isYTextureInverted() const {
		// false for OpenGL style
		// true for D3D 9 style
		return false;
	}

	virtual bool hasShaders() const=0;
	virtual Shader *createShaderCG(const char *file, const char *func, bool vertex_shader)=0;
	ShaderEffect *createShaderEffectSimpleCG(const char *file, const char *func_vs,const char *func_ps);
	virtual ShaderEffect *createShaderEffectCGFX(const char *file,const char **args)=0;
	virtual void disableShaders()=0;
};
