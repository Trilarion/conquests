//---------------------------------------------------------------------------
#include <cstring>
#include <ctime>
#include <algorithm>
using std::swap;
using std::min;
using std::max;

#include "Object3D.h"
#include "GraphicsEngine.h"
#include "GraphicsEnvironment.h"
#include "Renderer.h"
#include "RenderData.h"
#include "Entity.h"

//---------------------------------------------------------------------------

const float shadow_alpha_c = 0.6f;

ObjectData::ObjectData() : VisionObject() {
	//Vision::addObjectData(this);
	this->shadowplane = NULL;
	//this->setName("");
	//boundingRadius = 0.0;
	//boundingRadiusSquared = 0.0;
	n_frames = 0;
	//this->blendtype = V_BLENDTYPE_NONE;
	//this->alpha = 255;
	//this->mirror_alpha = 255;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	//this->updateFunc = NULL;
	//this->updateData = NULL;
	/*this->renderDatas = NULL;
	this->n_renderDatas = 0;*/
	/*this->shadow_renderDatas = NULL;
	this->n_shadow_renderDatas = 0;*/
	this->shadow_caster = false;
	/*this->vertex_shader_ambient = NULL;
	this->pixel_shader_ambient = NULL;
	this->vertex_shader_directional = NULL;
	this->pixel_shader_directional = NULL;
	this->vertex_shader_point = NULL;
	this->pixel_shader_point = NULL;*/

	/*for(int i=0;i<V_ANIMSTATE_MD2_NSTATES;i++) {
		frame_starts[i] = 0;
		frame_ends[i] = 0;
	}*/
	this->has_boundingBox = false;
}

ObjectData::~ObjectData() {
	/*if( this->vertex_shader_ambient != NULL )
	delete this->vertex_shader_ambient;
	if( this->pixel_shader_ambient != NULL )
	delete this->pixel_shader_ambient;
	if( this->vertex_shader_directional != NULL )
	delete this->vertex_shader_directional;
	if( this->pixel_shader_directional != NULL )
	delete this->pixel_shader_directional;
	if( this->vertex_shader_point != NULL )
	delete this->vertex_shader_point;
	if( this->pixel_shader_point != NULL )
	delete this->pixel_shader_point;*/
	/*if( renderDatas != NULL )
		delete [] renderDatas;*/

	for(size_t i=0;i<this->getNRenderData();i++) {
		delete this->renderDatas[i];
	}
	for(size_t i=0;i<this->getNShadowRenderData();i++) {
		delete this->shadow_renderDatas[i];
	}
	//delete [] this->shadow_renderDatas;

	//Vision::removeObjectData(this);
}

/*void ObjectData::setName(char *name) {
strncpy(this->name,name,NAMELEN-1);
this->name[NAMELEN-1] = '\0';
this->shadowplane = NULL;
}

char *ObjectData::getName() {
return this->name;
}*/

size_t ObjectData::memUsage() {
	size_t size = sizeof(this);
	/*if( shadowPlane != NULL )
	size += shadowPlane->memUsage();*/
	//size += renderData.memUsage();
	//for(int i=0;i<n_renderDatas;i++)
	for(size_t i=0;i<renderDatas.size();i++) {
		//size += renderDatas[i].memUsage();
		size += renderDatas.at(i)->memUsage();
	}
	return size;
}

void ObjectData::prepare() {
	//LOG("ObjectData::prepare() %d ( %d )\n", this->tag, this);
	if( !this->calc_bounds ) {
		//LOG("ObjectData::prepare() calc_bounds %d ( %d )\n", this->tag, this);
		//this->setBoundingRadius();
		this->setBounds();
	}
	if( !this->calc_renderdata || !this->calc_shadows ) {
		//LOG("ObjectData::prepare() calc_renderdata %d ( %d )\n", this->tag, this);
		if( !this->calc_shadows ) {
			for(size_t i=0;i<this->getNShadowRenderData();i++) {
				delete this->shadow_renderDatas[i];
			}
			this->shadow_renderDatas.clear();
		}

		if( !this->calc_renderdata ) {
			this->prepareRenderDatas();
		}

		// create shadow meshes
		if( !this->calc_shadows && this->isShadowCaster() ) {
			for(size_t i=0;i<this->getNRenderData();i++) {
				//this->shadow_renderDatas[i] = this->renderDatas[i].copy(true, false, false, false);
				RenderData *s_rd = this->renderDatas[i]->copy(true, false, false, false);
				s_rd->shareVertices();
				//s_rd->simplify(); // actually slows things down!
				this->shadow_renderDatas.push_back(s_rd);
			}

			if( this->getNShadowRenderData() > 1 ) {
				// now combine
				RenderData *first = this->shadow_renderDatas[0];
				for(size_t i=1;i<this->getNShadowRenderData();i++) {
					//first->combine( this->shadow_renderDatas[1] );
					first->combine( this->shadow_renderDatas.at(i) );
				}
				first->prepare(true);
				this->shadow_renderDatas.clear();
				this->shadow_renderDatas.push_back(first);
			}
		}

		this->calc_renderdata = true;
		this->calc_shadows = true;
	}
}

void ObjectData::prepareRenderDatas() {
	//this->calc_renderdata = true;
	//for(int i=0;i<this->n_renderDatas;i++) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		//this->renderDatas[i].prepare(this->isShadowCaster());
		//this->renderDatas[i].prepare(false);
		this->renderDatas[i]->prepare(false);
	}
	// shadow renderDatas now prepared when created in ObjectData::prepare()
	/*for(int i=0;i<this->getNShadowRenderData();i++)
		this->shadow_renderDatas[i]->prepare(this->isShadowCaster());*/
}

void ObjectData::setSpeculari(unsigned char r, unsigned char g, unsigned char b) {
	Color col;
	col.seti(r,g,b);
	this->setSpecular(&col);
}

void ObjectData::setShadowCaster(bool shadow_caster) {
	if( this->shadow_caster != shadow_caster ) {
		this->shadow_caster = shadow_caster;
		//this->calc_renderdata = false;
		this->calc_shadows = false;
		if( !Vision::isLocked() ) {
			this->prepare();
		}
	}
}

void ObjectData::setShadowPlane(VI_GraphicsEnvironment *genv, VI_ShadowPlane *shadowPlane, unsigned char alpha) {
	GraphicsEnvironment *d_genv = static_cast<GraphicsEnvironment *>(genv); // cast to the internal representation
	ShadowPlane *d_shadowPlane = static_cast<ShadowPlane *>(shadowPlane); // cast to the internal representation
	// can't be used to supply shaders for render-to-texture
	if( d_shadowPlane->isRenderToTexture() ) {
		Vision::setError(new VisionException(this, VisionException::V_GENERAL_ERROR, "Can't set render-to-texture shadowplane"));
	}
	this->setShadowPlane(d_genv->getRenderer(), d_shadowPlane, NULL, 0);
	if( d_shadowPlane != NULL ) {
		this->setAlphai(alpha);
	}
}

void ObjectData::setShadowPlane(Renderer *renderer, ShadowPlane *shadowplane, const ObjectDataShaders *shaders, int texture_size) {
	if( shadowplane != NULL && shadowplane->isRenderToTexture() && shaders != NULL ) {
		this->shaders = *shaders;
		// now create new texture for reflection
		if( shadowplane->getRenderTexture() == NULL ) {
			//shadowplane->setRenderTexture( renderer->createTexture(false, texture_size, texture_size) );
			shadowplane->setRenderTexture( renderer->createTexture(texture_size, texture_size, true, false) );
		}
		this->setSecondaryTexture(shadowplane->getRenderTexture());
	}
	this->shadowplane = shadowplane;
	this->calc_renderdata = false;
	this->calc_shadows = false; // not sure if this is needed to be recalculated?
}

#if 0
void ObjectData::setShadowPlane(Graphics3D *g3,ShadowPlane *shadowplane) {
	/*if( this->shadowplane == shadowplane )
	return;
	if( this->shadowplane == NULL ) {*/
	/*if( shadowplane != NULL && shadowplane->render_to_texture && g3->getGraphicsEnvironment()->cgContext == NULL ) {
	// need shaders for render-to-texture
	shadowplane->render_to_texture = NULL;
	}*/
	if( shadowplane != NULL && shadowplane->isRenderToTexture() && g3->getGraphicsEnvironment()->hasShaders() && g3->hasWaterShaders() ) {
		this->shaders.vertex_shader_ambient = g3->getWaterAmbientVertexShader();
		this->shaders.pixel_shader_ambient = g3->getWaterAmbientPixelShader();
		this->shaders.vertex_shader_directional = g3->getWaterDirectionalVertexShader();
		this->shaders.pixel_shader_directional = g3->getWaterDirectionalPixelShader();
		this->shaders.vertex_shader_point = g3->getWaterPointVertexShader();
		this->shaders.pixel_shader_point = g3->getWaterPointPixelShader();
		// now create new texture for reflection
		/*Texture *ref_tx = new Texture(false, REFLECTION_TEXTURE_SIZE, REFLECTION_TEXTURE_SIZE);
		this->setTexture(ref_tx, true);*/
		if( shadowplane->getRenderTexture() == NULL ) {
			//shadowplane->render_texture = new Texture(false, REFLECTION_TEXTURE_SIZE, REFLECTION_TEXTURE_SIZE);
			shadowplane->setRenderTexture( g3->getRenderer()->createTexture(false, REFLECTION_TEXTURE_SIZE, REFLECTION_TEXTURE_SIZE) );
			//if( Vision::framebufferObject )
			/*if( false )
			{
			shadowplane->fbo = new FramebufferObject();
			glGenFramebuffersEXT(1, &shadowplane->fbo->fbo);
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, shadowplane->fbo->fbo);

			GLuint depthbuffer;
			glGenRenderbuffersEXT(1, &depthbuffer);
			glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthbuffer);
			glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, REFLECTION_TEXTURE_SIZE, REFLECTION_TEXTURE_SIZE);
			glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthbuffer);

			shadowplane->render_texture->enable();
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, shadowplane->render_texture->textureID, 0);
			GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
			if( status != GL_FRAMEBUFFER_COMPLETE_EXT ) {
			LOG("Failed to create Framebuffer object!\n");
			//Vision::framebufferObject = false;
			shadowplane->fbo = NULL;
			}
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
			}*/
		}
		this->setSecondaryTexture(shadowplane->getRenderTexture());
	}
	/*else if( shadowplane == NULL ) {
	if( this->vertex_shader != NULL )
	delete this->vertex_shader;
	if( this->pixel_shader != NULL )
	delete this->pixel_shader;
	this->vertex_shader = NULL;
	this->pixel_shader = NULL;
	}*/
	this->shadowplane = shadowplane;
	this->calc_renderdata = false;
	this->calc_shadows = false; // not sure if this is needed to be recalculated?
}
#endif

void ObjectData::optimiseVertices() {
	Vector3D c = this->findCentre();
	this->translate(-c.x,-c.y,-c.z);
}

void ObjectData::rotateEuler(float x,float y,float z) {
	Rotation3D rot;
	rot.rotateToEuler(x,y,z);
	this->rotate(rot);
}

void ObjectData::getFrames(V_ANIMSTATE_t state,int *start,int *end) const {
	*start = frameinfo.frame_starts[state];
	*end = frameinfo.frame_ends[state];
	if( *start >= n_frames )
		*start = n_frames-1;
	if( *end >= n_frames )
		*end = n_frames-1;
}

const RenderData *ObjectData::getRenderData(size_t i) const {
	return renderDatas.at(i);
}

RenderData *ObjectData::getRenderData(size_t i) {
	return renderDatas.at(i);
}

Mesh::Mesh() {
	this->n_frames = 1;
}

Mesh::Mesh(int n_frames) {
	this->n_frames = n_frames;
}

Mesh::~Mesh() {
	/*for(set<Texture *>::iterator iter = this->texture_list.begin();iter != this->texture_list.end();++iter) {
		Texture *texture = *iter;
		delete texture;
	}*/
}

//void Mesh::setBoundingRadius() {
void Mesh::setBounds() {
	/*if( strcmp(this->getName(), "Terrain Chunk") == 0 ) {
		VI_log("terrain: %d\n", this);
	}*/
	if( this->calc_bounds ) {
		return;
	}

#if 0
	Vector3D centre(0,0,0);
	int n_vecs = 0;
	//for(int i=0;i<this->n_renderDatas;i++) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		//RenderData *rd = &this->renderDatas[i];
		const RenderData *rd = this->getRenderData(i);
		/*for(size_t j=0,count=0;j<rd->getNFrames()*rd->getNVertices();j++,count+=3) {
			Vector3D v;
			v.x = rd->va_vertices[count];
			v.y = rd->va_vertices[count+1];
			v.z = rd->va_vertices[count+2];
			centre += v;
			n_vecs++;
		}*/
		for(int j=0;j<rd->getNFrames();j++) {
			for(size_t k=0;k<rd->getNVertices();k++) {
				Vector3D v = rd->getVertex(j, k);
				centre += v;
				n_vecs++;
			}
		}
	}
	centre /= (float)n_vecs;
	//centre.set(0,0,0);
#endif
	Vector3D centre = this->findCentre();

	float radiusSquared = 0.0;
	Vector3D extremes[2];
	bool set_extreme = false;
	//for(int i=0;i<this->n_renderDatas;i++) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		//RenderData *rd = &this->renderDatas[i];
		const RenderData *rd = this->getRenderData(i);
		/*for(size_t j=0,count=0;j<rd->getNFrames()*rd->getNVertices();j++,count+=3) {
			Vector3D v;
			v.x = rd->va_vertices[count];
			v.y = rd->va_vertices[count+1];
			v.z = rd->va_vertices[count+2];
			Vector3D radius_vec = v - centre;
			float r2 = radius_vec.square();
			if( r2 > radiusSquared ) {
				radiusSquared = r2;
			}
			if( set_extreme ) {
				if( v.x < extremes[0].x )
					extremes[0].x = v.x;
				else if( v.x > extremes[1].x )
					extremes[1].x = v.x;
				if( v.y < extremes[0].y )
					extremes[0].y = v.y;
				else if( v.y > extremes[1].y )
					extremes[1].y = v.y;
				if( v.z < extremes[0].z )
					extremes[0].z = v.z;
				else if( v.z > extremes[1].z )
					extremes[1].z = v.z;
			}
			else {
				extremes[0] = extremes[1] = v;
				set_extreme = true;
			}
		}*/
		for(int j=0;j<rd->getNFrames();j++) {
			for(size_t k=0;k<rd->getNVertices();k++) {
				Vector3D v = rd->getVertex(j, k);
				Vector3D radius_vec = v - centre;
				float r2 = radius_vec.square();
				if( r2 > radiusSquared ) {
					radiusSquared = r2;
				}
				if( set_extreme ) {
					if( v.x < extremes[0].x )
						extremes[0].x = v.x;
					else if( v.x > extremes[1].x )
						extremes[1].x = v.x;
					if( v.y < extremes[0].y )
						extremes[0].y = v.y;
					else if( v.y > extremes[1].y )
						extremes[1].y = v.y;
					if( v.z < extremes[0].z )
						extremes[0].z = v.z;
					else if( v.z > extremes[1].z )
						extremes[1].z = v.z;
				}
				else {
					extremes[0] = extremes[1] = v;
					set_extreme = true;
				}
			}
		}
	}
	this->boundingSphere.centre = centre;
	this->boundingSphere.setRadiusSquared( radiusSquared );
	this->has_boundingBox = true;
	this->boundingBox.setAABB(extremes);

	/*for(int i=0;i<this->n_renderDatas;i++) {
	RenderData *rd = &this->renderDatas[i];
	for(int j=0,count=0;j<rd->n_frames*rd->n_vertices;j++,count+=3) {
	Vector3D v;
	v.x = rd->va_vertices[count];
	v.y = rd->va_vertices[count+1];
	v.z = rd->va_vertices[count+2];
	}
	s        }*/
	ObjectData::setBounds();
}

Vector3D Mesh::findCentre() const {
	int n=0;
	Vector3D c(0.0,0.0,0.0);
	//for(int i=0;i<this->n_renderDatas;i++) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		//const RenderData *rd = &this->renderDatas[i];
		const RenderData *rd = this->getRenderData(i);
		/*for(size_t j=0,count=0;j<rd->getNFrames()*rd->getNVertices();j++,count+=3) {
			Vertex3D v;
			v.x = rd->va_vertices[count];
			v.y = rd->va_vertices[count+1];
			v.z = rd->va_vertices[count+2];
			c += v;
			n++;
		}*/
		for(int j=0;j<rd->getNFrames();j++) {
			for(size_t k=0;k<rd->getNVertices();k++) {
				Vector3D v = rd->getVertex(j, k);
				c += v;
				n++;
			}
		}
	}
	c /= (float)n;
	return c;
}

/*void Mesh::setTexture(Texture *texture,bool reprepare) {
	//for(int i=0;i<n_renderDatas;i++) {
	for(int i=0;i<this->renderDatas.size();i++) {
		//this->renderDatas[i].texture = texture;
		this->renderDatas[i]->texture = texture;
	}
	this->calc_renderdata = false;
	if(reprepare && !Vision::isLocked() ) {
		this->prepare();
	}
}*/

/*void Mesh::rotateTexture(Texture *texture,int n_rotate,bool reprepare) {
	Vision::setError(new VisionException(this,VisionException::V_NOT_YET_SUPPORTED,"Mesh::rotateTexture not yet supported"));
}*/

void Mesh::setSecondaryTexture(Texture *secondary_texture) {
	//for(int i=0;i<n_renderDatas;i++) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		//this->renderDatas[i].secondary_texture = secondary_texture;
		//this->renderDatas[i]->secondary_texture = secondary_texture;
		if( secondary_texture != NULL ) {
			this->renderDatas[i]->setNTextures(2);
			this->renderDatas[i]->setTexture(1, secondary_texture);
		}
		else {
			this->renderDatas[i]->setNTextures(1);
		}
	}
	this->calc_renderdata = false;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::setAlphai(unsigned char alpha) {
	bool opaque = alpha == 255 || shadowplane != NULL;
	for(size_t i=0;i<this->renderDatas.size();i++) {
		RenderData *rd = this->renderDatas[i];
		if( !rd->hasAlphaChannel() ) {
			LOG("%d th render data has no alpha\n");
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "Mesh::setAlphai - at least one renderdata doesn't have alpha"));
		}
		rd->setBlending(!opaque);
		for(size_t vx=0;vx<rd->getNVertices();vx++) {
			rd->setColor(vx, 3, alpha);
		}
	}
	this->calc_renderdata = false;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::setBaseColori(unsigned char r, unsigned char g, unsigned char b) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		RenderData *rd = this->renderDatas[i];
		//if( rd->va_colors == NULL ) {
		/*if( rd->va_colors.size() == 0 ) {
			LOG("%d th render data has no color info\n");
			Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "Mesh::setBaseColori - at least one renderdata doesn't have color info"));
		}*/
		//int n_col_step = rd->hasAlphaChannel() ? 4 : 3;
		for(size_t vx=0;vx<rd->getNVertices();vx++) {
			/*rd->va_colors[n_col_step*vx] = r;
			rd->va_colors[n_col_step*vx+1] = g;
			rd->va_colors[n_col_step*vx+2] = b;*/
			rd->setColor(vx, 0, r);
			rd->setColor(vx, 1, g);
			rd->setColor(vx, 2, b);
		}
	}
	this->calc_renderdata = false;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::setSpecular(Color *specular) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		RenderData *rd = this->renderDatas[i];
		/*rd->material_specular[0] = specular->getRf();
		rd->material_specular[1] = specular->getGf();
		rd->material_specular[2] = specular->getBf();
		rd->material_specular[3] = specular->getAf();*/
		float material_specular[4] = {0.0f, 0.0f, 0.0f, 0.0f};
		specular->floatArray(material_specular);
		rd->setMaterialSpecular(material_specular);
	}
	this->calc_renderdata = false;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::setBlendType(V_BLENDTYPE_t blend_type) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		RenderData *rd = this->renderDatas[i];
		rd->setBlendType(blend_type);
	}
	this->calc_renderdata = false;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::getExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const {
	*low = 0.0;
	*high = 0.0;
	bool first = true;
	//for(int i=0;i<this->n_renderDatas;i++) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		//const RenderData *rd = &this->renderDatas[i];
		const RenderData *rd = this->getRenderData(i);
		int n_f = firstFrameOnly ? 1 : rd->getNFrames();
		/*for(size_t j=0,count=0;j<n_f*rd->getNVertices();j++,count+=3) {
			Vertex3D v;
			v.x = rd->va_vertices[count];
			v.y = rd->va_vertices[count+1];
			v.z = rd->va_vertices[count+2];
			float dot = v.dot(n);
			if(dot > *high || first)
				*high = dot;
			else if(dot < *low || first)
				*low = dot;
			first = false;
		}*/
		for(int j=0;j<rd->getNFrames();j++) {
			for(size_t k=0;k<rd->getNVertices();k++) {
				Vector3D v = rd->getVertex(j, k);
				float dot = v.dot(n);
				if(dot > *high || first)
					*high = dot;
				else if(dot < *low || first)
					*low = dot;
				first = false;
			}
		}
	}
}

void Mesh::getDirectionRadiusSq(float *radius_sq,Vector3D *n,bool firstFrameOnly) const {
	Vision::setError(new VisionException(this,VisionException::V_NOT_YET_SUPPORTED,"Mesh method not yet supported"));
}

void Mesh::scale(float sx,float sy,float sz) {
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	//for(int i=0;i<this->n_renderDatas;i++) {
	for(size_t i=0;i<this->renderDatas.size();i++) {
		//RenderData *rd = &this->renderDatas[i];
		RenderData *rd = this->getRenderData(i);
		/*for(size_t j=0,count=0;j<rd->getNFrames()*rd->getNVertices();j++,count+=3) {
			rd->va_vertices[count] *= sx;
			rd->va_vertices[count+1] *= sy;
			rd->va_vertices[count+2] *= sz;
		}*/
		for(int j=0;j<rd->getNFrames();j++) {
			for(size_t k=0;k<rd->getNVertices();k++) {
				Vector3D v = rd->getVertex(j, k);
				v.x *= sx;
				v.y *= sy;
				v.z *= sz;
				rd->setVertex(j, k, v);
			}
		}
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::translate(float sx,float sy,float sz) {
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	for(size_t i=0;i<this->renderDatas.size();i++) {
		RenderData *rd = this->getRenderData(i);
		for(int j=0;j<rd->getNFrames();j++) {
			for(size_t k=0;k<rd->getNVertices();k++) {
				Vector3D v = rd->getVertex(j, k);
				v.x += sx;
				v.y += sy;
				v.z += sz;
				rd->setVertex(j, k, v);
			}
		}
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::rotate(const Rotation3D &rot) {
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;

	Matrix m;
	rot.getMatrix(&m);

	for(size_t i=0;i<this->renderDatas.size();i++) {
		RenderData *rd = this->getRenderData(i);
		for(int j=0;j<rd->getNFrames();j++) {
			for(size_t k=0;k<rd->getNVertices();k++) {
				Vector3D v = rd->getVertex(j, k);
				m.transformVector(&v);
				rd->setVertex(j, k, v);
			}
		}
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Mesh::transform4d(const Matrix4d &m) {
	Vision::setError(new VisionException(this,VisionException::V_NOT_YET_SUPPORTED,"Mesh method not yet supported"));
}

void Mesh::deform(VI_Deform *deformFunc, void *deformData) {
	Vision::setError(new VisionException(this,VisionException::V_NOT_YET_SUPPORTED,"Mesh method not yet supported"));
}

void Mesh::setHardwareAnimation(const VI_GraphicsEnvironment *genv, bool enabled) {
	const GraphicsEnvironment *d_genv = static_cast<const GraphicsEnvironment *>(genv); // cast to the internal representation
	const Graphics3D *g3 = d_genv->getGraphics3D();
	if( !g3->hasHardwareAnimationShaders() )
		return;
	if( this->n_frames <= 1 )
		return;

	this->calc_renderdata = false;

	if( enabled ) {
		this->shaders.shader_ambient = g3->getHardwareAnimationAmbientShader();
		this->shaders.shader_directional = g3->getHardwareAnimationDirectionalShader();
		this->shaders.shader_point = g3->getHardwareAnimationPointShader();
	}
	else {
		this->shaders.clear();
	}

	for(size_t i=0;i<this->renderDatas.size();i++) {
		RenderData *rd = this->getRenderData(i);
		rd->setHardwareAnimation(enabled, 1);
	}
	
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

/*void Mesh::addTexture(VI_Texture *texture) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	this->texture_list.insert(d_texture);
	d_texture->owned = true;
}*/

VI_Mesh *Mesh::clone() {
	/*if( this->texture_list.size() > 0 ) {
		// share the textures
		for(set<Texture *>::iterator iter = texture_list.begin(); iter != texture_list.end(); ++iter) {
			Texture *texture = *iter;
			texture->owned = false;
		}
		texture_list.clear();
	}*/
	this->removeAllOwnedObjects(); // share the owned objects
	Mesh *clone = new Mesh();
	/*int n_tag = mesh->tag;
	*mesh = *this;
	mesh->tag = n_tag;*/

	clone->shadow_caster = this->shadow_caster;
	clone->frameinfo = this->frameinfo;
	clone->n_frames = this->n_frames;
	/*clone->vertex_shader_ambient = this->vertex_shader_ambient;
	clone->pixel_shader_ambient = this->pixel_shader_ambient;
	clone->vertex_shader_directional = this->vertex_shader_directional;
	clone->pixel_shader_directional = this->pixel_shader_directional;
	clone->vertex_shader_point = this->vertex_shader_point;
	clone->pixel_shader_point = this->pixel_shader_point;*/
	clone->shaders = this->shaders;
	clone->renderDatas.resize(this->getNRenderData());

	for(size_t i=0;i<clone->getNRenderData();i++) {
		/*
		// need to do it this way - otherwise push_back causes the destructor to be called, as we pass RenderData by
		// value, causing the destructor to be called when it goes out of scope!
		clone->renderDatas[i] = *this->renderDatas[i].copy(false, true, true, true);
		*/
		clone->renderDatas[i] = this->renderDatas.at(i)->copy(false, true, true, true);
	}

	if( !Vision::isLocked() )
		clone->prepare();
	return clone;
}

void Mesh::combineObject(VI_Mesh *mesh) {
	Mesh *d_mesh = dynamic_cast<Mesh *>(mesh); // cast to the internal representation
	if( d_mesh->getNChildObjects() > 0 ) {
		Vision::setError(new VisionException(this,VisionException::V_MODELLER_ERROR,"Mesh::combineObject not supported for meshes with owned objects"));
	}
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;

	for(size_t i=0;i<d_mesh->getNRenderData();i++) {
		bool done = false;
		for(size_t j=0;j<this->getNRenderData() && !done;j++) {
			if( this->renderDatas.at(j)->compatible(d_mesh->renderDatas.at(i)) ) {
				this->renderDatas.at(j)->combine(d_mesh->renderDatas.at(i));
				done = true;
			}
		}
		if( !done ) {
			this->renderDatas.push_back( d_mesh->renderDatas.at(i)->copy(false, true, true, true) );
		}
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void ObjectData::render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *npde, bool lighting_pass, bool use_shadowmesh, RenderData *renderData) {
	//const Material *material = entity->getMaterial();
	//bool do_render = false;
	/*Color black;
	black.seti(0,0,0);*/
	/*bool force_color = material->isColorForced();
	const Color *forced_color = NULL;
	unsigned char forced_alpha = 255;
	if( force_color ) {
		forced_color = material->getColor();
		forced_alpha = material->getAlpha();
	}*/
	//do_render = true;
	/*if( phase == RENDER_NONOPAQUE && force_color && forced_alpha < 255 ) {
		// still need test on 'phase', as although we shouldn't ever receive these objects in the opaque renderqueue,
		// they will be called in RENDER_STENCIL mode
		// TODO: avoid conflict with RenderData::blending flag being true?
		genv->getGraphics3D()->setBlend(material->getBlendtype(), lighting_pass);
	}*/

	//if( do_render )
	{
		//this->renderDatas[0].render(g, frame, frame2, frac, force_color, loc_col, loc_alpha);
		//int n_rd = phase == Graphics3D::RENDER_SHADOW ? this->n_shadow_renderDatas : this->n_renderDatas;
		//int n_rd = use_shadowmesh ? this->n_shadow_renderDatas : this->n_renderDatas;
		size_t n_rd = use_shadowmesh ? this->getNShadowRenderData() : this->getNRenderData();
		if( renderData != NULL ) {
			n_rd = 1;
		}
		for(size_t i=0;i<n_rd;i++) {
			//LOG("    i = %d\n", i);
			//RenderData *rd = phase == Graphics3D::RENDER_SHADOW ? this->shadow_renderDatas[i] : &this->renderDatas[i];
			//RenderData *rd = renderData != NULL ? renderData : phase == Graphics3D::RENDER_SHADOW ? this->shadow_renderDatas[i] : &this->renderDatas[i];
			//RenderData *rd = renderData != NULL ? renderData : use_shadowmesh ? this->shadow_renderDatas[i] : &this->renderDatas[i];
			RenderData *rd = renderData != NULL ? renderData : use_shadowmesh ? this->shadow_renderDatas.at(i) : this->renderDatas.at(i);
			//RenderData *rd = &this->renderDatas[i];
			//RenderData *rd = rds[i];
			//GraphicsEnvironment::checkGLError("ObjectData::render about to render");
			_DEBUG_CHECK_ERROR_
			//rd->render(renderer, genv, frame, frame2, frac, force_color, forced_color, forced_alpha, lighting_pass);
			rd->render(renderer, genv, frame, frame2, frac, lighting_pass);
			//GraphicsEnvironment::checkGLError("ObjectData::render done");
			_DEBUG_CHECK_ERROR_
		}
	}
}

void ObjectData::renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview) {
	//lights_only = false;
	//g->c_queue->renderQueue->add(entity);
	//return;
	if( node->getPoint()->isInfinite() ) {
		//g->c_queue->renderQueue_infinite->add(entity);
		queue->renderQueue_infinite->push_back(node);
	}
	else {
		if( !outofview )
		{
			if( shadowplane == NULL || shadowplane->isRenderToTexture() )
			{
				bool mat_opaque = true;
				if( node->getMaterial()->isColorForced() ) {
					if( node->getMaterial()->getAlpha() < 255 ) {
						mat_opaque = false;
					}
				}
				//g->c_queue->renderQueue->add(entity);
				//g->c_queue->renderQueue_nonopaque->add(entity);
				//g->c_queue->renderQueue->push_back(entity);
				//g->c_queue->addOpaque(entity);
				//for(int i=0;i<n_renderDatas;i++) {
				for(size_t i=0;i<renderDatas.size();i++) {
					//bool opaque = mat_opaque && !renderDatas[i].blending;
					bool opaque = mat_opaque && !getRenderData(i)->isBlending();
					Renderable renderable(node);
					//renderable.renderData = &renderDatas[i];
					renderable.renderData = renderDatas[i];
					if( opaque )
						queue->addOpaque(&renderable);
					else
						queue->addNonOpaque(&renderable);
				}
				//g->c_queue->renderQueue_nonopaque->push_back(entity);
				//g->c_queue->addNonOpaque(entity);
			}
		}
		// can't do clipping here for shadow volumes, as it depends on the light
		//if( entity->shadow_caster )
		if( this->shadow_caster ) {
			//if( shadowplane == NULL || shadowplane->render_to_texture || g->useShadowVolumes() )
			{
				queue->renderQueue_shadowcasters->push_back(node);
			}
		}
	}
}

void ObjectData::castShadow(Renderer *renderer, GraphicsEnvironment *genv, int frame, int frame2, float frac, const Vector3D &lightpos, bool infinite, bool shader) {
	/*for(int i=0;i<this->n_renderDatas;i++) {
	this->renderDatas[i].castShadow(frame, lightpos, infinite);
	}*/
	V__ENTER;
	_DEBUG_CHECK_ERROR_;
	for(size_t i=0;i<this->getNShadowRenderData();i++) {
		this->shadow_renderDatas.at(i)->castShadow(renderer, genv, frame, frame2, frac, lightpos, infinite, shader);
	}
	V__EXIT;
}

Object3D::Object3D() : ObjectData() {
	init(/*INIT_N_VERTICES,INIT_N_POLYS,*/1);
}

Object3D::Object3D(/*unsigned int max_vertices,unsigned short max_polys,*/int n_frames) : ObjectData() {
	init(/*max_vertices,max_polys,*/n_frames);
}

/*void Object3D::expandCapacity(unsigned int req_vertices,unsigned short req_polys) {
	if(req_polys > this->cap_polys) {
		//int i;
		Polygon3D *new_polys = new Polygon3D[req_polys];
		for(unsigned short i=0;i<n_polys;i++) {
			new_polys[i] = this->polys[i];
		}
		delete [] this->polys;
		this->polys = new_polys;

		for(int i=0;i<n_frames;i++) {
			Vector3D *new_normals = new Vector3D[req_polys];
			Vector3D *this_normals = poly_normals_frame[i];
			for(int j=0;j<n_polys;j++) {
				new_normals[j] = this_normals[j];
			}
			delete [] this_normals;
			poly_normals_frame[i] = new_normals;
		}

		this->cap_polys = req_polys;
	}
	if(req_vertices > this->cap_vertices) {
		for(int i=0;i<n_frames;i++) {
			Vertex3D *new_vertices = new Vertex3D[req_vertices];
			Vertex3D *this_vertices = frames[i];
			for(unsigned int j=0;j<n_vertices;j++) {
				new_vertices[j] = this_vertices[j];
			}
			delete [] this_vertices;
			frames[i] = new_vertices;
		}
		Color *new_colors = new Color[req_vertices];
		for(unsigned int j=0;j<n_vertices;j++) {
			new_colors[j] = colors[j];
		}
		delete [] colors;
		colors = new_colors;

		this->cap_vertices = req_vertices;
	}
}*/

/*int sortPolygonsFunc(const void *a, const void *b) {
Polygon3D *poly1 = (Polygon3D *)a;
Polygon3D *poly2 = (Polygon3D *)b;
if( poly1->getTextureID() > poly2->getTextureID() )
return 1;
else if( poly1->getTextureID() < poly2->getTextureID() )
return -1;
return 0;
}*/

/*void Object3D::sortPolygons() {
// disabled for now!
qsort(this->polys, n_polys, sizeof(polys[0]), sortPolygonsFunc);
if(!calc_pn)
calculatePolygonNormals();
}*/

VI_Face *Object3D::addPolygon(Polygon3D polygon) {
	if( !Vision::isLocked() ) {
		// would be slow to call prepare every time, so better to use locking
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Attempted to add polygon when not locked"));
	}
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	polys.push_back(polygon);
	return &polys.back();
}

/*void Object3D::addPolygon(Polygon3D *polygon) {
	if( !Vision::isLocked() ) {
		// would be slow to call prepare every time, so better to use locking
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Attempted to add polygon when not locked"));
	}
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	polys.push_back(*polygon);
	delete polygon;
}*/

/*void Object3D::addPolygon(Polygon3D *polygon, const char *name) {
	polygon->setName(name);
	this->addPolygon(polygon);
}*/

/*VI_Face *Object3D::addPolygon(size_t v0, size_t v1, size_t v2) {
	Polygon3D poly(v0, v1, v2);
	return this->addPolygon(poly);
}

VI_Face *Object3D::addPolygon(size_t v0, size_t v1, size_t v2, size_t v3) {
	Polygon3D poly(v0, v1, v2, v3);
	return this->addPolygon(poly);
}*/

VI_Face *Object3D::addPolygon(size_t v0, size_t v1, size_t v2, const char *name) {
	Polygon3D polygon(v0, v1, v2);
	polygon.setName(name);
	return this->addPolygon(polygon);
}

VI_Face *Object3D::addPolygon(size_t v0, size_t v1, size_t v2, size_t v3, const char *name) {
	Polygon3D polygon(v0, v1, v2, v3);
	polygon.setName(name);
	return this->addPolygon(polygon);
}

VI_Face *Object3D::addPolygon(const size_t *vlist, size_t n_vertices, const char *name) {
	Polygon3D polygon(vlist, n_vertices);
	polygon.setName(name);
	return this->addPolygon(polygon);
}

void Object3D::set_n_vertices(size_t n_vertices) {
	/*if( n_vertices > MAX_VERTICES ) {
		Vision::setError(new VisionException(this,VisionException::V_TOO_MANY_VERTICES,"Object3D has too many vertices"));
	}
	expandCapacity(n_vertices,0);
	this->n_vertices = n_vertices;*/
	for(int i=0;i<n_frames;i++) {
		frames[i].resize(n_vertices);
	}
	colors.resize(n_vertices);
}

size_t Object3D::addVertex(int frame, Vertex3D vertex) {
	/*if( n_frames != 1 ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Object3D::addVertex only supported for 1 frame"));
	}*/
	if( !Vision::isLocked() ) {
		// would be slow to call prepare every time, so better to use locking
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Attempted to add vertex when not locked"));
	}
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	frames[frame].push_back(vertex);
	if( frame == 0 ) {
		// There is only one set of colours for all frames. The burden is upon the caller of this function to assume
		// that all frames have the same number of vertices (which should, therefore, equal the number of colours). Otherwise
		// the object will fail to check.
		Color col;
		colors.push_back(col);
	}
	return frames[frame].size()-1;
}

size_t Object3D::addVertex(float x, float y, float z) {
	return addVertex(0, x, y, z);
}

size_t Object3D::addVertex(int frame, float x, float y, float z) {
	Vertex3D vertex(x, y, z);
	return addVertex(frame, vertex);
}

void Object3D::setVertexColori(size_t index, unsigned char r, unsigned char g, unsigned char b) {
	colors[index].seti(r, g, b);
}

/*void Object3D::addVertex(int frame,Vertex3D vertex) {
if( n_vertices == MAX_VERTICES ) {
Vision::setError(new VisionException(this,VisionException::V_TOO_MANY_VERTICES,"Object3D has too many vertices"));
}
Vertex3D *vxs = frames[frame];
vxs[n_vxs_frame[frame]++] = vertex;
if(n_vxs_frame[frame] > n_vertices)
n_vertices = n_vxs_frame[frame];
}

void Object3D::addVertex(int frame,Vertex3D *vertex) {
if( n_vertices == MAX_VERTICES ) {
Vision::setError(new VisionException(this,VisionException::V_TOO_MANY_VERTICES,"Object3D has too many vertices"));
}
Vertex3D *vxs = frames[frame];
vxs[n_vxs_frame[frame]++] = *vertex;
if(n_vxs_frame[frame] > n_vertices)
n_vertices = n_vxs_frame[frame];
delete vertex;
}*/

/*int Object3D::addVertex(Vertex3D *vertex) {
	if( n_frames != 1 ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Object3D::addVertex only supported for 1 frame"));
	}
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	frames[0].push_back(*vertex);
	delete vertex;
	Color col;
	colors.push_back(col);
	return frames[0].size()-1;
}*/

void Object3D::init(int n_frames) {
	this->calc_vn = false;
	this->calc_pn = false;
	//this->faces = NULL;
	this->n_frames = n_frames;
	this->frames = new vector<Vertex3D>[n_frames];
	this->poly_normals_frame = new vector<Vector3D>[n_frames];
}

Object3D::~Object3D() {
	freeData();
}

bool Object3D::check() const {
	if( !ObjectData::check() ) {
		return false;
	}
	else if( (frames==NULL) != (n_frames==0) ) {
		LOG("frames/n_frames inconsistent\n");
		return false;
	}
	else if( (poly_normals_frame==NULL) != (n_frames==0) ) {
		LOG("poly_normals_frame/n_frames inconsistent\n");
		return false;
	}
	else if( (colors.size()==0) != (n_frames==0) ) {
		LOG("colors/n_frames inconsistent\n");
		return false;
	}
	else if( (polys.size()==0) != (n_frames==0) ) {
		LOG("polys/n_frames inconsistent\n");
		return false;
	}

	for(int i=0;i<n_frames;i++) {
		if( frames[i].size() != frames[0].size() ) {
			LOG("unequal number of vertices in frames\n");
			return false;
		}
		if( frames[i].size() != colors.size() ) {
			LOG("inconsistent number of vertices vs colors\n");
			return false;
		}
	}

	return true;
}

void Object3D::compact() {
	// TODO?
	return;
	/*for(int frame=0;frame<n_frames;frame++) {
	Vertex3D *vxs = frames[frame];
	Vector3D *normals = poly_normals_frame[frame];
	Vertex3D *t_vertices = new Vertex3D[n_vxs_frame[frame]];
	Vector3D *t_poly_normals = new Vector3D[ n_vxs_frame[frame] ];
	for(int i=0;i<n_vxs_frame[frame];i++) {
	t_vertices[i] = vxs[i];
	t_poly_normals[i] = normals[i];
	}
	delete [] vxs;
	delete [] normals;
	frames[frame] = t_vertices;
	poly_normals_frame[frame] = t_poly_normals;
	}

	Polygon3D *t_polys = new Polygon3D[n_polys];
	for(int i=0;i<n_polys;i++)
	t_polys[i] = polys[i];
	delete [] polys;
	polys = t_polys;*/
}

/*void Object3D::setTexture(Texture *texture,bool reprepare) {
	setTexture(texture,false,0,reprepare);
}*/

/*void Object3D::setTexture(Texture *texture,bool mirror,bool reprepare) {
	setTexture(texture,mirror,0,reprepare);
}*/

/*void Object3D::setTexture(Texture *texture,bool mirror,int skip,bool reprepare) {
	setTexture(texture, mirror, skip, false, 0.0, 0.0, reprepare);
}*/

void Object3D::setTexture(VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v) {
	this->calc_renderdata = false;
	for(size_t i=0;i<get_n_polys();i++)
		polys.at(i).setTexture(texture,this->getVertices(0),mirror,skip,have_size,size_u,size_v);
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setTextureWrap(VI_Texture *texture, const TextureUV **text_uvs) {
	this->calc_renderdata = false;
	// only frame 0 atm
	for(size_t i=0;i<get_n_polys();i++) {
		//polys[i].setTextureWrap(frames[0],text_uvs,texture);
		polys.at(i).setTextureWrap(texture, text_uvs);
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setPolygonTextureWrap(VI_Face *face,VI_Texture *texture,const TextureUV **text_uvs) {
	this->calc_renderdata = false;
	//poly->setTextureWrap(vertices, text_uvs, texture);
	Polygon3D *d_face = static_cast<Polygon3D *>(face); // cast to the internal representation
	d_face->setTextureWrap(texture, text_uvs);
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setPolygonTextureCoords(VI_Face *face,VI_Texture *texture,const float *u, const float *v) {
	this->calc_renderdata = false;
	Polygon3D *d_face = static_cast<Polygon3D *>(face); // cast to the internal representation
	d_face->setTexture(texture);
	for(size_t i=0;i<d_face->getNVertices();i++) {
		d_face->setTextureCoords(i, u[i], v[i]);
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

/*void Object3D::setTexture(Texture *texture,bool mirror,int skip,float scale_u,float scale_v,bool reprepare) {
	this->calc_renderdata = false;
	for(int i=0;i<get_n_polys();i++)
		polys[i].setTexture(texture,this->getVertices(0),mirror,skip,scale_u,scale_v);
	if(reprepare && !Vision::isLocked() ) {
		this->prepare();
	}
}*/

/*void Object3D::rotateTexture(Texture *texture,int n_rotate,bool reprepare) {
	for(int i=0;i<get_n_polys();i++) {
		Polygon3D *poly = &polys[i];
		if( poly->texture == texture ) {
			poly->rotateTexture(n_rotate);
		}
	}
	if(reprepare && !Vision::isLocked() ) {
		this->prepare();
	}
}*/

void Object3D::setSecondaryTexture(Texture *secondary_texture) {
	this->calc_renderdata = false;
	for(size_t i=0;i<get_n_polys();i++)
		polys.at(i).setSecondaryTexture(secondary_texture);
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setBumpTexture(const VI_GraphicsEnvironment *genv, VI_Texture *bump_texture) {
	const GraphicsEnvironment *d_genv = static_cast<const GraphicsEnvironment *>(genv); // cast to the internal representation
	Texture *d_bump_texture = static_cast<Texture *>(bump_texture); // cast to the internal representation
	const Graphics3D *g3 = d_genv->getGraphics3D();
	if( !g3->hasBumpShaders() )
		return;
	if( this->attribs.need_tangents == ( d_bump_texture != NULL ) )
		return;

	this->calc_renderdata = false;
	//this->attribs.bump_texture = d_bump_texture;
	this->attribs.need_tangents = d_bump_texture != NULL;
	// need to set texture coords if not already present!
	for(size_t i=0;i<get_n_polys();i++) {
		if( polys.at(i).texture == NULL && d_bump_texture != NULL ) {
			//polys[i].setTexture(NULL, this->getVertices(0)); // this sets texture coords without setting the texture
			polys.at(i).setTexture(NULL, this->getVertices(0), false, 0, false, 0.0f, 0.0f); // this sets texture coords without setting the texture
		}
		polys.at(i).setSecondaryTexture(d_bump_texture);
	}

	if( bump_texture != NULL ) {
		this->shaders.shader_directional = g3->getBumpDirectionalShader();
		this->shaders.shader_point = g3->getBumpPointShader();
	}
	else {
		this->shaders.clear();
	}

	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setHardwareAnimation(const VI_GraphicsEnvironment *genv, bool enabled) {
	const GraphicsEnvironment *d_genv = static_cast<const GraphicsEnvironment *>(genv); // cast to the internal representation
	const Graphics3D *g3 = d_genv->getGraphics3D();
	if( !g3->hasHardwareAnimationShaders() )
		return;
	if( this->n_frames <= 1 )
		return;
	if( this->attribs.uses_hardware_animation == enabled )
		return;

	this->calc_renderdata = false;
	this->attribs.uses_hardware_animation = enabled;

	if( enabled ) {
		this->shaders.shader_ambient = g3->getHardwareAnimationAmbientShader();
		this->shaders.shader_directional = g3->getHardwareAnimationDirectionalShader();
		this->shaders.shader_point = g3->getHardwareAnimationPointShader();
	}
	else {
		this->shaders.clear();
	}

	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setShading(VI_Face::LightingStyle shading) {
	this->calc_renderdata = false;
	/*if(!calc_vn && shading==Polygon3D::CURVED)
	calculateVertexNormals();*/
	for(size_t i=0;i<get_n_polys();i++)
		polys.at(i).shading=shading;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setBaseColori(unsigned char r, unsigned char g, unsigned char b) {
	Color col;
	col.seti(r,g,b);

	this->calc_renderdata = false;
	for(size_t j=0;j<colors.size();j++) {
		colors[j] = col;
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::setSolid(bool solid) {
	if( this->attribs.solid != solid ) {
		this->calc_renderdata = false;
		this->calc_shadows = false; // not sure if this is needed?
		this->attribs.solid = solid;
		if( !Vision::isLocked() ) {
			this->prepare();
		}
	}
}

bool Object3D::isSolid() const {
	return this->attribs.solid;
}

//void Object3D::setBoundingRadius() {
void Object3D::setBounds() {
	if( this->calc_bounds ) {
		return;
	}

	Vector3D centre(0,0,0);
	int n_vecs = 0;
	for(int frame=0;frame<n_frames;frame++) {
		for(size_t k=0;k<frames[frame].size();k++) {
			centre += frames[frame].at(k);
			n_vecs++;
		}
	}
	centre /= (float)n_vecs;
	//centre.set(0,0,0);

	float maxr2 = 0;
	Vector3D extremes[2];
	bool set_extreme = false;
	for(int frame=0;frame<n_frames;frame++) {
		for(size_t k=0;k<frames[frame].size();k++) {
			Vector3D v = frames[frame].at(k);
			Vector3D radius = v - centre;
			float r2 = radius.square();
			if ( r2 > maxr2)
				maxr2 = r2;
			if( set_extreme ) {
				if( v.x < extremes[0].x )
					extremes[0].x = v.x;
				else if( v.x > extremes[1].x )
					extremes[1].x = v.x;
				if( v.y < extremes[0].y )
					extremes[0].y = v.y;
				else if( v.y > extremes[1].y )
					extremes[1].y = v.y;
				if( v.z < extremes[0].z )
					extremes[0].z = v.z;
				else if( v.z > extremes[1].z )
					extremes[1].z = v.z;
			}
			else {
				extremes[0] = extremes[1] = v;
				set_extreme = true;
			}
		}
	}
	this->boundingSphere.centre = centre;
	this->boundingSphere.setRadiusSquared(maxr2);

	this->has_boundingBox = true;
	this->boundingBox.setAABB(extremes);

	ObjectData::setBounds();
}

Vector3D Object3D::findCentre() const {
	int n=0;
	Vector3D c(0.0,0.0,0.0);
	for(int frame=0;frame<n_frames;frame++) {
		for(size_t k=0;k<frames[frame].size();k++) {
			c += frames[frame].at(k);
			n++;
		}
	}
	if( n > 0 )
		c /= (float)n;
	return c;
}

void Object3D::getExtent(float *low,float *high,const Vector3D &n,bool firstFrameOnly) const {
	*low = 0.0;
	*high = 0.0;
	bool first = true;
	for(int j=0;j<this->n_frames;j++) {
		for(size_t k=0;k<frames[j].size();k++) {
			float dot = frames[j].at(k).dot(n);
			if(dot > *high || first)
				*high = dot;
			else if(dot < *low || first)
				*low = dot;
			first = false;
		}
		if(firstFrameOnly)
			break;
	}
}

void Object3D::getDirectionRadiusSq(float *radius_sq,Vector3D *n,bool firstFrameOnly) const {
	*radius_sq = 0.0;
	*n = Vector3D(0,0,0);
	// TODO
	Vision::setError(new VisionException(this,VisionException::V_NOT_YET_SUPPORTED,"Object3D::getDirectionRadiusSq not yet supported"));
}

void Object3D::calculatePolygonNormals() {
	//Vertex3D *vertex;
	calc_pn = true;
	for(int frame=0;frame<n_frames;frame++) {
		//Vertex3D *vxs = frames[frame];
		Vertex3D *vxs = &*frames[frame].begin();
		//Polygon3D *polyptr = polys;
		Polygon3D *polyptr = &*polys.begin();
		//Vector3D *normalptr = poly_normals_frame[frame];
		//Vector3D *normalptr = &*poly_normals_frame[frame].begin();
		poly_normals_frame[frame].clear();
		for(size_t i=0;i<get_n_polys();i++) {
			vector<size_t>::iterator vptr = polyptr->vlist.begin();
			Vector3D v0 = vxs[*vptr++];
			Vector3D v1 = vxs[*vptr++];
			Vector3D v2 = vxs[*vptr];
			/*Vector3D n;
			*normalptr = v2;
			n = v1;
			normalptr->subtract(&v1);
			n.subtract(&v0);
			normalptr->cross(&n);
			if(normalptr->square() > V_TOL_MACHINE)
				normalptr->normalise();
			else
				normalptr->set(0.0,0.0,0.0);
			normalptr++;
			*/
			Vector3D tan = v1 - v0;
			Vector3D norm = v2 - v1;
			norm.cross(tan);
			if( norm.square() > V_TOL_MACHINE )
				norm.normalise();
			else
				norm.set(0.0,0.0,0.0);
			poly_normals_frame[frame].push_back(norm);
			polyptr++;
		}
	}
}

void Object3D::calculateVertexNormals() {
	if(!calc_pn)
		calculatePolygonNormals();
	calc_vn = true;
	Vertex3D normal;
	Polygon3D *poly_curr;
	Vector3D *normal_curr;

	//Vector *vx_polys = new Vector[n_vertices];
	size_t n_vertices = n_frames == 0 ? 0 : frames[0].size();
	vector<size_t> *vx_polys = new vector<size_t>[n_vertices];
	for(size_t l=0;l<get_n_polys();l++) {
		poly_curr=&polys[l];
		for(size_t m=0;m<poly_curr->getNVertices();m++) {
			size_t vx_indx = poly_curr->vlist.at(m);
			//vx_polys[vx_indx].add((void *)l);
			//LOG(">>> %d\n", vx_indx);
			vx_polys[vx_indx].push_back(l);
		}
	}

	for(int frame=0;frame<n_frames;frame++) {

		//Vertex3D *vxs = frames[frame];
		Vertex3D *vxs = &*frames[frame].begin();
		//Vector3D *poly_normals = poly_normals_frame[frame];
		Vector3D *poly_normals = &*poly_normals_frame[frame].begin();

		for(size_t k=0;k<frames[frame].size();k++) {
			normal.x=0;
			normal.y=0;
			normal.z=0;
			//int count=0;
			/*for(int l=0;l<n_polys;l++) {
			poly_curr=&polys[l];
			normal_curr=&poly_normals[l];
			for(int m=0;m<poly_curr->n_vertices;m++) {
			if(poly_curr->vlist[m]==k) {
			// vertex k is in polygon l
			normal.x += normal_curr->x;
			normal.y += normal_curr->y;
			normal.z += normal_curr->z;
			//count++;
			break;
			}
			}
			//if(count>8) // dodgy speedup - assume each vertex belongs to no more than 8 polygons
			//        break;
			}*/
			for(size_t l=0;l<vx_polys[k].size();l++) {
				//int poly_indx = (int)vx_polys[k].get(l);
				size_t poly_indx = vx_polys[k].at(l);
				normal_curr=&poly_normals[poly_indx];
				normal.x += normal_curr->x;
				normal.y += normal_curr->y;
				normal.z += normal_curr->z;
			}
			if(normal.square() > V_TOL_MACHINE)
				normal.normalise();
			vxs[k].normal=normal;
		}
	}
	delete [] vx_polys;
}

VI_VObject * Object3D::clone() const {
	Object3D *clone = new Object3D();
	/*int n_tag = clone->tag;
	*clone = *this;
	clone->tag = n_tag;*/

	clone->shadow_caster = this->shadow_caster;
	clone->frameinfo = this->frameinfo;
	clone->n_frames = this->n_frames;
	/*clone->vertex_shader_ambient = this->vertex_shader_ambient;
	clone->pixel_shader_ambient = this->pixel_shader_ambient;
	clone->vertex_shader_directional = this->vertex_shader_directional;
	clone->pixel_shader_directional = this->pixel_shader_directional;
	clone->vertex_shader_point = this->vertex_shader_point;
	clone->pixel_shader_point = this->pixel_shader_point;*/
	clone->shaders = this->shaders;

	clone->polys = this->polys;
	clone->colors = this->colors;
	/*clone->solid = this->solid;
	clone->alpha = this->alpha;
	clone->specular = this->specular;
	clone->bump_texture = this->bump_texture;*/
	clone->attribs = this->attribs;

	/*clone->renderDatas.clear();
	clone->shadow_renderDatas = NULL;
	clone->n_shadow_renderDatas = 0;
	clone->calc_bounds = false;
	clone->calc_pn = false;
	clone->calc_renderdata = false;
	clone->calc_shadows = false;
	clone->calc_vn = false;
	clone->colors.clear();
	clone->n_frames = 0;
	clone->frames = NULL;
	clone->poly_normals_frame = NULL;
	clone->polys.clear();
	delete clone;
	return clone;*/

	// but! - we need to duplicate arrays
	//clone->frames = new Vertex3D *[n_frames];
	clone->frames = new vector<Vertex3D>[n_frames];
	//int i;
	for(int i=0;i<n_frames;i++) {
		Vertex3D *vxs = &*this->frames[i].begin();
		for(size_t j=0;j<frames[i].size();j++)
			clone->frames[i].push_back( vxs[j] );
	}
	if( this->poly_normals_frame != NULL ) {
		//clone->poly_normals_frame = new Vector3D *[n_frames];
		clone->poly_normals_frame = new vector<Vector3D>[n_frames];
		for(int i=0;i<n_frames && this->poly_normals_frame[0].size() > 0;i++) {
			//clone->poly_normals_frame[i] = new Vector3D[cap_vertices];
			//clone->poly_normals_frame[i] = new Vector3D[cap_polys];
			//printf(">>> %d %d\n",clone,clone->poly_normals_frame[i]);
			//Vector3D *vxs = this->poly_normals_frame[i];
			//Vector3D *c_vxs = clone->poly_normals_frame[i];
			Vector3D *vxs = &*this->poly_normals_frame[i].begin();
			//for(int i=0;i<n_vertices;i++)
			for(size_t j=0;j<get_n_polys();j++) {
				//c_vxs[i] = vxs[i];
				clone->poly_normals_frame[i].push_back(vxs[j]);
			}
		}
	}
	//clone->polys = new Polygon3D[cap_polys];
	/*for(unsigned short i=0;i<get_n_polys();i++) {
		//if(polys[i]!=NULL)
		//        clone->polys[i] = new Polygon3D(this->polys[i]);
		//clone->polys[i] = polys[i];
		clone->polys.push_back(polys[i]);
	}*/
	/*clone->n_vxs_frame = new unsigned short[n_frames];
	for(i=0;i<n_frames;i++) {
	clone->n_vxs_frame[i] = this->n_vxs_frame[i];
	}*/
	//clone->displayLists = new GLuint[n_frames];
	/*if( this->renderData.displayLists != NULL ) {
	clone->renderData.initDisplayLists(n_frames);
	for(i=0;i<n_frames;i++) {
	clone->renderData.displayLists[i] = this->renderData.displayLists[i];
	}
	}*/
	if( !Vision::isLocked() )
		clone->prepare();
	return clone;
}

void Object3D::combineObject(VI_VObject *o) {
	if( o==NULL ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Object3D::combineObject: o is NULL"));
	}
	Object3D *d_o = dynamic_cast<Object3D *>(o); // cast to the internal representation
	if( this->n_frames != d_o->n_frames ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Object3D::combineObject: objects have different numbers of frames"));
	}
	/*this->prepare();
	o->prepare();*/
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	//expandCapacity(this->n_vertices + o->n_vertices/*, this->n_polys + o->n_polys*/);
	//int i;
	size_t n_old_vertices = n_frames == 0 ? 0 : frames[0].size();
	for(int i=0;i<n_frames;i++) {
		for(size_t k=0;k<d_o->frames[i].size();k++) {
			Vertex3D *vx = &d_o->frames[i].at(k);
			this->frames[i].push_back(*vx);
		}
		if( d_o->poly_normals_frame != NULL ) {
			for(size_t k=0;k<d_o->poly_normals_frame[i].size();k++) {
				Vector3D *poly = &d_o->poly_normals_frame[i].at(k);
				poly_normals_frame[i].push_back(*poly);
			}
		}
		//n_vxs_frame[i] += o->n_vxs_frame[i];
	}
	for(size_t i=0;i<d_o->colors.size();i++) {
		//colors[n_vertices+i] = o->colors[i];
		colors.push_back( d_o->colors.at(i) );
	}
	for(size_t i=0;i<d_o->get_n_polys();i++) {
		//polygons[n_polys+i] = new Polygon3D(o->polys[i]);
		//polys[n_polys+i] = o->polys[i];
		const Polygon3D *poly = static_cast<const Polygon3D *>(d_o->getFace(i)); // cast to internal representation
		polys.push_back(*poly);
		Polygon3D *new_poly = &polys[polys.size()-1];
		//poly_normal[nopolygons+i] = o->poly_normal[i];
		// we need to remap the vertices that the polygons index to!
		/*for(int l=0;l<polys[n_polys+i].getNVertices();l++)
			polys[n_polys+i].vlist[l] += n_vertices;*/
		for(size_t l=0;l<new_poly->getNVertices();l++)
			new_poly->vlist[l] += n_old_vertices;
	}
	//n_vertices += o->n_vertices;
	//n_polys += o->n_polys;
	/*if(o->isShade==1)
	isShade=1;*/
	//delete o;
	// why no delete? check callers
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

Mesh *Object3D::createMesh() {
	this->prepare(); // we need this version to be up to date, even if Vision has been locked

	Mesh *mesh = new Mesh();
	{
		//Vision::lock();
		SimpleVisionLocker vision_locker; // will unlock when it goes out of scope

		mesh->setShadowCaster( this->shadow_caster );
		mesh->frameinfo = this->frameinfo;
		mesh->n_frames = this->n_frames;
		/*mesh->vertex_shader_ambient = this->vertex_shader_ambient;
		mesh->pixel_shader_ambient = this->pixel_shader_ambient;
		mesh->vertex_shader_directional = this->vertex_shader_directional;
		mesh->pixel_shader_directional = this->pixel_shader_directional;
		mesh->vertex_shader_point = this->vertex_shader_point;
		mesh->pixel_shader_point = this->pixel_shader_point;*/
		mesh->shaders = this->shaders;
		/*mesh->n_renderDatas = this->n_renderDatas;
		mesh->renderDatas = new RenderData[mesh->n_renderDatas];*/
		//for(int i=0;i<this->n_renderDatas;i++) {
		mesh->renderDatas.resize( this->renderDatas.size() );
		for(size_t i=0;i<this->renderDatas.size();i++) {
			//RenderData *rd = this->renderDatas[i].copy(false, true, true, true);
			//mesh->renderDatas[i] = *rd;
			//mesh->renderDatas.push_back( *rd );
			/*
			// need to do it this way - otherwise push_back causes the destructor to be called, as we pass RenderData by
			// value, causing the destructor to be called when it goes out of scope!
			mesh->renderDatas[i] = *this->renderDatas[i].copy(false, true, true, true);*/
			mesh->renderDatas[i] = this->renderDatas.at(i)->copy(false, true, true, true);
			/*LOG("ping\n");
			RenderData *rd = &mesh->renderDatas[i];
			delete rd;*/
		}

		//Vision::unlock(false);
	}
	if( !Vision::isLocked() ) {
		mesh->prepare();
	}
	return mesh;
}

void Object3D::prepare() {
	/*if(n_vxs_frame != NULL) {
	delete [] n_vxs_frame;
	n_vxs_frame = NULL;
	}*/

	// set owner of polygons
	/*for(int i=0;i<this->n_polys;i++) {
	this->polys[i].owner = this;
	}*/

	//sortPolygons();
	if( !calc_pn ) {
		//LOG("calculatePolygonNormals()\n");
		calculatePolygonNormals();
	}
	if( !calc_vn ) {
		//LOG("calculateVertexNormals()\n");
		calculateVertexNormals();
	}

	ObjectData::prepare();
}

void Object3D::freeData() {
	if(frames != NULL) {
		/*for(int i=0;i<n_frames;i++) {
			if(frames[i] != NULL)
				delete [] frames[i];
		}*/
		delete [] frames;
		frames = NULL;
	}
	if(poly_normals_frame != NULL) {
		/*for(int i=0;i<n_frames;i++) {
			if(poly_normals_frame[i] != NULL)
				delete [] poly_normals_frame[i];
		}*/
		delete [] poly_normals_frame;
		poly_normals_frame = NULL;
	}
	/*if(n_vxs_frame != NULL) {
	delete [] n_vxs_frame;
	n_vxs_frame = NULL;
	}*/
	/*if(polys!=NULL) {
		delete [] polys;
		polys = NULL;
	}*/
	polys.clear();
}

void Object3D::scale(float sx,float sy,float sz) {
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	for(int frame=0;frame<n_frames;frame++) {
		//Vertex3D *vxs = frames[frame];
		Vertex3D *vxs = &*frames[frame].begin();
		for(size_t i=0;i<frames[frame].size();i++)
			vxs[i].scale(sx,sy,sz);
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::translate(float sx,float sy,float sz) {
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	for(int frame=0;frame<n_frames;frame++) {
		//Vertex3D *vxs = frames[frame];
		Vertex3D *vxs = &*frames[frame].begin();
		for(size_t i=0;i<frames[frame].size();i++)
			vxs[i].translate(sx,sy,sz);
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::rotate(const Rotation3D &rot) {
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false; // just to be safe
	this->calc_renderdata = false;
	this->calc_shadows = false;
	Matrix m;
	rot.getMatrix(&m);
	for(int frame=0;frame<n_frames;frame++) {
		//Vertex3D *vxs = frames[frame];
		Vertex3D *vxs = &*frames[frame].begin();
		for(size_t i=0;i<frames[frame].size();i++)
			m.transformVector(&vxs[i]);
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::transform4d(const Matrix4d &m) {
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	for(int frame=0;frame<n_frames;frame++) {
		//Vertex3D *vxs = frames[frame];
		Vertex3D *vxs = &*frames[frame].begin();
		for(size_t i=0;i<frames[frame].size();i++)
			m.transformVector(&vxs[i]);
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::deform(VI_Deform *deformFunc, void *deformData) {
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_bounds = false;
	this->calc_renderdata = false;
	this->calc_shadows = false;
	for(int frame=0;frame<n_frames;frame++) {
		Vertex3D *vxs = &*frames[frame].begin();
		for(size_t i=0;i<frames[frame].size();i++) {
			Vector3D vx = vxs[i];
			Vector3D new_vx = (*deformFunc)(vx, deformData);
			vxs[i].set(new_vx.x, new_vx.y, new_vx.z);
		}
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::initRenderData(RenderData *renderData, Texture *this_texture) const {
	unsigned char loc_alpha = attribs.alpha;
	bool opaque = loc_alpha == 255 || shadowplane != NULL;
	Texture *secondary_texture = (get_n_polys() == 0) ? NULL : this->polys[0].secondary_texture;
	renderData->setSolid(attribs.solid);
	renderData->setBlending(!opaque);
	renderData->setBlendType(attribs.blend_type);
	/*if( renderData->va_colors == NULL ) {
		renderData->cols[0] = same_col.getR();
		renderData->cols[1] = same_col.getG();
		renderData->cols[2] = same_col.getB();
		renderData->cols[3] = loc_alpha;
	}*/
	/*renderData->material_specular[0] = attribs.specular.getRf();
	renderData->material_specular[1] = attribs.specular.getGf();
	renderData->material_specular[2] = attribs.specular.getBf();
	renderData->material_specular[3] = attribs.specular.getAf();*/
	float material_specular[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	attribs.specular.floatArray(material_specular);
	renderData->setMaterialSpecular(material_specular);
	//renderData->texture = this_texture;
	//renderData->secondary_texture = secondary_texture;
	//renderData->bump_texture = attribs.bump_texture;
	if( secondary_texture != NULL ) {
		renderData->setNTextures(2);
		renderData->setTexture(0, this_texture);
		renderData->setTexture(1, secondary_texture);
	}
	/*else if( attribs.bump_texture != NULL ) {
		renderData->setNTextures(2);
		renderData->setTexture(0, this_texture);
		renderData->setTexture(1, attribs.bump_texture);
	}*/
	else if( this_texture != NULL ) {
		renderData->setNTextures(1);
		renderData->setTexture(0, this_texture);
	}
	else {
		renderData->setNTextures(0);
	}
	//renderData->setNeedTangents(attribs.bump_texture != NULL);
	renderData->setNeedTangents(attribs.need_tangents, 1);
	renderData->setHardwareAnimation(attribs.uses_hardware_animation, 1);
}

void Object3D::prepareRenderDatas() {
	//LOG("Object3D::doDisplayLists() : %d\n", this);
	//int n_vertices = frames[0].size();
	if( !check() ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Object3D model is corrupt"));
		return;
	}
	size_t n_vertices = colors.size();
	/*if( this->renderDatas != NULL ) {
		this->n_renderDatas = 0;
		delete [] this->renderDatas;
		this->renderDatas = NULL;
	}*/
	for(size_t i=0;i<getNRenderData();i++) {
		delete renderDatas[i];
	}
	this->renderDatas.clear();
	if( get_n_polys() == 0 ) {
		LOG("    no polys\n");
		ObjectData::prepareRenderDatas();
		return;
	}

	//int same_textureID = -1;
	int texturing = 0; // -1 = off, 0 = mixed, 1 = on
	int secondary_texturing = 0; // -1 = off, 0 = mixed, 1 = on
	//bool any_secondary = false;
	for(size_t i=0;i<this->get_n_polys();i++) {
		int this_texturing = this->polys[i].texture != NULL ?
			1 : -1;
		if(texturing==0)
			texturing = this_texturing;
		else if(texturing != this_texturing) {
			texturing = 0;
			break;
		}
	}
	for(size_t i=0;i<this->get_n_polys();i++) {
		int this_secondary_texturing = this->polys[i].secondary_texture != NULL ?
			1 : -1;
		if(secondary_texturing==0)
			secondary_texturing = this_secondary_texturing;
		else if(secondary_texturing != this_secondary_texturing) {
			secondary_texturing = 0;
			break;
		}
	}
	if( secondary_texturing == 0 ) {
		// not yet supported!
		Vision::setError(new VisionException(this, VisionException::V_PROGRAMMING_ERROR, "Object3Ds shouldn't have mixed secondary textures"));
	}
	set<Texture *> texture_list;
	for(size_t i=0;i<this->get_n_polys();i++) {
		//this->polys[i].textureID = 0;
		texture_list.insert(this->polys[i].texture);
		//secondary_texture_list->addIfAbsent( (void *)this->polys[i].secondary_texture );
	}
	Texture *secondary_texture = (get_n_polys() == 0) ? NULL : this->polys[0].secondary_texture;

	/*for(i=0;i<this->n_polys;i++) {
	if( this->polys[i].secondary_texture != NULL ) {
	any_secondary = true;
	if( texturing == 1 )
	texturing = 0;
	break;
	}
	}*/
	int poly_type = 0; // 3 = all triangles, 4 = all quads, 0 = mixed
	for(size_t i=0;i<this->get_n_polys();i++) {
		int this_poly_type = 0;
		if(this->polys[i].getNVertices() == 3)
			this_poly_type = 3;
		else if(this->polys[i].getNVertices() == 4)
			this_poly_type = 4;
		if(poly_type==0)
			poly_type = this_poly_type;
		else if(poly_type != this_poly_type) {
			poly_type = 0;
			break;
		}
	}

	unsigned char loc_alpha = attribs.alpha;
	// TODO: custom materials
	//bool opaque = loc_alpha == 255 || shadowplane != NULL;
	//bool alpha_channel = loc_alpha < 255;
	//bool alpha_channel = true; // seems to be faster to render with alpha channel when using VBOs
	//bool vx_arrays_blocked = !this->solid || any_secondary || alpha_channel;
	//bool vx_arrays_blocked = any_secondary || alpha_channel;
	//bool vx_arrays_blocked = any_secondary;
	//vx_arrays_blocked = false;
	//vx_arrays_blocked = true;

	//bool vx_arrays = ( texturing == -1 && poly_type == 3 );
	//bool vx_arrays = ( texturing == -1 );
	//bool vx_arrays = ( texturing != 0 );
	bool vx_arrays = true;
	for(size_t i=0;i<this->get_n_polys() && vx_arrays;i++) {
		vx_arrays = ( this->polys[i].getShading()==Polygon3D::CURVED );
	}

	//if( vx_arrays && !vx_arrays_blocked ) {
	//vx_arrays = false;
	if( vx_arrays ) {
		// TODO: unnecessary use of va_indices
		//LOG("    using VERTEX BUFFERS\n");
		this->renderDatas.resize( texture_list.size() );

		//RenderData *renderData = &this->renderDatas[0];

		//for(int rd=0;rd<this->n_renderDatas && vx_arrays;rd++)
		set<Texture *>::iterator iter = texture_list.begin();
		for(size_t rd=0;rd<this->getNRenderData() && vx_arrays;rd++,++iter)
		{
			//RenderData *renderData = &this->renderDatas[rd];
			this->renderDatas[rd] = new RenderData();
			RenderData *renderData = this->renderDatas[rd];

			// TODO: only using a subset of vertices (otherwise unoptimised may be faster!)
			//int this_textureID = (int)texture_list->get(rd);
			//Texture *this_texture = static_cast<Texture *>(texture_list->get(rd));
			//Texture *this_texture = texture_list->at(rd);
			Texture *this_texture = *iter;
			//renderData->initVertexArrays(n_frames, n_vertices, !all_same_color, alpha_channel, true, texturing!=-1);
			const bool want_normals = true;
			//renderData->initVertexArrays(n_frames, n_vertices, !all_same_color, alpha_channel, want_normals, this_texture!=NULL || attribs.bump_texture!=NULL);
			//renderData->initVertexArrays(n_frames, n_vertices, true, alpha_channel, want_normals, this_texture!=NULL || attribs.bump_texture!=NULL);
			bool want_texcoords = this_texture!=NULL || secondary_texture!=NULL;
			//renderData->initVertexArrays(n_frames, n_vertices, true, /*alpha_channel,*/ want_normals, want_texcoords);
			renderData->initVertexArrays(n_frames, n_vertices, want_normals);
			initRenderData(renderData, this_texture);
			//int n_col_step = alpha_channel ? 4 : 3;
			for(size_t vx=0;vx<n_vertices && vx_arrays;vx++) {
				//if( renderData->va_colors != NULL ) {
				//if( renderData->va_colors.size() != 0 ) {
				if( true ) {
					//Vertex3D *vertices = this->getVertices(0);
					//Color color = vertices[vx].color;
					Color color = colors[vx];
					/*renderData->va_colors[n_col_step*vx] = (unsigned char)color.getR();
					renderData->va_colors[n_col_step*vx+1] = (unsigned char)color.getG();
					renderData->va_colors[n_col_step*vx+2] = (unsigned char)color.getB();
					if( alpha_channel )
						renderData->va_colors[n_col_step*vx+3] = loc_alpha;*/
					renderData->setColor(vx, 0, (unsigned char)color.getR());
					renderData->setColor(vx, 1, (unsigned char)color.getG());
					renderData->setColor(vx, 2, (unsigned char)color.getB());
					if( renderData->hasAlphaChannel() )
						renderData->setColor(vx, 3, loc_alpha);
				}
				//if( renderData->va_texcoords != NULL ) {
				//if( renderData->va_texcoords.size() != 0 ) {
				if( want_texcoords ) {
					float tx = 0, ty = 0;
					bool found = false;
					for(size_t i=0;i<this->get_n_polys() && vx_arrays;i++) {
						Polygon3D *poly = &this->polys[i];
						//if( poly->texture == renderData->texture ) {
						//if( poly->texture == NULL || poly->texture == renderData->getTexture(0) ) {
						if( poly->texture == this_texture ) {
							for(size_t j=0;j<poly->getNVertices();j++) {
								if( poly->vlist[j] == vx ) {
									if( !found ) {
										found = true;
										tx = poly->tx[j];
										ty = poly->ty[j];
									}
									else if( tx != poly->tx[j] || ty != poly->ty[j] ) {
										vx_arrays = false;
									}
								}
							}
						}
					}
					if( found ) {
						/*renderData->va_texcoords[2*vx] = tx;
						renderData->va_texcoords[2*vx+1] = ty;*/
						renderData->setTexCoord(vx, tx, ty);
					}
					else {
						//vx_arrays = false;
						// vertex isn't used by this renderData
						/*renderData->va_texcoords[2*vx] = 0;
						renderData->va_texcoords[2*vx+1] = 0;*/
						renderData->setTexCoord(vx, 0.0f, 0.0f);
					}
				}
				/*Color color;
				bool found = false;
				for(int i=0;i<this->n_polys && vx_arrays;i++) {
				Polygon3D *poly = &this->polys[i];
				for(int j=0;j<poly->n_vertices;j++) {
				if( poly->vlist[j] == vx ) {
				if( !found ) {
				found = true;
				color.set(&poly->col);
				}
				else if( !color.equals(&poly->col) ) {
				vx_arrays = false;
				}
				}
				}
				}
				if( found ) {
				renderData.va_colors[3*vx] = (unsigned char)color.getR();
				renderData.va_colors[3*vx+1] = (unsigned char)color.getG();
				renderData.va_colors[3*vx+2] = (unsigned char)color.getB();
				}
				else {
				vx_arrays = false;
				}*/
			}
			for(int frame = 0/*,va_index = 0*/;frame<this->n_frames && vx_arrays;frame++) {
				const Vertex3D *vertices = this->getVertices(frame);
				for(size_t vx=0;vx<n_vertices && vx_arrays;vx++) {
					const Vertex3D *vertex = &vertices[vx];
					//if( renderData->va_normals != NULL ) {
					//if( renderData->va_normals.size() != 0 ) {
					if( want_normals ) {
						/*renderData->va_normals[3*va_index] = vertex->normal.x;
						renderData->va_normals[3*va_index+1] = vertex->normal.y;
						renderData->va_normals[3*va_index+2] = vertex->normal.z;*/
						renderData->setNormal(frame, vx, vertex->normal);
					}
					/*renderData->va_vertices[3*va_index] = vertex->x;
					renderData->va_vertices[3*va_index+1] = vertex->y;
					renderData->va_vertices[3*va_index+2] = vertex->z;*/
					renderData->setVertex(frame, vx, *vertex);
					//va_index++;
				}
			}
			if( vx_arrays ) {
				renderData->setNVertices(n_vertices);
				int n_renderlength = 0;
				//Polygon3D *polyptr = this->polys;
				Polygon3D *polyptr = &*this->polys.begin();

				//
				/*
				for(int i=0;i<this->n_polys;i++,polyptr++) {
				renderData->n_renderlength += polyptr->n_vertices;
				}
				renderData->va_indices = new unsigned short[renderData->n_renderlength];
				polyptr = this->polys;
				for(int i=0,count = 0;i<this->n_polys;i++,polyptr++) {
				for(short j=0;j<polyptr->n_vertices;j++) {
				renderData->va_indices[count++] = polyptr->vlist[j];
				}
				}*/
				//

				//
				for(size_t i=0;i<this->get_n_polys();i++,polyptr++) {
					//if( polyptr->texture == NULL || polyptr->texture == renderData->getTexture(0) ) {
					if( polyptr->texture == this_texture ) {
						n_renderlength += (unsigned short)(3 * (polyptr->getNVertices()-2));
					}
				}
				//renderData->va_indices = new unsigned short[renderData->n_renderlength];
				renderData->setRenderlength(true, n_renderlength);
				//renderData->va_indices.resize(renderData->n_renderlength);
				//polyptr = this->polys;
				polyptr = &*this->polys.begin();
				for(size_t i=0,count = 0;i<this->get_n_polys();i++,polyptr++) {
					//if( polyptr->texture == NULL || polyptr->texture == renderData->getTexture(0) ) {
					if( polyptr->texture == this_texture ) {
						//if( count + polyptr->getNVertices() >= 65536 ) {
						if( count + (3 * (polyptr->getNVertices()-2)) >= 65536 ) {
							Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Polygon index overflow"));
						}
						for(size_t j=0;j<polyptr->getNVertices()-2;j++) {
							/*renderData->va_indices[count++] = (unsigned short)(polyptr->vlist[0]);
							renderData->va_indices[count++] = (unsigned short)(polyptr->vlist[j+1]);
							renderData->va_indices[count++] = (unsigned short)(polyptr->vlist[j+2]);*/
							renderData->setVaIndex(count++, (unsigned short)(polyptr->vlist[0]));
							renderData->setVaIndex(count++, (unsigned short)(polyptr->vlist[j+1]));
							renderData->setVaIndex(count++, (unsigned short)(polyptr->vlist[j+2]));
							/*if( renderData->va_indices[count-3] == renderData->va_indices[count-2] ||
								renderData->va_indices[count-2] == renderData->va_indices[count-1] ||
								renderData->va_indices[count-1] == renderData->va_indices[count-3] ) {*/
							if( polyptr->vlist[0] == polyptr->vlist[j+1] ||
								polyptr->vlist[j+1] == polyptr->vlist[j+2] ||
								polyptr->vlist[j+2] == polyptr->vlist[0] ) {
								Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Degenerate model"));
							}
						}
					}
				}
				bool need_va_indices = false;
				for(size_t i=0;i<renderData->getRenderlength() && !need_va_indices;i++) {
					//need_va_indices = renderData->va_indices[i] != i;
					need_va_indices = renderData->getVaIndex(i) != i;
				}
				if( !need_va_indices ) {
					/*delete [] renderData->va_indices;
					renderData->va_indices = NULL;*/
					//renderData->va_indices.clear();
					renderData->clearVaIndices();
				}
				//

			}

		}
		if( !vx_arrays ) {
			//renderData.free();
			/*this->n_renderDatas = 0;
			delete [] this->renderDatas;
			this->renderDatas = NULL;*/
			this->renderDatas.clear();
		}
	}

	//if( !vx_arrays && texturing != 0 && !vx_arrays_blocked ) {
	//if( !vx_arrays && !vx_arrays_blocked ) {
	if( !vx_arrays ) {
		//poly_type = 0;
		if( poly_type == 4 )
			poly_type = 0; // seems to work faster rendering as two triangles!
		//LOG("    using UNOPTIMIZED VERTEX BUFFERS\n");
		vx_arrays = true;

		//this->n_renderDatas = 1;
		/*this->n_renderDatas = texture_list->size();
		this->renderDatas = new RenderData[this->n_renderDatas];*/
		this->renderDatas.resize( texture_list.size() );

		//RenderData *renderData = &this->renderDatas[0];

		//for(int rd=0;rd<this->n_renderDatas && vx_arrays;rd++)
		set<Texture *>::iterator iter = texture_list.begin();
		for(size_t rd=0;rd<this->getNRenderData() && vx_arrays;rd++,++iter)
		{
			//RenderData *renderData = &this->renderDatas[rd];
			this->renderDatas[rd] = new RenderData();
			RenderData *renderData = this->renderDatas[rd];

			// TODO: only using a subset of vertices
			//int this_textureID = (int)texture_list->get(rd);
			//Texture *this_texture = static_cast<Texture *>(texture_list->get(rd));
			//Texture *this_texture = texture_list->at(rd);
			Texture *this_texture = *iter;

			size_t n_unshared_vertices = 0;
			size_t n_renderlength = 0;
			for(size_t i=0;i<this->get_n_polys();i++) {
				Polygon3D *poly = &this->polys[i];
				if( poly->texture == this_texture ) {
					n_unshared_vertices += poly->getNVertices();
					if( poly_type == 4 )
						n_renderlength += poly->getNVertices();
					else
						n_renderlength += 3 * (poly->getNVertices()-2);
				}
			}
			//LOG("    n_unshared_vertices: %d\n", n_unshared_vertices);
			//LOG("    n_renderlength: %d\n", n_renderlength);
			//renderData->initVertexArrays(n_frames, n_unshared_vertices, !all_same_color, alpha_channel, true, texturing!=-1);
			//renderData->initVertexArrays(n_frames, n_unshared_vertices, !all_same_color, alpha_channel, true, this_texture!=0 || attribs.bump_texture!=NULL);
			//renderData->initVertexArrays(n_frames, n_unshared_vertices, true, alpha_channel, true, this_texture!=0 || attribs.bump_texture!=NULL);
			bool want_texcoords = this_texture!=NULL || secondary_texture!=NULL;
			//renderData->initVertexArrays(n_frames, n_unshared_vertices, true, /*alpha_channel,*/ true, want_texcoords);
			renderData->initVertexArrays(n_frames, n_unshared_vertices, true);
#if 0
			renderData->solid = attribs.solid;
			renderData->blending = !opaque;
			renderData->blend_type = attribs.blend_type;
			/*if( renderData->va_colors == NULL ) {
				renderData->cols[0] = same_col.getR();
				renderData->cols[1] = same_col.getG();
				renderData->cols[2] = same_col.getB();
				renderData->cols[3] = loc_alpha;
			}*/
			renderData->material_specular[0] = attribs.specular.getRf();
			renderData->material_specular[1] = attribs.specular.getGf();
			renderData->material_specular[2] = attribs.specular.getBf();
			renderData->material_specular[3] = attribs.specular.getAf();
			renderData->texture = this_texture;
			renderData->secondary_texture = secondary_texture;
			renderData->bump_texture = attribs.bump_texture;
			renderData->setNeedTangents(attribs.bump_texture != NULL);
#endif
			initRenderData(renderData, this_texture);
			renderData->setNVertices(n_unshared_vertices);
			if( n_renderlength >= 65536 ) {
				Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Polygon index overflow"));
			}
			//renderData->n_renderlength = (unsigned short)n_renderlength;
			renderData->setRenderlength(poly_type != 3 && poly_type != 4, (unsigned short)n_renderlength);
			//int n_col_step = alpha_channel ? 4 : 3;
			for(size_t i=0,count=0;i<this->get_n_polys();i++) {
				Polygon3D *poly = &this->polys[i];
				if( poly->texture == this_texture ) {
					//Vertex3D *vertices = this->getVertices(0);
					for(size_t j=0;j<poly->getNVertices();j++) {
						//Vertex3D *vx = &vertices[ poly->vlist[j] ];
						//Color color = vx->color;
						Color color = colors[ poly->vlist[j] ];
						//if( renderData->va_colors != NULL ) {
						//if( renderData->va_colors.size() != 0 ) {
						if( true ) {
							/*renderData->va_colors[n_col_step*count] = (unsigned char)color.getR();
							renderData->va_colors[n_col_step*count+1] = (unsigned char)color.getG();
							renderData->va_colors[n_col_step*count+2] = (unsigned char)color.getB();
							if( alpha_channel )
								renderData->va_colors[n_col_step*count+3] = loc_alpha;*/
							renderData->setColor(count, 0, (unsigned char)color.getR());
							renderData->setColor(count, 1, (unsigned char)color.getG());
							renderData->setColor(count, 2, (unsigned char)color.getB());
							if( renderData->hasAlphaChannel() )
								renderData->setColor(count, 3, loc_alpha);
						}
						//if( texturing != -1 ) {
						//if( this_texture != 0 ) {
						//if( renderData->va_texcoords != NULL ) {
						//if( renderData->va_texcoords.size() != 0 ) {
						if( want_texcoords ) {
							/*renderData->va_texcoords[2*count] = poly->tx[j];
							renderData->va_texcoords[2*count+1] = poly->ty[j];*/
							renderData->setTexCoord(count, poly->tx[j], poly->ty[j]);
						}
						count++;
					}
				}
			}
			if( poly_type != 3 && poly_type != 4 ) {
				//renderData->va_indices = new unsigned short[renderData->n_renderlength];
				//renderData->va_indices.resize(renderData->n_renderlength);
				for(size_t i=0,count=0,offset=0;i<this->get_n_polys();i++) {
					Polygon3D *poly = &this->polys[i];
					if( poly->texture == this_texture ) {
						if( offset + poly->getNVertices() >= 65536 ) {
							Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Polygon index overflow"));
						}
						for(size_t j=0;j<poly->getNVertices()-2;j++) {
							/*renderData->va_indices[count++] = (unsigned short)(offset);
							renderData->va_indices[count++] = (unsigned short)(offset+j+1);
							renderData->va_indices[count++] = (unsigned short)(offset+j+2);*/
							renderData->setVaIndex(count++, (unsigned short)(offset));
							renderData->setVaIndex(count++, (unsigned short)(offset+j+1));
							renderData->setVaIndex(count++, (unsigned short)(offset+j+2));
							/*if( renderData->va_indices[count-3] == renderData->va_indices[count-2] ||
								renderData->va_indices[count-2] == renderData->va_indices[count-1] ||
								renderData->va_indices[count-1] == renderData->va_indices[count-3] ) {
								Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "Degenerate model"));
							}*/
						}
						offset += poly->getNVertices();
					}
				}
			}

			for(int frame=0/*,count=0*/;frame<this->n_frames;frame++) {
				//Vertex3D *vxs = this->frames[frame];
				Vertex3D *vxs = &*frames[frame].begin();
				for(size_t i=0,count=0;i<this->get_n_polys();i++) {
					Polygon3D *poly = &this->polys[i];
					if( poly->texture == this_texture ) {
						for(size_t j=0;j<poly->getNVertices();j++) {
							Vertex3D *vx = &vxs[ poly->vlist[j] ];
							/*renderData->va_vertices[3*count] = vx->x;
							renderData->va_vertices[3*count+1] = vx->y;
							renderData->va_vertices[3*count+2] = vx->z;*/
							renderData->setVertex(frame, count, *vx);
							Vector3D normal;
							if( poly->getShading()==Polygon3D::CURVED )
								normal = vx->normal;
							else
								normal = this->poly_normals_frame[frame][i];
							/*renderData->va_normals[3*count] = vx->normal.x;
							renderData->va_normals[3*count+1] = vx->normal.y;
							renderData->va_normals[3*count+2] = vx->normal.z;*/
							/*renderData->va_normals[3*count] = normal.x;
							renderData->va_normals[3*count+1] = normal.y;
							renderData->va_normals[3*count+2] = normal.z;*/
							renderData->setNormal(frame, count, normal);
							count++;
						}
					}
				}
			}

		}
	}

	/*if( !vx_arrays && !vx_arrays_blocked )
	{
	for(int i=0;i<n_renderDatas;i++)
	renderDatas[i].prepare();
	}*/
	// END VERTEX BUFFER CODE

	ObjectData::prepareRenderDatas();

}

/*void Object3D::subdivide(VI_Face *face) {
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_renderdata = false;
	this->calc_shadows = false; // not sure if this is needed?
	Polygon3D *d_face = static_cast<Polygon3D *>(face); // cast to the internal representation
	for(int frame=0;frame<this->n_frames;frame++) {
		const Vertex3D *verts = &*this->frames[frame].begin();
		Vertex3D pos(0,0,0);
		for(int v=0;v<d_face->getNVertices();v++) {
			pos += verts[ d_face->vlist[v] ];
		}
		pos /= (float)d_face->getNVertices();
		// add new vertex
		this->frames[frame].push_back( pos );
	}
}*/

void Object3D::subdivide() {
	this->calc_pn = false;
	this->calc_vn = false;
	this->calc_renderdata = false;
	this->calc_shadows = false; // not sure if this is needed?
	size_t n_vx_indx = n_frames == 0 ? 0 : frames[0].size();
	for(int frame=0;frame<this->n_frames;frame++) {
		const Vertex3D *verts = &*this->frames[frame].begin();
		vector<Vertex3D> new_verts;
		for(size_t i=0;i<frames[frame].size();i++) {
			new_verts.push_back( verts[i] );
		}
		for(size_t p=0;p<this->get_n_polys();p++) {
			Polygon3D *poly = &this->polys[p];
			Vertex3D pos(0,0,0);
			for(size_t v=0;v<poly->getNVertices();v++) {
				pos += verts[ poly->vlist[v] ];
			}
			pos /= (float)poly->getNVertices();
			// add new vertex
			new_verts.push_back( pos );
			if( frame == 0 ) {
				// new vertices defaults to white, TODO: take an average
				Color col;
				this->colors.push_back(col);
			}
		}
		this->frames[frame] = new_verts;
	}

	//int n_vx_indx = this->n_vertices;
	//this->n_vertices = (unsigned short)n_new_verts;
	size_t n_old_polys = this->get_n_polys();
	for(size_t p=0;p<n_old_polys;p++) {
		//Polygon3D *poly = &this->polys[p];
		Polygon3D poly_copy = this->polys[p]; // need to take a copy, as pointer may become invalid if vector is resized!
		const Polygon3D *poly = &poly_copy;
		float tx = 0;
		float ty = 0;
		float secondary_tx = 0;
		float secondary_ty = 0;
		for(size_t v=0;v<poly->getNVertices();v++) {
			tx += poly->tx[v];
			ty += poly->ty[v];
			secondary_tx += poly->secondary_tx[v];
			secondary_ty += poly->secondary_ty[v];
		}
		tx /= poly->getNVertices();
		ty /= poly->getNVertices();
		secondary_tx /= poly->getNVertices();
		secondary_ty /= poly->getNVertices();
		for(size_t v=0;v<poly->getNVertices();v++) {
			//Polygon3D new_poly( v, (v+1) % poly->n_vertices, n_vx_indx );
			Polygon3D new_poly = *poly;
			new_poly.setNVertices(3);
			size_t n_v = (v+1) % poly->getNVertices();
			new_poly.vlist[0] = poly->vlist[v];
			new_poly.vlist[1] = poly->vlist[n_v];
			new_poly.vlist[2] = n_vx_indx;
			new_poly.tx[0] = poly->tx[v];
			new_poly.ty[0] = poly->ty[v];
			new_poly.tx[1] = poly->tx[n_v];
			new_poly.ty[1] = poly->ty[n_v];
			new_poly.tx[2] = tx;
			new_poly.ty[2] = ty;
			new_poly.secondary_tx[0] = poly->secondary_tx[v];
			new_poly.secondary_ty[0] = poly->secondary_ty[v];
			new_poly.secondary_tx[1] = poly->secondary_tx[n_v];
			new_poly.secondary_ty[1] = poly->secondary_ty[n_v];
			new_poly.secondary_tx[2] = secondary_tx;
			new_poly.secondary_ty[2] = secondary_ty;
			if( v+1 < poly->getNVertices() )
				this->addPolygon(new_poly);
			else {
				//*poly = new_poly;
				this->polys[p] = new_poly;
				break;
			}
		}
		n_vx_indx++;
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

/*void Object3D::triangulate(bool reprepare) {
}*/

size_t Object3D::getFaces(VI_Face ***faces) {
	size_t n_faces = this->get_n_polys();
	if( faces != NULL ) {
		*faces = new VI_Face *[n_faces];
		for(size_t i=0;i<n_faces;i++) {
			(*faces)[i] = this->getFace(i);
		}
	}
	return n_faces;
}

VI_Face *Object3D::findFace(const char *name) {
	for(vector<Polygon3D>::iterator iter = this->polys.begin(); iter != this->polys.end(); ++iter) {
		Polygon3D *poly = &*iter;
		if( stricmp(poly->getName(), name) == 0 ) {
			return poly;
		}
	}
	return NULL;
}

/*void Object3D::setPolygonBaseColor(Polygon3D *poly,Color col) {
	this->calc_renderdata = false;
	for(int i=0;i<poly->getNVertices();i++) {
		Color *color = &colors[ poly->vlist[i] ];
		*color = col;
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}*/

void Object3D::setPolygonBaseColori(VI_Face *face, unsigned char r, unsigned char g, unsigned char b) {
	Polygon3D *d_face = static_cast<Polygon3D *>(face); // cast to the internal representation
	Color col;
	col.seti(r,g,b);

	this->calc_renderdata = false;
	for(size_t i=0;i<d_face->getNVertices();i++) {
		Color *color = &colors[ d_face->vlist[i] ];
		*color = col;
	}
	if( !Vision::isLocked() ) {
		this->prepare();
	}

	//this->setPolygonBaseColor(d_face, col);
}

/*void Object3D::setPolygonTexture(Polygon3D *poly,Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v) {
	this->calc_renderdata = false;
	poly->setTexture(texture, this->getVertices(0), mirror, skip, have_size, size_u, size_v);
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}*/

void Object3D::setPolygonTexture(VI_Face *face,VI_Texture *texture,bool mirror,int skip,bool have_size,float size_u,float size_v) {
	Polygon3D *d_face = static_cast<Polygon3D *>(face); // cast to the internal representation
	//Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation

	this->calc_renderdata = false;
	d_face->setTexture(texture, this->getVertices(0), mirror, skip, have_size, size_u, size_v);
	if( !Vision::isLocked() ) {
		this->prepare();
	}
	//this->setPolygonTexture(d_face, d_texture, mirror, skip, have_size, size_u, size_v);
}

void Object3D::setPolygonTexture(VI_Face *face,VI_Texture *texture,const Vector3D *origin, const Vector3D *u_dir, const Vector3D *v_dir) {
	Polygon3D *d_face = static_cast<Polygon3D *>(face); // cast to the internal representation

	this->calc_renderdata = false;
	d_face->setTexture(texture, this->getVertices(0), origin, u_dir, v_dir);
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Object3D::invertPolygon(VI_Face *face) {
	Polygon3D *d_face = static_cast<Polygon3D *>(face); // cast to the internal representation
	d_face->invert();
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}


Polygon3D::Polygon3D() /*: VisionObject()*/ {
	init();
}

Polygon3D::Polygon3D(size_t v0,size_t v1,size_t v2) {
	if( v0 == v1 || v1 == v2 || v2 == v0 ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "degenerate polygon specified"));
	}
	init();
	this->setNVertices(3);
	vlist[0] = v0;
	vlist[1] = v1;
	vlist[2] = v2;
}

Polygon3D::Polygon3D(size_t v0,size_t v1,size_t v2,size_t v3) {
	if( v0 == v1 || v0 == v2 || v0 == v3 || v1 == v2 || v1 == v3 || v2 == v3 ) {
		Vision::setError(new VisionException(this, VisionException::V_MODEL_ERROR, "degenerate polygon specified"));
	}
	init();
	this->setNVertices(4);
	vlist[0] = v0;
	vlist[1] = v1;
	vlist[2] = v2;
	vlist[3] = v3;
}

Polygon3D::Polygon3D(const size_t *vlist, size_t n_vertices) {
	init();
	this->setNVertices(n_vertices);
	for(size_t i=0;i<n_vertices;i++) {
		this->vlist[i] = vlist[i];
	}
}

/*Polygon3D::Polygon3D(const Polygon3D * poly) {
	*this = *poly;
}*/

void Polygon3D::init() {
	//n_vertices=0;
	texture=0;
	secondary_texture=0;
	shading = CURVED;
	//col.seti(255,255,255);
}

void Polygon3D::setNVertices(size_t n_vertices) {
	//this->n_vertices = n_vertices;
	this->vlist.resize(n_vertices);
	this->tx.resize(n_vertices);
	this->ty.resize(n_vertices);
	this->secondary_tx.resize(n_vertices);
	this->secondary_ty.resize(n_vertices);
}

void Polygon3D::setTextureWrap(VI_Texture *texture, const TextureUV **text_uvs) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	this->texture = d_texture;
	vector<size_t>::iterator vptr = vlist.begin();
	for(size_t i=0;i<getNVertices();i++) {
		tx[i] = text_uvs[ *vptr ]->u;
		ty[i] = text_uvs[ *vptr ]->v;
		vptr++;
	}
}

/*void Polygon3D::setTexture(Texture *texture,const Vertex3D *vertices,bool mirror) {
	this->setTexture(texture,vertices,mirror,0);
}

void Polygon3D::setTexture(Texture *texture,const Vertex3D *vertices,bool mirror,int skip) {
	//this->setTexture(texture,vertices,mirror,skip,1.0f,1.0f);
	this->setTexture(texture,vertices,mirror,skip,false,0.0f,0.0f);
}*/

void Polygon3D::setTexture(VI_Texture *texture) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	this->texture = d_texture;
	// don't change texture coords - should be set by caller
}

//void Polygon3D::setTexture(Texture *texture,const Vertex3D *vertices,bool mirror,int skip,float scale_u,float scale_v) {
void Polygon3D::setTexture(VI_Texture *texture,const Vertex3D *vertices,bool mirror,int skip,bool have_size,float size_u,float size_v) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	this->texture = d_texture;
	if( vertices == NULL ) {
		Vision::setError(new VisionException(this, VisionException::V_MODELLER_ERROR, "No vertices supplied to Polygon::setTexture"));
		return;
	}
	size_t skip_u = skip;
	if( skip < 0 ) { // allow -ve values
		//skip += getNVertices();
		skip_u = getNVertices() - skip;
	}

	/*if( getNVertices() > 4 ) {
		Vision::setError(new VisionException(this,VisionException::V_MODELLER_ERROR,"Polygon::setTexture not supported for >4 vertices - use setTextureWrap instead"));
	}*/
	Vertex3D v0 = vertices[ vlist[skip_u] ];
	Vertex3D v1 = vertices[ vlist[(skip_u+1) % getNVertices()] ];
	Vector3D tangent = v1 - v0;
	if( tangent.square() <= V_TOL_MACHINE ) {
		Vision::setError(new VisionException(this, VisionException::V_MODELLER_ERROR, "Polygon::setTexture coi vertices"));
		return;
	}
	//tangent.normalise();

	Vector3D v2mv1;
	Vector3D norm;
	bool norm_found = false;
	for(size_t i=2;i<getNVertices() && !norm_found;i++) {
		Vertex3D v2 = vertices[ vlist[(skip_u+i) % getNVertices()] ];
		v2mv1 = v2 - v1;
		norm = v2mv1 ^ tangent;
		if( norm.square() > V_TOL_MACHINE )
			norm_found = true;
	}
	if( !norm_found ) {
		Vision::setError(new VisionException(this, VisionException::V_MODELLER_ERROR, "Polygon::setTexture not supported for degenerate textures"));
		return;
	}
	norm.normalise();

	Vector3D bitan = tangent ^ norm;
	/*bitan.normalise();
	float perp_scale = v2mv1 % bitan;
	bitan *= perp_scale;*/

	if( mirror ) {
		swap(tangent, bitan);
	}

	if( have_size ) {
		tangent.normalise();
		tangent *= size_u;
		bitan.normalise();
		bitan *= size_v;
	}

	float tan_sq = tangent.square();
	float bitan_sq = bitan.square();

	for(size_t i=0;i<getNVertices();i++) {
		Vertex3D vx = vertices[ vlist[i] ];
		vx -= v0;
		tx[i] = (vx % tangent) / tan_sq;
		ty[i] = (vx % bitan) / bitan_sq;
	}

	/*
	const float scale_u = 1.0f;
	const float scale_v = 1.0f;
	if(mirror) {
		for(int i=0;i<getNVertices();i++) {
			int m_i = (i + skip_u) % getNVertices();
			if(m_i==0) {
				tx[i]=0.0f;
				ty[i]=0.0f;
			}
			else if(m_i==1) {
				tx[i]=0.0f;
				ty[i]=scale_v;
			}
			else if(m_i==2) {
				tx[i]=scale_u;
				ty[i]=scale_v;
			}
			else {
				tx[i]=scale_u;
				ty[i]=0.0f;
			}
		}
	}
	else {
		for(int i=0;i<getNVertices();i++) {
			int m_i = (i + getNVertices() - skip_u) % getNVertices();
			if(m_i==0) {
				tx[i]=0.0f;
				ty[i]=0.0f;
			}
			else if(m_i==1) {
				tx[i]=scale_u;
				ty[i]=0.0f;
			}
			else if(m_i==2) {
				tx[i]=scale_u;
				ty[i]=scale_v;
			}
			else {
				tx[i]=0.0f;
				ty[i]=scale_v;
			}
		}
	}*/
}

void Polygon3D::setTexture(VI_Texture *texture,const Vertex3D *vertices,const Vector3D *origin, const Vector3D *u_dir, const Vector3D *v_dir) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	this->texture = d_texture;
	if( vertices == NULL ) {
		Vision::setError(new VisionException(this, VisionException::V_MODELLER_ERROR, "No vertices supplied to Polygon::setTexture"));
		return;
	}

	float u_sq = u_dir->square();
	float v_sq = v_dir->square();

	for(size_t i=0;i<getNVertices();i++) {
		Vertex3D vx = vertices[ vlist[i] ];
		vx -= *origin;
		tx[i] = (vx % *u_dir) / u_sq;
		ty[i] = (vx % *v_dir) / v_sq;
	}
}


/*void Polygon3D::rotateTexture(int n_rotate) {
	vector<float> copy_tx, copy_ty;
	copy_tx.reserve( getNVertices() );
	copy_ty.reserve( getNVertices() );
	for(int i=0;i<getNVertices();i++) {
		copy_tx.push_back( tx[i] );
		copy_ty.push_back( ty[i] );
	}
	for(int i=0;i<getNVertices();i++) {
		int copy_from = (i + n_rotate) % getNVertices();
		tx[i] = copy_tx[copy_from];
		ty[i] = copy_ty[copy_from];
	}
}*/

void Polygon3D::setSecondaryTexture(Texture *secondary_texture) {
	this->secondary_texture = secondary_texture;
}

void Polygon3D::invert() {
	for(size_t i=0;i<getNVertices()/2;i++) {
		size_t i2 = getNVertices()-1-i;
		size_t vlist2 = vlist[i2];
		vlist[i2] = vlist[i];
		vlist[i] = vlist2;
		float tx2 = tx[i2];
		tx[i2] = tx[i];
		tx[i] = tx2;
		float ty2 = ty[i2];
		ty[i2] = ty[i];
		ty[i] = ty2;
	}
}


/*Face::Face() : VisionObject() {
	this->owner = NULL;
	this->index = -1;
}

Face::~Face() {
}*/

/*void Light::calculateLightRadius() {
this->lightRadius = 10.0;
this->lightRadiusSquared = lightRadius*lightRadius;
}*/

void Light::setLightAmbient(Color *col) {
	this->ambient = *col;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Light::setLightDiffuse(Color *col) {
	this->diffuse = *col;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Light::setLightSpecular(Color *col) {
	this->specular = *col;
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Light::setLightAttenuation(float attenuation[3]) {
	for(int i=0;i<3;i++)
		this->attenuation[i] = attenuation[i];
	if( !Vision::isLocked() ) {
		this->prepare();
	}
}

void Light::setLightAmbienti(unsigned char r, unsigned char g, unsigned char b) {
	Color col;
	col.seti(r,g,b);
	this->setLightAmbient(&col);
}

void Light::setLightDiffusei(unsigned char r, unsigned char g, unsigned char b) {
	Color col;
	col.seti(r,g,b);
	this->setLightDiffuse(&col);
}

void Light::setLightSpeculari(unsigned char r, unsigned char g, unsigned char b) {
	Color col;
	col.seti(r,g,b);
	this->setLightSpecular(&col);
}

//void Light::setBoundingRadius() {
void Light::setBounds() {
	if( this->calc_bounds ) {
		return;
	}

	//this->boundingRadius = 12.0; // TODO: calculate
	//this->boundingRadiusSquared = boundingRadius*boundingRadius;
	//this->boundingSphere.setRadius( 1000.0 ); // TODO: calculate
	float radius = 1000.0f;
	//const float K = 0.25;
	const float K = 0.1f;
	if( attenuation[1] == 0.0 && attenuation[2] == 0.0 ) {
		// only constant attenuation
	}
	else if( attenuation[2] == 0.0 ) {
		// linear is dominant
		float amb_str = this->ambient.getMaxRGBf();
		float diff_str = this->diffuse.getMaxRGBf();
		float str = amb_str + diff_str;
		//float K_12 = str / ( attenuation[0] + attenuation[1] * 12.0 );
		// a[0] + a[1] D = str / K.
		// D = ( str / K - a[0] ) / a[1];
		radius = ( str / K - attenuation[0] ) / attenuation[1];
		radius = max(radius, 0.0f);
		//this->boundingSphere.setRadius( 12.0 ); // TODO: calculate
	}
	else {
		// quadratic is dominant
		// ( a[0] - str / K ) + a[1] D + a[2] D^2 = 0
		// so a = a[2], b = a[1], c = a[0] - str / K
		float amb_str = this->ambient.getMaxRGBf();
		float diff_str = this->diffuse.getMaxRGBf();
		float str = amb_str + diff_str;
		float a = attenuation[2];
		float b = attenuation[1];
		float c = attenuation[0] - str / K;
		float disc = b*b - 4.0f*a*c;
		if( disc >= 0.0 ) {
			float sqrt_d = sqrt(disc);
			float sol0 = ( - b - sqrt_d ) / (2.0f*a);
			float sol1 = ( - b + sqrt_d ) / (2.0f*a);
			if( sol0 >= 0 && sol1 >= 0 ) {
				radius = min(sol0, sol1);
			}
			else if( sol0 >= 0 && sol1 < 0 ) {
				radius = sol0;
			}
			else if( sol1 >= 0 && sol0 < 0 ) {
				radius = sol1;
			}
			radius = max(radius, 0.0f);
		}
	}
	//radius = 1000.0;
	this->boundingSphere.setRadius( radius );
	//this->boundingAABB.available = false;
	this->has_boundingBox = false;
	ObjectData::setBounds();
}

/*void Light::doDisplayLists() {
	// nothing
	ObjectData::doDisplayLists();
}*/

void Light::render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData) {
}

void Light::renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview) {
	// nothing to add to main render queue
	//if( !outofview || entity->getPoint()->isInfinite() ) {
	if( !outofview ) {
		//if( true ) {
		//g->c_queue->renderQueue_lights->add(entity);
		queue->renderQueue_lights->push_back(node);
	}
}

/*void Explosion::doDisplayLists() {
	// nothing
	ObjectData::doDisplayLists();
}*/

void Explosion::render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData) {
	if( lighting_pass ) {
		return;
	}
	/*const Material *material = entity->getMaterial();
	if( material->isColorForced() ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "Can't override material with Explosion types"));
	}*/
	float r = node->getTimeExistedMS() / 1000.0f;
	float alpha = 1.0f - r;
	float scale = this->max_size * r;
	//explosion->ambient.add(-r);
	//explosion->diffuse.add(-r);
	/*this->ambient = this->original_ambient;
	this->diffuse = this->original_diffuse;
	this->ambient.scale(alpha);
	this->diffuse.scale(alpha);*/
	this->scaled_ambient = this->ambient;
	this->scaled_diffuse = this->diffuse;
	this->scaled_ambient.scale(alpha);
	this->scaled_diffuse.scale(alpha);

	Explosion *explosion = this;
	if(alpha>=0)
	{
		/*if(phase == Graphics3D::RENDER_LIGHTING && explosion->lit && g->lID < GL_LIGHTMAX) {
		g->lights[g->lID - GL_LIGHT0] = entity;
		g->lID++;
		}*/
		if(phase == RENDER_NONOPAQUE) {
			//renderer->enableLighting(false);
			renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);
			renderer->enableCullFace(false);

			texture->enable();
			renderer->pushMatrix();
			//renderer->setColor4(explosion->diffuse.getR(),explosion->diffuse.getG(),explosion->diffuse.getB(),255*alpha);
			renderer->setFixedFunctionMatSpecularOff();

			Vector3D right, up;
			//g->getViewOrientation(&right, &up);
			renderer->getViewOrientation(&right, &up);
			Vector3D tright = right * ( scale / 2.0f);
			Vector3D tup = up * ( scale / 2.0f);
			Vector3D tdiagp = - ( tright + tup );
			Vector3D tdiagm = tright - tup;

			float texcoord_data[] = {
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f
			};
			float vertex_data[] = {
				-tdiagp.x, -tdiagp.y, -tdiagp.z,
				-tdiagm.x, -tdiagm.y, -tdiagm.z,
				tdiagp.x, tdiagp.y, tdiagp.z,
				tdiagm.x, tdiagm.y, tdiagm.z
			};
			unsigned char color_data[] = { explosion->diffuse.getR(), explosion->diffuse.getG(), explosion->diffuse.getB(), (unsigned char)(255*alpha) };

			if( renderer->getActiveShader() != NULL ) {
				genv->getGraphics3D()->setShaderConstants(this);
			}
			else {
				genv->getGraphics3D()->setFixedFunctionConstants(this);
			}
			renderer->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4, color_data, true, 4);
			/*genv->stats_poly_count += 1;
			genv->stats_vertex_count += 4;
			genv->stats_distinct_vertex_count += 4;
			genv->stats_render_calls_count++;*/
			genv->updateStats(1, 4, 4, 1);

			renderer->popMatrix();

			//renderer->enableLighting(true);
			renderer->enableCullFace(true);
		}
	}
	else {
		node->kill();
		//delete explosion;
		// error!
		//Vision::setError(new VisionException(this,VisionException::V_RENDER_ERROR,"Explosion has -ve alpha"));
	}
}

void Explosion::renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview) {
	float r = node->getTimeExistedMS() / 1000.0f;
	float alpha = 1.0f - r;
	if( alpha < 0 ) {
		return;
	}
	//return;
	if( !outofview ) {
		if( shadowplane == NULL || shadowplane->isRenderToTexture() ) {
			if( node->getPoint()->isInfinite() ) {
				//g->c_queue->renderQueue_infinite->add(entity);
				queue->renderQueue_infinite->push_back(node);
			}
			else {
				//g->c_queue->renderQueue_nonopaque->add(entity);
				//g->c_queue->renderQueue_nonopaque->push_back(entity);
				queue->addNonOpaque(node);
			}
		}
	}
	/*if( g->c_queue->lID < GL_LIGHTMAX && this->lit ) {
	g->c_queue->lights[g->c_queue->lID - GL_LIGHT0] = entity;
	g->c_queue->lID++;
	}*/
	if( !outofview ) {
		//g->c_queue->renderQueue_lights->add(entity);
		queue->renderQueue_lights->push_back(node);
	}
}

BillBoard::BillBoard(Texture *texture, float width, float height,bool blend) {
	this->texture = texture;
	this->width = width;
	this->height = height;
	this->blend = blend;
	this->n_billboards = 1;
	this->init();
}

BillBoard::BillBoard(Texture *texture, float width, float height,bool blend,int n_billboards) {
	this->texture = texture;
	this->width = width;
	this->height = height;
	this->blend = blend;
	this->n_billboards = n_billboards;
	this->init();
}

void BillBoard::init() {
	this->blend_type = V_BLENDTYPE_FADE_SRC;
	this->offsets = new Vector3D[n_billboards];
	for(int i=0;i<n_billboards;i++)
		this->offsets[i].set(0,0,0);
	this->cols = new unsigned char[n_billboards*4];
	for(int i=0;i<n_billboards*4;i++) {
		this->cols[i] = 255;
	}
	this->lock = false;
	this->wave_amp = 0.0;
	this->wave_sp = 0.0;

	int n_vertices = n_billboards*4;
	this->va_vertices = new float[3*n_vertices];
	for(int i=0;i<3*n_vertices;i++) {
		this->va_vertices[i] = 0.0f;
	}

	this->vao_vertices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_VERTICES, 0, true, va_vertices, 3*n_vertices*sizeof(float), 3);
	//this->vao_vertices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_VERTICES, 0, false, va_vertices, 3*n_vertices*sizeof(float), 3);

	this->va_normals = new float[3*n_vertices];
	for(int i=0,c=0;i<n_vertices;i++) {
		this->va_normals[c++] = 0.0;
		this->va_normals[c++] = 1.0;
		this->va_normals[c++] = 0.0;
	}
	this->vao_normals = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_NORMALS, 0, false, va_normals, 3*n_vertices*sizeof(float), 3);

	this->va_texcoords = new float[2*n_vertices];
	for(int i=0,c=0;i<n_billboards;i++) {
		this->va_texcoords[c++] = 0.0f;
		this->va_texcoords[c++] = 1.0f;

		this->va_texcoords[c++] = 1.0f;
		this->va_texcoords[c++] = 1.0f;

		this->va_texcoords[c++] = 1.0f;
		this->va_texcoords[c++] = 0.0f;

		this->va_texcoords[c++] = 0.0f;
		this->va_texcoords[c++] = 0.0f;
	}
	this->vao_texcoords = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, false, va_texcoords, 2*n_vertices*sizeof(float), 2);

	const int color_dim = 3;
	this->va_colors = new unsigned char[color_dim*n_vertices];
	for(int i=0;i<n_billboards;i++) {
		for(int j=0;j<4;j++) {
			for(int k=0;k<color_dim;k++)
				va_colors[(4*i+j)*color_dim+k] = cols[4*i+k];
		}
	}
	/*for(int i=0;i<color_dim*n_vertices;i++) {
		LOG("%d : %d\n", i, va_colors[i]);
	}*/
	this->vao_colors = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_COLORS, 0, true, va_colors, color_dim*n_vertices*sizeof(unsigned char), color_dim);

	int n_renderlength = 0 ;
	if( GraphicsEnvironment::getSingleton()->getRenderer()->supportsQuads() ) {
		// OpenGL is faster if we use Quads rather than triangles
		this->va_indices = NULL;
	}
	else {
		n_renderlength = n_billboards*6;
		this->va_indices = new unsigned short[n_renderlength];
		for(int i=0,index=0,v=0;i<n_billboards;i++) {
			va_indices[index++] = v;
			va_indices[index++] = v+1;
			va_indices[index++] = v+2;
			va_indices[index++] = v;
			va_indices[index++] = v+2;
			va_indices[index++] = v+3;
			v += 4;
		}
	}
	this->vao_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, false, va_indices, n_renderlength*sizeof(unsigned short), 2);

	//this->setOneColori(0, 255, 255, 255);
	//this->vao_colors->update();
}

BillBoard::~BillBoard() {
	delete [] offsets;
	delete [] cols;

	delete vao_vertices;
	delete vao_normals;
	delete vao_texcoords;
	delete vao_colors;
	delete vao_indices;
	delete [] va_vertices;
	delete [] va_normals;
	delete [] va_texcoords;
	delete [] va_colors;
	delete [] va_indices;
}

void BillBoard::setColor(const Color *color) {
	for(int i=0;i<n_billboards;i++) {
		this->setOneColor(i, color);
	}
}

void BillBoard::setOneColor(int i, const Color *color) {
	color->array(&cols[4*i]);

	for(int j=0;j<4;j++) {
		for(int k=0;k<3;k++)
			va_colors[(4*i+j)*3+k] = cols[4*i+k];
	}
	this->vao_colors->update(); // TODO: allow passing offset
}

Vector3D BillBoard::findCentre() const {
	Vector3D centre;
	for(int i=0;i<n_billboards;i++) {
		centre += offsets[i];
	}
	centre /= (float)n_billboards;
	return centre;
}

void BillBoard::setBounds() {
	if( this->calc_bounds ) {
		return;
	}

	Vector3D centre = findCentre();
	float max_radius = 0;
	float small_radius = sqrt( width*width + height*height );
	for(int i=0;i<n_billboards;i++) {
		Vector3D diff = offsets[i] - centre;
		float radius = diff.magnitude() + small_radius;
		if( radius > max_radius )
			max_radius = radius;
	}
	//max_radius = 1000.0;
	this->boundingSphere.centre = centre;
	this->boundingSphere.setRadius( max_radius );
	//this->boundingAABB.available = false; // TODO
	this->has_boundingBox = false;
	ObjectData::setBounds();
}

void BillBoard::render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData) {
	if( lighting_pass ) {
		return;
	}
	//LOG("BillBoard::render %d\n", this);
	/*const Material *material = entity->getMaterial();
	if( material->isColorForced() ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "Can't override material with Billboard types"));
	}*/
	renderer->setFixedFunctionMatSpecularOff();

	texture->enable();

	float off_z = 0.0;
	if( wave_sp > 0.0 ) {
		float time = ((float)Vision::getGameTimeMS()) / 1000.0f;
		off_z = wave_amp * sin(wave_sp * time);
		//off_z = wave_amp;
	}

	Vector3D right, up;
	if( lock ) {
		Vector3D dir;
		renderer->getViewDirection(&dir);
		up.set(0.0, 1.0, 0.0);
		right = dir ^ up;
		if( right.magnitude() <= V_TOL_LINEAR ) {
			right.set(1.0, 0.0, 0.0); // set to arbitrary value
			//return;
		}
		right.normalise();
	}
	else {
		renderer->getViewOrientation(&right, &up);
	}

	/*Vector3D dir = up ^ right;
	right = dir ^ offset;
	right.normalise();
	up = offset;
	up.normalise();*/

	renderer->enableCullFace(false);
	if( this->blend ) {
		//renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
		//renderer->enableLighting(false);
		genv->getGraphics3D()->setBlend(this->blend_type, lighting_pass);
	}
	else {
		renderer->setAlphaTestMode(Renderer::RENDERSTATE_ALPHA_TEST_ON);
	}
	Vector3D normal(0.0, 1.0, 0.0);

	Vector3D tright = right * ( this->width / 2.0f );
	Vector3D tup = up * ( this->height / 2.0f );
	Vector3D tdiagp = - ( tright + tup );
	Vector3D tdiagm = tright - tup;
	//Vector3D o = tright * 0 + tup * 1.0f;
	//Vector3D o(0.0f, 0.5f * this->size, 0.0f);

	Vector3D out = - right ^ up;
	out.scale(off_z);

	for(int i=0,v=0;i<n_billboards;i++) {
		Vector3D this_tdiagm = tdiagm;
		Vector3D this_tdiagp = tdiagp;
		//if( sizes != NULL ) {
		//	this_tdiagp *= sizes[i];
		//	this_tdiagm *= sizes[i];
		//}
		Vector3D offset = offsets[i];

		va_vertices[v++] = offset.x-tdiagp.x+out.x;
		va_vertices[v++] = offset.y-tdiagp.y+out.y;
		va_vertices[v++] = offset.z-tdiagp.z+out.z;

		va_vertices[v++] = offset.x-tdiagm.x+out.x;
		va_vertices[v++] = offset.y-tdiagm.y+out.y;
		va_vertices[v++] = offset.z-tdiagm.z+out.z;

		va_vertices[v++] = offset.x+tdiagp.x;
		va_vertices[v++] = offset.y+tdiagp.y;
		va_vertices[v++] = offset.z+tdiagp.z;

		va_vertices[v++] = offset.x+tdiagm.x;
		va_vertices[v++] = offset.y+tdiagm.y;
		va_vertices[v++] = offset.z+tdiagm.z;
	}

	//renderer->startArrays(Renderer::VERTEXFORMAT_PNT0C, false, true, false); // original
	renderer->startArrays(Renderer::VERTEXFORMAT_PNT0C); // original

	vao_vertices->renderVertices(true, 0);

	vao_normals->renderNormals(false, 0);
	vao_texcoords->renderTexCoords(false, 0);
	vao_colors->renderColors(false, 0);
	//g->getRenderer()->setColor4(255, 0, 0, 255);

	if( renderer->getActiveShader() != NULL ) {
		genv->getGraphics3D()->setShaderConstants(this);
	}
	else {
		genv->getGraphics3D()->setFixedFunctionConstants(this);
	}
	if( GraphicsEnvironment::getSingleton()->getRenderer()->supportsQuads() ) {
		vao_indices->renderElements(VertexArray::DRAWINGMODE_QUADS, 4*n_billboards, 4*n_billboards);
		/*genv->stats_poly_count += n_billboards;
		genv->stats_vertex_count += 4*n_billboards;
		genv->stats_distinct_vertex_count += 4*n_billboards;
		genv->stats_render_calls_count++;*/
		genv->updateStats(n_billboards, 4*n_billboards, 4*n_billboards, 1);
	}
	else {
		vao_indices->renderElements(VertexArray::DRAWINGMODE_TRIANGLES, 6*n_billboards, 4*n_billboards);
		/*genv->stats_poly_count += 2*n_billboards;
		genv->stats_vertex_count += 6*n_billboards;
		genv->stats_distinct_vertex_count += 4*n_billboards;
		genv->stats_render_calls_count++;*/
		genv->updateStats(2*n_billboards, 6*n_billboards, 6*n_billboards, 1);
	}

	renderer->endArrays();

	// Testing shows intermediate to be faster than even VBOs!
	//g->getRenderer()->renderBillboards(n_billboards, offsets, tdiagm, tdiagp, out, cols, false, NULL, normal); // original

	//g->getRenderer()->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, tex_coord_data, 4*n_billboards, cols, true, 4);
			/*float texcoord_data[] = {
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f
			};
			float vertex_data[] = {
				-tdiagp.x, -tdiagp.y, -tdiagp.z,
				-tdiagm.x, -tdiagm.y, -tdiagm.z,
				tdiagp.x, tdiagp.y, tdiagp.z,
				tdiagm.x, tdiagm.y, tdiagm.z
			};
			g->getRenderer()->render(VertexArray::DRAWINGMODE_QUADS, vertex_data, texcoord_data, 4);*/

	if( this->blend ) {
		//renderer->enableLighting(true);
	}
	else {
		renderer->setAlphaTestMode(Renderer::RENDERSTATE_ALPHA_TEST_OFF);
	}
	renderer->enableCullFace(true);
}

void BillBoard::renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview) {
	if( shadowplane == NULL || shadowplane->isRenderToTexture() ) {
		if( node->getPoint()->isInfinite() ) {
			//g->c_queue->renderQueue_infinite->add(entity);
			queue->renderQueue_infinite->push_back(node);
		}
		else {
			if( !outofview )
			{
				if( this->blend ) {
					//g->c_queue->renderQueue_nonopaque->add(entity);
					//g->c_queue->renderQueue_nonopaque->push_back(entity);
					queue->addNonOpaque(node);
				}
				else {
					//g->c_queue->renderQueue->add(entity);
					//g->c_queue->renderQueue->push_back(entity);
					queue->addOpaque(node);
				}
			}
			// no shadow casting
		}
	}
}


ParticleSystem::ParticleSystem(int max_particles) : ObjectData() {
	this->texture = NULL;
	this->max_particles = max_particles;
	this->lock = false;
	/*this->n_particles = 0;
	this->particles = new Particle[max_particles];*/
	this->accumulated_time_ms = 0;

	int n_vertices = max_particles*4;
	this->va_vertices = new float[3*n_vertices];
	for(int i=0;i<3*n_vertices;i++) {
		this->va_vertices[i] = 0.0f;
	}
	this->vao_vertices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_VERTICES, 0, true, va_vertices, 3*n_vertices*sizeof(float), 3);

	this->va_texcoords = new float[2*n_vertices];
	for(int i=0,c=0;i<max_particles;i++) {
		this->va_texcoords[c++] = 0.0f;
		this->va_texcoords[c++] = 1.0f;

		this->va_texcoords[c++] = 1.0f;
		this->va_texcoords[c++] = 1.0f;

		this->va_texcoords[c++] = 1.0f;
		this->va_texcoords[c++] = 0.0f;

		this->va_texcoords[c++] = 0.0f;
		this->va_texcoords[c++] = 0.0f;
	}
	this->vao_texcoords = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_TEXCOORDS, 0, false, va_texcoords, 2*n_vertices*sizeof(float), 2);

	this->va_colors = NULL;
	this->vao_colors = NULL;
	this->va_colors = new unsigned char[4*n_vertices];
	for(int i=0;i<4*n_vertices;i++) {
		this->va_colors[i] = 255;
	}
	this->vao_colors = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_COLORS, 0, true, va_colors, 4*n_vertices*sizeof(unsigned char), 4);

	int n_renderlength = 0;
	if( GraphicsEnvironment::getSingleton()->getRenderer()->supportsQuads() ) {
		// OpenGL is faster if we use Quads rather than triangles
		this->va_indices = NULL;
	}
	else {
		n_renderlength = max_particles*6;
		this->va_indices = new unsigned short[n_renderlength];
		for(int i=0,index=0,v=0;i<max_particles;i++) {
			va_indices[index++] = v;
			va_indices[index++] = v+1;
			va_indices[index++] = v+2;
			va_indices[index++] = v;
			va_indices[index++] = v+2;
			va_indices[index++] = v+3;
			v += 4;
		}
	}
	this->vao_indices = GraphicsEnvironment::getSingleton()->getRenderer()->createVertexArray(VertexArray::DATATYPE_INDICES, 0, false, va_indices, n_renderlength*sizeof(unsigned short), 2);
}

ParticleSystem::~ParticleSystem() {
	//delete [] this->particles;
	delete vao_vertices;
	delete vao_texcoords;
	delete vao_colors;
	delete vao_indices;
	delete [] va_vertices;
	delete [] va_texcoords;
	delete [] va_colors;
	delete [] va_indices;
}

void ParticleSystem::initSceneGraphNode(SceneGraphNode *node) {
	node->n_particles = 0;
	node->particles = new Particle[max_particles];
	//node->emit_new_particles = true;
}

void ParticleSystem::renderQueue(RenderQueue *queue, SceneGraphNode *node, bool outofview) {
	//return;
	if( shadowplane == NULL || shadowplane->isRenderToTexture() )
	{
		if( !outofview ) {
			if( node->getPoint()->isInfinite() ) {
				//g->c_queue->renderQueue_infinite->add(entity);
				queue->renderQueue_infinite->push_back(node);
			}
			else {
				//g->c_queue->renderQueue_nonopaque->add(entity);
				//g->c_queue->renderQueue_nonopaque->push_back(entity);
				queue->addNonOpaque(node);
			}
		}
	}
	// TODO: there may be lighting?
}

void ParticleSystem::render(Renderer *renderer, GraphicsEnvironment *genv, RenderPhase phase, int frame, int frame2, float frac, SceneGraphNode *node, bool lighting_pass, bool use_shadowmesh, RenderData *renderData) {
	if( node->n_particles == 0 ) {
		return;
	}
	if( phase != RENDER_NONOPAQUE ) {
		return;
	}
	/*if( lighting_pass ) {
		return;
	}*/
	//LOG("ParticleSystem::render()\n");
	/*const Material *material = entity->getMaterial();
	if( material->isColorForced() ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "Can't override material with ParticleSystem types"));
	}*/

	Vector3D right, up;
	if( lock ) {
		Vector3D dir;
		renderer->getViewDirection(&dir);
		up.set(0.0, 1.0, 0.0);
		right = dir ^ up;
		if( right.magnitude() <= V_TOL_LINEAR ) {
			right.set(1.0, 0.0, 0.0); // set to arbitrary value
			//return;
		}
		right.normalise();
	}
	else {
		renderer->getViewOrientation(&right, &up);
	}

	//renderer->disableFog(); // TODO: fix
	renderer->setFogMode(Renderer::RENDERSTATE_FOG_NONE, 0.0f, 0.0f, 0.0f); // TODO: fix
	//renderer->enableLighting(false);
	renderer->enableCullFace(false);

	//renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
	renderer->setBlendMode(Renderer::RENDERSTATE_BLEND_ADDITIVE);

	Particle *particles = node->particles;
	//int n_particles = entity->n_particles;

	renderer->setFixedFunctionMatSpecularOff();
	if( texture == NULL ) {
		renderer->enableTexturing(false);
	}
	else {
		texture->enable();
	}

//#define PS_RENDER_OLD

#ifdef PS_RENDER_OLD
	renderer->renderBegin(VertexArray::DRAWINGMODE_QUADS);
	for(int i=0;i<entity->n_particles;i++) {
		Color *col = &particles[i].color;
		//renderer->setColor4(col->getR(),col->getG(),col->getB(),particles[i].alpha);
		Vector3D *pos = &particles[i].pos;
		float size = particles[i].size;
		Vector3D tright = - ( right * size ) / 2.0;
		Vector3D tup = - ( up * size ) / 2.0;
		Vector3D tdiag = tright + tup;
		Vector3D tdiagl = tright - tup;

		float texcoord_data[] = {
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0
		};
		float vertex_data[] = {
			pos->x - tdiag.x,pos->y - tdiag.y,pos->z - tdiag.z,
			pos->x + tdiagl.x,pos->y + tdiagl.y,pos->z + tdiagl.z,
			pos->x + tdiag.x,pos->y + tdiag.y,pos->z + tdiag.z,
			pos->x - tdiagl.x,pos->y - tdiagl.y,pos->z - tdiagl.z
		};
		unsigned char color_data[] = { col->getR(),col->getG(),col->getB(),particles[i].alpha };
		renderer->renderIntermediate(vertex_data, texcoord_data, 4, color_data, true, 4);
	}
	renderer->renderEnd();

#else

	for(int i=0,v=0;i<node->n_particles;i++) {
		Vector3D *pos = &particles[i].pos;
		float size = particles[i].size;
		Vector3D tright = - ( right * size ) / 2.0;
		Vector3D tup = - ( up * size ) / 2.0;
		Vector3D tdiag = tright + tup;
		Vector3D tdiagl = tright - tup;

		va_vertices[v++] = pos->x - tdiag.x;
		va_vertices[v++] = pos->y - tdiag.y;
		va_vertices[v++] = pos->z - tdiag.z;

		va_vertices[v++] = pos->x + tdiagl.x;
		va_vertices[v++] = pos->y + tdiagl.y;
		va_vertices[v++] = pos->z + tdiagl.z;

		va_vertices[v++] = pos->x + tdiag.x;
		va_vertices[v++] = pos->y + tdiag.y;
		va_vertices[v++] = pos->z + tdiag.z;

		va_vertices[v++] = pos->x - tdiagl.x;
		va_vertices[v++] = pos->y - tdiagl.y;
		va_vertices[v++] = pos->z - tdiagl.z;
	}

	// TODO: only update if colors/alphas have changed?
	for(int i=0,v=0;i<node->n_particles;i++) {
		const Color *col = &particles[i].color;
		unsigned char alpha = particles[i].alpha;
		for(int k=0;k<4;k++) {
			va_colors[v++] = col->getR();
			va_colors[v++] = col->getG();
			va_colors[v++] = col->getB();
			va_colors[v++] = alpha;
		}
	}

	// test
	/*for(int i=0,c=0;i<entity->n_particles;i++) {
		this->va_texcoords[c++] = 0.0f;
		this->va_texcoords[c++] = 1.0f;

		this->va_texcoords[c++] = 1.0f;
		this->va_texcoords[c++] = 1.0f;

		this->va_texcoords[c++] = 1.0f;
		this->va_texcoords[c++] = 0.0f;

		this->va_texcoords[c++] = 0.0f;
		this->va_texcoords[c++] = 0.0f;
	}*/
	// end test

	//renderer->startArrays(Renderer::VERTEXFORMAT_PT0C, false, true, false);
	renderer->startArrays(Renderer::VERTEXFORMAT_PT0C);

	vao_vertices->renderVertices(true, 0);

	vao_texcoords->renderTexCoords(false, 0);
	vao_colors->renderColors(true, 0);

	if( renderer->getActiveShader() != NULL ) {
		genv->getGraphics3D()->setShaderConstants(this);
	}
	else {
		genv->getGraphics3D()->setFixedFunctionConstants(this);
	}
	if( GraphicsEnvironment::getSingleton()->getRenderer()->supportsQuads() ) {
		vao_indices->renderElements(VertexArray::DRAWINGMODE_QUADS, 4*node->n_particles, 4*node->n_particles);
		/*genv->stats_poly_count += entity->n_particles;
		genv->stats_vertex_count += 4*entity->n_particles;
		genv->stats_distinct_vertex_count += 4*entity->n_particles;
		genv->stats_render_calls_count++;*/
		genv->updateStats(node->n_particles, 4*node->n_particles, 4*node->n_particles, 1);
	}
	else {
		vao_indices->renderElements(VertexArray::DRAWINGMODE_TRIANGLES, 6*node->n_particles, 4*node->n_particles);
		/*genv->stats_poly_count += 2*entity->n_particles;
		genv->stats_vertex_count += 6*entity->n_particles;
		genv->stats_distinct_vertex_count += 4*entity->n_particles;
		genv->stats_render_calls_count++;*/
		genv->updateStats(2*node->n_particles, 6*node->n_particles, 6*node->n_particles, 1);
	}
	//renderer->endArrays(this->vao_vertices->hasVBO());
	renderer->endArrays();

#endif

	if( texture == NULL ) {
		renderer->enableTexturing(true);
	}
	//renderer->enableLighting(true);
	renderer->enableCullFace(true);
}

void ParticleSystem::setTexture(VI_Texture *texture) {
	Texture *d_texture = static_cast<Texture *>(texture); // cast to the internal representation
	this->texture = d_texture;
}


Snowstorm::Snowstorm(int max_particles,Texture *texture,float width,float height,float depth) : ParticleSystem(max_particles) {
	this->texture = texture;
	this->width = width;
	this->height = height;
	this->depth = depth;
	this->size = 0.3f;
	this->rate = 1.0f;
	this->color.setf(0.3f,0.3f,0.3f);
	this->velocity.set(0.0f,-10.0f,0.0f);
	this->variation.set(0.4f,0.0f,0.4f);
	this->lock = true; // lock the billboard alignment
}

/*bool Snowstorm::update(float time) {
ParticleSystem::update(time);

for(int i=0;i<n_particles;) {
if(particles[i].pos.y <= 0) {
particles[i] = particles[--n_particles];
}
else
i++;
}

this->accumulated_time += time;
int new_particles = rate * this->accumulated_time;
this->accumulated_time -= 1.0/(float)rate * new_particles;

if(new_particles > 0)
this->emit(new_particles);


//AbstractObject::update(world,time);
return false;
}*/

void Snowstorm::initialiseParticle(Particle *particles,int index) {
	particles[index].reset();

	//particles[index].pos.y = point.pos.y + height;
	particles[index].pos.y = height;
	float rx = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	float rz = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	rx = rx - 0.5f;
	rz = rz - 0.5f;
	//particles[index].pos.x = point.pos.x + rx * width;
	//particles[index].pos.z = point.pos.z + rz * depth;
	particles[index].pos.x = rx * width;
	particles[index].pos.z = rz * depth;

	particles[index].size = size;

	float rvx = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	float rvy = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	float rvz = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	rvx = 2.0f * rvx - 1.0f;
	rvy = 2.0f * rvy - 1.0f;
	rvz = 2.0f * rvz - 1.0f;
	rvx = rvy = rvz = 0.0f;
	particles[index].velocity.x = velocity.x + rvx * variation.x;
	particles[index].velocity.y = velocity.y + rvy * variation.z;
	particles[index].velocity.z = velocity.z + rvz * variation.y;

	particles[index].color = this->color;
	particles[index].size = this->size;
}

void Snowstorm::updateParticles(SceneGraphNode *node,int time_ms) {
	const Particle *particles = node->getParticles();

	for(int i=0;i<node->getNParticles();) {
		if(particles[i].pos.y <= 0.0) {
			//particles[i] = particles[--entity->n_particles];
			node->killParticle(i);
		}
		else
			i++;
	}

	this->accumulated_time_ms += time_ms;
	int new_particles = (int)(rate * this->accumulated_time_ms);
	this->accumulated_time_ms -= 1.0f/(float)rate * new_particles;

	if(new_particles > 0)
		node->emitParticle(new_particles);
}

const float nwn_size_fac_c = 1.0f;

NWNParticleSystem::NWNParticleSystem(int max_particles) : ParticleSystem(max_particles) {
	this->color_st.seti(255,255,255);
	this->color_nd.seti(255,255,255);
	this->alpha_st = 255;
	this->alpha_nd = 255;
	this->size_st = 1.0f;
	this->size_nd = 1.0f;
	this->birthRate = 0;
	this->lifeExp = 1;
	this->mass = 0;
	this->initVel = 0.0f;
	//this->expired = false;
	//this->color_nd.seti(0,0,0);
};

NWNParticleSystem::NWNParticleSystem(int max_particles,Texture *texture,Color color_st,Color color_nd,unsigned char alpha_st,unsigned char alpha_nd,float size_st,float size_nd,int birthRate,float lifeExp,float mass,float initVel) : ParticleSystem(max_particles) {
	this->texture = texture;
	this->color_st = color_st;
	this->color_nd = color_nd;
	this->alpha_st = alpha_st;
	this->alpha_nd = alpha_nd;
	this->size_st = size_st;
	this->size_nd = size_nd;
	this->birthRate = birthRate;
	this->lifeExp = lifeExp;
	this->mass = mass;
	this->initVel = initVel;
	//this->expired = false;
}

void NWNParticleSystem::updateParticles(SceneGraphNode *node,int time_ms) {
	Particle *particles = node->getParticles();

	int timeNow_ms = Vision::getGameTimeMS();
	for(int i=0;i<node->getNParticles();) {
		if(timeNow_ms > particles[i].birthTime_ms + this->lifeExp*1000) {
			//particles[i] = particles[--entity->n_particles];
			node->killParticle(i);
		}
		else
			i++;
	}

	int sr = this->color_st.getR();
	int sg = this->color_st.getG();
	int sb = this->color_st.getB();
	int er = this->color_nd.getR();
	int eg = this->color_nd.getG();
	int eb = this->color_nd.getB();
	for(int i=0;i<node->getNParticles();i++) {
		float f = ((float)(timeNow_ms - particles[i].birthTime_ms))/((float)(this->lifeExp*1000.0f));
		int r = (int)((1.0f - f) * sr + f * er);
		int g = (int)((1.0f - f) * sg + f * eg);
		int b = (int)((1.0f - f) * sb + f * eb);
		//LOG("%d : %d, %d, %d\n", i, r, g, b);
		particles[i].color.seti(r, g, b);
		particles[i].size = nwn_size_fac_c * ( (1.0f - f) * this->size_st + f * this->size_nd );
		particles[i].alpha = (unsigned char)((1.0f - f) * this->alpha_st + f * this->alpha_nd);
	}

	if( node->emitsNewParticles() ) {
		float rate = ((float)this->birthRate)/1000.0f;
		this->accumulated_time_ms += time_ms;
		//LOG("rate = %f , %d\n", birthRate, rate);
		//LOG("time: %f , %d\n", accumulated_time_ms, time_ms);
		int new_particles = (int)(rate * this->accumulated_time_ms);
		this->accumulated_time_ms -= 1.0f/(float)rate * new_particles;

		if(new_particles > 0) {
		    //LOG("%d new particles\n", new_particles);
			node->emitParticle(new_particles);
		}
	}
}

void NWNParticleSystem::initialiseParticle(Particle *particles,int index) {
	particles[index].reset();

	Particle *particle = &particles[index];
	particle->pos.set(0,0,0);
	particle->size = nwn_size_fac_c * this->size_st;
	particle->color = this->color_st;
	particle->alpha = this->alpha_st;
	float vx = ((float)(rand() % 257) - 128.0f)/128.0f;
	float vy = ((float)(rand() % 257) - 128.0f)/128.0f;
	float vz = ((float)(rand() % 257) - 128.0f)/128.0f;
	particle->velocity.set(vx, vy, vz);
	if( particle->velocity.magnitude() == 0 )
		particle->velocity.set(0, 1, 0);
	else
		particle->velocity.normalise();
	particle->velocity *= 2.0f * initVel;
	//particle->velocity *= 0.0;
	particle->acceleration.set(0.0, -9.0f * this->mass, 0.0);

	/*particles[index].pos.y = height;
	float rx = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	float rz = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	rx = rx - 0.5;
	rz = rz - 0.5;
	//particles[index].pos.x = point.pos.x + rx * width;
	//particles[index].pos.z = point.pos.z + rz * depth;
	particles[index].pos.x = rx * width;
	particles[index].pos.z = rz * depth;

	particles[index].size = size;

	float rvx = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	float rvy = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	float rvz = ((float)(rand() % RAND_MAX)) / (float)RAND_MAX;
	rvx = 2.0 * rvx - 1.0;
	rvy = 2.0 * rvy - 1.0;
	rvz = 2.0 * rvz - 1.0;
	particles[index].velocity.x = velocity.x + rvx * variation.x;
	particles[index].velocity.y = velocity.y + rvy * variation.z;
	particles[index].velocity.z = velocity.z + rvz * variation.y;*/
}

void NWNParticleSystem::setColoriStart(unsigned char r, unsigned char g, unsigned char b) {
	this->color_st.seti(r, g, b);
}

void NWNParticleSystem::setColoriEnd(unsigned char r, unsigned char g, unsigned char b) {
	this->color_nd.seti(r, g, b);
}

void NWNParticleSystem::setSizeStart(float size) {
	this->size_st = size;
}

void NWNParticleSystem::setSizeEnd(float size) {
	this->size_nd = size;
}

void NWNParticleSystem::setBirthRate(int value) {
	this->birthRate = value;
}

void NWNParticleSystem::setLife(float value) {
	this->lifeExp = value;
}

void NWNParticleSystem::setMass(float value) {
	this->mass = value;
}

void NWNParticleSystem::setInitialVelocity(float value) {
	this->initVel = value;
}


/*ShadowPlane::ShadowPlane() {
	this->shadow = true;
	this->reflection = false;
	//this->render_to_texture = true;
	this->render_to_texture = false;
	this->render_texture = NULL;
	//this->fbo = 0;
	//this->fbo = NULL;
	//this->light = NULL;
}*/

ShadowPlane::ShadowPlane(const Plane *plane, bool shadow, bool reflection, bool render_to_texture) {
	this->plane = *plane;
	this->shadow = shadow;
	this->reflection = reflection;
	this->render_to_texture = render_to_texture;
	this->render_texture = NULL;
	this->next_update = 0;
	this->frames_per_update = 1;
}

ShadowPlane::~ShadowPlane() {
	/*if( this->fbo != NULL ) {
		delete this->fbo;
	}*/
}

void ShadowPlane::setRenderTexture(Texture *render_texture) {
	this->render_texture = render_texture;
	this->render_texture->setWrapMode(VI_Texture::WRAPMODE_MIRROR); // needed so that reflection distortion (waves) works
}
