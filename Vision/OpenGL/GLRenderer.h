#pragma once

#ifdef _WIN32
#define WIN32_MEAN_AND_LEAN
#include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>

#ifdef _WIN32
#include "../glext.h"
#else
#include <GL/glext.h>
#endif

#include "../Renderer.h"
#include "../Cg/CGShader.h"

class GLRenderer;

extern PFNGLGENOCCLUSIONQUERIESNVPROC    glGenOcclusionQueriesNV;
extern PFNGLDELETEOCCLUSIONQUERIESNVPROC glDeleteOcclusionQueriesNV;
extern PFNGLGETOCCLUSIONQUERYUIVNVPROC   glGetOcclusionQueryuivNV;
extern PFNGLBEGINOCCLUSIONQUERYNVPROC    glBeginOcclusionQueryNV;
extern PFNGLENDOCCLUSIONQUERYNVPROC      glEndOcclusionQueryNV;

extern PFNGLLOCKARRAYSEXTPROC glLockArraysEXT;
extern PFNGLUNLOCKARRAYSEXTPROC glUnlockArraysEXT;

extern PFNGLSTENCILOPSEPARATEATIPROC glStencilOpSeparateATI;
extern PFNGLSTENCILFUNCSEPARATEATIPROC glStencilFuncSeparateATI;

extern PFNGLACTIVESTENCILFACEEXTPROC glActiveStencilFaceEXT;

extern PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT;
extern PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT;
extern PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT;
extern PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT;
extern PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebufferEXT;
extern PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT;
extern PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT;
extern PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT;
extern PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT;
extern PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT;

#ifdef _WIN32
// already defined in Linux?
extern PFNGLMULTITEXCOORD2FARBPROC glMultiTexCoord2fARB;
extern PFNGLACTIVETEXTUREARBPROC glActiveTextureARB;
extern PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTextureARB;
#endif

extern PFNGLGENBUFFERSARBPROC glGenBuffersARB;
extern PFNGLBINDBUFFERARBPROC glBindBufferARB;
extern PFNGLBUFFERDATAARBPROC glBufferDataARB;
extern PFNGLBUFFERSUBDATAARBPROC glBufferSubDataARB;
extern PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB;

extern PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElementsEXT;

class GLRendererInfo {
public:
	static bool pixelFormat;
	static bool compiledVAs;
	static bool vertexBufferObjects;
	static bool drawRangeElements;
	static bool separateStencilATI;
	static bool separateStencil;
	static int versionMajor;
	static int versionMinor;

	static void init() {
		pixelFormat = false;
		compiledVAs = false;
		vertexBufferObjects = false;
		drawRangeElements = false;
		separateStencilATI = false;
		separateStencil = false;
		versionMajor = 0;
		versionMinor = 0;
	}
	static bool hasVersion(int major, int minor) {
		if( versionMajor > major )
			return true;
		else if( versionMajor == major && versionMinor >= minor )
			return true;
		return false;
	}
};

class GLVertexArray : public VertexArray {
	unsigned int vbo;

	virtual char *render(bool new_data, int offset) const;
public:
	GLVertexArray(DataType data_type, int tex_unit, bool stream, void *data, int size, int dim);
	virtual ~GLVertexArray();

	virtual bool hasVBO() const {
		return vbo != 0;
	}
	/*void updateStream() {
		assert( stream );
		if( vbo != 0 ) {
			glBufferSubDataARB( GL_ARRAY_BUFFER_ARB, 0, size, data );
		}
	}*/
	virtual void renderVertices(bool new_data, int offset) const;
	virtual void renderNormals(bool new_data, int offset) const;
	virtual void renderTexCoords(bool new_data, int offset) const;
	virtual void renderTexCoords(bool new_data, int offset, int unit) const;
	virtual void renderColors(bool new_data, int offset) const;
	virtual void update() const;
	virtual void updateIndices() const;
	virtual void renderElements(DrawingMode mode, int n_this_renderlength, int n_this_vertices) const;
};

class CGGLShader : public CGShader {
	GLRenderer *renderer;

	//CGparameter cgparam_clipping_plane;

	// Textures
	//CGparameter cgparam_Texture;
	//CGparameter cgparam_TextureID;
	//CGparameter cgparam_SecondaryTexture;
	//CGparameter cgparam_SecondaryTextureID;

	// Matrices
	/*CGparameter cgparam_ModelViewProj;
	CGparameter cgparam_ModelViewIT;
	CGparameter cgparam_ModelView;*/

protected:
	virtual void enable() const;
	//virtual void disable() const;

public:
	CGGLShader(GLRenderer *renderer,bool vertex_shader);

	//void init();

	virtual void setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const;
	virtual void SetModelViewProjMatrix(const char *parameter) const;

	/*virtual void SetUniformParameter1f(const char *pszParameter, float p1) const;
	virtual void SetUniformParameter2f(const char *pszParameter, float p1, float p2) const;
	virtual void SetUniformParameter3f(const char *pszParameter, float p1, float p2, float p3) const;
	virtual void SetUniformParameter4f(const char *pszParameter, float p1, float p2, float p3, float p4) const;
	virtual void SetUniformParameter2fv(const char *pszParameter, const float v[2]) const;
	virtual void SetUniformParameter3fv(const char *pszParameter, const float v[3]) const;
	virtual void SetUniformParameter4fv(const char *pszParameter, const float v[4]) const;*/
};

class CGFXGLShaderEffect : public CGFXShaderEffect {
	GLRenderer *renderer;
public:
	CGFXGLShaderEffect(GLRenderer *renderer, CGcontext cg_context, const char *file, const char **args);

	virtual void setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const;
	virtual void SetModelViewProjMatrix(const char *parameter) const;
};


class GLTransformationMatrix : public TransformationMatrix {
	GLfloat mat[16];
public:
	virtual void load() const {
		glLoadMatrixf(mat);
	}
	virtual void save() {
		// TODO: other types of matrix?
		glGetFloatv(GL_MODELVIEW_MATRIX, mat);
	}
	virtual void mult() const {
		glMultMatrixf(mat);
	}

	virtual void set(int indx, float value) {
		this->mat[indx] = value;
	}
	virtual float &operator[](const int i) {
		return mat[i];
	}
	/*virtual const float &operator[](const int i) const {
		return mat[i];
	}*/
	virtual float get(int indx) const {
		return this->mat[indx];
	}
	virtual float get(int row, int col) const {
		int indx = 4*row + col;
		return this->mat[indx];
	}
	virtual Vector3D getPos() const {
		Vector3D pos(this->mat[12],this->mat[13],this->mat[14]);
		return pos;
	}
	virtual Vector3D transformVec(const Vector3D &in) const {
		/*D3DXVECTOR3 din(in->x, in->y, in->z);
		D3DXVECTOR4 dout;
		D3DXVec3Transform(dout, &din, &mat);
		Vector3D out(dout.x, dout.y, dout.z);
		return out;*/
		Vector3D out;
		out.x = this->mat[0] * in.x + this->mat[4] * in.y + this->mat[8] * in.z + this->mat[12];
		out.y = this->mat[1] * in.x + this->mat[5] * in.y + this->mat[9] * in.z + this->mat[13];
		out.z = this->mat[2] * in.x + this->mat[6] * in.y + this->mat[10] * in.z + this->mat[14];
		return out;
	}
};

/* The main texture data is stored via a handle/body idiom, so we can have
 * multiple GLTextures (each with their own Vision tags), but easily make sure
 * they have a copied identical GLTextureBody.
 */

class GLTextureBody {
public:
	string filename;
	bool mipmap, alpha, floating_point;
	int w,h;
	GLuint textureID;

	GLTextureBody() : mipmap(true), alpha(false), floating_point(false), w(0), h(0), textureID(0) {
	}
};

class GLTexture : public Texture {
protected:
	/*string filename;
	bool mipmap;
	int w,h;
	GLuint textureID;*/
	GLRenderer *renderer;
	GLTextureBody body;

	void load(const char *filename,bool mask, unsigned char maskcol[3]);
	void init(VI_Image *image,bool mask, unsigned char maskcol[3]);
	static GLTexture *textureFromCache(const char *filename);

	GLTexture(GLRenderer *renderer);
public:

	GLTexture(GLRenderer *renderer,int w,int h,bool alpha,bool floating_point);
	GLTexture(GLRenderer *renderer,VI_Image *image,bool mask, unsigned char maskcol[3]);
	virtual ~GLTexture();

	virtual Texture *copy();
	virtual void copyTo(int width, int height);
	int getTextureID() const {
		return body.textureID;
	}
	const char *getFilename() const {
		return body.filename.c_str();
	}

	static Texture *loadTexture(GLRenderer *renderer,const char *filename);
	static Texture *loadTextureWithMask(GLRenderer *renderer,const char *filename, unsigned char maskcol[3]);

	// interface
	virtual int getWidth() const {
		return body.w;
	}
	virtual int getHeight() const {
		return body.h;
	}
	virtual void draw(int x, int y);
	virtual void draw(int x, int y, int w, int h);
	virtual void enable();
	/*virtual void clampToEdge();
	virtual void clampMirror();*/
};

class GLFramebufferObject : public FramebufferObject {
	GLuint fbo;
	GLuint color_texture;
	GLuint depth_stencil_texture;
public:

	GLFramebufferObject() : FramebufferObject() {
		fbo = NULL;
		color_texture = NULL;
		depth_stencil_texture = NULL;
	}
	virtual ~GLFramebufferObject() {
		free();
	}

	virtual bool generate(int width,int height,bool want_depth_stencil);
	virtual void free();

	virtual void bind() const;
	virtual void enableTexture() const;
};

class GLQuadricObject : public QuadricObject {
	enum Type {
		TYPE_SPHERE = 0
	};
	Type type;

	// sphere data
	float radius;
	int slices;
	int stacks;

	GLUquadric *object;

	//GLQuadricObject(Type type) : QuadricObject(type) {
	GLQuadricObject(Type type) : QuadricObject(), type(type) {
		object = gluNewQuadric();
	}
public:
	virtual ~GLQuadricObject() {
		gluDeleteQuadric(object);
	}

	virtual void draw() const {
		if( type == TYPE_SPHERE ) {
			//SphereQuadricObjectData *ddata = static_cast<SphereQuadricObjectData *>(data);
			//gluSphere(object, ddata->radius, ddata->slices, ddata->stacks);
			gluSphere(object, radius, slices, stacks);
		}
	}

	static QuadricObject *createSphere(float radius, int slices, int stacks) {
		GLQuadricObject *qobj = new GLQuadricObject(TYPE_SPHERE);
		//qobj->data = new SphereQuadricObjectData(radius, slices, stacks);
		qobj->radius = radius;
		qobj->slices = slices;
		qobj->stacks = stacks;
		return qobj;
	}
};

#ifdef _WIN32
#define USE_WGL_FONTS
#endif

class GLFontBuffer : public FontBuffer {
	//HFONT                       g_hFont;        // font we render text in
	//GLYPHMETRICSFLOAT gmf[256];

#ifdef USE_WGL_FONTS
	unsigned int base;
#else
	Texture **textures;
	int descent;
	int *advance_x;
	int *offset_x;
	int *offset_y;
#endif
	int *width; // bitmap widths
	int *height; // bitmap heights
	int font_height;
	GLRenderer *renderer;

	int getIndex(char letter) const;
	void init(GLRenderer *renderer,const char *family,int pt,int style);
public:
	GLFontBuffer(GLRenderer *renderer,const char *family,int pt);
	GLFontBuffer(GLRenderer *renderer,const char *family,int pt,int style);
	virtual ~GLFontBuffer();

	virtual size_t memUsage();

	virtual int writeText(int x,int y,const char *text) const;
	virtual int writeText(int x,int y,const char *text,size_t length) const;
	virtual int getWidth(char letter) const;
	virtual int getWidth(const char *text) const {
		int w = 0;
		while(*text != NULL)
			w += getWidth(*text++);
		return w;
	}
	virtual int getHeight() const {
		/*if( height == NULL )
			return 1;
		return height[getIndex('I')];*/
		return font_height;
	}
};

class GLGraphics2D : public Graphics2D {
public:
	GLGraphics2D(GraphicsEnvironment *genv);

	//void setColor(Color *color);
	virtual void setColor3i(unsigned char r, unsigned char g, unsigned char b);
	virtual void setColor4i(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	//virtual void setColor(float r,float g,float b) const;
	virtual void plot(short x,short y);
	virtual void draw(short x,short y);
	virtual void draw(short x1,short y1,short x2,short y2) const;
	virtual void drawRect(short x,short y,short width,short height) const;
	virtual void fillRect(short x,short y,short width,short height,const float tu[4], const float tv[4]) const;
	virtual void fillTriangle(const short *x,const short *y) const;
};

class GLRenderer : public Renderer {
	friend class CGGLShader;
protected:
	//GLFramebufferObject hdr_fbo;
	bool shader_clipping;
	float shader_clipping_plane[4];
	int saved_height; // stored for glScissor!

	bool enabled_normals;
	bool enabled_colors;
	bool enabled_texcoords0;
	bool enabled_texcoords1;
	bool enabled_texcoords2;

	void extractFrustum(float frustum[6][4]) const;
	//void freeHDRBuffers();

	CGcontext cgContext;
	CGprofile cgVertexProfile;
	CGprofile cgPixelProfile;

	void initCG();
	static void cgErrorCallback(void);

public:

	//GLRenderer(Graphics3D *g3);
	GLRenderer(GraphicsEnvironment *genv);
	virtual ~GLRenderer();

	// factories
	virtual TransformationMatrix *createTransformationMatrix();
	virtual Texture *createTexture(int w,int h,bool alpha,bool floating_point) {
		return new GLTexture(this, w, h, alpha, floating_point);
	}
	virtual Texture *createTexture(VI_Image *image) {
		return new GLTexture(this, image, false, (unsigned char *)NULL);
	}
	virtual Texture *createTextureWithMask(VI_Image *image, unsigned char maskcol[3]) {
		return new GLTexture(this, image, true, maskcol);
	}
	virtual Texture *loadTexture(const char *filename);
	virtual Texture *loadTextureWithMask(const char *filename, unsigned char maskcol[3]);
	virtual FramebufferObject *createFramebufferObject() {
		return new GLFramebufferObject();
	}
	/*virtual QuadricObject *createQuadricObject() const {
		return new GLQuadricObject();
	}*/
	virtual QuadricObject *createSphereObject(float radius, int slices, int stacks) {
		return GLQuadricObject::createSphere(radius, slices, stacks);
	}
	virtual VertexArray *createVertexArray(VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim);
	virtual FontBuffer *createFont(const char *family,int pt) {
		return new GLFontBuffer(this, family, pt);
	}
	virtual FontBuffer *createFont(const char *family,int pt,int style) {
		return new GLFontBuffer(this, family, pt, style);
	}

	// general commands
	virtual void setViewport(int width, int height) {
		glViewport(0, 0, width, height);
	}
	virtual void resizeScene(float frustum[6][4], int width, int height, bool persp, float z_near, float z_far, float fovy);
	virtual void setClearColor(float r, float g, float b, float a) {
		glClearColor(r, g, b, a);
	}
	virtual void clear(bool color, bool depth, bool stencil) const {
		GLbitfield mask = 0;
		if( color )
			mask |= GL_COLOR_BUFFER_BIT;
		if( depth )
			mask |= GL_DEPTH_BUFFER_BIT;
		if( stencil )
			mask |= GL_STENCIL_BUFFER_BIT;
		glClear(mask);
	}

	// render states
	virtual void setToDefaults();

	virtual void setDepthBufferMode(RenderState state);
	virtual void setDepthBufferFunc(RenderState state);
	virtual void setBlendMode(RenderState state);
	virtual void setAlphaTestMode(RenderState state);
	virtual void setStencilMode(RenderState state);
	virtual void setFogMode(RenderState state, float start, float end, float density);
	virtual void setFogColor(const float color[4]);

	virtual int nShadowPasses() const;

	virtual void enableCullFace(bool enable) {
		enable ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
	}
	virtual void setFrontFace(bool ccw) {
		glFrontFace(ccw ? GL_CCW : GL_CW);
	}
	virtual void enableTexturing(bool enable) {
		enable ? glEnable(GL_TEXTURE_2D) : glDisable(GL_TEXTURE_2D);
		/*if( !enable ) {
			if( this->getActiveShader() != NULL ) {
				this->getActiveShader()->SetUniformParameter1f("textureID", 0.0f);
			}
		}*/
	}
	virtual void enableMultisample(bool enable);
	virtual void enableColorMask(bool enable) {
		GLboolean b = enable ? GL_TRUE : GL_FALSE;
		glColorMask(b, b, b, b);
	}
	virtual void enableClipPlane(const double pl[4]);
	virtual void reenableClipPlane();
	virtual void disableClipPlane() {
		glDisable(GL_CLIP_PLANE0);
		this->shader_clipping = false;
	}
	virtual void enableScissor(int x,int y,int w,int h) {
		// n.b., convert to OpenGL format!
		glScissor(x, this->saved_height - h - y, w, h);
		glEnable(GL_SCISSOR_TEST);
	}
	virtual void reenableScissor() {
		glEnable(GL_SCISSOR_TEST);
	}
	virtual void disableScissor() {
		glDisable(GL_SCISSOR_TEST);
	}
	/*virtual void disableFog() {
		glDisable(GL_FOG);
	}*/
	virtual void pushAttrib(bool viewport);
	virtual void popAttrib() const;

	// fixed function only functions
	virtual void setFixedFunctionMatSpecular(const float specular[4]);
	/*virtual void setFixedFunctionMatColor(const float color[4]) {
		glColor4fv(color);
	}*/
	virtual void enableFixedFunctionLighting(bool enable) {
		enable ? glEnable(GL_LIGHTING) : glDisable(GL_LIGHTING);
	}
	virtual void setFixedFunctionAmbientLighting(const float col[4]) {
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT,col);
	}
	virtual void enableFixedFunctionLight(unsigned int index, const float ambient[4], const float diffuse[4], const float specular[4], const float attenuation[3], const float position[3], bool directional);
	virtual void disableFixedFunctionLights();
	virtual void disableFixedFunctionLight(unsigned int index) {
		GLenum gl_light = GL_LIGHT0 + (GLenum)index;
		ASSERT( index >= 0 && index < RendererInfo::maxLights );
		glDisable(gl_light);
	}

	//virtual void enableVertexArrays(bool enable, bool vertices, bool normals, bool texcoords, bool colors) const;

	// drawing
	/*virtual void setColor3(unsigned char r, unsigned char g, unsigned char b) const {
		glColor3ub(r, g, b);
	}*/
	virtual void draw(const Vector3D *p0, const Vector3D *p1, const unsigned char rgba[4]);
	/*virtual void setNormal(float x, float y, float z) const {
		glNormal3f(x, y, z);
	}*/
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count);
	virtual void render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count, const unsigned char *color_data, bool alpha, int color_repeat);
	//virtual void renderBillboards(int count, Vector3D *offsets, Vector3D tdiagm, Vector3D tdiagp, Vector3D out, unsigned char *color_data, bool alpha, float *sizes, Vector3D normal) const;
	/*virtual void renderBegin(VertexArray::DrawingMode mode);
	virtual void renderIntermediate(float *vertex_data, float *texcoord_data, int count, unsigned char *color_data, bool alpha, int color_repeat) const;
	virtual void renderEnd() const;*/
	virtual bool supportsQuads() const {
		return true;
	}
	//virtual void drawPixels(int x, int y,int w, int h, void *data) const;

	//virtual void startArrays(VertexFormat vertexFormat, bool material_color, bool have_texcoords0, bool have_texcoords1);
	virtual void startArrays(VertexFormat vertexFormat);
	virtual void endArrays();
	//virtual void endArraysMulti(bool vbos);

	// texture unit handling
	virtual void activeTextureUnit(int unit);
	//virtual void activeClientTextureUnit(int unit) const;

	// transformation matrices
	virtual void switchToModelview() {
		glMatrixMode(GL_MODELVIEW);
	}
	virtual void switchToProjection() {
		glMatrixMode(GL_PROJECTION);
	}
	virtual void switchToTexture() {
		glMatrixMode(GL_TEXTURE);
	}
	virtual void ortho2D(int width, int height);
	virtual void pushMatrix() {
		glPushMatrix();
	}
	virtual void popMatrix() {
		glPopMatrix();
	}
	virtual void loadIdentity() {
		glLoadIdentity();
	}
	virtual void translate(float x,float y,float z) {
		glTranslatef(x, y, z);
	}
	virtual void rotate(float angle,float x,float y,float z) {
		glRotatef(angle, x, y, z);
	}
	virtual void scale(float x,float y,float z) {
		glScalef(x, y, z);
	}
	virtual void transform(const Vector3D *pos, const Quaternion *rot, bool infinite);
	virtual void transform(const Quaternion *rot);
	virtual void transformInverse(const Quaternion *rot);
	virtual void getViewOrientation(Vector3D *right,Vector3D *up);
	virtual void getViewDirection(Vector3D *dir);

	// frame buffers
	/*virtual bool generateHdrFbo(int width, int height) {
		//this->hdr_fbo.free();
		return this->hdr_fbo.generate(width, height, true);
	}
	virtual void bindHdrFbo() const {
		this->hdr_fbo.bind();
	}
	virtual void enableHdrFbo() const {
		this->hdr_fbo.enableTexture();
	}*/
	virtual void unbindFramebuffer() const;

	// shaders
	virtual bool hasShaders() const {
		return cgContext != NULL;
	}
	virtual Shader *createShaderCG(const char *file, const char *func, bool vertex_shader);
	virtual ShaderEffect *createShaderEffectCGFX(const char *file,const char **args);
	virtual void disableShaders();
};
