#pragma once

#ifdef _WIN32
#define WIN32_MEAN_AND_LEAN
#include <windows.h>
#endif
#include <GL/gl.h>

#ifdef _WIN32
#include "../glext.h"
#else
#include <GL/glext.h>
#endif

#include "../SDL_GraphicsEnvironment.h"

class Shader;
class Renderer;
class Graphics2D;
class Graphics3D;
class InputHandler;
class Movement;
class Panel;
class SceneGraphNode;
class Image2D;

/*extern PFNGLGENOCCLUSIONQUERIESNVPROC    glGenOcclusionQueriesNV;
extern PFNGLDELETEOCCLUSIONQUERIESNVPROC glDeleteOcclusionQueriesNV;
extern PFNGLGETOCCLUSIONQUERYUIVNVPROC   glGetOcclusionQueryuivNV;
extern PFNGLBEGINOCCLUSIONQUERYNVPROC    glBeginOcclusionQueryNV;
extern PFNGLENDOCCLUSIONQUERYNVPROC      glEndOcclusionQueryNV;

extern PFNGLLOCKARRAYSEXTPROC glLockArraysEXT;
extern PFNGLUNLOCKARRAYSEXTPROC glUnlockArraysEXT;

extern PFNGLSTENCILOPSEPARATEATIPROC glStencilOpSeparateATI;
extern PFNGLSTENCILFUNCSEPARATEATIPROC glStencilFuncSeparateATI;

extern PFNGLACTIVESTENCILFACEEXTPROC glActiveStencilFaceEXT;

extern PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT;
extern PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT;
extern PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT;
extern PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT;
extern PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebufferEXT;
extern PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT;
extern PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT;
extern PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT;
extern PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT;
extern PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT;

extern PFNGLMULTITEXCOORD2FARBPROC glMultiTexCoord2fARB;
extern PFNGLACTIVETEXTUREARBPROC glActiveTextureARB;
extern PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTextureARB;

extern PFNGLGENBUFFERSARBPROC glGenBuffersARB;
extern PFNGLBINDBUFFERARBPROC glBindBufferARB;
extern PFNGLBUFFERDATAARBPROC glBufferDataARB;
extern PFNGLBUFFERSUBDATAARBPROC glBufferSubDataARB;
extern PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB;

extern PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElementsEXT;*/

class SDL_GLGraphicsEnvironment : public SDL_GraphicsEnvironment {
	bool hasGLContext;

	Texture *dummy_texture;

	void free();

public:
	//GLGraphicsEnvironment(int width,int height,int multisample,bool fullscreen,bool hdr);
	SDL_GLGraphicsEnvironment(VisionPrefs visionPrefs);
	virtual ~SDL_GLGraphicsEnvironment();

	virtual bool hasContext() const {
		//return this->created;
		return this->hasGLContext;
	}
	//virtual Shader *createShader(const char *file, const char *func, bool vertex_shader);

	// interface
	virtual void swapBuffers();
	virtual bool saveScreen(const char *filename,const char *type);
	virtual void checkError(const char *label);
	//virtual bool hasShaders() const;
};
