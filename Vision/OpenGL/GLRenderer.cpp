//---------------------------------------------------------------------------
#include <cassert>

#include <set>
using std::set;

#include "GLRenderer.h"
#include "../GraphicsEnvironment.h"
#include "../Texture.h"
#include "../Misc.h"

#include <Cg/cgGL.h>

//---------------------------------------------------------------------------

PFNGLGENOCCLUSIONQUERIESNVPROC    glGenOcclusionQueriesNV    = NULL;
PFNGLDELETEOCCLUSIONQUERIESNVPROC glDeleteOcclusionQueriesNV = NULL;
PFNGLGETOCCLUSIONQUERYUIVNVPROC   glGetOcclusionQueryuivNV   = NULL;
PFNGLBEGINOCCLUSIONQUERYNVPROC    glBeginOcclusionQueryNV    = NULL;
PFNGLENDOCCLUSIONQUERYNVPROC      glEndOcclusionQueryNV      = NULL;

PFNGLLOCKARRAYSEXTPROC glLockArraysEXT = NULL;
PFNGLUNLOCKARRAYSEXTPROC glUnlockArraysEXT = NULL;

PFNGLSTENCILOPSEPARATEATIPROC glStencilOpSeparateATI = NULL;
PFNGLSTENCILFUNCSEPARATEATIPROC glStencilFuncSeparateATI = NULL;

PFNGLACTIVESTENCILFACEEXTPROC glActiveStencilFaceEXT = NULL;

PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT = NULL;
PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT = NULL;
PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT = NULL;
PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT = NULL;
PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebufferEXT = NULL;
PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT = NULL;
PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT = NULL;
PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT = NULL;
PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT = NULL;
PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT = NULL;

#ifdef _WIN32
// already defined in Linux?
PFNGLMULTITEXCOORD2FARBPROC glMultiTexCoord2fARB = NULL;
PFNGLACTIVETEXTUREARBPROC glActiveTextureARB = NULL;
PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTextureARB = NULL;
#endif

PFNGLGENBUFFERSARBPROC glGenBuffersARB = NULL;// VBO Name Generation Procedure
PFNGLBINDBUFFERARBPROC glBindBufferARB = NULL;// VBO Bind Procedure
PFNGLBUFFERDATAARBPROC glBufferDataARB = NULL;// VBO Data Loading Procedure
PFNGLBUFFERSUBDATAARBPROC glBufferSubDataARB = NULL;
PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB = NULL;// VBO Deletion Procedure

PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElementsEXT = NULL;

bool GLRendererInfo::pixelFormat = false;
bool GLRendererInfo::compiledVAs = false;
bool GLRendererInfo::vertexBufferObjects = false;
bool GLRendererInfo::drawRangeElements = false;
bool GLRendererInfo::separateStencilATI = false;
bool GLRendererInfo::separateStencil = false;
int GLRendererInfo::versionMinor = false;
int GLRendererInfo::versionMajor = false;

static GLenum GLUnits[MAX_TEXUNITS] = {
	GL_TEXTURE0_ARB,
	GL_TEXTURE1_ARB,
	GL_TEXTURE2_ARB,
	GL_TEXTURE3_ARB,
	GL_TEXTURE4_ARB,
	GL_TEXTURE5_ARB,
	GL_TEXTURE6_ARB,
	GL_TEXTURE7_ARB
};

GLVertexArray::GLVertexArray(DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
	if( data != NULL ) {
		ASSERT( size > 0 );
	}
	this->vbo = 0;
	this->data_type = data_type;
	this->tex_unit = tex_unit;
	this->stream = stream;
	this->data = data;
	this->size = size;
	this->dim = dim;
	if( data_type == DATATYPE_COLORS && ( dim != 3 && dim != 4 ) ) {
		Vision::setError(new VisionException(NULL, VisionException::V_RENDER_ERROR, "DATATYPECOLORS must have dim 3 or 4"));
	}
	// data can be NULL for a "dummy" case for element arrays (see VertexArray::renderElements())
	if( data != NULL && GLRendererInfo::vertexBufferObjects ) {
		GLenum type = ( data_type == DATATYPE_INDICES ) ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB;
		glGenBuffersARB( 1, &vbo );
		glBindBufferARB( type, vbo );
		if( stream )
			glBufferDataARB( type, size, data, GL_STREAM_DRAW_ARB );
		else
			glBufferDataARB( type, size, data, GL_STATIC_DRAW_ARB );
		glBindBufferARB( type, 0 );
	}
}

GLVertexArray::~GLVertexArray() {
	if( vbo != 0 ) {
		glDeleteBuffersARB(1, &vbo);
		vbo = 0;
	}
}

char *GLVertexArray::render(bool new_data, int offset) const {
	char *ptr = NULL;
	if( vbo != 0 ) {
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vbo );
		if( new_data ) {
			ASSERT( stream );
			ASSERT( data != NULL );
			ASSERT( size > 0 );
			glBufferSubDataARB( GL_ARRAY_BUFFER_ARB, 0, size, data );
		}
		ptr = (char *)offset;
	}
	else {
		//ASSERT(offset == 0);
		ptr = (char *)data + offset;
	}
	return ptr;
}

void GLVertexArray::renderVertices(bool new_data, int offset) const {
	void *ptr = this->render(new_data, offset);
	//LOG("%d : renderVertices: %d (offset %d, new_data %d)\n", this, ptr, offset, new_data);
	glVertexPointer(3, GL_FLOAT, 0, ptr);
}

void GLVertexArray::renderNormals(bool new_data, int offset) const {
	void *ptr = this->render(new_data, offset);
	glNormalPointer(GL_FLOAT, 0, ptr);
}

void GLVertexArray::renderTexCoords(bool new_data, int offset) const {
	void *ptr = this->render(new_data, offset);
	glTexCoordPointer(dim, GL_FLOAT, 0, ptr);
}

void GLVertexArray::renderTexCoords(bool new_data, int offset, int unit) const {
	// TODO: check unit
	if( unit != 0 ) {
		GLenum gl_unit = GLUnits[unit];
		glClientActiveTextureARB(gl_unit);
	}
	void *ptr = this->render(new_data, offset);
	glTexCoordPointer(dim, GL_FLOAT, 0, ptr);
	if( unit != 0 ) {
		GLenum gl_unit0 = GLUnits[0];
		glClientActiveTextureARB(gl_unit0);
	}
}

void GLVertexArray::renderColors(bool new_data, int offset) const {
	void *ptr = this->render(new_data, offset);
	glColorPointer(dim, GL_UNSIGNED_BYTE, 0, ptr);
}

void GLVertexArray::update() const {
	if( vbo != 0 ) {
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vbo );
		ASSERT( stream );
		ASSERT( data != NULL );
		ASSERT( size > 0 );
		glBufferSubDataARB( GL_ARRAY_BUFFER_ARB, 0, size, data );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
	}
}

static GLenum GLModes[] = {
	GL_POINTS,
	GL_LINES,
	GL_LINE_LOOP,
	GL_LINE_STRIP,
	GL_TRIANGLES,
	GL_TRIANGLE_STRIP,
	GL_TRIANGLE_FAN,
	GL_QUADS,
	GL_QUAD_STRIP,
	GL_POLYGON
};

void GLVertexArray::updateIndices() const {
	if( vbo != 0 ) {
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vbo );
		//ASSERT( stream );
		ASSERT( data != NULL );
		ASSERT( size > 0 );
		glBufferSubDataARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0, size, data );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );
	}
}

void GLVertexArray::renderElements(DrawingMode mode, int n_this_renderlength, int n_this_vertices) const {
	//n_this_renderlength = 3;
	/*if( GLRendererInfo::compiledVAs ) {
		// disabled as this is pointless on modern hardware with Vertex Buffer Objects, and really would only give
		// a performance improvement if we weren't locking/unlocking every frame
		// note that this caused lighting problems on Voodoo Banshee
		glLockArraysEXT(0, n_this_vertices);
	}*/
	//glColor4i(255, 255, 255, 255);
	// TODO: check mode
	GLenum gl_mode = GLModes[mode];
	//GraphicsEnvironment::checkGLError("a");
	_DEBUG_CHECK_ERROR_;
	if( data == NULL ) {
		glDrawArrays(gl_mode, 0, n_this_renderlength);
		/*glBegin(gl_mode);
		//_DEBUG_CHECK_ERROR_;
		//GraphicsEnvironment::checkGLError(">>>");
		int max_val = 0;
		for(int i=0;i<n_this_renderlength;i++) {
			//for(int i=0;i<3;i++) {
			//LOG("### %d\n", i);
			int val = i;
			if( val > max_val )
				max_val = val;
			//printf("### %d : %d\n", i, val);
			//printf("okay\n");
			//GraphicsEnvironment::checkGLError("***");
			glArrayElement(i);
			//_DEBUG_CHECK_ERROR_;
			//glArrayElement(val);
			//printf("ping\n");
		}
		//GraphicsEnvironment::checkGLError("<<<");
		glEnd();*/
	}
	else {
		//if( false ) {
		if( vbo != 0 ) {
			glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vbo );
			_DEBUG_CHECK_ERROR_;
			//GraphicsEnvironment::checkGLError("1");
			if( GLRendererInfo::drawRangeElements )
				glDrawRangeElementsEXT(gl_mode, 0, n_this_vertices, n_this_renderlength, GL_UNSIGNED_SHORT, (char *)NULL);
			else
				glDrawElements(gl_mode, n_this_renderlength, GL_UNSIGNED_SHORT, (char *)NULL);
			_DEBUG_CHECK_ERROR_;
			//GraphicsEnvironment::checkGLError("2");
			glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );
		}
		else {
			_DEBUG_CHECK_ERROR_;
			//GraphicsEnvironment::getSingleton()->checkError("3");
			if( GLRendererInfo::drawRangeElements )
				glDrawRangeElementsEXT(gl_mode, 0, n_this_vertices, n_this_renderlength, GL_UNSIGNED_SHORT, data);
			else {
				glDrawElements(gl_mode, n_this_renderlength, GL_UNSIGNED_SHORT, data);
				/*glBegin(gl_mode);
				_DEBUG_CHECK_ERROR_;
				int max_val = 0;
				for(int i=0;i<n_this_renderlength;i++) {
					//for(int i=0;i<3;i++) {
					//LOG("### %d : %d\n", i, ((unsigned short *)data)[i]);
					unsigned short *ptr = (unsigned short *)data;
					unsigned short val = ptr[i];
					if( val > max_val )
						max_val = val;
					//printf("### %d : %d\n", i, val);
					glArrayElement(((unsigned short *)data)[i]);
					_DEBUG_CHECK_ERROR_;
					//glArrayElement(val);
				}
				//GraphicsEnvironment::checkGLError("<<<");
				glEnd();*/
			}
			//GraphicsEnvironment::getSingleton()->checkError("4");
			_DEBUG_CHECK_ERROR_;
		}
	}
	_DEBUG_CHECK_ERROR_;
	//GraphicsEnvironment::checkGLError("b");
	/*if( GLRendererInfo::compiledVAs ) {
		glUnlockArraysEXT();
	}*/
}

//CGGLShader::CGGLShader(Graphics3D *g3,Renderer *renderer,bool vertex_shader) : Shader(g3, renderer, vertex_shader) {
//CGGLShader::CGGLShader(Graphics3D *g3,GLRenderer *renderer,bool vertex_shader) : Shader(g3, vertex_shader) {
CGGLShader::CGGLShader(GLRenderer *renderer,bool vertex_shader) : CGShader(vertex_shader) {
	this->renderer = renderer;
}

/*void CGGLShader::init() {
	//if( this->vertex_shader )
	{
		// Matrices
		cgparam_ModelViewProj = cgGetNamedParameter(cgProgram, "mx.ModelViewProj");
		cgparam_ModelViewIT = cgGetNamedParameter(cgProgram, "mx.ModelViewIT");
		cgparam_ModelView = cgGetNamedParameter(cgProgram, "mx.ModelView");

	}

	//this->setDefaults();
}*/

#if 0
void CGGLShader::setTexture(const Texture *texture) const {
	this->SetUniformParameter1f("textureID", texture != NULL ? 1.0f : 0.0f);
	/*const GLTexture *gl_texture = static_cast<const GLTexture *>(texture);
	if( cgparam_TextureID != NULL ) {
		//cgGLSetParameter1f( cgparam_TextureID, texture != NULL ? (float)texture->textureID : 0.0f );
		cgSetParameter1i( cgparam_TextureID, gl_texture != NULL ? 1 : 0 );
	}*/
	// not actually needed
	/*if( cgparam_Texture != NULL ) {
		if( gl_texture != NULL ) {
			cgGLSetTextureParameter( cgparam_Texture, gl_texture->getTextureID() );
			cgGLEnableTextureParameter( cgparam_Texture );
		}
		else {
			cgGLDisableTextureParameter( cgparam_Texture );
		}
	}*/
}
#endif

/*void CGGLShader::setSecondaryTexture(const Texture *texture) const {
	const GLTexture *gl_texture = static_cast<const GLTexture *>(texture);
	if( cgparam_SecondaryTexture != NULL ) {
		if( gl_texture != NULL ) {
			cgGLSetTextureParameter( cgparam_SecondaryTexture, gl_texture->getTextureID() );
			cgGLEnableTextureParameter( cgparam_SecondaryTexture );
		}
		else {
			cgGLDisableTextureParameter( cgparam_SecondaryTexture );
		}
	}
}*/

void CGGLShader::setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const {
	/*CGparameter cgparam_ModelViewProj = cgGetNamedParameter(cgProgram, "mx.ModelViewProj");
	CGparameter cgparam_ModelViewIT = cgGetNamedParameter(cgProgram, "mx.ModelViewIT");
	CGparameter cgparam_ModelView = cgGetNamedParameter(cgProgram, "mx.ModelView");*/
	/*if( cgparam_ModelViewProj != NULL )
		cgGLSetStateMatrixParameter(cgparam_ModelViewProj, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
	if( cgparam_ModelViewIT != NULL )
		cgGLSetStateMatrixParameter(cgparam_ModelViewIT, CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);
	if( cgparam_ModelView != NULL )
		cgGLSetStateMatrixParameter(cgparam_ModelView, CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);*/
	CGparameter p = NULL;
	p = cgGetNamedParameter(cgProgram, parameter_modelviewproj);
	if( p != NULL ) {
		cgGLSetStateMatrixParameter(p, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
	}
	p = cgGetNamedParameter(cgProgram, parameter_modelviewit);
	if( p != NULL ) {
		cgGLSetStateMatrixParameter(p, CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_INVERSE_TRANSPOSE);
	}
	p = cgGetNamedParameter(cgProgram, parameter_modelview);
	if( p != NULL ) {
		cgGLSetStateMatrixParameter(p, CG_GL_MODELVIEW_MATRIX, CG_GL_MATRIX_IDENTITY);
	}
}

void CGGLShader::SetModelViewProjMatrix(const char *parameter) const {
	CGparameter pMatrix = cgGetNamedParameter(cgProgram, parameter);
	if( pMatrix != NULL ) {
		cgGLSetStateMatrixParameter(pMatrix, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
	}
}

CGFXGLShaderEffect::CGFXGLShaderEffect(GLRenderer *renderer, CGcontext cg_context, const char *file, const char **args) : CGFXShaderEffect(cg_context, file, args), renderer(renderer) {
}

void CGFXGLShaderEffect::setMatrices(const char *parameter_modelviewproj, const char *parameter_modelviewit, const char *parameter_modelview) const {
	if( cgparam_ModelViewProj != NULL ) {
		cgGLSetStateMatrixParameter(cgparam_ModelViewProj, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
	}
}

void CGFXGLShaderEffect::SetModelViewProjMatrix(const char *parameter) const {
	if( cgparam_ModelViewProj != NULL ) {
		cgGLSetStateMatrixParameter(cgparam_ModelViewProj, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
	}
}

/*void CGGLShader::SetUniformParameter1f(const char *pszParameter, float p1) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgGLSetParameter1f(p, p1);
}

void CGGLShader::SetUniformParameter2f(const char *pszParameter, float p1, float p2) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgGLSetParameter2f(p, p1, p2);
}

void CGGLShader::SetUniformParameter3f(const char *pszParameter, float p1, float p2, float p3) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgGLSetParameter3f(p, p1, p2, p3);
}

void CGGLShader::SetUniformParameter4f(const char *pszParameter, float p1, float p2, float p3, float p4) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgGLSetParameter4f(p, p1, p2, p3, p4);
}

void CGGLShader::SetUniformParameter2fv(const char *pszParameter, const float v[2]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgGLSetParameter2fv(p, v);
}

void CGGLShader::SetUniformParameter3fv(const char *pszParameter, const float v[3]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgGLSetParameter3fv(p, v);
}

void CGGLShader::SetUniformParameter4fv(const char *pszParameter, const float v[4]) const {
	CGparameter p = cgGetNamedParameter(cgProgram, pszParameter);
	if( p )
		cgGLSetParameter4fv(p, v);
}*/

void CGGLShader::enable() const {
	// set renderer specific uniforms

	/*if( this->vertex_shader && this->cgparam_clipping_plane != 0 ) {
		if( renderer->shader_clipping )
			cgGLSetParameter4f( this->cgparam_clipping_plane, renderer->shader_clipping_plane[0], renderer->shader_clipping_plane[1], renderer->shader_clipping_plane[2], renderer->shader_clipping_plane[3] );
		//cgGLSetParameter4f( this->cgparam_clipping_plane, -g3->shader_clipping_plane[0], -g3->shader_clipping_plane[1], -g3->shader_clipping_plane[2], -g3->shader_clipping_plane[3] );
		else {
			cgGLSetParameter4f( this->cgparam_clipping_plane, 0.0, 0.0, 0.0, -1.0 );
		}
	}*/
	if( this->vertex_shader ) {
		if( renderer->shader_clipping ) {
			this->SetUniformParameter4fv("clipping_plane", renderer->shader_clipping_plane);
		}
		else {
			this->SetUniformParameter4f("clipping_plane", 0.0, 0.0, 0.0, -1.0);
		}
	}

	// TODO: fog disabled in shaders for now
	/*if( !this->vertex_shader ) {
	Fog *fog = &g3->getWorld()->fog;
	if( fog->isEnabled() ) {
	//cgGLSetParameter1f( this->cgparam_FogType, fog->fogtype + 1 );
	this->SetUniformParameter1f("fog.type", fog->fogtype + 1);
	float parms[3];
	parms[0] = fog->start;
	parms[1] = fog->end;
	parms[2] = fog->density;
	//cgGLSetParameter3f( this->cgparam_FogParms, fog->start, fog->end, fog->density );
	this->SetUniformParameter3f("fog.parms", fog->start, fog->end, fog->density);
	float color[4];
	fog->floatArray(color);
	//cgGLSetParameter4f( this->cgparam_FogColor, color[0], color[1], color[2], color[3] );
	this->SetUniformParameter4f("fog.color", color[0], color[1], color[2], color[3]);
	}
	else {
	//cgGLSetParameter1f( this->cgparam_FogType, 0 );
	this->SetUniformParameter1f("fog.type", 0);
	}
	}*/

	// set rest to some defaults
	/*if( this->vertex_shader ) {
		this->setHardwareAnimationAlpha(0.0);
	}*/

	cgGLEnableProfile(cgProfile);
	cgGLBindProgram(cgProgram);
	/*if( this->vertex_shader )
		renderer->setActiveVertexShader(this);
	else
		renderer->setActivePixelShader(this);*/
}

/*void CGGLShader::disable() const {
	cgGLDisableProfile(cgProfile);
}*/

GLTexture::GLTexture(GLRenderer *renderer) : renderer(renderer) {
	/*this->textureID = 0;
	this->mipmap = true;
	this->w = 0;
	this->h = 0;*/
}

// Used for render-to-texture
//Texture::Texture(bool mipmap,int w,int h) : Image2D(w, h) {
//GLTexture::GLTexture(GLRenderer *renderer,bool mipmap,int w,int h) : renderer(renderer) {
GLTexture::GLTexture(GLRenderer *renderer,int w,int h,bool alpha,bool floating_point) : renderer(renderer) {
	//this->filename = NULL;
	/*this->textureID = 0;
	this->mipmap = mipmap;
	this->w = w;
	this->h = h;*/
	body.mipmap = false;
	body.alpha = alpha;
	body.floating_point = floating_point;
	// n.b., floating point not yet supported for OpenGL
	body.w = w;
	body.h = h;
	this->init(NULL, false, NULL);
}

//Texture::Texture(Image2D *image) : Image2D() {
GLTexture::GLTexture(GLRenderer *renderer,VI_Image *image,bool mask, unsigned char maskcol[3]) : renderer(renderer) {
	// image is deleted automatically
	//this->filename = NULL;
	//this->rgbdata = image->rgbdata;
	//this->mask = NULL; // mask is ignored
	/*this->w = image->w;
	this->h = image->h;*/
	body.w = image->getWidth();
	body.h = image->getHeight();
	//this->alpha = image->alpha;

	/*image->rgbdata = NULL; // so the data isn't freed by image's destructor
	//image->mask = NULL;
	delete image;
	this->init(false, NULL);*/

	//ASSERT( image->getMask() == NULL ); // bug?! - Image2D's mask is an array, but here we only accept a single mask colour!
	this->init(image, mask, maskcol); // original
	//this->init(image, image->getMask() != NULL, image->getMask());
	delete image;
}

//vector<Texture *> textures;
//vector<GLTexture *> textures;
//typedef vector<GLTexture *> TexturesCollection;
typedef set<GLTexture *> TexturesCollection;
typedef TexturesCollection::iterator TexturesIter;
TexturesCollection textures;

GLTexture::~GLTexture() {
	//GraphicsEnvironment::checkGLError("Texture::~Texture enter");
	if(body.textureID != 0) {
		// search cache to make sure not in use
		bool found = false;
		//for(int i=0;i<textures.size() && !found;i++) {
		for(TexturesIter iter = textures.begin();iter != textures.end();++iter) {
			//GLTexture *texture = textures[i];
			GLTexture *texture = *iter;
			if( texture != this && texture->getTextureID() == body.textureID )
				found = true;
		}
		if( !found )
			glDeleteTextures(1,&body.textureID);
		//remove_vec(&textures, this);
		textures.erase(this);
	}
	//GraphicsEnvironment::checkGLError("Texture::~Texture exit");
}

GLTexture *GLTexture::textureFromCache(const char *filename) {
	//return NULL; // texture caching disabled for now
	// Already loaded?
	// If a texture is loaded from disk, it can't be modified, so we can cache them.
	// This can't be done with textures converted from images.
	// We create a new Texture object still, but give it the same textureID
	//for(int i=0;i<textures.size();i++) {
	for(TexturesIter iter = textures.begin();iter != textures.end();++iter) {
		//GLTexture *texture = textures[i];
		GLTexture *texture = *iter;
		//if( texture->filename != NULL && stricmp(texture->filename, filename) == 0 ) {
		if( stricmp(texture->getFilename(), filename) == 0 ) {
			//return texture;
			/*Texture *new_texture = new Texture();
			// need to preserve VisionObject members
			//assert( texture->rgbdata == NULL );
			//new_texture->mask = texture->mask;
			new_texture->w = texture->w;
			new_texture->h = texture->h;
			//new_texture->alpha = texture->alpha;
			new_texture->filename = new char[strlen(filename)+1];
			strcpy(new_texture->filename, filename);
			new_texture->mipmap = texture->mipmap;
			new_texture->textureID = texture->textureID;*/
			GLTexture *new_texture = static_cast<GLTexture *>(texture->copy());
			return new_texture;
		}
	}
	return NULL;
}

Texture *GLTexture::copy() {
	GLTexture *new_texture = new GLTexture(this->renderer);
	// need to preserve VisionObject members
	//assert( this->rgbdata == NULL );
	//new_texture->mask = this->mask;
	/*new_texture->w = this->w;
	new_texture->h = this->h;
	//new_texture->alpha = this->alpha;
	new_texture->filename = this->filename;
	new_texture->mipmap = this->mipmap;
	new_texture->textureID = this->textureID;*/
	new_texture->body = this->body;
	new_texture->wrap_mode = this->wrap_mode;
	new_texture->draw_mode = this->draw_mode;
	return new_texture;
}

Texture *GLTexture::loadTexture(GLRenderer *renderer,const char *filename) {
	//Texture *texture = new Texture();
	GLTexture *texture = textureFromCache(filename);
	if( texture == NULL ) {
		texture = new GLTexture(renderer);
		/*if(!texture->load(filename, false, NULL)) {
			delete texture;
			return NULL;
		}*/
		texture->load(filename, false, NULL);
	}
	//textures.push_back(texture);
	textures.insert(texture);
	return texture;
}

Texture *GLTexture::loadTextureWithMask(GLRenderer *renderer,const char *filename, unsigned char maskcol[3]) {
	GLTexture *texture = new GLTexture(renderer);
	/*if(!texture->load(filename, true, maskcol)) {
		delete texture;
		return NULL;
	}*/
	texture->load(filename, true, maskcol);
	return texture;
}

void GLTexture::enable() {
	//glEnable(GL_TEXTURE_2D); // always enable, in case not enabled (consistency with D3D9)
	//GraphicsEnvironment::checkGLError("Texture::enable BEGIN");
	glBindTexture(GL_TEXTURE_2D, body.textureID);
	//GraphicsEnvironment::checkGLError("Texture::enable bound");

	if( body.mipmap ) {
		//GraphicsEnvironment::checkGLError("Texture::enable set mag filter");
		if( this->draw_mode == DRAWMODE_SMOOTH ) {
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); // needed to avoid blockyness close-up to textures
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		}
		else {
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST_MIPMAP_NEAREST);
		}

		//GraphicsEnvironment::checkGLError("Texture::enable set min filter");
		/*glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST_MIPMAP_LINEAR);
		GraphicsEnvironment::checkGLError("Texture::enable set min filter b");*/
	}
	else {
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	}
	//GraphicsEnvironment::checkGLError("Texture::enable done filters");

	if( this->wrap_mode == WRAPMODE_CLAMP ) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
	else if( this->wrap_mode == WRAPMODE_MIRROR ) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	}
	else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	/*if( this->renderer->getActivePixelShader() != NULL ) {
		//this->renderer->getActivePixelShader()->setTexture(this);
		const CGShader *shader = static_cast<const CGShader *>(this->renderer->getActivePixelShader());
		shader->setTexture(this);
	}*/
	/*if( this->renderer->getActiveShader() != NULL ) {
		this->renderer->getActiveShader()->SetUniformParameter1f("textureID", 1.0f);
	}*/
	//GraphicsEnvironment::checkGLError("Texture::enable END");
}

/*void GLTexture::clampToEdge() {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void GLTexture::clampMirror() {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
}*/

void GLTexture::copyTo(int width, int height) {
	glBindTexture(GL_TEXTURE_2D, body.textureID);
	glCopyTexImage2D(GL_TEXTURE_2D, 0, body.alpha ? GL_RGBA : GL_RGB, 0, 0, width, height, 0);
}

void GLTexture::draw(int x, int y) {
	this->draw(x, y, body.w, body.h);
}

void GLTexture::draw(int x, int y, int w, int h) {
	//return;
	//glDisable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glColor4ub(255, 255, 255, 255);
	this->enable();
	if( body.alpha ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	glBegin(GL_QUADS);

	glTexCoord2s(0,1);
	glVertex2i(x,y-h);

	glTexCoord2s(0,0);
	glVertex2i(x,y);

	glTexCoord2s(1,0);
	glVertex2i(x+w,y);

	glTexCoord2s(1,1);
	glVertex2i(x+w,y-h);

	/*glTexCoord2s(0,0);
	glVertex2s(x,y);
	glTexCoord2s(0,1);
	glVertex2s(x,(short)(y+body.h));
	glTexCoord2s(1,1);
	glVertex2s((short)(x+body.w),(short)(y+body.h));
	glTexCoord2s(1,0);
	glVertex2s((short)(x+body.w),y);*/

	glEnd();

	if( body.alpha ) {
		glDisable(GL_BLEND);
	}
	//glFlush();
	glDisable(GL_TEXTURE_2D);
	//glEnable(GL_CULL_FACE);
}

void GLTexture::init(VI_Image *image,bool mask, unsigned char maskcol[3]) {
	//LOG("    Initialising Texture..\n");
	GraphicsEnvironment::getSingleton()->checkError("Texture::init enter");
	GLTexture *texture = this;
	bool image_alpha = false;
	if( image != NULL ) {
		body.w = image->getWidth();
		body.h = image->getHeight();
		image_alpha = image->getAlpha();
	}
	body.alpha = image_alpha || mask;

	if( !VI_isPower(body.w, 2) || !VI_isPower(body.h, 2) ) {
		// see note under D3D9Texture::createFromImage() - although OpenGL seems to cope better on Intel GMA 950, it's probably best to insist on power of 2 textures anyway.
		LOG("texture has dimensions %d x %d : not power of 2\n", body.w, body.h);
		Vision::setError(new VisionException(NULL, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed\n"));
		return;
	}

	//const bool mipmap = true;
	//const bool mipmap = false;
	glGenTextures(1, &texture->body.textureID);
	//LOG("    Texture has OpenGL ID %d\n",texture->body.textureID);
	// Typical Texture Generation Using Data From The Bitmap
	glBindTexture(GL_TEXTURE_2D, texture->body.textureID);

	// Generate The Texture
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->w, texture->h, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->rgbdata);
	//glTexImage2D(GL_TEXTURE_2D, 0, 3, texture->w, texture->h, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->rgbdata);

	//int w = 256;
	int n_bytes = body.alpha ? 4 : 3;
	GLubyte *data = new GLubyte[texture->body.w*texture->body.h*n_bytes];
	//LOG(">>> %d %d %d\n", texture->w, texture->h, n_bytes);
	if( image != NULL ) {
		GLubyte *ptr = data;
		unsigned char *s_ptr = (unsigned char *)image->getData();
		for(int i=0;i<texture->body.w*texture->body.h;i++) {
			//LOG(">>> %d\n", i);
			unsigned char r = s_ptr[0];
			unsigned char g = s_ptr[1];
			unsigned char b = s_ptr[2];
			bool masked = false;
			if( mask ) {
				if( r == maskcol[0] && g == maskcol[1] && b == maskcol[2] ) {
					masked = true;
					r = g = b = 0; // so the mask colour doesn't show up at all, e.g., with filtering
				}
			}
			*ptr++ = r;
			*ptr++ = g;
			*ptr++ = b;
			if( mask ) {
				int alpha = 255;
				//if( rgbdata[i].r == maskcol->r && rgbdata[i].g == maskcol->g && rgbdata[i].b == maskcol->b ) {
				//if( r == maskcol->r && g == maskcol->g && b == maskcol->b ) {
				//if( r == maskcol[0] && g == maskcol[1] && b == maskcol[2] ) {
				if( masked ) {
					alpha = 0;
				}
				*ptr++ = alpha;
			}
			else if( image_alpha ) {
				//*ptr++ = rgbdata[i].a;
				*ptr++ = s_ptr[3];
			}
			s_ptr += image_alpha ? 4 : 3;
		}
	}
	else {
		memset(data, 0, texture->body.w*texture->body.h*n_bytes);
	}
	//GraphicsEnvironment::checkGLError("Texture::init glTexImage2D");
	//LOG(" n_bytes %d , w %d , h %d , alpha %d , mask %d\n", n_bytes, texture->w, texture->h, alpha, mask);
	glTexImage2D(GL_TEXTURE_2D, 0, n_bytes, texture->body.w, texture->body.h, 0, body.alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);
	//GraphicsEnvironment::checkGLError("Texture::init gluBuild2DMipmaps");
	if(body.mipmap) {
		gluBuild2DMipmaps(GL_TEXTURE_2D, n_bytes, texture->body.w, texture->body.h, body.alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);
	}
	//GraphicsEnvironment::checkGLError("Texture::init gluBuild2DMipmaps done");
	delete [] data;

	// free stuff
	/*delete [] texture->rgbdata;
	texture->rgbdata = NULL;*/
	//LOG("    done\n");
	GraphicsEnvironment::getSingleton()->checkError("Texture::init exit");
}

/*void Texture::init(bool mask, rgb_struct *maskcol) {
//LOG("    Initialising Texture..\n");
GraphicsEnvironment::checkGLError("Texture::init enter");
Texture *texture = this;

if( !VI_isPower(this->w, 2) || !VI_isPower(this->h, 2) ) {
LOG("texture has dimensions %d x %d : not power of 2\n", this->w, this->h);
//Vision::setError(new VisionException(this, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed\n"));
//return;
}

//const bool mipmap = true;
//const bool mipmap = false;
glGenTextures(1, &texture->textureID);
LOG("    Texture has OpenGL ID %d\n",texture->textureID);
// Typical Texture Generation Using Data From The Bitmap
glBindTexture(GL_TEXTURE_2D, texture->textureID);

// Generate The Texture
//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->w, texture->h, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->rgbdata);
//glTexImage2D(GL_TEXTURE_2D, 0, 3, texture->w, texture->h, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->rgbdata);

//int w = 256;
{
int n_bytes = ( alpha || mask ) ? 4 : 3;
GLubyte *data = new GLubyte[texture->w*texture->h*n_bytes];
//LOG(">>> %d %d %d\n", texture->w, texture->h, n_bytes);
GLubyte *ptr = data;
unsigned char *s_ptr = (unsigned char *)rgbdata;
for(int i=0;i<texture->w*texture->h;i++) {
//LOG(">>> %d\n", i);
unsigned char r = s_ptr[0];
unsigned char g = s_ptr[1];
unsigned char b = s_ptr[2];
*ptr++ = *s_ptr++;
*ptr++ = *s_ptr++;
*ptr++ = *s_ptr++;
if( mask ) {
int alpha = 255;
//if( rgbdata[i].r == maskcol->r && rgbdata[i].g == maskcol->g && rgbdata[i].b == maskcol->b ) {
if( r == maskcol->r && g == maskcol->g && b == maskcol->b ) {
alpha = 0;
}
*ptr++ = alpha;
}
else if( alpha ) {
//*ptr++ = rgbdata[i].a;
*ptr++ = *s_ptr++;
}
}
//GraphicsEnvironment::checkGLError("Texture::init glTexImage2D");
//LOG(" n_bytes %d , w %d , h %d , alpha %d , mask %d\n", n_bytes, texture->w, texture->h, alpha, mask);
glTexImage2D(GL_TEXTURE_2D, 0, n_bytes, texture->w, texture->h, 0, ( alpha || mask ) ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);
//GraphicsEnvironment::checkGLError("Texture::init gluBuild2DMipmaps");
if(mipmap) {
gluBuild2DMipmaps(GL_TEXTURE_2D, n_bytes, texture->w, texture->h, ( alpha || mask ) ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);
}
//GraphicsEnvironment::checkGLError("Texture::init gluBuild2DMipmaps done");
delete [] data;
}

// free stuff
delete [] texture->rgbdata;
texture->rgbdata = NULL;
//LOG("    done\n");
GraphicsEnvironment::checkGLError("Texture::init exit");
}*/

void GLTexture::load(const char *filename,bool mask, unsigned char maskcol[3]) {
	/*bool ok = Image2D::load(filename);
	if( ok ) {
	this->init(mask, maskcol);*/
	Image2D *image = Image2D::loadImage(filename);
	/*if( image == NULL ) {
		return false;
	}*/

	if( mask && ( !VI_isPower(image->getWidth(), 2) || !VI_isPower(image->getHeight(), 2) ) ) {
		// We disallow this, as resizing would mess up the mask. Although we could get round this with a simple resize, it's probably better to explicitly disallow, and insist on power-by-2 texture files
		LOG("texture has dimensions %d x %d : not power of 2\n", image->getWidth(), image->getHeight());
		Vision::setError(new VisionException(NULL, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed with mask\n"));
		return;
	}
	if( !image->makePowerOf2() ) {
		delete image;
		Vision::setError(new VisionException(image, VisionException::V_IMAGE_ERROR, "Failed to scale texture image to power-of-2 size\n"));
		return;
	}

	this->init(image, mask, maskcol);
	/*this->filename = new char[strlen(filename)+1];
	strcpy(this->filename, filename);*/
	this->body.filename = filename;
	delete image;
}

bool GLFramebufferObject::generate(int width,int height,bool want_depth_stencil) {
	this->free(); // free any existing data

	if( !VI_isPower(width, 2) || !VI_isPower(height, 2) ) {
		// warn just in case?
		LOG("requested framebuffer has dimensions %d x %d : not power of 2\n", width, height);
		//Vision::setError(new VisionException(NULL, VisionException::V_TEXTURE_NON_POWER_2, "Non power of 2 textures not allowed\n"));
		//return false;
	}

	bool ok = true;
	glGenFramebuffersEXT(1, &fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);

	glGenTextures(1, &color_texture);
	glBindTexture(GL_TEXTURE_2D, color_texture);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,  tex_width, tex_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL); // dummy test
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,  tex_width, tex_height, 0, GL_RGBA, GL_INT, NULL);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB,  tex_width, height, 0, GL_RGBA, GL_INT, NULL);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB,  tex_width, tex_height, 0, GL_RGBA, GL_FLOAT, NULL); // correct
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB,  tex_width, tex_height, 0, GL_RGBA, GL_HALF_FLOAT_ARB, NULL);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,  width, height, 0, GL_RGBA, GL_FLOAT, NULL); // actually slightly faster!
	// GL_LINEAR apparentely runs in software on platforms (though seems okay on NVIDIA 8600GT), and GL_NEAREST works fine for multisampling so okay to use that
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, color_texture, 0);

	if( want_depth_stencil ) {
		// creating an 24-bit depth + 8-bit stencil texture
		GLenum texTarget = GL_TEXTURE_RECTANGLE_ARB;
		GLenum texInternalFormat = GL_DEPTH24_STENCIL8_EXT;
		GLenum texFormat = GL_DEPTH_STENCIL_EXT;
		GLenum texType = GL_UNSIGNED_INT_24_8_EXT;
		GLfloat texFilterMode = GL_NEAREST;
		glGenTextures(1, &depth_stencil_texture);
		glBindTexture(texTarget, depth_stencil_texture);
		glTexImage2D(texTarget, 0, texInternalFormat, width, height, 0, texFormat, texType, NULL);

		glTexParameterf(texTarget, GL_TEXTURE_MIN_FILTER, texFilterMode);
		glTexParameterf(texTarget, GL_TEXTURE_MAG_FILTER, texFilterMode);
		glTexParameterf(texTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(texTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(texTarget, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL);

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, texTarget , depth_stencil_texture, 0);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, texTarget , depth_stencil_texture, 0);
	}

	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	if( status != GL_FRAMEBUFFER_COMPLETE_EXT ) {
		ok = false;
	}
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	return ok;
}

void GLFramebufferObject::free() {
	if( fbo != NULL ) {
		glDeleteFramebuffersEXT(1, &fbo);
		fbo = NULL;
	}
	if( color_texture != NULL ) {
		glDeleteTextures(1, &color_texture);
		color_texture = NULL;
	}
	if( depth_stencil_texture != NULL ) {
		glDeleteTextures(1, &depth_stencil_texture);
		depth_stencil_texture = NULL;
	}
}

void GLFramebufferObject::bind() const {
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
}

void GLFramebufferObject::enableTexture() const {
	glBindTexture(GL_TEXTURE_2D, color_texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

#define WIDTHBYTES(i)       ((i+31)/32*4)
#define PALETTE_SIZE        2

const int START = 32;
const int N_CHARS = 256;
//const int N_CHARS = 128;

GLFontBuffer::GLFontBuffer(GLRenderer *renderer,const char *family,int pt) : FontBuffer() {
#ifdef _WIN32
	init(renderer,family,pt,FW_NORMAL);
#else
	init(renderer,family,pt,0);
#endif
}

GLFontBuffer::GLFontBuffer(GLRenderer *renderer,const char *family,int pt,int style) : FontBuffer() {
	init(renderer,family,pt,style);
}

void GLFontBuffer::init(GLRenderer *renderer,const char *family,int pt,int style) {
    this->renderer = renderer;
#ifdef USE_WGL_FONTS
	base = glGenLists(N_CHARS-START);

	HFONT g_hFont = CreateFontA(-pt,
		0, 0, 0,
		style,
		FALSE, FALSE, FALSE,
		ANSI_CHARSET,
		//OUT_DEFAULT_PRECIS,
		OUT_TT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		//DEFAULT_QUALITY,
		ANTIALIASED_QUALITY,
		//VARIABLE_PITCH,
		FF_DONTCARE | DEFAULT_PITCH,
		family);
	if(g_hFont != NULL) {
		//HDC hdc = genv->hDC;
		HDC hdc = GetDC(NULL);
		HFONT oldfont = (HFONT)SelectObject(hdc, g_hFont);
		wglUseFontBitmaps(hdc, START, N_CHARS-START, base);

		width = new int[N_CHARS-START];
		height = new int[N_CHARS-START];
		for(int i=0;i<N_CHARS-START;i++) {
			width[i] = 0;
			height[i] = 0;
		}
		{
			for(int ch = START;ch<N_CHARS;ch++) {
				SIZE size;
				//char c = (char)ch;
				char c[2];
				c[0] = (char)ch;
				c[1] = '\0';
				GetTextExtentPoint32A(hdc, c, 1, &size);
				width[(int)ch-START] = size.cx + 0;
				height[(int)ch-START] = size.cy + 0;
			}
		}
		SelectObject(hdc, oldfont);
		DeleteObject(g_hFont);
	}
	font_height = height[getIndex('I')];
#else
	advance_x = NULL;
	offset_x = NULL;
	offset_y = NULL;
	width = NULL;
	height = NULL;
	//base = 0;

	Image2D **images = NULL;
	Image2D::FontInfo *fontInfo = NULL;
	Image2D::GlobalFontInfo globalFontInfo;
	int n_images = Image2D::loadFont(&images, &globalFontInfo, &fontInfo, family, pt, START, N_CHARS);
	descent = globalFontInfo.descent;
	font_height = globalFontInfo.height;
	advance_x = new int[n_images];
	offset_x = new int[n_images];
	offset_y = new int[n_images];
	width = new int[n_images];
	height = new int[n_images];
	textures = new Texture *[n_images];
	for(int i=0;i<n_images;i++) {
		advance_x[i] = fontInfo[i].advance_x;
		offset_x[i] = fontInfo[i].offset_x;
		offset_y[i] = fontInfo[i].offset_y;
		//height[i] = globalFontInfo.height;
		if( images[i] == NULL ) {
			width[i] = 0;
			height[i] = 0;
			textures[i] = NULL;
		}
		else {
			width[i] = images[i]->getWidth();
			height[i] = images[i]->getHeight();
			//images[i]->makePowerOf2();
			/*for(int y=0;y<images[i]->getHeight();y++) {
				for(int x=0;x<images[i]->getWidth();x++) {
					unsigned char r, g, b, a;
					images[i]->getRGBA(&r, &g, &b, &a, x, y);
					if( r != 0 && r != 255 ) {
						LOG("%d, %d: %d %d %d %d\n", x, y, r, g, b, a);
					}
				}
			}*/
			textures[i] = new GLTexture(this->renderer, images[i], false, (unsigned char *)NULL); // image automatically deleted
		}
	}
	delete [] images;
	delete [] fontInfo;
#endif
}

GLFontBuffer::~GLFontBuffer() {
	/*for(int i=0;i<n_rasters;i++)
	delete [] rasters[i];
	delete [] rasters;*/
#ifdef USE_WGL_FONTS
	if(base != 0)
		glDeleteLists(base, N_CHARS-START);
#else
	delete [] advance_x;
	delete [] offset_x;
	delete [] offset_y;
	delete [] width;
	delete [] height;
	if( textures != NULL ) {
		for(int i=0;i<N_CHARS-START;i++) {
			delete textures[i];
		}
		delete [] textures;
	}
#endif
}

size_t GLFontBuffer::memUsage() {
	size_t size = sizeof(this);
	size += 3 * (N_CHARS-START) * sizeof(int);
	size += (N_CHARS-START) * sizeof(Texture *);
	return size;
}

int GLFontBuffer::getIndex(char letter) const {
	return (int)letter - START;
}

/*
#ifdef _WIN32
bool FontBuffer::font_InitDC(char *family,int pt)
{

g_hFont = CreateFont(pt,
0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE,
ANSI_CHARSET,
OUT_DEFAULT_PRECIS,
CLIP_DEFAULT_PRECIS,
DEFAULT_QUALITY,
VARIABLE_PITCH,
family);
if (g_hFont == NULL)
return FALSE;

return TRUE;
}
#endif*/
/*
bool FontBuffer::initRasters() {
int n_rasters = N_CHARS - 32;
rasters = new GLubyte *[n_rasters];
width = new int[n_rasters];
height = new int[n_rasters];
for(int i=32;i<128;i++) {
if(!initRaster((char)i))
return FALSE;
}
return TRUE;
}

bool FontBuffer::initRaster(char letter) {
#ifdef _WIN32
HDC                         hdc;
LPBITMAPINFO                pbmi;
SIZE                        size;
HDC                         g_hMemDC;       // memory DC for rendering text
HBITMAP                     g_hBitmap;      // bitmap for holding text
char str[2];
str[0] = letter;
str[1] = 0;

// Create a memory DC for rendering our text into
hdc = GetDC(HWND_DESKTOP);
g_hMemDC = CreateCompatibleDC(hdc);
ReleaseDC(NULL, hdc);
if (g_hMemDC == NULL)
{
DeleteObject(g_hFont);
return FALSE;
}

// Select font, and get text dimensions
SelectObject(g_hMemDC, g_hFont);
GetTextExtentPoint32(g_hMemDC, str, sizeof(str) - 1, &size);
int index = getIndex(letter);
int                         g_iBmpHeight;   // height of DIB
int                         g_iBmpWidth;    // width of DIB
g_iBmpWidth = size.cx + 2;
g_iBmpHeight = size.cy + 2;
width[index] = g_iBmpWidth;
height[index] = g_iBmpHeight;
if(g_iBmpWidth % 8 != 0)
g_iBmpWidth = ((g_iBmpWidth/8)+1) * 8;
rasters[index] = new GLubyte[ g_iBmpWidth*g_iBmpHeight)/8 ];

// Create a dib section for containing the bits
pbmi = (LPBITMAPINFO) LocalAlloc(LPTR, sizeof(BITMAPINFO) +
PALETTE_SIZE * sizeof(RGBQUAD));
if (pbmi == NULL)
{
DeleteObject(g_hFont);
DeleteDC(g_hMemDC);
return FALSE;
}
pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
pbmi->bmiHeader.biWidth = g_iBmpWidth;
pbmi->bmiHeader.biHeight = g_iBmpHeight;
pbmi->bmiHeader.biPlanes = 1;
pbmi->bmiHeader.biBitCount = 8;  // 8bpp makes it easy to get data

pbmi->bmiHeader.biCompression = BI_RGB;
pbmi->bmiHeader.biXPelsPerMeter = 0;
pbmi->bmiHeader.biYPelsPerMeter = 0;
pbmi->bmiHeader.biClrUsed = PALETTE_SIZE;
pbmi->bmiHeader.biClrImportant = PALETTE_SIZE;

pbmi->bmiHeader.biSizeImage = WIDTHBYTES(g_iBmpWidth * 8) * g_iBmpHeight;

// Just a plain monochrome palette
pbmi->bmiColors[0].rgbRed = 0;
pbmi->bmiColors[0].rgbGreen = 0;
pbmi->bmiColors[0].rgbBlue = 0;
pbmi->bmiColors[1].rgbRed = 255;
pbmi->bmiColors[1].rgbGreen = 255;
pbmi->bmiColors[1].rgbBlue = 255;

// Create a DIB section that we can use to read the font bits out of
LPSTR                       g_pBmpBits;     // pointer to DIB bits
g_hBitmap = CreateDIBSection(hdc,
pbmi,
DIB_RGB_COLORS,
(void **) &g_pBmpBits,
NULL,
0);
LocalFree(pbmi);
if (g_hBitmap == NULL)
{
DeleteObject(g_hFont);
DeleteDC(g_hMemDC);
return FALSE;
}

// Set up our memory DC with the font and bitmap
SelectObject(g_hMemDC, g_hBitmap);
SetBkColor(g_hMemDC, RGB(0, 0, 0));
SetTextColor(g_hMemDC, RGB(255, 255, 255));

// Output text to our memory DC (the bits end up in our DIB section)

PatBlt(g_hMemDC, 0, 0, g_iBmpWidth, g_iBmpHeight, BLACKNESS);

TextOut(g_hMemDC, 1, 1, str, lstrlen(str));

GLubyte *ptr = rasters[index];
LPSTR src = g_pBmpBits;
int bit = 0;
for(int y=0;y<g_iBmpHeight;y++) {
for(int x=0;x<g_iBmpWidth;x++) {
if(bit==0)
*ptr = 0;
GLubyte b = *ptr;
//b = 2*b +  *src;
//b = 2*b + 1;
b = 2*b;
if(src[x])
b++;
*ptr = b;
if(++bit == 8) {
bit = 0;
ptr++;
}
}
src += WIDTHBYTES(g_iBmpWidth * 8);
}
DeleteDC(g_hMemDC);
#endif
return TRUE;
}*/

int GLFontBuffer::writeText(int x,int y,const char *text) const {
	return this->writeText(x, y, text, strlen(text));
}

int GLFontBuffer::writeText(int x,int y,const char *text,size_t length) const {
	int width_pixels = 0;
	for(size_t i=0;i<length;i++) {
		unsigned char ch = text[i];
		if( ch < START || ch >= N_CHARS )
			continue;
		int wid = width[ch-START];
		width_pixels += wid;
	}

#ifdef USE_WGL_FONTS
	int this_height = this->getHeight();

	glRasterPos2i(x,y + this_height/2);
	glPushAttrib(GL_LIST_BIT);
	glListBase(base-START);
	glCallLists((GLsizei)length,GL_UNSIGNED_BYTE,text);
	glPopAttrib();
#else
	//glPushAttrib(GL_ENABLE_BIT|GL_COLOR_BUFFER_BIT|GL_CURRENT_BIT|GL_TEXTURE_BIT|GL_TRANSFORM_BIT);
	glPushAttrib(GL_ENABLE_BIT|GL_COLOR_BUFFER_BIT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	//glLoadIdentity();
	//int this_height = this->getHeight();
	//glTranslated(x,y + this_height/2,0);
	glTranslated(x,y,0);
	for(int i=0;i<length;i++) {
		unsigned char ch = text[i];
		//ASSERT( ch >= START && ch < N_CHARS );
		if( ch < START || ch >= N_CHARS )
			continue;
		Texture *texture = textures[ch-START];
		int wid = width[ch-START];
		if( texture != NULL ) {
			int hgt = height[ch-START];
			texture->enable();
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			/*glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);*/
			//float tx_w = ((float)wid) / (float)texture->getWidth();
			//float tx_h = ((float)hgt) / (float)texture->getHeight();
			float tx_w = 1.0f;
			float tx_h = 1.0f;
			int x = offset_x[ch-START];
			int y = descent - offset_y[ch-START];
			//int y = font_height - descent - offset_y[ch-START];
			//int y = 0;
			//int y = - hgt;
			//int y = descent;
			//int y = - offset_y[ch-START];
			// n.b., freetype images are flipped in y coordinates, so we take that into account here with the v texcoords
			glBegin(GL_QUADS);
			glTexCoord2f(0.0,0.0);
			glVertex2d(x,y);
			glTexCoord2f(0.0,tx_h);
			glVertex2d(x,y+hgt);
			glTexCoord2f(tx_w,tx_h);
			glVertex2d(x+wid,y+hgt);
			glTexCoord2f(tx_w,0.0);
			glVertex2d(x+wid,y);
			glEnd();
		}
		glTranslated(advance_x[ch-START], 0, 0);
	}
	glPopMatrix();
	//glDisable(GL_TEXTURE_2D);
	//glDisable(GL_BLEND);
	glPopAttrib();
#endif

	return width_pixels;
}

int GLFontBuffer::getWidth(char letter) const {
	/*if( width == NULL )
		return 1;*/
	int index = getIndex(letter);
#ifdef USE_WGL_FONTS
	int w = width[index];
#else
	int w = advance_x[index];
#endif
	return w;
}

GLGraphics2D::GLGraphics2D(GraphicsEnvironment *genv) : Graphics2D(genv) {
}

/*void Graphics2D::setColor(Color *color) {
glFlush(); // just to be safe, see note below for Graphics2D:setColor(float,float,float)
//glColor3f(color->col[0],color->col[1],color->col[2]);
//glColor3ub(color->getR(),color->getG(),color->getB());
glColor4ub(color->getR(),color->getG(),color->getB(),color->getA());
//glColor3f(color->R,color->G,color->B);
}*/

void GLGraphics2D::setColor3i(unsigned char r, unsigned char g, unsigned char b) {
	glFlush(); // needed for listbox bug where first entry does not get highlighted when selected (at least on Intel GMA950)
	glColor3ub(r,g,b);
}

void GLGraphics2D::setColor4i(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	glFlush(); // just to be safe, see note above for Graphics2D:setColor(r,g,b)
	glColor4ub(r,g,b,a);
}

/*void GLGraphics2D::setColor(float r,float g,float b) const {
glFlush(); // needed for listbox bug where first entry does not get highlighted when selected (at least on Intel GMA950)
glColor3f(r,g,b);
}*/

void GLGraphics2D::plot(short x,short y) {
	glBegin(GL_POINTS);
	glVertex2s(x,y);
	glEnd();
	curr_x = x;
	curr_y = y;
}

void GLGraphics2D::draw(short x,short y) {
	/*glBegin(GL_LINES);
	glVertex2s(curr_x,curr_y);
	glVertex2s(x,y);
	glEnd();*/
	this->draw(curr_x, curr_y, x, y);
	curr_x = x;
	curr_y = y;
}

void GLGraphics2D::draw(short x1,short y1,short x2,short y2) const {
	glBegin(GL_LINES);
	glVertex2s(x1,y1);
	glVertex2s(x2,y2);
	glEnd();
}

void GLGraphics2D::drawRect(short x,short y,short width,short height) const {
	// width of 2 should give us 2 pixels, not 3 pixels
	if( width <= 0 ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "GLGraphics2D::drawRect width too small\n"));
	}
	if( height <= 0 ) {
		Vision::setError(new VisionException(this, VisionException::V_RENDER_ERROR, "GLGraphics2D::drawRect height too small\n"));
	}
	// need to offset coords by 0.5f for GL_LINE_LOOP - needed to be at correct location (and consistent with fillRect)
	/*glBegin(GL_LINE_LOOP);
	glVertex2f(x+0.5f,y+0.5f);
	glVertex2f(x+width-0.5f,y+0.5f);
	glVertex2f(x+width-0.5f,y+height-0.5f);
	glVertex2f(x+0.5f,y+height-0.5f);
	glEnd();*/
	// need to do as GL_LINES, as GL_LINE_LOOP (and GL_LINE_STRIP) leaves missing bottom-right corner pixel on Intel GMA 3150!
	// need to offset coords by 0.5f for GL_LINES - needed to be at correct location (and consistent with fillRect)
	int x2 = x + width;
	int y2 = y + height;
	glBegin(GL_LINES);

	glVertex2f(x+0.5f,y+0.5f);
	glVertex2f(x2+0.5f,y+0.5f);

	glVertex2f(x+0.5f,y2-0.5f);
	glVertex2f(x2+0.5f,y2-0.5f);

	glVertex2f(x+0.5f,y+1.5f);
	glVertex2f(x+0.5f,y2-0.5f);

	glVertex2f(x2-0.5f,y+1.5f);
	glVertex2f(x2-0.5f,y2-0.5f);

	glEnd();
}

void GLGraphics2D::fillRect(short x,short y,short width,short height,const float tu[4], const float tv[4]) const {
	if( texture != NULL ) {
		glEnable(GL_TEXTURE_2D);
		texture->enable();
	}
	glBegin(GL_QUADS);
	if( texture != NULL ) {
		//glTexCoord2s(0,1);
		glTexCoord2f(tu[0],tv[0]);
	}
	glVertex2s(x,y);
	if( texture != NULL ) {
		//glTexCoord2s(0,0);
		glTexCoord2f(tu[1],tv[1]);
	}
	glVertex2s(x,(short)(y+height));
	if( texture != NULL ) {
		//glTexCoord2s(1,0);
		glTexCoord2f(tu[3],tv[3]); // 3 not 2, because we're drawing as quads, not trianglestrip
	}
	glVertex2s((short)(x+width),(short)(y+height));
	if( texture != NULL ) {
		//glTexCoord2s(1,1);
		glTexCoord2f(tu[2],tv[2]);
	}
	glVertex2s((short)(x+width),y);
	glEnd();
	if( texture != NULL ) {
		glDisable(GL_TEXTURE_2D);
	}
}

void GLGraphics2D::fillTriangle(const short *x,const short *y) const {
	// n.b., doesn't support textures
	glBegin(GL_TRIANGLES);
	for(int i=0;i<3;i++) {
		glVertex2s(x[i],y[i]);
	}
	glEnd();
}

//GLRenderer::GLRenderer(Graphics3D *g3) : Renderer(g3) {
GLRenderer::GLRenderer(GraphicsEnvironment *genv) :
Renderer(genv), shader_clipping(false), saved_height(0),
enabled_normals(false), enabled_colors(false), enabled_texcoords0(false), enabled_texcoords1(false), enabled_texcoords2(false),
cgContext(NULL), cgVertexProfile(CG_PROFILE_UNKNOWN), cgPixelProfile(CG_PROFILE_UNKNOWN)
{
	//GLRenderer::GLRenderer() : Renderer() {
	LOG("Initialising OpenGL Renderer...\n");

	if( genv->wantShaders() ) {
		this->initCG();
	}
}

GLRenderer::~GLRenderer() {
	//freeHDRBuffers();
	//hdr_fbo.free();
	/*for(vector<Shader *>::iterator iter = shaders.begin(); iter != shaders.end(); ++iter) {
		Shader *shader = *iter;
		delete shader;
	}*/
	for(vector<ShaderEffect *>::iterator iter = shader_effects.begin(); iter != shader_effects.end(); ++iter) {
		ShaderEffect *shader_effect = *iter;
		delete shader_effect;
	}
	if( cgContext != NULL ) {
		cgDestroyContext(cgContext);
		cgContext = NULL;
	}
}

/*void GLRenderer::freeHDRBuffers() {
LOG("freeHDRBuffers()\n");
hdr_fbo.free();
LOG("    done\n");
}*/

void GLRenderer::cgErrorCallback(void) {
	CGerror error = cgGetError();
	LOG("CG ERROR %d : %s\n", error, cgGetErrorString(error));
	const char *lastError = cgGetLastErrorString(NULL);
	if( lastError != NULL ) {
		LOG("Last Error String: %s\n", lastError);
	}
	//SDL_GLGraphicsEnvironment *genv = static_cast<SDL_GLGraphicsEnvironment *>(GraphicsEnvironment::getSingleton());
	const GLRenderer *renderer = static_cast<const GLRenderer *>(GraphicsEnvironment::getSingleton()->getRenderer());
	const char *lastListing = cgGetLastListing(renderer->cgContext);
	if( lastListing != NULL ) {
		LOG("Last Listing String: %s\n", lastListing);
	}
	Vision::setError(new VisionException(NULL,VisionException::V_CG_ERROR,"CG reported an error"));
}

void GLRenderer::initCG() {
	// setup CG
	cgSetErrorCallback(cgErrorCallback);

	cgContext = cgCreateContext();
	if( cgContext != NULL ) {
		LOG("Successfully initialised CG Context\n");
		cgGLRegisterStates(cgContext); // needed for CGFX files

		cgVertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX);
		if( cgVertexProfile == CG_PROFILE_UNKNOWN ) {
			LOG("### CG ERROR: Invalid vertex profile type\n");
			cgDestroyContext(cgContext);
			cgContext = NULL;
		}
		else {
			LOG("Successfully obtained vertex profile\n");
#ifndef __linux
            // see comment below for why this is disabled on Linux
			cgGLSetOptimalOptions(cgVertexProfile);
#endif

			cgPixelProfile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
			if( cgPixelProfile == CG_PROFILE_UNKNOWN ) {
				LOG("### CG ERROR: Invalid pixel profile type\n");
				cgDestroyContext(cgContext);
				cgContext = NULL;
			}
			else {
				LOG("Successfully obtained pixel profile\n");
#ifndef __linux
                /* Cg Toolkit 2.1 has a bug where cgGLSetOptimalOptions results in incorrect options being set, so that the shaders don't load. On Windows, we now compile with later versions,
                 * and similarly the Linux binaries use later versions, but we disable this for now just to be safe, for anyone compiling on anything before Ubuntu 10.10 (maverick), which doesn't
                 * support newer versions of Cg.
                 * Only affects the pixel shaders at the time of writing, but to be safe, we don't call this for vertex shaders either on Linux.
                 */
				cgGLSetOptimalOptions(cgPixelProfile);
#endif
			}
		}
	}
	else {
		LOG("### Failed to initialise CG Context\n");
	}

	//cgGLSetDebugMode(CG_FALSE);
}

Shader *GLRenderer::createShaderCG(const char *file, const char *func, bool vertex_shader) {
	if( !this->hasShaders() ) {
		Vision::setError(new VisionException(NULL,VisionException::V_CG_ERROR, "Shaders not available"));
		return NULL;
	}
	CGGLShader *shader = new CGGLShader(this, vertex_shader);
	LOG("Loading %s shader %d : %s / %s\n", vertex_shader?"vertex":"pixel", shader, file, func);
	shader->cgProfile = vertex_shader ? this->cgVertexProfile : this->cgPixelProfile;

	shader->cgProgram = cgCreateProgramFromFile(this->cgContext, CG_SOURCE, file, shader->cgProfile, func, 0);
	if( shader->cgProgram == NULL ) {
		LOG("### Failed to load %s shader:\n", vertex_shader?"vertex":"pixel");
		CGerror Error = cgGetError();
		LOG(cgGetErrorString(Error));
		LOG("\n");
		delete shader;
		Vision::setError(new VisionException(NULL,VisionException::V_CG_ERROR, "Failed to load CG program"));
		return NULL;
	}
	cgGLLoadProgram(shader->cgProgram);
	//shader->init();
	return shader;
}

ShaderEffect *GLRenderer::createShaderEffectCGFX(const char *file,const char **args) {
	if( !this->hasShaders() ) {
		Vision::setError(new VisionException(this,VisionException::V_CG_ERROR, "Shaders not available"));
		return NULL;
	}
	CGFXShaderEffect *shader_effect = new CGFXGLShaderEffect(this, this->cgContext, file, args);
	LOG("Loading CGFX shader %d : %s\n", shader_effect, file);
	this->addShaderEffect(shader_effect);
	return shader_effect;
}

TransformationMatrix *GLRenderer::createTransformationMatrix() {
	return new GLTransformationMatrix();
}

Texture *GLRenderer::loadTexture(const char *filename) {
	return GLTexture::loadTexture(this, filename);
}

Texture *GLRenderer::loadTextureWithMask(const char *filename, unsigned char maskcol[3]) {
	return GLTexture::loadTextureWithMask(this, filename, maskcol);
}

VertexArray *GLRenderer::createVertexArray(VertexArray::DataType data_type, int tex_unit, bool stream, void *data, int size, int dim) {
	return new GLVertexArray(data_type, tex_unit, stream, data, size, dim);
}

void GLRenderer::resizeScene(float frustum[6][4], int width, int height, bool persp, float z_near, float z_far, float fovy) {
	this->z_near = z_near;
	this->z_far = z_far;
	this->persp = persp;
	this->saved_height = height;
	setViewport(width, height);
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glPopMatrix();
	glLoadIdentity();							// Reset The Projection Matrix

	if( persp ) {
		gluPerspective(fovy,(GLfloat)width/(GLfloat)height,z_near,z_far);
	}
	else {
		//gluOrtho2D(-100.0, 100.0, -100.0, 100.0);
		glOrtho(-8.0, 8.0, -8.0, 8.0, z_near, z_far);
	}
	glMatrixMode(GL_MODELVIEW);						// Select The Modelview Matrix
	glLoadIdentity();							// Reset The Modelview Matrix
	extractFrustum(frustum);

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glPushMatrix();
	/*glLoadIdentity();
	gluOrtho2D(0.0,(GLfloat)width,0.0,(GLfloat)height);*/
	this->ortho2D(width, height);
	//glGetFloatv( GL_PROJECTION_MATRIX, proj_matrix );
	glMatrixMode(GL_MODELVIEW); // set to model view by default
}

/* Performs some calculations, so later on we can quickly determine whether
* objects/points/etc lie within the view frustum or not.
*/
void GLRenderer::extractFrustum(float frustum[6][4]) const {

	double proj[16];
	double modl[16];
	double clip[16];
	float t = 0.0f;

	glGetDoublev( GL_PROJECTION_MATRIX, proj );

	glGetDoublev( GL_MODELVIEW_MATRIX, modl );

	/* Combine the two matrices (multiply projection by modelview) */
	clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
	clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
	clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
	clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

	clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
	clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
	clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
	clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

	clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
	clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
	clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
	clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

	clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
	clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
	clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
	clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

	/* Extract the numbers for the RIGHT plane */
	frustum[0][0] = (float)(clip[ 3] - clip[ 0]);
	frustum[0][1] = (float)(clip[ 7] - clip[ 4]);
	frustum[0][2] = (float)(clip[11] - clip[ 8]);
	frustum[0][3] = (float)(clip[15] - clip[12]);
	/* Normalize the result */
	t = sqrt( frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2] );
	frustum[0][0] /= t;
	frustum[0][1] /= t;
	frustum[0][2] /= t;
	frustum[0][3] /= t;
	LOG("FRUSTUM RIGHT %f %f %f %f\n", frustum[0][0],  frustum[0][1], frustum[0][2], frustum[0][3]);

	/* Extract the numbers for the LEFT plane */
	frustum[1][0] = (float)(clip[ 3] + clip[ 0]);
	frustum[1][1] = (float)(clip[ 7] + clip[ 4]);
	frustum[1][2] = (float)(clip[11] + clip[ 8]);
	frustum[1][3] = (float)(clip[15] + clip[12]);
	/* Normalize the result */
	t = sqrt( frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2] );
	frustum[1][0] /= t;
	frustum[1][1] /= t;
	frustum[1][2] /= t;
	frustum[1][3] /= t;
	LOG("FRUSTUM LEFT %f %f %f %f\n", frustum[1][0],  frustum[1][1], frustum[1][2], frustum[1][3]);

	/* Extract the BOTTOM plane */
	frustum[2][0] = (float)(clip[ 3] + clip[ 1]);
	frustum[2][1] = (float)(clip[ 7] + clip[ 5]);
	frustum[2][2] = (float)(clip[11] + clip[ 9]);
	frustum[2][3] = (float)(clip[15] + clip[13]);
	/* Normalize the result */
	t = sqrt( frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2] );
	frustum[2][0] /= t;
	frustum[2][1] /= t;
	frustum[2][2] /= t;
	frustum[2][3] /= t;
	LOG("FRUSTUM BOTTOM %f %f %f %f\n", frustum[2][0],  frustum[2][1], frustum[2][2], frustum[2][3]);

	/* Extract the TOP plane */
	frustum[3][0] = (float)(clip[ 3] - clip[ 1]);
	frustum[3][1] = (float)(clip[ 7] - clip[ 5]);
	frustum[3][2] = (float)(clip[11] - clip[ 9]);
	frustum[3][3] = (float)(clip[15] - clip[13]);
	/* Normalize the result */
	t = sqrt( frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2] );
	frustum[3][0] /= t;
	frustum[3][1] /= t;
	frustum[3][2] /= t;
	frustum[3][3] /= t;
	LOG("FRUSTUM TOP %f %f %f %f\n", frustum[3][0],  frustum[3][1], frustum[3][2], frustum[3][3]);

	/* Extract the FAR plane */
	frustum[4][0] = (float)(clip[ 3] - clip[ 2]);
	frustum[4][1] = (float)(clip[ 7] - clip[ 6]);
	frustum[4][2] = (float)(clip[11] - clip[10]);
	frustum[4][3] = (float)(clip[15] - clip[14]);
	/* Normalize the result */
	t = sqrt( frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2] );
	frustum[4][0] /= t;
	frustum[4][1] /= t;
	frustum[4][2] /= t;
	frustum[4][3] /= t;
	LOG("FRUSTUM FAR %f %f %f %f\n", frustum[4][0],  frustum[4][1], frustum[4][2], frustum[4][3]);

	/* Extract the NEAR plane */
	frustum[5][0] = (float)(clip[ 3] + clip[ 2]);
	frustum[5][1] = (float)(clip[ 7] + clip[ 6]);
	frustum[5][2] = (float)(clip[11] + clip[10]);
	frustum[5][3] = (float)(clip[15] + clip[14]);
	/* Normalize the result */
	t = sqrt( frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2] );
	frustum[5][0] /= t;
	frustum[5][1] /= t;
	frustum[5][2] /= t;
	frustum[5][3] /= t;
	LOG("FRUSTUM NEAR %f %f %f %f\n", frustum[5][0],  frustum[5][1], frustum[5][2], frustum[5][3]);

	/*Vector3D n_l( frustum[1][0], frustum[1][1], frustum[1][2] );
	Vector3D n_r( frustum[0][0], frustum[0][1], frustum[0][2] );
	Vector3D up = n_l ^ n_r;
	up.normalise();
	Vector3D dir_left = up ^ n_l;*/

	/*double x, y, z;
	int viewport[] = { 0, 0, this->width-1, this->height-1 };

	gluUnProject(0.0, ((float)this->height-1.0) / 2.0, 1.0, modl, proj, viewport, &x, &y, &z);
	dir_left.set(x,y,z);
	dir_left.normalise();

	gluUnProject(this->width-1.0, ((float)this->height-1.0) / 2.0, 1.0, modl, proj, viewport, &x, &y, &z);
	dir_right.set(x,y,z);
	dir_right.normalise();

	gluUnProject(((float)this->width-1.0) / 2.0, this->height-1.0, 1.0, modl, proj, viewport, &x, &y, &z);
	dir_up.set(x,y,z);
	dir_up.normalise();

	gluUnProject(((float)this->width-1.0) / 2.0, 0.0, 1.0, modl, proj, viewport, &x, &y, &z);
	dir_down.set(x,y,z);
	dir_down.normalise();*/
}

void GLRenderer::setToDefaults() {
	// set up defaults
	glCullFace(GL_BACK);
	_DEBUG_CHECK_ERROR_;
	glEnable(GL_CULL_FACE);
	_DEBUG_CHECK_ERROR_;
	//glDisable(GL_DITHER);
	glEnable(GL_TEXTURE_2D);
	//glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
	glShadeModel(GL_SMOOTH);						// Enable Smooth Shading
	_DEBUG_CHECK_ERROR_;
	glEnable(GL_COLOR_MATERIAL);
	_DEBUG_CHECK_ERROR_;
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	_DEBUG_CHECK_ERROR_;
	glMaterialf(GL_FRONT, GL_SHININESS, 64.0);
	_DEBUG_CHECK_ERROR_;
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	_DEBUG_CHECK_ERROR_;
	if( GLRendererInfo::hasVersion(1, 2) ) {
		glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR); // see http://math.hws.edu/graphicsnotes/c4/s3.html - requires OpenGL 1.2
	}
	_DEBUG_CHECK_ERROR_;

	glEnable(GL_LIGHTING);
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);					// Black Background
	glClearDepth(1.0f);							// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);						// Enables Depth Testing
	//glDepthFunc(GL_LEQUAL);
	glDepthFunc(GL_LESS);

	glFrontFace(GL_CCW);
	//glEnable(GL_COLOR_MATERIAL);
	glBlendFunc(GL_ONE, GL_ZERO);

	GLenum hints = GL_NICEST;
	//GLenum hints = GL_FASTEST;
	glHint(GL_POLYGON_SMOOTH_HINT, hints);
	glHint(GL_FOG_HINT, hints);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, hints);
	_DEBUG_CHECK_ERROR_;

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);

	//glEnable(GL_NORMALIZE);
}

void GLRenderer::setDepthBufferMode(RenderState state) {
	if( state == RENDERSTATE_DEPTH_READWRITE ) {
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
	}
	else if( state == RENDERSTATE_DEPTH_READONLY ) {
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
	}
	else if( state == RENDERSTATE_DEPTH_NONE ) {
		glDisable(GL_DEPTH_TEST);
		//glDepthMask(GL_FALSE);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setDepthBufferMode"));
	}
}

void GLRenderer::setDepthBufferFunc(RenderState state) {
	if( state == RENDERSTATE_DEPTHFUNC_EQUAL ) {
		glDepthFunc(GL_EQUAL);
	}
	else if( state == RENDERSTATE_DEPTHFUNC_LESS ) {
		glDepthFunc(GL_LESS);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setDepthBufferFunc"));
	}
}

void GLRenderer::setBlendMode(RenderState state) {
	if( state == RENDERSTATE_BLEND_BOTH ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
	}
	else if( state == RENDERSTATE_BLEND_ADDITIVE ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	}
	else if( state == RENDERSTATE_BLEND_TRANSPARENCY ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else if( state == RENDERSTATE_BLEND_REVERSE_TRANSPARENCY ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);
	}
	else if( state == RENDERSTATE_BLEND_ONE_BY_SRCALPHA ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_SRC_ALPHA);
	}
	else if( state == RENDERSTATE_BLEND_ONE_BY_ONEMINUSSRCALPHA ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	}
	else if( state == RENDERSTATE_BLEND_DST_BY_ZERO ) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_DST_COLOR, GL_ZERO);
	}
	else if( state == RENDERSTATE_BLEND_NONE ) {
		glDisable(GL_BLEND);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setBlendMode"));
	}
}

void GLRenderer::setAlphaTestMode(RenderState state) {
	if( state == RENDERSTATE_ALPHA_TEST_ON ) {
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.5);
	}
	else if( state == RENDERSTATE_ALPHA_TEST_OFF ) {
		glDisable(GL_ALPHA_TEST);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setAlphaTestMode"));
	}
}

void GLRenderer::setStencilMode(RenderState state) {
	if( state == RENDERSTATE_STENCIL_REPLACE ) {
		glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
		glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
	}
	else if( state == RENDERSTATE_STENCIL_DRAWATSTENCIL_OVERDRAW ) {
		glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_EQUAL, 1, 0xFFFFFFFF);
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	}
	else if( state == RENDERSTATE_STENCIL_DRAWATNOSTENCIL ) {
		glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_EQUAL, 0, 0xFFFFFFFF);
		glStencilOp(GL_KEEP, GL_DECR, GL_DECR);
	}
	else if( state == RENDERSTATE_STENCIL_SHADOWPASS_ONE ) {
		glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
		if( GLRendererInfo::separateStencilATI ) {
			_DEBUG_CHECK_ERROR_
			glEnable(GL_STENCIL_TEST_TWO_SIDE_EXT);
			_DEBUG_CHECK_ERROR_
			glDisable(GL_CULL_FACE);
			glStencilOpSeparateATI(GL_FRONT, GL_KEEP, GL_KEEP, GL_INCR_WRAP_EXT);
			glStencilOpSeparateATI(GL_BACK, GL_KEEP, GL_KEEP, GL_DECR_WRAP_EXT);
		}
		else if( GLRendererInfo::separateStencil ) {
			glEnable(GL_STENCIL_TEST_TWO_SIDE_EXT);
			glDisable(GL_CULL_FACE);
			glActiveStencilFaceEXT(GL_BACK);
			glStencilOp(GL_KEEP, GL_KEEP, GL_DECR_WRAP_EXT);
			glActiveStencilFaceEXT(GL_FRONT); // this needs to be done second, for some reason?
			glStencilOp(GL_KEEP, GL_KEEP, GL_INCR_WRAP_EXT);
		}
		else {
			glStencilOp( GL_KEEP, GL_KEEP, GL_INCR );
		}
	}
	else if( state == RENDERSTATE_STENCIL_SHADOWPASS_TWO ) {
		// assumes stencil already enabled and with func set
		glStencilOp( GL_KEEP, GL_KEEP, GL_DECR );
	}
	else if( state == RENDERSTATE_STENCIL_SHADOWPASS_OFF ) {
		glDisable(GL_STENCIL_TEST_TWO_SIDE_EXT);
		glEnable(GL_CULL_FACE);
	}
	else if( state == RENDERSTATE_STENCIL_NONE ) {
		glDisable(GL_STENCIL_TEST);
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_INVALID_STATE, "Invalid render state for setBlendMode"));
	}
}

void GLRenderer::setFogMode(RenderState state, float start, float end, float density) {
	if( state == RENDERSTATE_FOG_LINEAR ) {
		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, GL_LINEAR);
		glFogf(GL_FOG_START,start);
		glFogf(GL_FOG_END,end);
	}
	else if( state == RENDERSTATE_FOG_EXP ) {
		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, GL_EXP);
		glFogf(GL_FOG_DENSITY,density);
	}
	else if( state == RENDERSTATE_FOG_EXP2 ) {
		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, GL_EXP2);
		glFogf(GL_FOG_DENSITY,density);
	}
	else if( state == RENDERSTATE_FOG_NONE ) {
		glDisable(GL_FOG);
	}
}

void GLRenderer::setFogColor(const float color[4]) {
	glFogfv(GL_FOG_COLOR, color);
}

int GLRenderer::nShadowPasses() const {
	if( GLRendererInfo::separateStencilATI || GLRendererInfo::separateStencil ) {
		return 1;
	}
	return 2;
}

void GLRenderer::enableMultisample(bool enable) {
	// n.b., has no effect on ATI cards
	if( this->genv->getMultisample() > 0 ) {
		enable ? glEnable(GL_MULTISAMPLE_ARB) : glDisable(GL_MULTISAMPLE_ARB);
	}
}

void GLRenderer::enableClipPlane(const double pl[4]) {
	glClipPlane(GL_CLIP_PLANE0, pl);
	glEnable(GL_CLIP_PLANE0);

	if( !genv->hasShaders() ) {
		// no shaders
		return;
	}

	// user clip planes seem to be supported for fixed pipeline, but not always
	// when using shaders (e.g., NVIDIA 8600GT, Intel GMA950).
	// we need to set with _both_ methods, because a scene can mix shaders and non-shaders!

	TransformationMatrix *model_matrix = this->createTransformationMatrix();

	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate((float)(pl[3]*pl[0]), (float)(pl[3]*pl[1]), (float)(pl[3]*pl[2]));
	model_matrix->save();
	genv->getRenderer()->popMatrix();
	Vector3D new_p = model_matrix->getPos();

	genv->getRenderer()->pushMatrix();
	genv->getRenderer()->translate((float)pl[0], (float)pl[1], (float)pl[2]);
	model_matrix->save();
	genv->getRenderer()->popMatrix();
	Vector3D new_n = model_matrix->getPos();
	model_matrix->save();
	new_n.x -= (*model_matrix)[12];
	new_n.y -= (*model_matrix)[13];
	new_n.z -= (*model_matrix)[14];
	assert( new_n.magnitude() > 0 );
	new_n.normalise();
	delete model_matrix;

	{
		// shader clipping
		this->shader_clipping = true;
		this->shader_clipping_plane[0] = new_n.x;
		this->shader_clipping_plane[1] = new_n.y;
		this->shader_clipping_plane[2] = new_n.z;
		this->shader_clipping_plane[3] = new_n % new_p;
		return;
	}

	// oblique code

	/*float matrix[16];
	// Grab the current projection matrix from OpenGL
	glGetFloatv(GL_PROJECTION_MATRIX, matrix);

	pl[0] = new_n.x;
	pl[1] = new_n.y;
	pl[2] = new_n.z;
	pl[3] = new_n % new_p;
	//pl[3] = 2;

	//pl[3] = 10;
	//assert( pl[3] <= 0 );
	//glGetFloatv(GL_MODELVIEW_INVERSE_MATRIX, model_matrix);

	// Calculate the clip-space corner point opposite the clipping plane
	// as (sgn(clipPlane.x), sgn(clipPlane.y), 1, 1) and
	// transform it into camera space by multiplying it
	// by the inverse of the projection matrix
	float q_x = (sgn((float)pl[0]) + matrix[8]) / matrix[0];
	float q_y = (sgn((float)pl[1]) + matrix[9]) / matrix[5];
	float q_z = -1.0f;
	float q_w = (1.0f + matrix[10]) / matrix[14];

	// Calculate the scaled plane vector
	//Vector4D c = clipPlane * (2.0F / Dot(clipPlane, q));
	float dot = q_x * (float)pl[0] + q_y * (float)pl[1] + q_z * (float)pl[2] + q_w * (float)pl[3];
	float scale = 2.0f / dot;

	//scale *= -5.0;
	//scale = 100;
	// Replace the third row of the projection matrix
	matrix[2] = (float)pl[0] * scale;
	matrix[6] = (float)pl[1] * scale;
	matrix[10] = (float)pl[2] * scale + 1.0f;
	//matrix[10] = pl[2] * scale - 1.0F;
	//matrix[10] = 0;
	//matrix[10] = pl[2] * scale;
	matrix[14] = (float)pl[3] * scale;

	//matrix[2] = matrix[10] = 0;
	//matrix[14] = - matrix[6];

	// Load it back into OpenGL
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadMatrixf(matrix);
	glMatrixMode(GL_MODELVIEW);
	//trace_extractFrustum();
	*/
}

void GLRenderer::reenableClipPlane() {
	glEnable(GL_CLIP_PLANE0);
	if( genv->hasShaders() ) {
		this->shader_clipping = true;
	}
}

void GLRenderer::pushAttrib(bool viewport) {
	GLbitfield bits = 0;
	if( viewport )
		bits |= GL_VIEWPORT_BIT;
	glPushAttrib(bits);
}

void GLRenderer::popAttrib() const {
	glPopAttrib();
}

void GLRenderer::setFixedFunctionMatSpecular(const float specular[4]) {
	//return;
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	/*float arr[4] = {1, 0, 0, 1};
	glMaterialfv(GL_FRONT, GL_SPECULAR, arr);*/
	/*CGGLShader *c_vertex_shader = static_cast<CGGLShader *>(g3->c_vertex_shader);
	CGGLShader *c_pixel_shader = static_cast<CGGLShader *>(g3->c_pixel_shader);
	if( c_vertex_shader != NULL && c_vertex_shader->cgparam_SpecMat != 0 ) {
	cgGLSetParameter4fv(c_vertex_shader->cgparam_SpecMat, specular);
	}
	if( c_pixel_shader != NULL && c_pixel_shader->cgparam_SpecMat != 0 ) {
	cgGLSetParameter4fv(c_pixel_shader->cgparam_SpecMat, specular);
	}*/
	/*if( c_vertex_shader != NULL )
		c_vertex_shader->setMatSpecular(specular);
	if( c_pixel_shader != NULL )
		c_pixel_shader->setMatSpecular(specular);*/
}

void GLRenderer::enableFixedFunctionLight(unsigned int index, const float ambient[4], const float diffuse[4], const float specular[4], const float attenuation[3], const float position[3], bool directional) {
	ASSERT( index >= 0 && index < RendererInfo::maxLights );
	/*if( index >= RendererInfo::maxLights )
		return; // TODO: too many lights - error?*/
	GLenum gl_light = GL_LIGHT0 + (GLenum)index;
	float f[4];

	// attenuation
	/*glLightf(gl_light, GL_CONSTANT_ATTENUATION, light->getLightAttenuation(0));
	glLightf(gl_light, GL_LINEAR_ATTENUATION, light->getLightAttenuation(1));
	glLightf(gl_light, GL_QUADRATIC_ATTENUATION, light->getLightAttenuation(2));*/
	glLightf(gl_light, GL_CONSTANT_ATTENUATION, attenuation[0]);
	glLightf(gl_light, GL_LINEAR_ATTENUATION, attenuation[1]);
	glLightf(gl_light, GL_QUADRATIC_ATTENUATION, attenuation[2]);

	// ambient
	//light->ambient.floatArray(f);
	/*light->getLightAmbient().floatArray(f);
	glLightfv(gl_light, GL_AMBIENT, f);*/
	glLightfv(gl_light, GL_AMBIENT, ambient);

	// diffuse
	//light->diffuse.floatArray(f);
	/*light->getLightDiffuse().floatArray(f);
	glLightfv(gl_light, GL_DIFFUSE, f);*/
	glLightfv(gl_light, GL_DIFFUSE, diffuse);
	//LOG("%d : %f, %f, %f, %f\n", index, f[0], f[1], f[2], f[3]);

	// specular
	//light->specular.floatArray(f);
	/*light->getLightSpecular().floatArray(f);
	glLightfv(gl_light, GL_SPECULAR, f);*/
	glLightfv(gl_light, GL_SPECULAR, specular);

	// position
	for(int i=0;i<3;i++)
		f[i] = position[i];
	f[3] = directional ? 0.0f : 1.0f;
	glLightfv(gl_light, GL_POSITION, f);

	glEnable(gl_light);
}

void GLRenderer::disableFixedFunctionLights() {
	for(unsigned int i=GL_LIGHT0;i<GL_LIGHT0+RendererInfo::maxLights;i++)
		glDisable((GLenum)i);
}

/*void GLRenderer::enableVertexArrays(bool enable, bool vertices, bool normals, bool texcoords, bool colors) const {
if( enable ) {
if( vertices )
glEnableClientState(GL_VERTEX_ARRAY);
if( normals )
glEnableClientState(GL_NORMAL_ARRAY);
if( texcoords )
glEnableClientState(GL_TEXTURE_COORD_ARRAY);
if( colors )
glEnableClientState(GL_COLOR_ARRAY);
}
else {
if( vertices )
glDisableClientState(GL_VERTEX_ARRAY);
if( normals )
glDisableClientState(GL_NORMAL_ARRAY);
if( texcoords )
glDisableClientState(GL_TEXTURE_COORD_ARRAY);
if( colors )
glDisableClientState(GL_COLOR_ARRAY);
}
}*/

void GLRenderer::render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count) {
	// TODO: do with Vertex Arrays via VertexArray functions? (no point using VBOs though!)
	// TODO: check mode
	glColor4i(255, 255, 255, 255);
	GLenum gl_mode = GLModes[mode];
	glBegin(gl_mode);
	for(int i=0;i<count;i++) {
		if( texcoord_data != NULL ) {
			glTexCoord2f(texcoord_data[2*i], texcoord_data[2*i+1]);
		}
		glVertex3f(vertex_data[3*i], vertex_data[3*i+1], vertex_data[3*i+2]);
	}
	glEnd();
}

void GLRenderer::draw(const Vector3D *p0, const Vector3D *p1, const unsigned char rgba[4]) {
	glColor4ubv(rgba);
	glBegin(GL_LINES);
	glVertex3f(p0->x, p0->y, p0->z);
	glVertex3f(p1->x, p1->y, p1->z);
	glEnd();
}

void GLRenderer::render(VertexArray::DrawingMode mode, const float *vertex_data, const float *texcoord_data, int count, const unsigned char *color_data, bool alpha, int color_repeat) {
	// TODO: do with Vertex Arrays via VertexArray functions? (no point using VBOs though!)
	// TODO: check mode
	glColor4i(255, 255, 255, 255);
	GLenum gl_mode = GLModes[mode];
	glBegin(gl_mode);
	const unsigned char *color_data_ptr = color_data;
	for(int i=0;i<count;i++) {
		if( texcoord_data != NULL ) {
			glTexCoord2f(texcoord_data[2*i], texcoord_data[2*i+1]);
		}
		if( color_data != NULL && i % color_repeat == 0 ) {
			if( alpha ) {
				glColor4ubv(color_data_ptr);
				color_data_ptr += 4;
			}
			else {
				glColor3ubv(color_data_ptr);
				color_data_ptr += 3;
			}
		}
		glVertex3f(vertex_data[3*i], vertex_data[3*i+1], vertex_data[3*i+2]);
	}
	glEnd();
}

//void GLRenderer::startArrays(VertexFormat vertexFormat, bool material_color, bool have_texcoords0, bool have_texcoords1) {
void GLRenderer::startArrays(VertexFormat vertexFormat) {
	enabled_normals = false;
	enabled_colors = false;
	enabled_texcoords0 = false;
	enabled_texcoords1 = false;
	enabled_texcoords2 = false;

	glEnableClientState(GL_VERTEX_ARRAY);

	/*if( !have_texcoords0 ) {
		glTexCoord2f(0.0f, 0.0f);
	}
	if( !have_texcoords1 ) {
		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glTexCoord2f(0.0f, 0.0f);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);
	}*/

	if( vertexFormat == VERTEXFORMAT_P ) {
	}
	else if( vertexFormat == VERTEXFORMAT_PT0C ) {
		//if( !material_color )
		{
			glEnableClientState(GL_COLOR_ARRAY);
			enabled_colors = true;
		}

		//if( have_texcoords0 )
		{
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			enabled_texcoords0 = true;
		}
	}
	else if( vertexFormat == VERTEXFORMAT_PNT0C ) {
		//if( !material_color )
		{
			glEnableClientState(GL_COLOR_ARRAY);
			enabled_colors = true;
		}
		glEnableClientState(GL_NORMAL_ARRAY);
		enabled_normals = true;

		//if( have_texcoords0 )
		{
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			enabled_texcoords0 = true;
		}
	}
	else if( vertexFormat == VERTEXFORMAT_PT0T1C ) {
		//if( !material_color )
		{
			glEnableClientState(GL_COLOR_ARRAY);
			enabled_colors = true;
		}

		//if( have_texcoords0 )
		{
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			enabled_texcoords0 = true;
		}
		//if( have_texcoords1 )
		{
			glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glClientActiveTextureARB(GL_TEXTURE0_ARB);
			enabled_texcoords1 = true;
		}
	}
	else if( vertexFormat == VERTEXFORMAT_PNT0T1C ) {
		//if( !material_color )
		{
			glEnableClientState(GL_COLOR_ARRAY);
			enabled_colors = true;
		}
		glEnableClientState(GL_NORMAL_ARRAY);
		enabled_normals = true;

		//if( have_texcoords0 )
		{
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			enabled_texcoords0 = true;
		}
		//if( have_texcoords1 )
		{
			glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glClientActiveTextureARB(GL_TEXTURE0_ARB);
			enabled_texcoords1 = true;
		}
	}
	else if( vertexFormat == VERTEXFORMAT_PNT0T1T2C ) {
		//if( !material_color )
		{
			glEnableClientState(GL_COLOR_ARRAY);
			enabled_colors = true;
		}
		glEnableClientState(GL_NORMAL_ARRAY);
		enabled_normals = true;

		//if( have_texcoords0 )
		{
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			enabled_texcoords0 = true;
		}
		//if( have_texcoords1 )
		{
			glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glClientActiveTextureARB(GL_TEXTURE0_ARB);
			enabled_texcoords1 = true;
		}
		glClientActiveTextureARB(GL_TEXTURE2_ARB);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);
		enabled_texcoords2 = true;
	}
	else {
		Vision::setError(new VisionException(this, VisionException::V_RENDERER_NOT_SUPPORTED, "GLRenderer::startArrays options not supported\n"));
	}
}

void GLRenderer::endArrays() {
	glDisableClientState(GL_VERTEX_ARRAY);
	if( enabled_normals )
		glDisableClientState(GL_NORMAL_ARRAY);
	if( enabled_colors )
		glDisableClientState(GL_COLOR_ARRAY);
	if( enabled_texcoords0 )
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	if( enabled_texcoords1 ) {
		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);
	}
	if( enabled_texcoords2 ) {
		glClientActiveTextureARB(GL_TEXTURE2_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);
	}
	//if( enabled_vbo_array )
	if( GLRendererInfo::vertexBufferObjects ) {
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
	}
}

/*void GLRenderer::endArraysMulti(bool vbos) {
	glDisableClientState(GL_VERTEX_ARRAY);
	//if( normals )
		glDisableClientState(GL_NORMAL_ARRAY);
	for(int i=0;i<MAX_TEXUNITS;i++) {
		//if( texcoords[i] )
		{
			GLenum gl_unit = GLUnits[i];
			glClientActiveTextureARB(gl_unit);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		}
	}
	GLenum gl_unit0 = GLUnits[0];
	glClientActiveTextureARB(gl_unit0);
	//if( colors )
		glDisableClientState(GL_COLOR_ARRAY);
	if( vbos )
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
}*/

void GLRenderer::activeTextureUnit(int unit) {
	// TODO: check unit
	GLenum gl_unit = GLUnits[unit];
	glActiveTextureARB(gl_unit);
}

/*void GLRenderer::activeClientTextureUnit(int unit) const {
// TODO: check unit
GLenum gl_unit = GLUnits[unit];
glClientActiveTextureARB(gl_unit);
}*/

void GLRenderer::ortho2D(int width, int height) {
	glLoadIdentity();
	gluOrtho2D(0.0,(GLfloat)width,(GLfloat)height,0.0);
}

void GLRenderer::transform(const Vector3D *pos, const Quaternion *rot, bool infinite) {
	GLfloat glmat[16];
	rot->convertToGLMatrix(glmat);
	glmat[12] = pos->x;
	glmat[13] = pos->y;
	glmat[14] = pos->z;
	if( infinite ) {
		glmat[15] = 0.0;
	}
	glMultMatrixf(glmat);
}

void GLRenderer::transform(const Quaternion *rot) {
	GLfloat glmat[16];
	rot->convertToGLMatrix(glmat);
	glMultMatrixf(glmat);
}

void GLRenderer::transformInverse(const Quaternion *rot) {
	GLfloat glmat[16];
	rot->convertInverseToGLMatrix(glmat);
	glMultMatrixf(glmat);
}

void GLRenderer::getViewOrientation(Vector3D *right,Vector3D *up) {
	// TODO: optimise so we don't have to allocate a matrix? Just call the matrix->save OpenGL code directly
	TransformationMatrix *mat = this->createTransformationMatrix();
	mat->save();
	right->set((*mat)[0],(*mat)[4],(*mat)[8]);
	up->set((*mat)[1],(*mat)[5],(*mat)[9]);
	delete mat;
}

void GLRenderer::getViewDirection(Vector3D *dir) {
	// TODO: optimise so we don't have to allocate a matrix? Just call the matrix->save OpenGL code directly
	TransformationMatrix *mat = this->createTransformationMatrix();
	mat->save();
	dir->set((*mat)[2],(*mat)[6],(*mat)[10]);
	delete mat;
}

void GLRenderer::unbindFramebuffer() const {
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

/*void Renderer::setParameter4f(CGparameter parameter, float p1, float p2, float p3, float p4) {
cgGLSetParameter4f( parameter, p1, p2, p3, p4 );
}*/

void GLRenderer::disableShaders() {
	cgGLDisableProfile(cgVertexProfile);
	cgGLDisableProfile(cgPixelProfile);
	this->setActiveShader(NULL);
}
