//---------------------------------------------------------------------------
#include "SDL_GraphicsEnvironment.h"
#include "InputHandler.h"
#include "Panel.h"

#ifdef _WIN32
#include <SDL.h>
#include <SDL_syswm.h>
#endif

#ifdef __linux
#include <SDL/SDL.h>
#include <SDL/SDL_syswm.h>

/*#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/CascadeB.h>
#include <Xm/MessageB.h>
#include <Xm/PushB.h>*/
#endif

//---------------------------------------------------------------------------

SDL_GraphicsEnvironment::SDL_GraphicsEnvironment(VisionPrefs visionPrefs) : GraphicsEnvironment(visionPrefs) {
	this->active = true; // SDL starts off as active by default, so this is needed, otherwise we don't recent an SDL message saying it's been activated the first time
#ifdef _WIN32
	this->hWnd = NULL;
#endif
#ifdef __linux
	this->x_display = NULL;
#endif
}

SDL_GraphicsEnvironment::~SDL_GraphicsEnvironment() {
}

// n.b., global function, to avoid including SDL.h in the header file
int translateKey(SDLKey key) {
	int v_key = -1;
	// hacky, but needed for now to be compatible with non-SDL code, and to avoid everywhere having to include SDL
	switch( key ) {
		case SDLK_ESCAPE: v_key = V_ESCAPE; break;
		// function keys
		case SDLK_F1: v_key = V_F1; break;
		case SDLK_F2: v_key = V_F2; break;
		case SDLK_F3: v_key = V_F3; break;
		case SDLK_F4: v_key = V_F4; break;
		case SDLK_F5: v_key = V_F5; break;
		case SDLK_F6: v_key = V_F6; break;
		case SDLK_F7: v_key = V_F7; break;
		case SDLK_F8: v_key = V_F8; break;
		case SDLK_F9: v_key = V_F9; break;
		case SDLK_F10: v_key = V_F10; break;
		case SDLK_F11: v_key = V_F11; break;
		case SDLK_F12: v_key = V_F12; break;
		// left side special keys
		case SDLK_BACKQUOTE: v_key = V_BACKQUOTE; break;
		case SDLK_TAB: v_key = V_TAB; break;
		case SDLK_CAPSLOCK: v_key = V_CAPSLOCK; break;
		case SDLK_LSHIFT: v_key = V_LSHIFT; break;
		case SDLK_LESS: v_key = V_BACKSLASH; break; // '\' character corresponds to SDLK_LESS not SDLK_BACKSLASH?!
		case SDLK_LCTRL: v_key = V_LCONTROL; break;
		case SDLK_LSUPER: v_key = V_LOS; break;
		case SDLK_LALT: v_key = V_LALT; break;
		// right side special keys
		case SDLK_MINUS: v_key = V_SUBTRACT; break;
		case SDLK_EQUALS: v_key = V_EQUALS; break;
		case SDLK_BACKSPACE: v_key = V_BACKSPACE; break;
		case SDLK_LEFTBRACKET: v_key = V_LEFTBRACKET; break;
		case SDLK_RIGHTBRACKET: v_key = V_RIGHTBRACKET; break;
		case SDLK_RETURN: v_key = V_RETURN; break;
		case SDLK_SEMICOLON: v_key = V_SEMICOLON; break;
		case SDLK_QUOTE: v_key = V_SINGLEQUOTE; break;
		case SDLK_BACKSLASH: v_key = V_HASH; break; // '#' character corresponds to SDLK_BACKSLASH not SDLK_HASH?!
		case SDLK_COMMA: v_key = V_COMMA; break;
		case SDLK_PERIOD: v_key = V_PERIOD; break;
		case SDLK_SLASH: v_key = V_SLASH; break;
		case SDLK_RSHIFT: v_key = V_RSHIFT; break;
		case SDLK_RALT: v_key = V_RALT; break;
		case SDLK_RSUPER: v_key = V_ROS; break;
		case SDLK_MENU: v_key = V_MENU; break;
		case SDLK_RCTRL: v_key = V_RCONTROL; break;
		// other special keys
		case SDLK_PRINT: v_key = V_PRINT; break;
		case SDLK_SCROLLOCK: v_key = V_SCROLLLOCK; break;
		case SDLK_PAUSE: v_key = V_PAUSE; break;
		case SDLK_INSERT: v_key = V_INSERT; break;
		case SDLK_HOME: v_key = V_HOME; break;
		case SDLK_PAGEUP: v_key = V_PGUP; break;
		case SDLK_DELETE: v_key = V_DELETE; break;
		case SDLK_END: v_key = V_END; break;
		case SDLK_PAGEDOWN: v_key = V_PGDOWN; break;
		// arrow keys
		case SDLK_LEFT: v_key = V_LEFT; break;
		case SDLK_RIGHT: v_key = V_RIGHT; break;
		case SDLK_UP: v_key = V_UP; break;
		case SDLK_DOWN: v_key = V_DOWN; break;
		// numeric keypad special keys
		case SDLK_NUMLOCK: v_key = V_NUMLOCK; break;
		case SDLK_KP_DIVIDE: v_key = V_NUMPAD_DIVIDE; break;
		case SDLK_KP_MULTIPLY: v_key = V_NUMPAD_MULTIPLY; break;
		case SDLK_KP_MINUS: v_key = V_NUMPAD_MINUS; break;
		case SDLK_KP_PLUS: v_key = V_NUMPAD_PLUS; break;
		case SDLK_KP_ENTER: v_key = V_RETURN; break; // n.b., treat as equal to Return, for compatibility with non-SDL Win32 code
		case SDLK_KP_PERIOD: v_key = V_NUMPAD_PERIOD; break;
		// numeric keypad
		case SDLK_KP0: v_key = V_NUMPAD0; break;
		case SDLK_KP1: v_key = V_NUMPAD1; break;
		case SDLK_KP2: v_key = V_NUMPAD2; break;
		case SDLK_KP3: v_key = V_NUMPAD3; break;
		case SDLK_KP4: v_key = V_NUMPAD4; break;
		case SDLK_KP5: v_key = V_NUMPAD5; break;
		case SDLK_KP6: v_key = V_NUMPAD6; break;
		case SDLK_KP7: v_key = V_NUMPAD7; break;
		case SDLK_KP8: v_key = V_NUMPAD8; break;
		case SDLK_KP9: v_key = V_NUMPAD9; break;
		// alphanumeric keys
		case SDLK_SPACE: v_key = V_SPACE; break;
		case SDLK_a: v_key = V_A; break;
		case SDLK_b: v_key = V_B; break;
		case SDLK_c: v_key = V_C; break;
		case SDLK_d: v_key = V_D; break;
		case SDLK_e: v_key = V_E; break;
		case SDLK_f: v_key = V_F; break;
		case SDLK_g: v_key = V_G; break;
		case SDLK_h: v_key = V_H; break;
		case SDLK_i: v_key = V_I; break;
		case SDLK_j: v_key = V_J; break;
		case SDLK_k: v_key = V_K; break;
		case SDLK_l: v_key = V_L; break;
		case SDLK_m: v_key = V_M; break;
		case SDLK_n: v_key = V_N; break;
		case SDLK_o: v_key = V_O; break;
		case SDLK_p: v_key = V_P; break;
		case SDLK_q: v_key = V_Q; break;
		case SDLK_r: v_key = V_R; break;
		case SDLK_s: v_key = V_S; break;
		case SDLK_t: v_key = V_T; break;
		case SDLK_u: v_key = V_U; break;
		case SDLK_v: v_key = V_V; break;
		case SDLK_w: v_key = V_W; break;
		case SDLK_x: v_key = V_X; break;
		case SDLK_y: v_key = V_Y; break;
		case SDLK_z: v_key = V_Z; break;
		case SDLK_0: v_key = V_0; break;
		case SDLK_1: v_key = V_1; break;
		case SDLK_2: v_key = V_2; break;
		case SDLK_3: v_key = V_3; break;
		case SDLK_4: v_key = V_4; break;
		case SDLK_5: v_key = V_5; break;
		case SDLK_6: v_key = V_6; break;
		case SDLK_7: v_key = V_7; break;
		case SDLK_8: v_key = V_8; break;
		case SDLK_9: v_key = V_9; break;
		//case SDLK_: v_key = V_; break;
	}
	if( v_key == -1 ) {
		LOG("UNKNOWN SDLKey code %d\n", key);
	}
	return v_key;
}

/*void SDL_GraphicsEnvironment::handleInput() {
	// we override this, so we can retrieve the input from SDL, and send it to the input handlers!

	GraphicsEnvironment::handleInput(); // n.b., do this first, as it flushes the input at the end! (so below, we are actually reading the input for the next loop)
*/

void SDL_GraphicsEnvironment::initSDL() {
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
	SDL_EnableUNICODE(1);
}

void SDL_GraphicsEnvironment::processEvents() {
	SDL_Event event;
	//bool quit = false;

	// hack for quit messages posted in GraphicsEnvironment::handleInput(), as otherwise SDL_PollEvent seems to eat it!
	/*if( this->inputSystem->getKeyboard()->keys[V_ESCAPE] ) {
		LOG("already quit\n");
		quit = true;
	}*/
	/*if( this->inputSystem->getKeyboard()->keys[V_ESCAPE] ) {
		// hack - convert to an SDL quit, as we'll eat the WM_QUIT message! Also needed for non-Windows platforms
		LOG("push SDL quit\n");
		SDL_Event quitEvent;
		quitEvent.type = SDL_QUIT;
		SDL_PushEvent(&quitEvent);
	}*/
	/*MSG msg;
	if ( PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) ) {
		if( msg.message == WM_QUIT ) {
			LOG("already quit\n");
			quit = true;
		}
	}*/

	//int count = 0;
	//while( !this->request_quit && SDL_PollEvent(&event) ) {
	while( SDL_PollEvent(&event) ) {
		//LOG("event %d\n", count++);
		switch(event.type) {
			case SDL_QUIT:
				LOG("SDL_QUIT\n");
				request_quit = true;
				break;
			case SDL_KEYDOWN:
				{
					int v_key = translateKey(event.key.keysym.sym);
					//LOG("SDL KEYDOWN %d\n", v_key);
					if( v_key != -1 ) {
						this->inputSystem->getKeyboard()->setKey(v_key, true);
						VI_KeyEvent key_event;
						key_event.code = v_key;
						//if( (event.key.keysym.unicode & 0xFF80) == 0 ) {
						if( (event.key.keysym.unicode & 0xFF00) == 0 ) {
							//key_event.ascii = event.key.keysym.unicode & 0x7F;
							key_event.ascii = event.key.keysym.unicode & 0xFF;
							//LOG("    ASCII %d\n", key_event.ascii);
							if( this->panel != NULL ) {
								Panel *use_panel = this->modal_panel;
								if( use_panel == NULL )
									use_panel = this->panel;
								use_panel->processKeydownEvent(&key_event);
								if( key_event.pushed_action != NULL ) {
									(*key_event.pushed_action)(key_event.pushed_action_src);
								}
							}
						}
						else {
							LOG("    UNICODE %d (IGNORED)\n", event.key.keysym.unicode);
						}
						if( inputSystem->getKeyboard()->isPressed(V_ESCAPE) ) {
							// n.b., check InputHandler, in case the keyboard was flushed when processing the keydown event
							LOG("Post quit\n");
							this->request_quit = true;
						}
					}
				}
				break;
			case SDL_KEYUP:
				{
					int v_key = translateKey(event.key.keysym.sym);
					//LOG("SDL KEYUP %d\n", v_key);
					if( v_key != -1 )
						this->inputSystem->getKeyboard()->setKey(v_key, false);
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				{
					//LOG("mouse button down\n");
					VI_MouseClickEvent mouse_click_event;
					if( event.button.button == SDL_BUTTON_LEFT ) {
						this->inputSystem->getMouse()->mouseLChange( true );
						mouse_click_event.left = true;
					}
					else if( event.button.button == SDL_BUTTON_MIDDLE ) {
						this->inputSystem->getMouse()->mouseMChange( true );
						mouse_click_event.middle = true;
					}
					else if( event.button.button == SDL_BUTTON_RIGHT ) {
						this->inputSystem->getMouse()->mouseRChange( true );
						mouse_click_event.right = true;
					}
					else if( event.button.button == SDL_BUTTON_WHEELUP ) {
						//LOG("mouse wheel up\n");
						this->inputSystem->getMouse()->setMouseWheelUp();
						mouse_click_event.wheel_up = true;
					}
					else if( event.button.button == SDL_BUTTON_WHEELDOWN ) {
						//LOG("mouse wheel down\n");
						this->inputSystem->getMouse()->setMouseWheelDown();
						mouse_click_event.wheel_down = true;
					}
					mouse_click_event.mouse_x = event.button.x;
					mouse_click_event.mouse_y = event.button.y;
					if( this->panel != NULL ) {
						Panel *use_panel = this->modal_panel;
						if( use_panel == NULL )
							use_panel = this->panel;
						use_panel->processMouseClickEvent(&mouse_click_event);
						if( mouse_click_event.pushed_action != NULL ) {
							(*mouse_click_event.pushed_action)(mouse_click_event.pushed_action_src);
						}
					}
				}
				break;
			case SDL_MOUSEBUTTONUP:
				{
					//LOG("mouse button up\n");
					VI_MouseReleaseEvent mouse_release_event;
					if( event.button.button == SDL_BUTTON_LEFT ) {
						this->inputSystem->getMouse()->mouseLChange( false );
						mouse_release_event.left = true;
					}
					else if( event.button.button == SDL_BUTTON_MIDDLE ) {
						this->inputSystem->getMouse()->mouseMChange( false );
						mouse_release_event.middle = true;
					}
					else if( event.button.button == SDL_BUTTON_RIGHT ) {
						this->inputSystem->getMouse()->mouseRChange( false );
						mouse_release_event.right = true;
					}
					mouse_release_event.mouse_x = event.button.x;
					mouse_release_event.mouse_y = event.button.y;
					if( this->panel != NULL ) {
						Panel *use_panel = this->modal_panel;
						if( use_panel == NULL )
							use_panel = this->panel;
						use_panel->processMouseReleaseEvent(&mouse_release_event);
					}
				}
				break;
			case SDL_MOUSEMOTION:
				if( this->mouseMotionEventFunc != NULL ) {
					VI_MouseMotionEvent mouse_motion_event;
					mouse_motion_event.x = event.motion.x;
					mouse_motion_event.y = event.motion.y;
					mouse_motion_event.rel_x = event.motion.xrel;
					mouse_motion_event.rel_y = event.motion.yrel;
					(*this->mouseMotionEventFunc)(&mouse_motion_event, this->mouseMotionEventData);
				}
				break;
			case SDL_ACTIVEEVENT:
				if( event.active.state & SDL_APPINPUTFOCUS ) {
					if( event.active.gain == 1 ) {
						LOG("SDL gained focus\n");
						this->active = true;
					}
					else if( event.active.gain == 0 ) {
						LOG("SDL lost focus\n");
						this->active = false;
					}
				}
				break;
			/*case SDL_SYSWMEVENT:
#ifdef _WIN32
				//LOG("received SDL_SYSWMEVENT\n");
				{
					SDL_SysWMmsg *sys_msg = event.syswm.msg;
					if( sys_msg->msg == WM_TOUCH ) {
						LOG("touch!\n");
					}
				}
#endif
				break;*/
			/*default:
				LOG("unknown event type: %d\n", event.type);
				break;*/
		}
	}

	// n.b., this bit could instead be done in an overridden MouseInputHandler (as with Win32MouseInputHandler), but we might as well do it here
	int m_x = 0, m_y = 0;
	// OS-specific code, as SDL doesn't return mouse coordinates outside the window!
#ifdef _WIN32
    if( this->isFullScreen() ) {
        // the OS-specific code doesn't work in fullscreen mode! But we don't need to worry about mouse outside of window anyway, when fullscreen
        SDL_GetMouseState(&m_x, &m_y);
    }
	else {
		tagPOINT tag;
		GetCursorPos(&tag);
		ScreenToClient(this->hWnd,(POINT*)&tag);
		m_x = tag.x;
		m_y = tag.y;
	}
#elif __linux
    if( this->isFullScreen() ) {
        // the OS-specific code doesn't work in fullscreen mode! But we don't need to worry about mouse outside of window anyway, when fullscreen
        SDL_GetMouseState(&m_x, &m_y);
    }
    else {
        Window root_return, child_return;
        int root_x_return = 0, root_y_return = 0, win_x_return = 0, win_y_return = 0;
        unsigned int mask_return = 0;
        if( XQueryPointer(x_display, x_window, &root_return, &child_return,
            &root_x_return, &root_y_return, &win_x_return, &win_y_return, &mask_return) )
        {
            //LOG("root: %d, %d\n", root_x_return, root_y_return);
            //LOG("win: %d, %d\n", win_x_return, win_y_return);
            m_x = win_x_return;
            m_y = win_y_return;
        }
        else {
            LOG("XQueryPointer failed\n");
        }
    }
#else
	SDL_GetMouseState(&m_x, &m_y);
#endif
	//LOG("%d, %d\n", m_x, m_y);
	this->inputSystem->getMouse()->setMouse(m_x, m_y);
}

void SDL_GraphicsEnvironment::waitForEvents() {
	SDL_WaitEvent(NULL);
}

void SDL_GraphicsEnvironment::sleep(int time) {
	SDL_Delay(time);
}

int SDL_GraphicsEnvironment::getRealTimeMS() {
	return SDL_GetTicks();
}

void SDL_GraphicsEnvironment::setVisible(bool visible) {
	// TODO: replace with SDL equivalent?
#ifdef _WIN32
	if( visible ) {
		ShowWindow(hWnd,SW_SHOW);
		SetForegroundWindow(hWnd);
		SetFocus(hWnd);
	}
	else
		ShowWindow(hWnd,SW_HIDE);
#endif
}

void SDL_GraphicsEnvironment::setWindowTitle(const char *title) {
	SDL_WM_SetCaption(title, "");
}

void SDL_GraphicsEnvironment::grabInput(bool grab) {
	SDL_WM_GrabInput(grab ? SDL_GRAB_ON : SDL_GRAB_OFF);
}

void SDL_GraphicsEnvironment::showCursor(bool show) {
	SDL_ShowCursor(show ? SDL_ENABLE : SDL_DISABLE);
}

void SDL_GraphicsEnvironment::setCursor(unsigned char *data, unsigned char *mask, int w, int h, int hot_x, int hot_y) {
	SDL_Cursor *cursor = SDL_CreateCursor(data, mask, 32, 32, hot_x, hot_y);
	SDL_Cursor *old_cursor = SDL_GetCursor();
	SDL_SetCursor(cursor);
	SDL_FreeCursor(old_cursor);
}
