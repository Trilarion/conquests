#pragma once

#define _USE_MATH_DEFINES // needed to get maths constants in cmath when using Visual Studio
#include <cmath>

#include <string>
using std::string;
#include <vector>
using std::vector;
#include <map>
using std::map;
#include <algorithm>
using std::find;

class IRotationMatrix;
class Matrix;
class Rotation3D;

float V_cosd(float theta);
float V_sind(float theta);

bool VI_isPower(int n,int p);
int VI_getNextPower(int n,int p);

class Vector2D {
public:
	float x, y;
	Vector2D() : x(0.0), y(0.0) {
	}
	/*Vector2D(const Vector2D &v) : x(v.x), y(v.y) {
	}*/
	Vector2D(const float x,const float y) : x(x), y(y) {
	}
	void set(const float x,const float y) {
		this->x = x;
		this->y = y;
	}
	float &operator[] (const int i) {
		return *((&x) + i);
	}
	const float &operator[] (const int i) const {
		return *((&x) + i);
	}
	const bool operator== (const Vector2D& v) const {
		return (v.x==x && v.y==y);
	}
	const bool operator!= (const Vector2D& v) const {
		return !(v.x==x && v.y==y);
		//return !(v == *this);
	}
	/*const*/ Vector2D operator+ () const {
		return Vector2D(x,y);
		//return (*this);
	}
	/*const*/ Vector2D operator- () const {
		return Vector2D(-x,-y);
	}
	const Vector2D& operator= (const Vector2D& v) {
		x = v.x;
		y = v.y;
		return *this;
	}
	/*const*/ Vector2D& operator+= (const Vector2D& v) {
		x+=v.x;
		y+=v.y;
		return *this;
	}
	/*const*/ Vector2D& operator-= (const Vector2D& v) {
		x-=v.x;
		y-=v.y;
		return *this;
	}
	/*const*/ Vector2D& operator*= (const float& s) {
		x*=s;
		y*=s;
		return *this;
	}
	/*const*/ Vector2D& operator/= (const float& s) {
		const float r = 1 / s;
		x *= r;
		y *= r;
		return *this;
	}
	/*const*/ Vector2D operator+ (const Vector2D& v) const {
		return Vector2D(x + v.x, y + v.y);
	}
	/*const*/ Vector2D operator- (const Vector2D& v) const {
		return Vector2D(x - v.x, y - v.y);
	}
	/*const*/ Vector2D operator* (const float& s) const {
		return Vector2D(x*s,y*s);
	}
	/*friend inline const Vector3D operator* (const float& s,const Vector3D& v) {
	return v * s;
	}*/
	/*const*/ Vector2D operator/ (float s) const {
		s = 1/s;
		return Vector2D(s*x,s*y);
	}

	void scale(const float s) {
		this->x *= s;
		this->y *= s;
	}
	void scale(const float sx,const float sy) {
		this->x*=sx;
		this->y*=sy;
	}
	void translate(const float tx,const float ty) {
		this->x+=tx;
		this->y+=ty;
	}
	void add(const Vector2D &v) {
		this->x+=v.x;
		this->y+=v.y;
	}
	void subtract(const Vector2D &v) {
		this->x-=v.x;
		this->y-=v.y;
	}
	const float square() const {
		return (x*x + y*y);
	}
	const float magnitude() const {
		return sqrt( x*x + y*y );
	}
	const float dot(const Vector2D &v) const {
		return ( x * v.x + y * v.y);
	}
	const float operator%(const Vector2D& v) const {
		return ( x * v.x + y * v.y);
	}
	void normalise() {
		float mag = magnitude();
		x = x/mag;
		y = y/mag;
	}
	void normaliseSafe() {
		float mag = magnitude();
		if(mag==0)
			return;
		x = x/mag;
		y = y/mag;
	}
	/*const*/ Vector2D unit() const {
		float mag = magnitude();
		return Vector2D(x / mag, y / mag);
		//return (*this) / magnitude();
	}
	bool isZero() const {
		return ( x == 0 && y == 0 );
	}
	bool isZero(float tol) const {
		float mag = magnitude();
		return mag <= tol;
	}
	bool isEqual(const Vector2D &that, float tol) const {
		Vector2D diff = *this - that;
		float mag = diff.magnitude();
		return mag <= tol;
	}
};

class Vector3D {
public:
	float x,y,z;
	static Vector3D forwards;
	static Vector3D backwards;
	static Vector3D left;
	static Vector3D right;
	static Vector3D upwards;
	static Vector3D downwards;
	Vector3D() : x(0.0f), y(0.0f), z(0.0f) {
	}
	/*Vector3D(const Vector3D &v) : x(v.x), y(v.y), z(v.z) {
	}*/
	Vector3D(const float x,const float y,const float z) : x(x), y(y), z(z) {
	}
	void set(const float x,const float y,const float z) {
		this->x=x;
		this->y=y;
		this->z=z;
	}
	float &operator[] (const int i) {
		return *((&x) + i);
	}
	const float &operator[] (const int i) const {
		return *((&x) + i);
	}
	const bool operator== (const Vector3D& v) const {
		return (v.x==x && v.y==y && v.z==z);
	}
	const bool operator!= (const Vector3D& v) const {
		return !(v.x==x && v.y==y && v.z==z);
		//return !(v == *this);
	}
	/*const*/ Vector3D operator+ () const {
		return Vector3D(x,y,z);
		//return (*this);
	}
	/*const*/ Vector3D operator- () const {
		return Vector3D(-x,-y,-z);
	}
	const Vector3D& operator= (const Vector3D& v) {
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}
	/*const*/ Vector3D& operator+= (const Vector3D& v) {
		x+=v.x;
		y+=v.y;
		z+=v.z;
		return *this;
	}
	/*const*/ Vector3D& operator-= (const Vector3D& v) {
		x-=v.x;
		y-=v.y;
		z-=v.z;
		return *this;
	}
	/*const*/ Vector3D& operator*= (const float& s) {
		x*=s;
		y*=s;
		z*=s;
		return *this;
	}
	/*const*/ Vector3D& operator/= (const float& s) {
		const float r = 1 / s;
		x *= r;
		y *= r;
		z *= r;
		return *this;
	}
	/*const*/ Vector3D operator+ (const Vector3D& v) const {
		return Vector3D(x + v.x, y + v.y, z + v.z);
	}
	/*const*/ Vector3D operator- (const Vector3D& v) const {
		return Vector3D(x - v.x, y - v.y, z - v.z);
	}
	/*const*/ Vector3D operator* (const float& s) const {
		return Vector3D(x*s,y*s,z*s);
	}
	/*friend inline const Vector3D operator* (const float& s,const Vector3D& v) {
	return v * s;
	}*/
	/*const*/ Vector3D operator/ (float s) const {
		s = 1/s;
		return Vector3D(s*x,s*y,s*z);
	}

	void scale(const float s) {
		this->x *= s;
		this->y *= s;
		this->z *= s;
	}
	void scale(const float sx,const float sy,const float sz) {
		this->x*=sx;
		this->y*=sy;
		this->z*=sz;
	}
	void translate(const float tx,const float ty,const float tz) {
		this->x+=tx;
		this->y+=ty;
		this->z+=tz;
	}
	void add(const Vector3D &v) {
		this->x+=v.x;
		this->y+=v.y;
		this->z+=v.z;
	}
	void subtract(const Vector3D &v) {
		this->x-=v.x;
		this->y-=v.y;
		this->z-=v.z;
	}
	const float square() const {
		return (x*x + y*y + z*z);
	}
	const float magnitude() const {
		return sqrt( x*x + y*y + z*z );
	}
	const float dot(const Vector3D &v) const {
		return ( x * v.x + y * v.y + z * v.z);
	}
	const float operator%(const Vector3D &v) const {
		return ( x * v.x + y * v.y + z * v.z);
	}
	void cross(const Vector3D &v) {
		float nx = this->y * v.z - this->z * v.y;
		float ny = -this->x * v.z + this->z * v.x;
		float nz = this->x * v.y - this->y * v.x;
		this->x=-nx;
		this->y=-ny;
		this->z=-nz;
	}
	/*const*/ Vector3D operator^(const Vector3D &v) const {
		return Vector3D(- y * v.z + z * v.y,
			x * v.z - z * v.x,
			- x * v.y + y * v.x);
	}
	void normalise() {
		float mag = magnitude();
		x = x/mag;
		y = y/mag;
		z = z/mag;
	}
	void normaliseSafe() {
		float mag = magnitude();
		if(mag==0)
			return;
		x = x/mag;
		y = y/mag;
		z = z/mag;
	}
	/*const*/ Vector3D unit() const {
		float mag = magnitude();
		return Vector3D(x / mag,y / mag,z / mag);
		//return (*this) / magnitude();
	}
	Vector3D perpendicular() const;
	bool isZero() const {
		return ( x == 0 && y == 0 && z == 0 );
	}
	bool isZero(float tol) const {
		float mag = magnitude();
		return mag <= tol;
	}
	bool isEqual(const Vector3D &that, float tol) const {
		Vector3D diff = *this - that;
		float mag = diff.magnitude();
		return mag <= tol;
	}

	void rotateBy(const Rotation3D &r);
	void rotateBy(float x,float y,float z);
	void dropOnLine(const Vector3D &o,const Vector3D &n);
	void parallelComp(const Vector3D &n);
	void perpComp(const Vector3D &n);
	float distFromLine(const Vector3D &o,const Vector3D &n) const;
	float distFromLineSq(const Vector3D &o,const Vector3D &n) const;
	Vector3D closestOnLine(const Vector3D &a,const Vector3D &b) const;
	bool pointInPolygon(const Vector3D *points,int n_points) const;
	Vector3D closestPointOnPolygon(const Vector3D *points,int n_points) const;
	static bool intersectSweptSphereWithPlane(bool *full,Vector3D *intersection1,Vector3D *intersection2,const Vector3D &a,const Vector3D &b,float r,const Vector3D &n,float D);
};

class Sphere {
public:
	Vector3D centre;
	float radius;
	float radiusSquared;

	Sphere() {
		this->radius = 0.0;
		this->radiusSquared = 0.0;
	}
	void setRadiusSquared(float radiusSquared) {
		this->radiusSquared = radiusSquared;
		this->radius = sqrt(radiusSquared);
	}
	void setRadius(float radius) {
		this->radius = radius;
		this->radiusSquared = radius*radius;
	}
	void expand(const Sphere &sphere);
};

/*class AABB {
	Vector3D low, high;
public:
	AABB() {
	}
	void setAABB(const Vector3D extremes[2]) {
		this->low = extremes[0];
		this->high = extremes[1];
	}
	Vector3D getLow() const {
		return low;
	}
	Vector3D getHigh() const {
		return high;
	}
	Vector3D calculateCentre() const {
		Vector3D centre = ( low + high ) * 0.5f;
		return centre;
	}
};*/

class Box {
	Vector3D corners[8];
	Vector3D x_axis, y_axis, z_axis;
public:

	Box() {
		x_axis.set(1.0f, 0.0f, 0.0f);
		y_axis.set(0.0f, 1.0f, 0.0f);
		z_axis.set(0.0f, 0.0f, 1.0f);
	}
	void setAABB(const Vector3D extremes[2]) {
		x_axis.set(1.0f, 0.0f, 0.0f);
		y_axis.set(0.0f, 1.0f, 0.0f);
		z_axis.set(0.0f, 0.0f, 1.0f);

		corners[0] = extremes[0];
		corners[1].set(extremes[1].x, extremes[0].y, extremes[0].z);
		corners[2].set(extremes[1].x, extremes[1].y, extremes[0].z);
		corners[3].set(extremes[0].x, extremes[1].y, extremes[0].z);
		corners[4].set(extremes[0].x, extremes[0].y, extremes[1].z);
		corners[5].set(extremes[1].x, extremes[0].y, extremes[1].z);
		corners[6].set(extremes[0].x, extremes[1].y, extremes[1].z);
		corners[7] = extremes[1];
	}
	Vector3D getCorner(int i) const {
		return corners[i];
	}
	void setCorner(int i, Vector3D corner) {
		corners[i] = corner;
	}
	Vector3D calculateCentre() const {
		Vector3D centre = ( corners[0] + corners[7] ) * 0.5f;
		return centre;
	}
	Vector3D getX() const {
		return ( corners[1] - corners[0] ) * 0.5;
	}
	Vector3D getY() const {
		return ( corners[3] - corners[0] ) * 0.5;
	}
	Vector3D getZ() const {
		return ( corners[4] - corners[0] ) * 0.5;
	}
	void getAxes(Vector3D *x_axis, Vector3D *y_axis, Vector3D *z_axis) const;
	void expand(float distance);

	bool collisionLine(float *intersection, const Vector3D &pos, const Vector3D &dir) const;
};

class Quaternion {
public:
	float w, x, y, z;
	Quaternion() : w(0), x(0), y(0), z(0) {
	}
	Quaternion(const float w,const float x,const float y,const float z) : w(w), x(x), y(y), z(z) {
	}
	Quaternion(Matrix *matrix) {
		set(matrix);
	}
	~Quaternion() {
	}

	void set(const float w,const float x,const float y,const float z) {
		this->w = w;
		this->x = x;
		this->y = y;
		this->z = z;
	}
	void setEuler(float rx,float ry,float rz);
	void set(Matrix *matrix);
	void setAngleAxis(float ax,float ay,float az,float angle);

	void convertToEuler(float *rx,float *ry,float *rz) const;
	void convertToMatrix(IRotationMatrix *matrix) const;
	void convertToMatrixInverse(IRotationMatrix *matrix) const;
	template<class T>
	void convertToGLMatrix(T glmat[16]) const {
		T xx = x * x;
		T xy = x * y;
		T xz = x * z;
		T xw = x * w;
		T yy = y * y;
		T yz = y * z;
		T yw = y * w;
		T zz = z * z;
		T zw = z * w;

		glmat[0] = 1 - 2 * ( yy + zz );
		glmat[4] = 2 * ( xy - zw );
		glmat[8] = 2 * ( xz + yw );

		glmat[1] = 2 * ( xy + zw );
		glmat[5] = 1 - 2 * ( xx + zz );
		glmat[9] = 2 * ( yz - xw );

		glmat[2] = 2 * ( xz - yw );
		glmat[6] = 2 * ( yz + xw );
		glmat[10] = 1 - 2 * ( xx + yy );

		glmat[3] = 0;
		glmat[7] = 0;
		glmat[11] = 0;

		glmat[12] = 0;
		glmat[13] = 0;
		glmat[14] = 0;

		glmat[15] = 1;
	}
	template<class T>
	void convertInverseToGLMatrix(T glmat[16]) const {
		T xx = x * x;
		T xy = x * y;
		T xz = x * z;
		T xw = x * w;
		T yy = y * y;
		T yz = y * z;
		T yw = y * w;
		T zz = z * z;
		T zw = z * w;

		glmat[0] = 1 - 2 * ( yy + zz );
		glmat[1] = 2 * ( xy - zw );
		glmat[2] = 2 * ( xz + yw );

		glmat[4] = 2 * ( xy + zw );
		glmat[5] = 1 - 2 * ( xx + zz );
		glmat[6] = 2 * ( yz - xw );

		glmat[8] = 2 * ( xz - yw );
		glmat[9] = 2 * ( yz + xw );
		glmat[10] = 1 - 2 * ( xx + yy );

		glmat[3] = 0;
		glmat[7] = 0;
		glmat[11] = 0;

		glmat[12] = 0;
		glmat[13] = 0;
		glmat[14] = 0;

		glmat[15] = 1;
	}
	Vector3D getHeading() const;
	Vector3D getUp() const;

	float square() const {
		return( w*w + x*x + y*y + z*z );
	}
	float magnitude() const {
		return sqrt( w*w + x*x + y*y + z*z );
	}
	float dot(const Quaternion& q) const;
	Quaternion slerp(const Quaternion& q, float t) const;
	Quaternion operator+ () const {
		return Quaternion(w,x,y,z);
	}
	Quaternion operator- () const {
		return Quaternion(-w,-x,-y,-z);
	}
	Quaternion operator+ (const Quaternion& q) const {
		return Quaternion(w + q.w, x + q.x, y + q.y, z + q.z);
	}
	Quaternion operator- (const Quaternion& q) const {
		return Quaternion(w - q.w, x - q.x, y - q.y, z - q.z);
	}
	Quaternion operator*(const Quaternion& q) const;
};

class Rotation3D {
	//float x,y,z;
	Quaternion quat;

	void check() {
		/*while( x < 0 )
		x += 360;
		while( x > 360 )
		x -= 360;
		while( y < 0 )
		y += 360;
		while( y > 360 )
		y -= 360;
		while( z < 0 )
		z += 360;
		while( z > 360 )
		z -= 360;*/
	}
public:
	Rotation3D();
	Rotation3D(const Quaternion &quat);
	Rotation3D(float x, float y, float z);

	void rotateTo(const Rotation3D &rot) {
		this->quat = rot.quat;
	}
	void rotateTo(const Quaternion &quat) {
		this->quat = quat;
	}
	void rotate(float x,float y,float z);
	void rotateLocal(float x,float y,float z);
	void rotateToEuler(float x,float y,float z);
	void rotateToDirection(const Vector3D &axis, const Vector3D &dir);
	void rotateToDirection(const Vector3D &dir);
	/*void rotateLocalX(float theta);
	void rotateLocalY(float theta);*/
	void getEuler(float *x,float *y,float *z) const;
	void getMatrix(IRotationMatrix *matrix) const;
	void getMatrixInverse(IRotationMatrix *matrix) const;
	Vector3D getHeading() const {
		return quat.getHeading();
	}
	Vector3D getUp() const {
		return quat.getUp();
	}
	/*void multGLMatrix();
	void multGLMatrixTranspose();*/
	void rotateToIdentity() {
		quat.set(1,0,0,0);
	}
	Quaternion *getQuaternion() {
		return &quat;
	}
	const Quaternion *getQuaternion() const {
		return &quat;
	}
	Rotation3D slerp(const Rotation3D &rot, float t) const {
		Quaternion res_q = this->quat.slerp(rot.quat, t);
		Rotation3D res;
		res.quat = res_q;
		return res;
	}
};

class Plane {
public:
	Vector3D normal;
	float D;
	Plane() {
		normal.set(0.0,0.0,0.0);
		D = 0.0;
	}
	float dist(const Vector3D &p) const {
		return normal.dot(p) - D;
	}
	void set(Vector3D normal, float D) {
		this->normal = normal;
		this->D = D;
	}
	void set(Vector3D normal, Vector3D pos) {
		this->normal = normal;
		this->D = pos % normal;
	}
};

class IRotationMatrix {
public:
	virtual void setZero()=0;
	virtual void setIdentity()=0;

	virtual void setElement(int row,int col,float value)=0;
	virtual float getElement(int row,int col) const=0;

	virtual void setRotation(const Rotation3D &r);
	virtual void setRotationInverse(const Rotation3D &r);

	virtual void transformVector(Vector3D *v) const=0;
};

class Matrix : public IRotationMatrix {
private:
	//float m[3][3]; // [col][row]
	float m[3][3]; // [row][col]

public:
	Matrix() {
		setZero();
	}

	virtual void setZero() {
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				this->m[i][j] = 0.0;
			}
		}
	}
	virtual void setIdentity() {
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				this->m[i][j] = (i==j) ? 1.0f : 0.0f;
			}
		}
	}

	virtual void setElement(int row,int col,float value) {
		//this->m[col][row] = value;
		this->m[row][col] = value;
	}
	virtual float getElement(int row,int col) const {
		//return this->m[col][row];
		return this->m[row][col];
	}

	/*virtual void setRotationMatrix(const Rotation3D *r);
	virtual void setRotationMatrixInverse(const Rotation3D *r);*/

	virtual void transformVector(Vector3D *v) const;

	void setOpenGL(const float v[16]) {
		this->m[0][0] = v[0];
		this->m[1][0] = v[1];
		this->m[2][0] = v[2];
		this->m[0][1] = v[4];
		this->m[1][1] = v[5];
		this->m[2][1] = v[6];
		this->m[0][2] = v[8];
		this->m[1][2] = v[9];
		this->m[2][2] = v[10];
	}
	void setOpenGLTranspose(const float v[16]) {
		this->m[0][0] = v[0];
		this->m[0][1] = v[1];
		this->m[0][2] = v[2];
		this->m[1][0] = v[4];
		this->m[1][1] = v[5];
		this->m[1][2] = v[6];
		this->m[2][0] = v[8];
		this->m[2][1] = v[9];
		this->m[2][2] = v[10];
	}
	float trace() const {
		return ( this->m[0][0] + this->m[1][1] + this->m[2][2] );
	}
};

class Matrix4d : public IRotationMatrix {
	//float m[4][4]; // column, row
	float m[4][4]; // row, column

public:
	Matrix4d() {
		setZero();
	}
	/*Matrix4d(const float glmatrix[16]) {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				//m[i][j] = glmatrix[4*i+j];
				m[j][i] = glmatrix[4*i+j];
			}
		}
	}*/

	virtual void setZero() {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				m[i][j]=0;
			}
		}
	}
	virtual void setIdentity() {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				if(i==j)
					m[i][j] = 1.0f;
				else
					m[i][j] = 0;
			}
		}
	}

	virtual void setElement(int row,int col,float value) {
		//this->m[col][row] = value;
		this->m[row][col] = value;
	}
	virtual float getElement(int row,int col) const {
		//return this->m[col][row];
		return this->m[row][col];
	}

	/*virtual void setRotationMatrix(const Rotation3D *r);
	virtual void setRotationMatrixInverse(const Rotation3D *r);*/
	void setTranslation(float x,float y,float z) {
		/*m[3][0] = x;
		m[3][1] = y;
		m[3][2] = z;
		m[3][3] = 1.0f;*/
		m[0][3] = x;
		m[1][3] = y;
		m[2][3] = z;
		m[3][3] = 1.0f;
	}
	void setTranslation(const Vector3D &v) {
		setTranslation(v.x, v.y, v.z);
	}
	void setAngleAxisRotation(float x,float y,float z,float angle);

	virtual void transformVector(Vector3D *v) const;
	void transformDirection(Vector3D *v) const;

	void premultiply(const Matrix4d &that);
	Matrix4d *multiply(const Matrix4d &that) const;

	Matrix4d& operator+= (const Matrix4d& v) {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				this->m[i][j] += v.m[i][j];
			}
		}
		return *this;
	}
	Matrix4d& operator-= (const Matrix4d& v) {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				this->m[i][j] -= v.m[i][j];
			}
		}
		return *this;
	}
	Matrix4d operator+ (const Matrix4d& v) const {
		Matrix4d m(*this);
		m += v;
		return v;
	}
	Matrix4d operator- (const Matrix4d& v) const {
		Matrix4d m(*this);
		m -= v;
		return v;
	}
	Matrix4d& operator*= (const float& v) {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				this->m[i][j] *= v;
			}
		}
		return *this;
	}
	Matrix4d& operator/= (const float& v) {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				this->m[i][j] /= v;
			}
		}
		return *this;
	}
	Matrix4d operator* (const float& v) const {
		Matrix4d m(*this);
		m *= v;
		return m;
	}
	Matrix4d operator/ (const float& v) const {
		Matrix4d m(*this);
		m /= v;
		return m;
	}

	// factories
	static Matrix4d * createIdentity() {
		Matrix4d * mat = new Matrix4d();
		mat->setIdentity();
		return mat;
	}
	static Matrix4d * createScale(float x,float y,float z) {
		Matrix4d * mat = new Matrix4d();
		mat->m[0][0]= x;
		mat->m[1][1]= y;
		mat->m[2][2]= z;
		mat->m[3][3]= 1.0f;
		return mat;
	}
	static Matrix4d * createTranslation(float x,float y,float z) {
		Matrix4d * mat = new Matrix4d();
		mat->setIdentity();
		mat->setTranslation(x,y,z);
		return mat;
	}
	static Matrix4d * createAngleAxisRotation(float x,float y,float z,float angle) {
		Matrix4d * mat = new Matrix4d();
		mat->setIdentity();
		mat->setAngleAxisRotation(x,y,z,angle);
		return mat;
	}
};

void VI_initPerlin();
float VI_perlin_noise1(float arg);
float VI_perlin_noise2(float vec[2]);
float VI_perlin_noise3(float vec[3]);

template<class T>
bool remove_vec(vector<T> *vec,const T& value) {
	for(size_t i=0;i<vec->size();i++) {
		if( vec->at(i) == value ) {
			vec->erase(vec->begin() + i);
			return true;
		}
	}
	return false;
}

template<class T>
bool contains_vec(vector<T> *vec,const T& value) {
	if( find(vec->begin(), vec->end(), value) == vec->end() ) {
		return false;
	}
	return true;
}

/*template<class T>
bool addIfAbsent_vec(vector<T> *vec,const T& value) {
	if( find(vec->begin(), vec->end(), value) == vec->end() ) {
		vec->push_back(value);
		return true;
	}
	return false;
}*/

class VI_XMLTreeNode {
	string name;
	map<string, string> attributes;
	bool has_data;
	string data;
	/*bool has_int_data;
	int int_data;
	bool has_double_data;
	double double_data;*/

	vector<VI_XMLTreeNode *> children;
public:
	VI_XMLTreeNode();
	virtual ~VI_XMLTreeNode();

	void setName(const char *name);
	string getName() const;

	void addAttribute(const char *name, const char *value);
	string getAttribute(const char *name);

	void setData(const char *data);
	bool hasData() const;
	string getData() const;

	/*void setIntData(int int_data);
	bool hasIntData() const;
	int getIntData() const;

	void setDoubleData(int double_data);
	bool hasDoubleData() const;
	int getDoubleData() const;*/

	size_t getNChildren() const;
	VI_XMLTreeNode *getChild(size_t i);
	const VI_XMLTreeNode *getChild(size_t i) const;
	void addChild(VI_XMLTreeNode *child);

	VI_XMLTreeNode *findNode(const char *match, bool recurse);
};

VI_XMLTreeNode *VI_parseXML(const char *filename);

bool VI_createFolder(const char *folder);
