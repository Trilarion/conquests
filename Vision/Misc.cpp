//---------------------------------------------------------------------------
#include "Misc.h"
#include "VisionShared.h"
#include "GraphicsEnvironment.h"
#include "Renderer.h"

//---------------------------------------------------------------------------

/*const float RES_LARGE = 1000.0f;
const float RES_LINEAR = 1.0e-3f;
const float RES_ANGULAR = RES_LINEAR / RES_LARGE;*/

Point3D::Point3D() : changedPositionListener(NULL) {
	//node = NULL;
	pos.set(0,0,0);
	rot.rotateToEuler(0,0,0);
	this->infinite = false;
}

/*Point3D::Point3D() {
Point3D(0,0,0,0,0,0);
}

Point3D::Point3D(float x,float y,float z,float rx,float ry,float rz) {
node = NULL;
pos.set(x,y,z);
rot.rotateToEuler(rx,ry,rz);
//printf("%d, %d, %d\n",dir.x,dir.y,dir.z);
}*/

void Point3D::checkNode() {
	/*if( node != NULL ) {
		node->changedPosition();
	}*/
	if( this->changedPositionListener != NULL ) {
		this->changedPositionListener->changedPosition();
	}
}

void Point3D::multGLMatrix() const {
	/*glTranslatef(pos.x,pos.y,pos.z);
	rot.multGLMatrix();*/

	/*GLfloat glmat[16];
	rot.getQuaternion()->convertToGLMatrix(glmat);
	glmat[12] = pos.x;
	glmat[13] = pos.y;
	glmat[14] = pos.z;
	if( this->infinite ) {
		glmat[15] = 0.0;
	}
	glMultMatrixf(glmat);*/
	GraphicsEnvironment::getSingleton()->getRenderer()->transform(&pos, rot.getQuaternion(), this->infinite);
}

void Point3D::multGLMatrixInverse() const {
	//rot.multGLMatrixTranspose();
	//glTranslatef(-pos.x,-pos.y,-pos.z);
	GraphicsEnvironment::getSingleton()->getRenderer()->transformInverse(rot.getQuaternion());
	GraphicsEnvironment::getSingleton()->getRenderer()->translate(-pos.x,-pos.y,-pos.z);
	// TODO: is_infinite
}

/*#ifdef _DEBUG

Vector AllocInfo::allocs;

AllocInfo::AllocInfo(unsigned int address,unsigned int size, const char *file, int line) {
this->address = address;
this->size = size;
strcpy(this->file,file);
this->line = line;
}

bool RemoveTrack(DWORD ptr) {
for(int i=0;i<AllocInfo::allocs.size();i++) {
AllocInfo *allocInfo = static_cast<AllocInfo *>(AllocInfo::allocs.get(i));
if(allocInfo->address == ptr) {
AllocInfo::allocs.remove(i);
return true;
}
}
// trying to delete something not allocated / already deleted
Vision::setError(new VisionException((void *)ptr,VisionException::V_ILLEGAL_DELETE,"Tried to delete something not allocated or already deleted"));
return false;
}

void dumpAllocs() {
unsigned int total = 0;
FILE *file = fopen("c:\\windows\\desktop\\vision_log.txt","a+");
fprintf(file,"\nVision Dump..\n\n");
for(int i=0;i<AllocInfo::allocs.size();i++) {
AllocInfo *allocInfo = static_cast<AllocInfo *>(AllocInfo::allocs.get(i));
fprintf(file,"%-50s:\t\tLINE %d,\t\tADDRESS %d\t%d unfreed\n",
allocInfo->file,allocInfo->line,allocInfo->address,allocInfo->size);
total += allocInfo->size;
}
fprintf(file, "-----------------------------------------------------------\n");
fprintf(file, "Total Unfreed: %d bytes\n", total);
fclose(file);
};

#endif*/


