#pragma once

#include "Resource.h"
#include "VisionIface.h"

struct FIBITMAP;

struct rgb_struct {
	unsigned char r, g, b;
};

struct rgba_struct {
	unsigned char r, g, b, a;
};

class Image2D : public VisionObject, public VI_Image {
protected:
	bool load(const char *filename);
	FIBITMAP *toFreeImage() const;
	void fromFreeImage(FIBITMAP *dib);
	void adjustBrightness1D(unsigned char bright, bool xdir);

	void *rgbdata;
	//struct rgb_struct *mask;
	int w,h;
	bool alpha;
public:

	Image2D();
	Image2D(int w,int h);
	Image2D(int w,int h,bool alpha);
	Image2D(int w, int h, unsigned char r, unsigned char g, unsigned char b, bool alpha, unsigned char a);
	Image2D(void *data,int w,int h);
	virtual ~Image2D();
	virtual V_CLASS_t getClass() const { return V_CLASS_IMAGE; };
	virtual bool isSubClass(V_CLASS_t v_class) {
		return ( v_class == V_CLASS_IMAGE );
	}
	virtual size_t memUsage();

	//void put(int x, int y, rgb_struct *c);
	//rgb_struct *get(int x,int y);
	/*rgb_struct *getMask() {
		return (rgb_struct *)this->mask;
	}
	const rgb_struct *getMask() const {
		return (const rgb_struct *)this->mask;
	}
	void createMask();
	void createMask(rgb_struct maskcol);*/

	static Image2D *loadImage(const char *filename);
	struct GlobalFontInfo {
		int descent;
		int height;
		GlobalFontInfo() : descent(0), height(0) {
		}
	};
	struct FontInfo {
		int advance_x;
		int offset_x;
		int offset_y;
		FontInfo() : advance_x(0), offset_x(0), offset_y(0) {
		}
	};
	static int loadFont(Image2D ***images, GlobalFontInfo *globalFontInfo, FontInfo **fontInfo, const char *font, int size, int char_start, int n_chars);

	//static Image2D *createNoise(int w,int h);
	static Image2D *createNoise(int w,int h,float scale_u,float scale_v,const unsigned char filter_max[3],const unsigned char filter_min[3],V_NOISEMODE_t noisemode,int n_iterations,bool maximise);
	static Image2D *createRadial(int w,int h,const unsigned char filter_max[3],const unsigned char filter_min[3],bool squared,bool alpha);

	/*void setAlpha(bool alpha) {
		this->alpha = alpha;
	}*/

	// interface
	/*virtual V_TAG_t getTag() const {
		return this->tag;
	}*/

	virtual int getWidth() const {
		return this->w;
	}
	virtual int getHeight() const {
		return this->h;
	}
	virtual bool getAlpha() const {
		return this->alpha;
	}
	virtual bool makePowerOf2();
	virtual bool makeSamePowerOf2();
	virtual unsigned char *getData() {
		return (unsigned char *)this->rgbdata;
	}
	virtual const unsigned char *getData() const {
		return (const unsigned char *)this->rgbdata;
	}
	virtual void putRGB(int x, int y, unsigned char r, unsigned char g, unsigned char b);
	virtual void putRGBA(int x, int y, unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	virtual void getRGB(unsigned char *r, unsigned char *g, unsigned char *b, int x, int y) const;
	virtual void getRGBA(unsigned char *r, unsigned char *g, unsigned char *b, unsigned char *a, int x, int y) const;
	virtual const void *get(int x,int y) const;

	virtual void setAllToi(unsigned char r, unsigned char g, unsigned char b);
	virtual void multiply(float scaleR, float scaleG, float scaleB);
	virtual void adjustBrightness(unsigned char bright);
	virtual void merge(VI_Image *image);
	virtual bool scale(int newWidth, int newHeight);
	virtual void flip(bool flipX, bool flipY);

	virtual VI_Image *copy() const;
	virtual VI_Image *createDerivative(float scale) const;
	virtual VI_Image *createReflection2D() const;
	virtual bool save(const char *filename,const char *ext) const;
};
