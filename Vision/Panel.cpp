//---------------------------------------------------------------------------
#include <cstring>
#include <cassert>

#include <algorithm>
using std::min;
using std::max;

#include "Panel.h"
#include "InputHandler.h"
#include "Renderer.h"

//---------------------------------------------------------------------------

const int scrollbar_w = 16;
const int arrow_width_c = 4;
const int arrow_height_c = 9;
const int scrollbar_buttons_h = 4*arrow_height_c + 8;

Panel::Panel() : VisionObject() {
	parent = NULL;
	//children = new vector<Panel *>();
	bgcolor.seti(0,0,0,0);
	fgcolor.seti(255,255,255,255);
	highlightcolor.seti(255, 0, 0);
	this->texture = NULL;
	this->x=0;
	this->y=0;
	this->width=0;
	this->height=0;
	this->from_left = true;
	this->from_top = true;
	this->mouseClickEventFunc = NULL;
	this->mouseClickEventData = NULL;
	this->mouseReleaseEventFunc = NULL;
	this->mouseReleaseEventData = NULL;
	this->keyDownEventFunc = NULL;
	this->keyDownEventData = NULL;
	this->paintPanel = NULL;
	this->paintData = NULL;
	this->action = NULL;
	this->mouse_enter_action = NULL;
	this->mouse_exit_action = NULL;
	this->userData = NULL;
	this->has_shortcut = false;
	this->shortcut = -1;
	this->mouse_over = false;
	this->last_event_time_ms = 0;
	this->visible = true;
	this->enabled = true;
	this->has_input_focus = false;
	this->show_popup = false;
	this->popup_x = -1;
	this->popup_y = -1;
	this->popup_width = -1;
	this->popup_height = -1;
	this->scrollbar_v_pressed = false;
	this->scrollbar_v_pressed_up = false;
	this->scrollbar_v_pressed_down = false;
	this->scrollbar_h_pressed = false;
	this->scrollbar_h_pressed_up = false;
	this->scrollbar_h_pressed_down = false;
}

Panel::~Panel() {
	/*if( children != NULL )
		delete children;
	children = NULL;*/
	GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
	if( genv->getPopup() == this ) {
		genv->setPopup(NULL);
	}

	this->detachFromParentPanel(); // detach from any parent
	for(vector<Panel *>::iterator iter = children.begin(); iter != children.end(); ++iter) {
		Panel *child = *iter;
		child->parent = NULL; // needed so we don't remove from this node's children vector when deleting the child
		delete child;
	}
}

size_t Panel::memUsage() {
	size_t size = sizeof(this);
	size += children.size() * sizeof(Panel *);
	return size;
}

void Panel::setPopupText(const char *popup_text, const VI_Font *font) {
	this->popup_text = popup_text;
	this->popup_font = static_cast<const FontBuffer *>(font); // cast to the internal representation
	this->popup_width = popup_font->getWidth(popup_text)+4;
	this->popup_height = popup_font->getHeight()+4;
}

void Panel::addChildPanel(VI_Panel *panel,int x,int y) {
	Panel *d_panel = dynamic_cast<Panel *>(panel); // cast to the internal representation
	//ASSERT( d_panel->parent == NULL );
	if( d_panel->parent != NULL ) {
		d_panel->detachFromParentPanel();
	}
	d_panel->fixed_x = x;
	d_panel->fixed_y = y;
	d_panel->parent = this;
	children.push_back(d_panel);
	validate();
	//d_panel->validate();
}

bool Panel::removeChildPanel(VI_Panel *panel) {
	Panel *d_panel = dynamic_cast<Panel *>(panel); // cast to the internal representation
	/*ASSERT( d_panel->parent == this );
	d_panel->parent = NULL;
	GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
	if( genv->getModalPanel() == panel ) {
		if( this == genv->getPanel() )
			genv->setModalPanel(NULL);
		else
			genv->setModalPanel(this);
	}
	remove_vec(&children, d_panel);*/
	for(size_t i=0;i<children.size();i++) {
		if( children.at(i) == d_panel ) {
			children.erase(children.begin() + i);
			ASSERT( d_panel->parent == this );
			d_panel->parent = NULL;
			GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
			if( genv->getModalPanel() == panel ) {
				if( this == genv->getPanel() )
					genv->setModalPanel(NULL);
				else
					genv->setModalPanel(this);
			}
			return true;
		}
	}
	ASSERT( d_panel->parent != this );
	return false;
}

void Panel::removeAllChildPanels() {
	/*GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
	for(vector<Panel *>::iterator iter = children.begin();iter != children.end();++iter) {
		Panel *panel = (*iter);
		ASSERT( panel->parent == this );
		panel->parent = NULL;
		if( genv->getModalPanel() == panel ) {
			if( this == genv->getPanel() )
				genv->setModalPanel(NULL);
			else
				genv->setModalPanel(this);
		}
	}
	children.clear();*/
	while( this->children.size() > 0 ) {
		Panel *child = this->children.at(0);
		this->removeChildPanel(child);
	}
}

void Panel::detachFromParentPanel() {
	//ASSERT( parent != NULL );
	if( parent != NULL ) {
		this->parent->removeChildPanel(this);
	}
}

void Panel::input(InputSystem *iS) {
	if( !visible )
		return;
	if( !enabled ) {
		inputChildren(iS);
		return;
	}
	int mx = iS->getMouse()->mouseX;
	int my = iS->getMouse()->mouseY;

	// mouse clicks  done in processMouseClickEvent()
	// key shortcuts done in processKeydownEvent()
	/*if( this->action != NULL ) {
		if( ( iS->getMouse()->isLMouseClicked() || iS->getMouse()->isRMouseClicked() ) &&
			( mx >= x && mx < x+width && my >= y && my < y+height ) ) {
			iS->pushed_action = action;
			iS->pushed_action_src = this;
		}
		// key shortcuts done in processKeydownEvent()
	}*/

	bool new_mouse_over = false;
	if(mx >= x && mx < x+width && my >= y && my < y+height) {
		new_mouse_over = true;
		if( this->popup_text.length() > 0 ) {
			GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
			genv->setPopup(this);
		}
	}
	if( new_mouse_over != mouse_over ) {
		if( new_mouse_over && this->popup_text.length() > 0 ) {
			this->show_popup = true;
			this->popup_x = mx + 8;
			this->popup_y = my + 8;
			GraphicsEnvironment *genv = GraphicsEnvironment::getSingleton();
			if( this->popup_x + this->popup_width >= genv->getWidth() ) {
				this->popup_x = genv->getWidth() - 1 - this->popup_width;
			}
			if( this->popup_y + this->popup_height >= genv->getHeight() ) {
				this->popup_y = genv->getHeight() - 1 - this->popup_height;
			}
		}
		else {
			this->show_popup = false;
		}
		if( new_mouse_over && mouse_enter_action != NULL ) {
			//mouse_enter_action(this->tag);
			//mouse_enter_action(this);
			iS->pushed_mouse_enter_action = mouse_enter_action;
			iS->pushed_mouse_enter_action_src = this;
		}
		else if( !new_mouse_over && mouse_exit_action != NULL ) {
			//mouse_exit_action(this->tag);
			//mouse_exit_action(this);
			iS->pushed_mouse_exit_action = mouse_exit_action;
			iS->pushed_mouse_exit_action_src = this;
		}
		mouse_over = new_mouse_over;
	}

	inputChildren(iS);
}

void Panel::processMouseClickEvent(VI_MouseClickEvent *mouse_click_event) {
	if( !visible )
		return;
	if( !enabled ) {
		processMouseClickEventChildren(mouse_click_event);
		return;
	}

	processMouseClickEventChildren(mouse_click_event);

	// must be done after processing children!

	// n.b., we push the action even if event already handled (needed for say, if the user clicks on a listbox changing the entry, we still want to call the action for that entry)
	int mx = mouse_click_event->mouse_x;
	int my = mouse_click_event->mouse_y;
	if( mx >= x && mx < x+width && my >= y && my < y+height ) {
		if( action != NULL && mouse_click_event->pushed_action == NULL ) {
			if( mouse_click_event->left || mouse_click_event->right ) {
				mouse_click_event->pushed_action = action;
				mouse_click_event->pushed_action_src = this;
				mouse_click_event->handled = true;
			}
		}
	}
	if( !mouse_click_event->handled ) {
		if( mx >= x && mx < x+width && my >= y && my < y+height ) {
			if( this->mouseClickEventFunc != NULL ) {
				mouseClickEventFunc(mouse_click_event, mouseClickEventData);
				// let the external function decide whether to set the handled flag or not
			}
		}
	}
}

void Panel::processMouseReleaseEvent(VI_MouseReleaseEvent *mouse_release_event) {
	//LOG("0: %d: %d\n", this, this->getNChildPanels());
	if( !visible )
		return;
	if( !enabled ) {
		processMouseReleaseEventChildren(mouse_release_event);
		return;
	}

	processMouseReleaseEventChildren(mouse_release_event);

	// must be done after processing children!

	//LOG("1\n");
	if( !mouse_release_event->handled ) {
		int mx = mouse_release_event->mouse_x;
		int my = mouse_release_event->mouse_y;
		//LOG("2: %d, %d: %d, %d - %d, %d\n", mx, my, x, y, width, height);
		if( mx >= x && mx < x+width && my >= y && my < y+height ) {
			//LOG("3\n");
			if( this->mouseReleaseEventFunc != NULL ) {
				//LOG("4\n");
				mouseReleaseEventFunc(mouse_release_event, mouseReleaseEventData);
				// let the external function decide whether to set the handled flag or not
			}
		}
	}
}

void Panel::processKeydownEvent(VI_KeyEvent *key_event) {
	if( !visible )
		return;
	if( !enabled ) {
		processKeydownEventChildren(key_event);
		return;
	}

	processKeydownEventChildren(key_event);

	// Behaviour if more than one item has same key shortcut should really be
	// undefined. But doing this after processing the children, and overwriting
	// any pushed action already there, means the highest level item takes
	// priority.
	if( action != NULL && has_shortcut && key_event->code == shortcut ) {
		LOG("%d detect shortcut: %d\n", this, shortcut);
		key_event->pushed_action = action;
		key_event->pushed_action_src = this;
	}
	if( this->keyDownEventFunc != NULL ) {
		keyDownEventFunc(key_event, keyDownEventData);
	}
}

void Panel::inputChildren(InputSystem *iS) {
	// do in reverse order, as children added last have higher priority
	for(size_t i=children.size(); i-- > 0 ; ) {
		Panel *child = children.at(i);
		child->input(iS);
	}
}

void Panel::processMouseClickEventChildren(VI_MouseClickEvent *mouse_click_event) {
	// do in reverse order, as children added last have higher priority
	for(size_t i=children.size(); i-- > 0 && !mouse_click_event->handled; ) {
		Panel *child = children.at(i);
		child->processMouseClickEvent(mouse_click_event);
	}
}

void Panel::processMouseReleaseEventChildren(VI_MouseReleaseEvent *mouse_release_event) {
	// do in reverse order, as children added last have higher priority
	for(size_t i=children.size(); i-- > 0 && !mouse_release_event->handled; ) {
		Panel *child = children.at(i);
		child->processMouseReleaseEvent(mouse_release_event);
	}
}

void Panel::processKeydownEventChildren(VI_KeyEvent *key_event) {
	// do in reverse order, as children added last have higher priority
	for(size_t i=children.size(); i-- > 0 ; ) {
		Panel *child = children.at(i);
		child->processKeydownEvent(key_event);
	}
}

void Panel::paint(Graphics2D *g) {
	if( !visible )
		return;
	int alpha = bgcolor.getA();
	if( alpha > 0 && width > 0 && height > 0 ) {
		if( alpha < 255 ) {
			g->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_TRANSPARENCY);
		}
		g->setColor4i(bgcolor.getR(), bgcolor.getG(), bgcolor.getB(), bgcolor.getA());
		if( this->texture != NULL ) {
			g->setTexture(texture);
		}
		g->fillRect(x,y,width,height);
		if( this->texture != NULL ) {
			g->setTexture(NULL);
		}
		if( alpha < 255 ) {
			g->getRenderer()->setBlendMode(Renderer::RENDERSTATE_BLEND_NONE);
		}
	}
	g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());

	if(paintPanel != NULL) {
		// need to switch to Modelview, for font positioning to work in D3D9!
		//g->getRenderer()->switchToModelview();
		g->getRenderer()->translate((float)this->x, (float)this->y, 0.0f);
		//g->getRenderer()->switchToProjection();
		paintPanel(g, paintData);
		//g->getRenderer()->switchToModelview();
		g->getRenderer()->translate((float)-this->x, (float)-this->y, 0.0f);
		//g->getRenderer()->switchToProjection();
	}

	paintChildren(g);

	//if( this->popup_text.length() > 0 ) {
		/*int mx = iS->getMouse()->mouseX;
		int my = iS->getMouse()->mouseY;

		if( mx >= x && mx < x+width && my >= y && my < y+height ) {
			popup_font->writeText(mx+2, my+2, popup_text.c_str(), popup_text.length());
		}*/
		/*if( this->show_popup ) {
			int popup_width = popup_font->getWidth(popup_text.c_str())+4;
			int popup_height = popup_font->getHeight()+4;
			g->setColor4i(bgcolor.getR(), bgcolor.getG(), bgcolor.getB(), bgcolor.getA());
			g->fillRect(popup_x,popup_y,popup_width,popup_height);
			g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());
			g->drawRect(popup_x,popup_y,popup_width,popup_height);
			popup_font->writeText(popup_x+2, popup_y+6, popup_text.c_str(), popup_text.length());
		}*/
	//}
}

void Panel::drawPopup(Graphics2D *g) {
	if( this->popup_text.length() > 0 ) {
		ASSERT( this->show_popup );
		g->setColor4i(bgcolor.getR(), bgcolor.getG(), bgcolor.getB(), bgcolor.getA());
		g->fillRect(popup_x,popup_y,popup_width,popup_height);
		g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());
		g->drawRect(popup_x,popup_y,popup_width,popup_height);
		popup_font->writeText(popup_x+2, popup_y+6, popup_text.c_str(), popup_text.length());
	}
}

void Panel::paintChildren(Graphics2D *g) {
	// do in forward order, so that children added last appear on top
	for(size_t i=0;i<children.size();i++) {
		Panel *child = children.at(i);
		child->paint(g);
	}
}

void Panel::setMouseClickEventFunc(VI_Panel_MouseClickEvent *mouseClickEventFunc,void *mouseClickEventData) {
	this->mouseClickEventFunc = mouseClickEventFunc;
	this->mouseClickEventData = mouseClickEventData;
}

void Panel::setMouseReleaseEventFunc(VI_Panel_MouseReleaseEvent *mouseReleaseEventFunc,void *mouseReleaseEventData) {
	this->mouseReleaseEventFunc = mouseReleaseEventFunc;
	this->mouseReleaseEventData = mouseReleaseEventData;
}

void Panel::setKeyDownEventFunc(VI_Panel_KeyDownEvent *keyDownEventFunc,void *keyDownEventData) {
	this->keyDownEventFunc = keyDownEventFunc;
	this->keyDownEventData = keyDownEventData;
}

void Panel::setPaintFunc(VI_Panel_Paint *paintPanel,void *paintData) {
	this->paintPanel = paintPanel;
	this->paintData = paintData;
}

void Panel::validate() {
	for(size_t i=0;i<children.size();i++) {
		Panel *child = children.at(i);
		child->x = this->x + child->fixed_x;
		child->y = this->y;
		child->y += ( child->from_top ? child->fixed_y : this->height - 1 - child->fixed_y );
		child->validate();
	}
	/*this->x = this->parent->x + this->fixed_x;
	this->y = this->parent->y;
	this->y += ( this->from_top ? this->fixed_y : this->parent->height - 1 - this->fixed_y );
	for(size_t i=0;i<children.size();i++) {
		Panel *child = children.at(i);
		child->validate();
	}*/
}

void Panel::setEnabled(bool enabled,bool do_children) {
	this->enabled = enabled;
	if( do_children ) {
		for(size_t i=0;i<children.size();i++) {
			Panel *child = children.at(i);
			child->setEnabled(enabled, do_children);
		}
	}
}

void Panel::setBackground(Color bg) {
	this->bgcolor=bg;
}

void Panel::setForeground(Color fg) {
	this->fgcolor=fg;
}

/*void Panel::setOpaque(bool opaque) {
this->opaque=opaque;
}*/

void Panel::setBackgroundAlphai(int alpha) {
	this->bgcolor.setAi(alpha);
}

void Panel::setForeground(float r,float g,float b) {
	Color col;
	col.setf(r, g, b);
	this->setForeground(col);
}

void Panel::setBackground(float r,float g,float b) {
	Color col;
	col.setf(r, g, b);
	this->setBackground(col);
}

void Panel::setForeground(float r,float g,float b,float a) {
	Color col;
	col.setf(r, g, b);
	col.setAf(a);
	this->setForeground(col);
}

void Panel::setBackground(float r,float g,float b,float a) {
	Color col;
	col.setf(r, g, b);
	col.setAf(a);
	this->setBackground(col);
}

void Panel::setHighlightColor(unsigned char r, unsigned char g, unsigned char b) {
	highlightcolor.seti(r, g, b);
}

void Panel::setAction(VI_Panel_Action *action) {
	this->action=action;
}

VI_Panel *Panel::find(int fx, int fy) {
	if( !visible )
		return NULL;

	for(size_t i=0;i<children.size();i++) {
		Panel *child = children.at(i);
		VI_Panel *found = child->find(fx, fy);
		if( found != NULL ) {
			return found;
		}
	}

	if( fx >= x && fx < x+width && fy >= y && fy < y+height ) {
		return this;
	}
	return NULL;
}

void Panel::inputScrollbar(InputSystem *iS, bool *moved, size_t *new_position, size_t n_viewentries, size_t n_entries, bool vertical) {
	if( vertical )
		this->scrollbar_v_pressed = false;
	else
		this->scrollbar_h_pressed = false;
	*moved = false;
	//if( n_entries - n_viewentries > 0 ) {
	if( n_entries > n_viewentries ) {
		int twidth = width - scrollbar_w; // for horizontal scrollbar
		int scroll_h = vertical ? height - scrollbar_buttons_h : twidth - scrollbar_buttons_h;
		if( iS->getMouse()->isLMousePressed() ) {
			int mx = iS->getMouse()->mouseX;
			int my = iS->getMouse()->mouseY;
			int buttons_y = vertical ? y+height-scrollbar_buttons_h : x+twidth-scrollbar_buttons_h;
			int sx = vertical ? x+width-scrollbar_w : y+height-scrollbar_w;
			int ex = vertical ? x+width-1 : y+height-1;
			bool ok = false;
			if( vertical && mx >= sx && mx <= ex && my >= y && my < buttons_y && buttons_y > y ) {
				ok = true;
			}
			else if( !vertical && my >= sx && my <= ex && mx >= x && mx < buttons_y && buttons_y > x ) {
				ok = true;
			}
			if( ok ) {
				int scroll_h = vertical ? height - scrollbar_buttons_h : twidth - scrollbar_buttons_h;
				float alpha = ((float)n_viewentries) / (float)n_entries;
				int scrollblock_h = (int)(alpha * scroll_h);

				int m_off = vertical ? my - y : mx - x;
				float frac = ((float)(m_off - scrollblock_h/2)) / (float)(buttons_y - y);
				if( vertical )
					this->scrollbar_v_pressed = true;
				else
					this->scrollbar_h_pressed = true;
				//LOG(": %d, %d\n", scrollbar_v_pressed, scrollbar_h_pressed);
				*moved = true;
				if( frac < 0.0 )
					frac = 0.0;
				*new_position = (size_t)(frac * n_entries);
				if( *new_position >= n_entries - n_viewentries )
					*new_position = n_entries - n_viewentries;
			}
		}
	}
}

void Panel::paintScrollbar(Graphics2D *g, size_t position, size_t n_viewentries, size_t n_entries, bool vertical) {
	//if( position > 0 || position + n_viewentries < n_entries ) {
	//if( n_entries - n_viewentries > 0 ) {
	if( n_entries > n_viewentries ) {
		int twidth = width - scrollbar_w; // for horizontal scrollbar
		g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());

		int scroll_h = vertical ? height - scrollbar_buttons_h : twidth - scrollbar_buttons_h;
		float alpha0 = ((float)position) / (float)n_entries;
		float alpha1 = ((float)(position+n_viewentries)) / (float)n_entries;
		int top = vertical ? y : x;
		int y0 = (int)(top + alpha0 * scroll_h);
		int y1 = (int)(top + alpha1 * scroll_h);
		if( vertical ? this->scrollbar_v_pressed : this->scrollbar_h_pressed )
			g->setColor4i(highlightcolor.getR(), highlightcolor.getG(), highlightcolor.getB(), highlightcolor.getA());
		if( vertical ) {
			g->fillRect(x+width-scrollbar_w, y0, scrollbar_w-1, y1-y0);
		}
		else {
			g->fillRect(y0, y+height-scrollbar_w, y1-y0, scrollbar_w-1);
		}
		if( vertical ? this->scrollbar_v_pressed : this->scrollbar_h_pressed )
			g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());

		int cx = vertical ? x+width-scrollbar_w/2 : y+height-scrollbar_w/2;
		int buttons_y = vertical ? y+height-scrollbar_buttons_h : x+twidth-scrollbar_buttons_h;
		if( vertical ) {
			g->draw(x+width-scrollbar_w,y,x+width-scrollbar_w,y+height-1);
			g->draw(x+width-scrollbar_w,buttons_y,x+width-1,buttons_y);
			g->draw(x+width-scrollbar_w,buttons_y+scrollbar_buttons_h/2,x+width-1,buttons_y+scrollbar_buttons_h/2);
		}
		else {
			g->draw(x, y+height-scrollbar_w, x+twidth-1, y+height-scrollbar_w);
			g->draw(buttons_y, y+height-scrollbar_w, buttons_y, y+height-1);
			g->draw(buttons_y+scrollbar_buttons_h/2, y+height-scrollbar_w, buttons_y+scrollbar_buttons_h/2, y+height-1);
			// need extra line in case no vertical scrollbar
			g->draw(buttons_y+scrollbar_buttons_h, y+height-scrollbar_w, buttons_y+scrollbar_buttons_h, y+height-1);
		}
		if( position > 0 ) {
			int cy = buttons_y+scrollbar_buttons_h/4;
			//g->fillRect(cx-2,cy,4,4);
			//g->setColor4i(bgcolor.getR(), bgcolor.getG(), bgcolor.getB(), bgcolor.getA());
			if( vertical ? this->scrollbar_v_pressed_up : this->scrollbar_h_pressed_up )
				g->setColor4i(highlightcolor.getR(), highlightcolor.getG(), highlightcolor.getB(), highlightcolor.getA());
			short vx[3], vy[3];
			//if( vertical ) {
				vx[0] = cx; vy[0] = cy-arrow_height_c;
				vx[1] = cx+arrow_width_c; vy[1] = cy+arrow_height_c;
				vx[2] = cx-arrow_width_c; vy[2] = cy+arrow_height_c;
			/*}
			else {
				vy[0] = cx; vx[0] = cy-arrow_height_c;
				vy[1] = cx+arrow_width_c; vx[1] = cy+arrow_height_c;
				vy[2] = cx-arrow_width_c; vx[2] = cy+arrow_height_c;
			}*/
			if( vertical )
				g->fillTriangle(vx, vy);
			else
				g->fillTriangle(vy, vx);
			//g->fillRect(cx-0.5*arrow_width_c, cy-0.5*arrow_height_c, arrow_width_c, arrow_height_c);
			/*g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());
			g->move(vx[0], vy[0]);
			for(int i=1;i<=3;i++) {
				g->draw(vx[i%3], vy[i%3]);
			}*/
			if( vertical ? this->scrollbar_v_pressed_up : this->scrollbar_h_pressed_up )
				g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());
		}
		if( position + n_viewentries < n_entries ) {
			int cy = buttons_y+3*scrollbar_buttons_h/4;
			//g->fillRect(cx-2,cy,4,4);
			//g->setColor4i(bgcolor.getR(), bgcolor.getG(), bgcolor.getB(), bgcolor.getA());
			if( vertical ? this->scrollbar_v_pressed_down : this->scrollbar_h_pressed_down )
				g->setColor4i(highlightcolor.getR(), highlightcolor.getG(), highlightcolor.getB(), highlightcolor.getA());
			short vx[3], vy[3];
			vx[0] = cx; vy[0] = cy+arrow_height_c;
			vx[1] = cx+arrow_width_c; vy[1] = cy-arrow_height_c;
			vx[2] = cx-arrow_width_c; vy[2] = cy-arrow_height_c;
			if( vertical )
				g->fillTriangle(vx, vy);
			else
				g->fillTriangle(vy, vx);
			//g->fillRect(cx-0.5*arrow_width_c, cy-0.5*arrow_height_c, arrow_width_c, arrow_height_c);
			/*g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());
			g->move(vx[0], vy[0]);
			for(int i=1;i<=3;i++) {
				g->draw(vx[i%3], vy[i%3]);
			}*/
			if( vertical ? this->scrollbar_v_pressed_down : this->scrollbar_h_pressed_down )
				g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());
		}
	}
}

FlowPanel::FlowPanel(bool across,int size) : Panel() {
	this->across=across;
	this->size=size;
	this->marginX=2;
	this->marginY=2;
}

void FlowPanel::addChildPanel(VI_Panel *panel) {
	Panel::addChildPanel(panel,0,0);
}

void FlowPanel::validate() {
	Panel::validate();
	width = marginX;
	height = 0;
	for(size_t i=0;i<children.size();i++) {
		Panel *child = children.at(i);
		/*child->x = x + width;
		child->y = y + marginY;*/
		/*child->setX(x + width);
		child->setY(y + marginY);*/
		child->setPos(x + width, y + marginY);
		width += child->getWidth();
		if(child->getHeight() > height)
			height = child->getHeight();
	}
	width += marginX;
	height += 2*marginY;
}

/*Button::Button() : Panel() {
	this->font = NULL;
}*/

Button::Button(const char *text,const FontBuffer *font) : Panel() {
	init(text,font);
}

void Button::init(const char *text,const FontBuffer *font) {
	this->text = text;
	this->font=font;
	this->width = font->getWidth(text)+4;
	this->height = font->getHeight()+4;
	this->border = true;

	//opaque = true;
	//alpha = 1.0;
	bgcolor.seti(127,127,127,255);
	fgcolor.seti(255,255,255,255);
}

Button::~Button() {
}

/*void Button::input(InputSystem *iS) {
	Panel::input(iS);
}*/

void Button::paint(Graphics2D *g) {
	Panel::paint(g);
	if( !visible )
		return;
	if( border )
		g->drawRect(x,y,width,height);
	//g->setColor(1.0,0.0,0.0);
	//g->drawRect(x,y,2,2);
	//g->fillRect(x,y,2,2);
	font->writeText(x+2,y+6,text.c_str(),text.length());
	//font->writeText(x+2,y,text.c_str(),text.length());
}

void Button::setText(const char *text) {
	this->text = text;
	this->width = font->getWidth(text)+4;
	if( this->parent != NULL ) {
		this->parent->validate();
	}
}

ImageButton::ImageButton(VI_Texture *texture) : Panel(), texture(texture), has_tex_coords(false) {
	this->width = texture->getWidth();
	this->height = texture->getHeight();
}

ImageButton::ImageButton(VI_Texture *texture, int width, int height) : Panel(), texture(texture), has_tex_coords(false) {
	this->width = width;
	this->height = height;
}

ImageButton::~ImageButton() {
}

/*void ImageButton::input(InputSystem *iS) {
	Panel::input(iS);
}*/

void ImageButton::paint(Graphics2D *g) {
	if( visible ) {
		//texture->draw(x, y + height);
		g->setTexture(texture);
		g->setColor4i(255, 255, 255, 255);
		if( this->has_tex_coords )
			g->fillRect(x,y,width,height,tu,tv);
		else
			g->fillRect(x,y,width,height);
		g->setTexture(NULL);
		// TODO: fix crash issue for OpenGL
	}
	Panel::paint(g); // n.b., do this after, so that a custom paintPanel by the user can draw over the texture
}

Cycle::Cycle() : Panel() {
	/*this->entries = NULL;
	this->n_entries = 0;*/
	this->font = NULL;
	this->active = 0;
}

Cycle::Cycle(char **entries,const FontBuffer *font) : Panel() {
	init(entries,font);
}

Cycle::~Cycle() {
}

void Cycle::init(char **entries,const FontBuffer *font) {
	/*this->entries = entries;
	this->n_entries = 0;*/
	this->entries.clear();
	this->active = 0;
	this->font = font;

	this->width = 4;
	this->height = font->getHeight()+4;

	if(entries != NULL) {
		char **ptr = entries;
		while( *ptr != NULL ) {
			char *text = *ptr;
			/*int twidth = font->getWidth(text)+4;
			if( twidth > width )
				width = twidth;*/
			this->updateWidth(text);

			//n_entries++;
			string entry(text);
			this->entries.push_back(entry);
			ptr++;
		}
	}
}

void Cycle::updateWidth(const char *entry) {
	int twidth = font->getWidth(entry)+4;
	if( twidth > width )
		width = twidth;
}

void Cycle::addEntry(const char *entry) {
	this->updateWidth(entry);
	this->entries.push_back(entry);
}

/*void Cycle::input(InputSystem *iS) {
	if( !visible )
		return;
	if( !enabled ) {
		Panel::input(iS);
		return;
	}
	if(iS->getMouse()->isLMouseClicked()) {
		int mx = iS->getMouse()->mouseX;
		int my = iS->getMouse()->mouseY;
		if(mx >= x && mx < x+width && my >= y && my < y+height) {
			this->active++;
			if( this->active >= this->getNEntries() )
				this->active = 0;
		}
	}
	Panel::input(iS);
}*/

void Cycle::processMouseClickEvent(VI_MouseClickEvent *mouse_click_event) {
	if( !visible )
		return;
	if( !enabled ) {
		Panel::processMouseClickEvent(mouse_click_event);
		return;
	}

	if( !mouse_click_event->handled && mouse_click_event->left ) {
		int mx = mouse_click_event->mouse_x;
		int my = mouse_click_event->mouse_y;
		if( mx >= x && mx < x+width && my >= y && my < y+height) {
			mouse_click_event->handled = true;
			this->active++;
			if( this->active >= this->getNEntries() )
				this->active = 0;
		}
	}

	Panel::processMouseClickEvent(mouse_click_event);
}

void Cycle::paint(Graphics2D *g) {
	Panel::paint(g);
	if( !visible )
		return;
	g->drawRect(x,y,width,height);
	//char *text = this->entries[ this->active ];
	string text = this->entries.at( this->active );
	font->writeText(x+2,y+4,text.c_str(),text.length());
}

Listbox::Listbox(int width,int height,char **entries,const FontBuffer *font) : Panel() {
	this->width = width;
	this->height = height;
	this->font = font;
	this->position = 0;
	this->active = -1;
	this->allow_select = true;
	this->scroll_x = 0;
	this->max_width_pixels = 0;

	reset(entries);
}

Listbox::Listbox(int width,int height,const string *entries,int n_entries,const FontBuffer *font) {
	this->width = width;
	this->height = height;
	this->font = font;
	this->position = 0;
	this->active = -1;
	this->allow_select = true;
	this->scroll_x = 0;
	this->max_width_pixels = 0;

	reset(entries, n_entries);
}

void Listbox::prepare() {
	// be careful for unsigned comparison!
	if( this->active != -1 && this->active > (int)this->entries.size()-1 ) {
		this->active = (int)(this->entries.size()-1);
	}
	if( this->position > (int)this->entries.size()-1 ) {
		this->position = (int)this->entries.size()-1;
	}

	this->scroll_x = 0;

	this->max_width_pixels = 0;
	for(size_t i=0;i<this->entries.size();i++) {
		string text = this->entries.at(i);
		int width_pixels = font->getWidth(text.c_str());
		max_width_pixels = max(max_width_pixels, width_pixels);
	}
}

bool Listbox::hasHScrollbar() const {
	return width - scrollbar_w < max_width_pixels;
}

const int vspace = 2;

int Listbox::getNViewEntries() const {
	int theight = height;
	if( this->hasHScrollbar() )
		theight -= scrollbar_w;
	int n_viewentries = theight / ( font->getHeight()+vspace );
	return n_viewentries;
}

void Listbox::reset(char **entries) {
	/*this->entries = entries;
	this->n_entries = 0;*/
	this->entries.clear();
	/*this->position = 0;
	this->active = -1;*/

	if(entries != NULL) {
		char **ptr = entries;
		while( *ptr != NULL ) {
			//n_entries++;
			char *text = *ptr;
			string entry(text);
			this->entries.push_back(entry);
			ptr++;
		}
	}

	this->prepare();
}

void Listbox::reset(const string *entries, int n_entries) {
	this->entries.clear();
	/*this->position = 0;
	this->active = -1;*/

	for(int i=0;i<n_entries;i++) {
		this->entries.push_back(entries[i]);
	}

	this->prepare();
}

void Listbox::input(InputSystem *iS) {
	if( !visible )
		return;
	if( !enabled ) {
		Panel::input(iS);
		return;
	}
	// mouse clicks now done in processMouseClickEvent
	/*if(iS->getMouse()->isLMouseClicked() || iS->getMouse()->isRMouseClicked() ) {
		int mx = iS->getMouse()->mouseX;
		int my = iS->getMouse()->mouseY;
		int twidth = width - scrollbar_w;
		if(mx >= x && mx < x+twidth && my >= y && my < y+height) {
			int ypos = my - y + vspace;
			int off = ypos / ( font->getHeight()+vspace );
			int index = this->position + off;
			if( index >= 0 && index < this->getNEntries() ) {
				this->active = index;
			}
			else {
				//this->active = -1;
				// don't change active
			}
		}
	}*/

	int time_ms = Vision::getGameTimeMS();
	const int delay = (int)(0.05f * 1000.0f);
	int mx = iS->getMouse()->mouseX;
	int my = iS->getMouse()->mouseY;
	//int n_viewentries = height / ( font->getHeight()+vspace );
	int n_viewentries = getNViewEntries();

	// mouse press on scrollbar
	bool moved = false;
	size_t new_position = 0;
	inputScrollbar(iS, &moved, &new_position, n_viewentries, this->getNEntries(), true);
	if( moved ) {
		this->position = (int)new_position;
	}
	inputScrollbar(iS, &moved, &new_position, width - scrollbar_w, max_width_pixels, false);
	if( moved ) {
		this->scroll_x = (int)new_position;
	}

	// mouse press on scrollbar buttons
	if( iS->getMouse()->isLMousePressed() ) {
		int buttons_y = y+height-scrollbar_buttons_h;
		int sx = x+width-scrollbar_w;
		int ex = x+width-1;
		if( mx >= sx && mx <= ex && my >= buttons_y && my < y+height ) {
			if( time_ms > last_event_time_ms + delay ) {
				last_event_time_ms = time_ms;
				int midy = buttons_y+scrollbar_buttons_h/2;
				if( my < midy ) {
					if( this->position > 0 ) {
						// up
						this->position--;
						this->scrollbar_v_pressed_up = true;
						this->scrollbar_v_pressed_down = false;
					}
				}
				else {
					if( this->position + n_viewentries < this->getNEntries() ) {
						// down
						this->position++;
						this->scrollbar_v_pressed_up = false;
						this->scrollbar_v_pressed_down = true;
					}
				}
			}
		}
		else {
			this->scrollbar_v_pressed_up = false;
			this->scrollbar_v_pressed_down = false;
		}

		int buttons_x = x+width-scrollbar_w-scrollbar_buttons_h;
		int sy = y+height-scrollbar_w;
		int ey = y+height-1;
		if( mx >= buttons_x && mx < x+width-scrollbar_w && my >= sy && my < ey ) {
			if( time_ms > last_event_time_ms + delay ) {
				last_event_time_ms = time_ms;
				int midx = buttons_x+scrollbar_buttons_h/2;
				if( mx < midx ) {
					if( this->scroll_x > 0 ) {
						// left
						this->scroll_x -= 4;
						scroll_x = max(scroll_x, 0);
						this->scrollbar_h_pressed_up = true;
						this->scrollbar_h_pressed_down = false;
					}
				}
				else {
					if( this->scroll_x + width-scrollbar_w < max_width_pixels ) {
						// right
						this->scroll_x += 4;
						scroll_x = min(scroll_x, max_width_pixels - (width-scrollbar_w));
						scroll_x = max(scroll_x, 0);
						this->scrollbar_h_pressed_up = false;
						this->scrollbar_h_pressed_down = true;
					}
				}
			}
		}
		else {
			this->scrollbar_h_pressed_up = false;
			this->scrollbar_h_pressed_down = false;
		}
	}
	else {
		this->scrollbar_v_pressed_up = false;
		this->scrollbar_v_pressed_down = false;
		this->scrollbar_h_pressed_up = false;
		this->scrollbar_h_pressed_down = false;
	}

	// mouse wheel now done in processMouseClickEvent
	/*if( mx >= x && mx < x+width && my >= y && my < y+height ) {
		if( iS->getMouse()->readMouseWheelUp() ) {
			if( this->position > 0 )
				this->position--;
		}
		else if( iS->getMouse()->readMouseWheelDown() ) {
			if( this->position + n_viewentries < this->getNEntries() )
				this->position++;
		}
	}*/
	// keyboard input handled in processKeydownEvent()
	// keyboard up/down
	/*if( this->has_input_focus ) {
		bool repeat = keyRepeatHandler.isNewRepeat(iS->getKeyboard()->keyAny(), time);
		if( iS->getKeyboard()->wasPressed(V_UP, true) ) {
			if( time > last_event_time + delay ) {
				last_event_time = time;
				if( this->position > 0 )
					this->position--;
			}
		}
		if( iS->getKeyboard()->wasPressed(V_DOWN, true) ) {
			if( time > last_event_time + delay ) {
				last_event_time = time;
				int n_viewentries = height / ( font->getHeight()+vspace );
				if( this->position + n_viewentries < this->getNEntries() )
					this->position++;
			}
		}
		if( iS->getKeyboard()->wasPressed(V_HOME) ) {
			this->position = 0;
		}
		if( iS->getKeyboard()->wasPressed(V_END) ) {
			int n_viewentries = height / ( font->getHeight()+vspace );
			this->position = this->getNEntries() - n_viewentries;
			if( this->position < 0 )
				this->position = 0;
		}
		if( iS->getKeyboard()->wasPressed(V_PGUP, repeat) ) {
			//if( time > last_event_time + delay )
			{
				//last_event_time = time;
				int n_viewentries = height / ( font->getHeight()+vspace );
				this->position -= n_viewentries;
				if( this->position < 0 )
					this->position = 0;
			}
		}
		if( iS->getKeyboard()->wasPressed(V_PGDOWN, repeat) ) {
			//if( time > last_event_time + delay )
			{
				//last_event_time = time;
				int n_viewentries = height / ( font->getHeight()+vspace );
				this->position += n_viewentries;
				if( this->position > this->getNEntries() - n_viewentries ) {
					this->position = this->getNEntries() - n_viewentries;
					if( this->position < 0 )
						this->position = 0;
				}
			}
		}
	}*/
	Panel::input(iS);
}

void Listbox::processMouseClickEvent(VI_MouseClickEvent *mouse_click_event) {
	if( !visible )
		return;
	if( !enabled ) {
		Panel::processMouseClickEvent(mouse_click_event);
		return;
	}

	if( !mouse_click_event->handled && this->allow_select ) {
		int mx = mouse_click_event->mouse_x;
		int my = mouse_click_event->mouse_y;

		if( mouse_click_event->left || mouse_click_event->right ) {
			int twidth = width - scrollbar_w;
			int theight = height;
			if( this->hasHScrollbar() )
				theight -= scrollbar_w;
			if( mx >= x && mx < x+twidth && my >= y && my < y+theight ) {
				mouse_click_event->handled = true;
				int ypos = my - y + vspace;
				int off = ypos / ( font->getHeight()+vspace );
				int index = this->position + off;
				if( index >= 0 && index < this->getNEntries() ) {
					this->active = index;
				}
				else {
					//this->active = -1;
					// don't change active
				}
			}
		}

		// mousewheel
		if( mx >= x && mx < x+width && my >= y && my < y+height ) {
			//int n_viewentries = height / ( font->getHeight()+vspace );
			int n_viewentries = getNViewEntries();
			if( mouse_click_event->wheel_up ) {
				mouse_click_event->handled = true;
				if( this->position > 0 )
					this->position--;
			}
			else if( mouse_click_event->wheel_down ) {
				mouse_click_event->handled = true;
				if( this->position + n_viewentries < this->getNEntries() )
					this->position++;
			}
		}
	}

	Panel::processMouseClickEvent(mouse_click_event);
}

void Listbox::processKeydownEvent(VI_KeyEvent *key_event) {
	if( !visible )
		return;
	if( !enabled ) {
		processKeydownEventChildren(key_event);
		return;
	}

	//int n_viewentries = height / ( font->getHeight()+vspace );
	int n_viewentries = getNViewEntries();
	// keyboard up/down
	if( this->has_input_focus ) {
		if( key_event->code == V_UP ) {
			if( this->position > 0 )
				this->position--;
		}
		else if( key_event->code == V_DOWN ) {
			if( this->position + n_viewentries < this->getNEntries() )
				this->position++;
		}
		else if( key_event->code == V_LEFT ) {
			this->scroll_x -= 4;
			scroll_x = max(scroll_x, 0);
		}
		else if( key_event->code == V_RIGHT ) {
			this->scroll_x += 4;
			scroll_x = min(scroll_x, max_width_pixels - (width-scrollbar_w));
			scroll_x = max(scroll_x, 0);
		}
		else if( key_event->code == V_HOME ) {
			this->position = 0;
		}
		else if( key_event->code == V_END ) {
			this->position = this->getNEntries() - n_viewentries;
			if( this->position < 0 )
				this->position = 0;
		}
		else if( key_event->code == V_PGUP ) {
			this->position -= n_viewentries;
			if( this->position < 0 )
				this->position = 0;
		}
		else if( key_event->code == V_PGDOWN ) {
			this->position += n_viewentries;
			if( this->position > this->getNEntries() - n_viewentries ) {
				this->position = this->getNEntries() - n_viewentries;
				if( this->position < 0 )
					this->position = 0;
			}
		}
	}

	processKeydownEventChildren(key_event);
}

void Listbox::paint(Graphics2D *g) {
	Panel::paint(g);
	if( !visible )
		return;
	g->drawRect(x,y,width,height);
	g->getRenderer()->enableScissor(x, y, width - scrollbar_w + 1, height);
	//int twidth = width - scrollbar_w;
	//int n_viewentries = height / ( font->getHeight()+vspace );
	int n_viewentries = getNViewEntries();
	int ypos = y + 2 * vspace;
	//max_width_pixels = 0;
	for(int i=0;i<n_viewentries;i++) {
		int index = this->position + i;
		if( index == this->getNEntries() )
			break;
		if( index == this->active ) {
			g->setColor3i(255,0,0); // highlight
			g->setColor4i(highlightcolor.getR(), highlightcolor.getG(), highlightcolor.getB(), fgcolor.getA());
		}
		else {
			g->setColor4i(fgcolor.getR(), fgcolor.getG(), fgcolor.getB(), fgcolor.getA());
		}
		string text = this->entries.at(index);
		//LOG("scroll_x: %d\n", scroll_x);
		font->writeText(x+2-scroll_x,ypos,text.c_str(),text.length());
		/*int width_pixels = font->writeText(x+2-scroll_x,ypos,text.c_str(),text.length());
		max_width_pixels = max(max_width_pixels, width_pixels);*/
		ypos += font->getHeight()+vspace;
	}
	g->getRenderer()->disableScissor();

	if( scroll_x + width-scrollbar_w > max_width_pixels ) {
		scroll_x = max_width_pixels - (width-scrollbar_w);
		if( scroll_x < 0 )
			scroll_x = 0;
	}

	// scrollbar
	paintScrollbar(g, position, n_viewentries, this->getNEntries(), true);
	paintScrollbar(g, scroll_x, width - scrollbar_w, max_width_pixels, false);
}


StringGadget::StringGadget() : Panel(), font(NULL), enter_action(NULL), view_pos(0), cursor_pos(0) {
}

StringGadget::StringGadget(const FontBuffer *font,int width) : Panel() {
	init(font,width);
}

void StringGadget::init(const FontBuffer *font,int width) {
	this->read_only = true;
	this->max_len = -1;
	this->font = font;
	this->width = width;
	this->height = font->getHeight()+4;
	//this->text = new StringBuffer();
	this->enter_action = NULL;
	/*this->time_repeat = -1;
	this->lasttime_input = -1;*/
	this->cursor_blink_time_start_ms = Vision::getGameTimeMS();
	this->view_pos = 0;
	this->cursor_pos = 0;
}

StringGadget::~StringGadget() {
	/*if( text != NULL )
	delete text;
	text = NULL;*/
}

void StringGadget::updateView() {
	// updates view_pos so that the cursor is in view
	if( this->text.length() > 0 )
		this->view_pos = min(this->view_pos, this->text.length()-1);
	else {
		this->view_pos = 0;
		return;
	}
	if( this->cursor_pos < this->view_pos ) {
		this->view_pos = this->cursor_pos;
	}
	else if( this->cursor_pos > this->view_pos ) {
		string substr = text.substr(0, this->cursor_pos);
		while( this->view_pos  < substr.length() ) {
			int text_w = this->font->getWidth( substr.substr( this->view_pos ).c_str() );
			if( text_w < width-1 ) {
				break;
			}
			this->view_pos++;
		}
	}
}

void StringGadget::addText(const char *str) {
	bool move_cursor = this->cursor_pos == text.length();
	text.append(str);
	if( move_cursor ) {
		this->cursor_pos = text.length();
		this->updateView();
	}
}

void StringGadget::clear() {
	//this->text->clear();
	text.clear();
	this->cursor_pos = 0;
	this->view_pos = 0;
}

void StringGadget::getText(char **text,size_t *length) const {
	//*length = this->text->getLength();
	*length = this->text.length();
	*text = new char[ *length + 1 ];
	strncpy(*text, this->text.c_str(), *length);
	(*text)[*length] = '\0';
}

void StringGadget::processKeydownEvent(VI_KeyEvent *key_event) {
	if( !visible )
		return;
	if( !enabled || this->read_only ) {
		Panel::processKeydownEvent(key_event);
		return;
	}

	if( key_event->code == V_RETURN ) {
		if( enter_action != NULL ) {
			//enter_action(this->tag);
			//enter_action(this);
			key_event->pushed_action = enter_action;
			key_event->pushed_action_src = this;
		}
	}
	else if( key_event->code == V_BACKSPACE ) {
		ASSERT( this->cursor_pos >= 0 );
		ASSERT( this->cursor_pos <= text.length() );
		if( this->cursor_pos > 0 ) {
			if( this->cursor_pos == 1 ) {
				this->text = text.substr(1);
			}
			else if( this->cursor_pos == text.length() ) {
				text.resize(text.length()-1);
			}
			else {
				string new_text = text.substr(0, this->cursor_pos-1) + text.substr(this->cursor_pos);
				this->text = new_text;
			}
			this->cursor_pos--;
			this->updateView();
		}
	}
	else if( key_event->code == V_DELETE ) {
		ASSERT( this->cursor_pos >= 0 );
		ASSERT( this->cursor_pos <= text.length() );
		if( this->cursor_pos < text.length() ) {
			if( this->cursor_pos == 0 ) {
				this->text = text.substr(1);
			}
			else if( this->cursor_pos == text.length()-1 ) {
				text.resize(text.length()-1);
			}
			else {
				string new_text = text.substr(0, this->cursor_pos) + text.substr(this->cursor_pos+1);
				this->text = new_text;
			}
		}
	}
	else if( key_event->code == V_LEFT ) {
		if( this->cursor_pos > 0 ) {
			this->cursor_pos--;
			this->updateView();
		}
	}
	else if( key_event->code == V_RIGHT ) {
		if( this->cursor_pos < text.length() ) {
			this->cursor_pos++;
			this->updateView();
		}
	}
	else if( key_event->code == V_HOME ) {
		if( this->cursor_pos > 0 ) {
			this->cursor_pos = 0;
			this->updateView();
		}
	}
	else if( key_event->code == V_END ) {
		if( this->cursor_pos < this->text.length() ) {
			this->cursor_pos = this->text.length();
			this->updateView();
		}
	}
	else if( key_event->ascii != 0 && ( this->max_len == -1 || ((int)this->text.length()) < this->max_len ) ) {
		if( isprint(key_event->ascii) || key_event->ascii == 163 || key_event->ascii == 172 ) { // extra characters (�, �) need special casing!
			if( this->blocked_characters.find(key_event->ascii) == string::npos ) { // check not a blocked character
				char buffer[2] = "";
				buffer[0] = key_event->ascii;
				buffer[1] = '\0';
				//this->addText(buffer);
				ASSERT( this->cursor_pos >= 0 );
				ASSERT( this->cursor_pos <= text.length() );
				if( this->cursor_pos == text.length() ) {
					this->text.append(buffer);
				}
				else if( this->cursor_pos == 0 ) {
					this->text = buffer + this->text;
				}
				else {
					string new_text = text.substr(0, this->cursor_pos) + buffer + text.substr(this->cursor_pos);
					this->text = new_text;
				}
				this->cursor_pos++;
				this->updateView();
			}
		}
	}

	Panel::processKeydownEvent(key_event);
}

void StringGadget::paint(Graphics2D *g) {
	Panel::paint(g);
	if( !visible )
		return;
	g->drawRect(x,y,width,height);
	g->getRenderer()->enableScissor(x, y, width, height);
	const int blink_speed_c = (int)(1000.0f  * 0.5);
	int time_ms = Vision::getGameTimeMS() - this->cursor_blink_time_start_ms;
	if( !this->read_only ) {
		string print_text = "";
		ASSERT( this->cursor_pos >= 0 );
		ASSERT( this->cursor_pos <= text.length() );
		if( this->cursor_pos > 0 ) {
			print_text += text.substr(view_pos, cursor_pos - view_pos);
		}
		if( ((int)( time_ms / blink_speed_c )) % 2 != 0 ) {
			print_text += '|';
		}
		else {
			print_text += ' ';
		}
		if( this->cursor_pos < text.length() ) {
			print_text += text.substr(cursor_pos);
		}
		font->writeText(x+2,y+4,print_text.c_str(),print_text.length());
	}
	else {
		//font->writeText(x+2,y+4,text.c_str(),text.length());
		string print_text = text.substr(view_pos);
		font->writeText(x+2,y+4,print_text.c_str(),print_text.length());
		//string blink_text = " " + text;
		//font->writeText(x+2,y+4,blink_text.c_str(),blink_text.length());
	}
	g->getRenderer()->disableScissor();
}

void StringGadget::setEnterAction(VI_Panel_Action *action) {
	this->enter_action = action;
}

TextField::TextField() : StringGadget() {
	//this->cline_off = 0;
	this->cpos = 0;
	//this->lines = NULL;
	//this->line_indxs = new Vector();
	this->line_indxs = new vector<size_t>();
}

TextField::TextField(const FontBuffer *font,int width,int height) : StringGadget(font, width) {
	this->height = height;
	//this->cline_off = 0;
	this->cpos = 0;
	//this->lines = NULL;
	//this->line_indxs = new Vector();
	this->line_indxs = new vector<size_t>();
	this->at_end = true;
}

TextField::~TextField() {
}

void TextField::addText(const char *str) {
	// n.b., subclassed from StringGadget, as we don't want to call updateView
	text.append(str);
	if( this->at_end ) {
		this->cpos = this->text.length()-1;
	}
}

void TextField::paint(Graphics2D *g) {
	Panel::paint(g);
	if( !visible )
		return;

	g->drawRect(x,y,width,height);
	const int LINE_LENGTH = 1024;
	char *buffer = new char[LINE_LENGTH];
	char *dest = buffer;
	//vector<char *> *lines = new vector<char *>();
	vector<char *> lines;

	this->line_indxs->clear();

	//unsigned char *ptr = (unsigned char *)text->getText();
	unsigned const char *ptr = (unsigned const char *)text.c_str();
	int marginX = 2;
	int cx = marginX;
	int twidth = width - scrollbar_w;
	//int height = 0;
	bool c_found = false;
	size_t size = 0;
	size_t start = 0;
	for(;;) {
		bool eol = false;
		if( *ptr == '\n' || *ptr == '\0' )
			eol = true;
		else if( *ptr >= 32 && *ptr < 128 ) {
			cx += font->getWidth(*ptr);
			if(cx > twidth-marginX)
				eol = true;
		}
		if( eol /*&& dest!=buffer*/ ){
			*dest = 0;
			// skip back to previous space
			if(*ptr != '\n' && *ptr != '\0' ) {
				//unsigned char *t_ptr = ptr;
				unsigned const char *t_ptr = ptr;
				while(*dest!=' ' && dest >= buffer) {
					dest--;
					ptr--;
				}
				if(dest >= buffer) {
					*dest = 0;
					ptr++;
				}
				else
					ptr = t_ptr;
			}
			else if( *ptr == '\n' )
				ptr++;
			// add line
			//g->writeText(font,x+marginX,y+cy,buffer);
			//int len = (int)(ptr - (unsigned char *)text->getText());
			size_t len = (size_t)(ptr - (unsigned const char *)text.c_str());
			if( !c_found && this->cpos < len ) {
				c_found = true;
				start = size;
			}
			//lines->add(buffer);
			//line_indxs->add((void *)len);
			lines.push_back(buffer);
			line_indxs->push_back(len);
			size++;
			buffer = NULL;
			if(*ptr == NULL)
				break;
			buffer = new char[LINE_LENGTH];
			cx = marginX;
			//cy += height;
			//height = 0;
			dest = buffer;
		}
		else {
			/*int h = font->getHeight(*ptr);
			if(h > height)
			height = h;*/
			*dest++ = *ptr++;
		}
	}
	if( !c_found ) {
		start = size-1;
	}
	int cy = 4;
	int h = font->getHeight();
	size_t nlines = this->height / h;
	//int size = lines->size();
	//int start = size - nlines - this->cline_off;
	/*if(start < 0) {
	//this->cline += start;
	start = 0;
	if( nlines > size )
	nlines = size;
	this->cline_off = size - nlines;
	}*/
	if( start+nlines >= lines.size() ) {
		at_end = true;
	}
	else {
		at_end = false;
	}

	if( start+nlines > lines.size() ) {
		//start = lines.size() - nlines;
		//if( start <= 0 ) {
		if( lines.size() <= nlines ) {
			start = 0;
			if( nlines > size )
				nlines = size;
			this->cpos = 0;
		}
		else {
			start = lines.size() - nlines;
			//this->cpos = (int)line_indxs->get(start-1);
			this->cpos = line_indxs->at(start-1);
		}
		//nlines = lines->size() - start;
	}
	for(size_t i=start;i<start+nlines;i++) {
		//char *line = static_cast<char *>(lines->get(i));
		const char *line = lines.at(i);
		font->writeText(x+marginX,y+cy,line,strlen(line));
		cy += h;
	}
	for(size_t i=0;i<lines.size();i++) {
		//char *line = static_cast<char *>(lines->get(i));
		char *line = lines.at(i);
		delete [] line;
	}

	// scrollbar
	paintScrollbar(g, start, nlines, lines.size(), true);

	//delete lines;
	if(buffer != NULL)
		delete [] buffer;
}

void TextField::scrollTo(size_t new_line) {
	if( new_line == 0 ) {
		this->cpos = 0;
	}
	else {
		T_ASSERT( new_line >= 1 );
		T_ASSERT( new_line <= line_indxs->size() );
		if( new_line >= 1 && new_line <= line_indxs->size() ) {
			this->cpos = line_indxs->at(new_line-1);
		}
	}
}

void TextField::scroll(bool up, size_t step) {
	size_t cline = 0;
	for(size_t i=0;i<line_indxs->size();i++) {
		//int len = (int)line_indxs->get(i);
		size_t len = line_indxs->at(i);
		if( this->cpos < len ) {
			cline = i;
			break;
		}
	}
	if( up ) {
		//cline -= step;
		//cline = max(cline, (size_t)0);
		// need to take extra care for unsigned!
		if( cline <= step ) {
			cline = 0;
		}
		else {
			cline -= step;
		}
	}
	else {
		cline += step;
		if( line_indxs->size() == 0 )
			cline = 0;
		else
			cline = min(cline, line_indxs->size()-1);
	}
	this->scrollTo(cline);
}

void TextField::input(InputSystem *iS) {
	Panel::input(iS);
	if( !visible )
		return;
	if( !enabled ) {
		return;
	}

	int time_ms = Vision::getGameTimeMS();
	const int delay = (int)(0.05f * 1000.0f);
	bool up = false, down = false;
	int step = 1;
	int h = font->getHeight();
	size_t nlines = this->height / h;
	int mx = iS->getMouse()->mouseX;
	int my = iS->getMouse()->mouseY;

	// mouse press on scrollbar
	bool moved = false;
	size_t new_position = 0;
	inputScrollbar(iS, &moved, &new_position, nlines, line_indxs->size(), true);
	if( moved ) {
		scrollTo(new_position);
	}

	// mouse press on scrollbar buttons
	if( iS->getMouse()->isLMousePressed() ) {
		int buttons_y = y+height-scrollbar_buttons_h;
		int sx = x+width-scrollbar_w;
		int ex = x+width-1;
		if( mx >= sx && mx <= ex && my >= buttons_y && my < y+height ) {
			/*iS->getMouse()->handled_event = true;
			iS->getMouse()->flush();*/

			if( time_ms > last_event_time_ms + delay ) {
				last_event_time_ms = time_ms;
				int midy = buttons_y+scrollbar_buttons_h/2;

				if( my < midy ) {
					up = true;
					this->scrollbar_v_pressed_up = true;
					this->scrollbar_v_pressed_down = false;
				}
				else {
					down = true;
					this->scrollbar_v_pressed_up = false;
					this->scrollbar_v_pressed_down = true;
				}
				/*int cline = 0;
				for(int i=0;i<line_indxs->size();i++) {
					//int len = (int)line_indxs->get(i);
					int len = line_indxs->at(i);
					if( this->cpos < len ) {
						cline = i;
						break;
					}
				}
				if( my < midy ) {
					// up
					if( cline > 0 )
						cline--;
				}
				else {
					// down
					if( cline < ((int)line_indxs->size())-1 )
						cline++;
				}
				if( cline == 0 ) {
					this->cpos = 0;
				}
				else {
					//this->cpos = (int)line_indxs->get(cline-1);
					this->cpos = line_indxs->at(cline-1);
				}*/
			}
		}
		else {
			this->scrollbar_v_pressed_up = false;
			this->scrollbar_v_pressed_down = false;
		}
	}
	else {
		this->scrollbar_v_pressed_up = false;
		this->scrollbar_v_pressed_down = false;
	}

	// mousewheel
	/*if( mx >= x && mx < x+width && my >= y && my < y+height ) {
		if( iS->getMouse()->readMouseWheelUp() ) {
			up = true;
		}
		else if( iS->getMouse()->readMouseWheelDown() ) {
			down = true;
		}
	}*/

	// keyboard
	/*if( this->has_input_focus ) {
		if( iS->getKeyboard()->wasPressed(V_UP, true) ) {
			if( time > last_event_time + delay ) {
				last_event_time = time;
				up = true;
			}
		}
		else if( iS->getKeyboard()->wasPressed(V_DOWN, true) ) {
			if( time > last_event_time + delay ) {
				last_event_time = time;
				down = true;
			}
		}
		else if( iS->getKeyboard()->wasPressed(V_PGUP) ) {
			if( time > last_event_time + delay ) {
				last_event_time = time;
				up = true;
				step = nlines;
			}
		}
		else if( iS->getKeyboard()->wasPressed(V_PGDOWN) ) {
			if( time > last_event_time + delay ) {
				last_event_time = time;
				down = true;
				step = nlines;
			}
		}
	}*/

	if( up || down ) {
		this->scroll(up, step);
	}
	/*else if( iS->getKeyboard()->wasPressed(V_HOME) ) {
		this->cpos = 0;
	}
	else if( iS->getKeyboard()->wasPressed(V_END) ) {
		this->cpos = line_indxs->at( line_indxs->size() - 1 );
	}*/
}

void TextField::processMouseClickEvent(VI_MouseClickEvent *mouse_click_event) {
	if( !visible )
		return;
	if( !enabled ) {
		Panel::processMouseClickEvent(mouse_click_event);
		return;
	}

	if( !mouse_click_event->handled ) {
		bool up = false, down = false;
		int step = 1;

		int mx = mouse_click_event->mouse_x;
		int my = mouse_click_event->mouse_y;

		// mousewheel
		if( mx >= x && mx < x+width && my >= y && my < y+height ) {
			if( mouse_click_event->wheel_up ) {
				mouse_click_event->handled = true;
				up = true;
			}
			else if( mouse_click_event->wheel_down ) {
				mouse_click_event->handled = true;
				down = true;
			}
		}

		if( up || down ) {
			this->scroll(up, step);
		}
	}

	Panel::processMouseClickEvent(mouse_click_event);
}

void TextField::processKeydownEvent(VI_KeyEvent *key_event) {
	if( !visible )
		return;
	if( !enabled ) {
		return;
	}
	if( !enabled ) {
		Panel::processKeydownEvent(key_event);
		return;
	}

	if( this->has_input_focus ) {
		bool up = false, down = false;
		int step = 1;
		int h = font->getHeight();
		int nlines = this->height / h;

		if( key_event->code == V_UP ) {
			up = true;
		}
		else if( key_event->code == V_DOWN ) {
			down = true;
		}
		else if( key_event->code == V_PGUP ) {
			up = true;
			step = nlines;
		}
		else if( key_event->code == V_PGDOWN ) {
			down = true;
			step = nlines;
		}

		if( up || down ) {
			this->scroll(up, step);
		}
		else if( key_event->code == V_HOME ) {
			this->cpos = 0;
		}
		else if( key_event->code == V_END ) {
			this->cpos = line_indxs->at( line_indxs->size() - 1 );
		}
	}

	Panel::processKeydownEvent(key_event);
}
