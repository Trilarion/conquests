#pragma once

#include <cstdio>
#include <cstring>
#include <cstdlib> // needed for Linux at least

#ifdef _WIN32
#include <io.h> // for access
#include <direct.h> // for mkdir
#define access _access
#elif __linux
#include <sys/stat.h>
#endif

#if _WIN32
#include <shlobj.h>
#include <Shlwapi.h>
#pragma comment(lib,"shlwapi.lib")
#endif

#ifndef _WIN32
#define stricmp strcasecmp
#define strnicmp strncasecmp
#endif

const int VISIONPREFS_STRING_LEN = 255;

const int nVisionAPIs = 3;
static char VisionAPIs[nVisionAPIs][VISIONPREFS_STRING_LEN+1] = {
	"Direct3D 9 Renderer",
	"OpenGL Renderer",
	"Software Renderer"
};
enum EnumVisionAPI {
	VISIONAPI_D3D9 = 0,
	VISIONAPI_OPENGL = 1,
	VISIONAPI_SOFTWARE = 2
}; // the entries and order must correspond to the VisionAPIs textual names

const int nVisionMultisample = 4;
static char VisionMultisample[nVisionMultisample][VISIONPREFS_STRING_LEN+1] = {
	"None",
	"2x AA",
	"4x AA",
	"8x AA"
};
const int VisionMultisampleValues[nVisionMultisample] = {
	0,
	2,
	4,
	8
}; // the entries and order must correspond to the VisionMultisample textual names
/*enum EnumVisionMultisample {
VISIONMULTISAMPLE_0 = 0,
VISIONMULTISAMPLE_2 = 1,
VISIONMULTISAMPLE_4 = 2,
VISIONMULTISAMPLE_8 = 3
}; // the entries and order must correspond to the VisionMultisample textual names
*/

/*#ifdef _WIN32
//const char config_folder[] = "%APPDATA%/Vision3Engine";
const char config_folder[] = "c:/temp/Vision3Engine";
const char config_filename[] = "%APPDATA%/Vision3Engine/vision.config";
#else
const char config_folder[] = "";
const char config_filename[] = "vision.config";
#endif*/
const char config_filename[] = "vision.config";

class MessageInterface {
public:
	virtual void show(const char *message)=0;
	virtual void log(const char *message)=0;
};

class VisionPrefs {
	void parseConfigLine(char *word, const char *line, const char *pref) const {
		size_t offset = strlen(pref);
		char *dest = word;
		const char *src = &line[offset];
		while( *src != ']' && *src != '\0' )
			*dest++ = *src++;
		*dest++ = '\0';
	}
	bool parseBool(bool *ok, const char *word) const {
		*ok = true;
		if( strcmp(word, "0") == 0 ) {
			return false;
		}
		else if( strcmp(word, "1") == 0 ) {
			return true;
		}
		*ok = false;
		return false;
	}

	bool matchPref(const char *line, const char *pref) const {
		if( strnicmp(line, pref, strlen(pref)) == 0 ) {
			return true;
		}
		else {
			return false;
		}
	}

public:
	//char api[VISIONPREFS_STRING_LEN+1];
	EnumVisionAPI api;
	int           resolution_width;
	int           resolution_height;
	bool          fullscreen;
	//char antialiasing[VISIONPREFS_STRING_LEN+1];
	int           multisample;
	bool          smp;
	bool          shaders;
	bool          bumpmapping;
	bool          terrainsplatting;
	bool          watereffects;
	bool          atmosphere;
	bool          hdr;
	bool          hardwarevertexanimation;
	bool          twosidedstencil;
	bool          hardwaretl;
	bool          vbo;

	// internal only
	bool          want_std_shaders;

	VisionPrefs() {
		//api[0] = '\0';
		api = VISIONAPI_D3D9;
		resolution_width = 800;
		resolution_height = 600;
		fullscreen = false;
		//antialiasing[0] = '\0';
		multisample = VisionMultisampleValues[0];
		smp = true;
		shaders = true;
		bumpmapping = true;
		terrainsplatting = true;
		watereffects = true;
		atmosphere = true;
		hdr = false;
		hardwarevertexanimation = true;
		twosidedstencil = true;
		hardwaretl = true;
		vbo = true;
		want_std_shaders = true;
	}
/*#ifdef _WIN32
	static bool getSaveFolder(char *folder) {
		if ( SUCCEEDED( SHGetFolderPathA( NULL, CSIDL_APPDATA,
			NULL, 0, folder ) ) ) {
				PathAppendA(folder, "Vision3Engine");
		}
		else {
			*folder = '\0';
			return false;
		}
		return true;
	}
#endif*/
	static char *getSaveFolderPath(bool create_folder) {
		char *folder_path = NULL;
		char foldername[] = "Vision3Engine";
#ifdef _WIN32
		folder_path = new char[MAX_PATH];
		*folder_path = '\0';
		if ( SUCCEEDED( SHGetFolderPathA( NULL, CSIDL_APPDATA,
			NULL, 0, folder_path ) ) )
		{
                       PathAppendA(folder_path, foldername);
		}
		else {
			delete [] folder_path;
			folder_path = NULL;
		}
#elif __linux
		//char *subdir = "/.";
		char *subdir = "/.config/";
		char *homedir = getenv("HOME");
		int len = strlen(homedir) + strlen(subdir) + strlen(foldername);
		folder_path = new char[len+1];
		sprintf(folder_path, "%s%s%s", homedir, subdir, foldername);
#else
		// just store locally
		folder_path = new char[1];
		*folder_path = '\0';
#endif
		if( create_folder && folder_path != NULL && *folder_path != '\0' ) {
			// n.b., if we fail to create the folder, we don't fail here; we'll fail later when we are unable to write to the file
#ifdef _WIN32
		if( access(folder_path, 0) != 0 ) {
			// folder doesn't seem to exist - try creating it
			mkdir(folder_path);
		}
#elif __linux
		if( access(folder_path, 0) != 0 ) {
			mkdir(folder_path, S_IRWXU | S_IRWXG | S_IRWXO);
		}
#else
			// shouldn't be here!
#endif
		}
		return folder_path;
	}

	void load(MessageInterface *messageInterface) {
		//createFolder();
/*#ifdef _WIN32
		char full_filename[MAX_PATH] = "";
		if( !getSaveFolder(full_filename) ) {
			return;
		}
		PathAppendA(full_filename, config_filename);
		FILE *file = fopen(full_filename, "r");
#else
		FILE *file = fopen(config_filename, "r");
#endif*/
		FILE *file = NULL;
		char *folder_path = getSaveFolderPath(false);
		if( folder_path == NULL ) {
			messageInterface->log("Unable to obtain folder path for config file\n");
			return;
		}
		else if( *folder_path != '\0' ) {
			messageInterface->log("Config folder path is: ");
			messageInterface->log(folder_path);
			messageInterface->log("\n");
			char *full_filename = new char[strlen(folder_path)+1+strlen(config_filename)+1];
                        sprintf(full_filename, "%s/%s", folder_path, config_filename);
                        messageInterface->log("Full filename: ");
                        messageInterface->log(folder_path);
                        messageInterface->log("\n");
                        file = fopen(full_filename, "r");
		}
		else {
                        messageInterface->log("Looking in local program folder for config file\n");
			file = fopen(config_filename, "r");
		}
		delete [] folder_path;
		folder_path  = NULL;
		if( file != NULL ) {
			const int max_line_c = 4095;
			char line[max_line_c+1] = "";
			char word[max_line_c+1] = "";
			while( fgets(line, max_line_c, file) != NULL ) {
				const char field_version_c[] = "[version=";
				const char field_api_c[] = "[api=";
				const char field_width_c[] = "[width=";
				const char field_height_c[] = "[height=";
				//const char field_bpp_c[] = "[bpp=";
				const char field_fullscreen_c[] = "[fullscreen=";
				//const char field_antialiasing_c[] = "[antialiasing=";
				const char field_multisample_c[] = "[multisample=";
				const char field_smp_c[] = "[smp=";
				const char field_shaders_c[] = "[shaders=";
				const char field_bumpmapping_c[] = "[bumpmapping=";
				const char field_terrainsplatting_c[] = "[terrainsplatting=";
				const char field_watereffects_c[] = "[watereffects=";
				const char field_atmosphere_c[] = "[atmosphere=";
				const char field_hdr_c[] = "[hdr=";
				const char field_hardwarevertexanimation_c[] = "[hardwarevertexanimation=";
				const char field_twosidedstencil_c[] = "[twosidedstencil=";
				const char field_hardwaretl_c[] = "[hardwaretl=";
				const char field_vbo_c[] = "[vbo=";
				//const char field__c[] = "[=";

				if( matchPref(line, field_version_c) ) {
					parseConfigLine(word, line, field_version_c);
					int version = atoi(word);
					if( version != 1 ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised config version","Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised config version");
					}
				}
				else if( matchPref(line, field_api_c) ) {
					parseConfigLine(word, line, field_api_c);
					bool found = false;
					for(int i=0;i<nVisionAPIs && !found;i++) {
						if( stricmp(VisionAPIs[i], word) == 0 ) {
							found = true;
							api = (EnumVisionAPI)i;
						}
					}
					if( !found ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised API","Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised API");
					}
				}
				else if( matchPref(line, field_width_c) ) {
					parseConfigLine(word, line, field_width_c);
					resolution_width = atoi(word);
				}
				else if( matchPref(line, field_height_c) ) {
					parseConfigLine(word, line, field_height_c);
					resolution_height = atoi(word);
				}
				else if( matchPref(line, field_fullscreen_c) ) {
					parseConfigLine(word, line, field_fullscreen_c);
					bool ok = true;
					fullscreen = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised Fullscreen setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised Fullscreen setting");
					}
				}
				/*else if( matchPref(line, field_antialiasing_c) ) {
				parseConfigLine(antialiasing, line, field_antialiasing_c);
				}*/
				else if( matchPref(line, field_multisample_c) ) {
					parseConfigLine(word, line, field_multisample_c);
					multisample = atoi(word);
				}
				else if( matchPref(line, field_smp_c) ) {
					parseConfigLine(word, line, field_smp_c);
					bool ok = true;
					smp = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised SMP setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised SMP setting");
					}
				}
				else if( matchPref(line, field_shaders_c) ) {
					parseConfigLine(word, line, field_shaders_c);
					bool ok = true;
					shaders = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised shaders setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised shaders setting");
					}
				}
				else if( matchPref(line, field_bumpmapping_c) ) {
					parseConfigLine(word, line, field_bumpmapping_c);
					bool ok = true;
					bumpmapping = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised bumpmapping setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised bumpmapping setting");
					}
				}
				else if( matchPref(line, field_terrainsplatting_c) ) {
					parseConfigLine(word, line, field_terrainsplatting_c);
					bool ok = true;
					terrainsplatting = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised terrainsplatting setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised terrain splatting setting");
					}
				}
				else if( matchPref(line, field_watereffects_c) ) {
					parseConfigLine(word, line, field_watereffects_c);
					bool ok = true;
					watereffects = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised watereffects setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised water effects setting");
					}
				}
				else if( matchPref(line, field_atmosphere_c) ) {
					parseConfigLine(word, line, field_atmosphere_c);
					bool ok = true;
					atmosphere = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised atmosphere setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised atmosphere setting");
					}
				}
				else if( matchPref(line, field_hdr_c) ) {
					parseConfigLine(word, line, field_hdr_c);
					bool ok = true;
					hdr = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised HDR setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised HDR setting");
					}
				}
				else if( matchPref(line, field_hardwarevertexanimation_c) ) {
					parseConfigLine(word, line, field_hardwarevertexanimation_c);
					bool ok = true;
					hardwarevertexanimation = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised hardware vertex animation setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised hardware vertex animation setting");
					}
				}
				else if( matchPref(line, field_twosidedstencil_c) ) {
					parseConfigLine(word, line, field_twosidedstencil_c);
					bool ok = true;
					twosidedstencil = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised two-sided setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised two-sided stencil setting");
					}
				}
				else if( matchPref(line, field_hardwaretl_c) ) {
					parseConfigLine(word, line, field_hardwaretl_c);
					bool ok = true;
					hardwaretl = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised hardware T&L setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised hardware T&L setting");
					}
				}
				else if( matchPref(line, field_vbo_c) ) {
					parseConfigLine(word, line, field_vbo_c);
					bool ok = true;
					vbo = parseBool(&ok, word);
					if( !ok ) {
						/*#ifdef _WIN32
						MessageBoxA(NULL, "Unrecognised VBO setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
						#endif*/
						messageInterface->show("Unrecognised VBO setting");
					}
				}
				else {
					/*#ifdef _WIN32
					MessageBoxA(NULL, "Unrecognised setting", "Warning", MB_OK|MB_ICONEXCLAMATION);
					#endif*/
					messageInterface->show("Unrecognised setting");
				}
			}
			fclose(file);
		}
                else {
                    messageInterface->log("config file either not present or can't be opened\n");
                }
	}
};

