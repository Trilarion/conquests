#pragma once

#include <cstdio>

#include <vector>
using std::vector;
#include <string>
using std::string;

class VI_Object;
class VI_VObject;
class VI_Mesh;
class VI_SceneGraphNode;
class VI_Entity;

class Object3D;
class MD2Object;
class TDS_Material;
class Mesh;

#define ENDIANSWAPLONG(x)
#define ENDIANSWAPSHORT(x)

class Loader {
	static void fskip(FILE *fp, int num_bytes);
	static bool fgetLine(FILE *fp, char *ptr);
	static char * stripWhiteSpace(char *ptr);
	static int parse3DSChunk(FILE * file,VI_VObject *obj,TDS_Material **first_mat,void * sub);
	static vector<string> getWords(const char *str);
public:
	static VI_VObject *importRWX(const char * filename);
	static VI_Mesh *importMD2(const char *filename, const char *skinfile);
	static VI_SceneGraphNode *importEntityMD2(VI_Mesh **v_obj,const char *filename,const char *texture);
	static VI_SceneGraphNode *importNodeNWN(int *n_objs,VI_Object ***objs,const char *filename) {
		return importNodeNWN(n_objs, objs, filename, true);
	}
	static VI_SceneGraphNode *importNodeNWN(int *n_objs,VI_Object ***objs,const char *filename,bool want_textures);
	static VI_Mesh *importVisionMesh(const char *filename);
	static VI_Mesh *importVisionMesh(const char *filename, const char *override_texture);
	static VI_SceneGraphNode *importVisionNode(const char *filename);
	static VI_SceneGraphNode *importVisionNode(const char *filename, const char *override_texture);
};
