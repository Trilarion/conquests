#pragma once

/** Main game file.
*/

const int versionMajor = 1;
const int versionMinor = 2;

#include "common.h"
#include "utils.h"

#include "../Vision/VisionIface.h"

extern "C" {
#ifdef _WIN32
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
#endif
#ifdef __linux
	#include <lua5.1/lua.h>
	#include <lua5.1/lualib.h>
	#include <lua5.1/lauxlib.h>
#endif
}

#include <vector>
using std::vector;
#include <set>
using std::set;
#include <map>
using std::map;
#include <algorithm>
using std::min;
using std::max;

#ifndef _WIN32
	#define stricmp strcasecmp
	#define strnicmp strncasecmp
#endif

class MainGamestate;
class Race;
class Civilization;
class Relationship;
class City;
class Technology;
class Buildable;
class Improvement;
class UnitTemplate;
class Unit;
class InfoWindow;
class BonusResource;
class Map;
class Distance;

class VI_GraphicsEnvironment;
class VI_Font;
class VI_Sound;
class VI_Texture;
class VI_Panel;
class VI_Button;
class VI_Listbox;
class ShaderEffect;

extern const bool CHEAT;

extern int frames;
extern bool quit;

extern char *savegames_dirname;
extern char *usermaps_dirname;

extern const char *rebel_race_name;

extern const char *cursor_arrow[];
extern const char *cursor_wait[];
extern const char *cursor_target[];
extern const char *cursor_block[];

const int road_move_scale_c = 3; // must match Lua globals
const int biplane_travel_range_c = 8; // must match Lua globals
const int bombers_travel_range_c = 12;
const int jet_travel_range_c = 24;
const float fade_duration_c = 1.0f;

/*enum ImprovementType {
	IMPROVEMENT_AQUEDUCT = 0,
	IMPROVEMENT_BARRACKS = 1,
	IMPROVEMENT_FARMLAND = 2,
	IMPROVEMENT_FORT = 3,
	IMPROVEMENT_HARBOUR = 4,
	IMPROVEMENT_LIBRARY = 5,
	IMPROVEMENT_MINE = 6,
	IMPROVEMENT_TEMPLE = 7,
	IMPROVEMENT_WALLS = 8,
	IMPROVEMENT_MARKET = 9,
	IMPROVEMENT_ARMOURER = 10,
	IMPROVEMENT_CASTLE = 11,
	IMPROVEMENT_CHURCH = 12,
	IMPROVEMENT_PORT = 13,
	IMPROVEMENT_UNIVERSITY = 14,
	IMPROVEMENT_COASTALDEFENCE = 15,
	IMPROVEMENT_FACTORY = 16,
	IMPROVEMENT_HOSPITAL = 17,
	IMPROVEMENT_SCHOOL = 18,
	IMPROVEMENT_AIRPORT = 19,
	IMPROVEMENT_ANTIAIRGUNS = 20,
	IMPROVEMENT_LAB = 21,
	IMPROVEMENT_MISSILELAUNCHER = 22,
	IMPROVEMENT_RADARTOWER = 23,
	IMPROVEMENT_SAMLAUNCHER = 24,
	IMPROVEMENT_BOMBSHELTER = 25,
	IMPROVEMENT_SPACEPORT = 26,
	IMPROVEMENT_SDILASERS = 27
};*/

void initButton(VI_Button *button);
void initListbox(VI_Listbox *listbox);

class Element {
	string name;
	const Improvement *requires_improvement;
	const Technology *requires_technology;
	int initial_amount;
	int base_rate;
	map<Type, int> terrain_rates;
	map<const BonusResource *, int> bonus_rates;
public:
	Element(const char *name, const Improvement *requires_improvement, const Technology *requires_technology, int initial_amount, int base_rate);

	const char *getName() const {
		return name.c_str();
	}
	bool equals(const char *name) const {
		bool equal = this->name == name;
		return equal;
	}
	const Improvement *getRequiresImprovement() const {
		return this->requires_improvement;
	}
	const Technology *getRequiresTechnology() const {
		return this->requires_technology;
	}
	void setTerrainRate(Type type, int rate) {
		terrain_rates[type] = rate;
	}
	void setBonusRate(const BonusResource *bonus_resource, int rate) {
		bonus_rates[bonus_resource] = rate;
	}
	int getInitialAmount() const {
		return this->initial_amount;
	}
	int getBaseRate() const {
		return this->base_rate;
	}
	int getTerrainRate(Type type) const {
		/*
		// if terrain rate doesn't exist for this type, will be initialised with default of 0
		return this->terrain_rates[type];
		*/
		map<Type, int>::const_iterator iter = terrain_rates.find(type);
		if( iter == terrain_rates.end() ) {
			return 0;
		}
		return iter->second;
	}
	int getBonusRate(const BonusResource *bonus_resource) const {
		if( bonus_resource == NULL ) {
			return 0; // if no bonus at the square, then can't be any output for the element
		}
		map<const BonusResource *, int>::const_iterator iter = bonus_rates.find(bonus_resource);
		if( iter == bonus_rates.end() ) {
			return 0;
		}
		return iter->second;
	}
};

class BonusResource {
	string name;
	VI_Texture *texture;
	//Type terrain;
	set<Type> terrains;
	const Improvement *requires_improvement;
	const Technology *requires_technology;
	int bonus_production;
	int bonus_reduce_growth_time;

public:
	BonusResource(const char *name, const unsigned char rgb[3], Type terrain, const Improvement *requires_improvement, const Technology *requires_technology, int bonus_production, int bonus_reduce_growth_time);

	/*Type getTerrain() const {
		return terrain;
	}*/
	void addTerrain(Type terrain) {
		terrains.insert(terrain);
	}
	bool hasTerrain(Type terrain) const {
		return terrains.find(terrain) != terrains.end();
	}
	VI_Texture *getTexture() const {
		// n.b., return non-const needed due to draw functions being non-const
		return texture;
	}
	const char *getName() const {
		return name.c_str();
	}
	bool equals(const char *name) const {
		bool equal = this->name == name;
		return equal;
	}
	const Improvement *getRequiresImprovement() const {
		return this->requires_improvement;
	}
	const Technology *getRequiresTechnology() const {
		return this->requires_technology;
	}
	bool canSee(const Civilization *civilization) const;
	bool canUse(const City *city) const;
	int getBonusProduction() const {
		return this->bonus_production;
	}
	int getBonusReduceGrowthTime() const {
		return this->bonus_reduce_growth_time;
	}
};

class Gamestate {
public:
	virtual VI_Panel *getPanel()=0;
	virtual bool start()=0;
	virtual void stop()=0;
	virtual void quitGame()=0;
	virtual void update()=0;
};

class GameData {
	vector<const Race *> races; // list of all races
	vector<Technology *> technologies; // list of all technologies
	vector<Buildable *> buildables; // list of all buildables
	vector<const Improvement *> improvements; // list of all improvements
	vector<const UnitTemplate *> unit_templates; // list of just unit templates
	vector<const BonusResource *> bonus_resources; // list of all bonus resources
	vector<const Element *> elements;

public:
	GameData() {
	}

	void clear();

	void addTechnology(Technology *technology);
	void addBuildable(Buildable *buildable);
	void addRace(const Race *race) {
		this->races.push_back(race);
	}
	void addBonusResource(const BonusResource *bonus_resource) {
		this->bonus_resources.push_back(bonus_resource);
	}
	void addElement(Element *element) {
		this->elements.push_back(element);
	}

	const Technology *getTechnology(size_t i) const {
		return technologies.at(i);
	}
	size_t getNTechnologies() const {
		return technologies.size();
	}
	const Buildable *getBuildable(size_t i) const {
		return buildables.at(i);
	}
	size_t getNBuildables() const {
		return buildables.size();
	}
	const Improvement *getImprovement(size_t i) const {
		return improvements.at(i);
	}
	size_t getNImprovements() const {
		return improvements.size();
	}
	const UnitTemplate *getUnitTemplate(size_t i) const {
		return unit_templates.at(i);
	}
	size_t getNUnitTemplates() const {
		return unit_templates.size();
	}
	const Race *getRace(size_t i) const {
		return races.at(i);
	}
	size_t getNRaces() const {
		return races.size();
	}
	const BonusResource *getBonusResource(size_t i) const {
		return bonus_resources.at(i);
	}
	size_t getNBonusResources() const {
		return bonus_resources.size();
	}
	const Element *getElement(size_t i) const {
		return elements.at(i);
	}
	/*Element *getElement(size_t i) {
		return elements.at(i);
	}*/
	size_t getNElements() const {
		return elements.size();
	}

	const Technology *findTechnology(const char *name) const;
	Buildable *findBuildable(const char *name);
	const Improvement *findImprovement(const char *name) const;
	//const Improvement *findImprovement(ImprovementType improvementType) const;
	const UnitTemplate *findUnitTemplate(const char *name) const;
	const Race *findRace(const char *name) const;
	const BonusResource *findBonusResource(const char *name) const;
	const Element *findElement(const char *name) const;
};

class Game {
	Gamestate *gamestate;
	Gamestate *newGamestate;
	GameData gameData;

	VI_GraphicsEnvironment *genv;
	VI_Sound               *sound;
	VI_Font                *smallfont;
	VI_Font                *font;
	VI_Font                *largefont;
	lua_State              *ls;

	/*Shader     *splat_vertex_shader_ambient;
	Shader     *splat_pixel_shader_ambient;
	Shader     *splat_3d_pixel_shader_ambient;*/
	ShaderEffect *splat_shader_ambient;
	ShaderEffect *splat_3d_shader_ambient;
	VI_Texture *panelTexture;
	VI_Texture *ageMedievalTexture;
	VI_Texture *ageIndustrialTexture;
	VI_Texture *ageModernTexture;
	VI_Texture *executionImpalementTexture;
	VI_Texture *executionBeheadingTexture;
	VI_Texture *executionHangingTexture;

	bool testmode_silent; // if true, InfoWindow requesters are closed automatically

	static void raceAction(VI_Panel *source);

	//bool display_progress;
	int progress_percentage;
	void updateProgress(int progress_percentage);
	static void paintPanel(VI_Graphics2D *g2d, void *data);
	bool init();
	void clear();
	//void registerScripts();

public:
	Game() {
		gamestate = NULL;
		newGamestate = NULL;
		genv = NULL;
		sound = NULL;
		smallfont = NULL;
		font = NULL;
		largefont = NULL;
		ls = NULL;
		/*splat_pixel_shader_ambient = NULL;
		splat_3d_pixel_shader_ambient = NULL;
		splat_vertex_shader_ambient = NULL;*/
		splat_shader_ambient = NULL;
		splat_3d_shader_ambient = NULL;
		panelTexture = NULL;
		ageMedievalTexture = NULL;
		ageIndustrialTexture = NULL;
		ageModernTexture = NULL;
		executionImpalementTexture = NULL;
		executionBeheadingTexture = NULL;
		executionHangingTexture = NULL;
		testmode_silent = false;
		//display_progress = false;
		progress_percentage = 0;
	}
	~Game() {
	}

	bool setupNewGame(MainGamestate *mainGamestate);
	void setCursor(const char *image[]);
	/*bool runScript(const char *script) {
		return runScript(script, NULL);
	}
	bool runScript(const char *script, const char *function);*/
	bool loadScript(const char *script);
	void pushScriptFunction(const char *function);
	bool runScriptFunction(int n_args);
	lua_State *getLuaState() {
		return this->ls;
	}

	Gamestate *getGamestate() const {
		return gamestate;
	}
	GameData *getGameData() {
		return &gameData;
	}
	const GameData *getGameData() const {
		return &gameData;
	}
	VI_GraphicsEnvironment *getGraphicsEnvironment() {
		return genv;
	}
	VI_Sound *getSound() {
		return sound;
	}
	VI_Font *getSmallFont() {
		return smallfont;
	}
	VI_Font *getFont() {
		return font;
	}
	VI_Font *getLargefont() {
		return largefont;
	}
	/*const Shader *getSplatPixelShaderAmbient() const {
		return splat_pixel_shader_ambient;
	}
	Shader *getSplat3DPixelShaderAmbient() {
		return splat_3d_pixel_shader_ambient;
	}
	const Shader *getSplatVertexShaderAmbient() const {
		return splat_vertex_shader_ambient;
	}*/
	const ShaderEffect *getSplatShaderAmbient() const {
		return splat_shader_ambient;
	}
	const ShaderEffect *getSplat3DShaderAmbient() const {
		return splat_3d_shader_ambient;
	}
	VI_Texture *getPanelTexture() const {
		// n.b., return non-const needed due to draw functions being non-const
		return panelTexture;
	}
	VI_Texture *getAgeTexture(Age age) const;
	VI_Texture *getExecutionTexture(Age age) const;
	void addTechnology(Technology *technology) {
		getGameData()->addTechnology(technology);
	}
	void addBuildable(Buildable *buildable) {
		getGameData()->addBuildable(buildable);
	}
	void addRace(const Race *race) {
		getGameData()->addRace(race);
	}
	void addBonusResource(const BonusResource *bonus_resource) {
		getGameData()->addBonusResource(bonus_resource);
	}
	void addElement(Element *element) {
		getGameData()->addElement(element);
	}

	const Technology *findTechnology(const char *name) const {
		return getGameData()->findTechnology(name);
	}
	Buildable *findBuildable(const char *name) {
		return getGameData()->findBuildable(name);
	}
	const Improvement *findImprovement(const char *name) const {
		return getGameData()->findImprovement(name);
	}
	/*const Improvement *findImprovement(ImprovementType improvementType) const {
		return getGameData()->findImprovement(improvementType);
	}*/
	const UnitTemplate *findUnitTemplate(const char *name) const {
		return getGameData()->findUnitTemplate(name);
	}
	const Race *findRace(const char *name) const {
		return getGameData()->findRace(name);
	}
	void pushNewGamestate(Gamestate *newGamestate) {
		if( this->newGamestate != NULL ) {
			// just to be safe and avoid memory leak - will only happen if we push two gamestates before being used...
			delete this->newGamestate;
		}
		this->newGamestate = newGamestate;
	}
	bool isTestmodeSilent() const {
		return this->testmode_silent;
	}

	void run();
	void runTests(TestID id);
	void generateDocs();
	void mapWriteHeader(FILE *file, int width, int height);
	void mapWriteFooter(FILE *file);
	void bitmapToXML(const char *filename, const char *out_filename);

	static void doGame();
	static void doTests(TestID id, bool testmode_silent = false);
	static void doDocs();
	static void doBitmapToXML(const char *filename, const char *out_filename);
};

class AIInterface {
	static MainGamestate *mainGamestate;
	static Civilization *ai_civilization;

	static MainGamestate *getMainGamestate() {
		return mainGamestate;
	}
public:
	static void setMainGamestate(MainGamestate *mainGamestate) {
		AIInterface::mainGamestate = mainGamestate;
	}
	static void setAICivilization(Civilization *ai_civilization) {
		AIInterface::ai_civilization = ai_civilization;
	}
	static Civilization *getAICivilization() {
		return ai_civilization;
	}

	static const Distance *calculateDistanceMap(int *width, int *height, const Unit *unit, int xpos, int ypos) {
		return calculateDistanceMap(width, height, unit, xpos, ypos, true);
	}
	static const Distance *calculateDistanceMap(int *width, int *height, const Unit *unit, int xpos, int ypos, bool include_travel);
	static int getYear();
	static Civilization *getRebelCiv();
	// must match Lua globals:
	enum GetcivilizationsType {
		GETCIVILIZATIONS_ALL = 0,
		GETCIVILIZATIONS_WAR = 1,
		GETCIVILIZATIONS_PEACE = 2
	};
	static vector<Civilization *> getCivilizations(GetcivilizationsType type);
	static vector<City *> getCities(Civilization *civilization);
	static vector<Unit *> getUnits(Civilization *civilization);
	static const Relationship *findRelationship(const Civilization *civilization);
	static bool hasAdjacentUnexplored(int xpos, int ypos);
	static bool hasAdjacentExplored(int xpos, int ypos);
	static vector<Pos2D> getAdjacentSquares(int xpos, int ypos, bool include_centre);
	static vector<Pos2D> getCitySquares(int xpos, int ypos, bool include_centre);
	static Type getTerrain(int xpos, int ypos);
	static const BonusResource *getBonusResource(int xpos, int ypos);
	static Road getRoad(int xpos, int ypos);
	static const Civilization *getTerritory(int xpos, int ypos);
	static bool canBuildCity(int xpos, int ypos);
	static bool givesRoadBonus(int xpos, int ypos);
	static bool canBeImproved(int xpos, int ypos);
	static bool hasEnemies(int xpos, int ypos);
	static const vector<Unit *> *getUnits(int xpos, int ypos);
	static Unit *getBestDefender(int xpos, int ypos);
	static City *getCity(int xpos, int ypos);
	// must match Lua globals:
	enum GetbuildablesType {
		GETBUILDABLES_ALL = 0,
		GETBUILDABLES_IMPROVEMENTS = 1,
		GETBUILDABLES_UNITS = 2
	};
	static vector<const Buildable *> cityGetBuildables(const City *city, GetbuildablesType type);
	static const Buildable *cityGetBuildable(const City *city);
	static bool canBomb(const UnitTemplate *unit_template, const City *source_city, Pos2D target, bool check_launch_capability);
	// must match Lua globals:
	enum UnitAction {
		UNITACTION_WAIT = 0, // skip turn without using up moves
		UNITACTION_NOTHING = 1, // do nothing, and use up remaining moves
		UNITACTION_MOVE = 2, // returns destination as target_square (also used for attack/bombing)
		UNITACTION_WORK = 3, // works the land and uses up remaining moves
		UNITACTION_BUILD_CITY = 4 // builds a city, destroys the unit
	};
};

//extern MainGamestate *game;
extern Game *game_g;
//extern int frames;
