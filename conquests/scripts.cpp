#include "conquests_stdafx.h"

#include "scripts.h"
#include "game.h"
#include "buildable.h"
#include "technology.h"
#include "unit.h"
#include "civilization.h"
#include "city.h"
#include "map.h"

#include "../Vision/VisionIface.h"

#include <ctime>

void luafunc_returnPointer(lua_State *ls, void *ptr) {
	if( ptr == NULL )
		lua_pushnil(ls);
	else
		lua_pushlightuserdata(ls, ptr);
}

template<class T>
void luafunc_returnArrayPointers(lua_State *ls, const vector<T *> *array) {
	// open/create table
	lua_newtable(ls);
	int top = lua_gettop(ls);

	size_t table_size = 0;
	if( array != NULL ) {
		table_size = array->size();
		for(size_t i=0;i<array->size();i++) {
			T *t = array->at(i);
			// new cell
			lua_pushnumber(ls, i+1); // lua arrays start at 1
			lua_pushlightuserdata(ls, t);
			lua_settable(ls, top); // insert the new cell (and pop index/value off stack)
		}
	}

	// close table
	lua_pushliteral(ls, "n");
	lua_pushnumber(ls, table_size); // number of cells
	lua_settable(ls, top);
}

template<class T>
void luafunc_returnArrayConstPointers(lua_State *ls, const vector<const T *> *array) {
	// open/create table
	lua_newtable(ls);
	int top = lua_gettop(ls);

	size_t table_size = 0;
	if( array != NULL ) {
		table_size = array->size();
		for(size_t i=0;i<array->size();i++) {
			// need const cast to return UnitTemplate to Lua! Lua won't be able modify it anyway
			T *t = const_cast<T *>(array->at(i));
			// new cell
			lua_pushnumber(ls, i+1); // lua arrays start at 1
			lua_pushlightuserdata(ls, t);
			lua_settable(ls, top); // insert the new cell (and pop index/value off stack)
		}
	}

	// close table
	lua_pushliteral(ls, "n");
	lua_pushnumber(ls, table_size); // number of cells
	lua_settable(ls, top);
}

void luafunc_returnArrayInts(lua_State *ls, const vector<int> *array) {
	// open/create table
	lua_newtable(ls);
	int top = lua_gettop(ls);

	size_t table_size = 0;
	if( array != NULL ) {
		table_size = array->size();
		for(size_t i=0;i<array->size();i++) {
			int t = array->at(i);
			// new cell
			lua_pushnumber(ls, i+1); // lua arrays start at 1
			lua_pushinteger(ls, t);
			lua_settable(ls, top); // insert the new cell (and pop index/value off stack)
		}
	}

	// close table
	lua_pushliteral(ls, "n");
	lua_pushnumber(ls, table_size); // number of cells
	lua_settable(ls, top);
}

/* Adds a new technology to the game engine.
 * Args: name (string), base cost (int), requires1 (string), requires2 (string), age (int), ai_weight(int)
 * Returns: pointer to the new technology
 */
int lua_addNewTechnology(lua_State *ls) {
	//VI_log("lua_addNewTechnology()\n");
	//this->addTechnology( technology = new Technology("Agriculture", 100, NULL, NULL, Technology::AGE_ANCIENT, 20) );
	int n_args = lua_gettop(ls);
	if( n_args != 6 ) {
		VI_log("lua_addNewTechnology: incorrect number of arguments\n");
		lua_error(ls);
	}
	const char *name = NULL;
	int base_cost = 0;
	const Technology *requires1 = NULL;
	const Technology *requires2 = NULL;
	Age age = AGE_ANCIENT;
	int ai_weight = 0;

	int arg_index = 1;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewTechnology: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name = lua_tostring(ls, arg_index);
	}
	arg_index++;

	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewTechnology: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		base_cost = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewTechnology: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			requires1 = game_g->findTechnology(tech_name);
			if( requires1 == NULL ) {
				VI_log("lua_addNewTechnology: requires1 unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewTechnology: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			requires2 = game_g->findTechnology(tech_name);
			if( requires2 == NULL ) {
				VI_log("lua_addNewTechnology: requires2 unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewTechnology: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		age = static_cast<Age>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewTechnology: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ai_weight = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	VI_log("lua_addNewTechnology: %s, %d, %s, %s, %d, %d\n", name, base_cost, requires1==NULL?"NULL":requires1->getName(), requires2==NULL?"NULL":requires2->getName(), age, ai_weight);
	Technology *technology = new Technology(name, base_cost, requires1, requires2, age, ai_weight);
	game_g->addTechnology(technology);

	lua_pushlightuserdata(ls, technology);
	return 1;
}

/* Adds a new improvement to the game engine.
 * Args: name (string), base cost (int), requires (string), ai_weight (int)
 * Returns: pointer to the new improvement
 */
int lua_addNewImprovement(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 4 ) {
		VI_log("lua_addNewImprovement: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const char *name = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewImprovement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name = lua_tostring(ls, arg_index);
	}
	arg_index++;

	int base_cost = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewImprovement: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		base_cost = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	const Technology *requires = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewImprovement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			requires = game_g->findTechnology(tech_name);
			if( requires == NULL ) {
				VI_log("lua_addNewImprovement: requires1 unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	int ai_weight = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewImprovement: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ai_weight = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	VI_log("lua_addNewImprovement: %s, %d, %s, %d\n", name, base_cost, requires==NULL?"NULL":requires->getName(), ai_weight);
	Improvement *improvement = new Improvement(name, base_cost, ai_weight);
	if( requires != NULL ) {
		improvement->setRequires(requires);
	}
	game_g->addBuildable(improvement);

	lua_pushlightuserdata(ls, improvement);
	return 1;
}

/* Adds a new unit template to the game engine.
 * Args: name (string), base cost (int), requires (string), attack (int), defence (int), moves (int)
 * Returns: pointer to the new unit template
 */
int lua_addNewUnitTemplate(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 6 ) {
		VI_log("lua_addNewUnitTemplate: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const char *name = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewUnitTemplate: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name = lua_tostring(ls, arg_index);
	}
	arg_index++;

	int base_cost = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewUnitTemplate: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		base_cost = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	const Technology *requires = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewUnitTemplate: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			requires = game_g->findTechnology(tech_name);
			if( requires == NULL ) {
				VI_log("lua_addNewImprovement: requires1 unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	int attack = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewUnitTemplate: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		attack = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int defence = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewUnitTemplate: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		defence = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int moves = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewUnitTemplate: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		moves = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	VI_log("lua_addNewUnitTemplate: %s, %d, %s, %d, %d, %d\n", name, base_cost, requires==NULL?"NULL":requires->getName(), attack, defence, moves);
	UnitTemplate *unit_template = new UnitTemplate(name, base_cost, attack, defence, moves);
	if( requires != NULL ) {
		unit_template->setRequires(requires);
	}
	game_g->addBuildable(unit_template);

	lua_pushlightuserdata(ls, unit_template);
	return 1;
}

/* Adds a new bonus resource to the game engine.
 * Args: name (string), r (int), g (int), b (int), terrain TYPE_* (int), requires_improvement (string), requires_technology (string), bonus_production (int), bonus_reduce_growth_time (int)
 * Returns: pointer to the new bonus resource
 */
int lua_addNewBonus(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 9 ) {
		VI_log("lua_addNewBonus: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const char *name = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewBonus: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name = lua_tostring(ls, arg_index);
	}
	arg_index++;

	unsigned char rgb[3] = {255, 255, 255};
	for(int i=0;i<3;i++) {
		if( !lua_isnumber(ls, arg_index) ) {
			VI_log("lua_addNewBonus: argument %d not a number\n", arg_index);
			lua_error(ls);
		}
		else {
			lua_Integer col = lua_tointeger(ls, arg_index);
			if( col < 0 || col > 255 ) {
				VI_log("lua_addNewBonus: argument %d , number not valid range\n", arg_index);
			}
			rgb[i] = static_cast<unsigned char>(col);
		}
		arg_index++;
	}

	Type type = TYPE_UNKNOWN;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewBonus: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		type = static_cast<Type>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	const Improvement *requires_improvement = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewBonus: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *improvement_name = lua_tostring(ls, arg_index);
		if( *improvement_name != '\0' ) {
			requires_improvement = game_g->findImprovement(improvement_name);
			if( requires_improvement == NULL ) {
				VI_log("lua_addNewBonus: unknown improvement %s\n", improvement_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	const Technology *requires_technology = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewBonus: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			requires_technology = game_g->findTechnology(tech_name);
			if( requires_technology == NULL ) {
				VI_log("lua_addNewBonus: unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	int bonus_production = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewBonus: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bonus_production = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int bonus_reduce_growth_time = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewBonus: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bonus_reduce_growth_time = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	VI_log("lua_addNewBonus: %s, %d/%d/%d, %s, %s, %d, %d\n", name, rgb[0], rgb[1], rgb[2], requires_improvement==NULL?"NULL":requires_improvement->getName(), requires_technology==NULL?"NULL":requires_technology->getName(), bonus_production, bonus_reduce_growth_time);
	BonusResource *bonus_resource = new BonusResource(name, rgb, type, requires_improvement, requires_technology, bonus_production, bonus_reduce_growth_time);

	lua_pushlightuserdata(ls, bonus_resource);
	return 1;
}

/* Adds a possible terrain for a bonus.
 * Args: bonus (pointer), terrain TYPE_* (int)
 * Returns: nothing
 */
int lua_bonusAddTerrain(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_bonusAddTerrain: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	BonusResource *bonus_resource = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_bonusAddTerrain: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		bonus_resource = static_cast<BonusResource *>(ptr);
	}
	arg_index++;

	Type terrain = TYPE_NONE;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_bonusAddTerrain: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		terrain = static_cast<Type>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bonus_resource->addTerrain(terrain);

	return 0;
}

/* Adds a new element to the game engine.
 * Args: name (string), requires_improvement (string), required_technology (string), initial_amount (int), base_rate(int)
 * Returns: pointer to the new element
 */
int lua_addNewElement(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 5 ) {
		VI_log("lua_addNewElement: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const char *name = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewElement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name = lua_tostring(ls, arg_index);
	}
	arg_index++;

	const Improvement *requires_improvement = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewElement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *improvement_name = lua_tostring(ls, arg_index);
		if( *improvement_name != '\0' ) {
			requires_improvement = game_g->findImprovement(improvement_name);
			if( requires_improvement == NULL ) {
				VI_log("lua_addNewElement: unknown improvement %s\n", improvement_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	const Technology *requires_technology = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewElement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			requires_technology = game_g->findTechnology(tech_name);
			if( requires_technology == NULL ) {
				VI_log("lua_addNewElement: unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	int initial_amount = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewElement: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		initial_amount = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int base_rate = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_addNewElement: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		base_rate = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	VI_log("lua_addNewElement: %s, %s, %s, %d, %d\n", name, requires_improvement==NULL?"NULL":requires_improvement->getName(), requires_technology==NULL?"NULL":requires_technology->getName(), initial_amount, base_rate);
	Element *element = new Element(name, requires_improvement, requires_technology, initial_amount, base_rate);

	lua_pushlightuserdata(ls, element);
	return 1;
}

/* Sets the element's output for a given terrain type.
 * Args: element (pointer), terrain TYPE_* (int), rate (int)
 * Returns: nothing
 */
int lua_elementSetTerrainOutput(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_elementSetTerrainOutput: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Element *element = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_elementSetTerrainOutput: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		element = static_cast<Element *>(ptr);
	}
	arg_index++;

	Type terrain = TYPE_NONE;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_elementSetTerrainOutput: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		terrain = static_cast<Type>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int rate = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_elementSetTerrainOutput: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		rate = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	element->setTerrainRate(terrain, rate);

	return 0;
}

/* Sets the element's output for a given bonus resource.
 * Args: element (pointer), bonus (string), rate (int)
 * Returns: nothing
 */
int lua_elementSetBonusOutput(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_elementSetBonusOutput: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Element *element = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_elementSetBonusOutput: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		element = static_cast<Element *>(ptr);
	}
	arg_index++;

	const BonusResource *bonus_resource = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_elementSetBonusOutput: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *bonus_name = lua_tostring(ls, arg_index);
		bonus_resource = game_g->getGameData()->findBonusResource(bonus_name);
	}
	arg_index++;

	int rate = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_elementSetBonusOutput: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		rate = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	element->setBonusRate(bonus_resource, rate);

	return 0;
}

/* Finds a technology by name.
 * Args: name (string)
 * Returns: pointer to the technology
 */
/*int lua_findTechnology(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_findTechnology: incorrect number of arguments\n");
		lua_error(ls);
	}

	const Technology *technology = NULL;
	int arg_index = 1;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_findTechnology: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		technology = game_g->findTechnology(tech_name);
	}
	arg_index++;

	lua_pushlightuserdata(ls, technology);
	return 1;
}*/

/* Sets the info text for a technology.
 * Args: technology (pointer), info (string)
 * Returns: nothing
 */
int lua_technologySetInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_technologySetInfo: incorrect number of arguments\n");
		lua_error(ls);
	}

	Technology *technology = NULL;
	const char *info = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_technologySetInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		technology = static_cast<Technology *>(ptr);
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_technologySetInfo: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		info = lua_tostring(ls, arg_index);
	}
	arg_index++;

	technology->setInfo(info);

	return 0;
}

/* Sets the ai hint enum for a technology.
 * Args: technology (pointer), aiHint (int)
 * Returns: nothing
 */
int lua_technologySetAIHint(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_technologySetAIHint: incorrect number of arguments\n");
		lua_error(ls);
	}

	Technology *technology = NULL;
	Technology::AIHint aiHint = Technology::AIHINT_NONE;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_technologySetAIHint: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		technology = static_cast<Technology *>(ptr);
	}
	arg_index++;

	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_technologySetAIHint: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		aiHint = static_cast<Technology::AIHint>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	technology->setAIHint(aiHint);

	return 0;
}

/* Sets the required terrain for a technology.
 * Args: technology (pointer), terrain TYPE_* (int)
 * Returns: nothing
 */
int lua_technologySetTerrain(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_technologySetTerrain: incorrect number of arguments\n");
		lua_error(ls);
	}

	Technology *technology = NULL;
	Type terrain = TYPE_NONE;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_technologySetTerrain: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		technology = static_cast<Technology *>(ptr);
	}
	arg_index++;

	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_technologySetTerrain: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		terrain = static_cast<Type>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	technology->setTerrain(terrain);

	return 0;
}

/* Sets the technology as preventing rebellions
 * Args: technology (pointer)
 * Returns: nothing
 */
int lua_technologySetPreventsRebellion(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_technologySetPreventsRebellion: incorrect number of arguments\n");
		lua_error(ls);
	}

	Technology *technology = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_technologySetPreventsRebellion: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		technology = static_cast<Technology *>(ptr);
	}
	arg_index++;

	technology->setPreventsRebellion(true);

	return 0;
}

/* Sets the info text for a buildable.
 * Args: buildable (pointer), info (string)
 * Returns: nothing
 */
int lua_buildableSetInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_buildableSetInfo: incorrect number of arguments\n");

		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_buildableSetInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		buildable = static_cast<Buildable *>(ptr);
	}
	arg_index++;

	const char *info = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetInfo: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		info = lua_tostring(ls, arg_index);
	}
	arg_index++;

	buildable->setInfo(info);

	return 0;
}

/* Sets the advice text for a buildable.
 * Args: buildable (pointer), advice (string)
 * Returns: nothing
 */
int lua_buildableSetAdvice(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_buildableSetAdvice: incorrect number of arguments\n");

		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_buildableSetAdvice: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		buildable = static_cast<Buildable *>(ptr);
	}
	arg_index++;

	const char *advice = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetAdvice: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		advice = lua_tostring(ls, arg_index);
	}
	arg_index++;

	buildable->setAdvice(advice);

	return 0;
}

/* Sets the required improvement for a buildable.
 * Args: buildable (pointer), requires (string)
 * Returns: nothing
 */
int lua_buildableSetRequiresImprovement(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_buildableSetRequiresImprovement: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_buildableSetRequiresImprovement: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		buildable = static_cast<Buildable *>(ptr);
	}
	arg_index++;

	const Improvement *requires = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetRequiresImprovement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *improvement_name = lua_tostring(ls, arg_index);
		if( *improvement_name != '\0' ) {
			requires = game_g->findImprovement(improvement_name);
			if( requires == NULL ) {
				VI_log("lua_buildableSetRequiresImprovement: requires1 unknown improvment %s\n", improvement_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	buildable->setRequires(requires);

	return 0;
}

/* Sets the obsoleted by technology for a buildable.
 * Args: buildable (pointer), obsoleted_by (string)
 * Returns: nothing
 */
int lua_buildableSetObsoletedBy(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_buildableSetObsoletedBy: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_buildableSetObsoletedBy: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		buildable = static_cast<Buildable *>(ptr);
	}
	arg_index++;

	const Technology *obsoleted_by = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetObsoletedBy: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			obsoleted_by = game_g->findTechnology(tech_name);
			if( obsoleted_by == NULL ) {
				VI_log("lua_buildableSetObsoletedBy: unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	buildable->setObsoletedBy(obsoleted_by);

	return 0;
}

/* Sets the replaced by buildable for a buildable.
 * Args: buildable (pointer), replaced_by (pointer)
 * Returns: nothing
 */
int lua_buildableSetReplacedBy(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_buildableSetReplacedBy: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_buildableSetReplacedBy: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		buildable = static_cast<Buildable *>(ptr);
	}
	arg_index++;

	const Buildable *replaced_by = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_buildableSetReplacedBy: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		replaced_by = static_cast<Buildable *>(ptr);
	}
	arg_index++;

	buildable->setReplacedBy(replaced_by);

	return 0;
}

/* Sets the buildable to only be able to be built by one Race (n.b., receives buildable as string, as called from different lua script).
 * Args: buildable (string), race (string)
 * Returns: nothing
 */
int lua_buildableSetRaceSpecific(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_buildableSetRaceSpecific: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetRaceSpecific: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *buildable_name = lua_tostring(ls, arg_index);
		if( *buildable_name != '\0' ) {
			buildable = game_g->findBuildable(buildable_name);
			if( buildable == NULL ) {
				VI_log("lua_buildableSetRequiresElement: unknown buildable %s\n", buildable_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	const Race *race = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetRaceSpecific: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *race_name = lua_tostring(ls, arg_index);
		if( *race_name != '\0' ) {
			race = game_g->findRace(race_name);
			if( race == NULL ) {
				VI_log("lua_buildableSetRaceSpecific: unknown tech %s\n", race_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	buildable->setRaceSpecific(race);

	return 0;
}

/* Sets the required element for a buildable (n.b., receives buildable as string, as called from different lua script).
 * Args: buildable (string), requires (string), amount (int)
 * Returns: nothing
 */
int lua_buildableSetRequiresElement(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_buildableSetRequiresElement: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetRequiresElement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *buildable_name = lua_tostring(ls, arg_index);
		if( *buildable_name != '\0' ) {
			buildable = game_g->findBuildable(buildable_name);
			if( buildable == NULL ) {
				VI_log("lua_buildableSetRequiresElement: unknown buildable %s\n", buildable_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	const Element *requires = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetRequiresElement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *element_name = lua_tostring(ls, arg_index);
		if( *element_name != '\0' ) {
			requires = game_g->getGameData()->findElement(element_name);
			if( requires == NULL ) {
				VI_log("lua_buildableSetRequiresElement: unknown element %s\n", element_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	int amount = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_buildableSetRequiresElement: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		amount = lua_tointeger(ls, arg_index);
	}
	arg_index++;

	buildable->setRequires(requires, amount);

	return 0;
}

/* Sets the required bonus for a buildable (i.e., the city must have this bonus in the city square) (n.b., receives buildable as string, as called from different lua script).
 * Args: buildable (string), requires (string)
 * Returns: nothing
 */
int lua_buildableSetRequiresBonus(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_buildableSetRequiresBonus: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Buildable *buildable = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetRequiresBonus: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *buildable_name = lua_tostring(ls, arg_index);
		if( *buildable_name != '\0' ) {
			buildable = game_g->findBuildable(buildable_name);
			if( buildable == NULL ) {
				VI_log("lua_buildableSetRequiresBonus: unknown buildable %s\n", buildable_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	const BonusResource *requires = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_buildableSetRequiresElement: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *bonus_name = lua_tostring(ls, arg_index);
		if( *bonus_name != '\0' ) {
			requires = game_g->getGameData()->findBonusResource(bonus_name);
			if( requires == NULL ) {
				VI_log("lua_buildableSetRequiresBonus: unknown bonus %s\n", bonus_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	buildable->setRequires(requires);

	return 0;
}

/* Sets whether coastal required for an improvement.
 * Args: improvement (pointer), requires_coastal (boolean)
 * Returns: nothing
 */
int lua_improvementSetRequiresCoastal(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_improvementSetRequiresCoastal: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetRequiresCoastal: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	bool requires_coastal = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetRequiresCoastal: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		requires_coastal = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	improvement->setRequiresCoastal(requires_coastal);

	return 0;
}

/* Sets the travel range for an improvement.
 * Args: improvement (pointer), range (int), by_air (boolean)
 * Returns: nothing
 */
int lua_improvementSetTravelRange(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_improvementSetTravelRange: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetTravelRange: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int range = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetTravelRange: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		range = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool by_air = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetTravelRange: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		by_air = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	improvement->setTravelRange(range, by_air);

	return 0;
}

/* Sets the power per turn for an improvement.
 * Args: improvement (pointer), power_per_turn (int)
 * Returns: nothing
 */
int lua_improvementSetPowerPerTurn(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_improvementSetPowerPerTurn: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetPowerPerTurn: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int power_per_turn = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetPowerPerTurn: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		power_per_turn = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	improvement->setPowerPerTurn(power_per_turn);

	return 0;
}

/* Sets the rebellion reduction bonus for an improvement.
 * Args: improvement (pointer), bonus (int)
 * Returns: nothing
 */
int lua_improvementRebellionBonus(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_improvementRebellionBonus: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementRebellionBonus: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int bonus = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementRebellionBonus: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bonus = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	improvement->setRebellionBonus(bonus);

	return 0;
}

/* Sets the change to growth rate caused by an improvement - note, negative means faster growth.
 * Args: improvement (pointer), growth_rate (int)
 * Returns: nothing
 */
int lua_improvementSetGrowthRate(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_improvementSetGrowthRate: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetGrowthRate: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int growth_rate = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetGrowthRate: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		growth_rate = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	improvement->setGrowthRate(growth_rate);

	return 0;
}

/* Sets the production bonus, either for one city or all cities.
 * Args: improvement (pointer), bonus (int), all_cities (boolean)
 * Returns: nothing
 */
int lua_improvementSetProductionBonus(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_improvementSetProductionBonus: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetProductionBonus: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int bonus = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetProductionBonus: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bonus = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool all_cities = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetProductionBonus: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		all_cities = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	improvement->setProductionBonus(bonus, all_cities);

	return 0;
}

/* Sets the defence bonus, either for one city or all cities.
 * Args: improvement (pointer), bonus (int), all_cities (boolean)
 * Returns: nothing
 */
int lua_improvementSetDefenceBonus(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_improvementSetDefenceBonus: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetDefenceBonus: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int bonus = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetDefenceBonus: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bonus = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool all_cities = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetDefenceBonus: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		all_cities = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	improvement->setDefenceBonus(bonus, all_cities);

	return 0;
}

/* Sets the auto veteran bonus, either for one city or all cities.
 * Args: improvement (pointer), bonus (int), all_cities (boolean)
 * Returns: nothing
 */
int lua_improvementSetAutoVeteranBonus(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_improvementSetAutoVeteranBonus: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetAutoVeteranBonus: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	bool auto_veteran = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetAutoVeteranBonus: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		auto_veteran = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	bool all_cities = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetAutoVeteranBonus: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		all_cities = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	improvement->setAutoVeteranBonus(auto_veteran, all_cities);

	return 0;
}

/* Sets the research multiplier bonus, either for one city or all cities.
 * Args: improvement (pointer), bonus (int), all_cities (boolean)
 * Returns: nothing
 */
int lua_improvementSetResearchMultiplierBonus(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_improvementSetResearchMultiplierBonus: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetResearchMultiplierBonus: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int bonus = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetResearchMultiplierBonus: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bonus = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool all_cities = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetResearchMultiplierBonus: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		all_cities = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	improvement->setResearchMultiplierBonus(bonus, all_cities);

	return 0;
}

/* Sets the ai hint enum for a improvement.
 * Args: improvement (pointer), aiHint (int)
 * Returns: nothing
 */
int lua_improvementSetAIHint(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_improvementSetAIHint: incorrect number of arguments\n");
		lua_error(ls);
	}

	Improvement *improvement = NULL;
	Improvement::AIHint aiHint = Improvement::AIHINT_NONE;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetAIHint: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetAIHint: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		aiHint = static_cast<Improvement::AIHint>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	improvement->setAIHint(aiHint);

	return 0;
}

/* Sets the air defence for a improvement.
 * Args: improvement (pointer), air_defence (int)
 * Returns: nothing
 */
int lua_improvementSetAirDefence(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_improvementSetAirDefence: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetAirDefence: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	int air_defence = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_improvementSetAirDefence: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		air_defence = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	improvement->setAirDefence(air_defence);

	return 0;
}

/* Sets whether an improvement is immune from bombing.
 * Args: improvement (pointer), immune (boolean)
 * Returns: nothing
 */
int lua_improvementSetImmuneFromBombing(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_improvementSetImmuneFromBombing: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Improvement *improvement = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementSetImmuneFromBombing: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		improvement = static_cast<Improvement *>(ptr);
	}
	arg_index++;

	bool immune = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_improvementSetImmuneFromBombing: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		immune = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	improvement->setImmuneFromBombing(immune);

	return 0;
}

/* Sets whether a unit template is a foot unit.
 * Args: unit_template (pointer), foot (boolean)
 * Returns: nothing
 */
int lua_unitTemplateSetFoot(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_unitTemplateSetFoot: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetFoot: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	bool foot = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetFoot: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		foot = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	unit_template->setFoot(foot);

	return 0;
}

/* Sets whether a unit template can build a city.
 * Args: unit_template (pointer), can_build (boolean)
 * Returns: nothing
 */
int lua_unitTemplateSetCanBuildCity(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_unitTemplateSetCanBuildCity: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetCanBuildCity: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	bool can_build = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetCanBuildCity: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		can_build = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	unit_template->setCanBuildCity(can_build);

	return 0;
}

/* Sets whether a unit template can build roads/railways.
 * Args: unit_template (pointer), can_build (boolean)
 * Returns: nothing
 */
int lua_unitTemplateSetCanBuildRoads(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_unitTemplateSetCanBuildRoads: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetCanBuildRoads: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	bool can_build = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetCanBuildRoads: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		can_build = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	unit_template->setCanBuildRoads(can_build);

	return 0;
}

/* Sets whether a unit template can upgrade.
 * Args: unit_template (pointer), can_upgrade (boolean)
 * Returns: nothing
 */
int lua_unitTemplateSetUpgrade(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_unitTemplateSetUpgrade: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetUpgrade: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	bool can_upgrade = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetUpgrade: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		can_upgrade = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	unit_template->setUpgrade(can_upgrade);

	return 0;
}

/* Makes a unit template into an air unit.
 * Args: unit_template (pointer), air_attack (int), air_defence (int), air_range (int), is_missile (boolean)
 * Returns: nothing
 */
int lua_unitTemplateSetAir(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 5 ) {
		VI_log("lua_unitTemplateSetAir: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetAir: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	int air_attack = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetAir: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		air_attack = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int air_defence = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetAir: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		air_defence = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int air_range = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetAir: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		air_range = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool is_missile = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetAir: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		is_missile = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	unit_template->setAir(air_attack, air_defence, air_range, is_missile);

	return 0;
}

/* Sets a unit template's visibility range.
 * Args: unit_template (pointer), range (int)
 * Returns: nothing
 */
int lua_unitTemplateSetVisibilityRange(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_unitTemplateSetVisibilityRange: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetVisibilityRange: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	int range = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetVisibilityRange: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		range = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	unit_template->setVisibilityRange(range);

	return 0;
}

/* Sets a unit template's bombard.
 * Args: unit_template (pointer), bombard_chance (int), bombard_power (int)
 * Returns: nothing
 */
int lua_unitTemplateSetBombard(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_unitTemplateSetBombard: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetBombard: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	int bombard_chance = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetBombard: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bombard_chance = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int bombard_power = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetBombard: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		bombard_power = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	unit_template->setBombard(bombard_chance, bombard_power);

	return 0;
}

/* Sets a unit template's nuclear type.
 * Args: unit_template (pointer), type NUCLEARTYPE_* (int)
 * Returns: nothing
 */
int lua_unitTemplateSetNuclearType(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_unitTemplateSetNuclearType: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetNuclearType: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	UnitTemplate::NuclearType type = UnitTemplate::NUCLEARTYPE_NONE;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetNuclearType: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		type = static_cast<UnitTemplate::NuclearType>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	unit_template->setNuclearType(type);

	return 0;
}

/* Makes a unit template into a sea unit.
 * Args: unit_template (pointer), sea_attack (int), sea_defence (int)
 * Returns: nothing
 */
int lua_unitTemplateSetSea(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_unitTemplateSetSea: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetSea: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	int sea_attack = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetSea: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		sea_attack = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int sea_defence = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetSea: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		sea_defence = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	unit_template->setSea(sea_attack, sea_defence);

	return 0;
}

/* Sets a unit template's combat sound effect.
 * Args: unit_template (pointer), type SOUNDEFFECT_* (int)
 * Returns: nothing
 */
int lua_unitTemplateSetSoundEffectCombat(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_unitTemplateSetSoundEffectCombat: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetSoundEffectCombat: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<UnitTemplate *>(ptr);
	}
	arg_index++;

	UnitTemplate::SoundEffect sound_effect = UnitTemplate::SOUNDEFFECT_SWORDS;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_unitTemplateSetSoundEffectCombat: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		sound_effect = static_cast<UnitTemplate::SoundEffect>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	unit_template->setSoundEffect(sound_effect);

	return 0;
}

/* Adds a new race to the game engine.
 * Args: name (string), adjective name (string), leader (string), color r (int), color g (int), color b (int), image (string)
 * Returns: pointer to the new race
 */
int lua_addNewRace(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 7 ) {
		VI_log("lua_addNewRace: incorrect number of arguments\n");
		lua_error(ls);
	}
	const char *name = NULL;
	const char *name_adjective = NULL;
	const char *name_leader = NULL;
	unsigned char rgb[] = {0, 0, 0};
	const char *image = NULL;

	int arg_index = 1;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewRace: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name = lua_tostring(ls, arg_index);
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewRace: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name_adjective = lua_tostring(ls, arg_index);
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewRace: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name_leader = lua_tostring(ls, arg_index);
	}
	arg_index++;

	for(int i=0;i<3;i++) {
		if( !lua_isnumber(ls, arg_index) ) {
			VI_log("lua_addNewRace: argument %d not a number\n", arg_index);
			lua_error(ls);
		}
		else {
			lua_Integer col = lua_tointeger(ls, arg_index);
			if( col < 0 || col > 255 ) {
				VI_log("lua_addNewRace: argument %d , number not valid range\n", arg_index);
			}
			rgb[i] = static_cast<unsigned char>(col);
		}
		arg_index++;
	}

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_addNewRace: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		image = lua_tostring(ls, arg_index);
	}
	arg_index++;

	VI_log("lua_addNewRace: %s, %s, %s, %d, %d, %d, %s\n", name, name_adjective, name_leader, rgb[0], rgb[1], rgb[2], image);
	Race *race = new Race(name, name_adjective, name_leader, rgb, image);
	// constructor automatically adds race to the game engine

	lua_pushlightuserdata(ls, race);
	return 1;
}

/* Sets the info text for a race.
 * Args: race (pointer), info (string)
 * Returns: nothing
 */
int lua_raceSetInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_raceSetInfo: incorrect number of arguments\n");
		lua_error(ls);
	}

	Race *race = NULL;
	const char *info = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_raceSetInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		race = static_cast<Race *>(ptr);
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_raceSetInfo: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		info = lua_tostring(ls, arg_index);
	}
	arg_index++;

	race->setInfo(info);

	return 0;
}

/* Sets the starting technology for a race.
 * Args: race (pointer), technology (string)
 * Returns: nothing
 */
int lua_raceSetStartingTechnology(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_raceSetStartingTechnology: incorrect number of arguments\n");
		lua_error(ls);
	}

	Race *race = NULL;
	const Technology *technology = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_raceSetStartingTechnology: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		race = static_cast<Race *>(ptr);
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_raceSetStartingTechnology: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			technology = game_g->findTechnology(tech_name);
			if( technology == NULL ) {
				VI_log("lua_raceSetStartingTechnology: requires2 unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	race->setStartingTechnology(technology);

	return 0;
}

/* Adds a city name for a race.
 * Args: race (pointer), name (string)
 * Returns: nothing
 */
int lua_raceAddCityName(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_raceAddCityName: incorrect number of arguments\n");
		lua_error(ls);
	}

	Race *race = NULL;
	const char *name = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_raceAddCityName: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		race = static_cast<Race *>(ptr);
	}
	arg_index++;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_raceAddCityName: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		name = lua_tostring(ls, arg_index);
	}
	arg_index++;

	race->addCityName(name);

	return 0;
}

/* Finds a buildable by name.
 * Args: name (string)
 * Returns: pointer to the buildable
 */
int lua_findBuildable(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_findBuildable: incorrect number of arguments\n");
		lua_error(ls);
	}

	const Buildable *buildable = NULL;
	int arg_index = 1;

	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_findBuildable: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *buildable_name = lua_tostring(ls, arg_index);
		buildable = game_g->findBuildable(buildable_name);
	}
	arg_index++;

	lua_pushlightuserdata(ls, const_cast<Buildable *>(buildable));
	return 1;
}

/* Returns a buildable's name, type, cost
 * Args: buildable (pointer)
 * Returns: name (string), type BUILDABLETYPE_* (int), cost (int)
 */
int lua_buildableGetInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_buildableGetInfo: incorrect number of arguments\n");
		lua_error(ls);
	}

	const char *name = NULL;
	Buildable::Type type = Buildable::TYPE_IMPROVEMENT;
	int cost = 0;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_buildableGetInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		Buildable *buildable = static_cast<Buildable *>(ptr);
		name = buildable->getName();
		type = buildable->getType();
		cost = buildable->getCost();
	}
	arg_index++;

	lua_pushstring(ls, name);
	lua_pushinteger(ls, (int)type);
	lua_pushinteger(ls, cost);
	return 3;
}

/* Returns an improvement's ai_weight, ai_hint
 * Args: improvement (pointer)
 * Returns: ai_weight (int), ai_hint (int)
 */
int lua_improvementGetInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_improvementGetInfo: incorrect number of arguments\n");
		lua_error(ls);
	}

	int ai_weight = 0;
	Improvement::AIHint ai_hint = Improvement::AIHINT_NONE;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_improvementGetInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const Improvement *improvement = static_cast<const Improvement *>(ptr);
		ai_weight = improvement->getAIWeight();
		ai_hint = improvement->getAIHint();
	}
	arg_index++;

	lua_pushinteger(ls, ai_weight);
	lua_pushinteger(ls, static_cast<int>(ai_hint));
	return 2;
}

/* Returns a unit template's types.
 * Args: unit_template (pointer)
 * Returns: is_foot (boolean), is_air (boolean), is_missile (boolean), is_sea (boolean)
 */
int lua_unittemplateType(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unittemplateType: incorrect number of arguments\n");
		lua_error(ls);
	}

	bool is_foot = false, is_air = false, is_missile = false, is_sea = false;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unittemplateType: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		UnitTemplate *unit_template = static_cast<UnitTemplate *>(ptr);
		is_foot = unit_template->isFoot();
		is_air = unit_template->isAir();
		is_missile = unit_template->isMissile();
		is_sea = unit_template->isSea();
	}
	arg_index++;

	lua_pushboolean(ls, is_foot);
	lua_pushboolean(ls, is_air);
	lua_pushboolean(ls, is_missile);
	lua_pushboolean(ls, is_sea);
	return 4;
}

/* Returns a unit template's name.
 * Args: unit_template (pointer)
 * Returns: name (string)
 */
int lua_unittemplateGetName(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unittemplateGetName: incorrect number of arguments\n");
		lua_error(ls);
	}

	const char *name = NULL;

	int arg_index = 1;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unittemplateGetName: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		UnitTemplate *unit_template = static_cast<UnitTemplate *>(ptr);
		name = unit_template->getName();
	}
	arg_index++;

	lua_pushstring(ls, name);
	return 1;
}

/* Returns a unit template's stats.
 * Args: unit_template (pointer)
 * Returns: attack (int), defence (int), moves (int), cost (int)
 */
int lua_unittemplateStats(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unittemplateStats: incorrect number of arguments\n");
		lua_error(ls);
	}

	int attack = 0, defence = 0, moves = 0, cost = 0;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unittemplateStats: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		UnitTemplate *unit_template = static_cast<UnitTemplate *>(ptr);
		attack = unit_template->getAttack();
		defence = unit_template->getDefence();
		moves = unit_template->getMoves();
		cost = unit_template->getCost();
	}
	arg_index++;

	lua_pushinteger(ls, attack);
	lua_pushinteger(ls, defence);
	lua_pushinteger(ls, moves);
	lua_pushinteger(ls, cost);
	return 4;
}

/* Returns a unit template's air stats.
 * Args: unit_template (pointer)
 * Returns: air_attack (int), air_defence (int), bombard_power (int), bombard (int), air_range (int), nucleartype NUCLEARTYPE_* (int)
 */
int lua_unittemplateAirStats(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unittemplateAirStats: incorrect number of arguments\n");
		lua_error(ls);
	}

	int air_attack = 0, air_defence = 0, bombard_power = 0, bombard = 0, air_range = 0;
	UnitTemplate::NuclearType nucleartype = UnitTemplate::NUCLEARTYPE_NONE;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unittemplateAirStats: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		UnitTemplate *unit_template = static_cast<UnitTemplate *>(ptr);
		air_attack = unit_template->getAirAttack();
		air_defence = unit_template->getAirDefence();
		bombard_power = unit_template->getBombardPower();
		bombard = unit_template->getBombard();
		air_range = unit_template->getAirRange();
		nucleartype = unit_template->getNuclearType();
	}
	arg_index++;

	lua_pushinteger(ls, air_attack);
	lua_pushinteger(ls, air_defence);
	lua_pushinteger(ls, bombard_power);
	lua_pushinteger(ls, bombard);
	lua_pushinteger(ls, air_range);
	lua_pushinteger(ls, static_cast<int>(nucleartype));
	return 6;
}

/* Returns a unit template's sea stats.
 * Args: unit_template (pointer)
 * Returns: sea_attack (int), sea_defence_range (int)
 */
int lua_unittemplateSeaStats(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unittemplateAirStats: incorrect number of arguments\n");
		lua_error(ls);
	}

	int sea_attack = 0, sea_defence_range = 0;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unittemplateAirStats: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		UnitTemplate *unit_template = static_cast<UnitTemplate *>(ptr);
		sea_attack = unit_template->getSeaAttack();
		sea_defence_range = unit_template->getSeaDefenceRange();
	}
	arg_index++;

	lua_pushinteger(ls, sea_attack);
	lua_pushinteger(ls, sea_defence_range);
	return 2;
}

/* Returns a unit template's abilities.
 * Args: unit_template (pointer)
 * Returns: can_build_roads (boolean), can_build_city (boolean), can_bomb (boolean)
 */
int lua_unittemplateAbility(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unittemplateAbility: incorrect number of arguments\n");
		lua_error(ls);
	}

	bool can_build_roads = false, can_build_city = false, can_bomb = false;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unittemplateAbility: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		UnitTemplate *unit_template = static_cast<UnitTemplate *>(ptr);
		can_build_roads = unit_template->canBuildRoads();
		can_build_city = unit_template->canBuildCity();
		can_bomb = unit_template->canBomb();
	}
	arg_index++;

	lua_pushboolean(ls, can_build_roads);
	lua_pushboolean(ls, can_build_city);
	lua_pushboolean(ls, can_bomb);
	return 3;
}

/* Returns a unit's template.
 * Args: unit (pointer)
 * Returns: unit_template (pointer)
 */
int lua_unitGetTemplate(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unitGetTemplate: incorrect number of arguments\n");
		lua_error(ls);
	}

	const UnitTemplate *unit_template = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitGetTemplate: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		Unit *unit = static_cast<Unit *>(ptr);
		unit_template = unit->getTemplate();
	}
	arg_index++;

	// need const cast to return UnitTemplate to Lua! Lua won't be able modify it anyway
	luafunc_returnPointer(ls, const_cast<UnitTemplate *>(unit_template));
	return 1;
}

/* Returns a unit's civilization.
 * Args: unit (pointer)
 * Returns: civilization (pointer)
 */
int lua_unitGetCivilization(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unitGetCivilization: incorrect number of arguments\n");
		lua_error(ls);
	}

	Civilization *civilization = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitGetCivilization: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		Unit *unit = static_cast<Unit *>(ptr);
		civilization = unit->getCivilization();
	}
	arg_index++;

	luafunc_returnPointer(ls, civilization);
	return 1;
}

/* Returns a unit's position.
 * Args: unit (pointer)
 * Returns: xpos (int), ypos (int)
 */
int lua_unitGetPos(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unitGetPos: incorrect number of arguments\n");
		lua_error(ls);
	}

	Pos2D pos;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitGetPos: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		Unit *unit = static_cast<Unit *>(ptr);
		pos = unit->getPos();
	}
	arg_index++;

	lua_pushinteger(ls, pos.x);
	lua_pushinteger(ls, pos.y);
	return 2;
}

/* Returns whether the unit may be able to upgrade next turn (based on the current location, city stocks, etc - though note circumstances may change by the actual end of turn)
 * Args: unit (pointer)
 * Returns: unit_template (pointer)
 */
int lua_unitCanUpgrade(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_unitCanUpgrade: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Unit *unit = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_unitCanUpgrade: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit = static_cast<Unit *>(ptr);
	}
	arg_index++;

	const UnitTemplate *upgrade = NULL;
	if( unit->getCivilization() == AIInterface::getAICivilization() ) {
		upgrade = unit->canUpgrade();
	}
	else {
		VI_log("lua_unitCanUpgrade: can't call for enemy units\n", arg_index);
		lua_error(ls);
	}

	// need const cast to return UnitTemplate to Lua! Lua won't be able modify it anyway
	luafunc_returnPointer(ls, const_cast<UnitTemplate *>(upgrade));
	return 1;
}

/* Returns a civilization's name.
 * Args: civilization (pointer)
 * Returns: name (string)
 */
int lua_civilizationGetName(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_civilizationGetName: incorrect number of arguments\n");
		lua_error(ls);
	}

	const char *name = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_civilizationGetName: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const Civilization *civilization = static_cast<const Civilization *>(ptr);
		name = civilization->getName();
	}
	arg_index++;

	lua_pushstring(ls, name);
	return 1;
}

/* Returns a city's civilization.
 * Args: city (pointer)
 * Returns: civilization (pointer)
 */
int lua_cityGetCivilization(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_cityGetCivilization: incorrect number of arguments\n");
		lua_error(ls);
	}

	Civilization *civilization = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetPos: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const City *city = static_cast<const City *>(ptr);
		civilization = city->getCivilization();
	}
	arg_index++;

	luafunc_returnPointer(ls, civilization);
	return 1;
}

/* Returns a city's position.
 * Args: city (pointer)
 * Returns: xpos (int), ypos (int)
 */
int lua_cityGetPos(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_cityGetPos: incorrect number of arguments\n");
		lua_error(ls);
	}

	Pos2D pos;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetPos: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const City *city = static_cast<const City *>(ptr);
		pos = city->getPos();
	}
	arg_index++;

	lua_pushinteger(ls, pos.x);
	lua_pushinteger(ls, pos.y);
	return 2;
}

/* Returns a city's name.
 * Args: city (pointer)
 * Returns: name (string)
 */
int lua_cityGetName(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_cityGetName: incorrect number of arguments\n");
		lua_error(ls);
	}

	const char *name = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetName: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const City *city = static_cast<const City *>(ptr);
		name = city->getName();
		//VI_log("lua_cityGetName: %s\n", city->getName());
	}
	arg_index++;

	lua_pushstring(ls, name);
	return 1;
}

/* Returns a city's size.
 * Args: city (pointer)
 * Returns: size (int)
 */
int lua_cityGetPublicInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_cityGetPublicInfo: incorrect number of arguments\n");
		lua_error(ls);
	}

	int size = 0;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetPublicInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const City *city = static_cast<const City *>(ptr);
		size = city->getSize();
	}
	arg_index++;

	lua_pushinteger(ls, size);
	return 1;
}

/* Returns whether "capital" (first city), production, science, sea travel range, air travel range.
 * is_capital is always false, production, science sea_travel_range, air_travel_range always 0, for cities that don't belong to the AI.
 * Args: city (pointer)
 * Returns: is_capital (boolean), production (int), science (int), sea_travel_range (int), air_travel_range (int)
 */
int lua_cityGetInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_cityGetInfo: incorrect number of arguments\n");
		lua_error(ls);
	}

	bool is_capital = false;
	int production = 0, science = 0, sea_travel_range = 0, air_travel_range = 0;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const City *city = static_cast<const City *>(ptr);
		if( city->getCivilization() == AIInterface::getAICivilization() ) {
			is_capital = city == AIInterface::getAICivilization()->getCity(0);
			production = city->calculateProduction();
			science = city->calculateScience();
			sea_travel_range = city->getTravelRange(false);
			air_travel_range = city->getTravelRange(true);
		}
		else {
			VI_log("lua_cityGetInfo: can't call for enemy cities\n", arg_index);
			lua_error(ls);
		}
	}
	arg_index++;

	lua_pushboolean(ls, is_capital);
	lua_pushinteger(ls, production);
	lua_pushinteger(ls, science);
	lua_pushinteger(ls, sea_travel_range);
	lua_pushinteger(ls, air_travel_range);
	return 5;
}

/* Returns sea travel range, air travel range.
 * sea_travel_range, air_travel_range is always 0 for cities that don't belong to the AI.
 * Args: city (pointer)
 * Returns: sea_travel_range (int), air_travel_range (int)
 */
int lua_cityGetRanges(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_cityGetRanges: incorrect number of arguments\n");
		lua_error(ls);
	}

	int sea_travel_range = 0, air_travel_range = 0;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetRanges: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		const City *city = static_cast<const City *>(ptr);
		if( city->getCivilization() == AIInterface::getAICivilization() ) {
			sea_travel_range = city->getTravelRange(false);
			air_travel_range = city->getTravelRange(true);
		}
		else {
			VI_log("lua_cityGetRanges: can't call for enemy cities\n", arg_index);
			lua_error(ls);
		}
	}
	arg_index++;

	lua_pushinteger(ls, sea_travel_range);
	lua_pushinteger(ls, air_travel_range);
	return 2;
}

/* Returns whether a city can build something.
 * can_build is always false for cities that don't belong to the AI.
 * Args: city (pointer), buildable (pointer)
 * Returns: can_build (boolean)
 */
int lua_cityCanBuild(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_cityCanBuild: incorrect number of arguments\n");
		lua_error(ls);
	}

	const City *city = NULL;
	const Buildable *buildable = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityCanBuild: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		city = static_cast<const City *>(ptr);
	}
	arg_index++;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityCanBuild: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		//const char *buildable_name = lua_tostring(ls, arg_index);
		//buildable = static_cast<const Buildable *>(lua_touserdata(ls, arg_index));
		void *ptr = lua_touserdata(ls, arg_index);
		buildable = static_cast<const Buildable *>(ptr);
	}
	arg_index++;

	//bool can_build = city->getCivilization() == AIInterface::getAICivilization() && city->canBuild(buildable);
	bool can_build = false;
	if( city->getCivilization() == AIInterface::getAICivilization() ) {
		can_build = city->canBuild(buildable);
	}
	else {
		VI_log("lua_cityCanBuild: can't call for enemy cities\n", arg_index);
		lua_error(ls);
	}
	lua_pushboolean(ls, can_build);
	return 1;
}

/* Returns whether a city can launch an air unit type.
 * can_launch is always false for cities that don't belong to the AI.
 * Args: city (pointer), unit_template (pointer)
 * Returns: can_launch (boolean)
 */
int lua_cityCanLaunch(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_cityCanLaunch: incorrect number of arguments\n");
		lua_error(ls);
	}

	const City *city = NULL;
	const UnitTemplate *unit_template = NULL;

	int arg_index = 1;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityCanLaunch: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		city = static_cast<const City *>(ptr);
	}
	arg_index++;

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_cityCanLaunch: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<const UnitTemplate *>(ptr);
	}
	arg_index++;

	//bool can_launch = city->getCivilization() == AIInterface::getAICivilization() && city->canLaunch(unit_template);
	bool can_launch = false;
	if( city->getCivilization() == AIInterface::getAICivilization() ) {
		can_launch = city->canLaunch(unit_template);
	}
	else {
		VI_log("lua_cityCanLaunch: can't call for enemy cities\n", arg_index);
		lua_error(ls);
	}
	lua_pushboolean(ls, can_launch);
	return 1;
}

/* Returns whether a city can be built at this location.
 * (Always returns false if square not explored.)
 * Args: xpos (int), ypos (int)
 * Returns: can_build_city (boolean)
 */
int lua_canBuildCity(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_canBuildCity: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_canBuildCity: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_canBuildCity: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	try {
		bool can_build = AIInterface::canBuildCity(xpos, ypos);
		lua_pushboolean(ls, can_build);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns whether a square can have improvements (roads, railways) built at this location.
 * (Always returns false if square not explored.)
 * Args: xpos (int), ypos (int)
 * Returns: can_be_improved (boolean)
 */
int lua_canBeImproved(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_canBeImproved: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_canBeImproved: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_canBeImproved: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	try {
		bool can_be_improved = AIInterface::canBeImproved(xpos, ypos);
		lua_pushboolean(ls, can_be_improved);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns whether the square is explored.
 * Args: xpos (int), ypos (int)
 * Returns: explored (boolean)
 */
int lua_isExplored(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_isExplored: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_isExplored: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_isExplored: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool is_explored = AIInterface::getAICivilization()->isExplored(xpos, ypos);
	lua_pushboolean(ls, is_explored);
	return 1;
}

/* Returns true if the square is unexplored, but at least one adjacent square is explored.
 * Args: xpos (int), ypos (int)
 * Returns: explorable (boolean)
 */
int lua_isExplorable(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_isExplorable: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_isExplorable: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_isExplorable: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool is_explorable = false;
	if( !AIInterface::getAICivilization()->isExplored(xpos, ypos) ) {
		is_explorable = AIInterface::hasAdjacentExplored(xpos, ypos);
	}
	lua_pushboolean(ls, is_explorable);
	return 1;
}

/* Returns the terrain at this square.
 * Returns TYPE_UNKNOWN if not explored.
 * Args: xpos (int), ypos (int)
 * Returns: terrain TYPE_* (int)
 */
int lua_getTerrain(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_getTerrain: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getTerrain: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getTerrain: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	Type terrain = AIInterface::getTerrain(xpos, ypos);
	lua_pushinteger(ls, static_cast<int>(terrain));
	return 1;
}

/* Returns the territory at this square.
 * Returns NULL if not explored.
 * Args: xpos (int), ypos (int)
 * Returns: civilization
 */
int lua_getTerritory(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_getTerritory: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getTerritory: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getTerritory: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	const Civilization *civilization = AIInterface::getTerritory(xpos, ypos);
	luafunc_returnPointer(ls, const_cast<Civilization *>(civilization)); // lua doesn't allow non-const
	return 1;
}

/* Returns whether the square is owned by a civilization the AI is at war with.
 * Returns NULL if not explored.
 * Args: xpos (int), ypos (int)
 * Returns: bool
 */
int lua_isEnemyTerritory(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_isEnemyTerritory: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_isEnemyTerritory: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_isEnemyTerritory: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool is_enemy = false;
	const Civilization *civilization = AIInterface::getTerritory(xpos, ypos);
	if( civilization != NULL && civilization != AIInterface::getAICivilization() ) {
		const Relationship *relationship = AIInterface::findRelationship(civilization);
		ASSERT( relationship != NULL );
		is_enemy = relationship->getStatus() == Relationship::STATUS_WAR;
	}

	lua_pushboolean(ls, is_enemy);
	return 1;
}

/* Returns whether there is a bonus resource at this square.
 * Returns false if no bonus, or if not explored.
 * Args: xpos (int), ypos (int)
 * Returns: bonus (boolean)
 */
int lua_hasBonusResource(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_hasBonusResource: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_hasBonusResource: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_hasBonusResource: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	const BonusResource *bonus_resource = AIInterface::getBonusResource(xpos, ypos);
	lua_pushboolean(ls, bonus_resource != NULL);
	return 1;
}

/* Returns the road at this square.
 * Returns ROAD_UNKNOWN if not explored.
 * Args: xpos (int), ypos (int)
 * Returns: road ROAD_* (int)
 */
int lua_getRoad(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_getRoad: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getRoad: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getRoad: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	Road road = AIInterface::getRoad(xpos, ypos);
	lua_pushinteger(ls, static_cast<int>(road));
	return 1;
}

/* Returns whether there's an adjacent unexplored square - i.e., moving to this square will uncover it.
 * Args: xpos (int), ypos (int)
 * Returns: adjacent_unexplored (boolean)
 */
int lua_hasAdjacentUnexplored(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_hasAdjacentUnexplored: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_hasAdjacentUnexplored: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_hasAdjacentUnexplored: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool adjacent_unexplored = false;
	try {
		adjacent_unexplored = AIInterface::hasAdjacentUnexplored(xpos, ypos);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	lua_pushboolean(ls, adjacent_unexplored);
	return 1;
}

/* Returns whether there are enemy units at a square. This means those belonging to a civilization that the AI is at war with; and only for units with attack greater than 0.
 * Args: xpos (int), ypos (int)
 * (Always returns false if square not explored.)
 * Returns: has_enemies (boolean)
 */
int lua_hasEnemies(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_hasEnemies: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_hasEnemies: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_hasEnemies: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool has_enemies = false;
	try {
		has_enemies = AIInterface::hasEnemies(xpos, ypos);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	lua_pushboolean(ls, has_enemies);
	return 1;
}

/* Returns the units at a square.
 * Args: xpos (int), ypos (int)
 * (Always returns no units if square not explored.)
 * Returns: units (table of pointers)
 */
int lua_getUnitsAt(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_getUnitsAt: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getUnitsAt: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getUnitsAt: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	try {
		const vector<Unit *> *units = AIInterface::getUnits(xpos, ypos);
		/*
		// open/create table
		lua_newtable(ls);
		int top = lua_gettop(ls);

		int table_size = 0;
		if( units != NULL ) {
			table_size = units->size();
			for(int i=0;i<units->size();i++) {
				Unit *unit = units->at(i);
				// new cell
				lua_pushnumber(ls, i+1); // lua arrays start at 1
				lua_pushlightuserdata(ls, unit);
				lua_settable(ls, top); // insert the new cell (and pop index/value off stack)
			}
		}

		// close table
		lua_pushliteral(ls, "n");
		lua_pushnumber(ls, table_size); // number of cells
		lua_settable(ls, top);*/
		luafunc_returnArrayPointers(ls, units);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns the best defender at a square.
 * Args: xpos (int), ypos (int)
 * (Always returns NULL if square not explored, or no units.)
 * Returns: unit (pointer)
 */
int lua_getBestDefenderAt(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_getBestDefenderAt: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getBestDefenderAt: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getBestDefenderAt: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	Unit *unit = NULL;
	try {
		unit = AIInterface::getBestDefender(xpos, ypos);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	luafunc_returnPointer(ls, unit);
	return 1;
}

/* Returns the city at a square.
 * (Always returns NULL if square not explored.)
 * Args: xpos (int), ypos (int)
 * Returns: city (pointer)
 */
int lua_getCityAt(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_getCityAt: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getCityAt: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getCityAt: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	City *city = NULL;
	try {
		city = AIInterface::getCity(xpos, ypos);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	//VI_log("lua_getCityAt returns: %d\n", city);
	luafunc_returnPointer(ls, city);
	return 1;
}

/* Returns a list of explored adjacent squares.
 * Args: xpos (int), ypos (int), include_centre (boolean)
 * Returns: xposes (table of pointers), yposes (table of pointers)
 */
int lua_getAdjacentSquares(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_getAdjacentSquares: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getAdjacentSquares: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getAdjacentSquares: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool include_centre = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_getAdjacentSquares: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		include_centre = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	vector<Pos2D> adjacent_squares = AIInterface::getAdjacentSquares(xpos, ypos, include_centre);
	vector<int> xposes;
	vector<int> yposes;
	for(size_t i=0;i<adjacent_squares.size();i++) {
		Pos2D pos = adjacent_squares.at(i);
		xposes.push_back( pos.x );
		yposes.push_back( pos.y );
	}
	luafunc_returnArrayInts(ls, &xposes);
	luafunc_returnArrayInts(ls, &yposes);
	return 2;
}

/* Returns a list of explored city squares.
 * Args: xpos (int), ypos (int), include_centre (boolean)
 * Returns: xposes (table of pointers), yposes (table of pointers)
 */
int lua_getCitySquares(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 3 ) {
		VI_log("lua_getCitySquares: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getCitySquares: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getCitySquares: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool include_centre = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_getCitySquares: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		include_centre = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	vector<Pos2D> city_squares = AIInterface::getCitySquares(xpos, ypos, include_centre);
	vector<int> xposes;
	vector<int> yposes;
	for(size_t i=0;i<city_squares.size();i++) {
		Pos2D pos = city_squares.at(i);
		xposes.push_back( pos.x );
		yposes.push_back( pos.y );
	}
	luafunc_returnArrayInts(ls, &xposes);
	luafunc_returnArrayInts(ls, &yposes);
	return 2;
}

/* Returns whether the AI has discovered a technology.
 * Args: technology (string)
 * Returns: answer (boolean)
 */
int lua_haveTechnology(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_haveTechnology: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const Technology *technology = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_haveTechnology: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		const char *tech_name = lua_tostring(ls, arg_index);
		if( *tech_name != '\0' ) {
			technology = game_g->findTechnology(tech_name);
			if( technology == NULL ) {
				VI_log("lua_haveTechnology: requires1 unknown tech %s\n", tech_name);
				lua_error(ls);
			}
		}
	}
	arg_index++;

	bool answer = AIInterface::getAICivilization()->hasTechnology(technology);
	lua_pushboolean(ls, answer);

	return 1;
}

/* Returns whether the AI has discovered all technologies.
 * Args: none
 * Returns: answer (boolean)
 */
int lua_haveAllTechnology(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 0 ) {
		VI_log("lua_haveAllTechnology: incorrect number of arguments\n");
		lua_error(ls);
	}

	bool answer = AIInterface::getAICivilization()->hasAllTechnology();
	lua_pushboolean(ls, answer);

	return 1;
}

/* Returns the list of the civilizations.
 * Args: type GETCIVILIZATIONS_* (int)
 * Returns: civilizations (table of pointers)
 */
int lua_getCivilizations(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_getCivilizations: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	AIInterface::GetcivilizationsType type = AIInterface::GETCIVILIZATIONS_ALL;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getCivilizations: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		type = static_cast<AIInterface::GetcivilizationsType>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	try {
		const vector<Civilization *> civilizations = AIInterface::getCivilizations(type);
		luafunc_returnArrayPointers(ls, &civilizations);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns the list of the civilization's cities
 * Args: civilization (pointer)
 * Returns: cities (table of pointers)
 */
int lua_getCities(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_getCities: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Civilization *civilization = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_getCities: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		civilization = static_cast<Civilization *>(ptr);
	}
	arg_index++;

	try {
		const vector<City *> cities = AIInterface::getCities(civilization);
		luafunc_returnArrayPointers(ls, &cities);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns the list of the AI's cities
 * Args: none
 * Returns: cities (table of pointers)
 */
int lua_getMyCities(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 0 ) {
		VI_log("lua_getMyCities: incorrect number of arguments\n");
		lua_error(ls);
	}

	try {
		const vector<City *> cities = AIInterface::getCities(AIInterface::getAICivilization());
		luafunc_returnArrayPointers(ls, &cities);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns the list of the civilization's units
 * Args: civilization (pointer)
 * Returns: units (table of pointers)
 */
int lua_getUnits(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_getUnits: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Civilization *civilization = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_getUnits: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		civilization = static_cast<Civilization *>(ptr);
	}
	arg_index++;

	try {
		const vector<Unit *> units = AIInterface::getUnits(civilization);
		luafunc_returnArrayPointers(ls, &units);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns the list of the AI's units
 * Args: none
 * Returns: units (table of pointers)
 */
int lua_getMyUnits(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 0 ) {
		VI_log("lua_getMyUnits: incorrect number of arguments\n");
		lua_error(ls);
	}

	try {
		const vector<Unit *> units = AIInterface::getUnits(AIInterface::getAICivilization());
		luafunc_returnArrayPointers(ls, &units);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns relationship info for a civilization
 * Args: civilization (pointer)
 * Returns: made_contact (boolean), at_war (boolean) (at_war is false if not made contact)
 */
int lua_getRelationshipInfo(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_getRelationshipInfo: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Civilization *civilization = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_getRelationshipInfo: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		civilization = static_cast<Civilization *>(ptr);
	}
	arg_index++;

	bool made_contact = false, at_war = false;
	try {
		const Relationship *relationship = AIInterface::findRelationship(civilization);
		made_contact = relationship->madeContact();
		at_war = relationship->getStatus() != Relationship::STATUS_PEACE;
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	lua_pushboolean(ls, made_contact);
	lua_pushboolean(ls, at_war);
	return 2;
}

/* Returns true if the civilization is the rebel civ
 * Args: civilization (pointer)
 * Returns: is_rebel_civ (boolean)
 */
int lua_isRebelCiv(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_isRebelCiv: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	Civilization *civilization = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_isRebelCiv: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		civilization = static_cast<Civilization *>(ptr);
	}
	arg_index++;

	bool is_rebel_civ = false;
	try {
		T_ASSERT( AIInterface::getRebelCiv() != NULL );
		is_rebel_civ = civilization == AIInterface::getRebelCiv();
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	lua_pushboolean(ls, is_rebel_civ);
	return 1;
}

/* Returns the buildables that a city can build.
 * (Always returns empty array if city doesn't belong to the AI.)
 * Args: city (pointer), type GETBUILDABLES_* (int)
 * Returns: buildables (table of pointers)
 */
int lua_cityGetBuildables(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 2 ) {
		VI_log("lua_cityGetBuildables: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const City *city = NULL;
	AIInterface::GetbuildablesType type = AIInterface::GETBUILDABLES_ALL;

	if( !lua_isuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetBuildables: argument %d not userdata\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		city = static_cast<const City *>(ptr);
	}
	arg_index++;

	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_cityGetBuildables: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		type = static_cast<AIInterface::GetbuildablesType>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	try {
		vector<const Buildable *> buildables = AIInterface::cityGetBuildables(city, type);
		luafunc_returnArrayConstPointers(ls, &buildables);
		/*
		// open/create table
		lua_newtable(ls);
		int top = lua_gettop(ls);

		int table_size = 0;
		table_size = buildables.size();
		for(int i=0;i<buildables.size();i++) {
			const Buildable *buildable = buildables.at(i);
			// new cell
			lua_pushnumber(ls, i+1); // lua arrays start at 1
			// need const cast to return UnitTemplate to Lua! Lua won't be able modify it anyway
			lua_pushlightuserdata(ls, const_cast<Buildable *>(buildable));
			lua_settable(ls, top); // insert the new cell (and pop index/value off stack)
		}

		// close table
		lua_pushliteral(ls, "n");
		lua_pushnumber(ls, table_size); // number of cells
		lua_settable(ls, top);
		*/
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 1;
}

/* Returns the buildable that a city is currently building.
 * (Always returns NULL if not building anything, or city doesn't belong to the AI.)
 * Args: city (pointer)
 * Returns: buildable (pointer)
 */
int lua_cityGetBuildable(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_cityGetBuildable: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const City *city = NULL;

	if( !lua_isuserdata(ls, arg_index) ) {
		VI_log("lua_cityGetBuildable: argument %d not userdata\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		city = static_cast<const City *>(ptr);
	}
	arg_index++;

	const Buildable *buildable = NULL;
	try {
		buildable = AIInterface::cityGetBuildable(city);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	// need const cast to return UnitTemplate to Lua! Lua won't be able modify it anyway
	luafunc_returnPointer(ls, const_cast<Buildable *>(buildable));
	return 1;
}

/* Returns the game year
 * Args: none
 * Returns: year (int)
 */
int lua_getYear(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 0 ) {
		VI_log("lua_getYear: incorrect number of arguments\n");
		lua_error(ls);
	}

	int year = -1;

	try {
		year = AIInterface::getYear();
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	lua_pushinteger(ls, year);
	return 1;
}

/* Returns whether an air unit can bomb a square.
 * Args: unit_template (pointer), city (pointer), target_xpos (int), target_ypos (int), check_launch_capability (boolean)
 * Returns: can_bomb (boolean)
 */
int lua_canBomb(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 5 ) {
		VI_log("lua_canBomb: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const UnitTemplate *unit_template = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_canBomb: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit_template = static_cast<const UnitTemplate *>(ptr);
	}
	arg_index++;

	const City *city = NULL;

	if( !lua_isuserdata(ls, arg_index) ) {
		VI_log("lua_canBomb: argument %d not userdata\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		city = static_cast<const City *>(ptr);
	}
	arg_index++;

	int target_xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_canBomb: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		target_xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int target_ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_canBomb: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		target_ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;


	bool check_launch_capability = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_canBomb: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		check_launch_capability = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	bool can_bomb = false;

	try {
		can_bomb = AIInterface::canBomb(unit_template, city, Pos2D(target_xpos, target_ypos), check_launch_capability);
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	lua_pushboolean(ls, can_bomb);
	return 1;
}

/* Calculates distance map.
 * Args: unit (pointer), xpos (int), ypos (int), include_travel (bool)
 * unit may be NULL, to indicate a general land unit.
 * Returns: costs (table of ints), squares (table of ints), width (int), height (int)
 */
int lua_calculateDistanceMap(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 4 ) {
		VI_log("lua_calculateDistanceMap: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const Unit *unit = NULL;
	if( !lua_isnil(ls, arg_index) && !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_calculateDistanceMap: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit = static_cast<const Unit *>(ptr);
	}
	arg_index++;

	int xpos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_calculateDistanceMap: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		xpos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int ypos = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_calculateDistanceMap: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		ypos = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	bool include_travel = false;
	if( !lua_isboolean(ls, arg_index) ) {
		VI_log("lua_calculateDistanceMap: argument %d not a boolean\n", arg_index);
		lua_error(ls);
	}
	else {
		include_travel = lua_toboolean(ls, arg_index)!=0 ? true : false;
	}
	arg_index++;

	const Distance *dists = NULL;
	int width = 0, height = 0;

	try {
		dists = AIInterface::calculateDistanceMap(&width, &height, unit, xpos, ypos, include_travel);
		//dists = new Distance[width*height];
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	// need const cast for Lua!
	lua_pushlightuserdata(ls, const_cast<Distance *>(dists));

	lua_pushinteger(ls, width);
	lua_pushinteger(ls, height);
	//return 4;
	return 3;
}

/* Returns the distance cost.
 * Args: dists (pointer), width (int), x (int), y (int)
 * Returns: distance cost (int), squares (int)
 */
int lua_getDistanceCost(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 4 ) {
		VI_log("lua_getDistanceCost: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const Distance *dists = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_getDistanceCost: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		dists = static_cast<const Distance *>(ptr);
	}
	arg_index++;

	int width = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getDistanceCost: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		width = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int x = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getDistanceCost: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		x = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	int y = 0;
	if( !lua_isnumber(ls, arg_index) ) {
		VI_log("lua_getDistanceCost: argument %d not a number\n", arg_index);
		lua_error(ls);
	}
	else {
		y = static_cast<int>(lua_tointeger(ls, arg_index));
	}
	arg_index++;

	const Distance *dist = &dists[y * width + x];
	lua_pushinteger(ls, dist->cost);
	lua_pushinteger(ls, dist->squares);
	//lua_pushboolean(ls, dist->is_travelling);
	/*if( dist->is_travelling ) {
		VI_log("travelling: %d, %d\n", x, y);
	}*/
	return 2;
}

/* Automates a worker unit.
 * Args: unit (pointer), costs (table of ints), squares (table of ints)
 * Returns: found_target (boolean), dist (int), candidates_x (table of ints), candidates_y (table of ints)
 */
int lua_automateWorker(lua_State *ls) {
	int n_args = lua_gettop(ls);
	//if( n_args != 3 ) {
	if( n_args != 2 ) {
		VI_log("lua_automateWorker: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const Unit *unit = NULL;
	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_automateWorker: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		unit = static_cast<const Unit *>(ptr);
	}
	arg_index++;

	int time_s = clock();
	//Distance *dists = NULL;
	const Distance *dists = NULL;
	/*int size = 0;

	if( !lua_istable(ls, arg_index) ) {
		VI_log("lua_automateWorker: argument %d not a table\n", arg_index);
		lua_error(ls);
	}
	else {
		size = lua_objlen(ls, arg_index);
		dists = new Distance[size];
		for(int i=0;i<size;i++) {
			lua_pushinteger(ls, i+1);
			lua_gettable(ls, arg_index);
			if( !lua_isnumber(ls, -1) ) {
				VI_log("lua_automateWorker: argument %d table index %d not a number\n", arg_index, i+1);
				break;
			}
			dists[i].cost = lua_tointeger(ls, -1);
			lua_pop(ls, 1);
		}
	}
	arg_index++;

	if( !lua_istable(ls, arg_index) ) {
		VI_log("lua_automateWorker: argument %d not a table\n", arg_index);
		lua_error(ls);
	}
	else {
		if( size != lua_objlen(ls, arg_index) ) {
			VI_log("lua_automateWorker: argument %d unexpected length of table\n", arg_index);
			lua_error(ls);
		}
		for(int i=0;i<size;i++) {
			lua_pushinteger(ls, i+1);
			lua_gettable(ls, arg_index);
			if( !lua_isnumber(ls, -1) ) {
				VI_log("lua_automateWorker: argument %d table index %d not a number\n", arg_index, i+1);
				break;
			}
			dists[i].squares = lua_tointeger(ls, -1);
			lua_pop(ls, 1);
		}
	}
	arg_index++;*/

	if( !lua_islightuserdata(ls, arg_index) ) {
		VI_log("lua_automateWorker: argument %d not a pointer\n", arg_index);
		lua_error(ls);
	}
	else {
		void *ptr = lua_touserdata(ls, arg_index);
		dists = static_cast<const Distance *>(ptr);
	}
	arg_index++;

	/*static int total_time = 0;
	int time = clock() - time_s;
	total_time += time;
	VI_log("<<< %d : %d\n", time, total_time);*/

	bool found_target = false;
	int dist = 0;
	vector<int> candidates_x;
	vector<int> candidates_y;
	try {
		vector<Pos2D> candidates;
		found_target = unit->automateBuildRoads(&dist, &candidates, dists);
		for(vector<Pos2D>::const_iterator iter = candidates.begin(); iter != candidates.end(); ++iter) {
			Pos2D pos = *iter;
			candidates_x.push_back(pos.x);
			candidates_y.push_back(pos.y);
		}
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	//delete [] dists;

	lua_pushboolean(ls, found_target);
	lua_pushinteger(ls, dist);
	luafunc_returnArrayInts(ls, &candidates_x);
	luafunc_returnArrayInts(ls, &candidates_y);

	return 4;
}

/* Logs the text to Conquests's log file.
 * Args: text (string)
 * Returns: nothing
 */
int lua_log(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 1 ) {
		VI_log("lua_log: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	const char *text = NULL;
	if( !lua_isstring(ls, arg_index) ) {
		VI_log("lua_log: argument %d not a string\n", arg_index);
		lua_error(ls);
	}
	else {
		text = lua_tostring(ls, arg_index);
	}
	arg_index++;

	try {
		VI_log("Lua: ");
		VI_log(text);
		VI_log("\n");
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 0;
}

/*
 * Args:
 * Returns: nothing
 */
/*int lua_(lua_State *ls) {
	int n_args = lua_gettop(ls);
	if( n_args != 0 ) {
		VI_log("lua_: incorrect number of arguments\n");
		lua_error(ls);
	}

	int arg_index = 1;

	try {
	}
	catch(const char *error) {
		VI_log("error: %s\n", error);
		lua_error(ls);
	}

	return 0;
}*/

// remember to only return what a civ can see!

void registerScripts(lua_State *ls) {
	VI_log("register lua functions...\n");
	lua_register(ls, "conquests_addNewTechnology", lua_addNewTechnology);
	lua_register(ls, "conquests_addNewImprovement", lua_addNewImprovement);
	lua_register(ls, "conquests_addNewUnitTemplate", lua_addNewUnitTemplate);
	lua_register(ls, "conquests_addNewBonus", lua_addNewBonus);
	lua_register(ls, "conquests_bonusAddTerrain", lua_bonusAddTerrain);
	lua_register(ls, "conquests_addNewElement", lua_addNewElement);
	lua_register(ls, "conquests_elementSetTerrainOutput", lua_elementSetTerrainOutput);
	lua_register(ls, "conquests_elementSetBonusOutput", lua_elementSetBonusOutput);
	lua_register(ls, "conquests_technologySetInfo", lua_technologySetInfo);
	lua_register(ls, "conquests_technologySetAIHint", lua_technologySetAIHint);
	lua_register(ls, "conquests_technologySetTerrain", lua_technologySetTerrain);
	lua_register(ls, "conquests_technologySetPreventsRebellion", lua_technologySetPreventsRebellion);
	lua_register(ls, "conquests_buildableSetInfo", lua_buildableSetInfo);
	lua_register(ls, "conquests_buildableSetAdvice", lua_buildableSetAdvice);
	lua_register(ls, "conquests_buildableSetRequiresImprovement", lua_buildableSetRequiresImprovement);
	lua_register(ls, "conquests_buildableSetObsoletedBy", lua_buildableSetObsoletedBy);
	lua_register(ls, "conquests_buildableSetReplacedBy", lua_buildableSetReplacedBy);
	lua_register(ls, "conquests_buildableSetRaceSpecific", lua_buildableSetRaceSpecific);
	lua_register(ls, "conquests_buildableSetRequiresElement", lua_buildableSetRequiresElement);
	lua_register(ls, "conquests_buildableSetRequiresBonus", lua_buildableSetRequiresBonus);
	lua_register(ls, "conquests_improvementSetRequiresCoastal", lua_improvementSetRequiresCoastal);
	lua_register(ls, "conquests_improvementSetTravelRange", lua_improvementSetTravelRange);
	lua_register(ls, "conquests_improvementSetPowerPerTurn", lua_improvementSetPowerPerTurn);
	lua_register(ls, "conquests_improvementRebellionBonus", lua_improvementRebellionBonus);
	lua_register(ls, "conquests_improvementSetGrowthRate", lua_improvementSetGrowthRate);
	lua_register(ls, "conquests_improvementSetProductionBonus", lua_improvementSetProductionBonus);
	lua_register(ls, "conquests_improvementSetDefenceBonus", lua_improvementSetDefenceBonus);
	lua_register(ls, "conquests_improvementSetAutoVeteranBonus", lua_improvementSetAutoVeteranBonus);
	lua_register(ls, "conquests_improvementSetResearchMultiplierBonus", lua_improvementSetResearchMultiplierBonus);
	lua_register(ls, "conquests_improvementSetAIHint", lua_improvementSetAIHint);
	lua_register(ls, "conquests_improvementSetAirDefence", lua_improvementSetAirDefence);
	lua_register(ls, "conquests_improvementSetImmuneFromBombing", lua_improvementSetImmuneFromBombing);
	lua_register(ls, "conquests_unitTemplateSetFoot", lua_unitTemplateSetFoot);
	lua_register(ls, "conquests_unitTemplateSetCanBuildCity", lua_unitTemplateSetCanBuildCity);
	lua_register(ls, "conquests_unitTemplateSetCanBuildRoads", lua_unitTemplateSetCanBuildRoads);
	lua_register(ls, "conquests_unitTemplateSetUpgrade", lua_unitTemplateSetUpgrade);
	lua_register(ls, "conquests_unitTemplateSetAir", lua_unitTemplateSetAir);
	lua_register(ls, "conquests_unitTemplateSetVisibilityRange", lua_unitTemplateSetVisibilityRange);
	lua_register(ls, "conquests_unitTemplateSetBombard", lua_unitTemplateSetBombard);
	lua_register(ls, "conquests_unitTemplateSetNuclearType", lua_unitTemplateSetNuclearType);
	lua_register(ls, "conquests_unitTemplateSetSoundEffectCombat", lua_unitTemplateSetSoundEffectCombat);
	lua_register(ls, "conquests_unitTemplateSetSea", lua_unitTemplateSetSea);
	lua_register(ls, "conquests_addNewRace", lua_addNewRace);
	lua_register(ls, "conquests_raceSetInfo", lua_raceSetInfo);
	lua_register(ls, "conquests_raceSetStartingTechnology", lua_raceSetStartingTechnology);
	lua_register(ls, "conquests_raceAddCityName", lua_raceAddCityName);
	lua_register(ls, "conquests_findBuildable", lua_findBuildable);
	lua_register(ls, "conquests_buildableGetInfo", lua_buildableGetInfo);
	lua_register(ls, "conquests_improvementGetInfo", lua_improvementGetInfo);
	lua_register(ls, "conquests_unittemplateType", lua_unittemplateType);
	lua_register(ls, "conquests_unittemplateGetName", lua_unittemplateGetName);
	lua_register(ls, "conquests_unittemplateStats", lua_unittemplateStats);
	lua_register(ls, "conquests_unittemplateAirStats", lua_unittemplateAirStats);
	lua_register(ls, "conquests_unittemplateSeaStats", lua_unittemplateSeaStats);
	lua_register(ls, "conquests_unittemplateAbility", lua_unittemplateAbility);
	lua_register(ls, "conquests_unitGetTemplate", lua_unitGetTemplate);
	lua_register(ls, "conquests_unitGetCivilization", lua_unitGetCivilization);
	lua_register(ls, "conquests_unitGetPos", lua_unitGetPos);
	lua_register(ls, "conquests_unitCanUpgrade", lua_unitCanUpgrade);
	lua_register(ls, "conquests_civilizationGetName", lua_civilizationGetName);
	lua_register(ls, "conquests_cityGetCivilization", lua_cityGetCivilization);
	lua_register(ls, "conquests_cityGetPos", lua_cityGetPos);
	lua_register(ls, "conquests_cityGetName", lua_cityGetName);
	lua_register(ls, "conquests_cityGetPublicInfo", lua_cityGetPublicInfo);
	lua_register(ls, "conquests_cityGetInfo", lua_cityGetInfo);
	lua_register(ls, "conquests_cityGetRanges", lua_cityGetRanges);
	lua_register(ls, "conquests_cityCanBuild", lua_cityCanBuild);
	lua_register(ls, "conquests_cityCanLaunch", lua_cityCanLaunch);
	lua_register(ls, "conquests_isExplored", lua_isExplored);
	lua_register(ls, "conquests_isExplorable", lua_isExplorable);
	lua_register(ls, "conquests_getTerrain", lua_getTerrain);
	lua_register(ls, "conquests_isEnemyTerritory", lua_isEnemyTerritory);
	lua_register(ls, "conquests_getTerritory", lua_getTerritory);
	lua_register(ls, "conquests_hasBonusResource", lua_hasBonusResource);
	lua_register(ls, "conquests_getRoad", lua_getRoad);
	lua_register(ls, "conquests_canBuildCity", lua_canBuildCity);
	lua_register(ls, "conquests_canBeImproved", lua_canBeImproved);
	lua_register(ls, "conquests_hasAdjacentUnexplored", lua_hasAdjacentUnexplored);
	lua_register(ls, "conquests_hasEnemies", lua_hasEnemies);
	lua_register(ls, "conquests_getUnitsAt", lua_getUnitsAt);
	lua_register(ls, "conquests_getBestDefenderAt", lua_getBestDefenderAt);
	lua_register(ls, "conquests_getCityAt", lua_getCityAt);
	lua_register(ls, "conquests_getAdjacentSquares", lua_getAdjacentSquares);
	lua_register(ls, "conquests_getCitySquares", lua_getCitySquares);
	lua_register(ls, "conquests_haveTechnology", lua_haveTechnology);
	lua_register(ls, "conquests_haveAllTechnology", lua_haveAllTechnology);
	lua_register(ls, "conquests_getCivilizations", lua_getCivilizations);
	lua_register(ls, "conquests_getCities", lua_getCities);
	lua_register(ls, "conquests_getMyCities", lua_getMyCities);
	lua_register(ls, "conquests_getUnits", lua_getUnits);
	lua_register(ls, "conquests_getMyUnits", lua_getMyUnits);
	lua_register(ls, "conquests_getRelationshipInfo", lua_getRelationshipInfo);
	lua_register(ls, "conquests_isRebelCiv", lua_isRebelCiv);
	lua_register(ls, "conquests_cityGetBuildables", lua_cityGetBuildables);
	lua_register(ls, "conquests_cityGetBuildable", lua_cityGetBuildable);
	lua_register(ls, "conquests_calculateDistanceMap", lua_calculateDistanceMap);
	lua_register(ls, "conquests_getDistanceCost", lua_getDistanceCost);
	lua_register(ls, "conquests_automateWorker", lua_automateWorker);
	lua_register(ls, "conquests_getYear", lua_getYear);
	lua_register(ls, "conquests_canBomb", lua_canBomb);
	lua_register(ls, "conquests_log", lua_log);

}
