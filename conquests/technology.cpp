#include "conquests_stdafx.h"

#include "technology.h"
#include "buildable.h"
#include "civilization.h"
#include "maingamestate.h"

#include "../Vision/VisionShared.h"

#include <sstream>
using std::stringstream;

Technology::Technology(const char *name, int base_cost, const Technology *requires1, const Technology *requires2, Age age, int ai_weight) :
name(name), base_cost(base_cost), requires1(requires1), requires2(requires2), age(age), ai_weight(ai_weight), aiHint(AIHINT_NONE), terrain(TYPE_NONE), prevents_rebellion(false)
{
}

string Technology::getHelp(bool html) const {
	stringstream text;
	string nl = html ? "<br>\n" : "\n";
	string nbsp = html ? "&nbsp;" : " ";
	string bold = html ? "<b>" : "";
	string bold_end = html ? "</b>" : "";

	text << bold << this->getName() << bold_end << " (" << this->getAgeName() << "), allows:" << nl;

	bool any_units = false;
	for(size_t i=0;i<game_g->getGameData()->getNUnitTemplates();i++) {
		const UnitTemplate *unit_template = game_g->getGameData()->getUnitTemplate(i);
		if( unit_template->getRequiresTechnology() == this ) {
			if( !any_units ) {
				text << nl << "Units:" << nl;
			}
			any_units = true;
			text << nbsp << nbsp << nbsp << nbsp;
			if( html ) {
				text << "<a href=\"#" << unit_template->getName() << "\">";
			}
			text << unit_template->getName();
			if( html ) {
				text << "</a>";
			}
			text << nl;
		}
	}
	/*if( any_units ) {
		text << nl;
	}*/

	bool any_improvements = false;
	for(size_t i=0;i<game_g->getGameData()->getNImprovements();i++) {
		const Improvement *improvement = game_g->getGameData()->getImprovement(i);
		if( improvement->getRequiresTechnology() == this ) {
			if( !any_improvements ) {
				text << nl << "Improvements:" << nl;
			}
			any_improvements = true;
			text << nbsp << nbsp << nbsp << nbsp;
			if( html ) {
				text << "<a href=\"#" << improvement->getName() << "\">";
			}
			text << improvement->getName();
			if( html ) {
				text << "</a>";
			}
			text << nl;
		}
	}
	/*if( any_improvements ) {
		text << nl;
	}*/

	bool any_technology = false;
	for(size_t i=0;i<game_g->getGameData()->getNTechnologies();i++) {
		const Technology *tech = game_g->getGameData()->getTechnology(i);
		if( tech->getRequires1() == this || tech->getRequires2() == this ) {
			if( !any_technology ) {
				text << nl << "Technology:" << nl;
			}
			any_technology = true;
			text << nbsp << nbsp << nbsp << nbsp;
			if( html ) {
				text << "<a href=\"#" << tech->getName() << "\">";
			}
			text << tech->getName();
			if( html ) {
				text << "</a>";
			}
			text << nl;
		}
	}
	/*if( any_technology ) {
		text << nl;
	}*/

	bool any_elements = false;
	for(size_t i=0;i<game_g->getGameData()->getNElements();i++) {
		const Element *element = game_g->getGameData()->getElement(i);
		if( element->getRequiresTechnology() == this ) {
			if( !any_elements ) {
				text << nl << "Elements:" << nl;
			}
			any_elements = true;
			text << nbsp << nbsp << nbsp << nbsp;
			/*if( html ) {
				text << "<a href=\"#" << tech->getName() << "\">";
			}*/
			text << element->getName();
			/*if( html ) {
				text << "</a>";
			}*/
			text << nl;
		}
	}

	bool any_obsoletes = false;
	for(size_t i=0;i<game_g->getGameData()->getNBuildables();i++) {
		const Buildable *buildable = game_g->getGameData()->getBuildable(i);
		if( buildable->getObsoletedBy() == this ) {
			if( !any_obsoletes ) {
				text << nl << "Makes Obsolete:" << nl;
			}
			any_obsoletes = true;
			text << nbsp << nbsp << nbsp << nbsp;
			if( html ) {
				text << "<a href=\"#" << buildable->getName() << "\">";
			}
			text << buildable->getName();
			if( html ) {
				text << "</a>";
			}
			text << nl;
		}
	}
	/*if( any_obsoletes ) {
		text << nl;
	}*/

	bool any_bonuses = false;
	for(size_t i=0;i<game_g->getGameData()->getNBonusResources();i++) {
		const BonusResource *bonus_resource = game_g->getGameData()->getBonusResource(i);
		if( bonus_resource->getRequiresTechnology() == this ) {
			text << nl << "Required for: " << bonus_resource->getName() << nl;
			any_bonuses = true;
		}
	}
	/*if( any_bonuses ) {
		text << nl;
	}*/

	if( this->requires1 != NULL ) {
		text << nl << "Requires technology : ";
		if( html ) {
			text << "<a href=\"#" << this->requires1->getName() << "\">";
		}
		text << this->requires1->getName();
		if( html ) {
			text << "</a>";
		}
		if( this->requires2 != NULL ) {
			text << ", ";
			if( html ) {
				text << "<a href=\"#" << this->requires2->getName() << "\">";
			}
			text << this->requires2->getName();
			if( html ) {
				text << "</a>";
			}
		}
		text << nl;
	}

	if( this->getPreventsRebellion() ) {
		text << nl << "Eliminates any chance of rebellion in our civilization." << nl;
	}

	if( *this->getInfo() != '\0' ) {
		text << nl;
		text << this->getInfo();
		text << nl;
	}

	return text.str();
}

int Technology::getCost(const MainGamestate *mainGamestate) const {
	int cost = this->base_cost;
	int n_discovered = 0;
	for(size_t i=0;i<mainGamestate->getNCivilizations();i++) {
		const Civilization *civilization = mainGamestate->getCivilization(i);
		if( civilization->hasTechnology(this) )
			n_discovered++;
	}
	if( n_discovered == 1 ) {
		//cost *= 0.9;
		cost = (int)(cost * 0.9);
	}
	else if( n_discovered == 2 ) {
		//cost *= 0.75;
		cost = (int)(cost * 0.75);
	}
	else if( n_discovered >= 3 ) {
		//cost *= 0.5;
		cost = (int)(cost * 0.5);
	}
	return cost;
}

string Technology::getAgeName(Age age) {
	if( age == AGE_ANCIENT )
		return "Ancient";
	else if( age == AGE_MEDIEVAL )
		return "Medieval";
	else if( age == AGE_INDUSTRIAL )
		return "Industrial";
	else if( age == AGE_MODERN )
		return "Modern";
	else {
		ASSERT( false );
		return NULL;
	}
}
