#pragma once

/** Contains classes for all items that can be built by a city (Buildable, Improvement, UnitTemplate).
*/

#include <string>
using std::string;
#include <map>
using std::map;

class MainGamestate;
class Civilization;
class MapSquare;
class Improvement;
class Technology;
class Race;
class Element;
class BonusResource;
class Distance;

#include "utils.h"
#include "common.h"

class VI_Texture;

class Buildable {
protected:
	string name;
	string info;
	string advice;
	int cost;
	const Improvement *requires_improvement;
	const Technology *requires_technology;
	/*const Element *requires_element;
	int requires_element_amount;*/
	map<const Element *, int> requires_element;
	const BonusResource *requires_bonus_resource;
	const Buildable *replaced_by;
	const Technology *obsoleted_by;
	const Race *race_specific;

	Buildable(const char *name, int cost);
public:
	virtual ~Buildable();

	const char *getName() const {
		return name.c_str();
	}
	bool equals(const char *name) const {
		bool equal = this->name == name;
		return equal;
	}
	bool equals(const Buildable *buildable) const {
		// TODO: could just compare pointers, since we should only have 1 of any given Buildable?
		return equals(buildable->getName());
	}

	void setInfo(const char *info) {
		this->info = info;
	}
	const char *getInfo() const {
		return info.c_str();
	}
	void setAdvice(const char *advice) {
		this->advice = advice;
	}
	const char *getAdvice() const {
		return advice.c_str();
	}
	virtual string getHelp(bool html=false) const=0;
	int getCost() const {
		return cost;
	}
	void setRequires(const Improvement *requires_improvement) {
		this->requires_improvement = requires_improvement;
	}
	const Improvement *getRequiresImprovement() const {
		return requires_improvement;
	}
	void setRequires(const Technology *requires_technology) {
		this->requires_technology = requires_technology;
	}
	const Technology *getRequiresTechnology() const {
		return requires_technology;
	}
	void setRequires(const Element *requires_element, int requires_element_amount);
	/*const Element *getRequiresElement() const {
		return requires_element;
	}
	int getRequiresElementAmount() const {
		return requires_element_amount;
	}*/
	int getRequiresElementAmount(const Element *element) const;
	const BonusResource *getRequiresBonusResource() const {
		return this->requires_bonus_resource;
	}
	void setRequires(const BonusResource *requires_bonus_resource) {
		this->requires_bonus_resource = requires_bonus_resource;
	}
	void setReplacedBy(const Buildable *replaced_by);
	const Buildable *getReplacedBy() const {
		return replaced_by;
	}
	void setObsoletedBy(const Technology *obsoleted_by) {
		this->obsoleted_by = obsoleted_by;
	}
	const Technology *getObsoletedBy() const {
		return obsoleted_by;
	}
	void setRaceSpecific(const Race *race_specific) {
		this->race_specific = race_specific;
	}
	const Race *getRaceSpecific() const {
		return race_specific;
	}

	enum Type {
		TYPE_IMPROVEMENT = 0,
		TYPE_UNIT = 1
	};
	virtual Type getType() const=0;
};

class Improvement : public Buildable {
public:
	enum AIHint {
		AIHINT_NONE = 0,
		AIHINT_RESEARCH = 1,
		AIHINT_FOW = 2
	};
private:
	//ImprovementType improvementType;
	AIHint aiHint;
	int ai_weight;
	bool requires_coastal; // can only be built by coastal cities?
	int travel_range;
	bool travel_by_air;
	int air_defence;
	bool immune_from_bombing;
	int power_per_turn;
	int rebellion_bonus;
	int growth_rate;
	int production_bonus;
	bool production_bonus_all_cities;
	int defence_bonus; // as percentage bonus, e.g., 50 means +50% 
	bool defence_bonus_all_cities;
	bool auto_veteran_bonus;
	bool auto_veteran_bonus_all_cities;
	int research_multiplier_bonus; // as percentage, e.g., 50 means +50%
	bool research_multiplier_bonus_all_cities;
public:
	Improvement(/*ImprovementType improvementType,*/const char *name, int cost, int ai_weight);
	virtual ~Improvement();

	virtual Type getType() const {
		return TYPE_IMPROVEMENT;
	}
	/*ImprovementType getImprovementType() const {
		return improvementType;
	}*/
	int getAIWeight() const {
		return ai_weight;
	}
	AIHint getAIHint() const {
		return aiHint;
	}
	void setAIHint(AIHint aiHint) {
		this->aiHint = aiHint;
	}
	bool requiresCoastal() const {
		return requires_coastal;
	}
	void setRequiresCoastal(bool requires_coastal) {
		this->requires_coastal = requires_coastal;
	}
	int getTravelRange() const {
		return travel_range;
	}
	bool isTravelByAir() const {
		return travel_by_air;
	}
	void setTravelRange(int travel_range, bool travel_by_air) {
		this->travel_range = travel_range;
		this->travel_by_air = travel_by_air;
	}
	int getAirDefence() const {
		return this->air_defence;
	}
	void setAirDefence(int air_defence) {
		this->air_defence = air_defence;
	}
	bool isImmuneFromBombing() const {
		return this->immune_from_bombing;
	}
	void setImmuneFromBombing(bool immune_from_bombing) {
		this->immune_from_bombing = immune_from_bombing;
	}
	int getPowerPerTurn() const {
		return this->power_per_turn;
	}
	void setPowerPerTurn(int power_per_turn) {
		this->power_per_turn = power_per_turn;
	}
	int getRebellionBonus() const {
		return this->rebellion_bonus;
	}
	void setRebellionBonus(int rebellion_bonus) {
		this->rebellion_bonus = rebellion_bonus;
	}
	int getGrowthRate() const {
		return this->growth_rate;
	}
	void setGrowthRate(int growth_rate) {
		this->growth_rate = growth_rate;
	}

	int getProductionBonus() const {
		return this->production_bonus;
	}
	bool getProductionBonusAllCities() const {
		return this->production_bonus_all_cities;
	}
	void setProductionBonus(int production_bonus, bool production_bonus_all_cities) {
		this->production_bonus = production_bonus;
		this->production_bonus_all_cities = production_bonus_all_cities;
	}

	int getDefenceBonus() const {
		return this->defence_bonus;
	}
	bool getDefenceBonusAllCities() const {
		return this->defence_bonus_all_cities;
	}
	void setDefenceBonus(int defence_bonus, bool defence_bonus_all_cities) {
		this->defence_bonus = defence_bonus;
		this->defence_bonus_all_cities = defence_bonus_all_cities;
	}

	bool getAutoVeteranBonus() const {
		return this->auto_veteran_bonus;
	}
	bool getAutoVeteranBonusAllCities() const {
		return this->auto_veteran_bonus_all_cities;
	}
	void setAutoVeteranBonus(bool auto_veteran_bonus, bool auto_veteran_bonus_all_cities) {
		this->auto_veteran_bonus = auto_veteran_bonus;
		this->auto_veteran_bonus_all_cities = auto_veteran_bonus_all_cities;
	}

	int getResearchMultiplierBonus() const {
		return this->research_multiplier_bonus;
	}
	bool getResearchMultiplierBonusAllCities() const {
		return this->research_multiplier_bonus_all_cities;
	}
	void setResearchMultiplierBonus(int research_multiplier_bonus, bool research_multiplier_bonus_all_cities) {
		this->research_multiplier_bonus = research_multiplier_bonus;
		this->research_multiplier_bonus_all_cities = research_multiplier_bonus_all_cities;
	}

	virtual string getHelp(bool html=false) const;
};

class UnitTemplate : public Buildable {
public:
	enum NuclearType {
		NUCLEARTYPE_NONE = 0,
		NUCLEARTYPE_FISSION = 1,
		NUCLEARTYPE_FUSION = 2
	};
	enum SoundEffect {
		SOUNDEFFECT_SWORDS = 0,
		SOUNDEFFECT_BREAK = 1,
		SOUNDEFFECT_GUNSHOT = 2
	};
private:
	int attack;
	int defence;
	int bombard; // % probability of destroying something
	int bombard_power; // maximum cost of improvement that can be destroyed
	int moves;
	bool is_foot;
	bool is_air;
	bool is_missile;
	NuclearType nuclear_type;
	int air_attack;
	int air_defence;
	int air_range;
	int visibility_range;
	bool is_sea;
	int sea_attack;
	int sea_defence_range;
	VI_Texture *texture;
	bool can_build_city;
	bool can_build_roads;
	bool upgrade;
	SoundEffect sound_effect;
public:
	UnitTemplate(const char *name, int cost, int attack, int defence, int moves);
	virtual ~UnitTemplate();

	virtual Type getType() const {
		return TYPE_UNIT;
	}
	VI_Texture *getTexture() const {
		// n.b., return non-const needed due to draw functions being non-const
		return texture;
	}
	int getAttack() const {
		return attack;
	}
	int getDefence() const {
		return defence;
	}
	int getBombard() const {
		return bombard;
	}
	int getBombardPower() const {
		return bombard_power;
	}
	void setBombard(int bombard, int bombard_power) {
		this->bombard = bombard;
		this->bombard_power = bombard_power;
	}
	int getMoves() const {
		return moves;
	}
	bool isFoot() const {
		return is_foot;
	}
	void setFoot(bool is_foot) {
		this->is_foot = is_foot;
	}
	bool isAir() const {
		return is_air;
	}
	bool isMissile() const {
		return is_missile;
	}
	NuclearType getNuclearType() const {
		return nuclear_type;
	}
	bool isSea() const {
		return is_sea;
	}
	void setNuclearType(NuclearType nuclear_type) {
		this->nuclear_type = nuclear_type;
	}
	bool canBomb() const {
		return ( this->getBombard() > 0 || this->getNuclearType() != UnitTemplate::NUCLEARTYPE_NONE );
	}
	void setAir(int air_attack, int air_defence, int air_range, bool is_missile) {
		this->is_air = true;
		this->air_attack = air_attack;
		this->air_defence = air_defence;
		this->air_range = air_range;
		this->is_missile = is_missile;
	}
	int getAirAttack() const {
		return air_attack;
	}
	int getAirDefence() const {
		return air_defence;
	}
	int getAirRange() const {
		return air_range;
	}
	int getVisibilityRange() const {
		return visibility_range;
	}
	void setVisibilityRange(int visibility_range) {
		this->visibility_range = visibility_range;
	}
	void setSea(int sea_attack, int sea_defence_range) {
		this->is_sea = true;
		this->sea_attack = sea_attack;
		this->sea_defence_range = sea_defence_range;
	}
	int getSeaAttack() const {
		return sea_attack;
	}
	int getSeaDefenceRange() const {
		return sea_defence_range;
	}
	bool canBuildCity() const {
		return can_build_city;
	}
	void setCanBuildCity(bool can_build_city) {
		this->can_build_city = can_build_city;
	}
	bool canBuildRoads() const {
		return can_build_roads;
	}
	void setCanBuildRoads(bool can_build_roads) {
		this->can_build_roads = can_build_roads;
	}
	bool canUpgrade() const {
		return upgrade;
	}
	void setUpgrade(bool upgrade) {
		this->upgrade = upgrade;
	}
	void setSoundEffect(SoundEffect sound_effect) {
		this->sound_effect = sound_effect;
	}
	SoundEffect getSoundEffect() const {
		return this->sound_effect;
	}
	virtual string getHelp(bool html=false) const;
};
