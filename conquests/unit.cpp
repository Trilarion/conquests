#include "conquests_stdafx.h"

#include "unit.h"
#include "buildable.h"
#include "civilization.h"
#include "city.h"
#include "map.h"
#include "maingamestate.h"

#include "../Vision/VisionIface.h"

#include <ctime>

Unit::Unit(MainGamestate *mainGamestate, Civilization *civilization,const UnitTemplate *unit_template, Pos2D pos) :
mainGamestate(mainGamestate), civilization(civilization), unit_template(unit_template), moves_used(0, 1), status(STATUS_NORMAL), is_veteran(false), is_automated(false), has_goto_target(false), is_moving(false), moving_time_ms(-1), ai_move_count(0),
pos(-1, -1), // need to initialise with dummy values
//last_target(-1, -1), // need to initialise with dummy values
ai_has_dest(false),
n_bombed(0)
{
	civilization->addUnit(this);
	setPos(pos); // call this method, so that we also uncover the map, and update the mapsquare units list
}

Unit::Unit(MainGamestate *mainGamestate, Civilization *civilization) :
mainGamestate(mainGamestate), civilization(civilization), unit_template(NULL), moves_used(0, 1), status(STATUS_UNDEFINED), is_veteran(false), is_automated(false), has_goto_target(false), is_moving(false), moving_time_ms(-1), ai_move_count(0),
pos(-1, -1), // need to initialise with dummy values
//last_target(-1, -1), // need to initialise with dummy values
ai_has_dest(false),
n_bombed(0)
{
	// create dummy unit, used by Unit::load()
}

Unit::~Unit() {
	if( civilization != NULL ) {
		civilization->removeUnit(this);
	}
	ASSERT( mainGamestate->getMap() != NULL );
	MapSquare *square = mainGamestate->getMap()->getSquare(this->pos);
	square->removeUnit(this);
}

Unit *Unit::load(MainGamestate *mainGamestate, Civilization *civilization, FILE *file) {
	const int max_line_c = 4095;
	char line[max_line_c+1] = "";
	char word[max_line_c+1] = "";
	bool found_end = false;
	bool ok = true;
	const char field_xpos_c[] = "xpos=";
	const char field_ypos_c[] = "ypos=";
	const char field_unit_template_c[] = "unit_template=";
	const char field_moves_used_c[] = "moves_used=";
	const char field_status_c[] = "status=";
	const char field_is_veteran_c[] = "is_veteran=";
	const char field_is_automated_c[] = "is_automated=";
	const char field_has_goto_target_c[] = "has_goto_target=";
	const char field_goto_target_x_c[] = "goto_target_x=";
	const char field_goto_target_y_c[] = "goto_target_y=";

	Unit *unit = new Unit(mainGamestate, civilization);
	Pos2D newPos(-1, -1);

	while( ok && fgets(line, max_line_c, file) != NULL ) {
		//if( strcmp(line, "[/unit]\n") == 0 ) {
		if( matchFileLine(line, "[/unit]") ) {
			found_end = true;
			break;
		}
		if( line[0] == '[' ) {
			VI_log("Unit::load: Unexpected '[' start of new section!\n");
			ok = false;
			break;
		}
		if( matchFileLine(line, field_unit_template_c) ) {
			parseFileLine(word, line, field_unit_template_c);
			unit->unit_template = game_g->findUnitTemplate(word);
			if( unit->unit_template == NULL ) {
				VI_log("Unit::load: can't find unit_template: %s\n", word);
				ok = false;
			}
		}
		else if( matchFileLine(line, field_xpos_c) ) {
			parseFileLine(word, line, field_xpos_c);
			newPos.x = atoi(word);
		}
		else if( matchFileLine(line, field_ypos_c) ) {
			parseFileLine(word, line, field_ypos_c);
			newPos.y = atoi(word);
		}
		else if( matchFileLine(line, field_moves_used_c) ) {
			parseFileLine(word, line, field_moves_used_c);
			unit->moves_used = Rational(atoi(word), road_move_scale_c);
		}
		else if( matchFileLine(line, field_status_c) ) {
			parseFileLine(word, line, field_status_c);
			unit->status = static_cast<Unit::UnitStatus>(atoi(word));
		}
		else if( matchFileLine(line, field_is_veteran_c) ) {
			parseFileLine(word, line, field_is_veteran_c);
			unit->is_veteran = parseBool(&ok, word);
		}
		else if( matchFileLine(line, field_is_automated_c) ) {
			parseFileLine(word, line, field_is_automated_c);
			//unit->is_automated = static_cast<bool>(atoi(word));
			unit->is_automated = parseBool(&ok, word);
		}
		else if( matchFileLine(line, field_has_goto_target_c) ) {
			parseFileLine(word, line, field_has_goto_target_c);
			unit->has_goto_target = parseBool(&ok, word);
		}
		else if( matchFileLine(line, field_goto_target_x_c) ) {
			parseFileLine(word, line, field_goto_target_x_c);
			unit->goto_target.x = atoi(word);
		}
		else if( matchFileLine(line, field_goto_target_y_c) ) {
			parseFileLine(word, line, field_goto_target_y_c);
			unit->goto_target.y = atoi(word);
		}
	}
	if( !found_end ) {
		VI_log("Unit::load: didn't find end section\n");
		ok = false;
	}
	else if( newPos.x == -1 || newPos.y == -1 ) {
		VI_log("Unit::load: didn't find position\n");
		ok = false;
	}
	// TODO: check all fields have been set

	if( ok && unit->has_goto_target && !unit->is_automated ) {
		// check against corrupted save game files
		VI_log("Unit::load has_goto_target set, but is_automated not set!\n");
		//unit->has_goto_target = false;
		ok = false;
	}

	if( ok ) {
		civilization->addUnit(unit);
		unit->setPos(newPos, false); // no need to uncover map or make relationships - and would fail, since relationships not yet loaded!
	}
	else {
		delete unit;
		unit = NULL;
	}
	return unit;
}

void Unit::save(FILE *file) const {
	fprintf(file, "[unit]\n");
	fprintf(file, "unit_template=%s\n", unit_template->getName());
	fprintf(file, "xpos=%d\n", pos.x);
	fprintf(file, "ypos=%d\n", pos.y);
	Rational scaled_moves_used = moves_used * road_move_scale_c;
	int mu, mu_num, mu_den;
	scaled_moves_used.value(&mu, &mu_num, &mu_den);
	ASSERT( mu_num == 0 );
	//fprintf(file, "moves_used=%d\n", moves_used);
	fprintf(file, "moves_used=%d\n", mu);
	fprintf(file, "status=%d\n", status);
	fprintf(file, "is_veteran=%d\n", is_veteran ? 1 : 0);
	fprintf(file, "is_automated=%d\n", is_automated ? 1 : 0);
	fprintf(file, "has_goto_target=%d\n", has_goto_target ? 1 : 0);
	fprintf(file, "goto_target_x=%d\n", goto_target.x);
	fprintf(file, "goto_target_y=%d\n", goto_target.y);
	fprintf(file, "[/unit]\n");
}

void Unit::setPos(Pos2D newPos, bool handle_exploration) {
	ASSERT( mainGamestate->getMap() != NULL );
	if( this->pos.x != -1 ) {
		MapSquare *square = mainGamestate->getMap()->getSquare(this->pos);
		square->removeUnit(this);
	}
	this->old_pos = pos;
	this->pos = newPos;
	MapSquare *square = mainGamestate->getMap()->getSquare(this->pos);
	square->addUnit(this);

	if( handle_exploration ) {
		this->getCivilization()->uncover(this->getX(), this->getY(), this->unit_template->getVisibilityRange());
		this->getCivilization()->updateFogOfWar(this->getPos());
		this->getCivilization()->makeContact(this->getX(), this->getY(), this->unit_template->getVisibilityRange());

		/*for(int cy=pos.y-1;cy<=pos.y+1;cy++) {
			for(int cx=pos.x-1;cx<=pos.x+1;cx++) {
				if( cx == pos.x && cy == pos.y ) {
					continue;
				}
				if( cx >= 0 && cx < mainGamestate->getMap()->getWidth() && cy >= 0 && cy < mainGamestate->getMap()->getHeight() ) {
					const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(cx, cy);
					City *city = mainGamestate->getMap()->findCity(cx, cy);
					if( city != NULL || units->size() > 0 ) {
						const Civilization *o_civ = city != NULL ? city->getCivilization() : units->at(0)->getCivilization();
						if( civilization != o_civ ) {
							Relationship *relationship = mainGamestate->findRelationship(civilization, o_civ);
							if( !relationship->madeContact() ) {
								if( civilization == mainGamestate->getPlayer() || o_civ == mainGamestate->getPlayer() ) {
									const Civilization *cpu_civ = NULL;
									if( civilization != mainGamestate->getPlayer() )
										cpu_civ = civilization;
									else if( o_civ != mainGamestate->getPlayer() )
										cpu_civ = o_civ;
									ASSERT( cpu_civ != NULL );
									stringstream text;
									text << "We have made contact with " << cpu_civ->getName() << ".";
									InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture());
									window->doModal();
									delete window;
								}
								relationship->makeContact();
							}
						}
					}
				}
			}
		}*/
	}
}

void Unit::setMoving() {
	T_ASSERT( !is_moving );
	is_moving = true;
	moving_time_ms = VI_getGameTimeMS();

}

void Unit::getMovingPosition(float *mx, float *my) {
	ASSERT( is_moving );
	int time_ms = VI_getGameTimeMS() - moving_time_ms;
	//float speed = 300.0f; // larger means slower
	float speed = 0.3f; // larger means slower; units are (seconds per distance)
	/*int dx = abs(pos.x - old_pos.x);
	int dy = abs(pos.y - old_pos.y);*/
	int dx = pos.x - old_pos.x;
	int dy = pos.y - old_pos.y;
	mainGamestate->getMap()->reduceDiff(&dx, &dy);
	if( abs(dx) <= 1 && abs(dy) <= 1 && mainGamestate->getMap()->getSquare(pos)->getRoad() == ROAD_RAILWAYS ) {
		//speed = 0.1f;
		speed = 0.05f;
	}
	float diff = (time_ms / speed) / 1000.0f;
	if( diff >= 1.0 ) {
		*mx = (float)pos.x;
		*my = (float)pos.y;
		is_moving = false;
	}
	else {
		/**mx = (1.0-diff) * old_pos.x + diff * pos.x;
		*my = (1.0-diff) * old_pos.y + diff * pos.y;*/
		*mx = old_pos.x + diff * dx;
		*my = old_pos.y + diff * dy;
	}
}

void Unit::setStatus(UnitStatus status) {
	this->status = status;
	if( this->status == Unit::STATUS_BUILDING_ROAD || this->status == Unit::STATUS_BUILDING_RAILWAYS ) {
		ASSERT( this->unit_template->canBuildRoads() );
		// TODO: assert technology check for railways
		if( this->status == Unit::STATUS_BUILDING_ROAD ) {
			ASSERT( mainGamestate->getMap()->getSquare(this->pos)->getRoad() == ROAD_NONE );
		}
		else if( this->status == Unit::STATUS_BUILDING_RAILWAYS ) {
			ASSERT( mainGamestate->getMap()->getSquare(this->pos)->getRoad() == ROAD_BASIC );
		}
	}
}

/*void Unit::moveTo(Pos2D pos) {
ASSERT( hasMovesLeft() );
this->moves_used++;
this->pos = pos;
}*/

bool Unit::canMoveTo(Pos2D newPos, bool checkEnemies, bool checkTerritory) const {
	ASSERT( mainGamestate->getMap()->isValid(newPos.x, newPos.y) );
	const MapSquare *mapSquare = mainGamestate->getMap()->getSquare(newPos);
	if( this->unit_template->isAir() || this->unit_template->isSea() ) {
		const City *city = mapSquare->getCity();
		if( city != NULL && city->getCivilization() == this->civilization )
			return true;
		return false;
	}
	/*bool ok = false;
	if( mapSquare->isLand() ) {
	ok = true;
	}
	return ok;
	*/
	bool ok = mapSquare->canMoveTo(this->unit_template);
	/*if( ok && this->unit_template->getAttack() == 0 ) {
		// can't attack enemies or cities
		vector<Unit *> *units = game->getMap()->findUnitsAt(newPos.x, newPos.y);
		if( units->size() > 0 && units->at(0)->getCivilization() != this->civilization ) {
			ok = false;
		}
		else {
			const City *city = game->getMap()->findCity(newPos.x, newPos.y);
			if( city != NULL && city->getCivilization() != this->civilization ) {
				ok = false;
			}
		}
		delete units;
	}*/
	if( ok && checkEnemies ) {
		// check for attacking enemies or cities (TODO: improve performance?)
		const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(newPos.x, newPos.y);
		if( units->size() > 0 && units->at(0)->getCivilization() != this->civilization ) {
			if( this->getTemplate()->getAttack() == 0 )
				ok = false;
			else {
				const Relationship *relationship = mainGamestate->findRelationship(units->at(0)->getCivilization(), this->civilization);
				if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
					ok = false;
				}
			}
		}
		if( ok ) {
			const City *city = mainGamestate->getMap()->findCity(newPos.x, newPos.y);
			if( city != NULL && city->getCivilization() != this->civilization ) {
				if( this->getTemplate()->getAttack() == 0 )
					ok = false;
				else {
					const Relationship *relationship = mainGamestate->findRelationship(city->getCivilization(), this->civilization);
					if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
						ok = false;
					}
				}
			}
		}
		//delete units;
	}
	if( ok && checkTerritory ) {
		//mainGamestate->calculateTerritory();
		const Civilization *target_territory = mainGamestate->getMap()->getSquare(newPos)->getTerritory();
		if( target_territory != NULL && target_territory != this->civilization && mainGamestate->getMap()->getSquare(pos)->getTerritory() != target_territory ) {
			// entering enemy territory - not allowed if at peace
			const Relationship *relationship = mainGamestate->findRelationship(target_territory, this->civilization);
			if( relationship->getStatus() == Relationship::STATUS_PEACE && !relationship->hasRightOfPassage() ) {
				ok = false;
			}
		}
	}
	return ok;
}

bool Unit::hasMovesLeft() const {
	int moves = this->unit_template->getMoves();
	return this->moves_used < moves;
}

Rational Unit::movesLeft() const {
	Rational val = Rational(unit_template->getMoves(), 1) - moves_used;
	if( val < 0 ) {
		val = 0;
	}
	return val;
}

void Unit::useMoves() {
	this->moves_used = Rational(unit_template->getMoves(), 1);
}

void Unit::reset() {
	this->moves_used = Rational(0, 1);
}

const UnitTemplate *Unit::canUpgrade() const {
	// whether the Unit can upgrade, taking into account the city it's at, what the city can build, and available elements etc
	// note that this doesn't guarantee that the Unit will upgrade at the end of this turn - it may be that the elements are used up (e.g., by another unit upgrading)
	// returns the unit to upgrade to, or NULL if it can't upgrade
	const UnitTemplate *upgrade = NULL;
	if( this->unit_template->canUpgrade() && this->unit_template->getReplacedBy() != NULL ) {
		const MapSquare *square = mainGamestate->getMap()->getSquare(pos.x, pos.y);
		City *city = square->getCity();
		if( city != NULL ) {
			ASSERT( city->getCivilization() == this->civilization );
			ASSERT( this->unit_template->getReplacedBy()->getType() == Buildable::TYPE_UNIT );
			// need to search hierarchy, in case the immediate replacement has already been made obsolete!
			const UnitTemplate *replaced_by_template = static_cast<const UnitTemplate *>(this->unit_template->getReplacedBy());
			while( replaced_by_template != NULL ) {
				if( city->canBuild(replaced_by_template) ) {
					upgrade = replaced_by_template;
					break;
				}
				replaced_by_template = static_cast<const UnitTemplate *>(replaced_by_template->getReplacedBy());
			}
		}
	}
	return upgrade;
}

void Unit::update() {
	const UnitTemplate *upgrade = this->canUpgrade();
	if( upgrade != NULL ) {
		VI_log("%s %s at %d, %d upgrades to %s\n", this->civilization->getNameAdjective(), this->unit_template->getName(), this->pos.x, this->pos.y, upgrade->getName());
		this->unit_template = upgrade;
		const MapSquare *square = mainGamestate->getMap()->getSquare(pos.x, pos.y);
		City *city = square->getCity();
		ASSERT( city != NULL );
		city->useUpElements(upgrade);
	}
	if( this->status == STATUS_BUILDING_ROAD && this->hasMovesLeft() ) {
		MapSquare *square = mainGamestate->getMap()->getSquare(pos.x, pos.y);
		if( square->getRoad() != ROAD_NONE ) {
			// road complete - may happen if built by another unit in the same square
			this->status = STATUS_NORMAL;
		}
		else {
			this->useMoves();
			if( square->buildRoad(this) ) {
				// road is now built
				this->status = STATUS_NORMAL;
			}
		}
	}
	else if( this->status == STATUS_BUILDING_RAILWAYS && this->hasMovesLeft() ) {
		MapSquare *square = mainGamestate->getMap()->getSquare(pos.x, pos.y);
		ASSERT( square->getRoad() != ROAD_NONE );
		if( square->getRoad() != ROAD_BASIC ) {
			// road complete - may happen if built by another unit in the same square
			this->status = STATUS_NORMAL;
		}
		else {
			this->useMoves();
			if( square->buildRailways(this) ) {
				// road is now built
				this->status = STATUS_NORMAL;
			}
		}
	}
}

bool Unit::automate() {
	// returns true iff the AI moved
	// we should return false if the AI doesn't move - it means that nothing has changed, so we don't want to get stuck in an endless loop
	if( this->has_goto_target ) {
		bool result = false;
		if( this->moveTowardsAI(goto_target.x, goto_target.y, true, false) ) {
			// n.b., combat disallowed; also means that this unit shouldn't ever be deleted here
			result = true;
		}
		else {
			VI_log("goto failed to move from %d, %d towards %d, %d\n", pos.x, pos.y, goto_target.x, goto_target.y);
			result = false;
		}

		if( this->pos == this->goto_target || !result ) {
			this->unsetGotoTarget();
			if( this->hasMovesLeft() ) {
				mainGamestate->activateUnit(this, true); // so the player gains control of the unit
			}
		}
		/*if( !result ) {
			this->useMoves(); // so we don't repeatedly try to move this Unit (important to avoid infinite loop!)
		}*/

		return result;
	}

	if( this->ai_has_dest ) {
		ASSERT( this->ai_dest != this->pos );
		bool moved = this->moveTowardsAI(ai_dest.x, ai_dest.y);
		// n.b., this unit may now be deleted if moved!
		if( !moved ) {
			this->useMoves(); // we do this, to speed performance (so we don't repeatedly try to move this Unit)
		}
		return moved;
	}

	AIInterface::setMainGamestate(this->mainGamestate);
	AIInterface::setAICivilization(this->civilization);

	//VI_log("calculate dists...\n");
	const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(civilization, this, true, true, pos.x, pos.y);
	//Decision decision = DECISION_UNDECIDED;
	bool found_target = false;
	bool target_is_work = false;
	int dist = -1;
	int squares = -1;
	vector<Pos2D> candidates;

	//automateBuildRoads(&found_target, &decision, &dist, &candidates, dists);
	if( automateBuildRoads(&dist, &candidates, dists) ) {
		found_target = true;
		target_is_work = true;
	}

	//if( false )  {
	if( !found_target && mainGamestate->getMap()->findCity(pos) == NULL ) {
		// try retreating to nearest city
		// n.b., don't put this code in automateBuildRoads(), as that routine is also used by the AI, and we don't necessarily want to be retreating, just because a unit can't work the land
		for(size_t i=0;i<civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			Pos2D city_pos = city->getPos();
			ASSERT( this->civilization->isExplored(city_pos.x, city_pos.y) );
			ASSERT( mainGamestate->getMap()->isValidBase(city_pos.x, city_pos.y) );
			int this_dist = dists[city_pos.y * mainGamestate->getMap()->getWidth() + city_pos.x].cost;
			if( this_dist == -1 )
				continue;
			int this_squares = dists[city_pos.y * mainGamestate->getMap()->getWidth() + city_pos.x].squares;
			if( !found_target || this_dist < dist || ( this_dist == dist && this_squares <= squares ) ) {
				if( found_target && ( this_dist < dist || this_squares < squares ) ) {
					candidates.clear();
				}
				found_target = true;
				dist = this_dist;
				squares = this_squares;
				candidates.push_back( city_pos );
			}
		}
		/*if( found_target ) {
			VI_log("closest city to %d, %d is %d, %d: dist %d\n", pos.x, pos.y, candidates.at(0).x, candidates.at(0).y, dist);
		}*/
	}

	bool result = false;
	//if( decision == DECISION_WORK && candidates.at(0) == pos ) {
	if( found_target && target_is_work && candidates.at(0) == pos ) {
		// work land
		const MapSquare *square = mainGamestate->getMap()->getSquare(pos);
		if( square->getRoad() == ROAD_NONE ) {
			this->setStatus(Unit::STATUS_BUILDING_ROAD);
			this->update();
		}
		else if( square->getRoad() == ROAD_BASIC ) {
			this->setStatus(Unit::STATUS_BUILDING_RAILWAYS);
			this->update();
		}
		//result = false;
		result = true;
	}
	else if( found_target ) {
		//ASSERT( decision != DECISION_UNDECIDED );
		ASSERT( candidates.size() > 0 );
		Pos2D target;
		bool found = false;
		// prefer to stick with the choice calculated for the previous move/turn - this is particularly important with Railways, to avoid the AI moving in loops back and forth along the railways, as it keeps changing its mind!
		// less important, now that we do caching of the target square in Conquests? Also doesn't take into account the situation changing!
		/*for(int i=0;i<candidates.size() && !found;i++) {
			Pos2D candidate_target = candidates.at(i);
			ASSERT( candidate_target != pos );
			if( candidate_target == last_target ) {
				target = candidate_target;
				found = true;
			}
		}*/
		if( !found ) {
			// last choice no longer available, so pick a new one at random
			int r = rand() % candidates.size();
			target = candidates.at(r);
			//last_target = target;
		}
		this->setAIDest(target);
		if( this->moveTowardsAI(target.x, target.y) ) {
			// n.b., this unit may now be deleted!
			result = true;
		}
		else {
			VI_log("failed to move from %d, %d towards %d, %d\n", pos.x, pos.y, target.x, target.y);
			result = false;
		}
	}
	else {
		// can't do anything
		result = false;
	}

	if( !result ) {
		this->useMoves(); // so we don't repeatedly try to move this Unit (important to avoid infinite loop!)
	}

	AIInterface::setMainGamestate(NULL);
	AIInterface::setAICivilization(NULL);
	delete [] dists;
	return result;
}

//void Unit::automateBuildRoads(bool *found_target, Decision *decision, int *dist, vector<Pos2D> *candidates, const Distance *dists) {
bool Unit::automateBuildRoads(int *dist, vector<Pos2D> *candidates, const Distance *dists) const {
	bool found_target = false;
	if( this->unit_template->canBuildRoads() ) {
		//bool build_road = false;
		for(size_t i=0;i<this->civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			bool poss_work = false;
			bool poss_has_bonus = false;
			int poss_target_x = -1;
			int poss_target_y = -1;
			//bool poss_found_target = *found_target;
			bool poss_found_target = found_target;
			int poss_dist = *dist;
			int n_improved_squares = 0;
			/*for(int j=0;j<city->getNCitySquares();j++) {
				Pos2D city_pos = city->getCitySquare(j);
				if( !mainGamestate->getMap()->isValid(city_pos.x, city_pos.y) ) {
					continue;
				}*/
			vector<Pos2D> city_squares = AIInterface::getCitySquares(city->getPos().x, city->getPos().y, true);
			for(vector<Pos2D>::iterator iter = city_squares.begin(); iter != city_squares.end(); ++iter) {
				Pos2D city_sq_pos = *iter;
				//VI_log("### %d, %d\n", city_sq_pos.x, city_sq_pos.y);
				ASSERT( mainGamestate->getMap()->isValidBase(city_sq_pos.x, city_sq_pos.y) );
				if( !this->civilization->isExplored(city_sq_pos.x, city_sq_pos.y) )
					continue;
				int this_dist = dists[city_sq_pos.y * mainGamestate->getMap()->getWidth() + city_sq_pos.x].cost;
				if( this_dist == -1 )
					continue;
				bool is_improved = false;
				//if( city->wantWorkerAt(j, this) ) {
				//if( square->canBeImproved(this->getCivilization()) ) {
				if( AIInterface::canBeImproved(city_sq_pos.x, city_sq_pos.y) ) {
					// now check the units at the square, to avoid cases with enemies present, or where another unit that is or could do the job is present
					bool ok = true;
					//const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(city_sq_pos.x, city_sq_pos.y);
					const vector<Unit *> *units = AIInterface::getUnits(city_sq_pos.x, city_sq_pos.y);
					if( units != NULL ) {
						for(vector<Unit *>::const_iterator iter2 = units->begin(); iter2 != units->end() && ok; ++iter2) {
							const Unit *this_unit = *iter2;
							if( this_unit == this )
								; // okay
							else if( this_unit->civilization != this->civilization )
								ok = false;
							else if( this_unit->status == STATUS_BUILDING_ROAD || this_unit->status == STATUS_BUILDING_RAILWAYS || ( this_unit->status == STATUS_NORMAL && this_unit->unit_template->canBuildRoads() ) ) {
								ok = false;
								is_improved = true;
							}
						}
					}
					//delete units;
					if( !ok )
						continue;
					//const MapSquare *square = mainGamestate->getMap()->getSquare(city_sq_pos);
					//bool this_has_bonus = square->givesRoadBonus();
					bool this_has_bonus = AIInterface::givesRoadBonus(city_sq_pos.x, city_sq_pos.y);
					bool candidate = false;
					if( !poss_found_target || this_dist < poss_dist )
						candidate = true;
					else if( this_dist == poss_dist ) {
						if( this_has_bonus && !poss_has_bonus )
							candidate = true; // prefer squares that give the road bonus
					}
					if( candidate ) {
						poss_work = true;
						poss_has_bonus = this_has_bonus;
						poss_found_target = true;
						poss_dist = this_dist;
						poss_target_x = city_sq_pos.x;
						poss_target_y = city_sq_pos.y;
					}
				}
				if( !is_improved ) {
					//if( square->getRoad() != ROAD_NONE ) {
					//if( !square->canBeImproved(this->getCivilization()) ) {
					if( !AIInterface::canBeImproved(city_sq_pos.x, city_sq_pos.y) ) {
						is_improved = true;
					}
				}
				//if( is_improved && j != city->getCentreCitySquareIndex() ) {
				if( is_improved && city_sq_pos != city->getPos() ) {
					n_improved_squares++;
				}
			}
			if( city->getSize() <= 2 && n_improved_squares >= max_road_bonus_c && Pos2D(poss_target_x, poss_target_y) != city->getPos() ) {
				poss_work = false;
					// small cities need fewer improved squares - this allows us to explore or build cities (also see corresponding code for City AI)
					// however, we make an exception for the central city square itself, to aid travel
			}
			if( poss_work ) {
				//build_road = true;
				candidates->clear();
				candidates->push_back(Pos2D(poss_target_x, poss_target_y));
				//*decision = DECISION_WORK;
				ASSERT( poss_found_target );
				//*found_target = true;
				found_target = true;
				*dist = poss_dist;
			}

		}

		//if( !(*found_target) )
		{
			// try to look for building roads between cities
			for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
				for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
					if( !mainGamestate->getMap()->isValid(x-1, y) || !mainGamestate->getMap()->isValid(x+1, y) ||
						!mainGamestate->getMap()->isValid(x, y-1) || !mainGamestate->getMap()->isValid(x, y+1) )
					{
						continue;
					}
					if( !this->civilization->isExplored(x, y) )
						continue;
					ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
					int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
					if( this_dist == -1 )
						continue;
					//const MapSquare *centre_square = mainGamestate->getMap()->getSquare(x, y);
					//if( centre_square->getTerritory() != NULL )
					if( AIInterface::getTerritory(x, y) != NULL )
						continue;
					//if( centre_square->getRoad() != ROAD_NONE )
					//	continue;
					//if( !centre_square->canBeImproved(this->getCivilization()) )
					if( !AIInterface::canBeImproved(x, y) )
						continue;
					// this is a non-territory square, that can be improved
					// now check the units at the square, to avoid cases with enemies present, or where another unit that is or could do the job is present
					bool ok = true;
					//const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(x, y);
					const vector<Unit *> *units = AIInterface::getUnits(x, y);
					if( units != NULL ) {
						for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end() && ok; ++iter) {
							const Unit *this_unit = *iter;
							if( this_unit == this )
								; // okay
							else if( this_unit->civilization != this->civilization )
								ok = false;
							else if( this_unit->status == STATUS_BUILDING_ROAD || this_unit->status == STATUS_BUILDING_RAILWAYS || ( this_unit->status == STATUS_NORMAL && this_unit->unit_template->canBuildRoads() ) ) {
								ok = false;
							}
						}
					}
					if( !ok )
						continue;

					ok = false;
					for(int dy=-1;dy<=1 && !ok;dy++) {
						for(int dx=-1;dx<=1 && !ok;dx++) {
							if( dx == 0 && dy == 0 )
								continue;
							int tx = x + dx;
							int ty = y + dy;
							//const MapSquare *square = mainGamestate->getMap()->getSquare(tx, ty);
							//if( square->getTerritory() != this->civilization )
							//	continue;
							//if( square->getRoad() == ROAD_NONE )
							if( AIInterface::getRoad(tx, ty) == ROAD_NONE )
								continue;
							// this square has a road (or railways)
							int sx, sy, ex, ey;
							if( dx != 0 && abs(dx) == abs(dy) ) {
								// diagonal - look at opposite diagonal
								sx = ex = -dx;
								sy = ey = -dy;
							}
							else {
								// non-diagonal - look at opposite set of 3
								if( dx == 0 ) {
									sx = -1;
									ex = 1;
									sy = ey = -dy;
								}
								else if( dy == 0 ) {
									sx = ex = -dx;
									sy = -1;
									ey = 1;
								}
								else {
									ASSERT( false );
								}
							}
							for(int dy2=sy;dy2<=ey && !ok;dy2++) {
								for(int dx2=sx;dx2<=ex && !ok;dx2++) {
									int tx2 = x + dx2;
									int ty2 = y + dy2;
									//const MapSquare *square = mainGamestate->getMap()->getSquare(tx, ty);
									//const MapSquare *square2 = mainGamestate->getMap()->getSquare(tx2, ty2);
									Road road2 = AIInterface::getRoad(tx2, ty2);
									const Civilization *territory = AIInterface::getTerritory(tx, ty);
									const Civilization *territory2 = AIInterface::getTerritory(tx2, ty2);
									//if( square2->getRoad() != ROAD_NONE && ( square->getTerritory() == this->civilization || square2->getTerritory() == this->civilization ) ) {
									//if( road2 != ROAD_NONE && ( square->getTerritory() == this->civilization || square2->getTerritory() == this->civilization ) ) {
									if( road2 != ROAD_NONE && ( territory == this->civilization || territory2 == this->civilization ) ) {
										// at least one is owned by this civ, and both squares have a road (or railways)
										// we say at least one, rather than both, so the ai will build roads towards enemy cities - perhaps useful for moving units quickly to attack
										ok = true;
									}
								}
							}
						}
					}
					if( ok ) {
						//if( !(*found_target) || this_dist <= *dist ) {
						if( !found_target || this_dist <= *dist ) {
							//if( *decision != DECISION_WORK || this_dist < *dist ) {
							if( this_dist < *dist ) {
								candidates->clear();
							}
							candidates->push_back(Pos2D(x, y));
							//*decision = DECISION_WORK;
							//*found_target = true;
							found_target = true;
							*dist = this_dist;
						}
					}
				}
			}
			/*if( found_target ) {
			VI_log("build connecting road at:\n");
			for(int i=0;i<candidates.size();i++) {
			VI_log("    %d, %d\n", candidates.at(i).x, candidates.at(i).y);
			}
			}*/
		}
	}
	return found_target;
}

#ifndef USE_LUA_UNIT_AI
//bool Unit::doAIAir() {
AIInterface::UnitAction Unit::doAIAir(Pos2D *target_square) {
	ASSERT( this->getTemplate()->isAir() );
	// initial checks
	// n.b., AIInterface::canBomb will do these checks too, but we do them here for performance
	if( !this->getTemplate()->canBomb() ) {
		// fighter rather than bomber
		//return false;
		return AIInterface::UNITACTION_NOTHING;
	}
	//const City *city = mainGamestate->getMap()->findCity(this->pos);
	const City *source_city = AIInterface::getCity(this->pos.x, this->pos.y);
	ASSERT( source_city != NULL );
	if( !source_city->canLaunch(this->unit_template) ) {
		// city no longer has launch capability (airport, missile launcher)
		//return false;
		return AIInterface::UNITACTION_NOTHING;
	}
	// AI:
	// TODO: rebasing
	vector<const City *> candidates;
	int best_score = 0;
	vector<Civilization *> civilizations = AIInterface::getCivilizations(AIInterface::GETCIVILIZATIONS_WAR);
	/*for(int i=0;i<this->mainGamestate->getNCivilizations();i++) {
		const Civilization *civ = this->mainGamestate->getCivilization(i);
		if( civ == civilization )
			continue;
		if( civ->isDead() )
			continue;
		const Relationship *relationship = mainGamestate->findRelationship(this->civilization, civ);
		if( relationship->getStatus() == Relationship::STATUS_PEACE )
			continue;*/
	for(vector<Civilization *>::const_iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
		Civilization *civ = *iter;
		// bomb cities?
		/*for(int j=0;j<civ->getNCities();j++) {
			const City *city = civ->getCity(j);
			Pos2D city_pos = city->getPos();
			if( !this->civilization->isExplored(city_pos.x, city_pos.y) )
				continue;*/
		vector<City *> cities = AIInterface::getCities(civ);
		for(vector<City *>::const_iterator iter2 = cities.begin(); iter2 != cities.end(); ++iter2) {
			const City *city = *iter2;
			Pos2D city_pos = city->getPos();
			if( unit_template->isMissile() ) {
				int min_city_size = unit_template->getNuclearType() == UnitTemplate::NUCLEARTYPE_NONE ? 6 : 10;
				if( city->getSize() < min_city_size ) {
					// don't waste missiles on smaller cities
					continue;
				}
			}
			//int dist = city_pos.distanceChebyshev(&this->pos);
			/*int dist = mainGamestate->getMap()->distanceChebyshev(city_pos, this->pos);
			if( dist <= this->getTemplate()->getAirRange() ) {*/
			if( AIInterface::canBomb(this->getTemplate(), source_city, city_pos, true) ) {
				// prefer largest cities (with some randomness)
				int score = city->getSize() + ( rand() % 2 );
				if( candidates.size() == 0 || score > best_score ) {
					candidates.clear();
					candidates.push_back(city);
					best_score = score;
				}
				else if( score == best_score ) {
					candidates.push_back(city);
				}
			}
		}
	}
	if( candidates.size() == 0 ) {
		// do nothing
		//return false;
		return AIInterface::UNITACTION_NOTHING;
	}
	int r = rand() % candidates.size();
	const City *city = candidates.at(r);
	VI_log("AI: %s %s bombs %s city of %s\n", this->civilization->getNameAdjective(), this->getTemplate()->getName(), city->getCivilization()->getNameAdjective(), city->getName());
	Pos2D city_pos = city->getPos();
	//const MapSquare *square = this->mainGamestate->getMap()->getSquare(city_pos);
	//this->mainGamestate->bomb(this, square);
	//this->mainGamestate->bomb(this, city_pos);
	//return true;
	*target_square = city_pos;
	return AIInterface::UNITACTION_MOVE;
}

//bool Unit::doAILand() {
AIInterface::UnitAction Unit::doAILand(Pos2D *target_square) {
	ASSERT( !this->getTemplate()->isAir() );
	if( !this->unit_template->canBuildCity() ) {
		//const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(pos.x, pos.y);
		const vector<Unit *> *units = AIInterface::getUnits(pos.x, pos.y);
		bool settler_present = false;
		/*for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end() && !settler_present;++iter) {
			const Unit *unit = *iter;
			// checking whether a City can be built here too is important - otherwise Units will end up waiting endlessly for a Settler that has nowhere to go!
			if( unit->unit_template->canBuildCity() && mainGamestate->getMap()->getSquare(unit->getPos())->canBuildCity() ) {
				settler_present = true;
			}
		}*/
		// checking whether a City can be built here too is important - otherwise Units will end up waiting endlessly for a Settler that has nowhere to go!
		//if( mainGamestate->getMap()->getSquare(pos)->canBuildCity() ) {
		if( units != NULL && AIInterface::canBuildCity(pos.x, pos.y) ) {
			for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end() && !settler_present;++iter) {
				const Unit *unit = *iter;
				if( unit->unit_template->canBuildCity() ) {
					settler_present = true;
					break;
				}
			}
		}
		if( settler_present ) {
			//return false; // wait for Settler to move first
			return AIInterface::UNITACTION_WAIT; // wait for Settler to move first
		}
	}

	Decision decision = DECISION_UNDECIDED;
	bool found_target = false;
	int dist = -1;
	vector<Pos2D> candidates;
	bool work_only = false;

	// defend city (check this first, to avoid creating distance map)
	//City *city = game->findCityAt(this->civ, this->xpos, this->ypos);
	//if( city != NULL && !found_target && !work_only ) {
	if( !found_target && unit_template->getDefence() > 0 ) {
		//City *city = mainGamestate->getMap()->findCity(pos.x, pos.y);
		City *city = AIInterface::getCity(pos.x, pos.y);
		if( city != NULL ) {
			// do we need to defend city?
			if( city->needDefendersAI(NULL, this, 0) ) {
				AIInterface::UnitAction unit_action = AIInterface::UNITACTION_NOTHING;
				// but might as well build roads if needed
				//const MapSquare *square = mainGamestate->getMap()->getSquare(pos);
				//if( this->unit_template->canBuildRoads() && square->canBeImproved(this->getCivilization()) ) {
				if( this->unit_template->canBuildRoads() && AIInterface::canBeImproved(pos.x, pos.y) ) {
					Road road = AIInterface::getRoad(pos.x, pos.y);
					//if( square->getRoad() == ROAD_NONE ) {
					if( road == ROAD_NONE ) {
						/*this->setStatus(Unit::STATUS_BUILDING_ROAD);
						this->update();*/
						unit_action = AIInterface::UNITACTION_WORK;
					}
					//else if( square->getRoad() == ROAD_BASIC ) {
					else if( road == ROAD_BASIC ) {
						/*this->setStatus(Unit::STATUS_BUILDING_RAILWAYS);
						this->update();*/
						unit_action = AIInterface::UNITACTION_WORK;
					}
					else {
						ASSERT(false);
					}
				}
				/*else {
					this->useMoves();
				}*/
				//delete [] dists;
				//VI_log("### Unit %d AI: %d\n", this, clock() - time_s);
				//return true;
				return unit_action;
			}
		}
	}

	const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(civilization, this, true, true, pos.x, pos.y);

	// settlers should either build city, or explore
	//if( !found_target && this->type == SETTLERS && this->aistate == BUILD_CITY ) {
	if( this->getTemplate()->canBuildCity() ) {
		//ASSERT( !work_only );
		// search for city space, or explore
		//if( mainGamestate->getMap()->getSquare(pos)->canBuildCity() ) {
		if( AIInterface::canBuildCity(pos.x, pos.y) ) {
			// can build city here, but do a quick check for better adjacent squares
			bool coastal = false;
			bool goodterrain = false;
			bool forest = false;
			Pos2D best_pos;
			bool first = true;
			bool has_enemies = false;
			vector<Pos2D> adjacent_squares = AIInterface::getAdjacentSquares(pos.x, pos.y, true);
			/*for(int iy=pos.y-1;iy<=pos.y+1;iy++) {
				for(int ix=pos.x-1;ix<=pos.x+1;ix++) {
					if( !mainGamestate->getMap()->isValid(ix, iy) )
						continue;
					int x = ix, y = iy;
					mainGamestate->getMap()->reduceToBase(&x, &y);*/
			for(vector<Pos2D>::const_iterator iter = adjacent_squares.begin(); iter != adjacent_squares.end(); ++iter) {
					Pos2D adjacent_square = *iter;
					int x = adjacent_square.x;
					int y = adjacent_square.y;
					//const MapSquare *square = mainGamestate->getMap()->getSquare(x, y);
					//if( !has_enemies && square->hasEnemies(this->civilization) ) {
					if( !has_enemies && AIInterface::hasEnemies(x, y) ) {
						has_enemies = true; // this check must be done before the dists check, as we can't get to a square with an enemy on it (since it's a Settler unit)
					}
					ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
					int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
					if( this_dist == -1 )
						continue;
					//if( !square->canBuildCity() )
					if( !AIInterface::canBuildCity(x, y) )
						continue;
					//bool is_coastal = mainGamestate->getMap()->isOnOrAdjacent(Pos2D(x, y), TYPE_OCEAN);
					bool is_coastal = false;
					{
						vector<Pos2D> adjacent_squares2 = AIInterface::getAdjacentSquares(x, y, true);
						for(vector<Pos2D>::const_iterator iter2 = adjacent_squares2.begin(); iter2 != adjacent_squares2.end() && !is_coastal; ++iter2) {
							Pos2D adjacent_square2 = *iter2;
							Type terrain2 = AIInterface::getTerrain(adjacent_square2.x, adjacent_square2.y);
							//VI_log("### %d, %d : %d\n", adjacent_square2.x, adjacent_square2.y, terrain2);
							if( terrain2 == TYPE_OCEAN )
								is_coastal = true;
						}
					}
					//bool is_goodterrain = square->getType() == TYPE_GRASSLAND || square->getType() == TYPE_HILLS;
					Type terrain = AIInterface::getTerrain(x, y);
					bool is_goodterrain = terrain == TYPE_GRASSLAND || terrain == TYPE_HILLS;
					//bool is_forest = mainGamestate->getMap()->isWithinCityRadius(Pos2D(x, y), TYPE_FOREST, false);
					bool is_forest = false;
					{
						vector<Pos2D> squares = AIInterface::getCitySquares(x, y, false);
						for(vector<Pos2D>::const_iterator iter2 = squares.begin(); iter2 != squares.end() && !is_forest; ++iter2) {
							Pos2D square = *iter2;
							Type terrain2 = AIInterface::getTerrain(square.x, square.y);
							if( terrain2 == TYPE_FOREST )
								is_forest = true;
						}
					}
					if( first ) {
						first = false;
						coastal = is_coastal;
						goodterrain = is_goodterrain;
						forest = is_forest;
						best_pos = Pos2D(x, y);
					}
					else {
						bool prefer = false;
						if( is_goodterrain && !goodterrain ) {
							// always prefer good terrain
							prefer = true;
						}
						else if( is_goodterrain == goodterrain ) {
							// for equal terrain, prefer forest
							if( is_forest && !forest ) {
								prefer = true;
							}
							else if( is_forest == forest ) {
								if( is_coastal && !coastal ) {
									// for equal forest, prefer coastal
									prefer = true;
								}
								else if( is_coastal == coastal ) {
									// for equal coastal, prefer current square
									if( x == pos.x && y == pos.y ) {
										prefer = true;
									}
								}
							}
						}
						if( prefer ) {
							coastal = is_coastal;
							goodterrain = is_goodterrain;
							forest = is_forest;
							best_pos = Pos2D(x, y);
						}
					}
				//}
			}

			if( has_enemies ) {
				// enemy adjacent, don't build city here!
			}
			else if( best_pos == pos ) {
				// build city here
				/*this->civilization->removeUnit(this, false);
					// remove it before creating city, to avoid AI confusion
					// but don't check dead, as if this is the only settler, with no existing cities, we'll think the Civilization has been wiped out!
				string city_name = this->civilization->makeCityName();
				this->civilization->advanceCityName();
				new City(mainGamestate, this->civilization, city_name.c_str(), pos.x, pos.y);
				this->civilization = NULL; // to stop the unit deconstructor trying to remove it again
				//VI_log("### Unit %d AI: %d\n", this, clock() - time_s);
				delete this;
				*/
				delete [] dists;
				//return true;
				return AIInterface::UNITACTION_BUILD_CITY;
			}
			else {
				candidates.clear();
				candidates.push_back(best_pos);
				found_target = true;
				decision = DECISION_BUILDCITY;
				ASSERT( mainGamestate->getMap()->isValidBase(best_pos.x, best_pos.y) );
				dist = dists[best_pos.y * mainGamestate->getMap()->getWidth() + best_pos.x].cost;
			}
		}
		if( !found_target ) {
			for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
				for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
					if( x == pos.x && y == pos.y )
						continue; // we've already tested whether we should build a city at the current square
					ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
					int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
					if( this_dist == -1 )
						continue;
					//if( !this->civ->isExplored(x, y) )
					//        continue;
					//if( !game->validCitySquare(x, y) )
					//        continue;
					//if( !this->civilization->isExplored(x, y) || !game->getMap()->canBuildCity(x, y) )
					//if( !this->civilization->isExplored(x, y) || !game->getMap()->getSquare(x, y)->canBuildCity() )
					//if( this->civilization->isExplored(x, y) && !mainGamestate->getMap()->getSquare(x, y)->canBuildCity() ) // if not explored, we shouldn't be cheating to see if we can build there!
					bool adjacent_unexplored = AIInterface::hasAdjacentUnexplored(x, y); // whether there's an adjacent unexplored square - i.e., moving to this square will uncover it
					//if( this->civilization->isExplored(x, y) && !AIInterface::canBuildCity(x, y) ) // if not explored, consider exploring anyway, so carry on
					if( !adjacent_unexplored && !AIInterface::canBuildCity(x, y) ) // if not explored, consider exploring anyway, so carry on
						continue;
					//if( mainGamestate->getYear() > 100 && !this->civilization->isExplored(x, y) ) {
					if( mainGamestate->getYear() > 100 && !adjacent_unexplored ) {
						// after early period, we leave exploring to the next phase below (hence, they may also do work if there is a closer square)
						continue;
					}
					//VI_log(">>> City Location: %d, %d : dist %d\n", x, y, this_dist);
					if( !found_target || this_dist <= dist ) {
						// only consider if a settler isn't already here!
						//const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(x, y);
						const vector<Unit *> *units = AIInterface::getUnits(x, y);
						bool settler_present = false;
						if( units != NULL ) {
							for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end() && !settler_present;++iter) {
								const Unit *unit = *iter;
								if( unit->unit_template->canBuildCity() ) {
									settler_present = true;
								}
							}
						}

						if( !settler_present ) {
							if( decision != DECISION_BUILDCITY || this_dist < dist ) {
								candidates.clear();
							}
							candidates.push_back(Pos2D(x, y));
							found_target = true;
							decision = DECISION_BUILDCITY;
							dist = this_dist;
						}

					}
				}
			}
		}
		//if( !found_target )
		//        this->aistate = WORK_LAND;
	}

	// build roads, attack or explore - whichever is closer
	if( !found_target ) {
		/*if( this->type == SETTLERS ) {
			if( work_only )
				this->aistate = WORK_LAND;
			ASSERT( this->aistate == BUILD_CITY || this->aistate == WORK_LAND );
		}*/
		//if( !found_target && this->type == SETTLERS && this->aistate == WORK_LAND ) {
		/*if( this->unit_template->canBuildCity() && game->getYear() <= 100 ) {
			// in early stages of game, settlers should focus on building cities!
		}
		else*/
		{
			//automateBuildRoads(&found_target, &decision, &dist, &candidates, dists);
			if( automateBuildRoads(&dist, &candidates, dists) ) {
				found_target = true;
				decision = DECISION_WORK;
			}
			if( decision == DECISION_WORK && candidates.at(0) == pos ) {
				// work land
				//const MapSquare *square = mainGamestate->getMap()->getSquare(pos);
				Road road = AIInterface::getRoad(pos.x, pos.y);
				//if( square->getRoad() == ROAD_NONE ) {
				if( road == ROAD_NONE ) {
					/*this->setStatus(Unit::STATUS_BUILDING_ROAD);
					this->update();*/
				}
				//else if( square->getRoad() == ROAD_BASIC ) {
				else if( road == ROAD_BASIC ) {
					/*this->setStatus(Unit::STATUS_BUILDING_RAILWAYS);
					this->update();*/
				}
				else {
					ASSERT(false);
				}
				delete [] dists;
				/*VI_log("AI: %s unit %s [%d] at (%d, %d) works land\n",
					this->civilization->getName(), this->unit_template->getName(), this, this->pos.x, this->pos.y);*/
				//VI_log("### Unit %d AI: %d\n", this, clock() - time_s);
				//return false;
				//return true;
				return AIInterface::UNITACTION_WORK;
			}
		}

		// attack
		//if( !found_target && !work_only && this->getBaseAttack() > 0 ) {
		if( this->getTemplate()->getAttack() > 0 ) {
			/*for(int i=0;i<mainGamestate->getNCivilizations();i++) {
				const Civilization *civ = mainGamestate->getCivilization(i);
				if( civ == this->civilization )
					continue;
				if( civ->isDead() )
					continue;*/
			vector<Civilization *> civilizations = AIInterface::getCivilizations(AIInterface::GETCIVILIZATIONS_ALL);
			for(vector<Civilization *>::iterator iter = civilizations.begin(); iter != civilizations.end(); ++iter) {
				Civilization *civ = *iter;
				const Relationship *relationship = mainGamestate->findRelationship(this->civilization, civ);
				// make contact?
				bool made_contact = relationship->madeContact();
				bool at_peace = relationship->getStatus() == Relationship::STATUS_PEACE;
				/*if( made_contact && at_peace )
					continue;*/
				if( !at_peace ) {
					// attack cities?
					/*for(int j=0;j<civ->getNCities();j++) {
						const City *city = civ->getCity(j);
						int c_xpos = city->getX();
						int c_ypos = city->getY();
						if( !this->civilization->isExplored(c_xpos, c_ypos) )
							continue;*/
					vector<City *> cities = AIInterface::getCities(civ);
					for(vector<City *>::const_iterator iter2 = cities.begin(); iter2 != cities.end(); ++iter2) {
						const City *city = *iter2;
						int c_xpos = city->getX();
						int c_ypos = city->getY();
						ASSERT( this->civilization->isExplored(c_xpos, c_ypos) );
						ASSERT( mainGamestate->getMap()->isValidBase(c_xpos, c_ypos) );
						int this_dist = dists[c_ypos * mainGamestate->getMap()->getWidth() + c_xpos].cost;
						if( this_dist == -1 )
							continue;
						if( !found_target || this_dist <= dist ) {
							if( decision != DECISION_ATTACKCITY || this_dist < dist ) {
								candidates.clear();
							}
							candidates.push_back(Pos2D(c_xpos, c_ypos));
							decision = DECISION_ATTACKCITY;
							found_target = true;
							dist = this_dist;
						}
					}
					// attack units?
					bool attack_adj = false;
					bool stronger_adj_attacker = true;
					/*for(int j=0;j<civ->getNUnits();j++) {
						const Unit *unit = civ->getUnit(j);
						int u_xpos = unit->getX();
						int u_ypos = unit->getY();
						if( !this->civilization->isExplored(u_xpos, u_ypos) )
							continue;*/
					vector<Unit *> units = AIInterface::getUnits(civ);
					for(vector<Unit *>::const_iterator iter2 = units.begin(); iter2 != units.end(); ++iter2) {
						const Unit *unit = *iter2;
						int u_xpos = unit->getX();
						int u_ypos = unit->getY();
						ASSERT( this->civilization->isExplored(u_xpos, u_ypos) );
						ASSERT( mainGamestate->getMap()->isValidBase(u_xpos, u_ypos) );
						int this_dist = dists[u_ypos * mainGamestate->getMap()->getWidth() + u_xpos].cost;
						if( this_dist == -1 )
							continue;
						if( this_dist <= road_move_scale_c ) {
							// check whether better to attack, or be attacked
							// note, this test should not depend on unit's current location, otherwise we risk an infinite move loop!
							// for the best attacker units, we try attacking anyway, to avoid stalemates
							/*
							bool at_city = mainGamestate->getMap()->findCity(pos.x, pos.y) != NULL;
							if( at_city || rand() % 2 == 0 ) {*/
							bool is_better_unit = false;
							for(int k=0;k<this->civilization->getNUnits() && !is_better_unit;k++) {
								const Unit *unit = this->civilization->getUnit(k);
								if( unit->unit_template->getAttack() > this->unit_template->getAttack() ) {
									is_better_unit = true;
								}
							}
							if( is_better_unit ) {
								//const MapSquare *square = mainGamestate->getMap()->getSquare(u_xpos, u_ypos);
								//const Unit *best_defender = square->getBestDefender();
								const Unit *best_defender = AIInterface::getBestDefender(u_xpos, u_ypos);
								ASSERT( best_defender != NULL );
								if( best_defender->getTemplate()->getAttack() > 0 ) {
									ASSERT( best_defender->getTemplate()->getDefence() > 0 );
									int ratio_attack = (100 * this->unit_template->getAttack()) / ( best_defender->getTemplate()->getDefence() );
									int ratio_defend = (100 * this->unit_template->getDefence()) / ( best_defender->getTemplate()->getAttack() );
									if( ratio_attack < ratio_defend ) {
										continue;
									}
								}
							}
						}
						// Prefer attacking cities to attacking units if equal distances. Even if we want to change this behaviour and allow a random choice, we still want to prefer undefended cities.
						//if( !found_target || this_dist < dist || ( this_dist == dist && decision == DECISION_ATTACKUNITS ) ) {
						if( !found_target || this_dist < dist || ( this_dist == dist && decision != DECISION_ATTACKCITY ) ) {
							if( decision != DECISION_ATTACKUNITS || this_dist < dist ) {
								candidates.clear();
							}
							candidates.push_back(Pos2D(u_xpos, u_ypos));
							decision = DECISION_ATTACKUNITS;
							found_target = true;
							dist = this_dist;
						}
					}
				}
				if( !made_contact ) {
					// contact units?
					/*for(int j=0;j<civ->getNUnits();j++) {
						const Unit *unit = civ->getUnit(j);
						int u_xpos = unit->getX();
						int u_ypos = unit->getY();
						if( !this->civilization->isExplored(u_xpos, u_ypos) )
							continue;*/
					vector<Unit *> units = AIInterface::getUnits(civ);
					for(vector<Unit *>::const_iterator iter2 = units.begin(); iter2 != units.end(); ++iter2) {
						const Unit *unit = *iter2;
						int u_xpos = unit->getX();
						int u_ypos = unit->getY();
						ASSERT( this->civilization->isExplored(u_xpos, u_ypos) );
						// need to look at adjacent squares, as the distance map will say we can't move to a square with a non-enemy unit!
						vector<Pos2D> adjacent_squares = AIInterface::getAdjacentSquares(u_xpos, u_ypos, false);
						/*for(int iy=u_ypos-1;iy<=u_ypos+1;iy++) {
							for(int ix=u_xpos-1;ix<=u_xpos+1;ix++) {
								if( !mainGamestate->getMap()->isValid(ix, iy) )
									continue;
								if( Pos2D(u_xpos, u_ypos) == Pos2D(ix, iy) )
									continue;
								int x = ix, y = iy;
								mainGamestate->getMap()->reduceToBase(&x, &y);*/
						for(vector<Pos2D>::iterator iter3 = adjacent_squares.begin(); iter3 != adjacent_squares.end(); ++iter3) {
								Pos2D adjacent_square = *iter3;
								int x = adjacent_square.x;
								int y = adjacent_square.y;
								ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
								int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
								if( this_dist == -1 )
									continue;
								if( !found_target || this_dist <= dist ) {
									if( decision != DECISION_CONTACTUNITS || this_dist < dist ) {
										candidates.clear();
									}
									candidates.push_back(Pos2D(x, y));
									decision = DECISION_CONTACTUNITS;
									found_target = true;
									dist = this_dist;
								}
							//}
						}
					}
				}
			}
		}

		// explore
		//if( !found_target && !work_only ) {
		{
			// explore
			for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
				for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
					if( this->civilization->isExplored(x, y) )
						continue;
					ASSERT( mainGamestate->getMap()->isValidBase(x, y) );
					/*int this_dist = dists[y * mainGamestate->getMap()->getWidth() + x].cost;
					if( this_dist == -1 )
						continue;
					if( !found_target || this_dist <= dist || ( decision == DECISION_WORK && this->unit_template->canBuildCity() ) ) {
						// if we can build cities, then exploring should take priority over working
						if( decision != DECISION_EXPLORE || this_dist < dist ) {
							candidates.clear();
						}
						candidates.push_back(Pos2D(x, y));
						found_target = true;
						decision = DECISION_EXPLORE;
						dist = this_dist;
					}*/
					// we need to move to an adjacent explored square, as the dists array will be -1
					vector<Pos2D> adjacent_squares = AIInterface::getAdjacentSquares(x, y, false);
					for(vector<Pos2D>::iterator iter = adjacent_squares.begin(); iter != adjacent_squares.end(); ++iter) {
						Pos2D adjacent_square = *iter;
						ASSERT( mainGamestate->getMap()->isValidBase(adjacent_square.x, adjacent_square.y) );
						int this_dist = dists[adjacent_square.y * mainGamestate->getMap()->getWidth() + adjacent_square.x].cost;
						if( this_dist == -1 )
							continue;
						if( !found_target || this_dist <= dist || ( decision == DECISION_WORK && this->unit_template->canBuildCity() ) ) {
							// if we can build cities, then exploring should take priority over working
							if( decision != DECISION_EXPLORE || this_dist < dist ) {
								candidates.clear();
							}
							candidates.push_back(adjacent_square);
							found_target = true;
							decision = DECISION_EXPLORE;
							dist = this_dist;
						}
					}
				}
			}
		}
	}

	// Retreat to a city - prefer those with travel ability.
	// This should be done as lower priority to previous actions, even if the city is closer.
	// Note that we try to spread units evenly.
	if( !found_target ) {
		//int travel_range = 0;
		int weight = 0;
		for(int i=0;i<civilization->getNCities();i++) {
			const City *city = civilization->getCity(i);
			Pos2D city_pos = city->getPos();
			ASSERT( this->civilization->isExplored(city_pos.x, city_pos.y) );
			ASSERT( mainGamestate->getMap()->isValidBase(city_pos.x, city_pos.y) );
			int this_dist = dists[city_pos.y * mainGamestate->getMap()->getWidth() + city_pos.x].cost;
			if( this_dist == -1 )
				continue;
			/*int this_travel_range = city->getTravelRange();
			if( !found_target || this_travel_range > travel_range || ( this_travel_range == travel_range && this_dist < dist ) ) {
				found_target = true;
				decision = DECISION_CITY;
				dist = this_dist;
				target_x = city_pos.x;
				target_y = city_pos.y;
				travel_range = this_travel_range;
			}*/
			//const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(city_pos.x, city_pos.y);
			const vector<Unit *> *units = AIInterface::getUnits(city_pos.x, city_pos.y);
			int n_units = 0;
			if( units != NULL ) {
				for(vector<Unit *>::const_iterator iter = units->begin(); iter != units->end(); ++iter) {
					const Unit *unit = *iter;
					if( unit != this && unit->getTemplate() == this->getTemplate() ) {
						// mustn't include this unit in the count, otherwise we get unstable behaviour - a unit will decide to leave the city, but then have to return due to the count then reducing!
						n_units++;
					}
				}
			}
			int this_weight = ( 100 * ( city->getTravelRange(false) + city->getTravelRange(true) + 1 ) ) / ( n_units + 1 );
			if( !found_target || this_weight > weight || ( this_weight == weight && this_dist < dist ) ) {
				candidates.clear();
				candidates.push_back(city_pos);
				found_target = true;
				decision = DECISION_CITY;
				dist = this_dist;
				weight = this_weight;
			}
		}
		/*if( found_target && target_x == pos.x && target_y == pos.y ) {
			// already at best square - reset
			found_target = false;
			decision = DECISION_UNDECIDED;
			target_x = -1;
			target_y = -1;
		}*/
		if( found_target ) {
			ASSERT( candidates.size() == 1 );
			if( candidates.at(0) == pos ) {
				// already at best square - reset
				found_target = false;
				decision = DECISION_UNDECIDED;
				candidates.clear();
			}
		}
	}

	delete [] dists;

	if( found_target ) {
		ASSERT( decision != DECISION_UNDECIDED );

		//ASSERT( target_x != pos.x || target_y != pos.y );
		ASSERT( candidates.size() > 0 );
		Pos2D target;
		bool found = false;
		// prefer to stick with the choice calculated for the previous move/turn - this is particularly important with Railways, to avoid the AI moving in loops back and forth along the railways, as it keeps changing its mind!
		// less important, now that we do caching of the target square in Conquests? Also doesn't take into account the situation changing!
		/*for(int i=0;i<candidates.size() && !found;i++) {
			Pos2D candidate_target = candidates.at(i);
			ASSERT( candidate_target != pos );
			if( candidate_target == last_target ) {
				// prefer to stick with the choice calculated for the previous move/turn - this is particularly important with Railways, to avoid the AI moving in loops back and forth along the railways, as it keeps changing its mind!
				target = candidate_target;
				found = true;
			}
		}*/
		if( !found ) {
			// last choice no longer available, so pick a new one at random
			int r = rand() % candidates.size();
			target = candidates.at(r);
			//last_target = target;
		}

		//if( mainGamestate->getPlayer() != NULL ) // don't display if doing tests (due to performance testing)
		//if( false )
		{
			VI_log("AI: %s unit %s [%d] at (%d, %d) moving towards (%d, %d) [%s]\n",
				this->civilization->getName(), this->unit_template->getName(), this, this->pos.x, this->pos.y, target.x, target.y,
				decision == DECISION_BUILDCITY ? "Build City" :
				decision == DECISION_WORK ? "Work" :
				decision == DECISION_ATTACKCITY ? "Attack City" :
				decision == DECISION_ATTACKUNITS ? "Attack Units" :
				decision == DECISION_CONTACTUNITS ? "Contact Units" :
				decision == DECISION_EXPLORE ? "Explore" :
				decision == DECISION_CITY ? "Retreat To City" :
				"UNKNOWN");
		}


		//this->setStatus(STATUS_NORMAL);

		/*if( target == this->start_pos ) {
			VI_log("### AI got stuck in infinite loop, attempting to return to start! Give up!\n");
			// n.b., there's a small chance this could happen without it being a loop - e.g., the unit goes somewhere to attack, then returns along the same route for something else. safer to just give up movement
			this->useMoves();
			return false;
		}*/
		/*Pos2D old_pos = this->pos;

		this->setAIDest(target);
		bool moved = this->moveTowardsAI(target.x, target.y);*/
		// n.b., this unit may now be deleted if moved!
		//bool moved = this->moveTowardsAI(target_x, target_y);
		//VI_log("### Unit %d AI: %d\n", this, clock() - time_s);

		/*
		// this code doesn't work (a) because the unit may have been deleted, (b) because it may get triggered when attacking an adjacent unit (since we don't move in that case)
		if( moved && this->pos == this->ai_start_pos ) {
			VI_log("###\n");
			VI_log("###\n");
			VI_log("### AI got stuck in infinite loop, doubling back on previous square! Give up!\n");
			VI_log("###\n");
			VI_log("###\n");
			// n.b., there's a small chance this could happen without it being a loop - e.g., the unit goes somewhere to attack, then returns along the same route for something else. safer to just give up movement
			this->useMoves();
		}
		this->ai_start_pos = old_pos;
		*/
		/*if( !moved ) {
			//VI_log("failed to move!\n");
			this->useMoves(); // we do this, to speed performance (so we don't repeatedly try to move this Unit)
		}
		return moved;*/
		*target_square = target;
		return AIInterface::UNITACTION_MOVE;
	}
	else {
		// can't do anything
		//this->movesleft = 0;
		//VI_log("### Unit %d AI: %d\n", this, clock() - time_s);
		/*this->useMoves(); // we do this, to speed performance (so we don't repeatedly try to move this Unit)
		return false;*/
		return AIInterface::UNITACTION_NOTHING;
	}
}
#endif

bool Unit::doAI() {
    //return true;
	//VI_log("doAI start\n");
	// returns true iff the AI moved (and hence we need to return to the main game loop for one iteration)
	// note that we'll need to return to the main loop even if after moving, this Unit has no moves left - in case moving means that a previous unit can now move
	// we should return false if the AI doesn't move - it means that nothing has changed, so we don't want to get stuck in an endless loop

	if( !this->getTemplate()->isAir() && !this->getTemplate()->isSea() ) {
		if( this->ai_move_count >= 100 ) {
			// last resort guard against infinite loops!
			VI_log("###\n");
			VI_log("###\n");
			VI_log("### AI too many moves: %d! Give up!\n", ai_move_count);
			VI_log("###\n");
			VI_log("###\n");
			this->useMoves();
			return false;
		}
		this->ai_move_count++;

		if( this->ai_has_dest ) {
			ASSERT( this->ai_dest != this->pos );
			bool moved = this->moveTowardsAI(ai_dest.x, ai_dest.y);
			// n.b., this unit may now be deleted if moved!
			if( !moved ) {
				this->useMoves(); // we do this, to speed performance (so we don't repeatedly try to move this Unit)
			}
			return moved;
		}

	}

	/*if( this->getTemplate()->isAir() ) {
		AIInterface::setMainGamestate(this->mainGamestate);
		AIInterface::setAICivilization(this->civilization);
		bool moved = this->doAIAir();
		AIInterface::setMainGamestate(NULL);
		AIInterface::setAICivilization(NULL);
		return moved;
	}*/

	//int time_s = clock();
	AIInterface::setMainGamestate(this->mainGamestate);
	AIInterface::setAICivilization(this->civilization);
	//bool moved = this->doAILand();

#ifdef USE_LUA_UNIT_AI
	// Lua code
	game_g->pushScriptFunction("doUnitAI");
	//game_g->pushScriptFunction("doUnitAITest");
	lua_pushlightuserdata(game_g->getLuaState(), this);
	int time_s = game_g->getGraphicsEnvironment()->getRealTimeMS();
	if( !game_g->runScriptFunction(1) ) {
		ASSERT(false);
	}
	int time = game_g->getGraphicsEnvironment()->getRealTimeMS() - time_s;
	static int total_time = 0;
	total_time += time;
	//VI_log("### Lua time: %d, %d\n", time, total_time);

	// read results
	AIInterface::UnitAction unit_action = AIInterface::UNITACTION_NOTHING;
	Pos2D target_square;
	bool allow_travel = true;
	int arg_index = 1;

	if( !lua_isnumber(game_g->getLuaState(), arg_index) ) {
		VI_log("Unit::doAI(): script return argument %d not a number\n", arg_index);
	}
	else {
		lua_Integer value = lua_tointeger(game_g->getLuaState(), arg_index);
		unit_action = static_cast<AIInterface::UnitAction>(value);
	}
	arg_index++;

	if( !lua_isnumber(game_g->getLuaState(), arg_index) ) {
		VI_log("Unit::doAI: script return argument %d not a number\n", arg_index);
	}
	else {
		target_square.x = static_cast<int>(lua_tointeger(game_g->getLuaState(), arg_index));
	}
	arg_index++;

	if( !lua_isnumber(game_g->getLuaState(), arg_index) ) {
		VI_log("Unit::doAI: script return argument %d not a number\n", arg_index);
	}
	else {
		target_square.y = static_cast<int>(lua_tointeger(game_g->getLuaState(), arg_index));
	}
	arg_index++;

	if( !lua_isboolean(game_g->getLuaState(), arg_index) ) {
		VI_log("Unit::doAI: script return argument %d not a boolean\n", arg_index);
	}
	else {
		allow_travel = lua_toboolean(game_g->getLuaState(), arg_index)!=0 ? true: false;
	}
	arg_index++;

	lua_settop(game_g->getLuaState(), 0); // important - clear the stack!
#else
	int time_s = clock();

	Pos2D target_square;
	AIInterface::UnitAction unit_action = AIInterface::UNITACTION_NOTHING;
	bool allow_travel = true;
	if( this->getTemplate()->isAir() ) {
		unit_action = this->doAIAir(&target_square);
	}
	else {
		unit_action = this->doAILand(&target_square);
	}

	int time = clock() - time_s;
	static int total_time = 0;
	total_time += time;
	//VI_log("### C++ time: %d, %d\n", time, total_time);

#endif

	if( unit_action != AIInterface::UNITACTION_NOTHING )
	{
		//VI_log("AI for unit at (%d, %d) returns: action %d, target square %d, %d, allow_travel? %d\n", pos.x, pos.y, unit_action, target_square.x, target_square.y, allow_travel?1:0);
	}

	bool moved = false;
	//unit_action = AIInterface::UNITACTION_NOTHING; // test
	//ASSERT( unit_action == AIInterface::UNITACTION_NOTHING ); // test
	if( unit_action == AIInterface::UNITACTION_WAIT ) {
		moved = false;
	}
	else if( unit_action == AIInterface::UNITACTION_NOTHING ) {
		//moved = true;
		moved = false;
		this->useMoves();
	}
	else if( unit_action == AIInterface::UNITACTION_MOVE ) {
		this->setStatus(STATUS_NORMAL);
		if( this->getTemplate()->isAir() ) {
			City *src_city = mainGamestate->getMap()->findCity(this->pos);
			ASSERT( src_city != NULL );
			City *dest_city = mainGamestate->getMap()->findCity(target_square);
			if( dest_city == NULL ) {
				VI_log("unit %d ( %s ) can't move to a non-city square\n", this, this->unit_template->getName());
				ASSERT(false);
			}
			else {
				if( !src_city->canLaunch(this->unit_template) ) {
					VI_log("unit %d ( %s ) can't launch from the city of %s\n", this, this->unit_template->getName(), src_city->getName());
					ASSERT(false);
				}
				else if( dest_city->getCivilization() != this->civilization ) {
					// bomb enemy city
					// n.b., the checks should be done in MainGamestate::bomb(), but check here just to be sure, and to give more informative debug info
					if( !mainGamestate->canBomb(this->getTemplate(), this->pos, target_square) ) {
						VI_log("unit %d ( %s ) can't launch from the city of %s to bomb %s\n", this, this->unit_template->getName(), src_city->getName(), dest_city->getName());
						ASSERT(false);
					}
					else {
						mainGamestate->bomb(this, target_square);
					}
				}
				else {
					// rebase
					src_city->initTravel(false, this->unit_template);
					if( !mainGamestate->getMap()->getSquare(target_square)->isTarget() ) {
						VI_log("unit %d ( %s ) can't rebase from the city of %s to the city of %s\n", this, this->unit_template->getName(), src_city->getName(), dest_city->getName());
						ASSERT(false);
					}
					mainGamestate->getMap()->resetTargets();
					mainGamestate->moveTo(this, target_square, true);
				}
			}
			moved = true;
		}
		else if( this->getTemplate()->isSea() ) {
			City *src_city = mainGamestate->getMap()->findCity(this->pos);
			ASSERT( src_city != NULL );
			City *dest_city = mainGamestate->getMap()->findCity(target_square);
			if( dest_city == NULL ) {
				VI_log("unit %d ( %s ) can't move to a non-city square\n", this, this->unit_template->getName());
				ASSERT(false);
			}
			else {
				if( dest_city->getCivilization() != this->civilization ) {
					VI_log("unit %d ( %s ) can't move to another civ's city\n", this, this->unit_template->getName());
					ASSERT(false);
				}
				else {
					// rebase
					src_city->initTravel(false, this->unit_template);
					if( !mainGamestate->getMap()->getSquare(target_square)->isTarget() ) {
						VI_log("unit %d ( %s ) can't rebase from the city of %s to the city of %s\n", this, this->unit_template->getName(), src_city->getName(), dest_city->getName());
						ASSERT(false);
					}
					mainGamestate->getMap()->resetTargets();
					mainGamestate->moveTo(this, target_square, true);
				}
			}
			moved = true;
		}
		else {
			this->setAIDest(target_square);
			moved = this->moveTowardsAI(target_square.x, target_square.y, allow_travel, true);
			// n.b., this unit may now be deleted if moved!
			if( !moved ) {
				//VI_log("failed to move!\n");
				this->useMoves(); // we do this, to speed performance (so we don't repeatedly try to move this Unit)
			}
		}
	}
	else if( unit_action == AIInterface::UNITACTION_WORK ) {
		if( !this->getTemplate()->canBuildRoads() ) {
			VI_log("unit %d ( %s ) does not have work ability\n", this, this->unit_template->getName());
			ASSERT(false);
		}
		moved = true;
		const MapSquare *square = mainGamestate->getMap()->getSquare(pos);
		if( square->getRoad() == ROAD_NONE ) {
			this->setStatus(Unit::STATUS_BUILDING_ROAD);
			this->update();
		}
		else if( square->getRoad() == ROAD_BASIC ) {
			this->setStatus(Unit::STATUS_BUILDING_RAILWAYS);
			this->update();
		}
		else {
			ASSERT( false );
		}
	}
	else if( unit_action == AIInterface::UNITACTION_BUILD_CITY ) {
		if( !this->getTemplate()->canBuildCity() ) {
			VI_log("unit %d ( %s ) does not have work ability\n", this, this->unit_template->getName());
			ASSERT(false);
		}
		moved = true;
		this->civilization->removeUnit(this, false);
			// remove it before creating city, to avoid AI confusion
			// but don't check dead, as if this is the only settler, with no existing cities, we'll think the Civilization has been wiped out!
		string city_name = this->civilization->makeCityName();
		this->civilization->advanceCityName();
		new City(mainGamestate, this->civilization, city_name.c_str(), pos.x, pos.y);
		this->civilization = NULL; // to stop the unit deconstructor trying to remove it again
		//VI_log("### Unit %d AI: %d\n", this, clock() - time_s);
		delete this;
		// unit now deleted!
	}
	AIInterface::setMainGamestate(NULL);
	AIInterface::setAICivilization(NULL);
	return moved;
}

bool Unit::moveTowardsAI(int target_x, int target_y, bool allow_travel, bool allow_combat) {
	// n.b., this unit may be deleted if we return true! (due to unit attacking, and losing)
	ASSERT( mainGamestate->getMap()->isValidBase(target_x, target_y) );
	//VI_log("move unit %d: %d, %d to %d, %d; allow_travel? %d\n", this, pos.x, pos.y, target_x, target_y, allow_travel?1:0);
	ASSERT( this->status == STATUS_NORMAL );
	ASSERT( this->pos != Pos2D(target_x, target_y) );
	const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(civilization, this, false, false, target_x, target_y); // traverse backwards from target
	//const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(this, false, true, target_x, target_y); // traverse backwards from target

	vector<Pos2D> candidate_moves;
	vector<bool> candidate_travelling;

	for(int cy=pos.y-1;cy<=pos.y+1;cy++) {
		for(int cx=pos.x-1;cx<=pos.x+1;cx++) {

			if( !mainGamestate->getMap()->isValid(cx,cy) )
				continue;

			int base_cx = cx, base_cy = cy;
			mainGamestate->getMap()->reduceToBase(&base_cx, &base_cy);
			candidate_moves.push_back(Pos2D(base_cx, base_cy));
			candidate_travelling.push_back(false);
			/*{
				int dist = dists[base_cy*mainGamestate->getMap()->getWidth() + base_cx].cost;
				int squares = dists[base_cy*mainGamestate->getMap()->getWidth() + base_cx].squares;
				VI_log("move to %d, %d costs %d, %d\n", base_cx, base_cy, dist, squares);
			}*/
		}
	}

	const City *city = mainGamestate->getMap()->findCity(pos);
	if( city != NULL && allow_travel ) {
		// TODO: cache target squares from City
		// can travel instead?
		city->initTravel(false, this->unit_template);
		for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
			for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
				const MapSquare *square = mainGamestate->getMap()->getSquare(x, y);
				if( square->isTarget() ) {
					Pos2D candidate_pos(x, y);
					// TODO: faster to use a set
					bool found = false;
					for(vector<Pos2D>::iterator iter = candidate_moves.begin(); iter != candidate_moves.end() && !found; ++iter) {
						Pos2D cpos = *iter;
						if( cpos == candidate_pos ) {
							found = true;
						}
					}
					if( !found ) {
						candidate_moves.push_back(candidate_pos);
						candidate_travelling.push_back(true);
						/*{
							int dist = dists[y*mainGamestate->getMap()->getWidth() + x].cost;
							int squares = dists[y*mainGamestate->getMap()->getWidth() + x].squares;
							VI_log("travel move to %d, %d costs %d, %d\n", x, y, dist, squares);
						}*/
					}
				}
			}
		}
		mainGamestate->getMap()->resetTargets();
	}

	bool found = false;
	int best_dist = -1, best_squares = -1;
	int nx = -1, ny = -1;
	bool best_danger = false; // dangerous square is one where a 0 defence unit is at risk of attack
	bool best_stack = false; // in cases of equal possibilities, prefer squares which already have a friendly unit at
	int best_abs = -1; // in cases of equal possibilities: 1. prefer non-diagonal moves to diagonal; 2. prefer not moving to moving (needed to avoid infinite loops - see TEST_UNIT_MOVE_6)
	bool best_travelling = false;

	/*for(vector<Pos2D>::iterator iter = candidate_moves.begin(); iter != candidate_moves.end(); ++iter) {
		Pos2D cpos = *iter;*/
	ASSERT( candidate_moves.size() == candidate_travelling.size() );
	for(size_t i=0;i<candidate_moves.size();i++) {
		Pos2D cpos = candidate_moves.at(i);
		int cx = cpos.x;
		int cy = cpos.y;
		ASSERT( mainGamestate->getMap()->isValidBase(cx, cy) );
		bool this_travelling = candidate_travelling.at(i);
		bool this_target = false;
		if( target_x == cx && target_y == cy )
			this_target = true;
		if( !this->canMoveTo( cpos, true, true )  ) {
			//ASSERT( !this_target );
			if( this_target ) {
				// can no longer reach target square (e.g., due to enemy being there)
				return false;
			}
			continue;
		}
		if( !this_target || !allow_combat ) {
			// if this_target and allow_combat are true, then we must be intending to attack any enemy units here
			const vector<Unit *> *units = mainGamestate->getMap()->findUnitsAt(cx,cy);
			if( units->size() > 0 ) {
				bool ok = true;
				if( units->at(0)->getCivilization() != this->civilization ) {
					ok = false;
				}
				if( !ok ) {
					if( this_target ) {
						ASSERT( !allow_combat );
						// can no longer reach target square (due to enemy being there, that we don't wish to attack)
						return false;
					}
					else {
						// we always want to go round any enemy units, if not at the target square
						continue;
					}
				}
			}
			//delete units;
		}
		int dist = dists[cy*mainGamestate->getMap()->getWidth() + cx].cost;
		if( dist == -1 )
			continue;
		int squares = dists[cy*mainGamestate->getMap()->getWidth() + cx].squares;
		ASSERT( squares != -1 );
		if( this_travelling ) {
			// need to take into account travel move!
			dist += road_move_scale_c * this->unit_template->getMoves();
			squares++;
		}
		//if( !this_target ) {
		if( cpos != pos ) {
			// need to take into account the move from the current square to this candidate square
			int mp = mainGamestate->getMap()->getSquare(cx, cy)->getScaledMovementCost(this->civilization);
			mp = min(mp,road_move_scale_c*this->getTemplate()->getMoves()); // need consistent behaviour with calculateDistanceMap - see infinite loop bug in TEST_UNIT_MOVE_4
			dist += mp;
			// n.b., we _don't_ increase the squares count
		}
		bool this_danger = false;
		if( this->unit_template->getDefence() == 0 ) {
			for(int ty=cy-1;ty<=cy+1 && !this_danger;ty++) {
				for(int tx=cx-1;tx<=cx+1 && !this_danger;tx++) {
					if( tx == cx && ty == cy ) {
						continue;
					}
					if( !mainGamestate->getMap()->isValid(tx, ty) ) {
						continue;
					}
					/*const vector<Unit *> *units = game->getMap()->findUnitsAt(tx,ty);
					if( units->size() > 0 ) {
						if( units->at(0)->getCivilization() != this->civilization ) {
							const Relationship *relationship = game->findRelationship(units->at(0)->getCivilization(), this->civilization);
							if( relationship->getStatus() == Relationship::STATUS_WAR ) {
								for(vector<Unit *>::const_iterator iter = units->begin();iter != units->end() && !this_danger;++iter) {
									const Unit *this_unit = *iter;
									if( this_unit->unit_template->getAttack() > 0 ) {
										this_danger = true;
									}
								}
							}
						}
					}*/
					if( this->civilization->isFogOfWarVisible(tx, ty) )
						this_danger = mainGamestate->getMap()->getSquare(tx, ty)->hasEnemies(this->civilization);
				}
			}
		}
		/*if( !found || dist < best_dist ||
		( dist == best_dist && dir % 2 == 0 ) // prefer non-diagonal moves
		) {*/
		//int this_abs = abs(cx-pos.x+cy-pos.y);
		//int this_abs = abs(cx-pos.x)+abs(cy-pos.y);
		int diffx = cx - pos.x;
		int diffy = cy - pos.y;
		mainGamestate->getMap()->reduceDiff(&diffx, &diffy);
		int this_abs = abs(diffx) + abs(diffy);
		bool this_stack = false;
		if( cpos != this->pos ) {
			const vector<Unit *> *units = mainGamestate->getMap()->getSquare(cpos)->getUnits();
			if( units->size() > 0 && units->at(0)->getCivilization() == this->civilization ) {
				this_stack = true;
			}
		}
		if( !found || dist < best_dist ||
			( best_danger && !this_danger ) || // prefer non-danger squares, even if longer distance
			( dist == best_dist && squares < best_squares ) || // prefer shortest distance without taking into account costs (needed for railways - important to prevent infinite loops!)
			( dist == best_dist && squares == best_squares && ( !best_stack && this_stack ) ) || // prefer to stack units
			( dist == best_dist && squares == best_squares && best_stack == this_stack && this_abs < best_abs )
			) {
				if( this_danger && !best_danger ) {
					// keep with the non-danger square
				}
				else {
					found = true;
					best_dist = dist;
					best_squares = squares;
					best_danger = this_danger;
					best_stack = this_stack;
					best_abs = this_abs;
					nx = cx;
					ny = cy;
					best_travelling = this_travelling;
				}
		}
	}
	delete [] dists;
	if( !found )
		return false;
	/*bool moved = game->translateUnitTo(this, nx, ny);
	ASSERT( moved );*/
	//moveTo(this, nx, ny);
	//moveTo(Pos2D(nx, ny));
	if( pos.x == nx && pos.y == ny ) {
		return false;
	}
	if( this->ai_has_dest && this->ai_dest == Pos2D(nx, ny) ) {
		// either we'll reach our destination (in which case, need to reset the dest)
		// or we're now attacking a unit, in which case, we should reset the dest, to reevaluate the AI choice
		this->setAIDest();
	}
	mainGamestate->moveTo(this, Pos2D(nx, ny), best_travelling);
	return true;
}
