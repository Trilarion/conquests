#pragma once

/** Represents an individual City, together with AI routines.
*/

#define USE_LUA_CITY_AI

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <map>
using std::map;

#include "utils.h"
#include "common.h"

class MainGamestate;
class Civilization;
class Buildable;
class Improvement;
class Unit;
class UnitTemplate;
class Element;
class Map;

const int max_road_bonus_c = 3; // must match Lua globals
const int barracks_turns_per_unit_c = 25;

const int size_limit_farmland_c = 3;
const int size_limit_aqueduct_c = 6;
const int size_limit_agriculture_c = 12;
const int size_limit_combustion_c = 16;

class City {
	MainGamestate *mainGamestate;
	Civilization *civilization;
	bool is_resisting;
	Civilization *resisting_civilization;
	int resistance;
	Pos2D pos; // saved
	string name; // saved
	int size; // saved
	map<const Element *, int> element_stocks; // saved

	int progress_population; // saved

	int progress_buildable; // saved
	const Buildable *buildable; // saved
	vector<const Improvement *> improvements; // saved
	int counter_barracks; // saved

	bool needs_update;

	void addImprovement(const Improvement *improvement);
	void setDefaultBuildable();
	void modifyForResistance(int *value) const;
	void modifyForDifficulty(int *value, bool science) const;
	int calculateRoadRailwaysScienceBonus() const;

	// AI functions
#ifndef USE_LUA_CITY_AI
	bool chooseBuildPopLimits();
	const Improvement *chooseImprovementAI();
	const UnitTemplate *chooseAirUnitAI(const UnitTemplate *bestFighterAirUnit, const UnitTemplate *bestBomberAirUnit, const UnitTemplate *bestConventionalMissileUnit, const UnitTemplate *bestNuclearMissileUnit, bool war_only);
#endif
	void chooseAndSetBuildAI();

	City(MainGamestate *mainGamestate, Civilization *civilization);
public:
	City(MainGamestate *mainGamestate, Civilization *civilization, const char *name, int x, int y, int size = 1);
	~City();

	static City *load(MainGamestate *mainGamestate, Civilization *civilization, FILE *file);
	void save(FILE *file) const;

	int getX() const {
		return pos.x;
	}
	int getY() const {
		return pos.y;
	}
	Pos2D getPos() const {
		return pos;
	}
	Civilization *getCivilization() const {
		// civilization isn't owned by this class, so okay to return non-const in const member function
		return civilization;
	}
	/*void setCivilization(Civilization *civilization) {
		this->civilization = civilization;
	}*/
	void detachCivilization() {
		this->civilization = NULL;
	}
	void capture(Civilization *new_civilization);
	int getSize() const {
		return size;
	}
	void setSize(int size);
	int getPopulation() const;
	string getPopulationString() const;
	int calculateProduction() const;
	int calculateScience() const;
	int calculatePowerPerTurn() const;
	const char *getName() const {
		return name.c_str();
	}
	void setName(const char *name) {
		this->name = name;
	}
	bool isResisting() const {
		return this->is_resisting;
	}
	int getResistance() const;
	int getProgressPopulation() const {
		return progress_population;
	}
	int getPopulationCost() const;
	int getElementStocks(const Element *element) {
		// if no entry for this element, will be initialised with default of 0
		return this->element_stocks[element];
	}
	void setElementStocksTest(const Element *element, int amount) {
		// for testing only - allows going over the maximum!
		this->element_stocks[element] = amount;
	}

	void setNeedsUpdate() {
		this->needs_update = true;
	}
	bool needsUpdate() const {
		return this->needs_update;
	}
	void addImprovementTest(const Improvement *improvement);
	bool improvementHasEffect(const Improvement *improvement) const;
	void update();
	void useUpElements(const Buildable *b);
	bool canLaunch(const UnitTemplate *unit_template) const;
	//bool canTravel(const UnitTemplate *unit_template) const;
	//bool canTravel(bool by_air) const;
	int getTravelRange(bool by_air) const;
	void getTravelRanges(int *sea_range, int *air_range, const UnitTemplate *unit_template) const;
	void initTravel(bool reveal, const UnitTemplate *unit_template) const {
		initTravel(reveal, unit_template, false);
	}
	void initTravel(bool reveal, const UnitTemplate *unit_template, bool sea_defending) const;
	const Improvement *getBestAirDefenceImprovement() const;
	void setBuildable(const Buildable *buildable);
	const Buildable *getBuildable() const {
		return this->buildable;
	}
	int getProgressBuildable() const {
		return progress_buildable;
	}
	size_t getNImprovements() const {
		return improvements.size();
	}
	const Improvement *getImprovement(size_t i) const {
		return improvements.at(i);
	}
	void removeImprovement(size_t i) {
		improvements.erase(improvements.begin() + i);
	}
	void removeImprovement(const Improvement *improvement) {
		for(vector<const Improvement *>::iterator iter = improvements.begin(); iter != improvements.end(); ++iter) {
			const Improvement *imp = *iter;
			if( imp == improvement ) {
				improvements.erase(iter);
				break;
			}
		}
	}
	bool hasImprovement(const char *improvement) const;
	bool hasImprovement(const Improvement *improvement) const;
	bool hasImprovementOrNewer(const Improvement *improvement) const;
	//bool hasImprovement(ImprovementType improvementType) const;
	bool canBuild(const Buildable *candidate_buildable) const;
	int getDefenceBonus(const Unit *attacker) const;
	bool losePopulation() const;

	// Remember to check that the returned positions isValid in the map!
	static Pos2D getCitySquare(const Map *map, int i, Pos2D pos);
	Pos2D getCitySquare(int i) const;
	static int getCentreCitySquareIndex();
	static int getNCitySquares();

	//Pos2D getAdjacentSquare(int i) const;
	bool isOnOrAdjacent(Type mapType) const;
	string getStatsInfo() const;

	// AI functions
	void doAI();
	bool needDefendersAI(bool *undefended, const Unit *this_unit, int min_defence) const;
	//bool wantWorkerAt(int indx, const Unit *unit) const;
	const Buildable *getBuildChoiceAI();
};
