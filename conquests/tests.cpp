#include "conquests_stdafx.h"

#include "tests.h"
#include "maingamestate.h"
#include "map.h"
#include "civilization.h"
#include "city.h"
#include "buildable.h"
#include "unit.h"
#include "infowindow.h"

#include "../Vision/VisionIface.h"

#include <ctime>

#include <sstream>
using std::stringstream;

/**
 *
 * Tests
 *
 * TEST_SETTLERS_MOVE_0 - test that a Settler moves away from adjacent enemy unit, and doesn't move into the adjacent unprotected city.
 * TEST_SETTLERS_MOVE_1 - test that a Settler moves away from adjacent enemy unit.
 * TEST_SETTLERS_MOVE_2 - test that a Settler ignores an adjacent friendly unit, and builds a city.
 * TEST_SETTLERS_MOVE_3 - test that a Settler ignores an adjacent enemy Settler unit, and builds a city.
 * TEST_SETTLERS_MOVE_4 - test that a Settler moves away from adjacent enemy unit, when building a road.
 * TEST_SETTLERS_MOVE_5 - test that a Settler can't move though another civ's territory without ROP agreement.
 * TEST_SETTLERS_MOVE_6 - test that a Settler can move though another civ's territory with ROP agreement.
 * TEST_SETTLERS_MOVE_7 - test that a Settler doesn't move though another civ's territory that the AI is at war with.
 * TEST_SETTLERS_MOVE_8 - test that a Settler doesn't move though another civ's territory that the AI is at war with, but that it can take a longer route around it.
 * TEST_SETTLERS_MOVE_9 - as TEST_SETTLERS_MOVE_7, but where the Settlers is already in enemy territory, so okay to continue.
 * TEST_MAKE_CONTACT_0 - test that a unit retreats, instead of moving towards unit, as we've already made contact.
 * TEST_MAKE_CONTACT_1 - test that a unit moves towards a foreign city (territory) to make contact.
 * TEST_ATTACK_UNIT - tests that a unit attacks an adjacent settler unit.
 * TEST_TRAVEL_EXPLORE_3 - tests that we can explore via harbour, across map wraparound
 * TEST_SETTLER_BUILDCITY_0 - build city at current location: grasslands, coastal.
 * TEST_SETTLER_BUILDCITY_1 - move from grasslands, non-coastal to grasslands, coastal.
 * TEST_SETTLER_BUILDCITY_2 - move from desert, coastal to grasslands, non-coastal.
 * TEST_SETTLER_BUILDCITY_3 - build city at current location: grasslands, non-coastal (compared with desert, coastal).
 * TEST_SETTLER_BUILDCITY_4 - move from desert, non-coastal to desert, coastal.
 * TEST_SETTLER_BUILDCITY_5 - build city at current location: grasslands, non-coastal (compared with grasslands, non-coastal).
 * TEST_SETTLER_BUILDCITY_6 - build city at current location: desert, non-coastal (compared with desert, non-coastal).
 * TEST_SETTLER_BUILDCITY_7 - as TEST_SETTLER_BUILDCITY_1 but with hills, coastal as target square.
 * TEST_SETTLER_BUILDCITY_8 - as TEST_SETTLER_BUILDCITY_1, but with forest such that we build city at original location.
 * TEST_SETTLER_BUILDCITY_9 - test that we can't build city at current location, due to city that is across the wraparound seam (cylinder topology)
 * TEST_SETTLER_BUILDCITY_10 - move to prefer forests.
 * TEST_SETTLER_BUILDCITY_11 - move to prefer bonus.
 * TEST_SETTLER_BUILDCITY_12 - move to prefer bonus+forest, over bonus.
 * TEST_SETTLER_BUILDCITY_13 - move so don't build on top of forest.
 * TEST_SETTLER_BUILDCITY_14 - move so don't build on top of bonus.
 * TEST_SETTLER_BUILDCITY_15 - tests that a settler unit moves from one city, to build a new city.
 * TEST_SETTLER_BUILDCITY_16 - as TEST_SETTLER_BUILDCITY_15, but in year 200.
 * TEST_BUILDROAD_0 - test that a Peasants unit near a city starts building roads
 * TEST_BUILDROAD_1 - test that a Peasants unit near a city starts building roads (automation)
 * TEST_BUILDROAD_2 - test that a Peasants unit between cities moves in order to start building roads
 * TEST_BUILDROAD_3 - test that a Peasants unit between cities moves in order to start building roads (automation)
 * TEST_BUILDROAD_4 - test that a Peasants unit between cities starts building roads
 * TEST_BUILDROAD_5 - test that a Peasants unit between cities starts building roads (automation)
 * TEST_BUILDROAD_6 - test that a Peasants unit near a small city doesn't start building roads
 * TEST_BUILDROAD_7 - test that a Peasants unit at a small city does start building roads
 * TEST_BUILDROAD_8 - test that a Peasants unit near a small city retreats to a city, due to nothing else to do (automation)
 * TEST_BUILDRAILWAYS_0 - test that a Peasants unit near a city starts building railways
 * TEST_BUILDRAILWAYS_1 - test that a Peasants unit near a city starts building railways (automation)
 * TEST_BUILDRAILWAYS_2 - test that a Peasants unit between cities moves in order to start building railways
 * TEST_BUILDRAILWAYS_3 - test that a Peasants unit between cities moves in order to start building railways (automation)
 * TEST_BUILDRAILWAYS_4 - test that a Peasants unit between cities starts building railways
 * TEST_BUILDRAILWAYS_5 - test that a Peasants unit between cities starts building railways (automation)
 * TEST_UNIT_MOVE_0 - test that a unit finds the shortest path along road
 * TEST_UNIT_MOVE_1 - test that a unit finds the shortest path along railways
 * TEST_UNIT_MOVE_2 - test that a unit finds the shortest path along road
 * TEST_UNIT_MOVE_3 - test that a unit finds the shortest path along railways
 * TEST_UNIT_MOVE_4 - test that a unit finds the shortest path along railways (infinite loop check)
 * TEST_UNIT_MOVE_5 - test that a unit finds the shortest path along railways (infinite loop check - case where the railway loop is much longer in terms of number of squares)
 * TEST_UNIT_MOVE_6 - test that a unit finds the shortest path along railways (infinite loop check - case where an enemy blocks the route for a Settlers unit)
 * TEST_UNIT_MOVE_7 - as TEST_UNIT_MOVE_1, with friendly civ territory
 * TEST_UNIT_MOVE_8 - as TEST_UNIT_MOVE_1, with enemy civ territory
 * TEST_UNIT_MOVE_9 - tests that a unit moves over the periodic seam of map with cylinder topology.
 * TEST_UNIT_MOVE_10 - movement from a city with port, with road along coast, unit with 1 move, move 2 squares away: check we move along road, with 1/3 moves left. Also check distance to dest square.
 * TEST_UNIT_MOVE_11 - movement from a city with port, with road along coast, unit with 1 move, move 4 squares away: check we travel, with 0 moves left. Also check distance to dest square.
 * TEST_UNIT_MOVE_12 - movement from a city with port, with road along coast, unit with 2 move, move 4 squares away: check we move along road, with 2/3 moves left. Also check distance to dest square.
 * TEST_UNIT_MOVE_13 - movement from a city with port, with railways along coast, unit with 1 move, move 6 squares away: check we move along railways, with 1 move left. Also check distance to dest square.
 * TEST_UNIT_MOVE_14 - as TEST_UNIT_MOVE_1, but with friendly unit at start square - checks that we don't return back to the original square.
 * TEST_UNIT_RETREAT_0 - tests that a tanks unit retreats to a city, prefer city that's further, but with port.
 * TEST_UNIT_RETREAT_1 - as TEST_UNIT_RETREAT_0, but prefer the city that has fewer tank units.
 * TEST_UNIT_RETREAT_2 - as TEST_UNIT_RETREAT_1, but the unit is located at the best city.
 * TEST_UNIT_RETREAT_3 - have 2 equidistant cities, city 0 has 2 Modern Infantry, city 1 has 3 Modern Infantry and 3 Tanks, and port. Test that a Modern Infantry goes to city 0 (to defend it).
 * TEST_UNIT_RETREAT_4 - as TEST_UNIT_RETREAT_3, but with Tank - now test that it goes to city 1 (due to the port).
 * TEST_AIR_BOMB_0 - Test for bombing enemy city with bomber (with airport).
 * TEST_AIR_BOMB_1 - Test for city with bomber and no airport, check that we can't bomb.
 * TEST_AIR_BOMB_2 - Test for bombing enemy city with fission bomb (with airport).
 * TEST_AIR_BOMB_3 - Test for city with fission bomb and no airport, check that we can't bomb.
 * TEST_AIR_BOMB_4 - Test for launching cruise missile against enemy city (with missile launcher and no airport).
 * TEST_AIR_BOMB_5 - Test for city with cruise missile, airport and no missile launcher, check that we can't bomb.
 * TEST_AIR_BOMB_6 - Test for launching nuclear missile against enemy city (with missile launcher and no airport).
 * TEST_AIR_BOMB_7 - Test for city with nuclear missile, airport and no missile launcher, check that we can't bomb.
 * TEST_AIR_REBASE_0 - Test for two cities and one bomber, each with airports, check that we can rebase.
 * TEST_AIR_REBASE_1 - Test for two cities and one jet bomber, only source has airport, check that we can't rebase.
 * TEST_AIR_REBASE_2 - Test for two cities and one stealth bomber, only destination has airport, check that we can't rebase.
 * TEST_AIR_REBASE_3 - Test for two cities and one cruise missile, each with airports, check that we can't rebase.
 * TEST_AIR_REBASE_4 - Test for land unit travelling between two cities, each with airports.
 * TEST_AIR_REBASE_5 - Test for land unit not travelling between two cities, only source has airport.
 * TEST_AIR_REBASE_6 - Test for land unit not travelling between two cities, only destination has airport.
 * TEST_ATTACK_0 - City defended with two spearmen. Test for swordsmen at city to attack adjacent catapults.
 * TEST_ATTACK_1 - City defended with three spearmen; also add one swordsmen. Test for one of the spearmen at city to attack adjacent catapults. (As it's better odds than being attacked.)
 * TEST_ATTACK_2 - City defended with three infantry. Test that we do attack an adjacent infantry unit. (As it's the best attacker unit.)
 * TEST_ATTACK_3 - City defended with three infantry; also add one tank. Test that we don't attack an adjacent infantry unit. (As it's better odds to be attacked.)
 * TEST_ATTACK_4 - Peasants not at a city but within city radius. Should first work land, then do new turn with additional adjacent enemy unit that's Chariots - the peasants should now attack the unit (as odds are better to attack then defend).
 * TEST_ATTACK_5 - As TEST_ATTACK_4, but new enemy unit is peasants - no better to attack, so should carry on working the land.
 * TEST_ATTACK_6 - As TEST_ATTACK_4, but current square already has road; also we only do 1 turn, with the enemy unit already adjacent. We should attack.
 * TEST_ATTACK_7 - As TEST_ATTACK_5, but current square already has road; also we only do 1 turn, with the enemy unit already adjacent. We should attack.
 * TEST_UNIT_EXPLORE_0 - Tests that a cuirassiers unit moves towards an unexplored square.
 * TEST_UNIT_EXPLORE_1 - Tests that a settlers unit adjacent to a city explores rather than building a road.
 * TEST_UNIT_EXPLORE_2 - Settlers and catapults at same square. Test that the catapults wait, then settlers build a city.
 * TEST_FOW_0 - As TEST_MAKE_CONTACT_0, but other unit is outside of fog of war, so we can't see it, so we retreat to the city instead.
 * TEST_FOW_1 - As TEST_UNIT_MOVE_6, the enemy unit can't be seen, so we unwittingly move the Settlers unit towards it
 * TEST_FOW_2 - As TEST_FOW_1, but without resetting the fog of war between moves.
 * TEST_FOW_3 - As TEST_FOW_0, but AI has Satellites, so we do move to make contact.
 * TEST_FOW_4 - As TEST_FOW_0, but city has Radar Tower, so we do move to make contact.
 * TEST_TRAVEL_INVADE_0 - Foot soldier, enemy unit visible, should be able to invade.
 * TEST_TRAVEL_INVADE_1 - Non-foot soldier, enemy unit visible, shouldn't be able to invade.
 * TEST_TRAVEL_INVADE_2 - Foot soldier, enemy unit invisible, should be able to invade.
 * TEST_TRAVEL_INVADE_3 - Non-foot soldier, enemy unit invisible, shouldn't be able to invade.
 * TEST_TRAVEL_INVADE_4 - As TEST_TRAVEL_INVADE_0, but don't invade, as city can build galleons but none present.
 * TEST_TRAVEL_INVADE_5 - As TEST_TRAVEL_INVADE_0, but don't invade, as city can build dreadnoughts but none present.
 * TEST_TRAVEL_INVADE_6 - As TEST_TRAVEL_INVADE_4, but galleons are present, so do invade.
 * TEST_TRAVEL_INVADE_7 - As TEST_TRAVEL_INVADE_4, but connected via land to target square - so we should attack, but via the land route.
 * TEST_TRAVEL_INVADE_8 - As TEST_TRAVEL_INVADE_7, but with battleships present, so we attack via sea.
 * TEST_TRAVEL_INVADE_9 - As TEST_TRAVEL_INVADE_4, but city doesn't have enough wood, so can't build galleons, so we should invade after all.
 * TEST_TRAVEL_INVADE_10 - 3 coastal cities, with 1 enemy coastal cities on a nearby island, attack with 6 tanks. They should all invade, and land at the same square.
 * TEST_TRAVEL_INVADE_11 - 3 coastal cities, with 3 enemy coastal cities of different population sizes on a nearby island, attack with 6 tanks. They should all invade the same square, going for the city with largest population.
 * TEST_MISC_0 - Civ builds a city, with another civ's unit adjacent to the new territory. Should make contact. Then test moving the other civ's unit.
 * TEST_REBEL_0 - Test that AI unit moves to attack a nearby rebel unit (visible within fog of war).
 * TEST_REBEL_1 - Test that AI unit doesn't move to attack a nearby rebel unit (visible within fog of war).
 * TEST_REBEL_2 - Test that rebels moves to attack city - shouldn't reveal map, as this should be done automatically.
 * TEST_CITYBUILD_0 - Tests that a city starts building peasants to defend with. City is on coast, cpu has Sailing - shouldn't build a harbour.
 * TEST_CITYBUILD_1 - Tests that city starts building horsemen to explore.
 * TEST_CITYBUILD_2 - Tests that city starts building farmland.
 * TEST_CITYBUILD_3 - Tests that city starts building peasants as a "cheap unit".
 * TEST_CITYBUILD_4 - Tests that city starts building harbour.
 * TEST_CITYBUILD_5 - Tests that city starts building peasants as worker unit for roads.
 * TEST_CITYBUILD_6 - Tests that city starts building peasants as worker unit for railways.
 * TEST_CITYBUILD_7 - As TEST_CITYBUILD_4, but with railways covering the map.
 * TEST_CITYBUILD_8 - Tests that city rushbuilds a nuke.
 * TEST_CITYBUILD_9 - Tests that a city starts building spearmen to defend with. City already has two peasants units. City is on coast, cpu has Sailing - shouldn't build a harbour.
 * TEST_CITYBUILD_10 - Tests that a city starts building tanks (when nothing else to build).
 * TEST_CITYBUILD_11 - As TEST_CITYBUILD_0, but size 2 city, already has one peasants defender. However for peasants, one defender is enough, so we should build settlers.
 * TEST_CITYBUILD_12 - As TEST_CITYBUILD_9, but size 2 city, already has one spearmen defender. Also have settlers, so we build the second spearman.
 * TEST_CITYBUILD_13 - Tests that we build a school. There's an enemy unit and city nearby, but civilizations are at peace, so improvement should take priority over attacker units.
 * TEST_CITYBUILD_14 - As TEST_CITYBUILD_13, but civilizations at war, so build tanks.
 * TEST_CITYBUILD_15 - As TEST_CITYBUILD_0, but with Rocketry and Agriculture - should build Farmland, as better to grow due to Modern Infantry taking too long to build.
 * TEST_CITYBUILD_16 - As TEST_CITYBUILD_12, but with no settlers unit, and so 2 peasants are enough. So start building settlers instead of spearmen.
 * TEST_CITYBUILD_17 - As TEST_CITYBUILD_5, where 3 nearby squares already have roads. Size 3 city, so we build peasants as worker. Make sure all improvements already built, due to random chance of building 1st phase improvements earlier for size >= 3 cities.
 * TEST_CITYBUILD_18 - As TEST_CITYBUILD_17, but with size 2 city, so we should build horsemen as explorers.
 * TEST_CITYBUILD_19 - As TEST_CITYBUILD_10, but no Oil, so tests that start building Peasants.
 * TEST_CITYBUILD_20 - As TEST_CITYBUILD_9, but size 3 city with one spearmen defender. City has 50 gunpowder, so should build musketeers.
 * TEST_CITYBUILD_21 - As TEST_CITYBUILD_20, but with two spearmen defenders. City has 50 gunpowder, but shouldn't build musketeers as spearmen will upgrade.
 * TEST_CITYBUILD_22 - 2 cities and 2 settlers units. One of the cities has two peasants defenders. Tests that we build spearmen.
 * TEST_CITYBUILD_23 - 2 cities and 2 settlers units. One of the cities has two spearmen defenders. Tests that we build settlers.
 * TEST_CITYBUILD_24 - 2 cities and 3 settlers units. One of the cities has two spearmen defenders. Tests that we don't build settlers.
 * TEST_CITYSTOCKS_0 - 4 cities, one with 190 wood, one with 250 wood, one with 0 wood, one with 50 wood; civ has Steam Power. Test that after update, stocks are 200, 200, 20, 70.
 * TEST_CITYSTOCKS_1 - As TEST_CITYSTOCKS_0, but no Steam Power, so after update stocks should be 190, 200, 0, 50.
 * TEST_CITYSTOCKS_2 - 2 civs each with 2 cities, one civ has Combustion; each has one city with Oil bonus. Tests that after end turn, only one city has new Oil.
 * TEST_CITYSTOCKS_3 - Civ has 2 cities each with Spearmen; civ has Medieval Arms. One city has 11 Iron, and the Pikemen should upgrade, leaving 1 Iron; the other city has 9 Iron, meaning we shouldn't upgrade.
 * TEST_CITY_0 - 3 cities, one with Temple, one with Church, one with both. Growth rate should be 9 turns for all 3.
 * TEST_CITY_1 - As TEST_CITY_0, but civilization has Feudalism. Growth rate should be 10, 9, 9.
 * TEST_CITY_2 - As TEST_CITY_0, but civilization has Feudalism and Scientific Method. Growth rate should be 10, 10, 10.
 * TEST_AIPERF_0 - Repeatedly call the AI routine, to move a unit across to attack an enemy unit. Also checks dist to enemy unit.
 * TEST_AIPERF_1 - Repeatedly call the AI routine, to move a unit across to attack an enemy unit, followed by a second enemy unit. Also checks dist, namely that we can't reach the second unit to begin with.
 * TEST_AIPERF_2 - As TEST_AIPERF_1, but the first unit is a friendly civ (which we also first make contact with), so we have to go round that unit to get to the second unit.
 * TEST_AIPERF_3 - As TEST_AIPERF_2, but first unit is now enemy civ again, but we force moving to the second unit, to ensure we navigate around it.
 * TEST_AIPERF_4 - As TEST_AIPERF_0, but this time resetting the ai_has_dest to force recalculation.
 * TEST_AIPERF_5 - Test with range of cities and units; tests performance of AI code for cities.
 * TEST_AIPERF_6 - As TEST_AIPERF_5, but testing performance of AI code for units.
 * TEST_AIPERF_7 - As TEST_AIPERF_6, but without revealing map to the AI (tests exploration).
 * TEST_AIPERF_8 - As TEST_AIPERF_0, but attacking enemy city instead of unit. There's an enemy unit that we miss on the way, that we can't see due to fog of war.
 * TEST_AIPERF_9 - As TEST_AIPERF_1, but without the unit to help fog of war for blocking unit.
 * TEST_AIPERF_10 - As TEST_AIPERF_2, but without the unit to help fog of war for blocking unit.
 * TEST_AIPERF_11 - As TEST_AIPERF_2, but instead checking that we pass around enemy territory that we don't have a right of passage agreement with.
 * TEST_AIPERF_12 - As TEST_AIPERF_11, but also with an enemy unit in the friendly territory - check that we don't attack this enemy, but continue with attacking the other enemy.
 * TEST_AIPERF_13 - As TEST_AIPERF_12, but this time we only have the one enemy unit in friendly territory, so we do nothing.
 * TEST_AIPERF_14 - As TEST_AIPERF_11, but have right of passage agreement, so can pass through territory.
 * TEST_AIPERF_15 - As TEST_AIPERF_12, but have right of passage agreement, so can attack the closer enemy in the other civ's territory.
 * TEST_AIPERF_16 - As TEST_AIPERF_13, but have right of passage agreement, so can attack the enemy in the other civ's territory.
 * TEST_AIPERF_17 - As TEST_AIPERF_10, but the blocking unit blocks completely - no way round.
 * TEST_DISTANCES_0 - performance test for calculating distance map
 * TEST_DISTANCES_1 - as TEST_DISTANCES_0, but with added roads and railways
 */

void Tests::run(MainGamestate *mainGamestate, const char *filename, TestID index) {
	// setup a game world for mainGamestate test
	Map *map = NULL;
	Civilization *cpu1 = NULL, *cpu2 = NULL, *cpu3 = NULL;
	const UnitTemplate *unit_template = NULL;
	City *city = NULL;

	if( index == TEST_SETTLERS_MOVE_0 || index == TEST_MAKE_CONTACT_0 || index == TEST_MAKE_CONTACT_1 || index == TEST_ATTACK_CITY || index == TEST_FOW_0 || index == TEST_FOW_3 || index == TEST_FOW_4 ) {
		map = new Map(mainGamestate, 16, 16);
		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap();
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		if( index == TEST_MAKE_CONTACT_0 )
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(4, 6));
		else if( index == TEST_MAKE_CONTACT_1 ) {
			new City(mainGamestate, cpu1, "", 6, 11);
		}
		else if( index == TEST_FOW_0 || index == TEST_FOW_3 || index == TEST_FOW_4 )
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(6, 9)); // invisible under fog of war (or would be, if not for Satellites)
		else
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The English") );
		cpu2->revealMap();

		if( index == TEST_FOW_3 )
			cpu2->addTechnology( game_g->findTechnology("Satellites") );

		if( index == TEST_MAKE_CONTACT_0 || index == TEST_MAKE_CONTACT_1 || index == TEST_FOW_0 || index == TEST_FOW_3 || index == TEST_FOW_4 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
		else
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
		if( index == TEST_MAKE_CONTACT_1 )
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(6, 7));
		else
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(6, 6));
		// create a nearby City, so we can't build a new City
		city = new City(mainGamestate, cpu2, "", 6, 5);
		if( index == TEST_FOW_4 ) {
			//city->addImprovementTest( mainGamestate->findImprovement(IMPROVEMENT_RADARTOWER) );
			city->addImprovementTest( game_g->findImprovement("Radar Tower") );
		}
	}
	else if( index == TEST_SETTLERS_MOVE_1 || index == TEST_SETTLERS_MOVE_2 || index == TEST_SETTLERS_MOVE_3 || index == TEST_SETTLERS_MOVE_4 || index == TEST_ATTACK_UNIT ) {
		map = new Map(mainGamestate, 16, 16);
		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap();
		if( index == TEST_SETTLERS_MOVE_3 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
		else
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The English") );
		cpu2->revealMap();
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
		new Unit(mainGamestate, cpu2, unit_template, Pos2D(6, 6));
	}
	else if( index == TEST_SETTLERS_MOVE_5 || index == TEST_SETTLERS_MOVE_6 || index == TEST_SETTLERS_MOVE_7 || index == TEST_SETTLERS_MOVE_8 || index == TEST_SETTLERS_MOVE_9 ) {
		map = new Map(mainGamestate, 20, 16);
		map->setAllTo(TYPE_MOUNTAINS);
		int end_y = index == TEST_SETTLERS_MOVE_8 ? 7 : 5;
		for(int y=3;y<=end_y;y++) {
			for(int x=3;x<=17;x++) {
				if( y == 7 && x >= 12 && index == TEST_SETTLERS_MOVE_8 ) {
					break;
				}
				map->getSquare(x, y)->setType(TYPE_GRASSLAND);
				if( index != TEST_SETTLERS_MOVE_8 ) {
					map->getSquare(x, y)->setRoad(ROAD_RAILWAYS);
				}
			}
		}
		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap();
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
		if( index == TEST_SETTLERS_MOVE_9 )
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 4));
		else
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
		new City(mainGamestate, cpu1, "Rome", 5, 4);
		if( index == TEST_SETTLERS_MOVE_8 ) {
			// so we don't work the land instead of building new city
			for(int i=0;i<cpu1->getCity(0)->getNCitySquares();i++) {
				Pos2D pos = cpu1->getCity(0)->getCitySquare(i);
				map->getSquare(pos)->setRoad(ROAD_RAILWAYS);
			}
		}

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The English") );
		cpu2->revealMap();
		new City(mainGamestate, cpu2, "London", 10, 4);

		const Technology *tech = game_g->findTechnology("Writing"); // needed for right of passage
		cpu1->addTechnology(tech);
		cpu2->addTechnology(tech);

		Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
		relationship->makeContact();
		if( index == TEST_SETTLERS_MOVE_6 )
			relationship->setRightOfPassage(true);
		else if( index == TEST_SETTLERS_MOVE_7 || index == TEST_SETTLERS_MOVE_8 || index == TEST_SETTLERS_MOVE_9 )
			relationship->setStatus(Relationship::STATUS_WAR);
	}
	else if( index == TEST_TRAVEL_EXPLORE_0 || index == TEST_TRAVEL_EXPLORE_1 || index == TEST_TRAVEL_EXPLORE_2 || index == TEST_TRAVEL_EXPLORE_3 ) {
		// N.B. - also tests for city close to boundary of map
		if( index == TEST_TRAVEL_EXPLORE_3 ) {
			map = new Map(mainGamestate, 1000, 16);
			map->setTopology(TOPOLOGY_CYLINDER);
		}
		else
			map = new Map(mainGamestate, 16, 16);
		map->setAllTo(TYPE_OCEAN);

		map->getSquare(5, 0)->setType(TYPE_GRASSLAND);
		map->getSquare(5, 1)->setType(TYPE_GRASSLAND);
		map->getSquare(5, 2)->setType(TYPE_GRASSLAND);
		map->getSquare(5, 3)->setType(TYPE_GRASSLAND);

		map->getSquare(4, 4)->setType(TYPE_GRASSLAND);
		map->getSquare(5, 4)->setType(TYPE_GRASSLAND);
		map->getSquare(6, 4)->setType(TYPE_GRASSLAND);
		map->getSquare(4, 5)->setType(TYPE_GRASSLAND);
		map->getSquare(5, 5)->setType(TYPE_GRASSLAND);
		map->getSquare(6, 5)->setType(TYPE_GRASSLAND);

		int offx = 0; // n.b., also defined below!
		if( index == TEST_TRAVEL_EXPLORE_3 )
			offx = 993;
		else
			offx = 4;
		map->getSquare(offx, 8)->setType(TYPE_GRASSLAND);
		map->getSquare(offx+1, 8)->setType(TYPE_GRASSLAND);
		map->getSquare(offx+2, 8)->setType(TYPE_GRASSLAND);
		map->getSquare(offx, 9)->setType(TYPE_GRASSLAND);
		map->getSquare(offx+1, 9)->setType(TYPE_GRASSLAND);
		map->getSquare(offx+2, 9)->setType(TYPE_GRASSLAND);
		map->getSquare(offx, 10)->setType(TYPE_GRASSLAND);
		map->getSquare(offx+1, 10)->setType(TYPE_GRASSLAND);
		map->getSquare(offx+2, 10)->setType(TYPE_GRASSLAND);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen"));
		if( index == TEST_TRAVEL_EXPLORE_0 || index == TEST_TRAVEL_EXPLORE_3 ) {
			// horsemen at city
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_TRAVEL_EXPLORE_1 ) {
			// horsemen near city
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
		}
		else if( index == TEST_TRAVEL_EXPLORE_2 ) {
			// horsemen near north city
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 2));
		}
		// add Spearmen so the city is defended
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		city = new City(mainGamestate, cpu1, "", 5, 5);
		if( index == TEST_TRAVEL_EXPLORE_3 )
			city->addImprovementTest(game_g->findImprovement("Port")); // need a Port, as we're testing a larger distance
		else
			city->addImprovementTest(game_g->findImprovement("Harbour"));
		city = new City(mainGamestate, cpu1, "", 5, 0);
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 0));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 0));
	}
	else if( index == TEST_SETTLER_BUILDCITY_0 || index == TEST_SETTLER_BUILDCITY_1 || index == TEST_SETTLER_BUILDCITY_2 || index == TEST_SETTLER_BUILDCITY_3 || index == TEST_SETTLER_BUILDCITY_4 || index == TEST_SETTLER_BUILDCITY_5 || index == TEST_SETTLER_BUILDCITY_6 || index == TEST_SETTLER_BUILDCITY_7 || index == TEST_SETTLER_BUILDCITY_8 || index == TEST_SETTLER_BUILDCITY_9 || index == TEST_SETTLER_BUILDCITY_10 || index == TEST_SETTLER_BUILDCITY_11 || index == TEST_SETTLER_BUILDCITY_12 || index == TEST_SETTLER_BUILDCITY_13 || index == TEST_SETTLER_BUILDCITY_14 || index == TEST_SETTLER_BUILDCITY_15 || index == TEST_SETTLER_BUILDCITY_16 ) {
		map = new Map(mainGamestate, 16, 16);
		if( index == TEST_SETTLER_BUILDCITY_9 ) {
			map->setTopology(TOPOLOGY_CYLINDER);
		}

		if( index == TEST_SETTLER_BUILDCITY_5 || index == TEST_SETTLER_BUILDCITY_9 || index == TEST_SETTLER_BUILDCITY_10 || index == TEST_SETTLER_BUILDCITY_11 || index == TEST_SETTLER_BUILDCITY_12 || index == TEST_SETTLER_BUILDCITY_13 || index == TEST_SETTLER_BUILDCITY_14 || index == TEST_SETTLER_BUILDCITY_15 || index == TEST_SETTLER_BUILDCITY_16 ) {
			//map->setAllTo(TYPE_GRASSLAND);
			// grassland by default
		}
		else if( index == TEST_SETTLER_BUILDCITY_6 )
			map->setAllTo(TYPE_DESERT);
		else {
			map->setAllTo(TYPE_OCEAN);
			map->getSquare(4, 4)->setType(TYPE_DESERT);
			map->getSquare(5, 4)->setType(TYPE_DESERT);
			if( index == TEST_SETTLER_BUILDCITY_8 )
				map->getSquare(6, 4)->setType(TYPE_FOREST);
			else
				map->getSquare(6, 4)->setType(TYPE_DESERT);
			map->getSquare(4, 5)->setType(TYPE_DESERT);
			map->getSquare(5, 5)->setType(index == TEST_SETTLER_BUILDCITY_4 ? TYPE_DESERT : TYPE_GRASSLAND);
			map->getSquare(6, 5)->setType(TYPE_DESERT);
			if( index == TEST_SETTLER_BUILDCITY_3 || index == TEST_SETTLER_BUILDCITY_4 )
				map->getSquare(4, 6)->setType(TYPE_DESERT);
			else if( index == TEST_SETTLER_BUILDCITY_7 )
				map->getSquare(4, 6)->setType(TYPE_HILLS);
			else
				map->getSquare(4, 6)->setType(TYPE_GRASSLAND);
			map->getSquare(5, 6)->setType(TYPE_DESERT);
			map->getSquare(6, 6)->setType(TYPE_DESERT);
		}

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap(); // needed so the AI can see the terrain, when making the decisions for best city location; also to avoid exploring
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
		if( index == TEST_SETTLER_BUILDCITY_0 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(4, 6));
		}
		else if( index == TEST_SETTLER_BUILDCITY_1 || index == TEST_SETTLER_BUILDCITY_3 || index == TEST_SETTLER_BUILDCITY_4 || index == TEST_SETTLER_BUILDCITY_5 || index == TEST_SETTLER_BUILDCITY_6 || index == TEST_SETTLER_BUILDCITY_7 || index == TEST_SETTLER_BUILDCITY_8 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_SETTLER_BUILDCITY_2 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(6, 5));
		}
		else if( index == TEST_SETTLER_BUILDCITY_9 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(15, 5));
			new City(mainGamestate, cpu1, "", 1, 5);
		}
		else if( index == TEST_SETTLER_BUILDCITY_10 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			map->getSquare(5, 2)->setType(TYPE_FOREST);
		}
		else if( index == TEST_SETTLER_BUILDCITY_11 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			const BonusResource *bonus_resource = mainGamestate->getGameData()->findBonusResource("Sheep");
			map->getSquare(5, 2)->setBonusResource(bonus_resource);
		}
		else if( index == TEST_SETTLER_BUILDCITY_12 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			map->getSquare(5, 2)->setType(TYPE_FOREST);
			const BonusResource *bonus_resource = mainGamestate->getGameData()->findBonusResource("Sheep");
			map->getSquare(6, 6)->setBonusResource(bonus_resource);
		}
		else if( index == TEST_SETTLER_BUILDCITY_13 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			map->getSquare(5, 5)->setType(TYPE_FOREST);
		}
		else if( index == TEST_SETTLER_BUILDCITY_14 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			const BonusResource *bonus_resource = mainGamestate->getGameData()->findBonusResource("Sheep");
			map->getSquare(5, 5)->setBonusResource(bonus_resource);
		}
		else if( index == TEST_SETTLER_BUILDCITY_15 || index == TEST_SETTLER_BUILDCITY_16 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new City(mainGamestate, cpu1, "Rome", 5, 5);
			if( index == TEST_SETTLER_BUILDCITY_16 ) {
				mainGamestate->setYear(200);
			}
		}
		else {
			ASSERT( false );
		}
	}
	else if( index == TEST_DEFEND_0 || index == TEST_DEFEND_1 ) {
		map = new Map(mainGamestate, 16, 16);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		city = new City(mainGamestate, cpu1, "", 5, 5);
		city->setSize(3);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Knights")); // not swordsmen, as that can work squares
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
	}
	else if( index == TEST_BUILDROAD_0 || index == TEST_BUILDROAD_1 || index == TEST_BUILDROAD_6 || index == TEST_BUILDROAD_7 || index == TEST_BUILDROAD_8 || index == TEST_BUILDRAILWAYS_0 || index == TEST_BUILDRAILWAYS_1 ) {
		map = new Map(mainGamestate, 16, 16);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		const Technology *tech = game_g->findTechnology("Steam Power");
		cpu1->addTechnology(tech);
		city = new City(mainGamestate, cpu1, "", 5, 5);
		if( index == TEST_BUILDROAD_6 || index == TEST_BUILDROAD_7 ) {
			city->setSize(2);
		}
		else {
			city->setSize(3);
		}
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		if( index == TEST_BUILDROAD_7 ) {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else {
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 6));
		}
		if( index == TEST_BUILDRAILWAYS_0 || index == TEST_BUILDRAILWAYS_1 ) {
			map->getSquare(5, 6)->setRoad(ROAD_BASIC);
		}
		map->getSquare(4, 4)->setRoad(ROAD_RAILWAYS);
		map->getSquare(5, 4)->setRoad(ROAD_RAILWAYS);
		map->getSquare(6, 4)->setRoad(ROAD_RAILWAYS);
		if( index == TEST_BUILDROAD_8 ) {
			cpu1->revealMap();
			for(size_t i=0;i<cpu1->getNCities();i++) {
				city = cpu1->getCity(i);
				for(int j=0;j<city->getNCitySquares();j++) {
					Pos2D pos = city->getCitySquare(j);
					if( map->isValid(pos.x, pos.y) ) {
						map->getSquare(pos)->setRoad(ROAD_RAILWAYS);
					}
				}
			}
		}
	}
	else if( index == TEST_BUILDROAD_2 || index == TEST_BUILDROAD_3 || index == TEST_BUILDROAD_4 || index == TEST_BUILDROAD_5 || index == TEST_BUILDRAILWAYS_2 || index == TEST_BUILDRAILWAYS_3 || index == TEST_BUILDRAILWAYS_4 || index == TEST_BUILDRAILWAYS_5 ) {
		map = new Map(mainGamestate, 16, 16);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		const Technology *tech = game_g->findTechnology("Steam Power");
		cpu1->addTechnology(tech);
		city = new City(mainGamestate, cpu1, "", 5, 5);
		city->setSize(3);
		city = new City(mainGamestate, cpu1, "", 11, 5);
		city->setSize(3);
		if( index == TEST_BUILDROAD_4 || index == TEST_BUILDROAD_5 || index == TEST_BUILDRAILWAYS_4 || index == TEST_BUILDRAILWAYS_5 ) {
			for(size_t i=0;i<cpu1->getNCities();i++) {
				city = cpu1->getCity(i);
				for(int j=0;j<city->getNCitySquares();j++) {
					Pos2D pos = city->getCitySquare(j);
					if( map->isValid(pos.x, pos.y) ) {
						map->getSquare(pos)->setRoad(ROAD_BASIC);
					}
				}
			}
		}
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 5));
		if( index == TEST_BUILDRAILWAYS_2 || index == TEST_BUILDRAILWAYS_3 ) {
			map->getSquare(8, 5)->setRoad(ROAD_RAILWAYS);
		}
		else if( index == TEST_BUILDRAILWAYS_4 || index == TEST_BUILDRAILWAYS_5 ) {
			map->getSquare(8, 5)->setRoad(ROAD_BASIC);
		}
		if( index == TEST_BUILDROAD_2 || index == TEST_BUILDROAD_3 || index == TEST_BUILDRAILWAYS_2 || index == TEST_BUILDRAILWAYS_3 ) {
			cpu1->revealMap(); // needed so that the unit moves to build roads/railways - otherwise exploring will take priority
		}
	}
	else if( index == TEST_UNIT_MOVE_0 || index == TEST_UNIT_MOVE_1 || index == TEST_UNIT_MOVE_2 || index == TEST_UNIT_MOVE_3 || index == TEST_UNIT_MOVE_4 || index == TEST_UNIT_MOVE_5 || index == TEST_UNIT_MOVE_6 || index == TEST_UNIT_MOVE_7 || index == TEST_UNIT_MOVE_8 || index == TEST_UNIT_MOVE_9 || index == TEST_UNIT_MOVE_14 || index == TEST_FOW_1 || index == TEST_FOW_2 ) {
		map = new Map(mainGamestate, 16, 16);

		if( index == TEST_UNIT_MOVE_9 ) {
			map->setTopology(TOPOLOGY_CYLINDER);
		}
		mainGamestate->setMap(map);
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap();

		if( index == TEST_UNIT_MOVE_0 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			map->getSquare(5, 5)->setRoad(ROAD_BASIC);
			map->getSquare(6, 6)->setRoad(ROAD_BASIC);
			map->getSquare(7, 5)->setRoad(ROAD_BASIC);
		}
		else if( index == TEST_UNIT_MOVE_1 || index == TEST_UNIT_MOVE_7 || index == TEST_UNIT_MOVE_8 || index == TEST_UNIT_MOVE_14 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			for(int y=0;y<map->getHeight();y++) {
				for(int x=0;x<map->getWidth();x++) {
					map->getSquare(x, y)->setRoad(ROAD_BASIC);
				}
			}
			map->getSquare(5, 5)->setRoad(ROAD_RAILWAYS);
			map->getSquare(6, 6)->setRoad(ROAD_RAILWAYS);
			map->getSquare(7, 5)->setRoad(ROAD_RAILWAYS);

			if( index == TEST_UNIT_MOVE_7 || index == TEST_UNIT_MOVE_8 ) {
				cpu2 = new Civilization(mainGamestate, game_g->findRace("The Greeks") );
				new City(mainGamestate, cpu2, "", 5, 4);
			}
			if( index == TEST_UNIT_MOVE_8 ) {
				Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
				relationship->makeContact();
				relationship->setStatus(Relationship::STATUS_WAR);
			}
			if( index == TEST_UNIT_MOVE_14 ) {
				new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			}
		}
		else if( index == TEST_UNIT_MOVE_2 || index == TEST_UNIT_MOVE_3 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen")); // need unit with moves 2, for test TEST_UNIT_MOVE_2
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(4, 6));
			Road road = index == TEST_UNIT_MOVE_2 ? ROAD_BASIC : ROAD_RAILWAYS;
			map->getSquare(5, 5)->setRoad(road);
			map->getSquare(4, 6)->setRoad(road);
			map->getSquare(5, 7)->setRoad(road);
			map->getSquare(6, 7)->setRoad(road);
		}
		else if( index == TEST_UNIT_MOVE_4 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants")); // need a unit with moves 1, to test an infinite loop bug
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(9, 10));
			map->getSquare(7, 9)->setRoad(ROAD_RAILWAYS);
			map->getSquare(7, 10)->setRoad(ROAD_RAILWAYS);
			map->getSquare(8, 10)->setRoad(ROAD_RAILWAYS);
			map->getSquare(9, 10)->setRoad(ROAD_RAILWAYS);
			map->getSquare(8, 9)->setType(TYPE_FOREST);
			map->getSquare(9, 9)->setType(TYPE_FOREST);
			map->getSquare(10, 9)->setType(TYPE_FOREST);
			map->getSquare(10, 8)->setType(TYPE_HILLS);
		}
		else if( index == TEST_UNIT_MOVE_5 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			map->getSquare(5, 5)->setRoad(ROAD_RAILWAYS);
			map->getSquare(5, 6)->setRoad(ROAD_RAILWAYS);
			map->getSquare(5, 7)->setRoad(ROAD_RAILWAYS);
			map->getSquare(5, 8)->setRoad(ROAD_RAILWAYS);
			map->getSquare(6, 8)->setRoad(ROAD_RAILWAYS);
			map->getSquare(7, 8)->setRoad(ROAD_RAILWAYS);
			map->getSquare(7, 7)->setRoad(ROAD_RAILWAYS);
			map->getSquare(7, 6)->setRoad(ROAD_RAILWAYS);
			map->getSquare(7, 5)->setRoad(ROAD_RAILWAYS);
		}
		else if( index == TEST_UNIT_MOVE_6 || index == TEST_FOW_1 || index == TEST_FOW_2 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers")); // must be a unit with 0 defence
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(0, 3));
			if( index == TEST_UNIT_MOVE_6 ) {
				new Unit(mainGamestate, cpu1, unit_template, Pos2D(2, 0)); // so the enemy unit is always visible under the fog of war
			}
			cpu2 = new Civilization( mainGamestate, game_g->findRace("The English") );
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants")); // must be a unit capable of attack
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(1, 1));
			for(int y=0;y<=3;y++) {
				for(int x=0;x<=3;x++) {
					map->getSquare(x, y)->setRoad(ROAD_RAILWAYS);
				}
			}
			Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
			// check we've made contact (as adjacent units)
			relationship->makeContact();
			relationship->setStatus(Relationship::STATUS_WAR);
		}
		else if( index == TEST_UNIT_MOVE_9 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(0, 5));
		}
		else {
			ASSERT( false );
		}

		map->createAlphamap();

	}
	else if( index == TEST_TRAVEL_INVADE_0 || index == TEST_TRAVEL_INVADE_1 || index == TEST_TRAVEL_INVADE_2 || index == TEST_TRAVEL_INVADE_3 || index == TEST_TRAVEL_INVADE_4 || index == TEST_TRAVEL_INVADE_5 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_7 || index == TEST_TRAVEL_INVADE_8 || index == TEST_TRAVEL_INVADE_9 ) {
		map = new Map(mainGamestate, 16, 16);
		map->setAllTo(TYPE_OCEAN);

		map->getSquare(5, 5)->setType(TYPE_GRASSLAND);
		map->getSquare(10, 4)->setType(TYPE_GRASSLAND);
		map->getSquare(10, 5)->setType(TYPE_GRASSLAND);
		map->getSquare(5, 5)->setRoad(ROAD_BASIC); // so we don't try working

		if( index == TEST_TRAVEL_INVADE_7 || index == TEST_TRAVEL_INVADE_8 ) {
			for(int x=5;x<=9;x++) {
				map->getSquare(x, 4)->setType(TYPE_GRASSLAND);
				map->getSquare(x, 4)->setRoad(ROAD_BASIC); // so we don't try working
			}
		}
		mainGamestate->setMap(map);
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap();

		city = new City(mainGamestate, cpu1, "", 5, 5);
		//city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_HARBOUR));
		city->addImprovementTest(game_g->findImprovement("Harbour"));
		if( index == TEST_TRAVEL_INVADE_4 || index == TEST_TRAVEL_INVADE_5 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_7 || index == TEST_TRAVEL_INVADE_8 || index == TEST_TRAVEL_INVADE_9 ) {
			//city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_PORT));
			city->addImprovementTest(game_g->findImprovement("Port"));
			cpu1->addTechnology( game_g->findTechnology("Magnetism") );
			if( index == TEST_TRAVEL_INVADE_5 ) {
				cpu1->addTechnology( game_g->findTechnology("Explosives") );
			}
			else if( index == TEST_TRAVEL_INVADE_8 ) {
				cpu1->addTechnology( game_g->findTechnology("Rocketry") );
			}
			if( index == TEST_TRAVEL_INVADE_4 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_7 ) {
				city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 50);
			}
			else if( index == TEST_TRAVEL_INVADE_5 ) {
				// so the city can build Dreadnoughts
				city->setElementStocksTest(game_g->getGameData()->findElement("Iron"), 100);
				city->setElementStocksTest(game_g->getGameData()->findElement("Oil"), 100);
			}
			else if( index == TEST_TRAVEL_INVADE_9 ) {
				city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 10);
			}
		}

		if( index == TEST_TRAVEL_INVADE_0 || index == TEST_TRAVEL_INVADE_2 || index == TEST_TRAVEL_INVADE_4 || index == TEST_TRAVEL_INVADE_5 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_7 || index == TEST_TRAVEL_INVADE_8 || index == TEST_TRAVEL_INVADE_9 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
		else if( index == TEST_TRAVEL_INVADE_1 || index == TEST_TRAVEL_INVADE_3 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen"));
		else {
			ASSERT( false );
		}
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));

		// create some defenders
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Infantry"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));

		if( index == TEST_TRAVEL_INVADE_6 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Galleons"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_TRAVEL_INVADE_8 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Battleships"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}

		if( index == TEST_TRAVEL_INVADE_0 || index == TEST_TRAVEL_INVADE_1 || index == TEST_TRAVEL_INVADE_4 || index == TEST_TRAVEL_INVADE_5 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_7 || index == TEST_TRAVEL_INVADE_8 || index == TEST_TRAVEL_INVADE_9 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(10, 4));
		}

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The English") );
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cuirassiers"));
		new Unit(mainGamestate, cpu2, unit_template, Pos2D(10, 5));

		map->createAlphamap();
	}
	else if( index == TEST_TRAVEL_INVADE_10 || index == TEST_TRAVEL_INVADE_11 ) {
		map = new Map(mainGamestate, 16, 16);
		map->setAllTo(TYPE_OCEAN);

		for(int x=1;x<=14;x++) {
			map->getSquare(x, 3)->setType(TYPE_GRASSLAND);
			map->getSquare(x, 4)->setType(TYPE_GRASSLAND);
			map->getSquare(x, 8)->setType(TYPE_GRASSLAND);
		}

		mainGamestate->setMap(map);
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->addTechnology( game_g->findTechnology("Magnetism") );
		cpu1->revealMap();

		city = new City(mainGamestate, cpu1, "A", 3, 8, 3);
		city->addImprovementTest(game_g->findImprovement("Port"));
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Tanks"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());

		city = new City(mainGamestate, cpu1, "B", 8, 8, 3);
		city->addImprovementTest(game_g->findImprovement("Port"));
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Tanks"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());

		city = new City(mainGamestate, cpu1, "C", 13, 8, 3);
		city->addImprovementTest(game_g->findImprovement("Port"));
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Tanks"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The English") );

		if( index == TEST_TRAVEL_INVADE_11 ) {
			city = new City(mainGamestate, cpu2, "1", 3, 3, 6);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
			new Unit(mainGamestate, cpu2, unit_template, city->getPos());
			new Unit(mainGamestate, cpu2, unit_template, city->getPos());
		}

		city = new City(mainGamestate, cpu2, "2", 8, 3, 8);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
		new Unit(mainGamestate, cpu2, unit_template, city->getPos());
		new Unit(mainGamestate, cpu2, unit_template, city->getPos());

		if( index == TEST_TRAVEL_INVADE_11 ) {
			city = new City(mainGamestate, cpu2, "3", 13, 3, 10);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
			new Unit(mainGamestate, cpu2, unit_template, city->getPos());
			new Unit(mainGamestate, cpu2, unit_template, city->getPos());
		}

		map->createAlphamap();
	}
	else if( index == TEST_MISC_0 ) {
		map = new Map(mainGamestate, 16, 16);
		map->setAllTo(TYPE_GRASSLAND);
		mainGamestate->setMap(map);
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The Greeks") );
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		new Unit(mainGamestate, cpu2, unit_template, Pos2D(2, 5));

		map->createAlphamap();
	}
	else if( index == TEST_REBEL_0 || index == TEST_REBEL_1 || index == TEST_REBEL_2 ) {
		map = new Map(mainGamestate, 64, 64);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		// so we can see the rebels:
		cpu1->revealMap();
		cpu1->addTechnology( game_g->findTechnology("Satellites") );

		/*
		// need to do this manually, as normally only created when initialising or loading a standard game!
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );
		*/

		if( index == TEST_REBEL_0 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cannon"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate,  mainGamestate->getRebelCiv(), unit_template, Pos2D(8, 8));
		}
		else if( index == TEST_REBEL_1 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cannon"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate,  mainGamestate->getRebelCiv(), unit_template, Pos2D(32, 8));
		}
		else if( index == TEST_REBEL_2 ) {
			city = new City(mainGamestate, cpu1, "City", 5, 5);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cannon"));
			new Unit(mainGamestate,  mainGamestate->getRebelCiv(), unit_template, Pos2D(32, 32));
		}
		else {
			ASSERT(false);
		}
	}
	else if( index == TEST_UNIT_MOVE_10 || index == TEST_UNIT_MOVE_11 || index == TEST_UNIT_MOVE_12 || index == TEST_UNIT_MOVE_13 ) {
		map = new Map(mainGamestate, 16, 16);
		map->setAllTo(TYPE_OCEAN);

		const int ypos = 5;
		for(int x=1;x<=14;x++) {
			map->getSquare(x, ypos)->setType(TYPE_GRASSLAND);
			if( x > 1 ) {
				if( index == TEST_UNIT_MOVE_13 )
					map->getSquare(x, ypos)->setRoad(ROAD_RAILWAYS);
				else
					map->getSquare(x, ypos)->setRoad(ROAD_BASIC);
			}
		}
		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Vikings") );
		city = new City(mainGamestate, cpu1, "City", 1, ypos);
		city->addImprovementTest(game_g->findImprovement("Port"));

		if( index == TEST_UNIT_MOVE_12 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen"));
		else
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(1, ypos));
	}
	else if( index == TEST_UNIT_RETREAT_0 || index == TEST_UNIT_RETREAT_1 || index == TEST_UNIT_RETREAT_2 ) {
		map = new Map(mainGamestate, 16, 16);
		for(int y=0;y<map->getHeight();y++) {
			for(int x=0;x<map->getWidth();x++) {
				if( x == 0 || x == map->getWidth()-1 || y == 0 || y == map->getHeight()-1 ) {
					map->getSquare(x, y)->setType(TYPE_OCEAN);
				}
			}
		}

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap();

		city = new City(mainGamestate, cpu1, "Rome", 1, 1);
		if( index == TEST_UNIT_RETREAT_0 ) {
			city->addImprovementTest(game_g->findImprovement("Port"));
		}

		city = new City(mainGamestate, cpu1, "Pompeii", 14, 1);

		if( index == TEST_UNIT_RETREAT_0 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Tanks"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(12, 2));
		}
		else if( index == TEST_UNIT_RETREAT_1 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Tanks"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(3, 2));
		}
		else if( index == TEST_UNIT_RETREAT_2 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Tanks"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(14, 1));
		}
		else {
			ASSERT(false);
		}

		if( index == TEST_UNIT_RETREAT_1 || index == TEST_UNIT_RETREAT_2 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Tanks"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(1, 1));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(1, 1));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(14, 1));
		}
	}
	else if( index == TEST_UNIT_RETREAT_3 || index == TEST_UNIT_RETREAT_4 ) {
		map = new Map(mainGamestate, 16, 16);
		for(int y=0;y<map->getHeight();y++) {
			for(int x=0;x<map->getWidth();x++) {
				if( y <= 1 ) {
					map->getSquare(x, y)->setType(TYPE_OCEAN);
				}
				else {
					map->getSquare(x, y)->setRoad(ROAD_RAILWAYS); // so we don't try to work the land when testing with Modern Infantry
				}
			}
		}

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap();
		cpu1->addTechnology( game_g->findTechnology("Rocketry") );
		cpu1->addTechnology( game_g->findTechnology("Motorised Warfare") );

		city = new City(mainGamestate, cpu1, "Rome", 2, 2);

		city = new City(mainGamestate, cpu1, "Pompeii", 8, 2);
		city->addImprovementTest(game_g->findImprovement("Port"));

		if( index == TEST_UNIT_RETREAT_3 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Infantry"));
		}
		else if( index == TEST_UNIT_RETREAT_4 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Tanks"));
		}
		else {
			ASSERT(false);
		}
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 3));

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Modern Infantry"));

		new Unit(mainGamestate, cpu1, unit_template, Pos2D(2, 2));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(2, 2));

		new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 2));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 2));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 2));

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Tanks"));

		new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 2));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 2));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(8, 2));

	}
	else if(
		index == TEST_AIR_BOMB_0 ||
		index == TEST_AIR_BOMB_1 ||
		index == TEST_AIR_BOMB_2 ||
		index == TEST_AIR_BOMB_3 ||
		index == TEST_AIR_BOMB_4 ||
		index == TEST_AIR_BOMB_5 ||
		index == TEST_AIR_BOMB_6 ||
		index == TEST_AIR_BOMB_7 )
	{
		map = new Map(mainGamestate, 16, 16);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		city = new City(mainGamestate, cpu1, "Rome", 5, 5);

		if( index == TEST_AIR_BOMB_0 || index == TEST_AIR_BOMB_2 )
			city->addImprovementTest(game_g->findImprovement("Airport"));
		else {
			cpu1->revealMap(); // only need to reveal map if no airport
		}

		if( index == TEST_AIR_BOMB_4 || index == TEST_AIR_BOMB_6 )
			city->addImprovementTest(game_g->findImprovement("Missile Launcher"));

		if( index == TEST_AIR_BOMB_0 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Bombers"));
		else if( index == TEST_AIR_BOMB_1 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Jet Bombers"));
		else if( index == TEST_AIR_BOMB_2 || index == TEST_AIR_BOMB_3 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Fission Bomb"));
		else if( index == TEST_AIR_BOMB_4 || index == TEST_AIR_BOMB_5 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cruise Missile"));
		else if( index == TEST_AIR_BOMB_6 || index == TEST_AIR_BOMB_7 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Nuclear Missile"));
		else {
			ASSERT(false);
		}

		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The French") );
		city = new City(mainGamestate, cpu2, "Paris", 10, 10);
		city->setSize(20); // make worth bombing
		// add some improvements to potentially destroy
		city->addImprovementTest(game_g->findImprovement("Mine"));
		city->addImprovementTest(game_g->findImprovement("Factory"));

		Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
		relationship->makeContact();
		relationship->setStatus(Relationship::STATUS_WAR);
	}
	else if(
		index == TEST_AIR_REBASE_0 ||
		index == TEST_AIR_REBASE_1 ||
		index == TEST_AIR_REBASE_2 ||
		index == TEST_AIR_REBASE_3 ||
		index == TEST_AIR_REBASE_4 ||
		index == TEST_AIR_REBASE_5 ||
		index == TEST_AIR_REBASE_6 )
	{
		map = new Map(mainGamestate, 16, 16);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );

		city = new City(mainGamestate, cpu1, "", 5, 5);
		city->addImprovementTest(game_g->findImprovement("Port"));
		if( index == TEST_AIR_REBASE_0 || index == TEST_AIR_REBASE_1 || index == TEST_AIR_REBASE_3 || index == TEST_AIR_REBASE_4 || index == TEST_AIR_REBASE_5 )
			city->addImprovementTest(game_g->findImprovement("Airport"));

		city = new City(mainGamestate, cpu1, "", 10, 10);
		if( index == TEST_AIR_REBASE_0 || index == TEST_AIR_REBASE_2 || index == TEST_AIR_REBASE_3 || index == TEST_AIR_REBASE_4 || index == TEST_AIR_REBASE_6 )
			city->addImprovementTest(game_g->findImprovement("Airport"));

		if( index == TEST_AIR_REBASE_0 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Bombers"));
		else if( index == TEST_AIR_REBASE_1 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Jet Bombers"));
		else if( index == TEST_AIR_REBASE_2 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Stealth Bombers"));
		else if( index == TEST_AIR_REBASE_3 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cruise Missile"));
		else if( index == TEST_AIR_REBASE_4 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		else if( index == TEST_AIR_REBASE_5 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Tanks"));
		else if( index == TEST_AIR_REBASE_6 )
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cuirassiers"));
		else {
			ASSERT(false);
		}

		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
	}
	else if( index == TEST_ATTACK_0 || index == TEST_ATTACK_1 || index == TEST_ATTACK_2 || index == TEST_ATTACK_3 || index == TEST_ATTACK_4 || index == TEST_ATTACK_5 || index == TEST_ATTACK_6 || index == TEST_ATTACK_7 ) {
		map = new Map(mainGamestate, 16, 16);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		cpu1->revealMap(); // so the AI doesn't decide to explore instead

		city = new City(mainGamestate, cpu1, "", 5, 5);
		city->setSize(3); // so min defenders is 2

		if( index == TEST_ATTACK_0 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Swordsmen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_ATTACK_1 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Swordsmen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_ATTACK_2 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_ATTACK_3 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Tanks"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_ATTACK_4 || index == TEST_ATTACK_5 || index == TEST_ATTACK_6 || index == TEST_ATTACK_7 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(7, 6));
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			if( index == TEST_ATTACK_6 || index == TEST_ATTACK_7 ) {
				map->getSquare(7, 6)->setRoad(ROAD_BASIC);
			}
		}
		else {
			ASSERT( false );
		}
		map->getSquare(5, 5)->setRoad(ROAD_BASIC); // so the units don't try to work the city square

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The Vikings") );
		cpu2->revealMap();
		cpu2->addTechnology( game_g->findTechnology("Satellites") ); // so the enemy can see

		if( index == TEST_ATTACK_0 || index == TEST_ATTACK_1 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
		}
		else if( index == TEST_ATTACK_2 || index == TEST_ATTACK_3 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Infantry"));
		}
		else if( index == TEST_ATTACK_4 || index == TEST_ATTACK_6 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Chariots"));
		}
		else if( index == TEST_ATTACK_5 || index == TEST_ATTACK_7 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		}
		else {
			ASSERT( false );
		}
		if( index == TEST_ATTACK_4 || index == TEST_ATTACK_5 ) {
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(9, 5));
		}
		else if( index == TEST_ATTACK_6 || index == TEST_ATTACK_7 ) {
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(8, 5));
		}
		else {
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(6, 5));
		}

		Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
		relationship->makeContact();
		relationship->setStatus(Relationship::STATUS_WAR);
	}
	else if( index == TEST_UNIT_EXPLORE_0 || index == TEST_UNIT_EXPLORE_1 || index == TEST_UNIT_EXPLORE_2 ) {
		map = new Map(mainGamestate, 16, 16);

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Americans") );

		if( index == TEST_UNIT_EXPLORE_0 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cuirassiers"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			for(int y=0;y<map->getHeight();y++) {
				for(int x=0;x<map->getWidth();x++) {
					if( x == 8 && y == 2 )
						continue;
					cpu1->uncover(x, y, 0);
				}
			}
		}
		else if( index == TEST_UNIT_EXPLORE_1 ) {
			city = new City(mainGamestate, cpu1, "Washington", 6, 5);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_UNIT_EXPLORE_2 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else {
			ASSERT(false);
		}

	}
	else if( index == TEST_CITYBUILD_0 || index == TEST_CITYBUILD_1 || index == TEST_CITYBUILD_2 || index == TEST_CITYBUILD_3 || index == TEST_CITYBUILD_4 || index == TEST_CITYBUILD_5 || index == TEST_CITYBUILD_6 || index == TEST_CITYBUILD_7 || index == TEST_CITYBUILD_8 || index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_11 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 || index == TEST_CITYBUILD_15 || index == TEST_CITYBUILD_16 || index == TEST_CITYBUILD_17 || index == TEST_CITYBUILD_18 || index == TEST_CITYBUILD_19 || index == TEST_CITYBUILD_20 || index == TEST_CITYBUILD_21 || index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
		map = new Map(mainGamestate, 16, 16);
		if( index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
			map->setAllTo(TYPE_GRASSLAND);
			for(int y=0;y<map->getHeight();y++) {
				for(int x=0;x<map->getWidth();x++) {
					map->getSquare(x, y)->setRoad(ROAD_BASIC);
				}
			}
		}
		else {
			map->setAllTo(TYPE_OCEAN);
			for(int y=3;y<=5;y++) {
				for(int x=3;x<=5;x++) {
					map->getSquare(x, y)->setType(TYPE_GRASSLAND);
					if( index == TEST_CITYBUILD_1 || index == TEST_CITYBUILD_2 || index == TEST_CITYBUILD_3 || index == TEST_CITYBUILD_4 || index == TEST_CITYBUILD_6 || index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 || index == TEST_CITYBUILD_19 ) {
						map->getSquare(x, y)->setRoad(ROAD_BASIC);
					}
					else if( index == TEST_CITYBUILD_7 ) {
						map->getSquare(x, y)->setRoad(ROAD_RAILWAYS);
					}
				}
			}
			if( index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 ) {
				for(int y=0;y<=2;y++) {
					for(int x=3;x<=5;x++) {
						map->getSquare(x, y)->setType(TYPE_GRASSLAND);
					}
				}
			}
			if( index == TEST_CITYBUILD_17 || index == TEST_CITYBUILD_18 ) {
				map->getSquare(4, 4)->setRoad(ROAD_BASIC);
				map->getSquare(5, 4)->setRoad(ROAD_BASIC);
				map->getSquare(6, 4)->setRoad(ROAD_BASIC);
			}
		}

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Americans") );
		cpu1->removeAllTechnologies(); // to get rid of starting technology, for testing purposes
		if( index == TEST_CITYBUILD_2 || index == TEST_CITYBUILD_3 || index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 || index == TEST_CITYBUILD_19 || index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
			cpu1->revealMap();
		}
		else if( index == TEST_CITYBUILD_4 || index == TEST_CITYBUILD_7 ) {
			for(int y=2;y<=6;y++) {
				for(int x=2;x<=6;x++) {
					cpu1->uncover(x, y, 1);
				}
			}
		}

		cpu1->addTechnology( game_g->findTechnology("Sailing") );
		if( index != TEST_CITYBUILD_19 ) {
			// to ensure we just build Peasants
			cpu1->addTechnology( game_g->findTechnology("Horse Riding") );
			cpu1->addTechnology( game_g->findTechnology("Mathematics") );
		}
		cpu1->addTechnology( game_g->findTechnology("Agriculture") );
		if( index == TEST_CITYBUILD_6 || index == TEST_CITYBUILD_7 ) {
			cpu1->addTechnology( game_g->findTechnology("Steam Power") );
		}
		if( index == TEST_CITYBUILD_0 || index == TEST_CITYBUILD_8 || index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_11 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_15 || index == TEST_CITYBUILD_16 ) {
			cpu1->addTechnology( game_g->findTechnology("Rocketry") );
			cpu1->addTechnology( game_g->findTechnology("Nuclear Missiles") );
		}
		if( index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_16 || index == TEST_CITYBUILD_20 || index == TEST_CITYBUILD_21 || index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
			cpu1->addTechnology( game_g->findTechnology("Bronze Working") );
		}
		if( index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 || index == TEST_CITYBUILD_19 ) {
			cpu1->addTechnology( game_g->findTechnology("Industrial Revolution") );
			cpu1->addTechnology( game_g->findTechnology("Motorised Warfare") );
		}
		if( index == TEST_CITYBUILD_15 ) {
			cpu1->addTechnology( game_g->findTechnology("Industrial Revolution") );
		}
		if( index == TEST_CITYBUILD_20 || index == TEST_CITYBUILD_21 ) {
			cpu1->addTechnology( game_g->findTechnology("Firearms") );
		}

		if( index == TEST_CITYBUILD_0 || index == TEST_CITYBUILD_15 ) {
		}
		else if( index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_22 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_CITYBUILD_11 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else if( index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_16 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			if( index == TEST_CITYBUILD_12 ) {
				unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
				new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			}
		}
		else if( index == TEST_CITYBUILD_20 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}
		else {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 5));
		}

		if( index == TEST_CITYBUILD_2 || index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_11 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_20 || index == TEST_CITYBUILD_21 ) {
			// if no settler units at all, with only 1 city, we'd give preferences to settlers
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
		}
		else if( index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
		}
		else if( index == TEST_CITYBUILD_24 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers"));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));
		}

		if( index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 ) {
			cpu2 = new Civilization( mainGamestate, game_g->findRace("The Vikings") );
			city = new City(mainGamestate, cpu2, "Enemy city", 5, 0);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(5, 0));
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(5, 2));
			Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
			relationship->makeContact();
			if( index == TEST_CITYBUILD_14 ) {
				relationship->setStatus(Relationship::STATUS_WAR);
			}
		}

		// city must be created after units, so we know if it's being defended!
		int size = 1;
		if( index == TEST_CITYBUILD_2 ) {
			size = size_limit_farmland_c;
			mainGamestate->setYear(100);
		}
		else if( index == TEST_CITYBUILD_5 || index == TEST_CITYBUILD_6 || index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 || index == TEST_CITYBUILD_17 || index == TEST_CITYBUILD_18 || index == TEST_CITYBUILD_19 ) {
			size = 2; // remove possibility of building peasants as "cheap unit" while growing
			mainGamestate->setYear(150); // prefer worker/attacker units to settlers
			if( index == TEST_CITYBUILD_17 ) {
				size = 3; // so we want to work all the city squares
			}
		}
		else if( index == TEST_CITYBUILD_8 ) {
			size = 25; // so we have enough production to consider rush-building nuclear missile
		}
		else if( index == TEST_CITYBUILD_11 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_16 || index == TEST_CITYBUILD_20 || index == TEST_CITYBUILD_21 ) {
			size = 3; // so we require 2 defenders instead of 1 (though depends on situation)
		}
		else if( index == TEST_CITYBUILD_15 ) {
			size = size_limit_farmland_c;
			mainGamestate->setYear(100);
		}
		else if( index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
			size = 2; // remove possibility of building peasants as "cheap unit" while growing
		}
		city = new City(mainGamestate, cpu1, "Washington", 5, 5, size);

		if( index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
			new City(mainGamestate, cpu1, "Boston", 10, 5, 1);
		}

		if( index == TEST_CITYBUILD_0 || index == TEST_CITYBUILD_8 || index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_11 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_15 || index == TEST_CITYBUILD_16 ) {
			//city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_MISSILELAUNCHER)); // needed to build nuclear missile!
			city->addImprovementTest(game_g->findImprovement("Missile Launcher")); // needed to build nuclear missile!
			if( index == TEST_CITYBUILD_15 ) {
				//city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FACTORY)); // needed to allow possibility of Modern Infantry
				city->addImprovementTest(game_g->findImprovement("Factory")); // needed to allow possibility of Modern Infantry
			}
		}
		else if( index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_19 ) {
			/*city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FORT));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_HARBOUR));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FARMLAND));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_SCHOOL));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FACTORY)); // needed to build tanks!
			*/
			city->addImprovementTest(game_g->findImprovement("Fort"));
			city->addImprovementTest(game_g->findImprovement("Quarry"));
			city->addImprovementTest(game_g->findImprovement("Harbour"));
			city->addImprovementTest(game_g->findImprovement("Farmland"));
			city->addImprovementTest(game_g->findImprovement("School"));
			city->addImprovementTest(game_g->findImprovement("Factory")); // needed to build tanks!
		}
		else if( index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 ) {
			/*city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FORT));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_HARBOUR));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FARMLAND));
			// leave school available to build
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FACTORY)); // needed to build tanks!
			*/
			city->addImprovementTest(game_g->findImprovement("Fort"));
			city->addImprovementTest(game_g->findImprovement("Quarry"));
			city->addImprovementTest(game_g->findImprovement("Harbour"));
			city->addImprovementTest(game_g->findImprovement("Farmland"));
			// leave school available to build
			city->addImprovementTest(game_g->findImprovement("Factory")); // needed to build tanks!
		}
		else if( index == TEST_CITYBUILD_17 ) {
			// so we don't build any improvements in "1st phase"
			/*city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FORT));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_HARBOUR));
			city->addImprovementTest(mainGamestate->findImprovement(IMPROVEMENT_FARMLAND));*/
			city->addImprovementTest(game_g->findImprovement("Fort"));
			city->addImprovementTest(game_g->findImprovement("Quarry"));
			city->addImprovementTest(game_g->findImprovement("Harbour"));
			city->addImprovementTest(game_g->findImprovement("Farmland"));
		}
		if( index == TEST_CITYBUILD_0 || index == TEST_CITYBUILD_1 || index == TEST_CITYBUILD_2 || index == TEST_CITYBUILD_3 || index == TEST_CITYBUILD_4 || index == TEST_CITYBUILD_5 || index == TEST_CITYBUILD_6 || index == TEST_CITYBUILD_7 || index == TEST_CITYBUILD_8 || index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_11 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 || index == TEST_CITYBUILD_15 || index == TEST_CITYBUILD_16 || index == TEST_CITYBUILD_17 || index == TEST_CITYBUILD_18 || index == TEST_CITYBUILD_20 || index == TEST_CITYBUILD_21 || index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
			city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Stone"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Bronze"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Iron"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Gunpowder"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Oil"), 200);
		}
		else if( index == TEST_CITYBUILD_19 ) {
			city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Stone"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Bronze"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Iron"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Gunpowder"), 200);
			city->setElementStocksTest(game_g->getGameData()->findElement("Oil"), 5);
		}
		city->setBuildable(NULL);
		city->doAI(); // should force choice again, to take into account improvements

	}
	else if( index == TEST_CITYSTOCKS_0 || index == TEST_CITYSTOCKS_1 || index == TEST_CITYSTOCKS_2 || index == TEST_CITYSTOCKS_3 ) {
		map = new Map(mainGamestate, 20, 20);
		map->setAllTo(TYPE_GRASSLAND);
		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Americans") );

		if( index == TEST_CITYSTOCKS_0 ) {
			cpu1->addTechnology( game_g->findTechnology("Steam Power") );
		}
		else if( index == TEST_CITYSTOCKS_3 ) {
			cpu1->addTechnology( game_g->findTechnology("Medieval Arms") );
		}

		city = new City(mainGamestate, cpu1, "city 0", 2, 2);
		if( index == TEST_CITYSTOCKS_0 || index == TEST_CITYSTOCKS_1 ) {
			city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 190);
		}
		else if( index == TEST_CITYSTOCKS_3 ) {
			city->setElementStocksTest(game_g->getGameData()->findElement("Iron"), 11);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		}

		city = new City(mainGamestate, cpu1, "city 1", 7, 2);
		if( index == TEST_CITYSTOCKS_0 || index == TEST_CITYSTOCKS_1 ) {
			city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 250);
		}
		else if( index == TEST_CITYSTOCKS_3 ) {
			city->setElementStocksTest(game_g->getGameData()->findElement("Iron"), 9);
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
			new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		}

		if( index == TEST_CITYSTOCKS_0 || index == TEST_CITYSTOCKS_1 ) {
			city = new City(mainGamestate, cpu1, "city 2", 2, 7);
			city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 0);

			city = new City(mainGamestate, cpu1, "city 3", 7, 7);
			city->setElementStocksTest(game_g->getGameData()->findElement("Wood"), 50);
		}
		else if( index == TEST_CITYSTOCKS_2 ) {
			cpu2 = new Civilization( mainGamestate, game_g->findRace("The Vikings") );
			cpu2->addTechnology( game_g->findTechnology("Combustion") );

			city = new City(mainGamestate, cpu2, "city 2", 2, 7);

			city = new City(mainGamestate, cpu2, "city 3", 7, 7);

			const BonusResource *oil = game_g->getGameData()->findBonusResource("Oil");
			mainGamestate->getMap()->getSquare(8, 2)->setType(TYPE_DESERT);
			mainGamestate->getMap()->getSquare(8, 2)->setBonusResource(oil);
			mainGamestate->getMap()->getSquare(8, 7)->setType(TYPE_ARTIC);
			mainGamestate->getMap()->getSquare(8, 7)->setBonusResource(oil);
		}
		else if( index == TEST_CITYSTOCKS_3 ) {
		}
		else {
			ASSERT(false);
		}
	}
	else if( index == TEST_CITY_0 || index == TEST_CITY_1 || index == TEST_CITY_2 ) {
		map = new Map(mainGamestate, 20, 20);
		map->setAllTo(TYPE_GRASSLAND);
		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Americans") );

		city = new City(mainGamestate, cpu1, "city 0", 2, 2);
		city->addImprovementTest(game_g->findImprovement("Temple"));

		city = new City(mainGamestate, cpu1, "city 1", 7, 2);
		city->addImprovementTest(game_g->findImprovement("Church"));

		city = new City(mainGamestate, cpu1, "city 2", 2, 7);
		city->addImprovementTest(game_g->findImprovement("Temple"));
		city->addImprovementTest(game_g->findImprovement("Church"));

		if( index == TEST_CITY_1 ) {
			cpu1->addTechnology(game_g->findTechnology("Feudalism"));
		}
		else if( index == TEST_CITY_2 ) {
			cpu1->addTechnology(game_g->findTechnology("Feudalism"));
			cpu1->addTechnology(game_g->findTechnology("Scientific Method"));
		}
	}
	else if( index == TEST_AIPERF_0 || index == TEST_AIPERF_1 || index == TEST_AIPERF_2 || index == TEST_AIPERF_3 || index == TEST_AIPERF_4 || index == TEST_AIPERF_8 || index == TEST_AIPERF_9 || index == TEST_AIPERF_10 || index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 || index == TEST_AIPERF_17 ) {
		map = new Map(mainGamestate, 100, 40);
		if( index == TEST_AIPERF_17 ) {
			map->setAllTo(TYPE_OCEAN);
		}
		mainGamestate->setMap(map);
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Americans") );
		cpu1->revealMap();
		cpu2 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
		if( index == TEST_AIPERF_2 || index == TEST_AIPERF_10 || index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 || index == TEST_AIPERF_17 ) {
			cpu3 = new Civilization( mainGamestate, game_g->findRace("The French") );
		}

		if( index == TEST_AIPERF_0 || index == TEST_AIPERF_4 || index == TEST_AIPERF_8 ) {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
		}
		else {
			unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Horsemen"));
		}
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(4, 4));
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		if( index != TEST_AIPERF_8 )
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(97, 4)); // so that we can see the enemy Settlers unit (fog of war)
		//if( index != TEST_AIPERF_8 && index != TEST_AIPERF_9 && index != TEST_AIPERF_10 )
		if( index == TEST_AIPERF_0 || index == TEST_AIPERF_1 || index == TEST_AIPERF_2 || index == TEST_AIPERF_3 || index == TEST_AIPERF_4 || index == TEST_AIPERF_5 || index == TEST_AIPERF_6 || index == TEST_AIPERF_7 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 )
			new Unit(mainGamestate, cpu1, unit_template, Pos2D(80, 3)); // so that we can see the enemy "blocking" unit (fog of war)

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Settlers")); // must be a unit we are guaranteed to defeat
		if( index == TEST_AIPERF_8 )
			new City(mainGamestate, cpu2, "", 96, 4);
		else if( index == TEST_AIPERF_12 || index == TEST_AIPERF_15 ) {
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(96, 4));
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(80, 4));
		}
		else if( index == TEST_AIPERF_13 || index == TEST_AIPERF_16 ) {
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(80, 4));
		}
		else
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(96, 4));
		if( index == TEST_AIPERF_1 || index == TEST_AIPERF_3 || index == TEST_AIPERF_9 ) {
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(80, 4));
		}
		else if( index == TEST_AIPERF_2 || index == TEST_AIPERF_10 || index == TEST_AIPERF_17 ) {
			new Unit(mainGamestate, cpu3, unit_template, Pos2D(80, 4));
		}
		else if( index == TEST_AIPERF_8 ) {
			new Unit(mainGamestate, cpu2, unit_template, Pos2D(80, 3));
		}
		else if( index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 ) {
			new City(mainGamestate, cpu3, "Paris", 80, 2);
		}
		for(int x=4;x<=96;x++) {
			if( index == TEST_AIPERF_17 ) {
				map->getSquare(x, 4)->setType(TYPE_GRASSLAND);
			}
			map->getSquare(x, 4)->setRoad(ROAD_RAILWAYS);
		}
		if( index == TEST_AIPERF_17 ) {
			map->getSquare(97, 4)->setType(TYPE_GRASSLAND);
		}

		map->createAlphamap();

		Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
		relationship->makeContact();
		relationship->setStatus(Relationship::STATUS_WAR);
		if( index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 ) {
			relationship = mainGamestate->findRelationship(cpu1, cpu3);
			relationship->makeContact();
			if( index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 ) {
				const Technology *technology = game_g->findTechnology("Writing");
				cpu1->addTechnology(technology);
				cpu3->addTechnology(technology);
				relationship->setRightOfPassage(true);
			}
		}
	}
	else if( index == TEST_AIPERF_5 || index == TEST_AIPERF_6 || index == TEST_AIPERF_7 ) {
		map = new Map(mainGamestate, 50, 20);
		for(int y=0;y<map->getHeight();y++) {
			for(int x=0;x<map->getWidth();x++) {
				if( x == 0 || x == map->getWidth()-1 || y == 0 || y == map->getHeight()-1 ) {
					map->getSquare(x, y)->setType(TYPE_OCEAN);
				}
			}
		}

		mainGamestate->setMap(map);
		map->createAlphamap();
		mainGamestate->createRebelCiv();
		ASSERT( mainGamestate->getRebelCiv() != NULL );

		cpu1 = new Civilization( mainGamestate, game_g->findRace("The Americans") );
		if( index == TEST_AIPERF_5 || index == TEST_AIPERF_6 ) {
			cpu1->revealMap();
		}
		cpu1->addTechnology( game_g->findTechnology("Sailing") );
		cpu1->addTechnology( game_g->findTechnology("Horse Riding") );
		cpu1->addTechnology( game_g->findTechnology("Mathematics") );
		cpu1->addTechnology( game_g->findTechnology("Agriculture") );
		cpu1->addTechnology( game_g->findTechnology("Bronze Working") );

		city = new City(mainGamestate, cpu1, "Washington", 1, 1, 3);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());

		city = new City(mainGamestate, cpu1, "New York", 14, 1, 1);

		city = new City(mainGamestate, cpu1, "Boston", 7, 2, 2);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());

		city = new City(mainGamestate, cpu1, "Los Angeles", 1, 6, 3);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());

		city = new City(mainGamestate, cpu1, "Seattle", 6, 7, 5);
		city->addImprovementTest(game_g->findImprovement("Farmland"));
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());
		new Unit(mainGamestate, cpu1, unit_template, city->getPos());

		// add units

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Peasants"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(3, 1));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(5, 4));

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Cuirassiers"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(4, 2));

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(6, 2));
		new Unit(mainGamestate, cpu1, unit_template, Pos2D(3, 6));

		cpu2 = new Civilization( mainGamestate, game_g->findRace("The Romans") );

		city = new City(mainGamestate, cpu2, "Rome", 11, 6, 3);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu2, unit_template, city->getPos());
		new Unit(mainGamestate, cpu2, unit_template, city->getPos());

		city = new City(mainGamestate, cpu2, "Pompeii", 14, 11, 2);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu2, unit_template, city->getPos());
		new Unit(mainGamestate, cpu2, unit_template, city->getPos());

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
		new Unit(mainGamestate, cpu2, unit_template, Pos2D(3, 3));

		Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
		relationship->makeContact();
		relationship->setStatus(Relationship::STATUS_WAR);

		cpu3 = new Civilization( mainGamestate, game_g->findRace("The English") );

		city = new City(mainGamestate, cpu3, "London", 1, 14, 3);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu3, unit_template, city->getPos());
		new Unit(mainGamestate, cpu3, unit_template, city->getPos());

		city = new City(mainGamestate, cpu3, "Cambridge", 6, 12, 3);
		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Spearmen"));
		new Unit(mainGamestate, cpu3, unit_template, city->getPos());
		new Unit(mainGamestate, cpu3, unit_template, city->getPos());

		unit_template = static_cast<const UnitTemplate *>(game_g->findBuildable("Catapults"));
		new Unit(mainGamestate, cpu3, unit_template, Pos2D(5, 11));

		mainGamestate->setYear(200); // better to test performance for later stages, when turns are slower due to more cities/units
	}
	else if( index == TEST_DISTANCES_0 || index == TEST_DISTANCES_1 ) {
		map = new Map(mainGamestate);
		if( !map->readFile(getFullPath("data/test_world0.map").c_str()) ) {
			VI_log("failed to read test map file\n");
			ASSERT(false);
		}
		else {
			mainGamestate->setMap(map);
			mainGamestate->createRebelCiv();
			ASSERT( mainGamestate->getRebelCiv() != NULL );
			cpu1 = new Civilization( mainGamestate, game_g->findRace("The Romans") );
			cpu1->revealMap(); // important! so we calculate the entire distance map
			if( index == TEST_DISTANCES_1 ) {
				for(int y=0;y<map->getHeight();y++) {
					for(int x=0;x<map->getWidth();x++) {
						MapSquare *square = map->getSquare(x, y);
						if( square->isLand() ) {
							Road roads[9] = {ROAD_NONE, ROAD_RAILWAYS, ROAD_NONE, ROAD_BASIC, ROAD_BASIC, ROAD_RAILWAYS, ROAD_NONE, ROAD_RAILWAYS, ROAD_BASIC};
							int indx = ( y * y + 3 * x - 4 ) % 9;
							Road road = roads[indx];
							square->setRoad(road);
						}
					}
				}
			}
			// to initialise threads before we do the timing
			/*if( index == TEST_DISTANCES_1 ) {
				int x = 0, y = 0;
				const MapSquare *square = map->getSquare(x, y);
				Distance *dists = map->calculateDistanceMap(cpu1, NULL, false, true, x, y, -1, -1, !square->isLand());
				delete [] dists;
			}*/
		}
	}
	else {
		ASSERT( false );
	}

	if( mainGamestate->getMap() != NULL ) {
		mainGamestate->generateTerrain();
	}

	// now need to generate terrain, after creating the map!
	/*if( view_3d && map != NULL ) {
		mainGamestate->generateTerrain();
	}*/

	//generateRelationships();
	if( !mainGamestate->checkRelationships() ) {
		//mainGamestate->clear();
		return;
	}
	mainGamestate->beginTurn();

	for(size_t i=0;i<mainGamestate->getNCivilizations();i++) {
		Civilization *civilization = mainGamestate->getCivilization(i);
		civilization->resetAI();
		civilization->calculateFogOfWar(false);
	}

	//const bool show = false;
	const bool show = true;
	if( show ) {
		stringstream text;
		text << "Starting test: " << (int)index;
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, 256, 128);
		window->doModal();
		delete window;
	}

	FILE *file = fopen(filename, "a+");
	ASSERT(file != NULL);

	// do the test
	bool test_result = true;
	int time_s = game_g->getGraphicsEnvironment()->getRealTimeMS();
	bool perf_test = false;
	try {
		if( index == TEST_SETTLERS_MOVE_0 || index == TEST_SETTLERS_MOVE_1 || index == TEST_SETTLERS_MOVE_2 || index == TEST_SETTLERS_MOVE_3 || index == TEST_SETTLERS_MOVE_4 || index == TEST_MAKE_CONTACT_0 || index == TEST_MAKE_CONTACT_1 || index == TEST_FOW_0 || index == TEST_FOW_3 || index == TEST_FOW_4 ) {
			// Tests making contact between adjacent Civs at start, and AI for Settlers moving away from enemies.
			// Tests 0 and 1 are similar, but test 1 has no nearby City, so checks that the AI Settler doesn't build a City right next to the enemy.
			Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
			ASSERT( relationship != NULL );
			// check we've made contact or not, as expected
			if( index == TEST_MAKE_CONTACT_0 ) {
				if( !relationship->madeContact() ) {
					throw "Should have set contact flag between civs";
				}
				cpu2->revealMap();
			}
			else if( index == TEST_MAKE_CONTACT_1 || index == TEST_FOW_0 || index == TEST_FOW_3 || index == TEST_FOW_4 ) {
				if( relationship->madeContact() ) {
					throw "Shouldn't have yet set contact flag between civs";
				}
				cpu2->revealMap();
			}
			else {
				if( !relationship->madeContact() ) {
					throw "Failed to set contact flag between civs with adjacent units";
				}
			}
			if( index != TEST_SETTLERS_MOVE_2 && index != TEST_MAKE_CONTACT_0 && index != TEST_MAKE_CONTACT_1 && index != TEST_FOW_0 && index != TEST_FOW_3 && index != TEST_FOW_4 )
				relationship->setStatus(Relationship::STATUS_WAR);
			Unit *unit = cpu2->getUnit(0);
			if( index == TEST_SETTLERS_MOVE_4 )
				unit->setStatus(Unit::STATUS_BUILDING_ROAD);
			Pos2D old_pos = unit->getPos();
			unit->doAI();
			if( index == TEST_SETTLERS_MOVE_0 || index == TEST_SETTLERS_MOVE_1 || index == TEST_SETTLERS_MOVE_4 ) {
				// expect unit to move away
				// does the unit still exist?
				if( cpu1->getNUnits() != 1 || cpu2->getNUnits() != 1 ) {
					throw "Expected 1 unit on each civ";
				}
				Pos2D pos = unit->getPos();
				VI_log("unit moved to %d, %d\n", pos.x, pos.y);
				Unit *unit1 = cpu1->getUnit(0);
				Pos2D pos1 = unit1->getPos();
				if( abs(pos.x - pos1.x) <= 1 && abs(pos.y - pos1.y) <= 1 ) {
					throw "AI Settlers should move away from enemy";
				}
			}
			else if( index == TEST_SETTLERS_MOVE_2 || index == TEST_SETTLERS_MOVE_3 ) {
				// expect unit to build new city
				if( cpu1->getNUnits() != 1 || cpu2->getNUnits() != 0 ) {
					throw "Unexpected number of units for each civ";
				}
				else if( map->getSquare(6, 6)->getCity() == NULL )
					throw "No city here";
			}
			else if( index == TEST_MAKE_CONTACT_0 ) {
				/*
				// expected to move towards other civ, to make contact
				// does the unit still exist?
				if( cpu1->getNUnits() != 1 || cpu2->getNUnits() != 1 ) {
					throw "Expected 1 unit on each civ";
				}
				Pos2D pos = unit->getPos();
				VI_log("unit moved to %d, %d\n", pos.x, pos.y);
				if( pos.x != 5 ) {
					throw "AI should move towards other civ to make contact";
				}
				else if( !relationship->madeContact() ) {
					throw "Failed to set contact flag between civs with adjacent units";
				}*/
				// now that we make contact via territory, there's no need for the unit to move to make contact
				// expected to retreat towards city, as unit is obscured by fog of war
				// does the unit still exist?
				if( cpu1->getNUnits() != 1 || cpu2->getNUnits() != 1 ) {
					throw "Expected 1 unit on each civ";
				}
				Pos2D pos = unit->getPos();
				VI_log("unit moved to %d, %d\n", pos.x, pos.y);
				if( pos != Pos2D(6, 5)) {
					throw "AI should retreat to city";
				}
			}
			else if( index == TEST_FOW_3 || index == TEST_FOW_4 ) {
				// expected to move towards other civ, to make contact
				// does the unit still exist?
				if( cpu1->getNUnits() != 1 || cpu2->getNUnits() != 1 ) {
					throw "Expected 1 unit on each civ";
				}
				Pos2D pos = unit->getPos();
				VI_log("unit moved to %d, %d\n", pos.x, pos.y);
				if( pos.y != 7 ) {
					throw "AI should move towards other civ to make contact";
				}
				// won't have made contact yet
				if( relationship->madeContact() ) {
					throw "Shouldn't have moved close enough to set contact";
				}
			}
			else if( index == TEST_MAKE_CONTACT_1 ) {
				// expected to move towards other civ, to make contact
				// does the unit still exist?
				if( cpu2->getNUnits() != 1 ) {
					throw "Expected 1 unit on second civ";
				}
				Pos2D pos = unit->getPos();
				VI_log("unit moved to %d, %d\n", pos.x, pos.y);
				if( pos.y != 8 ) {
					throw "AI should move towards other civ to make contact";
				}
				else if( !relationship->madeContact() ) {
					throw "Failed to set contact flag between civs with adjacent units";
				}
			}
			else if( index == TEST_FOW_0 ) {
				// expected to retreat towards city, as unit is obscured by fog of war
				// does the unit still exist?
				if( cpu1->getNUnits() != 1 || cpu2->getNUnits() != 1 ) {
					throw "Expected 1 unit on each civ";
				}
				Pos2D pos = unit->getPos();
				VI_log("unit moved to %d, %d\n", pos.x, pos.y);
				if( pos != Pos2D(6, 5)) {
					throw "AI should retreat to city";
				}
			}
			else {
				ASSERT( false );
			}
		}
		else if( index == TEST_SETTLERS_MOVE_5 || index == TEST_SETTLERS_MOVE_6 || index == TEST_SETTLERS_MOVE_7 || index == TEST_SETTLERS_MOVE_8 || index == TEST_SETTLERS_MOVE_9 ) {
			Unit *unit = cpu1->getUnit(0);
			while( true ) {
				bool moved = unit->doAI();
				// unit may have been destroyed if moved!
				if( cpu1->getNUnits() > 0 ) {
					Pos2D new_pos = unit->getPos();
					VI_log("unit now at %d, %d\n", new_pos.x, new_pos.y);
				}
				if( index == TEST_SETTLERS_MOVE_5 || index == TEST_SETTLERS_MOVE_7 ) {
					if( moved ) {
						throw "Didn't expect settlers to move";
					}
					break;
				}
				else if( index == TEST_SETTLERS_MOVE_6 || index == TEST_SETTLERS_MOVE_8 || index == TEST_SETTLERS_MOVE_9 ) {
					if( moved ) {
						if( cpu1->getNUnits() == 0 ) {
							// built city?
							if( cpu1->getNCities() != 2 ) {
								throw "Settlers destroyed, but no new city?!";
							}
							else {
								break;
							}
						}
						else {
							if( index == TEST_SETTLERS_MOVE_8 ) {
								// no railways, so need to reset moves each turn
								unit->reset();
							}
							else if( index == TEST_SETTLERS_MOVE_9 ) {
								// moving through enemy territory, so need to reset moves each turn
								unit->reset();
							}
							else if( !unit->hasMovesLeft() ) {
								// ROP agreement, so can use railways in other civ's territory
								throw "Didn't expect to run out of moves";
							}

							if( index == TEST_SETTLERS_MOVE_8 && map->getSquare( unit->getPos() )->getTerritory() == cpu2 ) {
								// if at war, shouldn't be moving into enemy territory, even though it's the more direct route
								throw "Didn't expect to enter enemy territory";
							}
						}
					}
					else {
						throw "Failed to move settlers unit";
					}
				}
			}
		}
		else if( index == TEST_TRAVEL_INVADE_0 || index == TEST_TRAVEL_INVADE_1 || index == TEST_TRAVEL_INVADE_2 || index == TEST_TRAVEL_INVADE_3 || index == TEST_TRAVEL_INVADE_4 || index == TEST_TRAVEL_INVADE_5 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_7 || index == TEST_TRAVEL_INVADE_8 || index == TEST_TRAVEL_INVADE_9 ) {
			City *city = cpu1->getCity(0);
			Unit *unit = cpu1->getUnit(0);
			city->initTravel(false, unit->getTemplate());

			bool target1 = mainGamestate->getMap()->getSquare(10, 4)->isTarget();
			bool target2 = mainGamestate->getMap()->getSquare(10, 5)->isTarget();
			VI_log("target1 = %d\n", target1);
			VI_log("target2 = %d\n", target2);

			bool exp_target1 = true, exp_target2 = true;
			if( index == TEST_TRAVEL_INVADE_1 || index == TEST_TRAVEL_INVADE_3 )
				exp_target2 = false;

			if( target1 != exp_target1 ) {
				throw "Unexpected target1";
			}
			else if( target2 != exp_target2 ) {
				throw "Unexpected target1";
			}

			if( index != TEST_TRAVEL_INVADE_7 && index != TEST_TRAVEL_INVADE_8 ) {
				for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
					for(int x=0;x<mainGamestate->getMap()->getWidth();x++) {
						if( x == 10 && y == 4 )
							continue;
						else if( x == 10 && y == 5 )
							continue;
						if( mainGamestate->getMap()->getSquare(x, y)->isTarget() ) {
							VI_log("Didn't expect %d, %d to be a target\n", x, y);
							throw "Didn't expect target";
						}
					}
				}
			}

			mainGamestate->getMap()->resetTargets();

			Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
			ASSERT( relationship != NULL );
			relationship->makeContact();
			relationship->setStatus(Relationship::STATUS_WAR);

			size_t before_n_units = cpu1->getNUnits() + cpu2->getNUnits();
			bool exp_move = false;
			if( index == TEST_TRAVEL_INVADE_0 || index == TEST_TRAVEL_INVADE_1 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_7 || index == TEST_TRAVEL_INVADE_8 || index == TEST_TRAVEL_INVADE_9 )
				exp_move = true;
			if( unit->doAI() != exp_move ) {
				throw "Unexpected unit move";
			}
			Pos2D pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", pos.x, pos.y);
			size_t after_n_units = cpu1->getNUnits() + cpu2->getNUnits();
			if( ( index == TEST_TRAVEL_INVADE_0 || index == TEST_TRAVEL_INVADE_6 || index == TEST_TRAVEL_INVADE_8 || index == TEST_TRAVEL_INVADE_9 ) && before_n_units - after_n_units != 1 ) {
				throw "Expected to destroy one of the units";
			}
			else if( ( index == TEST_TRAVEL_INVADE_1 || index == TEST_TRAVEL_INVADE_2 || index == TEST_TRAVEL_INVADE_3 || index == TEST_TRAVEL_INVADE_4 || index == TEST_TRAVEL_INVADE_5 || index == TEST_TRAVEL_INVADE_7 ) && before_n_units != after_n_units ) {
				throw "Expected to not destroy any units";
			}
			if( index == TEST_TRAVEL_INVADE_7 && pos != Pos2D(6, 4) ) {
				throw "Unexpected position - expected to invade via land route";
			}
		}
		else if( index == TEST_TRAVEL_INVADE_10 || index == TEST_TRAVEL_INVADE_11 ) {
			Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
			ASSERT( relationship != NULL );
			relationship->makeContact();
			relationship->setStatus(Relationship::STATUS_WAR);

			size_t n_old_units = cpu1->getNUnits();
			for(size_t i=0;i<cpu1->getNUnits();i++) {
				Unit *unit = cpu1->getUnit(i);
				unit->doAI();
			}
			size_t n_new_units = cpu1->getNUnits();

			if( n_old_units != n_new_units ) {
				throw "Didn't expect units to be destroyed";
			}
			bool found_tank = false;
			Pos2D tank_pos;
			for(size_t i=0;i<cpu1->getNUnits();i++) {
				Unit *unit = cpu1->getUnit(i);
				if( unit->getTemplate()->equals("Infantry") ) {
					const MapSquare *square = map->getSquare(unit->getPos());
					if( square->getCity() == NULL ) {
						throw "Infantry should have stayed to defend city";
					}
				}
				else if( unit->getTemplate()->equals("Tanks") ) {
					Pos2D pos = unit->getPos();
					VI_log("%d th unit (tanks) moves to %d, %d\n", i, pos.x, pos.y);
					if( !found_tank ) {
						found_tank = true;
						tank_pos = pos;
					}
					else if( tank_pos != pos ) {
						throw "Expected all tanks to invade at same square";
					}
					else if( index == TEST_TRAVEL_INVADE_10 && !( pos.x >= 7 && pos.x <= 9 ) ) {
						throw "Unexpected position for tanks";
					}
					else if( index == TEST_TRAVEL_INVADE_11 && !( pos.x >= 12 && pos.x <= 14 ) ) {
						throw "Unexpected position for tanks";
					}
				}
				else {
					throw "Unexpected unit type";
				}
			}
		}
		else if( index == TEST_MISC_0 ) {
			Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
			ASSERT( relationship != NULL );
			if( relationship->madeContact() ) {
				throw "Didn't expect to have made contact yet";
			}

			Unit *unit = cpu1->getUnit(0);
			unit->doAI();
			if( cpu1->getNUnits() != 0 ) {
				throw "Still has a unit - expected to build a city";
			}
			if( cpu1->getNCities() != 1 ) {
				throw "Expected to build a city";
			}

			if( !relationship->madeContact() ) {
				throw "Expected to have made contact now";
			}

			unit = cpu2->getUnit(0);
			Pos2D old_pos = unit->getPos();
			if( !unit->doAI()) {
				throw "Expected unit to do something";
			}
			Pos2D new_pos = unit->getPos();
			if( old_pos == new_pos ) {
				throw "Expected unit to move";
			}
		}
		else if( index == TEST_REBEL_0 || index == TEST_REBEL_1 || index == TEST_REBEL_2 ) {
			Unit *unit = NULL;
			if( index == TEST_REBEL_0 || index == TEST_REBEL_1 ) {
				unit = cpu1->getUnit(0);
			}
			else {
				unit = mainGamestate->getRebelCiv()->getUnit(0);
			}
			Pos2D old_pos = unit->getPos();
			bool moved = unit->doAI();
			Pos2D new_pos = unit->getPos();
			if( index == TEST_REBEL_0 ) {
				if( !moved ) {
					throw "Expected unit to do something";
				}
				else if( new_pos != Pos2D(6, 6) ) {
					throw "Expected unit to move to attack the rebels";
				}
			}
			else if( index == TEST_REBEL_1 ) {
				if( moved ) {
					throw "Expected unit to do nothing";
				}
				else if( new_pos != old_pos ) {
					throw "Expected unit to be in same place";
				}
			}
			else if( index == TEST_REBEL_2 ) {
				if( !moved ) {
					throw "Expected rebel unit to do something";
				}
				else if( new_pos != Pos2D(31, 31) ) {
					throw "Expected rebel unit to move to attack the city";
				}
			}
		}
		else if( index == TEST_ATTACK_CITY || index == TEST_ATTACK_UNIT ) {
			// Tests attacking an enemy City/Unit.
			// Same map as test 0/1, but we do AI on the other Civ.
			Relationship *relationship = mainGamestate->findRelationship(cpu1, cpu2);
			// check we've made contact (as adjacent units)
			ASSERT( relationship != NULL );
			if( !relationship->madeContact() ) {
				throw "Failed to set contact flag between civs with adjacent units";
			}
			relationship->setStatus(Relationship::STATUS_WAR); // so the Peasants should attack
			Unit *unit = cpu1->getUnit(0);
			unit->doAI();
			if( index == TEST_ATTACK_CITY ) {
				// expected the city to be destroyed
				if( cpu1->getNUnits() != 1 || cpu2->getNUnits() != 1 ) {
					throw "Expected both units to remain";
				}
				else if( cpu1->getNCities() != 0 || cpu2->getNCities() != 0 ) {
					throw "Expected city to be destroyed";
				}
			}
			else if( index == TEST_ATTACK_UNIT ) {
				// expected a unit to be defeated
				if( cpu1->getNUnits() + cpu2->getNUnits() != 1 ) {
					throw "Expected only 1 unit remaining in total";
				}
			}
			else {
				ASSERT( false );
			}
		}
		else if( index == TEST_TRAVEL_EXPLORE_0 || index == TEST_TRAVEL_EXPLORE_1 || index == TEST_TRAVEL_EXPLORE_2 || index == TEST_TRAVEL_EXPLORE_3 ) {
			// Tests using the harbour to explore
			Unit *unit = cpu1->getUnit(0);
			unit->doAI();
			if( unit->hasMovesLeft() ) {
				unit->doAI();
			}
			Pos2D pos = unit->getPos();
			VI_log("unit moved to %d, %d\n", pos.x, pos.y);
			if( ( index == TEST_TRAVEL_EXPLORE_0 || index == TEST_TRAVEL_EXPLORE_1 || index == TEST_TRAVEL_EXPLORE_3 ) && pos.y < 8 ) {
				throw "AI should have moved to other island";
			}
			else if( index == TEST_TRAVEL_EXPLORE_2 && pos.y != 4 ) {
				throw "AI should have moved south towards city with harbour";
			}
			int offx = 0; // n.b., also defined above!
			if( index == TEST_TRAVEL_EXPLORE_3 )
				offx = 990;
			else
				offx = 4;
			if( pos.x == offx+1 && pos.y == 9 ) {
				throw "How did AI move to here?";
			}
			/*if( abs(pos.x - pos1.x) <= 1 && abs(pos.y - pos1.y) <= 1 ) {
			throw "AI Settlers should move away from enemy";
			}*/
		}
		else if( index == TEST_SETTLER_BUILDCITY_0 || index == TEST_SETTLER_BUILDCITY_1 || index == TEST_SETTLER_BUILDCITY_2 || index == TEST_SETTLER_BUILDCITY_3 || index == TEST_SETTLER_BUILDCITY_4 || index == TEST_SETTLER_BUILDCITY_5 || index == TEST_SETTLER_BUILDCITY_6 || index == TEST_SETTLER_BUILDCITY_7 || index == TEST_SETTLER_BUILDCITY_8 || index == TEST_SETTLER_BUILDCITY_9 || index == TEST_SETTLER_BUILDCITY_10 || index == TEST_SETTLER_BUILDCITY_11 || index == TEST_SETTLER_BUILDCITY_12 || index == TEST_SETTLER_BUILDCITY_13 || index == TEST_SETTLER_BUILDCITY_14 || index == TEST_SETTLER_BUILDCITY_15 || index == TEST_SETTLER_BUILDCITY_16 ) {
			// Tests moving Settlers to a suitable City location
			Unit *unit = cpu1->getUnit(0);
			if( index == TEST_SETTLER_BUILDCITY_9 ) {
				// extra check
				if( mainGamestate->getMap()->getSquare( unit->getPos() )->canBuildCity() ) {
					throw "Didn't expect to be able to build a city here";
				}
				// also check some further squares
				for(int x=6;x<=12;x++) {
					for(int y=0;y<mainGamestate->getMap()->getHeight();y++) {
						if( !mainGamestate->getMap()->getSquare(x, y)->canBuildCity() ) {
							VI_log("failed check at %d, %d\n", x, y);
							throw "Expected to be able to build a city here";
						}
					}
				}
				for(int x=13;x<=21;x++) {
					for(int y=3;y<=7;y++) {
						if( mainGamestate->getMap()->getSquare(x, y)->canBuildCity() ) {
							VI_log("failed check at %d, %d\n", x, y);
							throw "Didn't expect to be able to build a city here";
						}
					}
				}
			}
			else if( index == TEST_SETTLER_BUILDCITY_15 || index == TEST_SETTLER_BUILDCITY_16 ) {
			}
			else {
				// extra check
				if( !mainGamestate->getMap()->getSquare( unit->getPos() )->canBuildCity() ) {
					throw "Expected to be able to build a city here";
				}
			}
			if( index == TEST_SETTLER_BUILDCITY_15 || index == TEST_SETTLER_BUILDCITY_16 ) {
				int count = 0;
				while( cpu1->getNUnits() > 0 ) {
					count++;
					unit->reset();
					Pos2D old_pos = unit->getPos();
					unit->doAI();
					// n.b., unit will have been deleted when city is built!
					if( cpu1->getNUnits() > 0 ) {
                        Pos2D new_pos = unit->getPos();
                        VI_log("moved to %d, %d\n", new_pos.x, new_pos.y);
                        if( old_pos == new_pos ) {
                            throw "Expected unit to move";
                        }
					}
				}
				if( cpu1->getNCities() != 2 ) {
					throw "Expected 2 cities now";
				}
				else if( count != 5 ) {
					throw "Settlers should have moved exactly 5 squares to build new city";
				}
			}
			else {
				unit->doAI();
				if( cpu1->getNUnits() == 0 ) {
					VI_log("built city\n");
				}
				else {
					Pos2D pos = cpu1->getUnit(0)->getPos();
					VI_log("moved to %d, %d\n", pos.x, pos.y);
				}
				if( index == TEST_SETTLER_BUILDCITY_0 ) {
					// Coastal grasslands - ideal spot
					if( cpu1->getNUnits() > 0 )
						throw "Should have built city here";
					else if( mainGamestate->getMap()->getSquare(4, 6)->getCity() == NULL )
						throw "No city here";
				}
				else if( index == TEST_SETTLER_BUILDCITY_1 ) {
					// Prefer coastal to non-coastal
					if( cpu1->getNUnits() != 1 )
						throw "Expected 1 unit";
					else if( unit->getPos() != Pos2D(4, 6) )
						throw "Move to unexpected location";
				}
				else if( index == TEST_SETTLER_BUILDCITY_2 ) {
					// Prefer grasslands to non-grasslands, even if the non-grasslands have coastal advantage
					if( cpu1->getNUnits() != 1 )
						throw "Expected 1 unit";
					else if( unit->getPos() != Pos2D(5, 5) )
						throw "Move to unexpected location";
				}
				else if( index == TEST_SETTLER_BUILDCITY_3 ) {
					// Non-coastal grasslands - better than coastal non-grasslands
					if( cpu1->getNUnits() > 0 )
						throw "Should have built city here";
					else if( mainGamestate->getMap()->getSquare(5, 5)->getCity() == NULL )
						throw "No city here";
				}
				else if( index == TEST_SETTLER_BUILDCITY_4 ) {
					// Prefer coastal to non-coastal, even when all are deserts
					if( cpu1->getNUnits() != 1 )
						throw "Expected 1 unit";
					else if( unit->getPos() == Pos2D(5, 5) )
						throw "Move to unexpected location";
				}
				else if( index == TEST_SETTLER_BUILDCITY_5 ) {
					// All grasslands, no coast, build city
					if( cpu1->getNUnits() > 0 )
						throw "Should have built city here";
					else if( mainGamestate->getMap()->getSquare(5, 5)->getCity() == NULL )
						throw "No city here";
				}
				else if( index == TEST_SETTLER_BUILDCITY_6 ) {
					// All desert, no coast, build city
					if( cpu1->getNUnits() > 0 )
						throw "Should have built city here";
					else if( mainGamestate->getMap()->getSquare(5, 5)->getCity() == NULL )
						throw "No city here";
				}
				else if( index == TEST_SETTLER_BUILDCITY_7 ) {
					// Prefer coastal to non-coastal
					if( cpu1->getNUnits() != 1 )
						throw "Expected 1 unit";
					else if( unit->getPos() != Pos2D(4, 6) )
						throw "Move to unexpected location";
				}
				else if( index == TEST_SETTLER_BUILDCITY_8 ) {
					// prefer forest, build city
					if( cpu1->getNUnits() > 0 )
						throw "Should have built city here";
					else if( mainGamestate->getMap()->getSquare(5, 5)->getCity() == NULL )
						throw "No city here";
				}
				else if( index == TEST_SETTLER_BUILDCITY_9 ) {
					// can't build city here
					if( cpu1->getNUnits() != 1 )
						throw "Expected 1 unit";
					else if( unit->getPos().x < 14 || unit->getPos().x > 15 )
						throw "Move to unexpected location";
				}
				else if( index == TEST_SETTLER_BUILDCITY_10 || index == TEST_SETTLER_BUILDCITY_11 || index == TEST_SETTLER_BUILDCITY_12 ) {
					// expected to move north to build city
					if( cpu1->getNUnits() != 1 )
						throw "Expected 1 unit";
					else if( unit->getPos().y != 4 )
						throw "Move to unexpected location";
					unit->reset();
					Pos2D pos = unit->getPos();
					unit->doAI();
					if( cpu1->getNUnits() > 0 )
						throw "Should have built city here";
					else if( mainGamestate->getMap()->getSquare(pos)->getCity() == NULL )
						throw "No city here";
				}
				else if( index == TEST_SETTLER_BUILDCITY_13 || index == TEST_SETTLER_BUILDCITY_14 ) {
					// expected to move away to build city
					if( cpu1->getNUnits() != 1 )
						throw "Expected 1 unit";
					else if( unit->getPos() == Pos2D(5, 5) )
						throw "Expected unit to move";
					unit->reset();
					Pos2D pos = unit->getPos();
					unit->doAI();
					if( cpu1->getNUnits() > 0 )
						throw "Should have built city here";
					else if( mainGamestate->getMap()->getSquare(pos)->getCity() == NULL )
						throw "No city here";
				}
				else {
					ASSERT( false );
				}
			}
		}
		else if( index == TEST_DEFEND_0 || index == TEST_DEFEND_1 ) {
			Unit *unit = NULL;
			if( index == TEST_DEFEND_0 )
				unit = cpu1->getUnit(0); // spearmen
			else if( index == TEST_DEFEND_1 )
				unit = cpu1->getUnit(2); // knights
			else {
				ASSERT( false );
			}
			unit->doAI();
			if( index == TEST_DEFEND_0 ) {
				// expect to defend city
				if( unit->getPos() != Pos2D(5, 5) ) {
					throw "Unit should have defended city";
				}
			}
			else if( index == TEST_DEFEND_1 ) {
				// expect to leave city to explore
				if( unit->getPos() == Pos2D(5, 5) ) {
					throw "Unit should have left city";
				}
			}
			else {
				ASSERT( false );
			}
		}
		else if( index == TEST_BUILDROAD_0 || index == TEST_BUILDROAD_1 || index == TEST_BUILDROAD_2 || index == TEST_BUILDROAD_3 || index == TEST_BUILDROAD_4 || index == TEST_BUILDROAD_5 || index == TEST_BUILDROAD_6 || index == TEST_BUILDROAD_7 || index == TEST_BUILDROAD_8 || index == TEST_BUILDRAILWAYS_0 || index == TEST_BUILDRAILWAYS_1 || index == TEST_BUILDRAILWAYS_2 || index == TEST_BUILDRAILWAYS_3 || index == TEST_BUILDRAILWAYS_4 || index == TEST_BUILDRAILWAYS_5 ) {
			Unit *unit = cpu1->getUnit(0);
			Pos2D pos = unit->getPos();
			if( index == TEST_BUILDROAD_0 || index == TEST_BUILDROAD_2 || index == TEST_BUILDROAD_4 || index == TEST_BUILDROAD_6 || index == TEST_BUILDROAD_7 || index == TEST_BUILDRAILWAYS_0 || index == TEST_BUILDRAILWAYS_2 || index == TEST_BUILDRAILWAYS_4 )
				unit->doAI();
			else
				unit->automate();
			if( index == TEST_BUILDROAD_2 || index == TEST_BUILDROAD_3 || index == TEST_BUILDRAILWAYS_2 || index == TEST_BUILDRAILWAYS_3 ) {
				// expect to move
				Pos2D new_pos = unit->getPos();
				if( new_pos.x != pos.x - 1 && new_pos.x != pos.x + 1 ) {
					throw "Expected unit to move towards a city";
				}
			}
			else if( index == TEST_BUILDROAD_6 ) {
				// expect to move
				Pos2D new_pos = unit->getPos();
				if( pos == new_pos || unit->getStatus() != Unit::STATUS_NORMAL ) {
					throw "Expected unit to move";
				}
			}
			else if( index == TEST_BUILDROAD_8 ) {
				// expect to move to city
				Pos2D new_pos = unit->getPos();
				if( new_pos != Pos2D(5, 5) || unit->getStatus() != Unit::STATUS_NORMAL ) {
					throw "Expected unit to move";
				}
			}
			else {
				// expect to stay and build road here
				Pos2D new_pos = unit->getPos();
				if( new_pos != pos ) {
					throw "Unit shouldn't have moved";
				}
				else if( index == TEST_BUILDROAD_0 || index == TEST_BUILDROAD_1 || index == TEST_BUILDROAD_4 || index == TEST_BUILDROAD_5 || index == TEST_BUILDROAD_7 ) {
					if( unit->getStatus() != Unit::STATUS_BUILDING_ROAD ) {
						throw "Unit should have started to build road";
					}
				}
				else {
					if( unit->getStatus() != Unit::STATUS_BUILDING_RAILWAYS ) {
						throw "Unit should have started to build railways";
					}
				}
			}
		}
		else if( index == TEST_UNIT_MOVE_0 || index == TEST_UNIT_MOVE_1 || index == TEST_UNIT_MOVE_7 || index == TEST_UNIT_MOVE_8 || index == TEST_UNIT_MOVE_14 ) {
			Unit *unit = cpu1->getUnit(0);
			int target_x = 7, target_y = 5;
			unit->moveTowardsAI(target_x, target_y);
			Pos2D new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( index == TEST_UNIT_MOVE_8 ) {
				if( new_pos != Pos2D(6, 5) ) {
					throw "Unit in unexpected position after 1st move";
				}
				else if( unit->movesUsed() != Rational(1) ) {
					throw "Unexpected number of used moves after 1st move";
				}
				else {
					unit->moveTowardsAI(target_x, target_y);
					new_pos = unit->getPos();
					VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
					if( new_pos != Pos2D(target_x, target_y) ) {
						throw "Unit in unexpected position after 2nd move";
					}
					else if( unit->movesUsed() != Rational(2) ) {
						throw "Unexpected number of used moves after 2nd move";
					}
				}
			}
			else {
				if( new_pos != Pos2D(6, 6) ) {
					throw "Unit in unexpected position after 1st move";
				}
				else if( index == TEST_UNIT_MOVE_0 && unit->movesUsed() != Rational(1, 3) ) {
					throw "Unexpected number of used moves after 1st move";
				}
				else if( ( index == TEST_UNIT_MOVE_1 || index == TEST_UNIT_MOVE_7 || index == TEST_UNIT_MOVE_14 ) && unit->movesUsed() != Rational(0) ) {
					throw "Unexpected number of used moves after 1st move";
				}
				else {
					unit->moveTowardsAI(target_x, target_y);
					new_pos = unit->getPos();
					VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
					if( new_pos != Pos2D(target_x, target_y) ) {
						throw "Unit in unexpected position after 2nd move";
					}
					else if( index == TEST_UNIT_MOVE_0 && unit->movesUsed() != Rational(2, 3) ) {
						throw "Unexpected number of used moves after 2nd move";
					}
					else if( ( index == TEST_UNIT_MOVE_1 || index == TEST_UNIT_MOVE_7 || index == TEST_UNIT_MOVE_14 ) && unit->movesUsed() != Rational(0) ) {
						throw "Unexpected number of used moves after 2nd move";
					}
				}
			}
		}
		else if( index == TEST_UNIT_MOVE_2 ) {
			Unit *unit = cpu1->getUnit(0);
			int target_x = 7, target_y = 7;
			unit->moveTowardsAI(target_x, target_y);
			Pos2D new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(5, 7) ) {
				throw "Unit in unexpected position after 1st move";
			}
			else if( unit->movesUsed() != Rational(1, 3) ) {
				throw "Unexpected number of used moves after 1st move";
			}
			else {
				unit->moveTowardsAI(target_x, target_y);
				new_pos = unit->getPos();
				VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
				if( new_pos != Pos2D(6, 7) ) {
					throw "Unit in unexpected position after 2nd move";
				}
				else if( unit->movesUsed() != Rational(2, 3) ) {
					throw "Unexpected number of used moves after 2nd move";
				}
				else {
					unit->moveTowardsAI(target_x, target_y);
					new_pos = unit->getPos();
					VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
					if( new_pos != Pos2D(target_x, target_y) ) {
						throw "Unit in unexpected position after 3rd move";
					}
					else if( unit->movesUsed() != Rational(5, 3) ) {
						throw "Unexpected number of used moves after 3rd move";
					}
				}
			}
		}
		else if( index == TEST_UNIT_MOVE_3 ) {
			Unit *unit = cpu1->getUnit(0);
			int target_x = 7, target_y = 7;
			unit->moveTowardsAI(target_x, target_y);
			Pos2D new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(5, 7) ) {
				throw "Unit in unexpected position after 1st move";
			}
			else if( unit->movesUsed() != Rational(0) ) {
				throw "Unexpected number of used moves after 1st move";
			}
			else {
				unit->moveTowardsAI(target_x, target_y);
				new_pos = unit->getPos();
				VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
				if( new_pos != Pos2D(6, 7) ) {
					throw "Unit in unexpected position after 2nd move";
				}
				else if( unit->movesUsed() != Rational(0) ) {
					throw "Unexpected number of used moves after 2nd move";
				}
				else {
					unit->moveTowardsAI(target_x, target_y);
					new_pos = unit->getPos();
					VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
					if( new_pos != Pos2D(target_x, target_y) ) {
						throw "Unit in unexpected position after 3rd move";
					}
					else if( unit->movesUsed() != Rational(1) ) {
						throw "Unexpected number of used moves after 3rd move";
					}
				}
			}
		}
		else if( index == TEST_UNIT_MOVE_4 ) {
			Unit *unit = cpu1->getUnit(0);
			int target_x = 11, target_y = 7;
			unit->moveTowardsAI(target_x, target_y);
			Pos2D new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(9, 9) ) {
				throw "Unit in unexpected position after 1st move";
			}
			else if( unit->movesUsed() != Rational(2) ) {
				throw "Unexpected number of used moves after 1st move";
			}
		}
		else if( index == TEST_UNIT_MOVE_5 ) {
			Unit *unit = cpu1->getUnit(0);
			int target_x = 7, target_y = 5;
			unit->moveTowardsAI(target_x, target_y);
			Pos2D new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(5, 6) ) {
				throw "Unit in unexpected position after 1st move";
			}
			else if( unit->movesUsed() != Rational(0) ) {
				throw "Unexpected number of used moves after 1st move";
			}
			else {
				unit->moveTowardsAI(target_x, target_y);
				new_pos = unit->getPos();
				VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
				if( new_pos != Pos2D(5, 7) ) {
					throw "Unit in unexpected position after 2nd move";
				}
				else if( unit->movesUsed() != Rational(0) ) {
					throw "Unexpected number of used moves after 2nd move";
				}
				else {
					unit->moveTowardsAI(target_x, target_y);
					new_pos = unit->getPos();
					VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
					if( new_pos != Pos2D(6, 8) ) {
						throw "Unit in unexpected position after 3rd move";
					}
					else if( unit->movesUsed() != Rational(0) ) {
						throw "Unexpected number of used moves after 3rd move";
					}
					else {
						unit->moveTowardsAI(target_x, target_y);
						new_pos = unit->getPos();
						VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
						if( new_pos != Pos2D(7, 7) ) {
							throw "Unit in unexpected position after 4th move";
						}
						else if( unit->movesUsed() != Rational(0) ) {
							throw "Unexpected number of used moves after 4th move";
						}
						else {
							unit->moveTowardsAI(target_x, target_y);
							new_pos = unit->getPos();
							VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
							if( new_pos != Pos2D(7, 6) ) {
								throw "Unit in unexpected position after 5th move";
							}
							else if( unit->movesUsed() != Rational(0) ) {
								throw "Unexpected number of used moves after 5th move";
							}
							else {
								unit->moveTowardsAI(target_x, target_y);
								new_pos = unit->getPos();
								VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
								if( new_pos != Pos2D(target_x, target_y) ) {
									throw "Unit in unexpected position after 6th move";
								}
								else if( unit->movesUsed() != Rational(0) ) {
									throw "Unexpected number of used moves after 6th move";
								}
							}
						}
					}
				}
			}
		}
		else if( index == TEST_UNIT_MOVE_6 || index == TEST_FOW_1 || index == TEST_FOW_2 ) {
			Unit *unit = cpu1->getUnit(0);
			int target_x = 0, target_y = 0;
			bool avoid_enemy = index == TEST_UNIT_MOVE_6;
			for(int x=0;x<=2;x++) {
				unit->setPos(Pos2D(x, 3));
				if( index != TEST_FOW_2 )
					cpu1->calculateFogOfWar(true); // reset
				unit->moveTowardsAI(target_x, target_y);
				Pos2D new_pos = unit->getPos();
				VI_log("test x: unit moves to %d, %d\n", new_pos.x, new_pos.y);
				if( avoid_enemy && new_pos != Pos2D(x, 3) ) {
					VI_log("moved from %d, %d to %d, %d\n", x, 3, new_pos.x, new_pos.y);
					throw "Unit in unexpected position, should avoid enemy";
				}
				else if( !avoid_enemy && new_pos.y != 2 ) {
					VI_log("moved from %d, %d to %d, %d\n", x, 3, new_pos.x, new_pos.y);
					throw "Unit in unexpected position, should be unaware of enemy";
				}
				if( unit->movesUsed() != Rational(0) ) {
					throw "Unexpected number of used moves";
				}
				if( index == TEST_FOW_2 )
					avoid_enemy = true; // have now discovered enemy, and we won't be resetting the fog of war
			}
			for(int y=0;y<=2;y++) {
				unit->setPos(Pos2D(3, y));
				if( index != TEST_FOW_2 )
					cpu1->calculateFogOfWar(true); // reset
				unit->moveTowardsAI(target_x, target_y);
				Pos2D new_pos = unit->getPos();
				VI_log("test y: unit moves to %d, %d\n", new_pos.x, new_pos.y);
				if( avoid_enemy && new_pos != Pos2D(3, y) ) {
					VI_log("moved from %d, %d to %d, %d\n", 3, y, new_pos.x, new_pos.y);
					throw "Unit in unexpected position, should avoid enemy";
				}
				else if( !avoid_enemy && new_pos.x != 2 ) {
					VI_log("moved from %d, %d to %d, %d\n", 3, y, new_pos.x, new_pos.y);
					throw "Unit in unexpected position, should be unaware of enemy";
				}
				if( unit->movesUsed() != Rational(0) ) {
					throw "Unexpected number of used moves";
				}
			}
			// here we do move, as we can reduce the "squares" count
			unit->setPos(Pos2D(3, 3));
			if( index != TEST_FOW_2 )
				cpu1->calculateFogOfWar(true); // reset
			unit->moveTowardsAI(target_x, target_y);
			Pos2D new_pos = unit->getPos();
			Pos2D exp_pos = avoid_enemy ? Pos2D(3, 2) : Pos2D(2, 2);
			VI_log("test corner: unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != exp_pos ) {
				VI_log("moved from %d, %d to %d, %d\n", 3, 3, new_pos.x, new_pos.y);
				throw "Unit in unexpected position";
			}
			else if( unit->movesUsed() != Rational(0) ) {
				throw "Unexpected number of used moves";
			}
		}
		else if( index == TEST_UNIT_MOVE_9 ) {
			Unit *unit = cpu1->getUnit(0);
			int target_x = 10, target_y = 5;
			unit->moveTowardsAI(target_x, target_y);
			Pos2D new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(15, 5) ) {
				throw "Unit in unexpected position after 1st move";
			}
			else if( unit->movesUsed() != Rational(1) ) {
				throw "Unexpected number of used moves after 1st move";
			}

			unit->moveTowardsAI(target_x, target_y);
			new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(14, 5) ) {
				throw "Unit in unexpected position after 2nd move";
			}
			else if( unit->movesUsed() != Rational(2) ) {
				throw "Unexpected number of used moves after 2nd move";
			}

			unit->reset(); // reset moves
			if( unit->movesUsed() != Rational(0) ) {
				throw "Failed to reset moves after 2nd move";
			}

			// now test moving back again
			target_x = 0;

			unit->moveTowardsAI(target_x, target_y);
			new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(15, 5) ) {
				throw "Unit in unexpected position after 3rd move";
			}
			else if( unit->movesUsed() != Rational(1) ) {
				throw "Unexpected number of used moves after 3rd move";
			}

			unit->moveTowardsAI(target_x, target_y);
			new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( new_pos != Pos2D(0, 5) ) {
				throw "Unit in unexpected position after 4th move";
			}
			else if( unit->movesUsed() != Rational(2) ) {
				throw "Unexpected number of used moves after 4th move";
			}
		}
		else if( index == TEST_UNIT_MOVE_10 || index == TEST_UNIT_MOVE_11 || index == TEST_UNIT_MOVE_12 || index == TEST_UNIT_MOVE_13 ) {
			Unit *unit = cpu1->getUnit(0);
			Pos2D target = unit->getPos();
			if( index == TEST_UNIT_MOVE_10 )
				target.x += 2;
			else if( index == TEST_UNIT_MOVE_11 )
				target.x += 4;
			else if( index == TEST_UNIT_MOVE_12 )
				target.x += 4;
			else if( index == TEST_UNIT_MOVE_13 )
				target.x += 6;

			const Distance *dists = mainGamestate->getMap()->calculateDistanceMap(cpu1, unit, true, true, unit->getX(), unit->getY());
			Distance dist = dists[target.y*mainGamestate->getMap()->getWidth() + target.x];
			Distance expected_dist;
			Rational expected_moves_left;
			if( index == TEST_UNIT_MOVE_10 ) {
				expected_dist.cost = 2;
				expected_dist.squares = 2;
				expected_moves_left = Rational(1, 3);
			}
			else if( index == TEST_UNIT_MOVE_11 ) {
				expected_dist.cost = 3;
				expected_dist.squares = 1;
				expected_moves_left = Rational(0);
			}
			else if( index == TEST_UNIT_MOVE_12 ) {
				expected_dist.cost = 4;
				expected_dist.squares = 4;
				expected_moves_left = Rational(2, 3);
			}
			else if( index == TEST_UNIT_MOVE_13 ) {
				expected_dist.cost = 0;
				expected_dist.squares = 6;
				expected_moves_left = Rational(1);
			}
			VI_log("dist cost %d, squares %d\n", dist.cost, dist.squares);

			if( dist.cost != expected_dist.cost ) {
				throw "Unexpected distance cost";
			}
			else if( dist.squares != expected_dist.squares ) {
				throw "Unexpected distance squares";
			}

			while( unit->hasMovesLeft() && unit->getPos() != target ) {
				unit->moveTowardsAI(target.x, target.y);
				Pos2D new_pos = unit->getPos();
				VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			}
			if( unit->getPos() != target ) {
				throw "Failed to reach target";
			}

			Rational moves_left = unit->movesLeft();
			if( moves_left != expected_moves_left ) {
				throw "Unexpected moves left";
			}

			delete [] dists;
		}
		else if( index == TEST_UNIT_RETREAT_0 || index == TEST_UNIT_RETREAT_1 || index == TEST_UNIT_RETREAT_2 || index == TEST_UNIT_RETREAT_3 || index == TEST_UNIT_RETREAT_4 ) {
			Unit *unit = cpu1->getUnit(0);
			Pos2D old_pos = unit->getPos();
			bool moved = unit->doAI();
			Pos2D new_pos = unit->getPos();
			VI_log("unit moves to %d, %d\n", new_pos.x, new_pos.y);
			if( index == TEST_UNIT_RETREAT_0 || index == TEST_UNIT_RETREAT_3 ) {
				if( new_pos.x - old_pos.x != -1 ) {
					throw "Expected unit to move west towards city";
				}
			}
			else if( index == TEST_UNIT_RETREAT_1 || index == TEST_UNIT_RETREAT_4 ) {
				if( new_pos.x - old_pos.x != 1 ) {
					throw "Expected unit to move east towards city";
				}
			}
			else if( index == TEST_UNIT_RETREAT_2 ) {
				if( moved ) {
					throw "Didn't expect unit to move";
				}
				else if( new_pos != old_pos ) {
					throw "Didn't expect unit to change position";
				}
			}
			else {
				ASSERT( false );
			}
		}
		else if(
			index == TEST_AIR_BOMB_0 ||
			index == TEST_AIR_BOMB_1 ||
			index == TEST_AIR_BOMB_2 ||
			index == TEST_AIR_BOMB_3 ||
			index == TEST_AIR_BOMB_4 ||
			index == TEST_AIR_BOMB_5 ||
			index == TEST_AIR_BOMB_6 ||
			index == TEST_AIR_BOMB_7 )
		{
			bool expected_can_bomb = index == TEST_AIR_BOMB_0 || index == TEST_AIR_BOMB_2 || index == TEST_AIR_BOMB_4 || index == TEST_AIR_BOMB_6;
			City *city = cpu1->getCity(0);
			City *city2 = cpu2->getCity(0);
			Unit *unit = cpu1->getUnit(0);
			if( !unit->getTemplate()->canBomb() ) {
				throw "Unit can't bomb";
			}
			else if( city->canLaunch(unit->getTemplate()) != expected_can_bomb ) {
				throw "Unexpected launch ability";
			}
			for(int cy=city2->getY()-1;cy<=city2->getY()+1;cy++) {
				for(int cx=city2->getX()-1;cx<=city2->getX()+1;cx++) {
					if( cpu1->isFogOfWarVisible(cx, cy) ) {
						throw "Didn't expect to already be able to see enemy city (fog of war)";
					}
				}
			}
			Pos2D old_pos = unit->getPos();
			bool is_missile = unit->getTemplate()->isMissile();
			int old_n_bombed = unit->getNBombed();
			unit->doAI();
			if( expected_can_bomb ) {
				if( is_missile ) {
					unit = NULL; // unit should now be deleted!
					if( cpu1->getNUnits() != 0 ) {
						throw "Didn't use missile";
					}
				}
				else {
					int new_n_bombed = unit->getNBombed();
					if( unit->movesLeft() != 0 ) {
						throw "Expected 0 moves left";
					}
					else if( new_n_bombed - old_n_bombed != 1 ) {
						throw "Unexpected n_bombed count";
					}
				}
			}
			else {
				int new_n_bombed = unit->getNBombed();
				VI_log("n_bombed was %d, now %d\n", old_n_bombed, new_n_bombed);
				/*if( unit->movesLeft() != 1 ) {
					throw "Expected 1 move left";
				}*/
				// we now test we didn't bomb via the n_bombed counts; as we now use up all the moves, even if we can't bomb (i.e., action NOTHING rather than WAIT)
				if( new_n_bombed != old_n_bombed ) {
					throw "Didn't expect to bomb";
				}
			}
			if( unit != NULL && unit->getPos() != old_pos ) {
				throw "Didn't expect air unit to move";
			}
			bool expected_reveal_fow = expected_can_bomb && !is_missile;
			for(int cy=city2->getY()-1;cy<=city2->getY()+1;cy++) {
				for(int cx=city2->getX()-1;cx<=city2->getX()+1;cx++) {
					if( cpu1->isFogOfWarVisible(cx, cy) != expected_reveal_fow ) {
						throw "Unexpected fog of war";
					}
				}
			}
		}
		else if(
			index == TEST_AIR_REBASE_0 ||
			index == TEST_AIR_REBASE_1 ||
			index == TEST_AIR_REBASE_2 ||
			index == TEST_AIR_REBASE_3 ||
			index == TEST_AIR_REBASE_4 ||
			index == TEST_AIR_REBASE_5 ||
			index == TEST_AIR_REBASE_6 )
		{
			bool expected_can_rebase0 = index == TEST_AIR_REBASE_0 || index == TEST_AIR_REBASE_1 || index == TEST_AIR_REBASE_4 || index == TEST_AIR_REBASE_5;
			bool expected_can_rebase1 = index == TEST_AIR_REBASE_0 || index == TEST_AIR_REBASE_2 || index == TEST_AIR_REBASE_4 || index == TEST_AIR_REBASE_6;
			City *city0 = cpu1->getCity(0);
			City *city1 = cpu1->getCity(1);
			Unit *unit = cpu1->getUnit(0);
			int sea_range0 = 0;
			int air_range0 = 0;
			int sea_range1 = 0;
			int air_range1 = 0;
			city0->getTravelRanges(&sea_range0, &air_range0, unit->getTemplate());
			city1->getTravelRanges(&sea_range1, &air_range1, unit->getTemplate());
			if( index == TEST_AIR_REBASE_0 || index == TEST_AIR_REBASE_1 || index == TEST_AIR_REBASE_2 || index == TEST_AIR_REBASE_3 ) {
				if( sea_range0 != 0 || sea_range1 != 0 ) {
					throw "Air units shouldn't be able to travel by sea";
				}
			}
			bool can_rebase0 = air_range0 != 0;
			bool can_rebase1 = air_range1 != 0;
			if( can_rebase0 != expected_can_rebase0 || can_rebase1 != expected_can_rebase1 ) {
				throw "Unexpected rebase ability";
			}
			if( air_range0 != 0 ) {
				city0->initTravel(false, unit->getTemplate());
				bool can_travel_to = mainGamestate->getMap()->getSquare(city1->getPos())->isTarget();
				if( can_travel_to != expected_can_rebase1 ) {
					throw "Unexpected travel to ability";
				}
			}
			if( can_rebase0 && can_rebase1 ) {
				mainGamestate->moveTo(unit, city1->getPos(), true);
			}
		}
		else if( index == TEST_ATTACK_0 || index == TEST_ATTACK_1 || index == TEST_ATTACK_2 || index == TEST_ATTACK_3 ) {
			size_t n_units1 = cpu1->getNUnits();
			size_t n_units2 = cpu2->getNUnits();
			Unit *unit = cpu1->getUnit(0);
			Pos2D pos = unit->getPos();
			unit->doAI();
			bool expected_attack = false;
			if( index == TEST_ATTACK_0 || index == TEST_ATTACK_1 || index == TEST_ATTACK_2 ) {
				expected_attack = true;
			}
			bool did_attack = false;
			if( n_units1 != cpu1->getNUnits() ) {
				// unit must have attacked and lost
				did_attack = true;
			}
			else if( n_units2 != cpu2->getNUnits() ) {
				// unit must have attacked and won
				did_attack = true;
			}
			if( expected_attack != did_attack ) {
				throw "Unexpected attack behaviour";
			}
		}
		else if( index == TEST_ATTACK_4 || index == TEST_ATTACK_5 ) {
			Unit *unit1 = cpu1->getUnit(0);
			Unit *unit2 = cpu2->getUnit(0);
			Pos2D old_pos = unit1->getPos();
			unit1->doAI();
			if( unit1->getPos() != old_pos ) {
				throw "Didn't expect unit1 to move";
			}
			else if( unit1->getStatus() != Unit::STATUS_BUILDING_ROAD ) {
				throw "Expected unit to build a road";
			}
			unit2->doAI();
			if( unit2->getPos().x != 8 ) {
				throw "Expected unit2 to move west";
			}
			unit1->reset();
			unit1->doAI();
			// unit1 or unit2 may have been destroyed
			if( index == TEST_ATTACK_4 ) {
				if( cpu1->getNUnits() + cpu2->getNUnits() != 3 ) { // count includes city defence units!
					throw "Expected to have attacked enemy";
				}
				else if( cpu1->getNUnits() >= 3 && unit1->getStatus() != Unit::STATUS_NORMAL ) {
					throw "Expected unit to have normal status now";
				}
			}
			else if( index == TEST_ATTACK_5 ) {
				if( cpu1->getNUnits() + cpu2->getNUnits() != 4 ) { // count includes city defence units!
					throw "Expected not to have attacked enemy";
				}
				else if( unit1->getPos() != old_pos ) {
					throw "Didn't expect unit1 to move on 2nd move";
				}
				else if( unit1->getStatus() != Unit::STATUS_BUILDING_ROAD ) {
					throw "Expected unit to build a road on 2nd move";
				}
			}
			else {
				ASSERT(false);
			}
		}
		else if( index == TEST_ATTACK_6 || index == TEST_ATTACK_7 ) {
			Unit *unit1 = cpu1->getUnit(0);
			Pos2D old_pos = unit1->getPos();
			unit1->doAI();
			// unit1 or unit2 may have been destroyed
			if( index == TEST_ATTACK_6 || index == TEST_ATTACK_7 ) {
				if( cpu1->getNUnits() + cpu2->getNUnits() != 3 ) { // count includes city defence units!
					throw "Expected to have attacked enemy";
				}
				else if( cpu1->getNUnits() >= 3 && unit1->getStatus() != Unit::STATUS_NORMAL ) {
					throw "Expected unit to have normal status now";
				}
			}
			/*else if( index == TEST_ATTACK_7 ) {
				if( cpu1->getNUnits() + cpu2->getNUnits() != 4 ) { // count includes city defence units!
					throw "Expected not to have attacked enemy";
				}
				else if( unit1->getPos() == old_pos ) {
					throw "Expect unit1 to move";
				}
			}*/
			else {
				ASSERT(false);
			}
		}
		else if( index == TEST_UNIT_EXPLORE_0 || index == TEST_UNIT_EXPLORE_1 || index == TEST_UNIT_EXPLORE_2 ) {
			Unit *unit = cpu1->getUnit(0);
			Pos2D old_pos = unit->getPos();
			unit->doAI();
			Pos2D pos = unit->getPos();
			if( index == TEST_UNIT_EXPLORE_0 ) {
				//if( pos != Pos2D(6, 5) ) {
				if( pos != Pos2D(6, 4) ) {
					throw "Unit didn't move towards unexplored square";
				}
			}
			else if( index == TEST_UNIT_EXPLORE_1 ) {
				if( pos == old_pos ) {
					throw "Unit didn't move towards unexplored square";
				}
			}
			else if( index == TEST_UNIT_EXPLORE_2 ) {
				if( pos != old_pos ) {
					throw "Unit should have waited";
				}
				else if( !unit->hasMovesLeft() ) {
					throw "Unit should have moves left";
				}
				else {
					unit = cpu1->getUnit(1);
					unit->doAI();
					if( cpu1->getNCities() != 1 ) {
						throw "Expected 1 city";
					}
					else if( cpu1->getNUnits() != 1 ) {
						throw "Expected 1 unit to remain";
					}
				}
			}
			else {
				ASSERT( false );
			}
		}
		else if( index == TEST_CITYBUILD_0 || index == TEST_CITYBUILD_1 || index == TEST_CITYBUILD_2 || index == TEST_CITYBUILD_3 || index == TEST_CITYBUILD_4 || index == TEST_CITYBUILD_5 || index == TEST_CITYBUILD_6 || index == TEST_CITYBUILD_7 || index == TEST_CITYBUILD_8 || index == TEST_CITYBUILD_9 || index == TEST_CITYBUILD_10 || index == TEST_CITYBUILD_11 || index == TEST_CITYBUILD_12 || index == TEST_CITYBUILD_13 || index == TEST_CITYBUILD_14 || index == TEST_CITYBUILD_15 || index == TEST_CITYBUILD_16 || index == TEST_CITYBUILD_17 || index == TEST_CITYBUILD_18 || index == TEST_CITYBUILD_19 || index == TEST_CITYBUILD_20 || index == TEST_CITYBUILD_21 || index == TEST_CITYBUILD_22 || index == TEST_CITYBUILD_23 || index == TEST_CITYBUILD_24 ) {
			City *city = cpu1->getCity(0);
			const Buildable *buildable = city->getBuildable();
			const Buildable *exp_buildable = NULL;
			if( index == TEST_CITYBUILD_0 )
				exp_buildable = game_g->findBuildable("Peasants");
			else if( index == TEST_CITYBUILD_1 )
				exp_buildable = game_g->findBuildable("Horsemen");
			else if( index == TEST_CITYBUILD_2 )
				exp_buildable = game_g->findBuildable("Farmland");
			else if( index == TEST_CITYBUILD_3 )
				exp_buildable = game_g->findBuildable("Peasants");
			else if( index == TEST_CITYBUILD_4 )
				exp_buildable = game_g->findBuildable("Harbour");
			else if( index == TEST_CITYBUILD_5 )
				exp_buildable = game_g->findBuildable("Peasants");
			else if( index == TEST_CITYBUILD_6 )
				exp_buildable = game_g->findBuildable("Peasants");
			else if( index == TEST_CITYBUILD_7 )
				exp_buildable = game_g->findBuildable("Harbour");
			else if( index == TEST_CITYBUILD_8 )
				exp_buildable = game_g->findBuildable("Nuclear Missile");
			else if( index == TEST_CITYBUILD_9 )
				exp_buildable = game_g->findBuildable("Spearmen");
			else if( index == TEST_CITYBUILD_10 )
				exp_buildable = game_g->findBuildable("Tanks");
			else if( index == TEST_CITYBUILD_11 )
				exp_buildable = game_g->findBuildable("Settlers");
			else if( index == TEST_CITYBUILD_12 )
				exp_buildable = game_g->findBuildable("Spearmen");
			else if( index == TEST_CITYBUILD_13 )
				exp_buildable = game_g->findBuildable("School");
			else if( index == TEST_CITYBUILD_14 )
				exp_buildable = game_g->findBuildable("Tanks");
			else if( index == TEST_CITYBUILD_15 )
				exp_buildable = game_g->findBuildable("Farmland");
			else if( index == TEST_CITYBUILD_16 )
				exp_buildable = game_g->findBuildable("Settlers");
			else if( index == TEST_CITYBUILD_17 )
				exp_buildable = game_g->findBuildable("Peasants");
			else if( index == TEST_CITYBUILD_18 )
				exp_buildable = game_g->findBuildable("Horsemen");
			else if( index == TEST_CITYBUILD_19 )
				exp_buildable = game_g->findBuildable("Peasants");
			else if( index == TEST_CITYBUILD_20 )
				exp_buildable = game_g->findBuildable("Musketeers");
			else if( index == TEST_CITYBUILD_21 )
				exp_buildable = game_g->findBuildable("Settlers");
			else if( index == TEST_CITYBUILD_22 )
				exp_buildable = game_g->findBuildable("Spearmen");
			else if( index == TEST_CITYBUILD_23 )
				exp_buildable = game_g->findBuildable("Settlers");
			else if( index == TEST_CITYBUILD_24 )
				exp_buildable = game_g->findBuildable("Quarry");
			else {
				ASSERT( false );
			}
			if( buildable != exp_buildable ) {
				VI_log("expected to build a %s\n", exp_buildable->getName());
				throw "Unexpected choice for city build";
			}
		}
		else if( index == TEST_CITY_0 || index == TEST_CITY_1 || index == TEST_CITY_2 ) {
			int growth_rate[3] = {0, 0, 0};
			int exp_growth_rate[3] = {0, 0, 0};
			for(size_t i=0;i<3;i++) {
				const City *city = cpu1->getCity(i);
				growth_rate[i] = city->getPopulationCost();
				VI_log("city %d has growth rate %d\n", i, growth_rate[i]);
			}
			if( index == TEST_CITY_0 ) {
				exp_growth_rate[0] = 9;
				exp_growth_rate[1] = 9;
				exp_growth_rate[2] = 9;
			}
			else if( index == TEST_CITY_1 ) {
				exp_growth_rate[0] = 10;
				exp_growth_rate[1] = 9;
				exp_growth_rate[2] = 9;
			}
			else if( index == TEST_CITY_2 ) {
				exp_growth_rate[0] = 10;
				exp_growth_rate[1] = 10;
				exp_growth_rate[2] = 10;
			}
			for(size_t i=0;i<3;i++) {
				if( growth_rate[i] != exp_growth_rate[i] ) {
					VI_log("expected %d th growth rate to be %d\n", i, exp_growth_rate[i]);
					throw "Unexpected growth rate";
				}
			}
		}
		else if( index == TEST_CITYSTOCKS_0 || index == TEST_CITYSTOCKS_1 ) {
			ASSERT( cpu1->getNCities() == 4 );
			for(size_t i=0;i<cpu1->getNCities();i++) {
				City *city = cpu1->getCity(i);
				city->setNeedsUpdate();
				city->update();
			}
			int exp_stocks[4] = {0, 0, 0, 0};
			if( index == TEST_CITYSTOCKS_0 ) {
				exp_stocks[0] = 200;
				exp_stocks[1] = 200;
				exp_stocks[2] = 20;
				exp_stocks[3] = 70;
			}
			else if( index == TEST_CITYSTOCKS_1 ) {
				exp_stocks[0] = 190;
				exp_stocks[1] = 200;
				exp_stocks[2] = 0;
				exp_stocks[3] = 50;
			}
			else {
				ASSERT( false );
			}
			for(size_t i=0;i<cpu1->getNCities();i++) {
				City *city = cpu1->getCity(i);
				int stock = city->getElementStocks(game_g->getGameData()->findElement("Wood"));
				VI_log("city %d has %d stocks\n", i, stock);
				if( stock != exp_stocks[i] ) {
					VI_log("city %d has %d stocks, expected %d\n", i, stock, exp_stocks[i]);
					throw "unexpected city stocks";
				}
			}
		}
		else if( index == TEST_CITYSTOCKS_2 ) {
			for(size_t i=0;i<cpu1->getNCities();i++) {
				City *city = cpu1->getCity(i);
				city->setNeedsUpdate();
				city->update();
			}
			for(size_t i=0;i<cpu2->getNCities();i++) {
				City *city = cpu2->getCity(i);
				city->setNeedsUpdate();
				city->update();
			}
			const Element *oil = game_g->getGameData()->findElement("Oil");
			if( cpu1->getCity(0)->getElementStocks(oil) != 0 ) {
				throw "Expected civ 1 city 0 to have no oil";
			}
			else if( cpu1->getCity(1)->getElementStocks(oil) != 0 ) {
				throw "Expected civ 1 city 1 to have no oil";
			}
			else if( cpu2->getCity(0)->getElementStocks(oil) != 0 ) {
				throw "Expected civ 2 city 0 to have no oil";
			}
			else if( cpu2->getCity(1)->getElementStocks(oil) != 30 ) {
				throw "Expected civ 2 city 1 to have 30 oil";
			}
		}
		else if( index == TEST_CITYSTOCKS_3 ) {
			for(size_t i=0;i<cpu1->getNCities();i++) {
				City *city = cpu1->getCity(i);
				city->setNeedsUpdate();
				city->update();
			}
			for(size_t i=0;i<cpu1->getNUnits();i++) {
				Unit *unit = cpu1->getUnit(i);
				unit->update();
			}
			const Element *iron = game_g->getGameData()->findElement("Iron");
			VI_log("city 0 has %d iron left\n", cpu1->getCity(0)->getElementStocks(iron));
			VI_log("city 1 has %d iron left\n", cpu1->getCity(1)->getElementStocks(iron));
			if( cpu1->getCity(0)->getElementStocks(iron) != 1 ) {
				throw "Expected city 0 to have 1 iron left";
			}
			else if( cpu1->getCity(1)->getElementStocks(iron) != 9 ) {
				throw "Expected city 1 to have 9 iron left";
			}
			const vector<Unit *> *units0 = mainGamestate->getMap()->findUnitsAt(cpu1->getCity(0)->getPos());
			const vector<Unit *> *units1 = mainGamestate->getMap()->findUnitsAt(cpu1->getCity(1)->getPos());
			if( units0->size() != 1 ) {
				throw "Unexpected number of units at city 0";
			}
			else if( units1->size() != 1 ) {
				throw "Unexpected number of units at city 1";
			}
			else if( !units0->at(0)->getTemplate()->equals("Pikemen") ) {
				throw "Expected Spearmen at city 1 to upgrade to Pikemen";
			}
			else if( !units1->at(0)->getTemplate()->equals("Spearmen") ) {
				throw "Didn't expect Spearmen at city 1 to upgrade";
			}
		}
		else if( index == TEST_AIPERF_0 || index == TEST_AIPERF_1 || index == TEST_AIPERF_2 || index == TEST_AIPERF_4 || index == TEST_AIPERF_8 || index == TEST_AIPERF_9 || index == TEST_AIPERF_10 || index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 || index == TEST_AIPERF_17 ) {
			// mainGamestate tests the performance of the AI, in particular, when we repeatedly call it for the same unit in the same turn
			// on 20110212:
			// TEST_AIPERF_0 on AMD 630, old algorithm: 2105
			// TEST_AIPERF_0 on AMD 630, new algorithm (ai_dest): 1023
			// TEST_AIPERF_0 on Intel Atom N450, old algorithm: 8705
			// TEST_AIPERF_0 on Intel Atom N450, new algorithm (ai_dest): 4799
			// on 20110313 (all testing with the "new" algorithm
			// TEST_AIPERF_0 on Intel Atom N450, C++: 5692
			// TEST_AIPERF_1 on Intel Atom N450, C++: 5135
			// TEST_AIPERF_2 on Intel Atom N450, C++: 5133
			// TEST_AIPERF_3 on Intel Atom N450, C++: 4963
			// TEST_AIPERF_4 on Intel Atom N450, C++: 12569 [Time spent in AI routine: 6077; Time spent in distance map: 10267]
			// TEST_AIPERF_0 on Intel Atom N450, Lua: 6382
			// TEST_AIPERF_1 on Intel Atom N450, Lua: 5472
			// TEST_AIPERF_2 on Intel Atom N450, Lua: 5507
			// TEST_AIPERF_3 on Intel Atom N450, Lua: 5228
			// TEST_AIPERF_4 on Intel Atom N450, Lua: 22226 [Time spent in AI routine: 12002]
			perf_test = true;
			Unit *unit = cpu1->getUnit(0);

			// firstly check the distance map
			Distance *dists = mainGamestate->getMap()->calculateDistanceMap(cpu1, unit, false, true, unit->getX(), unit->getY());
			Distance dist = dists[4 * mainGamestate->getMap()->getWidth() + 96];
			VI_log("dist cost %d, squares %d\n", dist.cost, dist.squares);
			if( ( index == TEST_AIPERF_0 || index == TEST_AIPERF_4 || index == TEST_AIPERF_14 ) && ( dist.cost != 0 || dist.squares != 92 ) ) {
				throw "Unexpected cost or squares";
			}
			else if( ( index == TEST_AIPERF_1 || index == TEST_AIPERF_2 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 ) && ( dist.cost != road_move_scale_c || dist.squares != 92 ) ) {
				throw "Unexpected cost or squares";
			}
			else if( index == TEST_AIPERF_8 && ( dist.cost != 9 || dist.squares != 92 ) ) {
				throw "Unexpected cost or squares";
			}
			else if( ( index == TEST_AIPERF_9 || index == TEST_AIPERF_10 || index == TEST_AIPERF_17 ) && ( dist.cost != 0 || dist.squares != 92 ) ) {
				throw "Unexpected cost or squares";
			}
			else if( ( index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 ) && ( dist.cost != 9 || dist.squares != 92 ) ) {
				throw "Unexpected cost or squares";
			}
			delete [] dists;

			if( !unit->hasMovesLeft() ) {
				throw "Unit has no moves available";
			}
			//for(;;) {
			while( unit->hasMovesLeft() ) {
				Pos2D old_pos = unit->getPos();
				if( !unit->doAI() ) {
					VI_log("old pos: %d, %d\n", old_pos.x, old_pos.y);
					Pos2D new_pos = unit->getPos();
					VI_log("new pos: %d, %d\n", new_pos.x, new_pos.y);
					if( index == TEST_AIPERF_13 || index == TEST_AIPERF_17 ) {
						// okay
						break;
					}
					throw "Failed to move";
				}
				if( index == TEST_AIPERF_4 ) {
					unit->setAIDest();
				}
				if( ( index == TEST_AIPERF_2 || index == TEST_AIPERF_10 || index == TEST_AIPERF_17 ) && cpu3->getNUnits() == 0 ) {
					throw "Didn't expect to attack friendly civilization's units!";
				}
				if( ( index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 ) && cpu3->getNCities() == 0 ) {
					throw "Didn't expect to attack friendly civilization's cities!";
				}
				if( index == TEST_AIPERF_8 ) {
					if( unit->getPos() == old_pos ) {
						throw "Unit failed to move position";
					}
				}
				else if( cpu2->getNUnits() == 0 ) {
					// must have defeated the enemy(s)
					break;
				}
				else {
					if( index != TEST_AIPERF_9 && index != TEST_AIPERF_11 && index != TEST_AIPERF_12 && index != TEST_AIPERF_15 && !unit->hasMovesLeft() ) {
						throw "Didn't expect to use up all the moves";
					}
					if( ( index == TEST_AIPERF_0 || index == TEST_AIPERF_2 || index == TEST_AIPERF_4 || index == TEST_AIPERF_9 || index == TEST_AIPERF_10 || index == TEST_AIPERF_11 || index == TEST_AIPERF_12 || index == TEST_AIPERF_13 || index == TEST_AIPERF_14 || index == TEST_AIPERF_15 || index == TEST_AIPERF_16 ) && unit->getPos() == old_pos ) {
						// in test TEST_AIPERF_1, we won't be moving when attacking the 1st unit
						throw "Unit failed to move position";
					}
				}
			}
			Pos2D pos = unit->getPos();
			VI_log("Unit moved to %d, %d\n", pos.x, pos.y);
			if( index == TEST_AIPERF_8 ) {
				if( pos.x != 94 ) {
					throw "Unit is in unexpected position";
				}
				else if( unit->hasMovesLeft() ) {
					throw "Shouldn't have moves left after moving into enemy territory";
				}
			}
			else if( index == TEST_AIPERF_9 || index == TEST_AIPERF_10 ) {
				if( pos.x != 96 ) {
					throw "Unit is in unexpected position";
				}
				else if( unit->hasMovesLeft() ) {
					throw "Shouldn't have moves left after moving around blocking unit, then attacking";
				}
			}
			else if( index == TEST_AIPERF_11 || index == TEST_AIPERF_12 ) {
				if( pos != Pos2D(80, 5) ) {
					throw "Unit is in unexpected position";
				}
				else if( unit->hasMovesLeft() ) {
					throw "Shouldn't have moves left due to moving around enemy territory";
				}
			}
			else if( index == TEST_AIPERF_13 ) {
				if( pos != Pos2D(4, 4) ) {
					throw "Unit is in unexpected position";
				}
			}
			else if( index == TEST_AIPERF_14 ) {
				if( pos != Pos2D(96, 4) ) {
					throw "Unit is in unexpected position";
				}
			}
			else if( index == TEST_AIPERF_15 ) {
				if( pos != Pos2D(96, 4) ) {
					throw "Unit is in unexpected position";
				}
				else if( cpu2->getNUnits() > 0 ) {
					throw "Expected to defeat all enemy units";
				}
			}
			else if( index == TEST_AIPERF_16 ) {
				if( pos != Pos2D(80, 4) ) {
					throw "Unit is in unexpected position";
				}
				else if( cpu2->getNUnits() > 0 ) {
					throw "Expected to defeat all enemy units";
				}
			}
			else if( index == TEST_AIPERF_17 ) {
				if( pos != Pos2D(79, 4) ) {
					throw "Unit is in unexpected position";
				}
			}
		}
		else if( index == TEST_AIPERF_5 || index == TEST_AIPERF_6 || index == TEST_AIPERF_7 ) {
			// mainGamestate tests the performance of the AI, for a range of cities/units
			perf_test = true;
			const int n_iter = index == TEST_AIPERF_5 ? 10 : 100;
			for(int i=0;i<n_iter;i++) {
				if( index == TEST_AIPERF_5 ) {
					for(size_t j=0;j<cpu1->getNCities();j++) {
						City *city = cpu1->getCity(j);
						city->setBuildable(NULL);
						city->doAI();
					}
				}
				else if( index == TEST_AIPERF_6 || index == TEST_AIPERF_7 ) {
					for(size_t j=0;j<cpu1->getNUnits();j++) {
						Unit *unit = cpu1->getUnit(j);
						unit->reset();
						unit->doAI();
					}
				}
				else {
					ASSERT( false );
				}
			}
		}
		else if( index == TEST_AIPERF_3 ) {
			// mainGamestate tests the performance of the AI movement routines
			perf_test = true;
			Unit *unit = cpu1->getUnit(0);

			Pos2D target(96, 4);

			for(;;) {
				Pos2D old_pos = unit->getPos();
				if( !unit->moveTowardsAI(target.x, target.y) ) {
					VI_log("old pos: %d, %d\n", old_pos.x, old_pos.y);
					VI_log("new pos: %d, %d\n", unit->getPos().x, unit->getPos().y);
					throw "Failed to move";
				}
				if( cpu2->getNUnits() == 1 ) {
					// must have defeated the enemy
					if( unit->hasMovesLeft() ) {
						throw "Shouldn't have moves left after defeating enemy";
					}
					break;
				}
				else {
					if( !unit->hasMovesLeft() ) {
						throw "Didn't expect to use up all the moves";
					}
					if( unit->getPos() == old_pos ) {
						throw "Unit failed to move position";
					}
				}
			}
		}
		else if( index == TEST_DISTANCES_0 || index == TEST_DISTANCES_1 ) {
			perf_test = true;
			//game_g->getGraphicsEnvironment()->setSMP( index == TEST_DISTANCES_1 );
			/*
			// TEST_DISTANCES_0 on AMD 630: 13019, 12987, 13294
			// TEST_DISTANCES_0 on Intel Core Duo: 24766, 24953
			*/
			// on 20100405:
			// TEST_DISTANCES_0 on AMD 630, old algorithm: 12839, 12652
			// TEST_DISTANCES_0 on AMD 630, Dijkstra's algorithm: 10359, 10327
			// TEST_DISTANCES_1 on AMD 630, old algorithm: 16318, 16302
			// TEST_DISTANCES_1 on AMD 630, Dijkstra's algorithm: 10390, 10405
			// TEST_DISTANCES_0 on Intel Core Duo, old algorithm: 26860, 26484
			// TEST_DISTANCES_0 on Intel Core Duo, Dijkstra's algorithm: 17062, 17031, 17234
			// TEST_DISTANCES_1 on Intel Core Duo, old algorithm: 33312, 33031
			// TEST_DISTANCES_1 on Intel Core Duo, Dijkstra's algorithm: 17203, 17235
			// on 20110105
			// TEST_DISTANCES_0 on Intel Atom N450, Dijkstra's algorithm: 37414
			// TEST_DISTANCES_1 on Intel Atom N450, Dijkstra's algorithm: 36956
			{
				for(int y=0;y<mainGamestate->getMap()->getHeight();y++)
					//int y = 0;
				{
					//VI_log("%d\n", y);
					for(int x=0;x<mainGamestate->getMap()->getWidth();x++)
						//int x = 0;
					{
						//VI_log(">>> %d, %d\n", x, y);
						const MapSquare *square = mainGamestate->getMap()->getSquare(x, y);
						Distance *dists = mainGamestate->getMap()->calculateDistanceMap(cpu1, NULL, false, true, x, y, -1, -1, !square->isLand());
						/*for(int cy=0;cy<map->getHeight();cy++) {
							for(int cx=0;cx<map->getWidth();cx++) {
								int dist = dists[cy*map->getWidth()+cx].cost;
								if( dist != -1 ) {
									VI_log("        %d, %d: %d\n", cx, cy, dist);
								}
							}
						}*/
						delete [] dists;
					}
				}
			}
			/*for(int i=0;i<200;i++) {
			int x = 0, y = 0;
			const MapSquare *square = map->getSquare(x, y);
			Distance *dists = map->calculateDistanceMap(cpu1, NULL, false, true, x, y, -1, -1, !square->isLand());
			delete [] dists;
			}*/
		}
		else {
			ASSERT( false );
		}
	}
	catch(const char *error) {
		VI_log("TEST ERROR: %s\n", error);
		fprintf(file, "%d,FAILED,%s\n", (int)index, error);
		test_result = false;
	}
	int time_e = game_g->getGraphicsEnvironment()->getRealTimeMS();
	int time_taken = time_e - time_s;
	VI_log("Test Time: %d\n", time_taken);
	if( test_result ) {
		fprintf(file, "%d,PASSED", (int)index);
		if( perf_test ) {
			fprintf(file, ",%d", time_taken);
		}
		fprintf(file, "\n");
	}
	fclose(file);

	if( show ) {
		stringstream text;
		text << "Finishing test: " << (int)index;
		text << "\nResult: " << ( test_result ? "PASSED" : "FAILED" );
		if( test_result && perf_test ) {
			text << "\nTime: " << time_taken;
		}
		InfoWindow *window = new InfoWindow(game_g->getGraphicsEnvironment(), mainGamestate->getPanel(), text.str(), game_g->getFont(), game_g->getPanelTexture(), infowindow_def_x_c, infowindow_def_y_c, 256, 128);
		window->doModal();
		delete window;
	}
}
