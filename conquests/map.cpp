#include "conquests_stdafx.h"

#include "map.h"
#include "buildable.h"
#include "unit.h"
#include "civilization.h"
#include "city.h"
#include "maingamestate.h"

#include "../Vision/Renderer.h"

#include <ctime>

#include <sstream>
using std::stringstream;

#include <queue>
using std::priority_queue;

VI_Texture *MapSquare::mapTextures[N_TYPES];
VI_Texture *MapSquare::mapOverlays[N_TYPES][n_map_overlays_c];
//VI_Texture *MapSquare::roadTextures[9];
VI_Texture *MapSquare::combinedRoadTextures[256];
VI_Texture *MapSquare::combinedRailwayTextures[256];

MapSquare::MapSquare() :
mainGamestate(NULL), pos(-1, -1), type(TYPE_GRASSLAND), road(ROAD_NONE), progress_road(0), bonus_resource(NULL),
city(NULL), territory(NULL), target(false) {
}

MapSquare::~MapSquare() {
}

void MapSquare::validateType(Type type) {
	int i_type = (int)type;
	ASSERT( i_type >= 0 && i_type < N_TYPES );
}

int MapSquare::getRoadCost() const {
	switch( type ) {
		case TYPE_OCEAN:
			ASSERT( false );
			break;
		case TYPE_GRASSLAND:
			return 3;
		case TYPE_FOREST:
		case TYPE_DESERT:
		case TYPE_ARTIC:
			return 4;
		case TYPE_HILLS:
			return 5;
		case TYPE_MOUNTAINS:
			return 6;
	}
	ASSERT(false);
	return -1;
}

VI_Texture *MapSquare::getOverlayTexture(int index) const {
	ASSERT( index >= 0 && index < n_map_overlays_c );
	return mapOverlays[type][index];
}

bool MapSquare::drawAboveRoad() const {
	return type == TYPE_FOREST;
}

void MapSquare::setProgressRoad(int progress_road) {
	T_ASSERT( progress_road < this->getRoadCost() );
	this->progress_road = progress_road;
}

bool MapSquare::buildRoad(const Unit *unit) {
	T_ASSERT( this->road == ROAD_NONE );
	int required_progress = this->getRoadCost();
	T_ASSERT( this->progress_road < required_progress );
	int increase = 1;
	if( unit->getCivilization()->hasBonus(Civilization::BONUS_WORKERS) )
		increase = 2;
	this->progress_road += increase;
	if( this->progress_road >= required_progress ) {
		this->road = ROAD_BASIC;
		this->progress_road = 0;
		return true;
	}
	return false;
}

bool MapSquare::buildRailways(const Unit *unit) {
	T_ASSERT( this->road == ROAD_BASIC );
	int required_progress = this->getRoadCost();
	T_ASSERT( this->progress_road < required_progress );
	int increase = 1;
	if( unit->getCivilization()->hasBonus(Civilization::BONUS_WORKERS) )
		increase = 2;
	this->progress_road += increase;
	if( this->progress_road >= required_progress ) {
		this->road = ROAD_RAILWAYS;
		this->progress_road = 0;
		return true;
	}
	return false;
}

/*bool MapSquare::hasWorkers() const {
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		const Unit *unit = *iter;
		if( unit->getStatus() == Unit::STATUS_BUILDING_ROAD ) {
			return true;
		}
	}
	return false;
}*/

bool MapSquare::canBeImproved(const Civilization *civilization) const {
	if( this->isLand() ) {
		if( this->road == ROAD_NONE )
			return true;
		else if( this->road == ROAD_BASIC ) {
			//const Technology *tech = game_g->getGameData()->findTechnology("Steam Power");
			//if( civilization->hasTechnology(tech) )
			if( civilization->hasTechnology("Steam Power") )
				return true;
			return false;
		}
	}
	return false;
}

bool MapSquare::canMoveTo(const UnitTemplate *unit_template) const {
	// for now, unit_template is ignored, but if it's ever implemented, we need to allow it to be NULL, which means to consider a generic land unit
	bool ok = true;
	switch( type ) {
		case TYPE_OCEAN:
		case TYPE_MOUNTAINS:
			ok = false;
			break;
		case TYPE_GRASSLAND:
		case TYPE_DESERT:
		case TYPE_ARTIC:
		case TYPE_FOREST:
		case TYPE_HILLS:
			ok = true;
			break;
		default:
			ASSERT(false);
	}
	return ok;
}

void MapSquare::setBonusResource(const BonusResource *bonus_resource) {
	this->bonus_resource = bonus_resource;
	if( bonus_resource != NULL ) {
		//ASSERT( bonus_resource->getTerrain() == this->type );
		ASSERT( bonus_resource->hasTerrain( this->type ) );
	}
}

const char *MapSquare::getName() const {
	switch( type ) {
		case TYPE_OCEAN:
			return "Ocean";
		case TYPE_GRASSLAND:
			return "Grassland";
		case TYPE_FOREST:
			return "Forest";
		case TYPE_HILLS:
			return "Hills";
		case TYPE_MOUNTAINS:
			return "Mountains";
		case TYPE_DESERT:
			return "Desert";
		case TYPE_ARTIC:
			return "Artic";
	}
	ASSERT(false);
	return NULL;
}

Rational MapSquare::getMovementCost(const Civilization *civilization) const {
	/*if( this->road == ROAD_BASIC ) {
		return Rational(1, road_move_scale_c);
		//return 0;
	}
	else if( this->road == ROAD_RAILWAYS ) {
		return 0;
	}*/
	if( this->road != ROAD_NONE ) {
		bool ok = false;
		if( this->territory == NULL || civilization == this->territory ) {
			ok = true;
		}
		else {
			const Relationship *relationship = mainGamestate->findRelationship(this->territory, civilization);
			if( relationship->getStatus() == Relationship::STATUS_PEACE ) {
				ok = true;
			}
		}

		if( ok ) {
			if( this->road == ROAD_BASIC ) {
				return Rational(1, road_move_scale_c);
			}
			else if( this->road == ROAD_RAILWAYS ) {
				return 0;
			}
		}
	}
	switch( type ) {
		case TYPE_OCEAN:
			return 1;
		case TYPE_GRASSLAND:
			return 1;
		case TYPE_FOREST:
			return 2;
		case TYPE_HILLS:
			return 2;
		case TYPE_DESERT:
			return 1;
		case TYPE_ARTIC:
			return 2;
		case TYPE_MOUNTAINS:
			return 3;
		default:
			ASSERT(false);
	}
	return 0;
}

int MapSquare::getScaledMovementCost(const Civilization *civilization) const {
	Rational mp_r = this->getMovementCost(civilization);
	mp_r *= road_move_scale_c;
	int mp, mp_num, mp_den;
	mp_r.value(&mp, &mp_num, &mp_den);
	ASSERT( mp_num == 0 );
	return mp;
}

void MapSquare::setCity(City *city, bool update) {
	// The update flag controls whether to update territory and map display.
	// If adding multiple cities (e.g., when loading a game), update should
	// be set to false for performance - but the territory and map should be
	// updated at the end.
	this->city = city;
	//VI_log("calculate territory for %s\n", city->getName());
	if( update ) {
		this->mainGamestate->calculateTerritory();
		this->mainGamestate->refreshMapDisplay();
	}
	//const Civilization *civ = this->mainGamestate->getMap()->getSquare(32, 9)->getTerritory();
	//VI_log("### %s\n", civ==NULL ? "NULL" : civ->getName());
}

bool MapSquare::hasForeignUnits(const Civilization *civ) const {
	if( units.size() > 0 && units.at(0)->getCivilization() != civ ) {
		return true;
	}
	else if( city != NULL && city->getCivilization() != civ ) {
		return true;
	}
	return false;
}

bool MapSquare::hasEnemies(const Civilization *civ) const {
	if( units.size() > 0 && units.at(0)->getCivilization() != civ ) {
		const Relationship *relationship = mainGamestate->findRelationship(units.at(0)->getCivilization(), civ);
		if( relationship->getStatus() == Relationship::STATUS_WAR ) {
			for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
				const Unit *unit = *iter;
				if( unit->getTemplate()->getAttack() > 0 ) {
					return true;
				}
			}
		}
	}
	return false;
}

Unit *MapSquare::getBestDefender() const {
	Unit *defender = NULL;
	int best_defence = 0;
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		Unit *this_unit = *iter;
		// note, even if the unit template is air or sea unit, we still carry on here (if a square only has air or sea units, we still want to return one of them)
		int defence = this_unit->getTemplate()->getDefence();
		// find best defender - for equal defences, choose one with lower attack (so only the weaker unit is put at risk of being destroyed)
		if( defender == NULL || defence > best_defence || ( defence == best_defence && this_unit->getTemplate()->getAttack() < defender->getTemplate()->getAttack() ) ) {
			defender = this_unit;
			best_defence = defence;
		}
	}
	return defender;
}

Unit *MapSquare::getBestSeaUnit() const {
	if( this->city == NULL ) {
		return NULL; // sea units are only located at cities
	}
	Unit *unit = NULL;
	int best_attack = 0;
	for(vector<Unit *>::const_iterator iter = units.begin(); iter != units.end(); ++iter) {
		Unit *this_unit = *iter;
		if( !this_unit->getTemplate()->isSea() )
			continue;
		int attack = this_unit->getTemplate()->getSeaAttack();
		if( unit == NULL || attack > best_attack ) {
			unit = this_unit;
			best_attack = attack;
		}
	}
	return unit;
}

void MapSquare::removeUnit(const Unit *unit) {
	vector<Unit *>::iterator iter = find(units.begin(), units.end(), unit);
	ASSERT( iter != units.end() );
	units.erase(iter);
}

void MapSquare::loadTextures() {
	for(int i=0;i<N_TYPES;i++) {
		mapTextures[i] = NULL;
		for(int j=0;j<n_map_overlays_c;j++)
			mapOverlays[i][j] = NULL;
	}
	/*for(int i=0;i<9;i++) {
		roadTextures[i] = NULL;
	}*/
	for(int i=0;i<256;i++) {
		combinedRoadTextures[i] = NULL;
		combinedRailwayTextures[i] = NULL;
	}

	VI_Image *image = NULL;

	unsigned char filter_max_ocean[3] = {180, 215, 240};
	unsigned char filter_min_ocean[3] = {60, 95, 115};
	image = VI_Image::createNoise(64, 64, 4.0f, 4.0f, filter_max_ocean, filter_min_ocean, V_NOISEMODE_PERLIN, 4, false);
	mapTextures[TYPE_OCEAN] = VI_createTexture(image);

	unsigned char filter_max_grassland[3] = {110, 135, 90};
	unsigned char filter_min_grassland[3] = {50, 75, 40};
	image = VI_Image::createNoise(64, 64, 8.0f, 8.0f, filter_max_grassland, filter_min_grassland, V_NOISEMODE_PERLIN, 4, false);
	mapTextures[TYPE_GRASSLAND] = VI_createTexture(image);

	mapTextures[TYPE_FOREST] = mapTextures[TYPE_GRASSLAND]; // uses same base
	mapTextures[TYPE_HILLS] = mapTextures[TYPE_GRASSLAND]; // uses same base
	mapTextures[TYPE_MOUNTAINS] = mapTextures[TYPE_GRASSLAND]; // uses same base

	unsigned char filter_max_desert[3] = {225, 190, 160};
	unsigned char filter_min_desert[3] = {185, 150, 120};
	image = VI_Image::createNoise(64, 64, 8.0f, 8.0f, filter_max_desert, filter_min_desert, V_NOISEMODE_PERLIN, 4, false);
	mapTextures[TYPE_DESERT] = VI_createTexture(image);

	unsigned char filter_max_artic[3] = {250, 250, 255};
	unsigned char filter_min_artic[3] = {205, 200, 210};
	image = VI_Image::createNoise(64, 64, 8.0f, 8.0f, filter_max_artic, filter_min_artic, V_NOISEMODE_PERLIN, 4, false);
	mapTextures[TYPE_ARTIC] = VI_createTexture(image);

	mapOverlays[TYPE_FOREST][0] = VI_createTexture(getFullPath("data/gfx/mapoverlay_forest.png").c_str());
	mapOverlays[TYPE_HILLS][0] = VI_createTexture(getFullPath("data/gfx/mapoverlay_hills.png").c_str());
	mapOverlays[TYPE_MOUNTAINS][0] = VI_createTexture(getFullPath("data/gfx/mapoverlay_mountains.png").c_str());
	mapOverlays[TYPE_FOREST][1] = VI_createTexture(getFullPath("data/gfx/mapoverlay_forest1.png").c_str());
	mapOverlays[TYPE_HILLS][1] = VI_createTexture(getFullPath("data/gfx/mapoverlay_hills1.png").c_str());
	mapOverlays[TYPE_MOUNTAINS][1] = VI_createTexture(getFullPath("data/gfx/mapoverlay_mountains1.png").c_str());

	VI_Image **roadImages = new VI_Image *[9];
	for(int i=0;i<9;i++) {
		stringstream name;
		name << getFullPath("data/gfx/roads_") << i << ".png";
		roadImages[i] = VI_createImage(name.str().c_str());
	}

	for(int i=0;i<256;i++) {
		VI_Image *image = VI_createImage(64, 64);
		for(int y=0;y<image->getHeight();y++) {
			for(int x=0;x<image->getWidth();x++) {
				image->putRGB(x, y, 255, 0, 255);
			}
		}
		bool any = false;
		for(int j=0;j<8;j++) {
			int mask = 1 << j;
			int bit = i & mask;
			if( bit > 0 ) {
				any = true;
				image->merge(roadImages[j]);
			}
		}
		if( !any ) {
			image->merge(roadImages[8]);
		}
		//combinedRoadTextures[i] = VI_createTexture(image);
		combinedRoadTextures[i] = VI_createTextureWithMask(image, 255, 0, 255);
	}

	VI_Image **railwayImages = new VI_Image *[9];
	for(int i=0;i<9;i++) {
		stringstream name;
		name << getFullPath("data/gfx/railways_") << i << ".png";
		railwayImages[i] = VI_createImage(name.str().c_str());
	}

	for(int i=0;i<256;i++) {
		VI_Image *image = VI_createImage(64, 64);
		for(int y=0;y<image->getHeight();y++) {
			for(int x=0;x<image->getWidth();x++) {
				image->putRGB(x, y, 255, 0, 255);
			}
		}
		bool any = false;
		for(int j=0;j<8;j++) {
			int mask = 1 << j;
			int bit = i & mask;
			if( bit > 0 ) {
				any = true;
				image->merge(railwayImages[j]);
			}
		}
		if( !any ) {
			image->merge(railwayImages[8]);
		}
		combinedRailwayTextures[i] = VI_createTextureWithMask(image, 255, 0, 255);
	}

	/*for(int i=0;i<9;i++) {
		roadTextures[i] = VI_createTexture(roadImages[i]);
	}*/
}

bool MapSquare::canBuildCity() const {
	//if( this->isLand() )
	//	return false;
	if( type != TYPE_GRASSLAND && type != TYPE_FOREST && type != TYPE_HILLS &&
		type != TYPE_DESERT && type != TYPE_ARTIC ) {
			return false;
	}
	for(size_t i=0;i<mainGamestate->getNCivilizations();i++) {
		const Civilization *civilization = mainGamestate->getCivilization(i);
		for(size_t j=0;j<civilization->getNCities();j++) {
			const City *city = civilization->getCity(j);
			int dist_x = (city->getX() - pos.x);
			int dist_y = (city->getY() - pos.y);
			this->mainGamestate->getMap()->reduceDiff(&dist_x, &dist_y);
			dist_x = abs(dist_x);
			dist_y = abs(dist_y);
			const int min_gap_c = 4;
			//if( dist_x <= min_gap_c && dist_y <= min_gap_c ) {
			//	return false;
			//}
			if( dist_x > min_gap_c || dist_y > min_gap_c ) {
				// okay
			}
			else if( ( dist_x >= 3 && dist_y >= 4 ) || ( dist_x >= 4 && dist_y >= 3 ) ) {
				// still okay
			}
			else {
				return false;
			}
		}
	}
	return true;
}

Map::Map(MainGamestate *mainGamestate) : mainGamestate(mainGamestate), width(0), height(0), topology(TOPOLOGY_UNDEFINED), squares(NULL), splat_alpha(NULL) {
}

Map::Map(MainGamestate *mainGamestate, int width, int height) : mainGamestate(mainGamestate), width(width), height(height), topology(TOPOLOGY_FLAT), splat_alpha(NULL) {
	init();
}

Map::~Map() {
	free();
}

void Map::init() {
	VI_log("Map::init()\n");
	//int time_s = clock();
	this->squares = new MapSquare[width*height];
	//VI_log("time alloc: %d\n", clock() - time_s);
	for(int y=0;y<height;y++) {
		for(int x=0;x<width;x++) {
			this->getSquare(x, y)->setMaingameState(mainGamestate);
			this->getSquare(x, y)->setPos(Pos2D(x, y));
		}
	}
	//VI_log("time all: %d\n", clock() - time_s);
	VI_log("...done\n");
}

void Map::free() {
	if( this->squares != NULL ) {
		delete [] this->squares;
		this->squares = NULL;
	}
	this->width = 0;
	this->height = 0;
	if( splat_alpha ) {
		delete splat_alpha;
		splat_alpha = NULL;
	}
}

void Map::createAlphamap() {
	//ASSERT( game->splat_pixel_shader_ambient != NULL );
	T_ASSERT( this->splat_alpha == NULL );
	VI_Image *alphamap = VI_createImage(width, height);
	for(int y=0;y<height;y++) {
		for(int x=0;x<width;x++) {
			const MapSquare *square = this->getSquare(x, y);
			if( square->isLand() ) {
				if( square->getType() == TYPE_DESERT )
					alphamap->putRGB(x, y, 0, 255, 0);
				else if( square->getType() == TYPE_ARTIC )
					alphamap->putRGB(x, y, 0, 0, 255);
				else
					//alphamap->putRGB(x, y, 255, 0, 0);
					alphamap->putRGB(x, y, 0, 0, 0);
			}
			else {
				//alphamap->putRGB(x, y, 0, 0, 0);
				alphamap->putRGB(x, y, 255, 0, 0);
			}
		}
	}
	alphamap->makePowerOf2();
	Renderer *renderer = game_g->getGraphicsEnvironment()->getRenderer();
	// so that the splat_alpha doesn't get flushed, as it's created before we close the OptionsGamestate, and we delete in manually ourselves anyway (TODO: can this be improved?)
	VI_set_default_persistence(-1);
	this->splat_alpha = renderer->createTexture(alphamap);
	VI_set_default_persistence(0);
}

void Map::createRandomBonuses() {
	const int bonus_chance = 15;
	const int bonus_chance_artic_desert = 4;
	const int bonus_chance_ocean = 2;
	const int bonus_chance_grassland = 1;
	for(int y=0;y<height;y++) {
		for(int x=0;x<width;x++) {
			this->getSquare(x, y)->setBonusResource(NULL);
			Type type = this->getSquare(x, y)->getType();
			int this_bonus_chance = bonus_chance;
			if( type == TYPE_ARTIC || type == TYPE_DESERT )
				this_bonus_chance = bonus_chance_artic_desert;
			else if( type == TYPE_OCEAN )
				this_bonus_chance = bonus_chance_ocean;
			else if( type == TYPE_GRASSLAND )
				this_bonus_chance = bonus_chance_grassland;
			if( rand() % 100 <= this_bonus_chance ) {
				vector<const BonusResource *> candidates;
				for(size_t i=0;i<game_g->getGameData()->getNBonusResources();i++) {
					const BonusResource *bonus_resource = game_g->getGameData()->getBonusResource(i);
					//if( bonus_resource->getTerrain() == type ) {
					if( bonus_resource->hasTerrain(type) ) {
						candidates.push_back(bonus_resource);
					}
				}
				if( candidates.size() > 0 ) {
					int r = rand() % candidates.size();
					const BonusResource *bonus_resource = candidates.at(r);
					this->getSquare(x, y)->setBonusResource(bonus_resource);
				}
			}
		}
	}
}

void Map::createRandom() {
	VI_log("create random map: %d x %d\n", width, height);
	ASSERT( width > 0 && height > 0 );
	unsigned char filter_max[] = {255, 0, 0};
	unsigned char filter_min[] = {0, 0, 0};
	VI_initPerlin(); // reset for random seed
	VI_Image *image = VI_Image::createNoise(width, height, 5.0f, 5.0f, filter_max, filter_min, V_NOISEMODE_PERLIN, 10, false);
	//VI_Image *image = VI_createImageNoise(width, height, 1.0f, filter_max, filter_min, V_NOISEMODE_PERLIN, 10, false);

	VI_log("processing map...\n");
	// scale
	unsigned char min_r = 255, max_r = 0;
	for(int y=0;y<height;y++) {
		for(int x=0;x<width;x++) {
			unsigned char r = 0, g = 0, b = 0;
			image->getRGB(&r, &g, &b, x, y);
			if( r < min_r )
				min_r = r;
			if( r > max_r )
				max_r = r;
		}
	}
	float scale_r = 255.0f / ( ((float)max_r) - ((float)min_r) );
	for(int y=0;y<height;y++) {
		for(int x=0;x<width;x++) {
			unsigned char r = 0, g = 0, b = 0;
			image->getRGB(&r, &g, &b, x, y);
			float fr = r;
			fr -= (float)min_r;
			fr *= scale_r;
			if( fr < 0.0f )
				fr = 0.0f;
			else if( fr > 255.0f )
				fr = 255.0f;
			r = (unsigned char)fr;
			image->putRGB(x, y, r, g, b);
		}
	}

	const int forest_coverage = 10;
	const int land_coverage = 48;
	const int artic_extent = (int)(0.10 * height);
	const int desert_extent = (int)(0.05 * height);
	this->setAllTo(TYPE_OCEAN);
	int sx = 0, ex = 1;
	if( topology == TOPOLOGY_FLAT ) {
		// avoid land on boundaries
		sx = 1;
		ex = width-1;
	}
	else if( topology == TOPOLOGY_CYLINDER ) {
		sx  = 0;
		ex = width;
	}
	else {
		ASSERT( false );
	}
	for(int y=1;y<height-1;y++) {
		//VI_log("#");
		for(int x=sx;x<ex;x++) {
			unsigned char r = 0, g = 0, b = 0;
			image->getRGB(&r, &g, &b, x, y);
			//VI_log("%d,", r);
			T_ASSERT( g == 0 && b == 0 );
			Type type = TYPE_OCEAN;
			float fr = ((float)r) / 255.0f;
			/*if( r < 127 ) {
				type = TYPE_OCEAN;
			}
			else if( r < 175 ) {
				type = TYPE_GRASSLAND;
			}
			else if( r < 185 ) {
				type = TYPE_HILLS;
			}
			else {
				type = TYPE_MOUNTAINS;
			}*/
			if( 100.0f * fr <= 100.0f - land_coverage ) {
				type = TYPE_OCEAN;
			}
			else {
				float scaled_fr = ( 100.0f * fr - 100.0f + land_coverage ) / land_coverage;
				/*if( scaled_fr < 0.375f )
					type = TYPE_GRASSLAND;
				else if( scaled_fr < 0.45f )
					type = TYPE_HILLS;
				else
					type = TYPE_MOUNTAINS;*/
				if( scaled_fr < 0.55f )
					type = TYPE_GRASSLAND;
				else if( scaled_fr < 0.8f )
					type = TYPE_HILLS;
				else
					type = TYPE_MOUNTAINS;
				int flip_chance = 20;
				if( rand() % flip_chance == 0 ) {
					if( type == TYPE_GRASSLAND )
						type = rand() % 3 <= 1 ? TYPE_HILLS : TYPE_MOUNTAINS;
					else
						type = TYPE_GRASSLAND;
				}
			}

			if( type == TYPE_GRASSLAND ) {
				int forest = rand() % 100;
				if( forest < forest_coverage )
					type = TYPE_FOREST;
				else {
					const int var = 3;
					int ty = y + ( rand() % (2*var+1) ) - var;
					if( ty < artic_extent || height-1-ty < artic_extent ) {
						type = TYPE_ARTIC;
					}
					else if( ty >= height/2 - desert_extent && ty < height/2 + desert_extent ) {
						type = TYPE_DESERT;
					}
				}
			}
			this->getSquare(x, y)->setType(type);
		}
		//VI_log("\n");
	}

	this->createRandomBonuses();

	delete image;
	this->createAlphamap();
}

bool Map::readFile(const char *filename) {
	free();

	FILE *file = fopen(filename, "r");
	if( file == NULL )
		return false;

	const int BUFSIZE = 1024;
	char buffer[BUFSIZE+1];
	if( fgets(buffer, BUFSIZE, file) == NULL )
		return false;
	this->width = atoi(buffer);
	if( fgets(buffer, BUFSIZE, file) == NULL )
		return false;
	this->height = atoi(buffer);
	if( this->width <= 0 || this->height <= 0 )
		return false;

	if( fgets(buffer, BUFSIZE, file) == NULL )
		return false;
	int i_topology = atoi(buffer);
	if( i_topology != (int)TOPOLOGY_FLAT && i_topology != (int)TOPOLOGY_CYLINDER ) {
		VI_log("unknown topology type\n");
		return false;
	}
	this->topology = (Topology)i_topology;

	init();
	for(int i=0;i<width*height;i++) {
		this->squares[i].setType(TYPE_OCEAN);
	}

    int max_line = width+3; // allow for up to 2 newline characters (this will happen when reading Windows-saved text files on Linux), and the NULL terminator
	char *line = new char[max_line];
	for(int y=0;y<height;y++) {
        if( fgets(line, max_line, file) == NULL ) {
            delete [] line;
            delete [] this->squares;
            this->squares = NULL;
            this->width = 0;
            this->height = 0;
            fclose(file);
            return false;
        }
		for(int x=0;x<width;x++) {
			Type type = static_cast<Type>(line[x] - '0');
			/*if( type != TYPE_OCEAN && type != TYPE_HILLS )
			type = TYPE_GRASSLAND;*/
            int i_type = (int)type;
            if( !( i_type >= 0 && i_type < N_TYPES ) ) {
                VI_log("map type at %d, %d is invalid: %d\n", x, y, i_type);
                delete [] line;
                delete [] this->squares;
                this->squares = NULL;
                this->width = 0;
                this->height = 0;
                fclose(file);
                return false;
            }
			this->getSquare(x, y)->setType(type);
			/*if( type == TYPE_GRASSLAND ) {
			this->getSquare(x, y)->setRoad(ROAD_BASIC);
			}*/
		}
	}

	this->createAlphamap();
	fclose(file);
    delete [] line;
	return true;
}

int Map::distanceChebyshev(Pos2D p0, Pos2D p1) const {
	ASSERT( isValid(p0.x, p0.y) );
	ASSERT( isValid(p1.x, p1.y) );
	reduceToBase(&p0.x, &p0.y);
	reduceToBase(&p1.x, &p1.y);
	int dx = 0;
	if( topology == TOPOLOGY_FLAT ) {
		dx = abs(p0.x - p1.x);
	}
	else if( topology == TOPOLOGY_CYLINDER ) {
		// handle wraparound
		int dx0 = abs(p0.x - p1.x);
		int dx1 = width - abs(p0.x - p1.x);
		dx = min(dx0, dx1);
	}
	else {
		ASSERT( false );
	}
	int dy = abs(p0.y - p1.y);
	int dist = max(dx, dy);
	return dist;
}

void Map::reduceScreenPosToBase(int *x, int *y) const {
	if( topology == TOPOLOGY_CYLINDER ) {
		int screen_w = game_g->getGraphicsEnvironment()->getWidth();
		int whole_w = this->width * mainGamestate->getSqWidth();
		while( *x < 0 )
			*x += whole_w;
		while( *x >= screen_w )
			*x -= whole_w;
	}
}

City *Map::findCity(int x, int y) const {
	/*for(int i=0;i<game->getNCivilizations();i++) {
	const Civilization *civilization = game->getCivilization(i);
	for(int j=0;j<civilization->getNCities();j++) {
	City *city = civilization->getCity(j);
	if( city->getX() == x && city->getY() == y )
	return city;
	}
	}
	return NULL;*/
	City *city = this->getSquare(x, y)->getCity();
	if( city != NULL ) {
		this->reduceToBase(&x, &y);
		ASSERT( city->getX() == x && city->getY() == y );
	}
	return city;
}

const vector <Unit *> *Map::findUnitsAt(int x, int y) const {
	//VI_log("Map::findUnitsAt(%d, %d)\n", x, y);
	/*vector<Unit *> *units = new vector<Unit *>();
	for(int i=0;i<game->getNCivilizations();i++) {
	const Civilization *civilization = game->getCivilization(i);
	for(int j=0;j<civilization->getNUnits();j++) {
	Unit *unit = civilization->getUnit(j);
	if( unit->getX() == x && unit->getY() == y )
	units->push_back(unit);
	}
	}
	return units;*/
	//const vector<Unit *> *units = this->getSquare(x, y)->getUnits();
	const vector<Unit *> *units = this->getSquare(x, y)->getUnits();
	if( units->size() > 0 ) {
		const Unit *unit = units->at(0);
		this->reduceToBase(&x, &y);
		ASSERT( unit->getX() == x && unit->getY() == y );
	}
	return units;
}

Civilization *Map::findCivilizationAt(Pos2D pos) const {
	// civilization isn't owned by this class, so okay to return non-const in const member function
	const MapSquare *square = this->getSquare(pos);
	const City *city = square->getCity();
	if( city != NULL )
		return city->getCivilization();
	const vector<Unit *> *units = square->getUnits();
	if( units->size() > 0 )
		return units->at(0)->getCivilization();
	return NULL;
}

/*bool Map::canBuildCity(int x, int y) const {
const MapSquare *square = getSquare(x, y);
//if( square->isLand() )
//	return false;
Type squareType = square->getType();
if( squareType != TYPE_GRASSLAND && squareType != TYPE_FOREST && squareType != TYPE_HILLS &&
squareType != TYPE_DESERT && squareType != TYPE_ARTIC ) {
return false;
}
for(int i=0;i<game->getNCivilizations();i++) {
const Civilization *civilization = game->getCivilization(i);
for(int j=0;j<civilization->getNCities();j++) {
const City *city = civilization->getCity(j);
int dist_x = abs(city->getX() - x);
int dist_y = abs(city->getY() - y);
const int min_gap_c = 4;
//if( dist_x <= min_gap_c && dist_y <= min_gap_c ) {
//	return false;
//}
if( dist_x > min_gap_c || dist_y > min_gap_c ) {
// okay
}
else if( ( dist_x >= 3 && dist_y >= 4 ) || ( dist_x >= 4 && dist_y >= 3 ) ) {
// still okay
}
else {
return false;
}
}
}
return true;
}*/

bool Map::findRandomFreeSquare(Pos2D *pos) const {
	*pos = Pos2D();
	const int mindist = 2;
	vector<Pos2D> freesquares;
	for(int y=0;y<height;y++) {
		for(int x=0;x<width;x++) {
			const MapSquare *square = this->getSquare(x, y);
			Type type = square->getType();
			if( type == TYPE_GRASSLAND || type == TYPE_FOREST || type == TYPE_HILLS || type == TYPE_DESERT || type == TYPE_ARTIC ) {
				bool ok = true;
				for(int cy=y-mindist;cy<=y+mindist && ok;cy++) {
					for(int cx=x-mindist;cx<=x+mindist && ok;cx++) {
						if( this->isValid(cx, cy) ) {
							const MapSquare *this_square = this->getSquare(cx, cy);
							if( this_square->getCity() != NULL || this_square->getUnits()->size() > 0 ) {
								ok = false;
							}
						}
					}
				}
				if( ok ) {
					Pos2D pos(x, y);
					freesquares.push_back(pos);
				}
			}
		}
	}
	if( freesquares.size() == 0 ) {
		return false;
	}
	int index = rand() % freesquares.size();
	*pos = freesquares.at(index);
	return true;
}

bool Map::isOnOrAdjacent(Pos2D pos, Type type) const {
	for(int y=pos.y-1;y<=pos.y+1;y++) {
		for(int x=pos.x-1;x<=pos.x+1;x++) {
			if( this->isValid(x, y) ) {
				const MapSquare *square = this->getSquare(x, y);
				if( square->getType() == type ) {
					return true;
				}
			}
		}
	}
	return false;
}

bool Map::isWithinCityRadius(Pos2D pos, Type type, bool include_centre) const {
	for(int i=0;i<City::getNCitySquares();i++) {
		if( !include_centre && i == City::getCentreCitySquareIndex() ) {
			continue;
		}
		Pos2D this_pos = City::getCitySquare(this, i, pos);
		if( this->isValid(this_pos.x, this_pos.y) ) {
			const MapSquare *square = this->getSquare(this_pos);
			if( square->getType() == type ) {
				return true;
			}
		}
	}
	return false;
}

void Map::ensureNearbyTerrain(Pos2D pos, Type type) {
	T_ASSERT( type != TYPE_OCEAN );
	for(int y=pos.y-1;y<=pos.y+1;y++) {
		for(int x=pos.x-1;x<=pos.x+1;x++) {
			if( this->isValid(x, y) ) {
				MapSquare *square = this->getSquare(x, y);
				if( square->getType() == type ) {
					// already present
					return;
				}
			}
		}
	}
	for(int y=pos.y-1;y<=pos.y+1;y++) {
		for(int x=pos.x-1;x<=pos.x+1;x++) {
			if( this->isValid(x, y) ) {
				MapSquare *square = this->getSquare(x, y);
				if( square->isLand() && square->getType() != type ) {
					square->setType(type);
					return;
				}
			}
		}
	}
}

/*void Map::calculateDistanceMap_processQueueItem(int indx, const Distance *dists, Distance *temp, vector<int> *queue, bool reached_target, const Civilization *civilization, const Unit *unit, bool forwards, int ex, int ey, bool by_sea) const {
}*/

#if 0
Distance *Map::calculateDistanceMap(const Civilization *civilization, const Unit *unit, bool include_travel, bool forwards, int sx, int sy, int ex, int ey, bool by_sea) {
	// if unit is NULL, assume general land unit, unless by_sea is true
	ASSERT( civilization != NULL );
	//VI_log("Map::calculateDistanceMap from %d, %d\n", sx, sy);
	// N.B. - the AI cheats here, in that we also know the distance map for unexplored area.
	// To be fairer, we should estimate the movement cost always as (1*3) for unexplored squares?
	int time_s = clock();
	//const int MAX_VALUE = 32767;
	Distance *dists = new Distance[width*height];
	Distance *temp = new Distance[width*height];
	/*for(int i=0;i<width*height;i++)
		dists[i] = -1;*/
	//int *queue = new int[width*height];
	vector<int> queue;
	queue.push_back(sy * width + sx);
	int c_queue = 0;
	//int n_queue = 1;

	dists[sy * width + sx].cost = 0;
	dists[sy * width + sx].squares = 0;

	bool reached_target = false;

	if( include_travel ) {
		const City *city = this->findCity(sx, sy);
		if( city != NULL ) {
			// TODO: cache target squares from City
			city->initTravel(false, unit == NULL ? NULL : unit->getTemplate());
			for(int i=0;i<width*height && !reached_target;i++) {
				if( squares[i].isTarget() ) {
					dists[i].cost = 1;
					dists[i].squares = 1;
					//queue[n_queue++] = i;
					queue.push_back(i);
					if( i == ey * width + ex ) {
						reached_target = true;
					}
				}
			}
			this->resetTargets();
		}
	}

	while( !reached_target ) {
		for(int i=0;i<width*height;i++)
			temp[i] = dists[i];

		// process items in queue
		///int old_n_queue = n_queue;
		int old_n_queue = queue.size();
		for(int i=c_queue;i<old_n_queue && !reached_target;i++) {
			int indx = queue[i];
			//this->calculateDistanceMap_processQueueItem(indx, dists, temp, &queue, reached_target, civilization, unit, forwards, ex, ey, by_sea);

			int cost = dists[indx].cost;
			int squares = dists[indx].squares;
			ASSERT( cost != -1 );
			ASSERT( squares != -1 );
			int x = indx % width;
			int y = indx / width;
			//LOG("process queue: %d : %d ( %d, %d) = %d\n", i, indx, x, y, cost);
			const MapSquare *from_square = this->getSquare(x, y);

			for(int cy=y-1;cy<=y+1 && !(reached_target);cy++) {
				for(int cx=x-1;cx<=x+1 && !(reached_target);cx++) {

					if( cx == x && cy == y )
						continue;

					if( !isValid(cx,cy) )
						continue;

					//if( dists[cy*width + cx] == -1 )
					{
						const MapSquare *square = this->getSquare(cx, cy);
						bool valid_move = false;
						if( by_sea )
							valid_move = !square->isLand();
						else if( unit == NULL ) {
							// general land unit
							valid_move = square->canMoveTo(NULL);
						}
						else {
							// also take into account other units there, and if we can attack
							valid_move = unit->canMoveTo( Pos2D(cx, cy) );
						}
						/*if( ( unit == NULL && ( square->isLand() == !by_sea ) ) ||
						( unit != NULL && unit->canMoveTo( Pos2D(cx, cy) ) ) ) {*/
						if( valid_move ) {
							int mp = forwards ? square->getScaledMovementCost(civilization) : from_square->getScaledMovementCost(civilization);
							if( unit == NULL )
								mp = min(mp,road_move_scale_c);
							else
								mp = min(mp,road_move_scale_c*unit->getTemplate()->getMoves());
							int val = cost + mp;
							//ASSERT( val <= MAX_VALUE );

							// add to queue if not already present

							bool cheaper = false;
							int c_val = temp[cy * width + cx].cost;
							if( c_val == -1 || val <= c_val ) {
								if( c_val == -1 || val < c_val ) {
									temp[cy * width + cx].cost = val;
									cheaper = true;
								}
								int c_squares = temp[cy * width + cx].squares;
								if( c_squares == -1 || val < c_val || squares+1 < c_squares ) { // if val < c_val (i.e., we've found a route with lower cost), we should always reset the number of squares
									temp[cy * width + cx].squares = squares+1;
									cheaper = true;
								}
							}
							if( cheaper ) {
								queue.push_back(cy*width + cx);
							}
							// TODO:
							/*if( cx == ex && cy == ey ) {
							*reached_target = true;
							}*/
							/*if( stop_x != -1 && stop_y != -1 ) {
							int diffx = abs( cx - stop_x );
							int diffy = abs( cy - stop_y );
							if( diffx <= 1 && diffy <= 1 ) {
							check_stop = true;
							}
							}*/
						}
					}

				}
			}
		}

		c_queue = old_n_queue;
		if( c_queue == queue.size() ) {
			break;
		}

		swap(temp, dists);
	}

	//VI_log("### %d\n", queue.size());
	delete [] temp;
	//delete [] queue;
	//VI_log("### Map::calculateDistanceMap %d : %d\n", unit, clock() - time_s);
	return dists;
}
#endif

// new, Dijkstra's
//#if 0

class DistanceComparison {
public:
	bool operator() (const Distance &lhs, const Distance &rhs) const {
		// return iff rhs < lhs
		/*if( lhs.cost == rhs.cost ) {
			if( lhs.squares == rhs.squares )
				return false; // equal (also includes cost==-1 case)
			return rhs.squares < lhs.squares;
		}
		else if( lhs.cost == -1 )
			return true;
		else if( rhs.cost == -1 )
			return false;
		return rhs.cost < lhs.cost;*/
		// don't have to worry about cost == -1 cases
		if( lhs.cost == rhs.cost ) {
			return rhs.squares < lhs.squares;
		}
		return rhs.cost < lhs.cost;
	}
};

Distance *Map::calculateDistanceMap(const Civilization *civilization, const Unit *unit, bool include_travel, bool forwards, int sx, int sy, int ex, int ey, bool by_sea, bool by_travel) {
	// if unit is NULL, assume general land unit (that's capable of attack), unless by_sea is true
	// if by_travel is true, then we ignore whether the squares are explored or not (as we'll be revealing squares within the travel distance)
	ASSERT( civilization != NULL );
	//VI_log("Map::calculateDistanceMap from %d, %d\n", sx, sy);
	int time_s = clock();
	//VI_log("Map::calculateDistanceMap() enter\n");
	int wh = width*height;
	Distance *dists = new Distance[wh];
	bool *done_squares = new bool[wh];
	for(int i=0;i<wh;i++) {
		dists[i].indx = i;
		done_squares[i] = false;
	}

	dists[sy * width + sx].cost = 0;
	dists[sy * width + sx].squares = 0;

	priority_queue<Distance, vector<Distance>, DistanceComparison> queue;
	queue.push(dists[sy * width + sx]);

	bool reached_target = false;
	bool allow_enter_enemy_territory = unit == NULL || unit->getTemplate()->getDefence() > 0;
	if( !allow_enter_enemy_territory ) {
		// okay if already in enemy territory
		const MapSquare *square = this->getSquare( unit->getPos() );
		if( square->getTerritory() != NULL && square->getTerritory() != civilization ) {
			const Relationship *relationship = mainGamestate->findRelationship(civilization, square->getTerritory());
			ASSERT( relationship != NULL );
			if( relationship->getStatus() == Relationship::STATUS_WAR ) {
				allow_enter_enemy_territory = true;
			}
		}
	}

	if( include_travel ) {
		ASSERT( !by_sea ); // when counting by sea, shouldn't be including travelling
		ASSERT( forwards ); // can only cope with forwards calculations
		const City *city = this->findCity(sx, sy);
		if( city != NULL ) {
			// TODO: cache target squares from City
			city->initTravel(false, unit == NULL ? NULL : unit->getTemplate());
			for(int i=0;i<width*height && !reached_target;i++) {
				if( squares[i].isTarget() ) {
					bool ok = true;
					if( !allow_enter_enemy_territory && squares[i].getTerritory() != NULL && squares[i].getTerritory() != civilization ) {
						const Relationship *relationship = mainGamestate->findRelationship(civilization, squares[i].getTerritory());
						ASSERT( relationship != NULL );
						if( relationship->getStatus() == Relationship::STATUS_WAR ) {
							ok = false;
						}
					}
					if( ok ) {
						ASSERT( dists[i].cost == -1 ); // shouldn't already be in the queue!
						//dists[i].cost = 1;
						dists[i].cost = unit == NULL ? road_move_scale_c : road_move_scale_c*unit->getTemplate()->getMoves();
						dists[i].squares = 1;
						//dists[i].is_travelling = true;
						queue.push(dists[i]);
						if( i == ey * width + ex ) {
							reached_target = true;
						}
					}
				}
			}
			this->resetTargets();
		}
	}

	//while( !reached_target && !queue.empty() ) {
	while( !queue.empty() ) {
		/*{
			priority_queue<Distance, vector<Distance>, DistanceComparison> queue_copy(queue);
			int count = 0;
			while( !queue_copy.empty() ) {
				const Distance dist = queue_copy.top();
				VI_log("### %d : %d : %d, %d\n", count, dist.indx, dist.cost, dist.squares);
				queue_copy.pop();
				count++;
			}
		}*/
		// find current cheapest
		const Distance cheapest_dist = queue.top();
		queue.pop();
		int cheapest = cheapest_dist.indx;
		//VI_log(">>> queue size %d\n", queue.size());

		int x = cheapest % width;
		int y = cheapest / width;
		const MapSquare *from_square = this->getSquareBase(x, y);
		// if there's an enemy unit here, we can't pass through it this turn, so it should be considered an "end"
		//const vector<Unit *> *units_at_sq = from_square->getUnits();
		const vector<Unit *> *units_at_sq = NULL;
		if( civilization->isFogOfWarVisible(x, y) ) {
			units_at_sq = from_square->getUnits();
		}
		if( forwards && units_at_sq != NULL && units_at_sq->size() > 0 && units_at_sq->at(0)->getCivilization() != civilization ) {
			// skip
		}
		// check that we've got the "cheapest" version
		else if( !done_squares[cheapest] ) {
			//VI_log("### %d : (%d,%d): %d, %d : %d\n", cheapest, cheapest % width, cheapest / width, cheapest_dist.cost, cheapest_dist.squares, done_squares[cheapest]);
			done_squares[cheapest] = true;

			// assert in sync with dists array:
			ASSERT( cheapest_dist.cost == dists[cheapest].cost );
			ASSERT( cheapest_dist.squares == dists[cheapest].squares );

			// process this square
			int cost = dists[cheapest].cost;
			/*if( cost == -1 ) {
				// done all squares that can be visited
				break;
			}*/
			//ASSERT( cost != -1 );
			int dists_squares = dists[cheapest].squares;
			//ASSERT( dists_squaressquares != -1 );

			for(int cy=y-1;cy<=y+1;cy++) {
				for(int cx=x-1;cx<=x+1;cx++) {
					// n.b., this code is performance critical!
					// see tests TEST_DISTANCES_0, TEST_DISTANCES_1

					if( cx == x && cy == y )
						continue;

					if( !isValid(cx,cy) )
						continue;

					/*if( !by_travel && !civilization->isExplored(cx, cy) ) {
						//VI_log("ping\n");
						continue;
					}*/

					int base_cx = cx, base_cy = cy;
					reduceToBase(&base_cx, &base_cy); // need to call this, so that we access the correct entry in dists array

					// now use *Base functions for performance

					//int indx = base_cy*width+base_cx;
					if( !by_travel && !civilization->isExploredBase(base_cx, base_cy) ) {
					//if( !by_travel && !civilization->isExploredBase(indx) ) {
						continue;
					}

					//const MapSquare *square = this->getSquareBase(base_cx, base_cy);
					//const MapSquare *square = &squares[indx]; // for performance
					const MapSquare *square = &squares[base_cy*width+base_cx]; // for performance
					bool valid_move = false;
					if( by_sea )
						valid_move = !square->isLand();
					else if( unit == NULL ) {
						// general land unit
						valid_move = square->canMoveTo(NULL);
					}
					else {
						// also take into account other units there, and if we can attack - if units can be seen
						bool check_enemies = civilization->isFogOfWarVisible(base_cx, base_cy);
						valid_move = unit->canMoveTo( Pos2D(base_cx, base_cy), check_enemies, true );
					}
					if( valid_move && !by_sea && !forwards ) {
						// if searching backwards, we shouldn't ever move to a square with another civ's unit, as we won't ever be able to move from that square in this go
						//const vector<Unit *> *units_at_nsq = square->getUnits();
						const vector<Unit *> *units_at_nsq = NULL;
						if( civilization->isFogOfWarVisible(base_cx, base_cy) ) {
							units_at_nsq = square->getUnits();
						}
						if( units_at_nsq != NULL && units_at_nsq->size() > 0 && units_at_nsq->at(0)->getCivilization() != civilization ) {
							valid_move = false;
						}
					}
					if( valid_move && !allow_enter_enemy_territory && square->getTerritory() != NULL && square->getTerritory() != civilization ) {
						const Relationship *relationship = mainGamestate->findRelationship(civilization, square->getTerritory() );
						ASSERT( relationship != NULL );
						if( relationship->getStatus() == Relationship::STATUS_WAR ) {
							valid_move = false;
						}
					}
					if( valid_move ) {
						int mp = forwards ? square->getScaledMovementCost(civilization) : from_square->getScaledMovementCost(civilization);
						if( unit == NULL )
							mp = min(mp,road_move_scale_c);
						else
							mp = min(mp,road_move_scale_c*unit->getTemplate()->getMoves());
						int val = cost + mp;

						int c_val = dists[base_cy * width + base_cx].cost;
						bool changed = false;
						if( c_val == -1 || val <= c_val ) {
							if( c_val == -1 || val < c_val ) {
								dists[base_cy * width + base_cx].cost = val;
								changed = true;
							}
							int c_squares = dists[base_cy * width + base_cx].squares;
							if( c_squares == -1 || val < c_val || dists_squares+1 < c_squares ) { // if val < c_val (i.e., we've found a route with lower cost), we should always reset the number of squares
								dists[base_cy * width + base_cx].squares = dists_squares+1;
								changed = true;
							}
							/*if( changed ) {
								dists[base_cy * width + base_cx].is_travelling = dists[cheapest].is_travelling;
							}*/
						}
						if( changed ) {
							// update with cheaper version
							queue.push(dists[base_cy * width + base_cx]);
						}
					}
				}
			}
		}
	}

	delete [] done_squares;

	/*static int total_time = 0;
	int time = clock() - time_s;
	total_time += time;
	static int count = 0;
	count++;
	//VI_log("Map::calculateDistanceMap() exit %d : %d\n", unit, clock() - time_s);
	VI_log("### %d Map::calculateDistanceMap %d : %d, %d\n", count, unit, time, total_time);
	//VI_log(">>> ### dist: %d\n", dists[8 * this->getWidth() + 4].cost);
	*/

	return dists;
}
//#endif

#if 0
// OLD, buggy
Distance *Map::calculateDistanceMap(const Unit *unit, bool include_travel, bool forwards, int sx, int sy, int ex, int ey, bool by_sea) {
	//VI_log("Map::calculateDistanceMap from %d, %d\n", sx, sy);
	// N.B. - the AI cheats here, in that we also know the distance map for unexplored area.
	// To be fairer, we should estimate the movement cost always as (1*3) for unexplored squares?
	int time_s = clock();
	// if unit is NULL, assume general land unit, unless by_sea is true
	const int MAX_VALUE = 32767;
	Distance *dists = new Distance[width*height];
	Distance *temp = new Distance[width*height];
	/*for(int i=0;i<width*height;i++)
		dists[i] = -1;*/
	int *queue = new int[width*height];
	queue[0] = sy * width + sx;
	int c_queue = 0;
	int n_queue = 1;

	dists[sy * width + sx].cost = 0;
	dists[sy * width + sx].squares = 0;

	bool reached_target = false;

	if( include_travel ) {
		const City *city = this->findCity(sx, sy);
		if( city != NULL ) {
			// TODO: cache target squares from City
			city->initTravel(false, unit == NULL ? NULL : unit->getTemplate());
			for(int i=0;i<width*height && !reached_target;i++) {
				if( squares[i].isTarget() ) {
					dists[i].cost = 1;
					dists[i].squares = 1;
					queue[n_queue++] = i;
					if( i == ey * width + ex ) {
						reached_target = true;
					}
				}
			}
			this->resetTargets();
		}
	}

	while( !reached_target ) {
		for(int i=0;i<width*height;i++)
			temp[i] = dists[i];

		// process items in queue
		int old_n_queue = n_queue;
		for(int i=c_queue;i<old_n_queue && !reached_target;i++) {
			int indx = queue[i];
			int cost = dists[indx].cost;
			int squares = dists[indx].squares;
			ASSERT( cost != -1 );
			ASSERT( squares != -1 );
			int x = indx % width;
			int y = indx / width;
			//LOG("process queue: %d : %d ( %d, %d) = %d\n", i, indx, x, y, cost);
			const MapSquare *from_square = this->getSquare(x, y);

			for(int cy=y-1;cy<=y+1 && !reached_target;cy++) {
				for(int cx=x-1;cx<=x+1 && !reached_target;cx++) {

					if( cx == x && cy == y )
						continue;

					if( !isValid(cx,cy) )
						continue;

					//if( dists[cy*width + cx] == -1 )
					{
						const MapSquare *square = this->getSquare(cx, cy);
						bool valid_move = false;
						if( by_sea )
							valid_move = !square->isLand();
						else if( unit == NULL ) {
							// general land unit
							valid_move = square->canMoveTo(NULL);
						}
						else {
							// also take into account other units there, and if we can attack
							valid_move = unit->canMoveTo( Pos2D(cx, cy) );
						}
						/*if( ( unit == NULL && ( square->isLand() == !by_sea ) ) ||
						( unit != NULL && unit->canMoveTo( Pos2D(cx, cy) ) ) ) {*/
						if( valid_move ) {
							int mp = forwards ? square->getScaledMovementCost() : from_square->getScaledMovementCost();
							if( unit == NULL )
								mp = min(mp,road_move_scale_c);
							else
								mp = min(mp,road_move_scale_c*unit->getTemplate()->getMoves());
							int val = cost + mp;
							ASSERT( val <= MAX_VALUE );

							// add to queue if not already present
							if( temp[cy * width + cx].cost == -1 ) {
								//LOG("queue[%d] = %d : ( %d, %d ) = %d\n", n_queue, cy*width + cx, cx, cy, val);
								ASSERT( n_queue < width*height );
								/*for(int j=0;j<n_queue;j++) {
								if( queue[j] == cy*width + cx ) {
								LOG("### repeat: %d\n", j);
								ASSERT(false);
								}
								}*/
								queue[n_queue++] = cy*width + cx;
							}

							int c_val = temp[cy * width + cx].cost;
							if( c_val == -1 || val <= c_val ) {
								/*if( c_val != -1 && val < c_val ) {
									VI_log("    overwrote (%d,%d) : old cost %d, new cost %d\n", cx, cy, c_val, val);
								}*/
								temp[cy * width + cx].cost = val;
								int c_squares = temp[cy * width + cx].squares;
								if( c_squares == -1 || val < c_val || squares+1 <= c_squares ) // if val < c_val (i.e., we've found a route with lower cost), we should always reset the number of squares
									temp[cy * width + cx].squares = squares+1;
							}
							if( cx == ex && cy == ey ) {
								reached_target = true;
							}
							/*if( stop_x != -1 && stop_y != -1 ) {
							int diffx = abs( cx - stop_x );
							int diffy = abs( cy - stop_y );
							if( diffx <= 1 && diffy <= 1 ) {
							check_stop = true;
							}
							}*/
						}
					}

				}
			}
		}
		// print out new info:
		//if( sx == 53 && sy == 10 ) {
		/*if( true ) {
			VI_log("queue expanded from %d to %d\n", old_n_queue, n_queue);
			for(int i=old_n_queue;i<n_queue;i++) {
				int indx = queue[i];
				int x = indx % width;
				int y = indx / width;
				VI_log("%d: (%d,%d) : cost %d, squares %d\n", i, x, y, temp[indx].cost, temp[indx].squares);
			}
		}*/
		//LOG("===\n");

		c_queue = old_n_queue;
		if( c_queue == n_queue ) {
			break;
		}

		swap(temp, dists);
		/*if( check_stop && !done && dists[stop_y * width + stop_x] != -1 ) {
		done = true;
		for(int dir=0;dir<8 && done;dir++) {
		int cx = 0, cy = 0;
		if( !this->translateDirection(&cx, &cy, stop_x, stop_y, dir) ) {
		if( dists[cy * width + cx] == -1 && unit->validSquare(cx,cy) ) {
		// valid adjacent square that hasn't been checked
		done = false;
		}
		}
		}
		}*/
	}

	delete [] temp;
	delete [] queue;
	//VI_log("### Map::calculateDistanceMap %d : %d\n", unit, clock() - time_s);
	return dists;
}
#endif

void Map::resetTargets() {
	for(int i=0;i<width*height;i++) {
		this->squares[i].setTarget(false);
	}
}

#if 0
short *Map::calculateDistanceMap(const Unit *unit, int sx, int sy, int ex, int ey) const {
	// N.B. - the AI cheats here, in that we also know the distance map for unexplored area.
	// To be fairer, we should estimate the movement cost always as (1*3) for unexplored squares?
	int time_s = clock();
	// if unit is NULL, assume general land unit
	short *dists = new short[width*height];
	short *temp = new short[width*height];
	for(int i=0;i<width*height;i++)
		dists[i] = -1;

	dists[sy * width + sx] = 0;
	bool done = false;
	bool reached_target = false;
	while( !done && !reached_target ) {
		done = true;
		for(int i=0;i<width*height;i++)
			temp[i] = -1;
		//bool check_stop = false;
		for(int y=0;y<height && !reached_target;y++) {
			for(int x=0;x<width && !reached_target;x++) {
				int cost = dists[y * width + x];
				if(cost==-1) {
					/*if(unit->validLocation(this,x,y))
					done = false;*/
					continue;
				}
				for(int cy=y-1;cy<=y+1 && !reached_target;cy++) {
					for(int cx=x-1;cx<=x+1 && !reached_target;cx++) {

						if( !isValid(cx,cy) )
							continue;

						if( dists[cy*width + cx] == -1 ) {
							MapSquare *square = this->getSquare(cx, cy);
							if( ( unit == NULL && square->isLand() ) ||
								( unit != NULL && unit->canMoveTo( square ) ) ) {
									int mp = square->getScaledMovementCost();
									if( unit == NULL )
										mp = min(mp,road_move_scale_c);
									else
										mp = min(mp,road_move_scale_c*unit->getTemplate()->getMoves());
									temp[cy * width + cx] = cost + mp;
									if( cx == ex && cy == ey ) {
										reached_target = true;
									}
									done = false;
									/*if( stop_x != -1 && stop_y != -1 ) {
									int diffx = abs( cx - stop_x );
									int diffy = abs( cy - stop_y );
									if( diffx <= 1 && diffy <= 1 ) {
									check_stop = true;
									}
									}*/
							}
						}

					}
				}
				temp[y * width + x] = cost;
			}
		}
		/*short *t = temp;
		temp = dists;
		dists = t;*/
		swap(temp, dists);
		/*if( check_stop && !done && dists[stop_y * width + stop_x] != -1 ) {
		done = true;
		for(int dir=0;dir<8 && done;dir++) {
		int cx = 0, cy = 0;
		if( !this->translateDirection(&cx, &cy, stop_x, stop_y, dir) ) {
		if( dists[cy * width + cx] == -1 && unit->validSquare(cx,cy) ) {
		// valid adjacent square that hasn't been checked
		done = false;
		}
		}
		}
		}*/
	}

	delete [] temp;
	VI_log("### Map::calculateDistanceMap %d : %d\n", unit, clock() - time_s);
	return dists;
}
#endif
