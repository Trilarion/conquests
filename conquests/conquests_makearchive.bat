rd /S /Q c:\temp\conquests\

mkdir c:\temp\conquests\

REM set src="c:\documents and settings\mark\work\programming\vision3"
REM set src="u:\programming\vision3"
set src="."
set dst="c:\temp\conquests"

copy %src%\conquests_Release\conquests.exe %dst%
copy %src%\readme.html %dst%
copy %src%\reference.html %dst%
copy %src%\FreeImage.dll %dst%
copy %src%\cg.dll %dst%
copy %src%\cggl.dll %dst%
copy %src%\cgD3D9.dll %dst%
copy %src%\SDL.dll %dst%
copy %src%\SDL_mixer.dll %dst%
copy %src%\libogg-0.dll %dst%
copy %src%\libvorbis-0.dll %dst%
copy %src%\libvorbisfile-3.dll %dst%
copy %src%\freetype6.dll %dst%
copy %src%\zlib1.dll %dst%
copy %src%\lua5.1.dll %dst%

copy %src%\gpl.txt %dst%

REM Preferences and Qt DLLs
copy %src%\..\Preferences.exe "%dst%\Conquests Preferences.exe"
copy %src%\..\libgcc_s_dw2-1.dll %dst%
copy %src%\..\mingwm10.dll %dst%
copy %src%\..\QtCore4.dll %dst%
copy %src%\..\QtGui4.dll %dst%

mkdir %dst%\shaders
copy %src%\..\Vision\shaders\*.cg %dst%\shaders\

mkdir %dst%\conquests

mkdir %dst%\conquests\shaders
copy %src%\conquests\shaders\shaders.cg %dst%\conquests\shaders

mkdir %dst%\conquests\data

mkdir %dst%\conquests\data\gfx
copy %src%\conquests\data\gfx\*.* %dst%\conquests\data\gfx

mkdir %dst%\conquests\data\gfx\civs
copy %src%\conquests\data\gfx\civs\*.* %dst%\conquests\data\gfx\civs

mkdir %dst%\conquests\data\sound
copy %src%\conquests\data\sound\*.* %dst%\conquests\data\sound

mkdir %dst%\conquests\data\music
copy %src%\conquests\data\music\*.* %dst%\conquests\data\music

mkdir %dst%\conquests\data\scripts
copy %src%\conquests\data\scripts\*.lua %dst%\conquests\data\scripts

mkdir %dst%\conquests\data\maps
copy %src%\conquests\data\maps\*.* %dst%\conquests\data\maps
