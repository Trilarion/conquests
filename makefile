# This file should be in the root directory, i.e., with subfolders conquests/,
# Vision/ and VisionPreferences/ .
# This is needed on Linux for debuild, as it expects a makefile in the root
# folder.

all:
	cd conquests/; $(MAKE)

install:
	cd conquests/; $(MAKE) install

uninstall:
	cd conquests/; $(MAKE) uninstall

clean:
	cd conquests/; $(MAKE) clean

